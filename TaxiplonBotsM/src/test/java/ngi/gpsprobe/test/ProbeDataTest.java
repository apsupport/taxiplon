package ngi.gpsprobe.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.auth.CredentialsProvider;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import aploon.httpconnector.HttpConnectionProvider;
import aploon.ngi.gpsprobe.ProbeData;

import com.google.gson.Gson;

public class ProbeDataTest {

	private static final Logger LOGGER = Logger.getLogger(ProbeDataTest.class);
	
	private ProbeData probeData;
	private Gson gson;
	
	@Before
	public void createProbeData() {
		probeData = new ProbeData();
		probeData.setAltitude(81.2);
		probeData.setLatitude(54.2);
		probeData.setLongitude(4.3);
		probeData.setHeading(2.4);
		probeData.setNumberOfSatellites(4);
		probeData.setSpeed(111.1);
		probeData.setTimestamp(99999999L);
		gson = new Gson();
	}
	
	@Test
	public void testProbeData() {
	
		String jsonsStr = gson.toJson(probeData);
		LOGGER.info("The json str "+jsonsStr);
	}

	//@Test
	public void testStr() {
		
		LOGGER.info(String.format("{\"DeviceId\": %d,\"ProbeData\": %s }", 12L,gson.toJson(new ProbeData[] { probeData} )));
		
	}
	
	@Test
	public void testSendData() throws HttpException, IOException {
		
		Map<String,Object> probe = new HashMap<String, Object>();
		probe.put("DeviceId", "1");
		probe.put("ProbeData", new ProbeData[]{probeData});
		String jsonsStr = gson.toJson(probe);
		LOGGER.info("The jsonStr "+jsonsStr);
		PostMethod postMethod = new PostMethod("https://gpsprobe.geointelligence.gr/api/probe");
		HttpClient httpClient = HttpConnectionProvider.getHttpClient();
		httpClient.getParams().setAuthenticationPreemptive(true);
		httpClient.getState().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("taxiplon", "+@><1Pl)_$%on1542"));
	    StringRequestEntity stringRequestEntity = new StringRequestEntity(jsonsStr, "application/json", "UTF-8");
		postMethod.setRequestEntity(stringRequestEntity);
		httpClient.executeMethod(postMethod);
		String response = new String(postMethod.getResponseBody());
		postMethod.releaseConnection();
		LOGGER.info("The response body is "+response);
	}
	
}
