package aploon.xmpp.ssclient.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.taxiplon.db.NamedParameterStatement;

import aploon.datahelper.DataHelper;
import aploon.datahelper.DatasourceProvider;

public class DispatcherTest {

	private static final Logger logger = Logger.getLogger(DispatcherTest.class);
	
	@Test
	public void findNearByDevices() throws SQLException {
		
		Connection connection = DatasourceProvider.getConnection();
		
		NamedParameterStatement namedParameterStatement = new NamedParameterStatement(connection,
				"SELECT * FROM (SELECT DISTINCT(d.deviceid), "
						+ "d.currentlat,d.currentlong, "
							+ "d.imei, d.locale, d.extra, dr.driverId, "
							+ "distance_between(d.currentlat, d.currentlong, t.pickuplat, t.pickuplong) as distance, "
							+ "IF(t.zoneid IS NULL,NULL,dr.zoneJoinTime) as zoneJoinTime,"
							+ "dc.taxicompanyid "
							+ "FROM device d, driver dr, "
							+ "drivercompanies dc, trip t, "
							+ "passenger p WHERE t.tripid = :tripidArg "
							+ "AND p.passengerid = t.passengerid "
							+ "AND dc.driverid = dr.driverid "
							+ "AND dc.taxicompanyid = t.taxicompanyid "
							+ "AND dr.deviceid = d.deviceid "
							+ "AND d.status = 'ava' AND dr.active = 'true' "
							+ "AND dr.status = 'act' "
							+ "AND (case when (t.zoneid IS NULL) then dr.OnRide = 'false' else TRUE end) "
							+ "AND (case when (t.zoneid IS NULL) then TRUE else ( dr.zoneid=t.zoneid AND dr.zoneJoinTime IS NOT NULL) end)"
							+ "AND (case when ( "
							+ " (SELECT calltype.ForceAssignment FROM calltype WHERE calltype.calltypeid=t.calltypeid ) IS FALSE ) "
							+ "then TRUE else (dr.zoneid IS NULL AND dr.zoneJoinTime IS NULL ) end) "
							+ "AND (dr.driverid NOT IN "
							+ "(SELECT bla.DriverID FROM blacklisted bla WHERE bla.PassengerID = t.passengerid ))) a"
							+ ", taxicompany tc WHERE a.taxicompanyid = tc.taxicompanyid "
							+ "AND ((:multi IS NULL AND distance <= tc.hailrange) "
							+ "OR (:multi IS NOT NULL AND distance <= (tc.hailrange * :multi))) "
							+ "ORDER BY zoneJoinTime,distance ASC LIMIT :numberOfDevices");

		namedParameterStatement.setLong("tripidArg", 8315341L);
		namedParameterStatement.setLong("numberOfDevices", 10);
		namedParameterStatement.setString("multi", null);
	
		ResultSet rs = namedParameterStatement.executeQuery();
		
		while (rs.next()) {
			
			logger.debug(rs.getLong("driverId")+" "+rs.getLong("deviceid")+" "+
					rs.getDouble("distance")+
					rs.getBigDecimal("currentlat")+" "+rs.getBigDecimal("currentlong")+" "+
					rs.getString("imei")+" "+" "+
					rs.getString("locale")+" "+rs.getString("extra")+" "+
					rs.getString("brandname"));
		}
		
		rs.close();
		
		namedParameterStatement.close();

		connection.close();
		
		List<Long> driverIds = DataHelper.getPassengerDriversLastDays(73229, 0);
		
		for(Long driverId:driverIds) {
			
			logger.debug("The driverid is "+driverId);
			
		}
		

	}
	
	@Test
	public void findNearByDevicesByType() {
		
	}
	
}