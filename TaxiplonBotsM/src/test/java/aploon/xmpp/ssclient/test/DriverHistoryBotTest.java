package aploon.xmpp.ssclient.test;

import org.apache.log4j.Logger;
import org.junit.Test;

import aploon.xmpp.ssclient.DriverHistoryBot;

public class DriverHistoryBotTest {

	private static final Logger LOGGER = Logger.getLogger(DriverHistoryBotTest.class);
	
	//@Test
	public void testDriverInfo() throws Exception {
		LOGGER.info("Now will get the driver info");
		String newone = DriverHistoryBot.getAssignedRides(70L);
		LOGGER.info(newone);
	}
	
	//@Test
	public void testDriverLost() throws Exception {
		LOGGER.info("Get lost trips");
		String lostrip = DriverHistoryBot.getAssignedRides(70L);
		LOGGER.info(lostrip);
	}
	
	@Test
	public void testTripPicked() throws Exception {
		LOGGER.info("Get trips picked");
		String ltrippicked = DriverHistoryBot.getTripPicked(1022364L);
		LOGGER.info(ltrippicked);
	}
	
}
