package aploon.xmpp.ssclient.client.test;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.proxy.ProxyInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import aploon.xmpp.Communicator;
import aploon.xmpp.ssclient.registration.RegisterPassenger;

public class DriverHistoryBotClient {

	private XMPPConnection xmppConnection;
	private Chat chat;
		
	private static final String TARGET_USER = "driverhistorybot";
	private static final String TARGET_SERVICE = "driver.taxiplon.com";
	
	private static final Logger LOGGER = Logger.getLogger(DriverHistoryBotClient.class);
	
	private RegisterPassenger registerPassenger;
	
	/*
	private static final MessageListener messageListener = new MessageListener() {
		
		@Override
		public void processMessage(Chat arg0, Message arg1) {
			LOGGER.info("The message is "+arg1.getBody());
		}
	};
	*/
	
	
	@Before
	public void prepare() throws Exception {
	
		/**
		SmackConfiguration.setKeepAliveInterval(1000*30);
		ConnectionConfiguration config = new ConnectionConfiguration("95.138.183.132", 5222,
					       "driver.taxiplon.com", ProxyInfo.forDefaultProxy());
		config.setReconnectionAllowed(true);
    	config.setSASLAuthenticationEnabled(true);
    	SASLAuthentication.supportSASLMechanism("PLAIN");
    	xmppConnection = new XMPPConnection(config);
    	xmppConnection.connect();
    	xmppConnection.login("driver", "driver","860162010028949");
    	
    	LOGGER.info("Connection done");
    	*/
	}
	
	//@Test
	public void sendMessageToLogin() throws XMPPException {
		
		/**
		LOGGER.info("Sending the messages from chatmanager");
		
		Chat chat = xmppConnection.getChatManager().createChat("login", new MessageListener() {
			
			@Override
			public void processMessage(Chat arg0, Message arg1) {
				LOGGER.info("Got a message from "+arg1.getFrom()+" "+arg1.getBody()+" ");
			}
		});
		
		
		
		Message message = new Message();
		message.setBody("act=list&imei=351780064047188");
		
		chat.sendMessage(message);
		
		LOGGER.info("Message sent");
		
		while(true);
		*/
	}
	
	//@Test
	public void changePassword() throws XMPPException {
		
		LOGGER.info("Changing password");
		
		/**
		AccountManager accountManager = new AccountManager(xmppConnection);
		accountManager.changePassword("mitsos99");
		
		LOGGER.info("changed password");
	
		while(true);
		*/
	}
	
	//@Test
	public void deleteAccount() throws XMPPException {
		
		LOGGER.info("Deleting the account");
		
		/**
		AccountManager accountManager = new AccountManager(xmppConnection);
		accountManager.deleteAccount();
		*/
	}
	
	//@Test
	public void createAccount() throws XMPPException {
		
		/**
		AccountManager accouAccountManager = new AccountManager(xmppConnection);
		accouAccountManager.createAccount("dude","dude" );
		*/
	}
	
	/*
	@Test
	public void createUser() throws XMPPException {
	
		LOGGER.info("Continuing to user");
		
		registerPassenger.createXMPPAccount(Communicator.PASS_UNAME_PREFIX+"63748482","687467362" , "", "Passenger");
	
		LOGGER.info("Continuing to user");
	}
  	*/
	
	/*
	@Test
	public void getCompleted() throws XMPPException {
	
		LOGGER.info("Getting the completed trips");
		chat.sendMessage("action=assigned&driverId=70");
	}

	@Test
	public void getLost() throws XMPPException {
		
		LOGGER.info("Getting the lost trips");
		chat.sendMessage("action=lost&driverId=70&deviceId=1708");
	}
	
	@Test 
	public void getTripPicked() throws XMPPException {
		
		LOGGER.info("Getting the trip picked");
		chat.sendMessage("action=trippicked&tripId=1022364");	
	}
	
	@Test
	public void getTripSummary() throws XMPPException {
		
		LOGGER.info("Getting the summary");
		chat.sendMessage("action=summary&driverId=70");	
	}
	*/
	
	
	
	@After
	public void waitForReply() {
		
		/*
		LOGGER.info("Fetching the message");
		PacketFilter filter = new AndFilter(new PacketTypeFilter(Message.class),new FromContainsFilter(TARGET_USER+"@"+TARGET_SERVICE));
		PacketCollector packetCollector = xmppConnection.createPacketCollector(filter);

		Packet packet = packetCollector.nextResult(100000);
		if(packet instanceof Message) {
			Message message = (Message) packet;
			LOGGER.info("The message is "+message.getBody());
		}
		*/

	}
	
}
