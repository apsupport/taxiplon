package aploon.xmpp.ssclient.poi.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.junit.Test;

public class UserAgentTest {

	@Test
	public void testIt() throws IOException {
		
		URL url = new URL("http://www.marinetraffic.com/en/ais/index/port_moves/port:PIRAEUS/move_type:0");
		
		URLConnection conn = url.openConnection();
		conn.setRequestProperty("User-Agent",
		        "Mozilla/5.0 (Windows NT 5.1; rv:19.0) Gecko/20100101 Firefox/19.0");
		conn.connect();
		
		
		BufferedReader serverResponse = new BufferedReader(
				new InputStreamReader(conn.getInputStream())
				);
		
		String theLine = null;
		
		while ( (theLine=serverResponse.readLine())!=null ) {
			System.out.println("A line "+theLine);
		}
		
		serverResponse.close();

	}
	
}
