package aploon.xmpp.ssclient.poi.test;

import gr.datamation.dataloader.entities.ShipInfo;
import gr.datamation.dataloader.parser.MarineTrafficParser;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.taxiplon.Util;

import aploon.model.Coordinates;



public class BoatParseTest {

	private ArrayList<PortData> boatReports;
	private PortEntry[] portsOfInterest;

	private static final Logger LOGGER = Logger.getLogger(BoatParseTest.class);
	
	@Before
	public void setUp() {
	
		/**
	    Authenticator.setDefault(new ProxyAuthenticator("mgkatziouras", "Pfoa604#"));  
	    System.setProperty("http.proxyHost", "192.168.1.5");  
	    System.setProperty("http.proxyPort", "3129");  
	    */
	    
		String portsInprops = Util.getProperty("ports", "config");
		portsOfInterest = parsePortEntryInPropsFile(portsInprops);
	}
	
	//@Test
	public void parseBoats() {

		LOGGER.info("The parsing begins");
		
	}

	@Test
	public void getArrivals() {
		
		MarineTrafficParser marineTrafficParser = new MarineTrafficParser("PIRAEUS");
		List<ShipInfo> shipInfos = marineTrafficParser.getEstimatedArrivals();
		for(ShipInfo shipInfo:shipInfos) {
			LOGGER.info("The shipInfo "+shipInfo.getPort());
		}
		
	}
	
	
	private PortEntry[] parsePortEntryInPropsFile(String portsInPropsFile) {
    	PortEntry[] ports;
    	if (portsInPropsFile == null) {
    		ports = new PortEntry[0];
    	} else {
    		String[] temp = portsInPropsFile.split("\n"); //one port entry per line
    		ports = new PortEntry[temp.length];
    		for (int i = 0; i < temp.length; i++) {
    			try {
    				String[] stemp = temp[i].split("	");
    				ports[i] = stemp.length > 1 ? new PortEntry(stemp[0], stemp[1], stemp[2]) : new PortEntry(stemp[0]);
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	
    	return ports;
    }
	
	class PortEntry {

		String portName;
		long range;
		Coordinates location;

		PortEntry(String name) {
		    portName = name;
		    range = -1;
		}

		PortEntry(String name, String rang, String coords) {
		    portName = name;
		    range = Long.parseLong(rang);
		    String[] cor = coords.split("@");
		    location = new Coordinates(Double.parseDouble(cor[0]), Double.parseDouble(cor[1]));
		}


		public String getPort() {
		    return portName;
		}

		public Coordinates getLocation() {
		    return location;
		}

		public long getRange() {
		    return range;
		}

		public boolean isInRange(Coordinates position) {
		    if (range == -1 || position == null)
			return true;

		    return (location.distFromCoordinates(position.getLat(), position.getLng()) <= (range * 1000));
		}
	
	}
	
	class PortData {
    	
    	PortEntry boatport;
		String portData;

		public PortData(PortEntry port, String dat) {
			boatport = port;
			portData = dat;
		}

		public PortEntry getPort() {
			return boatport;
		}

		public String getPortData() {
			return portData;
		}

    }
	
	
	class ProxyAuthenticator extends Authenticator {  
		  
	    private String user, password;  
	  
	    public ProxyAuthenticator(String user, String password) {  
	        this.user = user;  
	        this.password = password;  
	    }  
	  
	    protected PasswordAuthentication getPasswordAuthentication() {  
	        return new PasswordAuthentication(user, password.toCharArray());  
	    }  
	}  
	
}
