package aploon.xmpp.ssclient.iphone;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.taxiplon.Util;

public class PushSenderTest {

	private static final Logger LOGGER = Logger.getLogger(PushSenderTest.class);
	
	private PushSender pushSender;
	
	@Before
	public void setUp() {
		
		LOGGER.info("Setting up the sender");
		pushSender = new PushSender(false, null);
		
	}
	
	@Test
	public void test() throws Exception {

		PushSender.getInstance(null).sendPushNotification(
				"8059d20f d2fe3cab 4398a173 c8e46d36 28b4e966 8896264d 1d897daa 7b58e210@i3.2.0",
				"Tralalalala", 
				"DriverAssign", 
				"Trololololo", null, 0, Util.getProperty("push_sound", "config"));
	
	}

	@After
	public void destroy() {
		
		LOGGER.info("Destroying the pusher");
		while(true);
		
	}
	
}
