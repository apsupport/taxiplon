package aploon.xmpp.test;

import org.jivesoftware.smack.XMPPException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import aploon.xmpp.Communicator;

public class CommunicatorTest {

	private Communicator communicator;
	
	@Before
	public void setUp() throws XMPPException {
		
		communicator = new Communicator("dispatcher", "dispatcher", "testit");
		
	}

	@Test
	public void sendSimpleMessage() throws XMPPException {
	
		System.out.println("Sending the message");
		
		communicator.sendMessageToDriver("<PlaceCall><distance>0.118</distance><Call><action>place</action><id>5023665</id><Info><lat>38.040718</lat><lng>23.7508974</lng><gaddress>Ελ Αλαμέιν 10 | Νέα Ιωνία</gaddress><regtime>1408622478000</regtime><status>req</status><name>111</name><mobile>111</mobile>fare>0.0</fare><callTypeName>Default Call</callTypeName><clearBiddingTime>5</clearBiddingTime><requestWaitTime>25</requestWaitTime></Info></Call></PlaceCall>", "351780064047188");
		
		System.out.println("Teh message was sent");
	}
	
	@After
	public void afterSend() {
		while(true);
	}
	
}
