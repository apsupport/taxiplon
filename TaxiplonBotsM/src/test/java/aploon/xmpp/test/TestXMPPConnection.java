package aploon.xmpp.test;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestXMPPConnection {

	private XMPPConnection xmppConnection;
	
	@Before
	public void setUp() throws XMPPException {
		
	}
	
	@Test
	public void sendSimpleMessage() throws XMPPException, InterruptedException {
	
		//communicator = new Communicator("d1", "d1","test1");
		
		ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration(
				"staging.aploon.internal", 
				5222, "driver.taxiplon.com");
		
		xmppConnection = new XMPPConnection(connectionConfiguration);
		xmppConnection.connect();
		System.out.println("Connect successufll ");
		xmppConnection.login("dummy", "dummy", "registertest");
		
		DeliveryReceiptManager.getInstanceFor(xmppConnection).enableAutoReceipts();
		
		
		System.out.println("login ok ");
		
		PacketFilter packetFilter = new PacketTypeFilter(Message.class);
		
		PacketListener requestListener = new PacketListener() {
			
			public void processPacket(Packet pack) {
				
				System.out.println(pack.toString());
				
				if (pack instanceof Message && pack.getError() == null) {
			
					Message msg = (Message)pack;
					
					System.out.println("The msg is "+msg.getBody());
					System.out.println("Tthe full is "+msg.getXmlns());
					
				}
			}
		};
		
		xmppConnection.addPacketListener(requestListener, packetFilter);
		
		/**
		Chat chat = xmppConnection.getChatManager().createChat("dummy", new MessageListener() {
			
			public void processMessage(Chat arg0, Message arg1) {
				System.out.println(arg1.getXmlns());
				System.out.println(arg1.getBody());
				
			}
		});
		
		System.out.println("Chat 1 set");
		
		Chat chat2 = xmppConnection.getChatManager().createChat("d1", new MessageListener() {
			
			public void processMessage(Chat arg0, Message arg1) {
				System.out.println(arg1.getXmlns());
				System.out.println(arg1.getBody());
				
			}
		});
		*/
		System.out.println("Chat set ");		
		
		while(true);
	}
	
	
	@After
	public void destroy() throws Exception {
	
		xmppConnection.disconnect();
	}
	
}
