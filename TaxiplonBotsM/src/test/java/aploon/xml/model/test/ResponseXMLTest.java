package aploon.xml.model.test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Test;

import aploon.xml.model.ResponseXml;

public class ResponseXMLTest {

	//@Test
	public void test() throws JAXBException {
		
		ResponseXml responseXML = new ResponseXml();
		JAXBContext jaxbContext = JAXBContext.newInstance(ResponseXml.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
		marshaller.marshal(responseXML, System.out);
		
	}

}
