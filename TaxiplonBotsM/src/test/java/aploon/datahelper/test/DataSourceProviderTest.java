package aploon.datahelper.test;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.junit.Test;

import aploon.datahelper.DatasourceProvider;

public class DataSourceProviderTest {

	private static final Logger LOGGER = Logger.getLogger(DataSourceProviderTest.class);
	
	//@Test
	public void test() throws SQLException {
		
		LOGGER.info("Initializing the test");
		
		DataSource dataSource = DatasourceProvider.getDataSource();
		Connection connection = dataSource.getConnection();
		
		LOGGER.info("Successfully created a connection");
		
		connection.close();
		
	}

}
