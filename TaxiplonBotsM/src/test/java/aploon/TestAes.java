package aploon;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Test;

import com.taxiplon.Util;

public class TestAes {

	private static final byte[] messageKey = new byte[] { 'm', 'e', 's', 's', 'a', 'g', 'e', 'i', 'd', '_', '_', '_', '_' ,'K','e','y'};

	@Test
	public void checkEncrytpion() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		String theUtil = Util.encrypt("5777|null|1409229959", messageKey);
		System.out.println("The encrypt is "+theUtil);
		
		String theUnecript = Util.decrypt(theUtil,messageKey);
		System.out.println("THe descrypt is "+theUnecript);
	
	}
	
}
