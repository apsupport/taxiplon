package com.aploon.mbean;

public interface LoggerMBean {

	public void setDebugAppender();
	
	public void removeDebugAppender();

}
