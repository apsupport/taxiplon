package com.aploon.mbean;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class LoggerMBeanImpl implements LoggerMBean {

	private FileAppender fileAppender;
	
	@Override
	public void setDebugAppender() {
		fileAppender = new FileAppender();
		fileAppender.setName("DebugAppender");
		fileAppender.setFile("/var/log/taxiplon/taxiplon_debug.log");
		fileAppender.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		fileAppender.setThreshold(Level.DEBUG);
		fileAppender.activateOptions();
		Logger.getRootLogger().addAppender(fileAppender);
	}

	@Override
	public void removeDebugAppender() {
		Logger.getRootLogger().removeAppender(fileAppender);
	}

}
