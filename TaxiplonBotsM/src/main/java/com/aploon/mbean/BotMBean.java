package com.aploon.mbean;

public interface BotMBean {

	public void openConnection();
	
	public void closeConnection();
	
}
