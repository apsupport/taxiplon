package com.aploon.mbean;

public interface DispatcherMBean {

	public void enableGoogleDistance();
	
	public void disableGoogleDistance();
	
	public void openConnection();
	
	public void closeConnection();
	
}
