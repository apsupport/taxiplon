package com.aploon.geocoding;

public enum GeoProvider {

	google,ngi,google_route,ngi_route;

	public static GeoProvider getGeoProvider(int num) {

		GeoProvider geoProvider;
		
		switch (num) {
		case 1:
			geoProvider = GeoProvider.google;
			break;
		case 2:
			geoProvider = GeoProvider.ngi;
			break;
		case 3:
			geoProvider = GeoProvider.google_route;
			break;
		case 4:
			geoProvider = GeoProvider.ngi_route;
			break;
		default:
			geoProvider = null;
			break;
		}

		return geoProvider;
	}

}
