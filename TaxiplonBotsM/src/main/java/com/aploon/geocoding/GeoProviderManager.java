package com.aploon.geocoding;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class GeoProviderManager {

	private static final Logger LOGGER = Logger.getLogger(GeoProviderManager.class);
	
	public static AploonGeoProvider getGeoProvider(Long taxiCompanyId,GeoProvider geoProvider) {
		
		LOGGER.info("The taxicompany is "+taxiCompanyId+" "+geoProvider);
		
		LOGGER.info("The geoprovider selected "+geoProvider);
		
		AploonGeoProvider aploonGeoProvider = null;
		
		if(geoProvider!=null&&taxiCompanyId!=null) {
	
			Properties properties = getGeoProperties(taxiCompanyId, geoProvider);
				
			switch (geoProvider) {
			case google_route:
				aploonGeoProvider = new GoogleGeocoder(properties);
				break;
			case ngi_route:
				if(properties!=null) {
					//aploonGeoProvider = new NgiGeocoder(properties);
				}
				break;
			default:
				break;
			}
			
		}
		
		return aploonGeoProvider;
	}

	private static Properties getGeoProperties(Long taxiCompanyid,GeoProvider geoProvider) {
		
		Properties properties = null;
		
		try {
			String propertyFile = "geoprovider_"+taxiCompanyid+"_"+geoProvider.toString()+".properties";
			InputStream is = GeoProvider.class.getClassLoader().getResourceAsStream(propertyFile);
			if(is==null) {
				File propertiesFile = new File(propertyFile);
				if(propertiesFile.exists()) {
					is = new FileInputStream(propertiesFile);
				}
			}

			if(is!=null) {
				properties = new Properties();
				properties.load(is);
			}
			
		} catch(FileNotFoundException e) {
			LOGGER.error("Problem loading properties files for geoprovider",e);
		} catch (IOException e) {
			LOGGER.error("Problem loading properties files for geoprovider IO",e);
		}
		return properties;
	}
	
}
