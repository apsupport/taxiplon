package com.aploon.geocoding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import aploon.model.Coordinates;

public class GoogleGeocoder implements AploonGeoProvider {

	private static final String googleMatrixApiTmpl = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=%s&destinations=%s&mode=driving&sensor=false";
	private static final Logger LOGGER = Logger.getLogger(GoogleGeocoder.class);
	private String apiKey = null;
	
	public GoogleGeocoder(Properties properties) {
		
		if(properties!=null) {
			if(properties.contains("api_key")) {
				apiKey = properties.getProperty("api_key");
			}
		}
	}
	
	public Map<Long, BigDecimal> orderedCoordinates(Map<Long, Coordinates> driverCoordinates,Coordinates towards) {
	
		String towardsParam = towards.getLat()+","+towards.getLng();
		StringBuilder orderedParamsBuilder = new StringBuilder();
		
		Set<Long> driverIds = driverCoordinates.keySet();
		Long[] driverArray = driverIds.toArray(new Long[driverIds.size()]);
		
		for(Long driverId:driverArray) {
			
			Coordinates coordinate = driverCoordinates.get(driverId);
			orderedParamsBuilder.append("|"+coordinate.getLat()+","+coordinate.getLng());
		
		}
		
		String coordParam = orderedParamsBuilder.toString().replaceFirst("|", "");
	
		String googleMatrixApiUrl = String.format(googleMatrixApiTmpl,coordParam,towardsParam);
		
		if(apiKey!=null) {
			googleMatrixApiUrl = googleMatrixApiUrl+"&apiKey="+apiKey;
		}
		
		LOGGER.debug("Google matrix api "+googleMatrixApiUrl);
		
		String jsonData = executeRequest(googleMatrixApiUrl);
		
		LOGGER.debug("The data "+jsonData);
		
		Map<Long, BigDecimal> orderedDrivers = null;
		
		try {
			JSONObject jsonObject = new JSONObject(jsonData);
			JSONArray jsonArray = jsonObject.getJSONArray("rows");
			
			if(jsonArray.length()==driverArray.length ) {
				
				orderedDrivers = new HashMap<Long,BigDecimal>();
				for(int i=0;i<driverArray.length;i++) {
					JSONObject jo = jsonArray.getJSONObject(i);
					JSONArray elements = jo.getJSONArray("elements");
					JSONObject element = elements.getJSONObject(0);
					JSONObject distanceObj = element.getJSONObject("distance");
					Integer value = distanceObj.getInt("value");
					
					orderedDrivers.put(driverArray[i], new BigDecimal(value).divide(new BigDecimal("1000")));
				}
				
			} else {
				LOGGER.error("Not functioning valid for all drivers");
			}		
		} catch (JSONException e) {
			LOGGER.error("Malformed json from google?",e);
		}
		
		
		return orderedDrivers;
	}
	
	
	private String executeRequest(String googleUrl) {
		
		try {
			URL url = new URL(googleUrl);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setConnectTimeout(2*1000);
			urlConnection.setReadTimeout(4*1000);
			BufferedReader serverResponse = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			
			String theLine = null;
			StringBuilder stringBuilder = new StringBuilder();
			
			while ( (theLine=serverResponse.readLine())!=null ) {
				stringBuilder.append(theLine);
			}
			
			serverResponse.close();
			
			LOGGER.debug("Response "+urlConnection.getResponseCode()+" "+urlConnection.getResponseMessage());
			
			return stringBuilder.toString();
			
		} catch (IOException e) {
			LOGGER.error("IO exception",e);
		}
		
		return null;
	}
	
}
