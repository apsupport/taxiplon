package com.aploon.geocoding;

import java.math.BigDecimal;
import java.util.Map;

import aploon.model.Coordinates;

public interface AploonGeoProvider {

	public Map<Long, BigDecimal> orderedCoordinates(Map<Long, Coordinates> driverCoordinates,Coordinates towards);

}
