package com.taxiplon.event;

import com.taxiplon.Util;

import java.util.ArrayList;
import java.util.Date;

public abstract class SiteParser {
	
	public static final String DEFAULT_ENCODING = "UTF8";
	
	protected Util util;
	
	public SiteParser() {
		super();
		util = new Util();
	}
	
	protected String getPageContent(String relativeOrAsboluteLink) throws Exception {
	    return util.getContent(relativeOrAsboluteLink.startsWith("http://")? relativeOrAsboluteLink : getBaseUrl() + relativeOrAsboluteLink, DEFAULT_ENCODING);
	}
	
	/**
	 * Method providing a process to complete the task of fetching events for the particular provider.
	 * Implementation should reflect special properties of the provider and invoke parseTodayDataPage.
	 * @throws Exception
	 */
	protected abstract ArrayList<Event> fetchEvents() throws Exception;
	
	/**
	 * Fetches events from web pages, starting with given number.
	 * Resulting events are placed into the events ArrayList.
	 * @param pageNumber
	 * @throws Exception
	 */
	protected abstract void parseTodayDataPage(int pageNumber) throws Exception;
	
	/**
	 * Describes transformations applied to the event properties in order to make it as clear as possible.
	 * Trasformations are specific to the particular provider.
	 * @param nameOfEvent
	 * @param placeOfEvent
	 * @param description
	 * @param addressOfEvent
	 * @param eventType
	 * @param eventCategory
	 * @param eventStartDateTime
	 * @param eventEndDateTime
	 * @param eventDateTimeString
	 * @param url
	 * @param lat
	 * @param lng
	 */
	protected abstract void addEvent(String nameOfEvent, String placeOfEvent, String description, String addressOfEvent,
				 String eventType, String eventCategory, Date eventStartDateTime, Date eventEndDateTime,
				 String eventDateTimeString, String url, Double lat, Double lng);
	
	protected abstract String getBaseUrl();
	
}
