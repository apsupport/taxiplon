package com.taxiplon.event;

import com.taxiplon.Util;

import java.text.SimpleDateFormat;

import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.taxiplon.rss.model.Feed;
import com.taxiplon.rss.model.FeedMessage;

import com.taxiplon.rss.RSSFeedParser;

public class OpenCalendar extends SiteParser {
	
	public static final String BASE_URL = "http://www.opencalendar.gr";
//	public static final String TODAY_EVENTS_RSS_URL = "/portal/resource/scheduledContentArea/topSuggestedEvents/resourceRepresentationType/rss_2.0/feedTitle/%CE%A0%CF%81%CE%BF%CF%84%CE%AC%CF%83%CE%B5%CE%B9%CF%82%20%CE%B3%CE%B9%CE%B1%20%CE%A3%CE%AE%CE%BC%CE%B5%CF%81%CE%B1";
	public static final String TODAY_EVENTS_HTML_URL = "/portal/resource/event/location/attika/period/today/pageNumber/%d";
	
	public OpenCalendar() {
		super();
	}
	
	ArrayList<String> pager = new ArrayList<String>() {
		public boolean add(String s) {
			if(s == null || s.equals("")) {
				return false;
			}
			for(String c : this) {
				if(c.equals(s)) {
					return false;
				}
			}
			return super.add(s);
		}
	};
	
	private ArrayList<Event> events = new ArrayList<Event>();
//	HashMap<String, String> eventEntries = new HashMap<String, String>();
	
	protected String getBaseUrl() {
		return BASE_URL;
	}
	
	public ArrayList<Event> fetchEvents() throws Exception {
	    pager.add(String.format(TODAY_EVENTS_HTML_URL, 1));
	    parseTodayDataPage(1);
//	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//	    for(Event entry : events) {
//	        System.out.println(entry.getEventDateTimeString() + " => " + 
//	                           (entry.getEventStartDateTime() != null? sdf.format(entry.getEventStartDateTime()) : "") + 
//	                           (entry.getEventEndDateTime() != null? "-" + sdf.format(entry.getEventEndDateTime()) : ""));
//	    }
		return events;
	}
	
	public void parseTodayDataPage(int pageNumber) throws Exception {
		if(pageNumber <= pager.size()) {
			String url = pager.get(pageNumber - 1);
			/*
			 * It seems that RSS has insufficient data
			RSSFeedParser parser = new RSSFeedParser(TODAY_EVENTS_RSS_URL, null);
			Feed feed = parser.readFeed();
			for (FeedMessage message : feed.getMessages()) {}
			 */
			//get the today events page
			String today = util.getContent(url.startsWith("http://")? url : BASE_URL + url, DEFAULT_ENCODING);
			//parse html and extract semantics
			Matcher m = Pattern.compile("href=\"([^\"]*)\"[^>]*>[0-9]+</a>").matcher(today);
			while (m.find()) {
				String link = m.group(1);
				pager.add(link);
			}
			//"class=\"contentObjectList\".*?" +
			m = Pattern.compile("<tr>.*?<a href=\"([^\"]*\\/contentObject\\/[^\"]*)\">" + //link to the event
								"([^<]*)</a>" + //event title
								".*?<p>(.*?)</p>" + //description
 								".*?\\/event\\/event(.*?)\\/(.*?)\\/" + //category or type
 								".*?\\/event\\/event.*?\\/(.*?)\\/" + //category or type
								".*?</td><td>([^<]*)</td>" + //where
								".*?<td>([^<]*)</td>" //when
								, Pattern.MULTILINE).matcher(today);
			while(m.find()) {
				int i = 1;
				String link = m.group(i++);
				String title = m.group(i++);
				String description = m.group(i++);
				String[] typeOrCategoryStr = new String[2];
			    String category = null;
			    String type = null;
				typeOrCategoryStr[0] = m.group(i++);//in fact it's unkown which one contains type and which one category
				String temp = m.group(i++);
				if(typeOrCategoryStr[0].equals("Type")) {
				    type = temp;
				    category = m.group(i++);
				}
				else {
					category = temp;
					type = m.group(i++);
				}
			    String where = m.group(i++);
				String when = m.group(i++);
				addEvent(title, null, description, where, type, category, null, null, when, link, null, null);
//				eventEntries.put(link, description + ": " + where + " @ " + when);
			}
			//put extracted and processed data into the list
			//repeat the process
			parseTodayDataPage(++pageNumber);
		}
	}
	
	protected void addEvent(String nameOfEvent, String placeOfEvent, String description, String addressOfEvent,
				 String eventType, String eventCategory, Date eventStartDateTime, Date eventEndDateTime,
				 String eventDateTimeString, String url, Double lat, Double lng) {
		String linkedPage = null;
	    Matcher m;
		//fix the location
		if(lat == null || lng == null) {
			if(linkedPage == null) {
				//linked page might have a google map with coordinates
				try {
					linkedPage = getPageContent(url);
					m = Pattern.compile("GLatLng\\(([0-9\\.,]*), ([0-9\\.,]*)\\)").matcher(linkedPage);
					if(m.find()) {
							lat = Double.parseDouble(m.group(1).replace(',', '.'));
							lng = Double.parseDouble(m.group(2).replace(',', '.'));
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				//if bot finds no address, it will attempt to fetch it through geocoding
			}
		}
		//fix the category
		if(eventType == null && eventCategory == null) {//both must be null to attempt to fix them
			if(linkedPage == null) {
				//linked page might have a google map with coordinates
				try {
					linkedPage = getPageContent(url);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
			//event category and event type seem to be different

			m = Pattern.compile("").matcher(linkedPage);		

			if(m.find()) {
				String[] typeOrCategoryStr = new String[2];
				typeOrCategoryStr[0] = m.group(1);//in fact it's unkown which one contains type and which one category
				String temp = m.group(2);
				if(typeOrCategoryStr[0].equals("Type")) {
				    eventType = temp;
				    eventCategory = m.group(3);
				}
				else {
					eventCategory = temp;
					eventType = m.group(3);
				}
			}
		}
		//fetch datetime
		int hoursFound = 0;
		GregorianCalendar startDate = new GregorianCalendar();
	    startDate.set(Calendar.HOUR_OF_DAY, 0);
	    startDate.set(Calendar.MINUTE, 0);
	    startDate.set(Calendar.SECOND, 0);
		m = Pattern.compile("(\\d{2})[\\.:](\\d{2})[ -]?[(\\d{2})[\\.:](\\d{2})]?").matcher(eventDateTimeString);
		while(m.find()) {
			hoursFound++;
			//parse starting date
			try {
				int startHour = Integer.parseInt(m.group(1));
				int startMin = Integer.parseInt(m.group(2));
				startDate.set(Calendar.HOUR_OF_DAY, startHour);
			    startDate.set(Calendar.MINUTE, startMin);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			//parse ending date if something was found
			if(m.groupCount() > 2) {
				try {
					int endHour = Integer.parseInt(m.group(3));
					int endMin = Integer.parseInt(m.group(4));
					GregorianCalendar gc = new GregorianCalendar();
					gc.set(Calendar.HOUR_OF_DAY, endHour);
					gc.set(Calendar.MINUTE, endMin);
					eventEndDateTime = gc.getTime();
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		//assign the start date anyway: if no time was found it'll be set to the 0:0:0 time
	    eventStartDateTime = startDate.getTime();
	    events.add(new Event(nameOfEvent, placeOfEvent, description, addressOfEvent,
				 eventType, eventCategory, eventStartDateTime, eventEndDateTime,
				 eventDateTimeString, url, lat, lng, hoursFound == 1));
	}
	
	
	public static void main(String args[]) {
		try {
		    OpenCalendar parser = new OpenCalendar();
			parser.fetchEvents();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
