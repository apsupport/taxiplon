package com.taxiplon.event;

import java.util.Date;

public class Event {

	private String nameOfEvent;
	private String placeOfEvent;
	private String description;
	private String addressOfEvent;
	private String eventType;
	private String eventCategory;
	private Date eventStartDateTime;
	private Date eventEndDateTime;
	private String eventDateTimeString;
	private String url;
	private Double lat;
	private Double lng;
	private boolean timeAccurate;

	public Event(String nameOfEvent, String placeOfEvent, String description, String addressOfEvent,
				 String eventType, String eventCategory, Date eventStartDateTime, Date eventEndDateTime,
				 String eventDateTimeString, String url, Double lat, Double lng, boolean timeAccurate) {
		super();
		this.nameOfEvent = nameOfEvent;
		this.placeOfEvent = placeOfEvent;
		this.description = description;
		this.addressOfEvent = addressOfEvent;
		this.eventType = eventType != null? eventType.toLowerCase() : null;
		this.eventCategory = eventCategory != null? eventCategory.toLowerCase() : null;
		this.eventStartDateTime = eventStartDateTime;
		this.eventEndDateTime = eventEndDateTime;
		this.eventDateTimeString = eventDateTimeString;
	    this.url = url;
	    this.lat = lat;
	    this.lng = lng;
	}

	public void setNameOfEvent(String nameOfEvent) {
		this.nameOfEvent = nameOfEvent;
	}

	public String getNameOfEvent() {
		return nameOfEvent;
	}

	public void setPlaceOfEvent(String placeOfEvent) {
		this.placeOfEvent = placeOfEvent;
	}

	public String getPlaceOfEvent() {
		return placeOfEvent;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setAddressOfEvent(String addressOfEvent) {
		this.addressOfEvent = addressOfEvent;
	}

	public String getAddressOfEvent() {
		return addressOfEvent;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventStartDateTime(Date eventStartDateTime) {
		this.eventStartDateTime = eventStartDateTime;
	}

	public Date getEventStartDateTime() {
		return eventStartDateTime;
	}

	public void setEventEndDateTime(Date eventEndDateTime) {
		this.eventEndDateTime = eventEndDateTime;
	}

	public Date getEventEndDateTime() {
		return eventEndDateTime;
	}

	public void setEventDateTimeString(String eventDateTimeString) {
		this.eventDateTimeString = eventDateTimeString;
	}

	public String getEventDateTimeString() {
		return eventDateTimeString;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLat() {
		return lat;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Double getLng() {
		return lng;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	public String getEventCategory() {
		return eventCategory;
	}

	public void setTimeAccurate(boolean timeAccurate) {
		this.timeAccurate = timeAccurate;
	}

	public boolean isTimeAccurate() {
		return timeAccurate;
	}
}
