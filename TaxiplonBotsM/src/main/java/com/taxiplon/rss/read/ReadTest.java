package com.taxiplon.rss.read;

import com.taxiplon.rss.model.Feed;
import com.taxiplon.rss.model.FeedMessage;
import com.taxiplon.rss.RSSFeedParser;


public class ReadTest {

	/*
   * Read more on http://www.vogella.de/articles/RSSFeed/article.html
   */

	public static void main(String[] args) {
		RSSFeedParser parser =
				  new RSSFeedParser("http://news.google.com/news", "pz=1&cf=all&ned=us&hl=en&output=rss");
		try {
			Feed feed = parser.readFeed();
			System.out.println(feed);
			for(FeedMessage message : feed.getMessages()) {
				System.out.println(message);
			}
		}
		catch (Exception e) {
			System.out.println("Failed to parse feed due to exception: " + e.getMessage());
		}
	}
}
