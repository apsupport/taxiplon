package com.taxiplon.rss.model;

import java.text.SimpleDateFormat;

import java.util.Date;

/*
 * Represents one RSS message
 */
public class FeedMessage {

	String title;
	String description;
	String link;
	String author;
	String guid;
	Date pubDate;
	String category;

	public FeedMessage(String title, String description, String link, String author, String guid,
					   Date pubDate, String category) {
		this.title = title;
		this.description = description;
		this.link = link;
		this.author = author;
		this.guid = guid;
		this.pubDate = pubDate;
		this.category = category;
	}

	public String toString() {
		return "FeedMessage: {title: " + title + ", description: " + description + ", link: " + link +
				  ", " + "author: " + author + ", guid: " + guid + ", pubDate: " + pubDate + ", category: " +
				  category + "}";
	}

	public String getFormattedPubDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return sdf.format(pubDate);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}

	public Date getPubDate() {
		return pubDate;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}
}
