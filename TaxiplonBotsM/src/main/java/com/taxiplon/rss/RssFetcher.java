package com.taxiplon.rss;

import com.taxiplon.rss.model.Feed;
import com.taxiplon.rss.model.FeedMessage;

import java.util.ArrayList;
import java.util.List;


public class RssFetcher {
	
	private String link;
	private String params;
	
	public RssFetcher(String link, String params) {
		super();
		this.link = link;
		this.params = params;
	}
	
	public List<FeedMessage> parseRSSandReturnList() {
		List<FeedMessage> newsList = new ArrayList<FeedMessage>();
		try {
		    RSSFeedParser parser = new RSSFeedParser(link, params);
		    Feed feed = parser.readFeed();
		    for (FeedMessage message : feed.getMessages()) {
		        newsList.add(message);
		    }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return newsList;
	}
}
