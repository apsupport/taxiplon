package com.taxiplon.rss;

//import com.sun.org.apache.xpath.internal.XPathAPI;

import com.taxiplon.Util;

import com.taxiplon.rss.model.Feed;
import com.taxiplon.rss.model.FeedMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.w3c.dom.Document;

import org.w3c.dom.Node;
import org.w3c.dom.traversal.NodeIterator;

import org.xml.sax.InputSource;


public class RSSFeedParser {
	
	static final String TITLE = "title";
	static final String DESCRIPTION = "description";
	static final String CHANNEL = "channel";
	static final String LANGUAGE = "language";
	static final String COPYRIGHT = "copyright";
	static final String LINK = "link";
	static final String AUTHOR = "author";
	static final String ITEM = "item";
	static final String BUILD_DATE = "lastBuildDate";
	static final String PUB_DATE = "pubDate";
	static final String CATEGORY = "category";
	static final String GUID = "guid";
	DateFormat incomingDateFormat;


	final String url;
	final String requestParams;

	public RSSFeedParser(String feedUrl) {
		this(feedUrl, null);
	}

	public RSSFeedParser(String feedUrl, String params) {
	    this.url = feedUrl;
	    this.requestParams = params;
		incomingDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", new Locale("en", "US"));
	}

	public Feed readFeed() throws Exception {
		Feed feed = null;
		// Set header values intial to the empty string
		String description = "";
		String title = "";
		String link = "";
		String language = "";
		String copyright = "";
		String author = "";
		String buildDateString = "";
		Date buildDate = null;
		String pubdateString = "";
		Date pubDate = null;
		String guid = "";
		String category = "";
		String rssContent = new Util().getContent(url, "UTF8");
//		rssContent = "<?xml version='1.0' encoding='UTF-8'?>" + rssContent;
		try {
			/*
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			// Parse the XML file and build the Document object in RAM
			Document doc = docBuilder.parse(new InputSource(new StringReader(rssContent.substring(1))));
			doc.getDocumentElement().normalize();
			//create a feed
			title = XPathAPI.eval(doc, "/rss/channel/" + TITLE).toString();
			link = XPathAPI.eval(doc, "/rss/channel/" + LINK).toString();
			language = XPathAPI.eval(doc, "/rss/channel/" + LANGUAGE).toString();
			copyright = XPathAPI.eval(doc, "/rss/channel/" + COPYRIGHT).toString();
			author = XPathAPI.eval(doc, "/rss/channel/" + AUTHOR).toString();
			buildDateString = XPathAPI.eval(doc, "/rss/channel/" + BUILD_DATE).toString();
			buildDate = incomingDateFormat.parse(buildDateString);
			guid = XPathAPI.eval(doc, "/rss/channel/" + GUID).toString();
			description = XPathAPI.eval(doc, "/rss/channel/" + DESCRIPTION).toString();
			feed = new Feed(title, link, description, language, copyright, pubDate);
			//populate the feed with messages
			NodeIterator items = XPathAPI.selectNodeIterator(doc, "/rss/channel/" + ITEM);
			Node item;
			while((item = items.nextNode()) != null) {
				title = XPathAPI.eval(item, TITLE).toString();
				description = XPathAPI.eval(item, DESCRIPTION).toString();
				link = XPathAPI.eval(item, LINK).toString();
				author = XPathAPI.eval(item, AUTHOR).toString();
				guid = XPathAPI.eval(item, GUID).toString();
				pubdateString = XPathAPI.eval(item, PUB_DATE).toString();
				// sample of date coming from Google News - Wed, 06 Oct 2010 02:21:19 GMT+00:00
				pubDate = incomingDateFormat.parse(pubdateString);
				category = XPathAPI.eval(item, CATEGORY).toString();
				FeedMessage msg =
								new FeedMessage(title, description, link, author, guid, pubDate, category);
				feed.getMessages().add(msg);
			}
			*/
		}
		catch(Exception e) {
			System.err.println("Failed to parse the RSS XML due to exception: " + e.getMessage());
		}
		return feed;
	}

//	public String getContent() throws IOException {
//		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
//		connection.addRequestProperty("User-Agent", "blah");
//		//      URLConnection serviceConnection = url.openConnection();
//		connection.setDoOutput(true);
//		//write request to the connection
//		OutputStreamWriter request = new OutputStreamWriter(connection.getOutputStream());
//		if(requestParams != null) {
//			request.write(requestParams);
//		}
//		request.flush();
//		//read returned output
//		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//		String inputLine;
//		String returnedContent = "";
//		//    BufferedWriter output = new BufferedWriter(new FileWriter("index.html"));
//		while((inputLine = in.readLine()) != null) {
//			returnedContent += inputLine;
//			//      output.write(inputLine + "\n", 0, inputLine.length() + 1);
//		}
//		in.close();
//		request.close();
//		//    output.close();
//		return returnedContent; //new String(returnedContent.getBytes(SERVICE_CHARSET), "UTF-8");
//	}
}
