package com.taxiplon.db;


import com.mysql.jdbc.Driver;
import com.taxiplon.Util;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;


/**
 * If there's a problem connecting to MySQL saying 'Host ... is not allowed to connect to this MySQL server',
 * execute on the server from command prompt
 * mysql> grant all privileges on *.* to 'taxi'@'10.200.0.99' identified by 'datama';
 */

public class DataPool {

	public static final String MAIN_PROPERTIES_FILENAME = "DB";

	private static DataPool instance;
	private String propertiesFile;
	private String dbDriver;
	private String dbUsername;
	private String dbPassword;
	//private OracleDataSource dataSource;
	private Connection conn;
	private Statement statement;
	private String dbConnectionURL;

	private void $init$() {
		this.dbDriver = "";
		this.dbUsername = "";
		this.dbPassword = "";
	}

	/**
	 * Default way to obtain the default instance. Nevertheless, the constructor is public, leaving creation of more than one instances of the datapool open, though not encouraged.
	 */
	public static synchronized DataPool getInstance(String ... propertiesFile) throws SQLException {
		if (instance == null) {
			instance = new DataPool(propertiesFile.length == 0? MAIN_PROPERTIES_FILENAME : propertiesFile[0]);
		}
		return instance;
	}
	
	private static final Logger LOGGER = Logger.getLogger(DataPool.class);
	
	public DataPool(String propertiesFile) throws SQLException {
	    $init$();
	    this.propertiesFile = propertiesFile;
	    DriverManager.registerDriver(new Driver()); //new OracleDriver());
	    URL location = DataPool.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
	    this.dbDriver = Util.getProperty("dbDriver", propertiesFile);
	    this.dbConnectionURL = "jdbc:mysql://" + this.dbDriver; //"jdbc:mysql:thin:@//" + this.dbDriver;
	    //this.dbConnectionURL = Util.getProperty("jdbcConnection", propertiesFile);
	}

	public Connection getConnection() throws SQLException {
		if ((this.conn == null) || (this.conn.isClosed())) {
			try {
				
				LOGGER.info("The connectionurl "+dbConnectionURL);
				
				this.conn = DriverManager.getConnection(dbConnectionURL, Util.loadPropertiesFromFile(propertiesFile));
			    conn.setAutoCommit(true);
				statement = conn.createStatement();
			}
			catch (Exception e) {
				System.err.println("Failed to connect to the DB due to exception: " + e.getMessage());
			}
		}
		return this.conn;
	}
	
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return getConnection().prepareStatement(sql);
	}
	
	public PreparedStatement prepareStatement(String sql, String[] args) throws Exception {
		return getConnection().prepareStatement(sql, args);
	}

	public void closeConnection() throws SQLException {
		if ((this.conn != null) && (!this.conn.isClosed())) {
			//this.conn.close();
		}
	}
	
	public int executeUpdate(String query) throws SQLException {
		if(statement == null || statement.isClosed()) {
			statement = getConnection().createStatement();
		}
		return statement.executeUpdate(query);
	}
	
	public ResultSet executeQuery(String query) throws SQLException {
	    if(statement == null || statement.isClosed()) {
	        statement = getConnection().createStatement();
	    }
	    return statement.executeQuery(query);
	}
}
