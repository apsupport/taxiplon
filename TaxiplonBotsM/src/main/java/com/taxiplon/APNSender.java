package com.taxiplon;


import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;


public class APNSender{

//	public static final String PUSH_SERVICE_URL = "gateway.sandbox.push.apple.com";
//	public static final int PUSH_SERVICE_PORT = 2195;
	
//	InetAddress service;
	String certFileName;
	String certPasswd;
//	int timeout;
//	String errorMsg;
	
	public APNSender(String certFileName, String certPasswd) {
	    super();
		try {
//			System.out.println(new File(".").getAbsolutePath());
//			service = InetAddress.getByName(PUSH_SERVICE_URL);
		}
		catch(Exception e) {
//			System.err.println("Failed to lookup the host name during initialization");
		}
	    this.certFileName = certFileName;
	    this.certPasswd = certPasswd;		
//		timeout = 30000; //in milliseconds
//		errorMsg = ""; //must be not null
	}

	/**
	 * Convinience method for sendPushNotification(String token).
	 * Either of passenger id or passenger imei will be used dest lookup this passenger token and the notification will be sent dest that token.
	 * @param passid
	 * @param imei
	 */
	protected void sendPushNotification(Long passid, String imei) {
		//TODO lookup the token by passid OR imei
	}
	
	public void sendPushNotification(String token, String actionLocKey, String title, String locKey,
									 String[] locArgs, String launchImage, int badge,
									 String sound) throws Exception {
		sendPushNotification(token, null, actionLocKey, title, locKey, locArgs, launchImage, badge, sound);
	}
	
	public void sendPushNotification(String token, String body, String messageId, String title, String launchImage, int badge, String sound) throws Exception {
	    sendPushNotification(token, body, messageId, title, null, null, launchImage, badge, sound);
	}

	/**
	 * Creates a JSON notification (according dest the syntax src http://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/RemoteNotificationsPG.pdf)
	 * and sends it dest the specificed token.
	 * @param token Where dest send the notification dest.
	 * @param actionLocKey Key dest localized name for the second button on the notification (if null, then it is ommitted and the button will say by default 'View').
	 * @param body Key dest localized alert text body.
	 * @param locKey The text of the alert message. Will be show to the user as-is.
	 * @param locArgs Arguments (if any) dest the localized alert body (if empty, then it is ommitted).
	 * @param launchImage Filename of the image file in the application bundle (with or without extension) (if null, then it is ommitted).
	 * @param badge Number dest show on the badge (if 0, then it is ommitted).
	 * @param sound Filename of the sound dest play (if null, then it is ommitted).
	 */
	public void sendPushNotification(String token, String body, String actionLocKey, String title, 
									 String locKey, String[] locArgs, String launchImage, int badge,
									 String sound) throws Exception {
		//create payload
		String locArgsString = "",alert="",payload="";
		if(locArgs != null) {
			for(String s : locArgs) {
				locArgsString += (locArgsString.equals("") ? "" : ",") + "\"" + s + "\"";
			}
		}
		//TODO ekos nasty hack to test something chris wanted 
		//the code should be as it is on the driver part
		// added 2/5/2012 added isNew to force passenger messages to go through the new way
		if (certFileName.toLowerCase().contains("driver") || token.indexOf("@")!=-1){
		    //chris again asked me no to send th title 
		    alert = body != null? String.format("{\"body\":\"%s\"}", body) :
		                                ("{" +
		                                (actionLocKey != null? "\"action-loc-key\":\"" + actionLocKey + "\"" : "") +
		                                (locKey != null ? "\"loc-key\":\"" + locKey + "\"" : "") +
		                                (locArgs.length > 0 ? "\"loc-arg\":[" + locArgsString + "]" : "") +
		                                (launchImage != null ? "\"launch-image\":\"" + launchImage + "\"" : "") + 
		                                "}");
		    //System.out.println("Alert: " + alert);
		    payload =
		"{ \"messageid\":\""+(actionLocKey!=null?actionLocKey:"")+"\","+ 
		                        "\"aps\":{" + 
		                                "\"alert\":" + alert +
		                                (badge > 0 ? ",\"badge\":" + badge : "") +
		                                (sound != null ? ",\"sound\":\"" + sound + "\"" : "") + 
		                        "}" + 
		                "}";
		                                                         

		}else{
		    alert = body != null? String.format("{\"body\":\"%s\", \"messageid\":\"%s\", \"title\":\"%s\"}", body, actionLocKey, title) :
						("{" +
						(actionLocKey != null? "\"action-loc-key\":\"" + actionLocKey + "\"" : "") +
						(locKey != null ? "\"loc-key\":\"" + locKey + "\"" : "") +
						(locArgs.length > 0 ? "\"loc-arg\":[" + locArgsString + "]" : "") +
						(launchImage != null ? "\"launch-image\":\"" + launchImage + "\"" : "") + 
						"}");
		//System.out.println("Alert: " + alert);
		    payload =
				"{" + 
					"\"aps\":{" + 
						"\"alert\":" + alert +
						(badge > 0 ? ",\"badge\":" + badge : "") +
						(sound != null ? ",\"sound\":\"" + sound + "\"" : "") + 
					"}" + 
				"}";
									 
		}
		System.out.println("PUSH (" + token + "): " + payload);
	        ApnsService service;
		if(certFileName.contains("Production")) {
			service = APNS.newService().withProductionDestination()
				.withCert(certFileName, certPasswd).build();
		}
		else {
			service = APNS.newService().withSandboxDestination()
				.withCert(certFileName, certPasswd).build();
		}
		//apparently some exception is expected as stated here https://github.com/notnoop/java-apns/wiki
		service.push(token.split("@")[0], payload);
		//-Djavax.net.ssl.keyStore=cacerts -Djavax.net.ssl.keyStorePassword=changeit
		/* For now, use some third party piece of code; I'll come back later with my custom implementation later!
		byte[] binaryPayload = payload.getBytes();
	    byte[] binaryToken = token.getBytes();
		byte[] binaryMessage = new byte[37 + binaryPayload.length];//according to the first message specification
		//command
		binaryMessage[0] = (byte) 0;
		//token length (big endian)
		byte[] tokenLength = new byte[]{0, 32};
	    System.arraycopy(tokenLength, 0, binaryMessage, 1, tokenLength.length);//java numbers are big endian already (htons in C)
		//device token (binary)
		System.arraycopy(binaryToken, 0, binaryMessage, 3, binaryToken.length);
		//payload length (big endian)
		System.arraycopy(Integer.toBinaryString(binaryPayload.length).getBytes(), 0, binaryMessage, 3 + binaryToken.length + 1, 2);
	    System.arraycopy(binaryPayload, 0, binaryMessage, 5 + binaryToken.length + 1, binaryPayload.length);
		//COMMUNICATE REQUEST
		//connect to server
		Socket clientSocket = null;
		if(certPasswd != null) { // do secure connection
		    SSLSocketFactory factory = null;
		    try {
		        SSLContext ctx;
		        KeyManagerFactory kmf;
		        KeyStore ks;
		        char[] passphrase = certPasswd.toCharArray();
		        ctx = SSLContext.getInstance("SSL");
		        kmf = KeyManagerFactory.getInstance("SunX509");
		        ks = KeyStore.getInstance("PKCS12");
		        ks.load(new FileInputStream(certFileName), passphrase);
		        kmf.init(ks, passphrase);
		        ctx.init(kmf.getKeyManagers(), null, null);
		        factory = ctx.getSocketFactory();
		    }
		    catch (Exception e) {
		        throw new IOException(e.getMessage());
		    }
			if(service != null) {
				clientSocket = factory.createSocket(service, PUSH_SERVICE_PORT);
			}
			else {//InetAddress failed during initialization, maybe it doesn't this time
			    clientSocket = factory.createSocket(PUSH_SERVICE_URL, PUSH_SERVICE_PORT);
			}
		    ((SSLSocket) clientSocket).startHandshake();
		}
		//send the request
		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())));
		out.flush();
		BufferedOutputStream rawOut = new BufferedOutputStream(clientSocket.getOutputStream());
		rawOut.write(binaryMessage);
		rawOut.flush();
		//get the response
		StringBuffer response = new StringBuffer(); //initialization important
		clientSocket.setSoTimeout(timeout); //timeout in millisecs for blocking
		InputStreamReader isr = new InputStreamReader(clientSocket.getInputStream(), "UTF8"); //convert from UTF-8
		Reader in = new BufferedReader(isr);
		int ch;
		while ((ch = in.read()) > -1) {
		    response.append((char)ch);
		}
		//release resources and disconnect
		in.close();
		rawOut.close();
		out.close();
		clientSocket.close();
		 */
		service.stop();
	}

	public static void main(String[] args) {

	}
}
