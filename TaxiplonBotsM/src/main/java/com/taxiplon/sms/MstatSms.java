package com.taxiplon.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.HttpResponse;

import java.net.URLEncoder;

import java.util.List;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;

public class MstatSms {

    public final String userName;
    public final String passWord;

    public MstatSms(String userName,String passWord) {
    
        this.userName = userName;
        this.passWord = passWord;
    }
    
    public void sendSms(long msgId,String from,String to,String message) {
        
        try {
            
        HttpClient client = new DefaultHttpClient();
        String bulkrString;

            bulkrString =
                    "http://cpanel.bulker.gr/api/aploon/sms.php?id="+String.valueOf(msgId)+
            "&uname="+userName+
            "&pass="+passWord+
            "&oa="+from+
            "&ta="+to+
            "&sms="+URLEncoder.encode(message,"UTF-8");
        HttpGet request = new HttpGet(bulkrString);
        
        try {
            HttpResponse response = client.execute(request);
            System.out.println("\nSending 'GET' request to URL : " + bulkrString);
                        System.out.println("Response Code : " + 
                               response.getStatusLine().getStatusCode());
            
            BufferedReader bfReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder responseBuilder = new StringBuilder();
            String line = "";

            while((line=bfReader.readLine())!=null) {
                responseBuilder.append(line);
            }

            System.out.println(responseBuilder.toString());
        
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }catch (UnsupportedEncodingException e) {
        }
         
        
        
        /*
        HttpPost httpPost = new HttpPost("http://cpanel.bulker.gr/api/aploon/sms.php");

        System.out.println("The oa is "+from);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair("id",String.valueOf(msgId)));
        nameValuePairs.add(new BasicNameValuePair("uname",userName));
        nameValuePairs.add(new BasicNameValuePair("pass",passWord));
        nameValuePairs.add(new BasicNameValuePair("oa",from));
        nameValuePairs.add(new BasicNameValuePair("ta",to));
        nameValuePairs.add(new BasicNameValuePair("sms",message));

        try {

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse httpResponse = client.execute(httpPost);
            BufferedReader bfReader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            StringBuilder responseBuilder = new StringBuilder();
            String line = "";

            while((line=bfReader.readLine())!=null) {
                responseBuilder.append(line);
            }

            System.out.println(responseBuilder.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

    }

}