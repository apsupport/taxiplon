package com.taxiplon.sms;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;


public class GRSms extends BulkSMS{
    //Attention this provider uses https and needs the certificate inserted in cacerts 
    //to work
    public GRSms() {
	super();
    }
    
    public GRSms(String user, String Pass) {
	super(user,Pass);
    }
    final static String HTTP_BASIC_REQ = "https://bulksmsn.gr/api/http/send.php?";
    final static String HTTP_QUERY = "username=%s&password=%s&from=%s&to=%s&message=%s";
    
    public void sendSMS(final String mobileNumber, final String smsText, final String sender) {
	Thread thr = new Thread() {

	    public void run() {
		GetMethod method = null;
		String acc = "";
		BufferedReader br = null;
		try {
		    HttpClient client = new HttpClient();
		    method = new GetMethod(HTTP_BASIC_REQ);
		    int returnCode = -1;
		    
	    method.setQueryString(URIUtil.encodeQuery(String.format(HTTP_QUERY, user, pass, getSender()==null?sender:getSender(), mobileNumber,
									    smsText)));
		    returnCode = client.executeMethod(method);
		    if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
			// still consume the response body
			method.getResponseBodyAsString();
			throw new SMSException("The Get method is not implemented by this URI");
		    } else {
			br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
			String readLine = null;
			while (((readLine = br.readLine()) != null)) {
			    acc += readLine;
			}
		    }
		    System.out.println(String.format(REPORT, mobileNumber, returnCode, acc, smsText));
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    if (method != null)
			method.releaseConnection();
		    if (br != null) {
			try {
			    br.close();
			} catch (Exception fe) {
			    fe.printStackTrace();
			}
		    }
		}
	    }
	};
	thr.start();
    }
    
    public String getSender(){
	return null;
    }

}
