package com.taxiplon.sms.model;

public class SMSSettings {

    public enum GateWay {
        bulker,bulksmsn,directcall,smscentral,voxility
    };

    private String userName;
    private String passWord;
    private GateWay gateWay;
    private String fromName;
    
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setGateWay(String gatewayString) {
        
        gateWay = SMSSettings.GateWay.valueOf(gatewayString);    
    }

    public void setGateWay(SMSSettings.GateWay gateWay) {
        this.gateWay = gateWay;
    }

    public SMSSettings.GateWay getGateWay() {
        return gateWay;
    }
    
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromName() {
        return fromName;
    }
    
}
