package com.taxiplon.sms;


public abstract class BulkSMS {

    String user;
    String pass;
    final static String REPORT =
	"SMS : Send SMS to %s the return code is : %d the request answer is : %s  message was : %s";

    public BulkSMS() {

    }

    public BulkSMS(String usr, String passwd) {
	user = usr;
	pass = passwd;
    }

    public void setUser(String usr) {
	user = usr;
    }

    public void setPassword(String pwd) {
	pass = pwd;
    }


    public abstract String getSender();

    public abstract void sendSMS(final String mobileNumber, final String smsText, final String sender);

    protected class SMSException extends Exception {
	public SMSException(String sMessage) {
	    super(sMessage);
	}
    }


}
