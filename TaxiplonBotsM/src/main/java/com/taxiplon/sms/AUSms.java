package com.taxiplon.sms;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;


public class AUSms extends BulkSMS {
    private static final String API_URL = "https://my.smscentral.com.au/wrapper/sms";

    public AUSms(String usr, String pass) {
	super(usr, pass);
    }

    public AUSms() {
	super();
    }

    @Override
    public String getSender() {
	return null;
    }

    @Override
    public void sendSMS(final String mobileNumber, final String smsText, final String sender) {
	Thread thr = new Thread() {
	    public void run() {
		try {
		    HttpClient client = new HttpClient();
		   
		    BufferedReader br = null;
		    PostMethod method = new PostMethod(API_URL);
		    method.addParameter("USERNAME", user);
		    method.addParameter("PASSWORD", pass);
		    method.addParameter("ACTION", "send");
		    method.addParameter("ORIGINATOR", sender);
		    method.addParameter("MESSAGE_TEXT",smsText);
		    method.addParameter("RECIPIENT", mobileNumber.substring(2));
		    
		    String acc = "";
		    try {
			int returnCode = client.executeMethod(method);

			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
			    System.err.println("The Post method is not implemented by this URI");
			    // still consume the response body
			    method.getResponseBodyAsString();
			} else {
			    br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
			    String readLine;
			    while (((readLine = br.readLine()) != null)) {
				acc += (readLine);
			    }
			    String rep = String.format(REPORT, mobileNumber, returnCode, acc, smsText);
			    System.out.println(rep);

			}
		    } catch (Exception e) {
			System.err.println(e);
		    } finally {
			method.releaseConnection();
			if (br != null)
			    try {
				br.close();
			    } catch (Exception fe) {
			    }
		    }


		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	};
	thr.start();

    }

    public static void main(String[] args) {
	BulkSMS op = new AUSms("rollins.matthew@taxilink.com.au", "Trustno1");

	op.sendSMS("00306972531344", "test", "taxiplon");
    }
}
