package com.taxiplon.sms;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;


public class ROSms extends BulkSMS {
    final static String HTTP_BASIC_VOX_REQ = "http://web.smsc.voxility.com/smssend.html?";
    final static String HTTP_VOX_QUERY = "sid=69&u=%s&p=%s&from=%s&to=40%s&message=%s";
    public final static String SMS_COUNTRY = "RO";
    public ROSms() {
	super();
    }

    public ROSms(String user, String pass) {
	super(user, pass);
    }

    public void sendSMS(final String mobileNumber, final String smsText, final String sender) {

	Thread thr = new Thread() {
	    public void run() {
		HttpClient client = new HttpClient();
		String acc = "";
		BufferedReader br = null;
		String fixedMobileNumber = mobileNumber.startsWith("0") ? mobileNumber.substring(1) : mobileNumber;
		String query =
  String.format(HTTP_VOX_QUERY, user, pass, getSender() == null ? sender : getSender(), fixedMobileNumber, smsText);
		GetMethod method = new GetMethod(HTTP_BASIC_VOX_REQ);
		try {
		    method.setQueryString(URIUtil.encodeQuery(query));
		} catch (URIException e1) {
		    e1.printStackTrace();
		}
		try {
		    int returnCode = client.executeMethod(method);

		    if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
			method.getResponseBodyAsString();
			throw new SMSException("The Get method is not implemented by this URI");
		    } else {
			br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
			String readLine;
			while (((readLine = br.readLine()) != null)) {
			    acc += readLine;
			}
		    }
		    String rep = String.format(REPORT, mobileNumber, returnCode, acc, smsText);
		    System.out.println(rep);

		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    if (method != null)
			method.releaseConnection();
		    if (br != null)
			try {
			    br.close();
			} catch (Exception fe) {
			    fe.printStackTrace();
			}
		}
	    }
	};
	thr.start();
    }

    public String getSender() {
	return null;
    }
}
