package com.taxiplon.sms;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;


public class BRSms extends BulkSMS {
    final static String HTTP_BASIC_DIRCALL_REQ = "https://www.directcallsoft.com/sms/enviar.php?";
    final static String HTTP_DIRCALL_QUERY = "api=%s&pin=%s&origem=%s&destino=%s&texto=%s&formato=txt";

    public BRSms(String string, String string1) {
	super(string, string1);
    }

    public BRSms() {
	super();
    }

    @Override
    public String getSender() {
	return "557932271716";
    }

    @Override
    public void sendSMS(final String mobileNumber, final String smsText, final String sender) {
	Thread thr = new Thread() {
	    public void run() {
		HttpClient client = new HttpClient();
		String acc = "";
		BufferedReader br = null;

		String query =
  String.format(HTTP_DIRCALL_QUERY, user, pass, getSender() == null ? sender : getSender(), mobileNumber, smsText);
		GetMethod method = new GetMethod(HTTP_BASIC_DIRCALL_REQ);
		try {
		    method.setQueryString(URIUtil.encodeQuery(query));
		} catch (URIException e1) {
		    e1.printStackTrace();
		}
		try {
		    int returnCode = client.executeMethod(method);

		    if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
			method.getResponseBodyAsString();
			throw new SMSException("The Get method is not implemented by this URI");
		    } else {
			br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
			String readLine;
			while (((readLine = br.readLine()) != null)) {
			    acc += readLine;
			}
		    }
		    String rep = String.format(REPORT, mobileNumber, returnCode, acc, smsText);
		    System.out.println(rep);

		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    if (method != null)
			method.releaseConnection();
		    if (br != null)
			try {
			    br.close();
			} catch (Exception fe) {
			    fe.printStackTrace();
			}
		}
	    }
	};
	thr.start();
    }
    
    public static void main(String[] args){
	BRSms br=new BRSms();
	br.setUser("4992987082412160");
	br.setPassword("PCP63ZL29R1664329027470720");
	br.sendSMS("00557991292622", "test for taxiplon from brazil", "taxiplon");
    }
}
