package com.taxiplon.sms;


import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;

import java.util.HashMap;
import java.util.Map;


public class USSms extends BulkSMS {
    public USSms() {
	super();
    }


    public USSms(String user, String pass) {
	super(user, pass);
    }

    public void sendSMS(final String mobileNumber, final String smsText, final String sender) {

	Thread thr = new Thread() {
	    public void run() {
		try {
		    TwilioRestClient client = new TwilioRestClient(user, pass);
		    Account mainAccount = client.getAccount();

		    SmsFactory smsFactory = mainAccount.getSmsFactory();
		    Map<String, String> smsParams = new HashMap<String, String>();
		    smsParams.put("To", "+"+mobileNumber.substring(2)); // Replace with a valid phone number
		    smsParams.put("From", getSender() == null ? sender : getSender()); // Replace with a valid phone
		    // number in your account
		    smsParams.put("Body", smsText);
		    smsFactory.create(smsParams);

		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	};
	thr.start();
    }

    public String getSender() {
	return "+16463500655";
    }

    public static void main(String[] args) {
	BulkSMS op = new USSms("AC5f1e4a5b71598110ae0e2a959964c702", "3f3f528d9569b3aad641e9f754576d4b");
	op.sendSMS("00306972531344", "test", null);
    }
}
