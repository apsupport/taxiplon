package com.taxiplon;

import de.agitos.dkim.DKIMSigner;

import de.agitos.dkim.SMTPDKIMMessage;

import java.io.ByteArrayInputStream;

import java.io.File;

import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import sun.net.smtp.SmtpClient;


public class MailClient {
	
	String signingDomain, selector, privateKeyPath;
	
	public MailClient(String signingDomain, String selector, String privateKeyPath) {
	    this.signingDomain = signingDomain;
		this.selector = selector;
		this.privateKeyPath = privateKeyPath;
	}

	public void sendMail(final String mailServer, String senderEmail, String senderPass, String to,
						 String subject, String messageBody) throws Exception {
		//sign the message with the dkim
		DKIMSigner dkimSigner =
				  new DKIMSigner(signingDomain, selector, privateKeyPath);
		/* 
		 * set an address or user-id of the user on behalf this message was signed;
		 * this identity is up to you, except the domain part must be the signing domain
		 * or a subdomain of the signing domain.
		 */
		dkimSigner.setIdentity("info@" + signingDomain);
		// Setup mail server
		Properties props = System.getProperties();
		props.put("mail.smtp.host", mailServer);
		props.put("mail.smtp.auth", "true");
//		Authenticator auth = new JAuthenticate(senderEmail, senderPass);
		// Get a mail session
		Session session = Session.getDefaultInstance(props, null);//auth);
		// Define a new mail message
		Message message = new SMTPDKIMMessage(session, dkimSigner) {
			/**
			 * Implementation according to the documentation (http://www.oracle.com/technetwork/java/faq-135477.html#msgid)
			 * to override the Message-ID header.
			 */
			protected void updateMessageID() throws MessagingException {
				String msgidHeader = "Message-ID";
				setHeader(msgidHeader, "<" + System.currentTimeMillis() + "@" + mailServer + ">");
			}
		};
		message.setFrom(new InternetAddress(senderEmail));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject(subject); // Create a message part to represent the body text
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText("<body style='font-family: arial, sans serif;'>" + messageBody + "</body>");
		message.setContent(messageBody, "text/html;charset=\"UTF-8\"");
		message.setReplyTo(new InternetAddress[] { new InternetAddress(senderEmail) });
		message.setSentDate(new Date());
		message.setHeader("Importance", "Normal");
		message.setHeader("X-Mailer", "registration");
		// Send the message
//		Transport.send(message);
		Transport transport = session.getTransport("smtp");
		transport.connect(mailServer, senderEmail, senderPass);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}

	public static void main(String[] args) {
		if(args.length < 4) {
			args =
		new String[] { "mail.taxiplon.com", "info@taxiplon.com", "12345678", "check-auth@verifier.port25.com" };
		}
		try {
			System.out.println(new File(".").getAbsolutePath());
			new MailClient("taxiplon.com", "selector", "private.key.der").
				sendMail(args[0], args[1], args[2], args[3],
									  "Please complete your taxiplon registration",
									  String.format("<html><h2>Dear customer,<br/><br/>you are almost there!</h2>" +
													"<p style=\"font-size\\:20; color\\:black;\">Follow <a href=\"%1$s\" target=_BLANK>this link</a> to complete your taxiplon registration.</p>" +
													"<p style=\"font-style\\:italic; font-size\\:-1; color\\:grey;\">You received this message because your email address was given during the registration process of the taxiplon app. If it wasn't given by you please ignore it</p>" +
													"%2$s", "google.com",
													"<p style=\"font-style\\:italic; font-size\\:-1; color\\:grey;\"><br/><br/>--" +
													"<br/>Taxiplon Team" +
													"<br/>Click a Taxi</p></html>"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	class JAuthenticate extends Authenticator {
		String sUsername = null;
		String sPassword = null;

		public JAuthenticate(String username, String password) {
			// Assign username and password values passed in data from calling mail connection
			sUsername = username;
			sPassword = password;
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			// username/password get authenticated next line
			return new PasswordAuthentication(sUsername, sPassword);
		}
	}

}
