package com.taxiplon;

import java.io.File;

public class TileOrganizer {
	
	String srcPath;
	String destPath;
	
	public TileOrganizer(String srcPath, String destPath) {
		super();
		this.srcPath = srcPath;
	    this.destPath = destPath;
	}
	
	public void renameFiles() throws Exception {
		File srcDir = new File(srcPath);
	    File destDir = new File(destPath);
		if(!srcDir.exists() || !srcDir.isDirectory() || !destDir.exists() || !destDir.isDirectory()) {
			throw new RuntimeException("Either source or destination directories don't exist or are not directories");
		}
		//the export created by the terra man has row/col order, whilst the online ESRI servers have the col/row order
		for(String level : srcDir.list()) {
			File levelDir = new File(srcPath + "/" + level);
			for(String row : levelDir.list()) {
			    File rowDir = new File(srcPath + "/" + level + "/" + row);
				for(String column : rowDir.list()) {
				    File colFile = new File(srcPath + "/" + level + "/" + row + "/" + column);
					File outputDir = new File(destPath 
											  + "/" + Integer.parseInt(level.substring(1), 16)
											  + "/" + Integer.parseInt(column.substring(1).replaceAll(".png", ""), 16));
					outputDir.mkdirs();
					colFile.renameTo(new File(outputDir.getAbsolutePath()
											  + "/" + Integer.parseInt(row.substring(1), 16)
											  ));
				}
			}
		}
	}
	
	public static void main(String args[]) {
		try {
			TileOrganizer to = new TileOrganizer("F:\\Documents\\Terra\\RoadMaps_roadmaps_190111webmercatorTerraScales\\Layers\\_alllayers",
												 "F:\\Documents\\Terra\\RoadMaps_roadmaps_190111webmercatorTerraScales\\Layers\\row-col");
			to.renameFiles();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
				
}
