package com.taxiplon;

import gnu.crypto.cipher.Twofish;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;


public class Util {
	
	private static MessageDigest hashDigester;
	private String sessionCharset;
	
	public static String digest(String s) throws Exception {
		if(hashDigester == null) {
		    hashDigester = MessageDigest.getInstance("SHA-1");
		}
		/*
		 * password should be 40 characters
		 * check out http://stackoverflow.com/questions/2860943/suggestions-for-library-to-hash-passwords-in-java/2861125#2861125
		 * and implement for absolute secutiy
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		KeySpec spec = new PBEKeySpec("password".toCharArray(), salt, 2048, 160);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = f.generateSecret(spec).getEncoded();
		System.out.println("salt: " + new BigInteger(1, salt).toString(16));
		System.out.println("hash: " + new BigInteger(1, hash).toString(16));
		 */
	    return new BigInteger(1, hashDigester.digest(s.getBytes())).toString(16);
	}
	
	public String getContent(String serviceUrl, String serviceCharset) throws Exception {
		return getContent(serviceUrl, null, serviceCharset, true);
	}

	/**
	 * Fetch the content of service at the given URL with provided parameters.
	 * Also transcode the input into UTF-8 according to specified charset used by the service.
	 * @param serviceUrl
	 * @param serviceCharset
	 * @return
	 * @throws Exception
	 */
	public String getContent(String serviceUrl, String params, String serviceCharset, boolean allowSessionCharsetChange) throws Exception {
		HttpURLConnection serviceConnection = (HttpURLConnection) new URL(serviceUrl + (params == null? "" : params)).openConnection();
		serviceConnection.setDoOutput(true);
		serviceConnection.addRequestProperty("User-Agent", "Mozilla/9.876 (X11; U; Linux 2.2.12-20 i686, en; rv:2.0) Gecko/25250101 Netscape/5.432b1 (C-MindSpring)"); //google denies access to some pages when User-Agent is Java
		//read returned output
		BufferedReader in = (serviceCharset != null) ? new BufferedReader(new InputStreamReader(serviceConnection.getInputStream(),
																					  serviceCharset)) :
				  new BufferedReader(new InputStreamReader(serviceConnection.getInputStream()));
	    String inputLine;
		String returnedContent = "";
	    Pattern charsetPattern = Pattern.compile("<meta.*content=\"text/html;.*charset=([A-Za-z0-9\\-]*)\"");
	    while ((inputLine = in.readLine()) != null) {
	        if (serviceCharset == null) {
	            //if charset is not predefined, then find out and format all contents using this charset
	            Matcher m = charsetPattern.matcher(inputLine);
	            if (m.find()) {
	                serviceCharset = m.group(1);
	                if(allowSessionCharsetChange) {
	                    sessionCharset = serviceCharset;
	                }
	            }

	        }
			//fixed the case when charset might occur on line other than first
	        returnedContent += inputLine;//serviceCharset == null && sessionCharset == null? inputLine :  new String(inputLine.getBytes(), serviceCharset != null? serviceCharset : sessionCharset);
	    }
	    in.close();
		return returnedContent;
	}

	public static String getProperty(String key, String fileName) {
//		System.out.println("key: " + key + ", from file: " + fileName);
		String result = "";
		try {
			Properties props = loadPropertiesFromFile(fileName);
			if (props == null) {
				try {
					props = loadPropertiesFromFile(fileName);
				}
				catch (Exception e) {
					System.err.println("Failed to fetch property for key " + key + " from the file " + fileName +
									   " due to exception: " + e.getMessage());
				}
			}
			result = props.getProperty(key);
			if ((result == null) && (key != null)) {
				result = props.getProperty(key.toLowerCase());
			}
		}
		catch (Exception e) {
			System.err.println("Failed to load the properties file " + fileName + " due to exception: " +
							   e.getMessage());
		}
		return result;
	}

	public static void setProperty(String propertyName, String value, String fileName) throws Exception {
		Properties requestedPropertiesSet = loadPropertiesFromFile(fileName);
		requestedPropertiesSet.setProperty(propertyName, value);
		OutputStream os = new FileOutputStream(fileName + ".properties");
		requestedPropertiesSet.store(os, null);
	}

	public static Properties loadPropertiesFromFile(String fileName) throws Exception {
		//second attempt to figure out where is the properties file (inside the JAR or the WAR file)
		Properties prop = new Properties();
		fileName += ".properties";
//                System.out.println(new File(".").getAbsolutePath() + ", " + Util.class.getClassLoader().getResource(fileName).toString());
	    try {
			InputStream is = Util.class.getClassLoader().getResourceAsStream(fileName);
			if(is == null) {
				File propertiesFile = new File(fileName);
				if(propertiesFile.exists()) {
					is = new FileInputStream(propertiesFile);
				}
				else {
				    //if all failed, then create the properties file, it'll be needed in the future, and will indicate that something went wrong
				    propertiesFile.createNewFile();
				}
			}
			prop.load(is);
		}
		catch (Exception e) {
			prop = new Properties();
		}
		return prop;
	}
	
	/**
	 * As optional fields can be left empty during a request, this method provides the framework with a generic way of obtaining a typed version of that request param.
	 * @param <T>
	 * @param value Value of the optional parameter.
	 * @param type Class to type the output to (Number or a custom enum).
	 * @return Typed value of the param, or null if the param value was not provided by the request.
	 */
	public static <T> T parseOptionalField(String value, Class<T> type) {
	    T param = null;
		if(value != null) {
		    try {
				Constructor init = type.getConstructor(String.class);
				param = (T) init.newInstance(value.replace(',', '.'));//this mostly handles the case when a float comes with a comma
			}
		    catch(Exception ie) {
		        ie.printStackTrace();
		    }
		}
		return param;
	}
	
	private static final String ALGORITHM = "AES";
	
	private static final byte[] keyValue = 
	    new byte[] { 'i', 'l', '0', 'v', 'e', 'T', '@', 'x', '1', 'p', 'l', '0', 'n' ,'K','e','y'};

	public static String encrypt(String valueToEnc,byte[] keyBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException  {
		
		byte[] plainText;
		byte[] encryptedText;
		
		Twofish twofish = new Twofish();
		Object keyObject = twofish.makeKey(keyBytes, 16);
		
		if((valueToEnc.length()%16)!=0) {
			while(valueToEnc.length()%16!=0) {
				valueToEnc +=" ";
			}
		}
		
		plainText = valueToEnc.getBytes("UTF8");
		encryptedText = new byte[valueToEnc.length()];
		
		for(int i=0;i<Array.getLength(plainText);i+=16) {
			twofish.encrypt(plainText, i, encryptedText, i, keyObject, 16);
		}
		
		String encryptedString = new String(Base64.encodeBase64(encryptedText));
		return encryptedString;
	}
	
	public static String decrypt(String encryptedValue,byte[] keyBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		
		byte[] encryptedText;
		byte[] decryptedText;
		
		Twofish twofish = new Twofish();
		
		Object keyObject = twofish.makeKey(keyBytes, 16);
		
		if((encryptedValue.length()%16)!=0) {
			while((encryptedValue.length()%16)!=0) {
				encryptedValue +=" ";
			}
		}
		
		encryptedText = Base64.decodeBase64(encryptedValue.getBytes());
		decryptedText = new byte[encryptedValue.length()];
		
		for(int i=0;i<Array.getLength(encryptedText);i+=16) {
			twofish.decrypt(encryptedText, i, decryptedText, i, keyObject, 16);
		}
		
		String decryptedString = new String(decryptedText, "UTF8");
		return decryptedString;

	}
	
	public static String encrypt(String valueToEnc) throws Exception {
		Key key = generateKey();	
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encValue = c.doFinal(valueToEnc.getBytes());
		String encryptedValue = new String(Base64.encodeBase64(encValue));
		//String encryptedValue = new BASE64Encoder().encode(encValue);
	    
		return encryptedValue;
	}

	public static String decrypt(String encryptedValue) throws Exception {
	    Key key = generateKey();
	    Cipher c = Cipher.getInstance(ALGORITHM);
	    c.init(Cipher.DECRYPT_MODE, key);
	    //byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
	    byte[] decordedValue = Base64.decodeBase64(encryptedValue.getBytes());
	    
	    byte[] decValue = c.doFinal(decordedValue);
	    String decryptedValue = new String(decValue);
	    return decryptedValue;
	}

	private static Key generateKey() throws Exception {
	    Key key = new SecretKeySpec(keyValue, ALGORITHM);
	    // SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
	    // key = keyFactory.generateSecret(new DESKeySpec(keyValue));
	    return key;
	}

	private static Key generateKey(byte[] keyBytes) {
		Key key = new SecretKeySpec(keyBytes, ALGORITHM);
		return key;
	}
	
	/*
	public static Double getDistance(Double latFrom,Double longFrom,Double latTo,Double longTo) {
		
		Double x = (longFrom-longTo)*Math.cos((latFrom+latTo)/2);
		Double y = (latFrom-latTo);
		
		return Math.sqrt(x*x+y*y)*3.14;
	}
	*/
	
	public static double getDistance(double fromLat, double fromLng, double toLat, double toLng) {
		
		///return 6371*2*Math.atan2(Math.sqrt(Math.pow(Math.sin(Math.toRadians(toLat - fromLat)/2), 2) + Math.pow(Math.sin(Math.toRadians(toLng - fromLng)/2), 2) * Math.cos(Math.toRadians(fromLat)) * Math.cos(Math.toRadians(toLat))), Math.sqrt(1 - Math.pow(Math.sin(Math.toRadians(toLat - fromLat)/2), 2) + Math.pow(Math.sin(Math.toRadians(toLng - fromLng)/2), 2) * Math.cos(Math.toRadians(fromLat)) * Math.cos(Math.toRadians(toLat))));
		return 6371*2*Math.atan2(Math.sqrt(Math.pow(Math.sin(Math.toRadians(toLat - fromLat)/2), 2) + Math.pow(Math.sin(Math.toRadians(toLng - fromLng)/2), 2) * Math.cos(Math.toRadians(fromLat)) * Math.cos(Math.toRadians(toLat))), Math.sqrt(1 - Math.pow(Math.sin(Math.toRadians(toLat - fromLat)/2), 2) + Math.pow(Math.sin(Math.toRadians(toLng - fromLng)/2), 2) * Math.cos(Math.toRadians(fromLat)) * Math.cos(Math.toRadians(toLat))));
	}
	
}
