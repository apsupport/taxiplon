package aploon;


import aploon.model.Coordinates;

import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;

import com.google.gson.Gson;

import com.taxiplon.Util;

import java.net.URLEncoder;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The interface for geocoding and reverse geocoding. Utilizes the services provided by terra.
 */
public class TerraLocator 	{
	
	// documentation can be found at http://mapsrv6.terra.gr/avl/WebService.asmx
	public static final String TERRA_REVERSE_GEOCODING_SERVICE_URL = "http://mapsrv6.terra.gr/avl/WebService.asmx/getReverseAddressJSON?";
	public static final String TERRA_REVERSE_GEOCODING_SERVICE_PARAMS = "countryCode=30&srid=4326&x=%s&y=%s";// x is the longitude and y is the latitude
	public static final String TERRA_GEOCODING_SERVICE_URL =  "http://mapsrv6.terra.gr/avl/WebService.asmx/getAddressJSON?";
	public static final String TERRA_GEOCODING_SERVICE_PARAMS = "countryCode=30&input=%s&srid=4326";
	//4326 is the WGS84 spatial reference (used by the GPS)
	public static final String SERVICE_CHARSET = "UTF-8";
	private Pattern wordFollowedByDotPattern;
	private Pattern numberMinusNumberPattern;
	private Pattern streetCornerPattern;
	private Util util;

	public TerraLocator() {
		super();
		wordFollowedByDotPattern = Pattern.compile(".*\\. ?(.+)");
		numberMinusNumberPattern = Pattern.compile("(.*[0-9]+)-[0-9]+(.*)");
		streetCornerPattern = Pattern.compile("(.*\\d+ *&|&.*\\d+)"); //this will have an exception in the matching that the split symbol will also create a group
		util = new Util();
	}
	
	final SpatialReference SPATIAL_REFERENCE_TAXIPLON = SpatialReference.create(3857);
	final SpatialReference SPATIAL_REFERENCE_GPS = SpatialReference.create(4326);
	final SpatialReference SPATIAL_REFERENCE_2100 = SpatialReference.create(2100);
	
	public Point projectPointFrom2100ToGPS(float lat, float lng) {
	    return (Point) GeometryEngine.project(new Point(lng, lat), SPATIAL_REFERENCE_2100, SPATIAL_REFERENCE_GPS);
	}
	
	public Point projectPointFromGPSTo2100(float lat, float lng) {
	    return (Point) GeometryEngine.project(new Point(lng, lat), SPATIAL_REFERENCE_GPS, SPATIAL_REFERENCE_2100);
	}

	/**
	 * As Google Maps API requires the address dest look up dest be in specific form,
	 * this method normalizes misformed addresses, that will clearly return no results.
	 * @param addr
	 * @return Same address stripped of misformed parts that prevent Google Maps API src locating it.
	 */
	public String normalizeAddress(String addr) {
		addr = addr.toUpperCase();
		// escape commas
		addr = addr.replaceAll(",", "");
		// Eliminate corners (e.g. yakinthou & kapodistriou 54 or ). The number can follow the first or the second street name.
		addr = addr.replaceAll("\u039A\u0391\u0399", "&");
		Matcher m = streetCornerPattern.matcher(addr);
		if (m.find()) {
			addr = m.group(1);
			addr = addr.replace('&', ' ');
		}
//		// special case is saint (e.g. ag. georgiou should not be treated as georgiou)
//		addr = addr.replace("\u0391\u0393\\.?", "\u0391\u0393\u0399\u039F\u03A5");
//		// Strip off the parts followed by dots (e.g. el.venizelou, v.sofias)
//		m = wordFollowedByDotPattern.matcher(addr);
//		if (m.find()) {
//			addr = m.group(1);
//		}
//		// Keep just one number of the building (e.g. kifisias 221-223)
//		m = numberMinusNumberPattern.matcher(addr);
//		if (m.find()) {
//			addr = m.group(1) + m.group(2);
//		}
		// special treatment for 'kon/nou'
		addr =
	 addr.replace("\u039A\u03A9\u039D/\u039D\u039F\u03A5", "\u039A\u03A9\u039D\u03A3\u03A4\u0391\u039D\u03A4\u0399\u039D\u039F\u03A5");
		// special treatment for 'kon/pol' (matches both kon/polis and kon/poleos)
		addr =
	 addr.replace("\u039A\u03A9\u039D/\u03A0\u039F\u039B", "\u039A\u03A9\u039D\u03A3\u03A4\u0391\u039D\u03A4\u0399\u039D\u039F\u03A5\u03A0\u039F\u039B");
		// special treatment for 'thes/niki'
		addr =
	 addr.replace("\u0398\u0395\u03A3/\u039D\u0399\u039A\u0397", "\u0398\u0395\u03A3\u03A3\u0391\u039B\u039F\u039D\u0399\u039A\u0397");
		// remove useless spaces
		addr = addr.trim();
		return addr;
	}
	
	public Coordinates getCoordinatesByAddress(String address) throws Exception {
		String params = String.format(TERRA_GEOCODING_SERVICE_PARAMS, URLEncoder.encode(normalizeAddress(address), SERVICE_CHARSET));
		Address a = parseAddressesFromJSON(util.getContent(TERRA_GEOCODING_SERVICE_URL, params, SERVICE_CHARSET, false));
		AddPoint p = a.addPoint;
		return new Coordinates(p.y, p.x, a.customFormattedAddress);//gr is the only country code terra works with
	}
	
	public String getAddressByCoordinates(Coordinates coord) throws Exception {
		String address = getAddressByCoordinates(coord.getLat(), coord.getLng());
		coord.setNormalizedAddress(address);
		return address;
	}
	
	public String getAddressByCoordinates(Double lat, Double lng) throws Exception {
		String params = String.format(TERRA_REVERSE_GEOCODING_SERVICE_PARAMS, Double.toString(lng).replace(',', '.'), Double.toString(lat).replace(',', '.'));
		return parseAddressesFromJSON(util.getContent(TERRA_REVERSE_GEOCODING_SERVICE_URL, params, SERVICE_CHARSET, false)).customFormattedAddress;
	}
	
	protected Address parseAddressesFromJSON(String content) throws Exception {
	    content = content.replaceAll("<.*?>\\[", "{\"addresses\":[").replace("</string>", "}");
	    Addresses fetchedAddresses = new Gson().fromJson(content, Addresses.class);
	    if(!fetchedAddresses.addresses.isEmpty()) {
	        Address a = fetchedAddresses.addresses.get(0);
	        try {
	            String part1 = a.addStreet;
	            int part2 = a.addNumb;
	            String part3 = a.addMunicip;
	            a.customFormattedAddress = part1 + (part2 > -1 ? " " + part2 : "") +
	                    (part3 != null ? " | " + part3 : ""); //the symbol | tells the parser that the following part should be on a new line;
	        }
	        catch(Exception e) {
				//if the location couldn't been determined very precisely
	            a.customFormattedAddress = a.addFormated;
	        }
			return a;
	    }
	    else {
	        throw new RuntimeException("no_results");
	    }
	}
	
	
	public class Addresses {
		public List<Address> addresses;
	}
	
	public class Address {
		public String customFormattedAddress;
	    public int addType;
		public int addId;
		public String addZip;
		public String addStreet;
		public String addMunicip;
		public int addNumb;
		public AddPoint addPoint;
		public AddPoint addPointWGS84;
		public String addFormated;
	}
	
	public class AddPoint {
		public float x;
		public float y;
	}

	public static void main(String[] args) {
		TerraLocator l = new TerraLocator();
//		try {
//			Coordinates coord = new Coordinates(new BigDecimal("38.0336412"), new BigDecimal("23.8340532"));
//			System.out.println("Address found: " + l.getAddressByCoordinates(coord));
//		}
//		catch (Exception e) {
//			System.err.println("Failed to find address by given coordinates due to exception: " + e.getMessage());
//		}
		args = new String[] {
				   "��.����.���������������� 282-284, ������������",
				   "���������������� ������.46 ��������������"
//					        "\u039B. \u039A\u0397\u03A6\u0399\u03A3\u0399\u0391\u03A3 221-223, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
//							"\u03A5\u0391\u039A\u0399\u039D\u0398\u039F\u03A5 & \u039A\u0391\u03A0\u039F\u0394\u0399\u03A3\u03A4\u03A1\u0399\u039F\u03A5 54, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
//							"\u039B\u0395\u03A9\u03A6. \u039A\u03A5\u03A0\u03A1\u039F\u03A5 39 & \u039C\u03A0\u0399\u0396\u0391\u039D\u0399\u039F\u03A5 1 \u0391\u03A1\u0393\u03A5\u03A1\u039F\u03A5\u03A0\u039F\u039B\u0397",
//							"\u03A7\u03B5\u03B9\u03BC\u03AC\u03C1\u03B1\u03C2 61 \u03BA\u03B1\u03B9 \u039A\u03B1\u03BB\u03BB\u03B9\u03C0\u03CC\u03BB\u03B5\u03C9\u03C2",
//							"\u03A0\u03A1\u03A9\u03A4\u039F\u03A0\u0391\u03A0\u0391\u0394\u0391\u039A\u0397 1 \u0395\u039B.\u0392\u0395\u039D\u0399\u0396\u0395\u039B\u039F\u03A5&\u03A3\u03A5\u03A1\u039F\u03A5, \u0393\u0391\u039B\u0391\u03A4\u03A3\u0399",
//					        "\u0392.\u03A3\u039F\u03A6\u0399\u0391\u03A3 13, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
//					        "\u039B\u0395\u03A9\u03A6. \u039A\u0397\u03A6\u0399\u03A3\u0399\u0391\u03A3 170 - \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
//					        "\u039B.\u039A\u0397\u03A6\u0399\u03A3\u0399\u0391\u03A3 205, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
//					        "\u039B. \u039A\u03A5\u039C\u0397\u03A3 110, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
//					        "\u0391\u0393.\u039A\u03A9\u039D/\u039D\u039F\u03A5 24, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399"
					} ;
		for (String s : args) {
			System.out.print(s + ": ");
			try {
				System.out.println(l.getCoordinatesByAddress(s));
			}
			catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}
}
