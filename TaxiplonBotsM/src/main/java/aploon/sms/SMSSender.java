package aploon.sms;


import aploon.xmpp.ssclient.entities.CountryCodes;

import com.taxiplon.Util;
import com.taxiplon.sms.BulkSMS;
import com.taxiplon.sms.GRSms;
import com.taxiplon.sms.ROSms;

public class SMSSender {
    //TODO:
    //This class should be refactored to use reflection when i find time will do
    private BulkSMS bulk;
    private String defSender;
    private static boolean init = false;
    private static boolean bUseSMS = false;

    public static synchronized SMSSender getInstance(String abrev) {
    	if (!init) {
    		String tmp = Util.getProperty("use_sms", "config");
    		if (tmp != null) {
    			bUseSMS = Boolean.parseBoolean(tmp);
    		}
    		init = true;
    	}
	
    	String smsud = Util.getProperty("sms_user", "config");
    	String smspd = Util.getProperty("sms_password", "config");
    	String smsco = Util.getProperty("sms_user_" +( abrev == null ? "" : abrev), "config");
    	String smspp = Util.getProperty("sms_pass_" +( abrev == null ? "" : abrev), "config");
    	
    	//after this will be done with hashkeys
    	return new SMSSender((smsco == null || smsco == "") ? smsud : smsco,
    			(smspp == null || smspp == "") ? smspd : smspp, abrev);
    }

    public SMSSender(String usr, String pwd, String abrev) {
    	if (abrev != null) {
    		try {
    			Class<?> objSMS = Class.forName("com.taxiplon.sms." + abrev + "Sms");
    			if (objSMS == null) {
    				bulk = new GRSms(usr, pwd);
    			} else {
    				bulk = (BulkSMS)objSMS.newInstance();
    				bulk.setUser(usr);
    				bulk.setPassword(pwd);
    			}

    		} catch (Exception e) {
    			bulk = new GRSms(usr, pwd);
    		}
    	} else {
    		bulk = new GRSms(usr, pwd);
    	}

    	defSender  = Util.getProperty("sms_sender", "config");

    }

    public void sendSMS(String mobileTo, String mobileFrom, String message, String country) {
    	if (bUseSMS) {
    		String telTo = mobileTo;
    		if (country != null && (!country.equals(ROSms.SMS_COUNTRY))) {
    			telTo = CountryCodes.getInstance().getInternationalMobile(country, mobileTo);
    		}
    		bulk.sendSMS(telTo, message, mobileFrom == null ? defSender : mobileFrom);
    	}
    }
}
