package aploon.sms;

import com.taxiplon.db.DataPool;

import aploon.datahelper.DataHelper;

import java.sql.SQLException;

import com.taxiplon.sms.SMSFactory;
import com.taxiplon.sms.provider.BulkSMSnImpl;
import com.taxiplon.sms.SMSProvider;
import com.taxiplon.sms.model.SMSSettings;

import com.taxiplon.Util;

import org.apache.log4j.Logger;

/**
 * The new SMS implementation
 */
public class SMSSUtil {
    
    private boolean enabled;

    private long taxiCompanyId;
    private Long passengerId;
    private SMSSettings smsSettings;
    private DataPool dataPool;
    private String countryCode;
    
    private SMSProvider smsProvider;

    private static final Logger LOGGER = Logger.getLogger(SMSSUtil.class);
    
    public SMSSUtil(Long taxiCompanyId) {

        try {
            
            if(taxiCompanyId!=null) {

                LOGGER.info("The taxicompanyid was not null");

                this.taxiCompanyId = taxiCompanyId;

                dataPool = DataPool.getInstance("DB");
                smsSettings = DataHelper.getSmsSettings(dataPool.getConnection(), taxiCompanyId);            
                
                if(smsSettings!=null) {
                
                    smsProvider = SMSFactory.createSMSProvider(smsSettings);       
                    enabled = smsProvider!=null;
                    if(enabled) LOGGER.info("Valid sms provider");
                    
                } else {
                    LOGGER.error("No sms Settings found");    
                }    
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("PROBLEM");
        }
    }
    
    public void setCountryCode(String countryCode) {
        
        this.countryCode = countryCode;
    }
    
    public void setPassengerId(Long passengerId) {
        
        this.passengerId = passengerId;
    }
    
    public boolean isEnabled() {
        
        return enabled;
    }

    public long sendSms(String mobileTo, String message) {

        LOGGER.info("Invocation of the continual one");
       
        try {
         
        	if(!mobileTo.startsWith("+")) {
        	
        		String mobilePrefix = getCountryCallCode();
        		
        		if(mobilePrefix.equals("0030")) {
        			if(mobileTo.startsWith("69")) {
        				mobileTo = mobilePrefix+ mobileTo;
        			} else {
        				return -1;
        			}
        		} else if(mobilePrefix.equals("0041")) {
        			if(mobileTo.startsWith("07")) {
        				mobileTo = mobilePrefix+mobileTo;
        			} else {
        				return -1;
        			}
        		} else if(mobilePrefix.equals("00357")) {
        			if(mobileTo.startsWith("9")) {
        				mobileTo = mobilePrefix+mobileTo;
        			} else {
        				return -1;
        			}
        		} else {
        			mobileTo = mobilePrefix+ mobileTo;
                }
        		
        	}
        	
            Long lastSms = DataHelper.insertToSMSLog(dataPool.getConnection(), taxiCompanyId,mobileTo, message, null);
            
            LOGGER.info("The lastsms is "+lastSms);
            
            if(lastSms!=null&&lastSms!=0) {
                smsProvider.sendSms(lastSms, mobileTo, message);
            }
            
        } catch(SQLException e) {
            LOGGER.error("Problem with the lastSms procedure ",e);
        }
           
        return 0L;
    }

    /**
     * Soon to be depreacated
     */
    @Deprecated
    public void sendSmsAsync(String mobileTon,final String message) {
        
        LOGGER.info("asynchronous message");
        
        //final String mobileTo = getCountryCallCode()+ mobileTon;
        
        String mobilePrefix = "";
        
        if(!mobileTon.startsWith("+")) {
        	
    		mobilePrefix = getCountryCallCode();
    		
    		if(mobilePrefix.equals("0030")) {
    			if(mobileTon.startsWith("69")) {
    				mobileTon = mobilePrefix+ mobileTon;
    			} else {
    				return;
    			}
    		} else if(mobilePrefix.equals("0041")) {
    			if(mobileTon.startsWith("07")) {
    				mobileTon = mobilePrefix+mobileTon;
    			} else {
    				return;
    			}
    		} else if(mobilePrefix.equals("00357")) {
    			if(mobileTon.startsWith("9")) {
    				mobileTon = mobilePrefix+mobileTon;
    			} else {
    				return;
    			}
    		} else {
    			mobileTon = mobilePrefix+ mobileTon;
            }
    		
    	}
        
        final String mobileTo = mobileTon;
        
        try {
            Long lastSms = DataHelper.insertToSMSLog(dataPool.getConnection(), taxiCompanyId, mobileTo, message, null);   
            LOGGER.info("The lastsms is "+lastSms);
            if(lastSms!=null&&lastSms!=0) {
                smsProvider.sendSms(lastSms, mobileTo, message);
            }
            
        } catch(SQLException e) {
            LOGGER.error("Problem with the lastSms procedure ",e);
        }
        
        //TODO no more asynchronous calls
        /**
        new Thread(new Runnable(){
        
            @Override    
            public void run() {
                try {
                    Long lastSms = DataHelper.insertToSMSLog(dataPool.getConnection(), taxiCompanyId, mobileTo, message, null);   
                    LOGGER.info("The lastsms is "+lastSms);
                    if(lastSms!=null&&lastSms!=0) {
                        smsProvider.sendSms(lastSms, mobileTo, message);
                    }
                    
                } catch(SQLException e) {
                    LOGGER.error("Problem with the lastSms procedure ",e);
                }
            }
        }).start();
        */
    }

    public static void sendDefaultSms(String mobileTo,String message,String fromName) {
        SMSProvider smsProvider = defaultSmsProvider(fromName);
        smsProvider.sendSms(1L, mobileTo, message);
    }

    /**
     * 
     * @param mobileTo
     * @param message
     * @param fromName
     */
    @Deprecated
    public static void sendDefaultSmsAsync(final String mobileTo, final String message, final String fromName) {
        
        LOGGER.info("Sending default asynchronous message");
       
        sendDefaultSms(mobileTo, message, fromName);
        
        //TODO disabling asynchronous sms
        /**
        new Thread(new Runnable(){
            @Override
            public void run() {
                sendDefaultSms(mobileTo, message, fromName);              
            }
        }).start();   
        */
    }

    private static SMSProvider defaultSmsProvider(String fromName) {
    
        return new BulkSMSnImpl(Util.getProperty("sms_user","config"),
                                Util.getProperty("sms_password","config"),
                                fromName!=null?fromName:"Aploon",0);
    }

    private String getCountryCallCode() {
        
    	String prefix = null;
    	
        try {
            if(countryCode!=null) {
            	if(prefix==null) prefix = DataHelper.getCallCode(dataPool.getConnection(),countryCode);
            } else if(passengerId!=null) {
                if(prefix==null) prefix = DataHelper.getPassengerCallCode(dataPool.getConnection(), passengerId);
            } else {
                if(prefix==null) prefix = DataHelper.getPassengerCallCode(dataPool.getConnection(), taxiCompanyId);
            }   
        } catch(SQLException e) {
            LOGGER.error("Not retrieving call code",e);    
        }
        
        return prefix!=null?prefix:"0030";
    }

}
