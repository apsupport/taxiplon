package aploon.httpconnector;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;

public class HttpConnectionProvider {

	private static final Logger LOGGER = Logger.getLogger(HttpConnectionProvider.class);
	
	private static HttpClient httpClient;
	
	private HttpConnectionProvider() {}
	
	static {
		LOGGER.info("Initializing the http connectin provider");
		MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = connectionManager.getParams();
		params.setMaxTotalConnections(25);
		params.setDefaultMaxConnectionsPerHost(5);
		connectionManager.setParams(params);
		httpClient = new HttpClient(connectionManager);
	}
	
	public static HttpClient getHttpClient() {
		return httpClient;
	}
	
}
