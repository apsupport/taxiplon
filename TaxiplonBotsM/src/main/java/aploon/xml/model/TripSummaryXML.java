package aploon.xml.model;

import javax.xml.bind.annotation.XmlElement;

public class TripSummaryXML {

	private Integer assigned;
	private Integer lost;
	private Integer month;
	private Integer year;
	
	public TripSummaryXML(Integer assigned, Integer lost, Integer month, Integer year) {
		super();
		this.assigned = assigned;
		this.lost = lost;
		this.month = month;
		this.year = year;
	}

	public Integer getAssigned() {
		return assigned;
	}
	
	@XmlElement
	public void setAssigned(Integer assigned) {
		this.assigned = assigned;
	}
	
	public Integer getLost() {
		return lost;
	}
	
	@XmlElement
	public void setLost(Integer lost) {
		this.lost = lost;
	}
	
	public Integer getMonth() {
		return month;
	}
	
	@XmlElement
	public void setMonth(Integer month) {
		this.month = month;
	}
	
	public Integer getYear() {
		return year;
	}
	
	@XmlElement
	public void setYear(Integer year) {
		this.year = year;
	}
	
}
