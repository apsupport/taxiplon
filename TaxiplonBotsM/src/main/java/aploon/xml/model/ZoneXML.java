package aploon.xml.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


@Root(name="Zone")
public class ZoneXML {

	@Element
	private Long id;
	@Element(name="companyid",required=true)
	private Long companyId;
	@Element(name="zonename",required=true)
	private String zoneName;
	
	@Element(required=false)
	private Double zoneLat;
	@Element(required=false)
	private Double zoneLong;
	@Element(required=false)
	private Double zoneRange;
	
	@Element(required=true)
	private Integer drivers;

	@Element(required=false)
	private String zonePoints;
	
	@Element(required=false)
	private Boolean strictBorders;
	
	public ZoneXML(Long id, Long companyId, String zoneName, Double zoneLat,Double zoneLong,
			Double zoneRange, Integer drivers, String zonePoints, Boolean strictBorders) {
		super();
		this.id = id;
		this.companyId = companyId;
		this.zoneName = zoneName;
		this.zoneLat = zoneLat;
		this.zoneLong = zoneLong;
		this.zoneRange = zoneRange;
		this.drivers = drivers;
		this.zonePoints = zonePoints;
		this.strictBorders = strictBorders;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	public String getZoneName() {
		return zoneName;
	}
	
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	
	public Double getZoneLat() {
		return zoneLat;
	}
	
	public void setZoneLat(Double zoneLat) {
		this.zoneLat = zoneLat;
	}
	
	public Double getZoneLong() {
		return zoneLong;
	}
	
	public void setZoneLong(Double zoneLong) {
		this.zoneLong = zoneLong;
	}
	
	public Double getZoneRange() {
		return zoneRange;
	}
	
	public void setZoneRange(Double zoneRange) {
		this.zoneRange = zoneRange;
	}
	
	public Integer getDrivers() {
		return drivers;
	}
	
	public void setDrivers(Integer drivers) {
		this.drivers = drivers;
	}

	public String getZonePoints() {
		return zonePoints;
	}

	public void setZonePoints(String zonePoints) {
		this.zonePoints = zonePoints;
	}

	public Boolean getStrictBorders() {
		return strictBorders;
	}

	public void setStrictBorders(Boolean strictBorders) {
		this.strictBorders = strictBorders;
	}
	
	
	
}
