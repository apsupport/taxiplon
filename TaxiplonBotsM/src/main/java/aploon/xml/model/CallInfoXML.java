package aploon.xml.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Info")
public class CallInfoXML {

	@Element
	private Double lat;
	@Element
	private Double lng;
	@Element
	private String gaddress;
	@Element(required=false)
	private String address;
	@Element(name="regtime")
	private Long hailTime;
	@Element(required=false)
	private String special;
	@Element(required=false)
	private String status;
	@Element(required=false)
	private String name;
	@Element(required=false)
	private String destination;
	@Element(required=false)
	private String gender;
	@Element(required=false)
	private String mobile;
	@Element(required=false)
	private String remarks;
	@Element(required=false)
	private String companyname;
	@Element(required=false)
	private Double fare;
	@Element(required=false)
	private String paymenttype;
	@Element(required=false)
	private String callTypeName;
	@Element(required=false)
	private Integer clearBiddingTime;
	@Element(required=false)
	private Integer requestWaitTime;
	
	public Double getLat() {
		return lat;
	}
	
	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	public Double getLng() {
		return lng;
	}
	
	public void setLng(Double lng) {
		this.lng = lng;
	}
	
	public String getGaddress() {
		return gaddress;
	}
	
	public void setGaddress(String gaddress) {
		this.gaddress = gaddress;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Long getHailTime() {
		return hailTime;
	}

	public void setHailTime(Long hailTime) {
		this.hailTime = hailTime;
	}

	public String getSpecial() {
		return special;
	}
	
	public void setSpecial(String special) {
		this.special = special;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getRemarks() {
		return remarks;
	}
	
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public String getCompanyname() {
		return companyname;
	}
	
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	
	public Double getFare() {
		return fare;
	}
	
	public void setFare(Double fare) {
		this.fare = fare;
	}

	public String getPaymenttype() {
		return paymenttype;
	}

	public void setPaymenttype(String paymenttype) {
		this.paymenttype = paymenttype;
	}

	public String getCallTypeName() {
		return callTypeName;
	}

	public void setCallTypeName(String callTypeName) {
		this.callTypeName = callTypeName;
	}

	public Integer getClearBiddingTime() {
		return clearBiddingTime;
	}

	public void setClearBiddingTime(Integer clearBiddingTime) {
		this.clearBiddingTime = clearBiddingTime;
	}

	public Integer getRequestWaitTime() {
		return requestWaitTime;
	}

	public void setRequestWaitTime(Integer requestWaitTime) {
		this.requestWaitTime = requestWaitTime;
	}
	
}
