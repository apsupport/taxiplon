package aploon.xml.model;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.Element;

@Default
public class DriverAssignedTripXml {
	
	@Element(required=false)
	private Long tripId;
	@Element(required=false)
	private Long hailTime;
	@Element(required=false)
	private String pickUpAddress;
	@Element(required=false)
	private String specialNeeds;
	@Element(required=false)
	private String status;
	@Element(required=false)
	private Double fare;
	@Element(required=false)
	private String paymentType;
	@Element(required=false)
	private String shortName;
	
	public DriverAssignedTripXml() {}
	
	public DriverAssignedTripXml(Long tripId, Long hailTime,
			String pickUpAddress, String specialNeeds, String status,
			Double fare, String paymentType, String shortName) {
		super();
		this.tripId = tripId;
		this.hailTime = hailTime;
		this.pickUpAddress = pickUpAddress;
		this.specialNeeds = specialNeeds;
		this.status = status;
		this.fare = fare;
		this.paymentType = paymentType;
		this.shortName = shortName;
	}

	public Long getTripId() {
		return tripId;
	}
	
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	
	public Long getHailTime() {
		return hailTime;
	}
	
	public void setHailTime(Long hailTime) {
		this.hailTime = hailTime;
	}
	
	public String getPickUpAddress() {
		return pickUpAddress;
	}
	
	public void setPickUpAddress(String pickUpAddress) {
		this.pickUpAddress = pickUpAddress;
	}
	
	public String getSpecialNeeds() {
		return specialNeeds;
	}
	
	public void setSpecialNeeds(String specialNeeds) {
		this.specialNeeds = specialNeeds;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Double getFare() {
		return fare;
	}
	
	public void setFare(Double fare) {
		this.fare = fare;
	}

	public String getPaymentType() {
		return paymentType;
	}
	
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
}