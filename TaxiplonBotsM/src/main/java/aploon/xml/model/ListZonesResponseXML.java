package aploon.xml.model;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Zones")
public class ListZonesResponseXML {

	@Element(required=false)
	private Boolean success;
	@Element(required=false)
	private String message;
	@Element(required=false)
	private Integer total;
	@ElementList(required=false)
	private List<ZoneXML> content;
	
	public Boolean getSuccess() {
		return success;
	}
	
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public List<ZoneXML> getContent() {
		return content;
	}
	
	public void setContent(List<ZoneXML> content) {
		this.content = content;
	}
	
}
