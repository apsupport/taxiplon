package aploon.xml.model;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;

@Default
public class ResponseXml {

	@Element(required=false)
	private Boolean success;
	@Element(required=false)
	private String message;
	@ElementArray(required=false)
	private Object[] content;
	
	public Boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public Object[] getContent() {
		return content;
	}
	
	public void setContent(Object[] content) {
		this.content = content;
	}
	
}
