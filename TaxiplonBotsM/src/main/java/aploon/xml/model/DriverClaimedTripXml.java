package aploon.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DriverClaimedTripXml {

	private Long tripId;
	private Long hailTime;
	private String pickUpAddress;
	private String shortName;
	private String comments;
	private Double latitude;
	private Double longtitude;
	private String specialNeeds;
	
	public DriverClaimedTripXml() {}
	
	public DriverClaimedTripXml(Long tripId, Long hailTime, String pickUpAddress,
			String shortName, String comments,Double latitude,Double longtitude,String specialNeeds) {
		super();
		this.tripId = tripId;
		this.hailTime = hailTime;
		this.pickUpAddress = pickUpAddress;
		this.shortName = shortName;
		this.comments = comments;
		this.latitude = latitude;
		this.longtitude = longtitude;
		this.specialNeeds = specialNeeds;
	}

	public Long getTripId() {
		return tripId;
	}
	
	@XmlElement
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	
	public Long getHailTime() {
		return hailTime;
	}
	
	@XmlElement
	public void setHailTime(Long hailTime) {
		this.hailTime = hailTime;
	}
	
	public String getPickUpAddress() {
		return pickUpAddress;
	}
	
	@XmlElement
	public void setPickUpAddress(String pickUpAddress) {
		this.pickUpAddress = pickUpAddress;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	@XmlElement
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	public String getComments() {
		return comments;
	}
	
	@XmlElement
	public void setComments(String comments) {
		this.comments = comments;
	}

	public Double getLatitude() {
		return latitude;
	}

	@XmlElement
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongtitude() {
		return longtitude;
	}

	@XmlElement
	public void setLongtitude(Double longtitude) {
		this.longtitude = longtitude;
	}

	public String getSpecialNeeds() {
		return specialNeeds;
	}

	@XmlElement
	public void setSpecialNeeds(String specialNeeds) {
		this.specialNeeds = specialNeeds;
	}
	
	
}