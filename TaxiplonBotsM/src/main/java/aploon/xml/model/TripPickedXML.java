package aploon.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TripPickedXML {

	private Long tripId;
	private Long deviceId;
	private Long driverId;
	private String codeName;
	private String lastName;
	private String firstName;
	private Double latitude;
	private Double longtitude;
	private Double distance;
	private Double distanceAssigned;
	private String comments;
	
	public TripPickedXML() {}

	public TripPickedXML(Long tripId, Long deviceId, Long driverId,
			String codeName, String lastName, String firstName,
			Double latitude, Double longtitude, Double distance,
			Double distanceAssigned, String comments) {
		super();
		this.tripId = tripId;
		this.deviceId = deviceId;
		this.driverId = driverId;
		this.codeName = codeName;
		this.lastName = lastName;
		this.firstName = firstName;
		this.latitude = latitude;
		this.longtitude = longtitude;
		this.distance = distance;
		this.distanceAssigned = distanceAssigned;
		this.comments = comments;
	}

	public Long getTripId() {
		return tripId;
	}
	
	@XmlElement
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	
	public Long getDeviceId() {
		return deviceId;
	}
	
	@XmlElement
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	
	public Long getDriverId() {
		return driverId;
	}
	
	@XmlElement
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public String getCodeName() {
		return codeName;
	}

	@XmlElement
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getLastName() {
		return lastName;
	}
	
	@XmlElement
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	@XmlElement
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Double getLatitude() {
		return latitude;
	}
	
	@XmlElement
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongtitude() {
		return longtitude;
	}
	
	@XmlElement
	public void setLongtitude(Double longtitude) {
		this.longtitude = longtitude;
	}
	
	public Double getDistance() {
		return distance;
	}
	
	@XmlElement
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public Double getDistanceAssigned() {
		return distanceAssigned;
	}
	
	@XmlElement
	public void setDistanceAssigned(Double distanceAssigned) {
		this.distanceAssigned = distanceAssigned;
	}
	
	public String getComments() {
		return comments;
	}
	
	@XmlElement
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
