package aploon.xml.model;

import org.simpleframework.xml.Element;

public class NearByDriverXML {
	
	@Element(required=true)
	private Long driverId;
	@Element(required=true)
	private Long deviceId;	
	@Element(required=true)
	private String codeName;
	@Element(required=true)
	private String surName;
	@Element(required=true)
	private String name;
	@Element(required=true)
	private Double currentLat;
	@Element(required=true)
	private Double currentLong;
	@Element(required=true)
	private String plateNumber;
	@Element(required=true)
	private String carModel;
	@Element(required=true)
	private Double distance;

	@Element(required=true)
	private Boolean isFaved;
	@Element(required=true)
	private Boolean speaksEnglish;
	@Element(required=true)
	private Boolean newVehicle;
	@Element(required=false)
	private Boolean minivan;
	@Element(required=true)
	private Boolean hybrid;
	@Element(required=true)
	private Boolean hasWifi;
	@Element(required=true)
	private Boolean isSmoker;
	@Element(required=true)
	private Boolean petsAllowed;
	@Element(required=true)
	private Integer totalFavs;
	@Element(required=true)
	private Boolean bigTeamVeh;
	@Element(required=true)
	private Boolean takeCreditCards;
	@Element(required=true)
	private Boolean supportDisabled;
	
	
	public Long getDriverId() {
		return driverId;
	}
	
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public String getCodeName() {
		return codeName;
	}
	
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	
	public String getSurName() {
		return surName;
	}
	
	public void setSurName(String surName) {
		this.surName = surName;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public Double getCurrentLat() {
		return currentLat;
	}
	
	public void setCurrentLat(Double currentLat) {
		this.currentLat = currentLat;
	}
	
	public Double getCurrentLong() {
		return currentLong;
	}
	
	public void setCurrentLong(Double currentLong) {
		this.currentLong = currentLong;
	}
	
	public String getPlateNumber() {
		return plateNumber;
	}
	
	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}
	
	public String getCarModel() {
		return carModel;
	}
	
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	
	public Double getDistance() {
		return distance;
	}
	
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public Boolean getIsFaved() {
		return isFaved;
	}
	
	public void setIsFaved(Boolean isFaved) {
		this.isFaved = isFaved;
	}
	
	public Boolean getSpeaksEnglish() {
		return speaksEnglish;
	}
	
	public void setSpeaksEnglish(Boolean speaksEnglish) {
		this.speaksEnglish = speaksEnglish;
	}
	
	public Boolean getNewVehicle() {
		return newVehicle;
	}
	
	public void setNewVehicles(Boolean newVehicles) {
		this.newVehicle = newVehicles;
	}
	
	public Boolean getMinivan() {
		return minivan;
	}
	
	public void setMinivan(Boolean minivan) {
		this.minivan = minivan;
	}
	
	public Boolean getHybrid() {
		return hybrid;
	}
	
	public void setHybrid(Boolean hybrid) {
		this.hybrid = hybrid;
	}
	
	public Boolean getHasWifi() {
		return hasWifi;
	}
	
	public void setHasWifi(Boolean hasWifi) {
		this.hasWifi = hasWifi;
	}
	
	public Boolean getIsSmoker() {
		return isSmoker;
	}
	
	public void setIsSmoker(Boolean isSmoker) {
		this.isSmoker = isSmoker;
	}
	
	public Boolean getPetsAllowed() {
		return petsAllowed;
	}
	
	public void setPetsAllowed(Boolean petsAllowed) {
		this.petsAllowed = petsAllowed;
	}
	
	public Integer getTotalFavs() {
		return totalFavs;
	}
	
	public void setTotalFavs(Integer totalFavs) {
		this.totalFavs = totalFavs;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Boolean getBigTeamVeh() {
		return bigTeamVeh;
	}

	public void setBigTeamVeh(Boolean bigTeamVeh) {
		this.bigTeamVeh = bigTeamVeh;
	}

	public Boolean getTakeCreditCards() {
		return takeCreditCards;
	}

	public void setTakeCreditCards(Boolean takeCreditCards) {
		this.takeCreditCards = takeCreditCards;
	}

	public Boolean getSupportDisabled() {
		return supportDisabled;
	}

	public void setSupportDisabled(Boolean supportDisabled) {
		this.supportDisabled = supportDisabled;
	}

	public void setNewVehicle(Boolean newVehicle) {
		this.newVehicle = newVehicle;
	}
	
}
