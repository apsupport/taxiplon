package aploon.xml.model;

import java.util.List;

public class NearbyCompanyXML {

	private Long taxiCompanyId;
	private String shortName;
	private String officialName;
	private String companyType;
	private String address;
	private String telephone;
	private String fax;
	private String email;
	private String webSite;
	private Double hailRange;
	private Integer clearBiddingTime;
	private Integer requestWaitTime;
	private Integer appointmentRemind;
	private Integer candidateNumber;
	private String companyInfo;
	private Integer availableDrivers;
	private Double minimumDistance;
	private List<NearByDriverXML> nearByDriverXMLs;
	
	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}
	
	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	public String getOfficialName() {
		return officialName;
	}
	
	public void setOfficialName(String officialName) {
		this.officialName = officialName;
	}
	
	public String getCompanyType() {
		return companyType;
	}
	
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getTelephone() {
		return telephone;
	}
	
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getFax() {
		return fax;
	}
	
	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getWebSite() {
		return webSite;
	}
	
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	
	public Double getHailRange() {
		return hailRange;
	}
	
	public void setHailRange(Double hailRange) {
		this.hailRange = hailRange;
	}
	
	public Integer getClearBiddingTime() {
		return clearBiddingTime;
	}
	
	public void setClearBiddingTime(Integer clearBiddingTime) {
		this.clearBiddingTime = clearBiddingTime;
	}
	
	public Integer getRequestWaitTime() {
		return requestWaitTime;
	}
	
	public void setRequestWaitTime(Integer requstWaitTime) {
		this.requestWaitTime = requstWaitTime;
	}
	
	public Integer getAppointmentRemind() {
		return appointmentRemind;
	}
	
	public void setAppointmentRemind(Integer appointmentRemind) {
		this.appointmentRemind = appointmentRemind;
	}
	
	public Integer getCandidateNumber() {
		return candidateNumber;
	}
	
	public void setCandidateNumber(Integer candidateNumber) {
		this.candidateNumber = candidateNumber;
	}
	
	public String getCompanyInfo() {
		return companyInfo;
	}
	
	public void setCompanyInfo(String companyInfo) {
		this.companyInfo = companyInfo;
	}
	
	public Integer getAvailableDrivers() {
		return availableDrivers;
	}
	
	public void setAvailableDrivers(Integer availableDrivers) {
		this.availableDrivers = availableDrivers;
	}
	
	public Double getMinimumDistance() {
		return minimumDistance;
	}
	
	public void setMinimumDistance(Double minimumDistance) {
		this.minimumDistance = minimumDistance;
	}

	public List<NearByDriverXML> getNearByDriverXMLs() {
		return nearByDriverXMLs;
	}

	public void setNearByDriverXMLs(List<NearByDriverXML> nearByDriverXMLs) {
		this.nearByDriverXMLs = nearByDriverXMLs;
	}
	
}
