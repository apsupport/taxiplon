package aploon.xml.model;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="responseXML")
public class LostTripsResponseXML {
	
	@Element(required=false)
	private Boolean success;
	@Element(required=false)
	private String message;
	@ElementList(required=false)
	private List<DriverClaimedTripXml> content;
	
	public Boolean getSuccess() {
		return success;
	}
	
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<DriverClaimedTripXml> getContent() {
		return content;
	}
	
	public void setContent(List<DriverClaimedTripXml> content) {
		this.content = content;
	}
	
}
