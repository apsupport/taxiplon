package aploon.statusmanager;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;

import aploon.datahelper.DataHelper;
import aploon.model.OnRideStatusInfo;

public class OnRideStatusManager {

	private Long driverId;
	private Integer minutes;
	
	private static final Logger LOGGER = Logger.getLogger(OnRideStatusManager.class);
	
	public OnRideStatusManager(Long driverId,Integer minutes) {
		this.driverId = driverId;
		this.minutes = minutes;
	}
	
	public OnRideStatusManager(Long driverId) {
		this.driverId = driverId;
		this.minutes = 7;
	}

	public void setOnRide(long tripId) {
		try {
			minutes = DataHelper.getTripPickedRideDelay(tripId);
			if(minutes>0) {
				try {
					DataHelper.setDriverStatusOnRide(driverId);
				} catch(SQLException e) {
					LOGGER.error("Problem setting driver ",e);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("Problem setting driver ",e);
		}
	}
	
	public void undoOnRideStatus() {
	
		try {
			
			OnRideStatusInfo onRideStatusInfo = DataHelper.getOnRideStatusInfo(driverId);
			
			if(onRideStatusInfo==null) {
				LOGGER.debug("The onrider status info is null ");
				return;
			}
			
			LOGGER.debug("The on ride status is "+onRideStatusInfo.getPickedRideDelay()+" "+
						onRideStatusInfo.getPickUpTime());
			
			if(onRideStatusInfo.getPickUpTime()!=null&&onRideStatusInfo.getPickedRideDelay()!=null&&onRideStatusInfo.getPickedRideDelay()>0) {
				
				long minuteDifference = timestampMinuteDifference(onRideStatusInfo.getPickUpTime(), new Timestamp(new Date().getTime()));
				
				if(minuteDifference>onRideStatusInfo.getPickedRideDelay()) {
					
					try {
						DataHelper.unsetDriverStatusOnRide(driverId);
					} catch (SQLException e) {
						LOGGER.error("Problem setting driver on ride ",e);
					}
				
				}
				
			} else {
				try {
					DataHelper.unsetDriverStatusOnRide(driverId);
				} catch (SQLException e) {
					LOGGER.error("Problem setting driver on ride ",e);
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("Problem fetching timestamp");
		}
		
	}
	
	/**
     * Minutes in timestamp
     * @param first
     * @param last
     * @return
     */
    public static long timestampMinuteDifference(Timestamp first,Timestamp last) {
    	long milliseconds1 = first.getTime();
    	long milliseconds2 = last.getTime();
    	long diff = milliseconds2 - milliseconds1;
    	long diffSeconds = diff / 1000;
    	long diffMinutes = diff / (60 * 1000);
    	long diffHours = diff / (60 * 60 * 1000);
    	long diffDays = diff / (24 * 60 * 60 * 1000);
    	return diffMinutes;
    }
	
}
