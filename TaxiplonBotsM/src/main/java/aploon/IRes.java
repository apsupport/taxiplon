package aploon;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class IRes {

	public int iResT;
	public String iResM;
	public List<IResItem> iResItems;
	
	public class IResItem {
		
		public int iT;
		public List<IResItem> iC;
		public List<IResItemProps> iA;
		public float iLat;
		public float iLon;
		public int isSuggested;
		public int isLive;
		public int dist;
		public int ord;
		
	    public void sortByDistanceAsc() {
	        Object[] array = iC.toArray();
	        Arrays.sort(array, new Comparator<Object>() {
	            @Override
	            public int compare(Object left, Object right) {
	                return ((IResItem) left).dist > ((IResItem) right).dist? 1 : -1;
	            }
	        });
	        iC.clear();
	        for(Object item : array) {
	            iC.add((IResItem) item);
	        }
	    }
	}
	
	public class IResItemProps {
		public int aT;
		public String aD;
		public String aF;
		public String aV;
		public int sDisp;
	}
}
