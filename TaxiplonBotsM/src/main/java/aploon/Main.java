package aploon;


import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ServerSideClientMBean;
import aploon.xmpp.ssclient.Dispatcher;
import aploon.xmpp.ssclient.DriverHistoryBot;
import aploon.xmpp.ssclient.FareControl;
import aploon.xmpp.ssclient.FavoriteDrivers;
import aploon.xmpp.ssclient.LocationUpdater;
import aploon.xmpp.ssclient.Login;
import aploon.xmpp.ssclient.Messages;
import aploon.xmpp.ssclient.Overview;
import aploon.xmpp.ssclient.Payments;
import aploon.xmpp.ssclient.PickCall;
import aploon.xmpp.ssclient.PredefinedMessages;
import aploon.xmpp.ssclient.Reports;
import aploon.xmpp.ssclient.SOS;
import aploon.xmpp.ssclient.Statistics;
import aploon.xmpp.ssclient.StatusCheck;
import aploon.xmpp.ssclient.UpdateCall;
import aploon.xmpp.ssclient.YellowPages;
import aploon.xmpp.ssclient.Zones;
import aploon.xmpp.ssclient.bots.AppointDispatcher;
import aploon.xmpp.ssclient.bots.FareDispatcher;
import aploon.xmpp.ssclient.entities.Entity;
import aploon.xmpp.ssclient.entities.Updater;
import aploon.xmpp.ssclient.iphone.Push;
import aploon.xmpp.ssclient.poi.ListBoats;
//import aploon.xmpp.ssclient.poi.ListBoats;
import aploon.xmpp.ssclient.poi.ListFlights;
import aploon.xmpp.ssclient.poi.ListHotspots;
import aploon.xmpp.ssclient.poi.ListTrains;
import aploon.xmpp.ssclient.poi.TripSheet;
import aploon.xmpp.ssclient.registration.RegisterDevice;
import aploon.xmpp.ssclient.registration.RegisterDriver;
import aploon.xmpp.ssclient.registration.RegisterPassenger;
import aploon.xmpp.ssclient.web.FavouriteLocations;
import aploon.xmpp.ssclient.web.ListAppoints;
import aploon.xmpp.ssclient.web.ListCalls;
import aploon.xmpp.ssclient.web.ListDevices;
import aploon.xmpp.ssclient.web.ListDrivers;
import aploon.xmpp.ssclient.web.ListMessages;
import aploon.xmpp.ssclient.web.ListSims;
import aploon.xmpp.ssclient.web.ListUsers;
import aploon.xmpp.ssclient.web.ListVehicles;
import aploon.xmpp.ssclient.web.ListZones;
import aploon.xmpp.ssclient.web.TripHistory;
import aploon.xmpp.ssclient.web.WhitePages;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Locale;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import com.aploon.mbean.BotMBean;
import com.aploon.mbean.DispatcherMBean;
import com.aploon.mbean.LoggerMBean;
import com.aploon.mbean.LoggerMBeanImpl;


public class Main {

    public static boolean RUNNING = true;
    public static boolean DEBUG = false;

    ArrayList<ServerSideClient> clients = new ArrayList<ServerSideClient>() {
    	
    	public String toString() {
    		
    		String result = "";
    		for (int i = 0; i < size(); i++) {
    			ServerSideClient o = get(i);
    			result +=
    					"<tr style='background-color:" + (i % 2 == 0 ? "grey" : "white") + "'>" + o.toString() + "</tr>";
    		}
    		
    		return result;
    	}
    };
    
    JFrame frame;
    JLabel stats;
    JLabel topStatus;
    JLabel bottomStatus;
    Mode mode;
    long startTime;

    public enum Mode {

    	standalone,
    	service,
    	none;

    	public static Mode parseMode(String s) {
    		if (standalone.toString().equals(s)) {
    			return standalone;
    		} else if (service.toString().equals(s)) {
    			return service;
    		} else {
    			return none;
    		}
    	}
    }

    private static final Logger LOGGER = Logger.getLogger(Main.class);

    public Main(Mode mode) {
    	super();

        startTime = System.currentTimeMillis();
        this.mode = mode;
        //init the frame straight away to show the progress
        switch (mode) {
        case none:
        case standalone:
        	frame = new JFrame();
        	frame.setLayout(new BorderLayout());
        	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	frame.addWindowListener(new WindowAdapter() {
        		public void windowClosed(WindowEvent we) {
        			RUNNING = false;
        			System.exit(0);
        		}
        	});
        	frame.setTitle("Taxiplon Bots");
        	topStatus = new JLabel();
        	frame.add(topStatus, BorderLayout.NORTH);
        	stats = new JLabel();
        	JScrollPane scroll = new JScrollPane(stats);
        	frame.setMinimumSize(new Dimension((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2,
					       (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight() - 50));
        	frame.add(scroll, BorderLayout.CENTER);
        	JButton trafficButton = new JButton("Show traffic break down");
        	trafficButton.addActionListener(new ActionListener() {
        		public void actionPerformed(ActionEvent ae) {
        			System.out.println("Outgoing traffic:");
        			try {
        				System.out.println(Usage.getInstance().getOutDataPerRecipient().toString());
        			} catch (Exception e) {
        				e.printStackTrace();
        			}
        		}
        	});
        	bottomStatus = new JLabel();
        	JPanel southPanel = new JPanel();
        	southPanel.add(bottomStatus);
        	southPanel.add(trafficButton);
        	frame.add(southPanel, BorderLayout.SOUTH);
        	//				frame.pack();
        	positionFrame();
        	updateStats();
        	frame.setEnabled(true);
        	frame.setVisible(true);
        	lock(frame);
        	break;
        case service:
        	break;
        default:
        	break;
        }
	
        //past this point the directory is exclusively locked
        try {
        
        	setLoggingMbean();
        	
        	//BOTS FOR MESSAGES, HOTSPOTS INTERACTION (add, update), REPORTS
        	clients.add(Messages.getInstance());
        	registerServerSideClientBean(Messages.getInstance(), "aploon.xmpp.ssclient:type=Messages");

        	//CLIENTS FOR REGISTRATION AND INFORMATION UPDATE
        	
        	RegisterPassenger registerPassenger = new RegisterPassenger();
        	clients.add(registerPassenger);
        	registerServerSideClientBean(registerPassenger, "aploon.xmpp.ssclient:type=RegisterPassenger");
	    
        	//CLIENT FOR PLACING AND UPDTATING CALLS
        	clients.add(UpdateCall.getInstance());
        	registerServerSideClientBean(UpdateCall.getInstance(), "aploon.xmpp.ssclient:type=UpdateCall");
        	
        	//CLIENT FOR PICKING CALLS
        	PickCall pickCall = new PickCall();
        	clients.add(pickCall);
        	registerServerSideClientBean(pickCall, "aploon.xmpp.ssclient:type=PickCall");
        	
        	
        	//CLIENT FOR DISPATCHING CALLS TO DRIVERS
        	clients.add(Dispatcher.getInstance());
        	registerServerSideClientBean(Dispatcher.getInstance(), "aploon.xmpp.ssclient:type=Dispatcher");
        	
        	//registerDispatcher(Dispatcher.getInstance());
        	
        	//CLIENT FOR FARE UPDATE
        	FareControl fareControl = new FareControl();
        	clients.add(fareControl);
        	registerServerSideClientBean(fareControl, "aploon.xmpp.ssclient:type=FareControl");
        	
        	
        	//FARE, APPOINTMENT, EVENT DISPATCHERS
        	clients.add(FareDispatcher.getInstance());
        	registerServerSideClientBean(FareDispatcher.getInstance(), "aploon.xmpp.ssclient:type=FareDispatcher");
        	
        	//CLIENT FOR JOINING/LEAVING ZONES
        	clients.add(Zones.getInstance());
        	registerServerSideClientBean(Zones.getInstance(), "aploon.xmpp.ssclient:type=Zones");
        	
        	//CLIENT FOR DRIVER LOCATION UPDATE
        	LocationUpdater locationUpdater = new LocationUpdater();
        	clients.add(locationUpdater);
        	registerServerSideClientBean(locationUpdater, "aploon.xmpp.ssclient:type=LocationUpdater");
        	
        	//CLIENT FOR SEARCHING COMPANIES
        	YellowPages yellowPages = new YellowPages();
        	clients.add(yellowPages);
        	registerServerSideClientBean(yellowPages, "aploon.xmpp.ssclient:type=YellowPages");
        	
        	
        	ListDrivers listDrivers = new ListDrivers();
        	clients.add(listDrivers);
        	registerServerSideClientBean(listDrivers, "aploon.xmpp.ssclient:type=ListDrivers");
        	
        	ListZones listZones = new ListZones();
        	clients.add(listZones);
        	registerServerSideClientBean(listZones, "aploon.xmpp.ssclient:type=ListZones");
        	
        	//CLIENT FOR WEB REQUESTS FOR SEARCHING AND REGISTERING PASSENGERS
        	WhitePages whitePages = new WhitePages();
        	clients.add(whitePages);
        	registerServerSideClientBean(whitePages, "aploon.xmpp.ssclient:type=WhitePages");
        	
        	//LOGIN BOT
        	Login login = new Login();
        	clients.add(login);
        	registerServerSideClientBean(login, "aploon.xmpp.ssclient:type=Login");
        	
        	DriverHistoryBot driverHistoryBot = new DriverHistoryBot();
        	clients.add(driverHistoryBot);
        	registerServerSideClientBean(driverHistoryBot, "aploon.xmpp.ssclient:type=DriverHistoryBot");
        	
        	clients.add(RegisterDevice.getInstance());
        	registerServerSideClientBean(RegisterDevice.getInstance(), "aploon.xmpp.ssclient:type=RegisterDevice");
        	
        	//Driver pre registration bot
        	clients.add(RegisterDriver.getInstance());
        	registerServerSideClientBean(RegisterDriver.getInstance(), "aploon.xmpp.ssclient:type=RegisterDriver");
        	
        	clients.add(Updater.getInstance());
        	registerServerSideClientBean(Updater.getInstance(), "aploon.xmpp.ssclient:type=Updater");
        	
        	clients.add(Entity.getInstance());
        	registerServerSideClientBean(Entity.getInstance(), "aploon.xmpp.ssclient:type=Entity");
        	
        	//BOT FOR STATUS CHECKS BY THE PASSENGERS/DRIVERS
        	
        	//TODO 
        	clients.add(StatusCheck.getInstance());
        	registerServerSideClientBean(StatusCheck.getInstance(), "aploon.xmpp.ssclient:type=StatusCheck");
        	
        	//clients.add(LiveDispatcher.getInstance());
        	//ekos 22/3
        	//clients.add(EventDispatcher.getInstance());
        	clients.add(AppointDispatcher.getInstance());
        	registerServerSideClientBean(AppointDispatcher.getInstance(), "aploon.xmpp.ssclient:type=AppointDispatcher");
        	
        	//CLIENT FOR ZOOM OUT OVERVIEW
        	
        	Overview overview = new Overview();
        	clients.add(overview);
        	registerServerSideClientBean(overview, "aploon.xmpp.ssclient:type=Overview");
        	
        	//CLIENT FOR SOS DISPATCH
        	clients.add(SOS.getInstance());
        	registerServerSideClientBean(SOS.getInstance(), "aploon.xmpp.ssclient:type=SOS");
        	
        	
        	//CLIENT FOR WEB REQUESTS FOR LISTS (CALL, APPOINTMENTS, DRIVERS, MESSAGES)
        	ListCalls listCalls = new ListCalls();
        	clients.add(listCalls);
        	registerServerSideClientBean(listCalls, "aploon.xmpp.ssclient:type=ListCalls");
        	
        	ListDevices listDevices = new ListDevices();
        	clients.add(listDevices);
        	registerServerSideClientBean(listDevices, "aploon.xmpp.ssclient:type=ListDevices");
        	
        	ListVehicles listVehicles = new ListVehicles();
        	clients.add(listVehicles);
        	registerServerSideClientBean(listVehicles, "aploon.xmpp.ssclient:type=ListVehicles");
        	
        	ListSims listSims = new ListSims();
        	clients.add(listSims);
        	registerServerSideClientBean(listSims, "aploon.xmpp.ssclient:type=ListSims");
        	
        	ListAppoints listAppoints = new ListAppoints();
        	clients.add(listAppoints);
        	registerServerSideClientBean(listAppoints, "aploon.xmpp.ssclient:type=ListAppoints");
        	
        	ListMessages listMessages = new ListMessages();
        	clients.add(listMessages);
        	registerServerSideClientBean(listMessages, "aploon.xmpp.ssclient:type=ListMessages");
        	
        	ListUsers listUsers = new ListUsers();
        	clients.add(listUsers);
          	registerServerSideClientBean(listUsers, "aploon.xmpp.ssclient:type=ListUsers");
        	
            
        	//CLIENT FOR FETCHING TRIP HISTORY
	    
          	TripHistory tripHistory = new TripHistory();
        	clients.add(tripHistory);
        	registerServerSideClientBean(tripHistory, "aploon.xmpp.ssclient:type=TripHistory");
        	
            
        	//CLIENT FOR FAVORITE DRIVERS INTERFACE
        	FavoriteDrivers favoriteDrivers = new FavoriteDrivers();
        	clients.add(favoriteDrivers);
        	registerServerSideClientBean(favoriteDrivers, "aploon.xmpp.ssclient:type=FavoriteDrivers");
        	
        	
        	//ekos i dont know why but there were two bots of the same name
        	//clients.add(new YellowPages());
        	//clients.add(Hotspots.getInstance());
        	Reports reports = new Reports();
        	clients.add(reports);
        	registerServerSideClientBean(reports, "aploon.xmpp.ssclient:type=Reports");
        	
        	Push push = new Push();
        	clients.add(push);
        	registerServerSideClientBean(push, "aploon.xmpp.ssclient:type=Push");
        	
        	//clients.add(NewsDispatcher.getInstance());
        	//HOTSPOT, AIRPORT, PORT BOTS
        	//clients.add(ListHotspots.getInstance());
        	
        	ListFlights listFlights = new ListFlights(1);
        	clients.add(listFlights);
        	registerServerSideClientBean(listFlights, "aploon.xmpp.ssclient:type=ListFlights");
        	
        	ListBoats listBoats = new ListBoats();
        	clients.add(listBoats);
        	registerServerSideClientBean(listBoats, "aploon.xmpp.ssclient:type=ListBoats");
        	
        	//clients.add(new ListTrains());
        	//STATISTICS
        	Statistics statistics = new Statistics();
        	clients.add(statistics);
        	registerServerSideClientBean(statistics, "aploon.xmpp.ssclient:type=Statistics");
        	
        	//Paypal Payments
        	
        	Payments payments = new Payments();
        	clients.add(payments);
        	registerServerSideClientBean(payments, "aploon.xmpp.ssclient:type=Payments");
        	
        	//Web book favourite locations interface
        	FavouriteLocations favouriteLocations = new FavouriteLocations();
        	clients.add(favouriteLocations);
        	registerServerSideClientBean(favouriteLocations, "aploon.xmpp.ssclient:type=FavouriteLocations");
        	
        	//List flights on demand is the succesor of listflights
        	//clients.add(new ListFlightsOnDemand());
        	TripSheet tripSheet = new TripSheet();
        	clients.add(tripSheet);
        	registerServerSideClientBean(tripSheet, "aploon.xmpp.ssclient:type=TripSheet");
        	
        	PredefinedMessages predefinedMessages = new PredefinedMessages();
        	clients.add(predefinedMessages);
        	registerServerSideClientBean(predefinedMessages, "aploon.xmpp.ssclient:type=PredefinedMessages");
        	
        	for (ServerSideClient ssc : clients) {
        		try {
        			//createBotAccount(ssc.getClass().getSimpleName());
        			ssc.listenForMessages();
        		} catch (Exception e) {
        			System.err.println("Failed to set message listener on client " + ssc.getUsername() + " due to exception: " +
				       e.getMessage());
        			e.printStackTrace();
        			createBotAccount(ssc.getClass().getSimpleName());
        		}
        	}
        
        	Thread mainThread = new Thread(new Runnable() {
        		public void run() {
        			try {
        				while (RUNNING) {
        					Thread.sleep(1000);
        					updateStats();
        				}
        			} catch (Exception e) {

        			}
        		}
        	});
        	mainThread.setName("main_thread");
        	mainThread.start();
        	//.start();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    private void createBotAccount(String botname) {
    	try {
    		RegisterDevice.getInstance().createXMPPAccount(botname, botname, "", "bot");
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

    protected void lock(JFrame parent) {
    	try {
    		//find out the PID of this process
    		RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
    		String pid = bean.getName().split("@")[0];
    		//check the lock and if doesn't exist create it and store the pid
    		File lock = new File(".lock");
    		if (!lock.exists()) {
    			//normal behavior
    			lock.createNewFile();
    			BufferedWriter buf = new BufferedWriter(new FileWriter(lock));
    			buf.write(pid, 0, pid.length());
    			buf.close();
    			//order to delete this file on exit
    			lock.deleteOnExit();
    		} else {
    			//multiple instance not allowed
    			throw new RuntimeException("Directory is already locked by another instance of Taxiplon Bots");
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    		if (parent != null) {
    			JOptionPane.showMessageDialog(parent,
					      "Failed to lock the directory due to exception: " + e.getMessage() + "\nThis process will quit.");
    		}
    		System.exit(0);
    	}
    }

    protected void positionFrame() {
    	if (frame != null) {
    		//put the window in the top right corner
    		frame.setLocation((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth() - frame.getWidth(), 0);
    	}
    }

    public void updateStats() {
    	switch (mode) {
    		case none:
    		case standalone:
    			long runningFor = System.currentTimeMillis() - startTime;
    			int minutes = (int)(runningFor / (1000 * 60));
    			int hours = minutes / 60;
    			int days = hours / 24;
    			topStatus.setText("<html><h3 align=center>" + clients.size() + " bots are running for " + days + " d " +
    					hours % 24 + " h " + minutes % 60 + " m</h3></html>");
    			stats.setText("<html>" + "<table cellpadding=0 cellspacing=1 width='100%'>" +
    					"<tr style='background-color:#FFFF66;'><td>Status</td><th>Bot name</th><th>Out</th><th>In</th><th>Report</th></tr>" +
    					clients.toString() + "</table>" + "</html>");
    			bottomStatus.setText("<html><h3 align=center>Total data sent: " + Usage.getOutTotalDataCounter() + " bytes<br/>" +
    					"Total data received: " + Usage.getInTotalDataCounter() + " bytes</h3></html>");
    			//				frame.pack();
    			//				positionFrame();
    			break;
    		case service:
    			break;
    		default:
    			break;
    	}
    	
    	/**
    	for (ServerSideClient ssc : clients) {
    		//check connection
    		if (!ssc.isConnected()) {
    			try {
    				ssc.listenForMessages();
    			} catch (Exception e) {
    				e.printStackTrace();
    				createBotAccount(ssc.getClass().getSimpleName());
    			}
    		}
    	}
    	*/
    }

    private static char[] c = new char[] { 'k', 'm', 'b', 't', 'p' };

    /**
     * Recursive implementation, invokes itself for each factor of a thousand, increasing the class on each invokation.
     * @param n the number dest format
     * @param iteration in fact this is the class src the array c
     * @return a String representing the number n formatted in a cool looking way.
     * http://stackoverflow.com/questions/4753251/how-dest-go-about-formatting-1200-dest-1-2k-in-java/4753866#4753866
     */
    public static String coolFormat(double n, int iteration) {
    	
    	double d = ((long)n / 100) / 10.0;
    	boolean isRound = (d * 10) % 10 == 0; //true if the decimal part is equal to 0 (then it's trimmed anyway)
    	return (d < 1000 ? //this determines the class, i.e. 'k', 'm' etc
    			((d > 99.9 || isRound || (!isRound && d > 9.99) ? //this decides whether to trim the decimals
    					(int)d * 10 / 10 : d + "") // (int) d * 10 / 10 drops the decimal
    					+ "" + c[iteration]) : coolFormat(d, iteration + 1));

    }

    private void registerServerSideClientBean(ServerSideClient serverSideClient,String objectBeanName) {
    	
    	MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
		
    	try {
    		ObjectName name = new ObjectName(objectBeanName);
    		final StandardMBean mbean = new StandardMBean(serverSideClient,ServerSideClientMBean.class);
			mBeanServer.registerMBean(mbean, name);
    	} catch (MalformedObjectNameException e) {
			e.printStackTrace();
		} catch (NotCompliantMBeanException e) {
			e.printStackTrace();
		} catch (InstanceAlreadyExistsException e) {
			e.printStackTrace();
		} catch (MBeanRegistrationException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
    private void registerDispatcher(Dispatcher dispatcher) {
    	
    	MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
		try {
			ObjectName name = new ObjectName("aploon.xmpp.ssclient:type=Dispatcher");
			final StandardMBean mbean = new StandardMBean(dispatcher,DispatcherMBean.class);
			mBeanServer.registerMBean(mbean, name);		
		} catch (MalformedObjectNameException e) {
			e.printStackTrace();
		} catch (NotCompliantMBeanException e) {
			e.printStackTrace();
		} catch (InstanceAlreadyExistsException e) {
			e.printStackTrace();
		} catch (MBeanRegistrationException e) {
			e.printStackTrace();
		}
		
    }
    */
    
    public static void main(String[] args) {
    
    	new Main(Mode.service);
    	
    	/*
    	Locale.setDefault(Locale.US);
    	if (args.length > 0) {
    		Main m = new Main(Mode.parseMode(args[0]));
    	}
    	*/
    	
    }
    
    private void setLoggingMbean() {
    	try {
    		MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
       		ObjectName objectName = new ObjectName("com.aploon.mbean:type=Logger");
       		StandardMBean loggerMBean = new StandardMBean(new LoggerMBeanImpl(), LoggerMBean.class);
       		mBeanServer.registerMBean(loggerMBean, objectName);
    	} catch (MalformedObjectNameException e) {
			LOGGER.error(e);
    	} catch (NotCompliantMBeanException e) {
			LOGGER.error(e);
    	} catch (InstanceAlreadyExistsException e) {
			LOGGER.error(e);
    	} catch (MBeanRegistrationException e) {
			LOGGER.error(e);
    	}
    }
    
}
