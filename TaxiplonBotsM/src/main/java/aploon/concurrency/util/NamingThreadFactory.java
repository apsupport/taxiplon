package aploon.concurrency.util;

import java.util.concurrent.ThreadFactory;

public class NamingThreadFactory implements ThreadFactory {

	private String threadName;
	
	public NamingThreadFactory(String threadName) {
		this.threadName = threadName;
	}
	
	@Override
	public Thread newThread(Runnable r) {
	
		return new Thread(r,threadName) ;
	}

}
