package aploon.xmpp;

import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import aploon.TerraLocator;
import aploon.Usage;
import aploon.cache.DriverCache;
import aploon.model.Driver;

import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polyline;
import com.taxiplon.Util;
import com.taxiplon.db.DataPool;


abstract public class ServerSideClient implements ServerSideClientMBean {

    public static final String QUERIES_PROPERTIES = "Queries";
    public static final String XML_PROPERTIES = "XMLs";
    public static final String JSON_PROPERTIES = "JSON";
    public static final String DEFAULT_PASSENGER_LOCALE = "en";
    public static final int SLASH_CODE_POINT = Character.codePointAt("/", 0);
    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat(Util.getProperty("time_format", "config"));
    public static final SimpleDateFormat DATE_TIME_FORMAT =
	new SimpleDateFormat(Util.getProperty("date_time_format", "config"));
    public static final boolean DEBUG = Boolean.parseBoolean(Util.getProperty("debug", "config"));

    
    protected static boolean APPLICATION_RUNNING = true;

    protected DataPool dataPool;
    protected TerraLocator locator;
    protected Communicator comm;
    protected HashMap<String, String> request;
    protected String response;
    protected String from;
    protected String caller;
    private String repoPath;

    private static final Logger LOGGER = Logger.getLogger(ServerSideClient.class);
    
    public ServerSideClient() {
    	
    	try {
    		dataPool = DataPool.getInstance("DB");
    	} catch (Exception e) {
    		System.err.println("Failed to instantiate DataPool object due to exception: " + e.getMessage());
    	}
    		locator = new TerraLocator();
    	}

    public void listenForMessages() throws Exception {
    	
    	establishConnection();
    	PacketFilter incomingFilter = new PacketTypeFilter(Message.class);
    	
    	PacketListener requestListener = new PacketListener() {
    		
    		@Override
    		public void processPacket(Packet pack) {
	    	
    			if (pack instanceof Message && pack.getError() == null) {
    				response = "";
    				try {
    					Message msg = (Message)pack;
    					from = msg.getFrom();
    					caller = from.substring(from.indexOf(SLASH_CODE_POINT) + 1);
    					String msgBody = msg.getBody() != null ? URLDecoder.decode(msg.getBody(), "UTF8") : "";
	    			
    					LOGGER.info("Received from "+from+" message "+msgBody);
			
    					if (!from.contains("d1@") && !from.contains("d2@") && !from.contains("d3@")) {
    						//account the traffic stats
    						recordInboundTraffic(msg, from);
    						if (DEBUG) {
    							System.out.println("RCV (" + from + "): " + msgBody);
    						}
    					}
    					if (asynchronousRequestProcessing != null) {
    						getRequests().put(from,msgBody); 
    						//replace older messages from the same sender to this bot with the last message
    					} else {
    						proceedWithRequest(from, msgBody);
    					}
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
    			} 
    			
    		}
    	};
		comm.getConn().addPacketListener(requestListener, incomingFilter);
		
    }

    //can be implemented by a subclass to allow asynchronous processing
    protected Thread asynchronousRequestProcessing = null;

    LinkedHashMap<String, String> requests;

    protected void proceedWithRequest(String from, String msg) {
    	
    	request = new LinkedHashMap<String, String>();
    	String[] requestParts = msg.split("&");
    	
    	for (String r : requestParts) {
    		String[] keyValue = r.split("=");
    		if (keyValue.length == 2) {
    			request.put(keyValue[0] != null ? keyValue[0].trim() : null,keyValue[1] != null ? keyValue[1].trim() : null);
    		}
    	}
    	
    	if(msg!=null&&msg.contains("<action>getDate</action>")) {
    		try {
				comm.sendMessage("<date>"+new Date().getTime()+"</date>", from);
			} catch (XMPPException e) {
				LOGGER.error("Problem sending xmpp message ",e);
			}
    		return;
    	}
    	
    	processRequest();
    	
    	try {
    		if (response != null) {
    			comm.sendMessage(response, from);
    		}
    	} catch (Exception e) {
    		LOGGER.error("Problem sending xmpp message ",e);
		}
    }

    protected void recordInboundTraffic(Message msg, String from) {
    	if (ServerSideClient.DEBUG) {
    		try {
    			Usage.getInstance().recordInboundTraffic(msg.toXML().length(), msg.getTo(), from);
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    }

    abstract public void processRequest();
    abstract public void processIqPacket(IQ iq);
    
    protected void notAllowedToCallDirectly() {
    	throw new RuntimeException("Not allowed to invoke this client.");
    }

    public void establishConnection() throws Exception {

    	LOGGER.debug("The username is "+getUsername()+" The password is "+getPassword());
    	comm = new Communicator(getUsername(), getPassword(), getResource());
    	
    	if (repoPath != null) {
    		comm.initFileTransferManager(repoPath);
    	}
    }

    public String getUsername() {
    	return Communicator.canonicalizeUsername(this.getClass().getSimpleName());
    }

    protected String getPassword() {
    	return getClass().getSimpleName();
    }

    protected String getResource() {
    	return Communicator.XMPP_RESOURCE;
    }

    public boolean isConnected() {
    	return comm != null && comm.getConn().isConnected() && comm.getConn().isAuthenticated();
    }

    public String toString() {
	try {
	    return (isConnected() ? "<td style='color:green;' align=center>ON</td>" :
		    "<td style='color:red'>OFF</td>") +
		//status
		"<td>" + this.getClass().getSimpleName() + "</td>" + //bot name
		(comm != null ?
		 "<td align=right>" + Usage.getInstance().getPersonalOutDataCounter(comm.getConn().getUser()) + "</td>" +
		//out data counter
		"<td align=right>" + Usage.getInstance().getPersonalInDataCounter(comm.getConn().getUser()) + "</td>" :
		//in data counter
		"<td></td><td></td>") + "<td><table cellspacing=0 cellpadding=0>" + getReport() +
			      "</table></td>"; //report;
	} catch (Exception e) {
	    return "";
	}
    }

    protected HashMap<String, String> reports = new HashMap<String, String>() {
	public String toString() {
	    String output = "";
	    for (Map.Entry<String, String> entry : entrySet()) {
		output +=
	   "<tr valign='top'>" + "<td>&nbsp;&nbsp;" + entry.getKey() + "</td>" + "<td>&nbsp;=>&nbsp;</td>" + "<td>" + entry.getValue() +
		       "</td>" + "</tr>";
	    }
	    return output;
	}
    };

    public String getReport() {
	return reports.size() == 0 ? "&nbsp;&nbsp;All cool" : reports.toString();
    }

    public void createXMPPAccount(String username, String password, String email,String description) throws XMPPException {
    	
    	String secretKey = Util.getProperty("openfire_key", "config");
    	String apiUrl = Util.getProperty("openfire_account_api", "config");
    	
    	LOGGER.info("Openfire Rest Request on creating user");
    	
    	AccountAdministrator accountAdministrator = new AccountAdministrator(apiUrl, secretKey);
    	accountAdministrator.createAccount(username, password);
    	
    	//URL url = new URL("http://%s:9090/plugins/userService/userservice?type=add&secret=8yGR28HU&username=kafkas&password=drowssap&name=franz&email=franz@kafka.com&jid=kafkas@driver.taxiplon.com");
    	
    	/**
    	LOGGER.debug("Creating the xmpp account new account");
    	
    	AccountManager accountManager = new AccountManager(comm.getConn());
    	HashMap<String, String> attributes = new HashMap<String, String>();
    	//attributes.put("email", email);
    	attributes.put("text", description);
    	accountManager.createAccount(username, password, attributes);
    	accountManager.createAccount(username, password);
    	
    	LOGGER.debug("Account created");
    	*/
    }

    // UTILS

    protected static String getQuery(String queryName) {
	return Util.getProperty(queryName, QUERIES_PROPERTIES);
    }

    protected static String getXML(String xmlName) {
	return Util.getProperty(xmlName, XML_PROPERTIES);
    }

    protected static String getJSON(String jsonName) {
	return Util.getProperty(jsonName, JSON_PROPERTIES);
    }

    /**
     * Looks up the paramname in the config.properties file and if doesn't find, returns the default value supplied
     * @param paramName
     * @param defaultValue
     * @return
     */
    protected int getNumberFromProps(String paramName, int defaultValue) {
    	try {
    		return Integer.parseInt(Util.getProperty(paramName, "config"));
    	} catch (Exception e) {
    		e.printStackTrace();
    		return defaultValue;
    	}
    }

    /**
     * Sets the number given into the place where all numbers go.
     * @param paramName
     * @param value
     * @return
     */
    protected void setNumberToProps(String paramName, int value) {
	try {
	    Util.setProperty(paramName, Integer.toString(value), "config");
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public static String normalizeTextForXML(String text) {
	return text != null ? text.replaceAll("\\&", "&amp;").replaceAll("<", "&lt;").replace(">", "&gt;").trim() : "";
    }

    protected long getDistance(Point start, Point end) {
	Polyline line = new Polyline();
	line.startPath(start);
	line.lineTo(end);
	return (long)line.calculateLength2D();
    }

    /**
     * As optional fields can be left empty during a request, this method provides the framework with a generic way of obtaining a typed version of that request param.
     * @param <T>
     * @param paramName Name of the optional parameter in the request Map.
     * @param type Class to type the output to.
     * @return Typed value of the param, or null if the param value was not provided by the request.
     */
    protected <T> T parseOptionalField(String paramName, Class<T> type) {
    	T param = null;
    	String paramStr = request.get(paramName);
    	if (paramStr != null) {
    		//sanitize
    		paramStr = paramStr.replace(',', '.');
    		//and parse
    		try {
    			//try to build an instance with a constructor (boxed primitive types)
    			Constructor init = type.getConstructor(String.class);
    			param = (T)init.newInstance(paramStr); //this mostly handles the case when a float comes with a comma
    		} catch (Exception ie) {
    			try {
    				//if constructor failed, try to find the valueOf method (enum's)
    				Method valueOf = type.getMethod("valueOf", String.class);
    				param = (T)valueOf.invoke(type, paramStr);
    			} catch (Exception e) {
    				ie.printStackTrace();
    				e.printStackTrace();
    			}
    		}
    	}
    	
    	return param;
    }

    protected String sanitize(String s) {
    	if (s == null) {
    		return "NULL";
    	}
    	return "'" + s.replaceAll("'", "\\'") + "'"; //sanitize
    }

    public void setRequests(LinkedHashMap<String, String> requests) {
    	this.requests = requests;
    }

    public LinkedHashMap<String, String> substituteRequests() {
    	LinkedHashMap<String, String> oldRequests = requests;
    	requests = new LinkedHashMap<String, String>();
    	return oldRequests;
    }

    public LinkedHashMap<String, String> getRequests() {
    	if (requests == null) {
    		requests = new LinkedHashMap<String, String>();
    	}
    	return requests;
    }

    protected String getCaller() {
    	return caller;
    }

    protected boolean isMobileCaller() {
    	return caller != null && (caller.toLowerCase().equals("android") || caller.toLowerCase().equals("iphone"));
    }

    protected void setRepoPath(String path) {
    	repoPath = path;
    }

    protected String getRepoPath() {
    	return repoPath;
    }

    protected String getEmptyForNull(ResultSet rs, String field) throws Exception {
    	String tmp = rs.getString(field);
    	return tmp == null ? "" : tmp;
    }

    public static String serializeObject(Object object) throws Exception {
    	
    	Serializer serializer = new Persister();
    	StringWriter stringWriter = new StringWriter();
    	serializer.write(object, stringWriter);
    	
    	return stringWriter.toString();
    }

	@Override
	public void openConnection() {
		try {
			try {
				LOGGER.info("Opening xmpp connection "+getUsername()+" "+getResource());
				listenForMessages();
			} catch (XMPPException e) {
				LOGGER.error("Problem with xmpp connection",e);
			}
		} catch(Exception e) {
			LOGGER.error("Listen for messages exception ",e);
		}
    	
	}

	@Override
	public void closeConnection() {
		try {
			LOGGER.info("Closing xmpp connection");
			comm.destroy();
		} catch (Exception e) {
			LOGGER.error("Problem closing connection ",e);
		}
	}
    
}
