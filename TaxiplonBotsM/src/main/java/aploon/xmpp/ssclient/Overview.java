package aploon.xmpp.ssclient;


import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;

import org.jivesoftware.smack.packet.IQ;


public class Overview extends ServerSideClient {
	
    public static final int DEFAULT_PRECISION = 4;

    private NamedParameterStatement percentageOverviewPerDeviceStmt;

    public Overview() throws Exception {
        super();
    }

    private void prepareOverviewPerDeviceStmt() throws Exception {
        if (percentageOverviewPerDeviceStmt == null || percentageOverviewPerDeviceStmt.isClosed()) {
            percentageOverviewPerDeviceStmt =
                    new NamedParameterStatement(dataPool, getQuery("percentageOverviewPerDevice"));
        }
    }

    public void processRequest() {
        try {
            Long deviceId = Long.parseLong(request.get("deviceid"));
            String interval = request.get("int");
            Integer precision = parseOptionalField("prec", Integer.class);
			Boolean getTotal = parseOptionalField("total", Boolean.class);
			if(getTotal != null && getTotal) {
				response = getOverviewTotalByDeviceId(deviceId, Integer.parseInt(interval), precision);
			}
			else {
				response = getOverviewByDeviceId(deviceId, interval, precision == null ? DEFAULT_PRECISION : precision);
			}
        } catch (Exception e) {
            e.printStackTrace();
            response = "";
        }
    }

    private String getOverviewByDeviceId(Long deviceId, String interval, int precision) throws Exception {
        prepareOverviewPerDeviceStmt();
        percentageOverviewPerDeviceStmt.setLong("deviceId", deviceId);
        percentageOverviewPerDeviceStmt.setInt("precision", precision);
        percentageOverviewPerDeviceStmt.setString("interval",
                                                  interval == null ? "10 0:0:0.000000" : interval); //10 days is default
        ResultSet rs = percentageOverviewPerDeviceStmt.executeQuery();
        String calls = "";
        while (rs.next()) {
            Long amount = rs.getLong("amount");
            Long total = rs.getLong("total");
            Double percentage = rs.getDouble("pct");
            Double lat = rs.getDouble("arealat");
            Double lng = rs.getDouble("arealng");
            calls += String.format(getXML("OVERVIEW_STAT_INFO"), amount, total, percentage, lat, lng) + "\n";
        }
        rs.close();
        dataPool.closeConnection();
        return String.format(getXML("OVERVIEW_HEAT_WRAPPER"), calls);
    }

    private String getOverviewTotalByDeviceId(Long deviceId, int interval, int precision) throws Exception {
        NamedParameterStatement stmt = new NamedParameterStatement(dataPool, getQuery("percentageOverviewTotalPerDevice"));
        stmt.setLong("deviceId", deviceId);
        stmt.setInt("precision", precision);
        stmt.setInt("interval", interval);
        ResultSet rs = stmt.executeQuery();
        String calls = "";
        while (rs.next()) {
            Long amount = rs.getLong("amount");
            Long total = rs.getLong("total");
            Double percentage = rs.getDouble("pct");
            Double lat = rs.getDouble("arealat");
            Double lng = rs.getDouble("arealng");
            calls += String.format(getXML("OVERVIEW_STAT_INFO"), amount, total, percentage, lat, lng) + "\n";
        }
        rs.close();
        dataPool.closeConnection();
        return String.format(getXML("OVERVIEW_HEAT_WRAPPER"), calls);
    }

    protected String getPassword() {
        return getClass().getSimpleName();
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
