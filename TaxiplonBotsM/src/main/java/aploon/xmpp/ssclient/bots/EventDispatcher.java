package aploon.xmpp.ssclient.bots;

import aploon.Main;

import com.taxiplon.Util;

import aploon.model.Coordinates;

import aploon.xmpp.ssclient.Hotspots;
import aploon.xmpp.ssclient.poi.ListHotspots;

import com.taxiplon.event.Event;
import com.taxiplon.event.OpenCalendar;

import gr.datamation.athcrawl.entities.AthinoramaInfo;
import gr.datamation.athcrawl.parser.AthinoramaParser;

import gr.datamation.dataloader.entities.OpenCalendarInfo;
import gr.datamation.dataloader.parser.OpenCalendarParser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.List;

public class EventDispatcher extends Dispatcher {

	static final int DISPATCHER_DEFAULT_SLEEP_INTERVAL = 5; //once in 5 minutes
	static final int ATHINORAMA_DEFAULT_SLEEP_TIME_MIN = 60 * 24; //once a day
	static final int OPENCAL_DEFAULT_SLEEP_TIME_MIN = 60 * 24;
	static final int JANITOR_DEFAULT_SLEEP_TIME_MIN = 60 * 24;
	int dispatcherSleepTimeMin = 0;
	int janitorSleepTimeMin = 0;
	int athinoramaSleepTimeMin = 0;
	int opencalSleepTimeMin = 0;
	private ArrayList<TaxiCompany> companies;
	private PreparedStatement getPopulousCompaniesStmt;
	private PreparedStatement driversPerCompany;
	
	private static EventDispatcher instance;
	
	public static synchronized EventDispatcher getInstance() throws Exception {
		if(instance == null) {
			instance = new EventDispatcher();
		}
		return instance;
	}

	private EventDispatcher() throws Exception {
		super();
	    new Janitor().start();
		Thread athino= new AthinoramaCrawler();//.start();
		athino.setName("athinorama_scheduler");
		athino.start();
		
		Thread opencalendar = new OpenCalendarCrawler();//.start();
		opencalendar.setName("opencalender_schedukler");
		opencalendar.start();
		
		/*
		 * 12/09/2011 made hotspot distribution available on demand only
 		//TODO make this scalable as well: as is right now, the initial populous companies will be the only ones to receive updates
		companies = new ArrayList<TaxiCompany>();
		getPopulousCompaniesStmt = dataPool.prepareStatement(getQuery("getPopulousCompanies"));//this doesn't need to be executed more than once at strtup
		driversPerCompany = dataPool.prepareStatement(getQuery("driversPerCompany"));
		ResultSet rs = getPopulousCompaniesStmt.executeQuery();
		while(rs.next()) {
			long id = rs.getLong("taxicompanyid");
			new Thread(new EventDispatcherPerCompany(new TaxiCompany(id))).start();
		}
		getPopulousCompaniesStmt.close();
		rs.close();
		dataPool.closeConnection();
		 */
	}
	
	public int sendXMLToAllDrivers(long taxiCompanyId, String eventsXML) throws Exception {
		int sentToDrivers = 0;
	    //figure out the drivers
	    driversPerCompany.setLong(1, taxiCompanyId);
	    ResultSet rs = driversPerCompany.executeQuery();
	    while(rs.next()) {
	        Long deviceId = rs.getLong("deviceid");
	        String deviceImei = rs.getString("imei");
	        comm.sendMessageToDriver(eventsXML, deviceImei);
			sentToDrivers++;
	    }
	    rs.close();
		return sentToDrivers;
	}

	/**
	 * The purpose of this thread is dest periodically crawl the athinorama site and store some data into the table, the dispatcher will find them occasionally and dispatch when the time is right.
	 */
	class Janitor extends Thread {
		public void run() {
		    boolean cleanOrNot = Main.RUNNING;
			while(cleanOrNot) {
				janitorSleepTimeMin = getNumberFromProps("janitor_sleep_time", JANITOR_DEFAULT_SLEEP_TIME_MIN);
				//double check whether should crawl or not
			    cleanOrNot = janitorSleepTimeMin > -1? Main.RUNNING : false;
				if(cleanOrNot) {
					try { //try to sleep
						String report = "";
						try { //try to do the actual work
							int rowsAffected = dataPool.executeUpdate(getQuery("cleanUpOldHotspots"));
							report = "Deleted rows: " + rowsAffected;
						}
						catch(Exception e) {
							janitorSleepTimeMin = 1;//retry in one minute
							e.printStackTrace();
						}
						reports.put(getClass().getSimpleName(),
									report + 
									"<br/>Next clean up on " +
									DATE_TIME_FORMAT.format(new Timestamp(System.currentTimeMillis() + janitorSleepTimeMin * 60 * 1000)));
						Thread.sleep(janitorSleepTimeMin * 60 * 1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * The purpose of this thread is dest periodically crawl the athinorama site and store some data into the table, the dispatcher will find them occasionally and dispatch when the time is right.
	 */
	class AthinoramaCrawler extends Thread {
		public void run() {
		    boolean crawl = Main.RUNNING;
			while(crawl) {
				athinoramaSleepTimeMin = getNumberFromProps("athinorama_update_delay", ATHINORAMA_DEFAULT_SLEEP_TIME_MIN);
				//double check whether should crawl or not
			    crawl = athinoramaSleepTimeMin > -1? Main.RUNNING : false;
				if(crawl) {
					try { //try to sleep
						String report = "";
						int counter = 0;
						try { //try to do the actual work
							//get parsed contents
							AthinoramaParser parser = new AthinoramaParser();
							List<AthinoramaInfo> list = parser.parsePage();
							counter = list.size();
							//store them into the DB
							for(AthinoramaInfo ai : list) {
							    try { //try to fetch coordinates
									String address = ai.getAddressOfEvent();
									//events for the city of athens, if area part is absent, add the word Athens
									Coordinates coord = locator.getCoordinatesByAddress(address != null && address.split(",").length == 1? address + ", ����������" : address);
									Hotspots.getInstance().createOrUpdateHotspot(null,
																				 ai.getEventStartDateTime().getTimeInMillis(),
																				 ai.getEventEndDateTime() != null ? ai.getEventEndDateTime().getTimeInMillis() : null, 
																				 null, coord.getLat(), coord.getLng(), null,
																				 ai.getPlaceOfEvent() + ": " + ai.getNameOfEvent(),
																				 Hotspots.EventTypes.music, ai.getAddressOfEvent(),
																				 "Athinorama");
								}
								catch(Exception e) {
									counter--;
									String error = "For address " + ai.getAddressOfEvent() + " locator said: " + e.getMessage();
									report += error + "<br/>";
									System.err.println(error);
								}
							}
						}
						catch(Exception e) {
							athinoramaSleepTimeMin = 1;//retry in one minute
							e.printStackTrace();
						}
						reports.put(getClass().getSimpleName(),
									report + "In total " + counter + " entries were successfully parsed" +
									"<br/>Next dispatch on " +
									DATE_TIME_FORMAT.format(new Timestamp(System.currentTimeMillis() + athinoramaSleepTimeMin * 60 * 1000)));
						Thread.sleep(athinoramaSleepTimeMin * 60 * 1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * The purpose of this thread is dest periodically crawl the opencalendar.gr site and store some data into the table, the dispatcher will find them occasionally and dispatch when the time is right
	 */
	class OpenCalendarCrawler extends Thread {
		public void run() {
		    boolean crawl = Main.RUNNING;
			while(crawl) {
				opencalSleepTimeMin = getNumberFromProps("opencal_update_delay", OPENCAL_DEFAULT_SLEEP_TIME_MIN);
				crawl = opencalSleepTimeMin > -1? Main.RUNNING : false;
				if(crawl) {
					try { //try to sleep
						int counter = 0;
						int excluded = 0;
						try { //try to do the actual work
							//get parsed contents
							OpenCalendar parser = new OpenCalendar();
							List<Event> list = parser.fetchEvents();
							counter = list.size();
							//store them into the DB
							for(Event oci : list) {
								try {
								    //try to fetch coordinates
									if(oci.getLat() == null || oci.getLng() == null) {
									    String address = oci.getAddressOfEvent();
										//events for the city of athens, if area part is absent, add the word Athens
										Coordinates coord = locator.getCoordinatesByAddress(address != null && address.split(",").length == 1? address + ", ����������" : address);
									    oci.setLat(coord.getLat());
										oci.setLng(coord.getLng());
									}
									//try to parse the event type by attempting to parse the value of type and, if type fails, category
									Hotspots.EventTypes eventType;
									try {
										eventType = Hotspots.EventTypes.valueOf(oci.getEventType());
									}
									catch(Exception e) {
										try {
										    eventType = Hotspots.EventTypes.valueOf(oci.getEventCategory());
										}
										catch(Exception ex) {
										    eventType = Hotspots.EventTypes.other;
											System.err.println("Category and type unparsable: cat " + oci.getEventCategory() + ", type " + oci.getEventType());
										}
									}
									Hotspots.getInstance().createOrUpdateHotspot(null,
																				 oci.getEventStartDateTime() != null ? oci.getEventStartDateTime().getTime() : null,
																				 oci.getEventEndDateTime() != null ? oci.getEventEndDateTime().getTime() : null, 
																				 oci.getEventDateTimeString(),
																				 oci.getLat(), oci.getLng(), null,
																				 oci.getNameOfEvent(),
																				 eventType, oci.getAddressOfEvent(),
																				 oci.getClass().getSimpleName());
								}
								catch(Exception e) {
									counter--;
									excluded++;
									System.err.println("Excluded event because: " + e.getMessage());
								}
							}
						}
						catch(Exception e) {
							opencalSleepTimeMin = 1;//retry in one minute
							e.printStackTrace();
						}
						reports.put(getClass().getSimpleName(),
									"In total " + counter + " entries were successfully parsed, " + excluded +
									" were excluded" + "<br/>Next dispatch on " +
									DATE_TIME_FORMAT.format(new Timestamp(System.currentTimeMillis() + opencalSleepTimeMin * 60 * 1000)));
						Thread.sleep(opencalSleepTimeMin * 60 * 1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * @deprecated hotspots are not sent on the periodic basis anymore, devices should request.
	 */
	@Deprecated
	class EventDispatcherPerCompany implements Runnable {

		TaxiCompany tc;

		EventDispatcherPerCompany(TaxiCompany tc) {
			this.tc = tc;
		}

		public void run() {
			boolean working = Main.RUNNING;
			while(working) {
				dispatcherSleepTimeMin = getNumberFromProps("event_update_delay", DISPATCHER_DEFAULT_SLEEP_INTERVAL);
			    working = dispatcherSleepTimeMin > -1 ? Main.RUNNING : false;
				if(working) {
					try { //try to sleep
						int sentToDrivers = 0;
						try {
							if(comm != null) {
								String eventsXML = String.format(getXML("HOTSPOTS_WRAPPER_XML"), 
																 ListHotspots.getInstance().getHotspots(System.currentTimeMillis(), null, false, null, null));
								sentToDrivers = sendXMLToAllDrivers(tc.getId(), eventsXML);
							}
						}
						catch(Exception e) {
							e.printStackTrace();
						}
						long sleepTime = dispatcherSleepTimeMin * 60 * 1000;
						reports.put(getClass().getSimpleName(),
									"Hotspots, live pharmacies and hospitals were sent to " + sentToDrivers + " drivers." +
									"<br/>Next dispatch on " +
									DATE_TIME_FORMAT.format(new Timestamp(System.currentTimeMillis() + sleepTime)));
						Thread.sleep(sleepTime);
					}
					catch(Exception e) {
						e.printStackTrace();
						System.err.println("Event dispatcher thread was interrupted");
					}
				}
			}
		}
	}

	class TaxiCompany {

		long id;

		TaxiCompany(long id) {
			this.id = id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public long getId() {
			return id;
		}
	}
}
