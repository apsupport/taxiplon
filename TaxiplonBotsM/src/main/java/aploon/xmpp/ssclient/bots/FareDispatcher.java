package aploon.xmpp.ssclient.bots;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;

import aploon.Main;
import aploon.concurrency.util.NamingThreadFactory;
import aploon.datahelper.DataHelper;
import aploon.datahelper.DatasourceProvider;
import aploon.model.BiddingTime;
import aploon.model.CallType;
import aploon.model.Driver;
import aploon.model.TripData;
import aploon.model.Zone;
import aploon.offer.OfferProvider;
import aploon.sms.SMSSUtil;
import aploon.xml.model.CallInfoXML;
import aploon.xmpp.ssclient.Dispatcher;
import aploon.xmpp.ssclient.Zones;
import aploon.xmpp.ssclient.iphone.Push;
import aploon.xmpp.ssclient.iphone.PushSender;

import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;

/*
 * Is about call management from everyware to driver.
 */
public class FareDispatcher extends aploon.xmpp.ssclient.bots.Dispatcher {

    public static final int DEFAULT_SLEEP_TIME_SEC = 1; //time to sleep before next fare dispatch

    int sleepTimeSec = 0;
    private Map<Long, BiddingTime> tripsToDispatch;
    private Map<Long, BiddingTime> processingTrips;
    private Map<Long, BiddingTime> lastChanceTrips;
    private PreparedStatement restoreTripsToDispatchStmt;
    
    private NamedParameterStatement findDriversForTripStmt;
    private CallableStatement assignTripToDeviceStmt;
    private CallableStatement markTripAsExpiredStmt;
    private PreparedStatement fetchPassengerInfoForTripStmt;
    
    private Executor dispatchingExecutor; 
    private static FareDispatcher instance;

    private static final Logger LOGGER = Logger.getLogger(FareDispatcher.class);
    
    public static synchronized FareDispatcher getInstance() throws Exception {
    	if (instance == null) {
    		instance = new FareDispatcher();
    	}
    	return instance;
    }

    public boolean isLastChanceForTripId(Long tripId) {
    	return getLastChanceTrips().containsKey(tripId);
    }

    private FareDispatcher() throws Exception {
    	super();
    
    	initializeExecutor();
    	
    	tripsToDispatch = new ConcurrentHashMap<Long, BiddingTime>();
    	processingTrips = new ConcurrentHashMap<Long, BiddingTime>();
    	lastChanceTrips = new ConcurrentHashMap<Long, BiddingTime>();
    	try {
    		restoreTripsToDispatch();
    	} catch (Exception e) {
    		System.err.println("Failed to restore the trips to dispatch due to exception: " + e.getMessage());
    	}
    	sleepTimeSec = getNumberFromProps("fare_disp_interval_sec", DEFAULT_SLEEP_TIME_SEC);
    	//thread for dispatching trips *** THE ACTUAL DISPATCHING IS DONE HERE - WAVE 1
    	new Thread(new Runnable() {
    		public void run() {
    			while (Main.RUNNING) {
    				try {
    					Thread.sleep(sleepTimeSec * 1000);
    					if (comm != null) {
    						Iterator<Long> keys = getProcessingTrips().keySet().iterator();
    						while (!getProcessingTrips().isEmpty()) {
    							Long tripId = keys.next();

    							try {
    								
    								/*
    								if(!transactionalAssignment(tripId)) {
    									getLastChanceTrips().put(tripId, getProcessingTrips().get(tripId));
    								}
    								*/
    								
    								processAssignment(tripId,getProcessingTrips().get(tripId));
    								getProcessingTrips().remove(tripId);
    								
    								//used to be, but THIS CAN CAUSE ConcurrentModificationException
    								//keys.remove(); //TODO CHECK THIS WHETHER IT WORKS AT ALL (INTENDED TO FIX THE ConcurrentModificationException)
    							} catch (Exception er) {
    								System.out.println("***MAJOR dispatch error " + tripId + " " + er.getStackTrace());
    								Thread.sleep(1000);
    								getProcessingTrips().remove(tripId);
    							}
    						}
    					}
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
    			}
    		}
    	},"assignment_scheduler").start();
    	
    	//thread for last chance dispatching *** REMOVES TRIPS FROM THE LAST CHANCE LIST, MARKS THEM AS EXPIRED AND INFORMS THE PASSENGER THAT HE'S SCREWED
    	new Thread(new Runnable() {
    		public void run() {
    			while (Main.RUNNING) {
    				try {
    					Thread.sleep(sleepTimeSec * 1000);
    					if (comm != null) {
    						long currentTime = System.currentTimeMillis();
    						for (Map.Entry<Long, BiddingTime> entry : getLastChanceTrips().entrySet()) {
    							if (entry.getValue().getWaitTime() < currentTime) {
    								expireTrip(entry.getKey(),entry.getValue().isDispatched() ? "no_interest" : "no_dispatch");
    								getLastChanceTrips().remove(entry.getKey());
    							}
    						}
    					}
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
    			}
    		}
    	},"expiring_scheduler").start();
	
    	//thread finding trips for which the bidding time is over
    	new Thread(new Runnable() {
    		public void run() {
    			while (Main.RUNNING) {
    				try {
    					if (comm != null) {
    						long currentTime = System.currentTimeMillis();
    						//traverse the whole hashmap with trips to dispatch to check whether their time is up and if so, mark these entries as pending process by moving them to the processingtrips list
    						for (Map.Entry<Long, BiddingTime> entry : getTripsToDispatch().entrySet()) {
    							if (entry.getValue().getBiddingTime() < currentTime) {
    								getProcessingTrips().put(entry.getKey(), entry.getValue());
    							}
    						}
    						//remove trips marked for process
    						for (Long tripId : getProcessingTrips().keySet()) {
    							getTripsToDispatch().remove(tripId);
    						}
    					}
    					Thread.sleep(sleepTimeSec * 1000);
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
    			}
    		}
    	},"waitingtrips_scheduler").start();
    }

    private void initializeExecutor() {
    	String processesStr = Util.getProperty("trip_dispatching_processes", "processes");
    	int processes = 1;
    	if(processesStr!=null) {
    		processes = Integer.parseInt(processesStr);	
    	}
    	
    	LOGGER.info("The processes are "+processes);
    	
    	dispatchingExecutor = Executors.newFixedThreadPool(processes,new NamingThreadFactory("assignment_pool"));    	
    }
    
    private void prepareRestoreTripsToDispatchStmt() throws Exception {
    	if (restoreTripsToDispatchStmt == null || restoreTripsToDispatchStmt.isClosed()) {
    		restoreTripsToDispatchStmt = dataPool.prepareStatement(getQuery("restoreTripsToDispatch"));
    	}
    }

    /**
    private void prepareFetchTripDataStmt() throws Exception {
    	if (fetchTripDataStmt == null || fetchTripDataStmt.isClosed()) {
    		fetchTripDataStmt = dataPool.prepareStatement(getQuery("fetchTripData"));
    	}
    }
    */

    private void prepareFindDriversForTripStmt() throws Exception {
    	if (findDriversForTripStmt == null || findDriversForTripStmt.isClosed()) {
    		findDriversForTripStmt = new NamedParameterStatement(dataPool, getQuery("findDriversForTrip"));
    	}
    }

    private void prepareAssignTripToDeviceStmt() throws Exception {
    	if (assignTripToDeviceStmt == null || assignTripToDeviceStmt.isClosed()) {
    		assignTripToDeviceStmt = dataPool.getConnection().prepareCall(getQuery("assignTripToDevice"));
    	}
    }

    private void prepareMarkTripAsExpiredStmt() throws Exception {
    	if (markTripAsExpiredStmt == null || markTripAsExpiredStmt.isClosed()) {
    		markTripAsExpiredStmt = dataPool.getConnection().prepareCall(getQuery("markTripAsExpired"));
    	}
    }

    private void prepareFetchPassengerInfoForTripStmt() throws Exception {
    	if (fetchPassengerInfoForTripStmt == null || fetchPassengerInfoForTripStmt.isClosed()) {
    		fetchPassengerInfoForTripStmt = dataPool.prepareStatement(getQuery("fetchPassengerInfoForTrip"));
    	}
    }

    public void addTrip(Long tripId, BiddingTime biddingTime) {
    	getTripsToDispatch().put(tripId, biddingTime);
    }

    public BiddingTime getTripBiddingTime(Long tripId, Long currentTimeMillis) {
    	
    	BiddingTime tripBiddingTime = getTripsToDispatch().get(tripId);
    	//it might be not in the tripsToDispatch hashmap
    	if (tripBiddingTime == null) {
    		tripBiddingTime = getProcessingTrips().get(tripId);
    		if (tripBiddingTime == null) {
    			tripBiddingTime = getLastChanceTrips().get(tripId);
    			if (tripBiddingTime == null) {
    				tripBiddingTime = new BiddingTime(0, 0, 0L, false);
    			}
    		}
    	}
    
    	return tripBiddingTime;
    
    }

    /**
     * In the case of a server restart, this method will restore non expired trips.
     * @throws Exception
     */
    private void restoreTripsToDispatch() throws Exception {
    	prepareRestoreTripsToDispatchStmt();
    	ResultSet rs = restoreTripsToDispatchStmt.executeQuery();
    	while (rs.next()) {
    		try {
    			boolean dispatched = true;
    			try {
    				dispatched = rs.getBoolean("dispatched");
    			} catch (Exception ignored) {
    			}
    			addTrip(rs.getLong("tripid"),
    					new BiddingTime(rs.getInt("clearbiddingtime"), rs.getInt("requestwaittime"), rs.getLong("hailtime"),dispatched));
    		} catch (Exception e) {
    			System.err.println("Failed to add a trip during tripsToDispatch restore due to exception: " +e.getMessage());
    		}
    	}
    	rs.close();
    	dataPool.closeConnection();
    }

    public void cancelTrip(Long tripId) {
    	getProcessingTrips().remove(tripId);
    	getTripsToDispatch().remove(tripId);
    	getLastChanceTrips().remove(tripId);
    }

    public TripData fetchTripData(Long tripId) throws Exception {
    	
    	Connection connection = null;
    	PreparedStatement fetchTripDataStmt = null;
    	
    	//dataPool.prepareStatement(getQuery("fetchTripData"));
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		
    		TripData fetchedData = new TripData();
    	
    		fetchTripDataStmt = connection.prepareStatement(getQuery("fetchTripData"));
    		fetchTripDataStmt.setLong(1, tripId);
    	
    		//fetch trip data
    	
    		ResultSet tripDataRS = fetchTripDataStmt.executeQuery();
    	
    		if (tripDataRS.next()) { //single row result
    		
    			Long hailtime = tripDataRS.getTimestamp("hailtime").getTime();
    		
    			Double lat = tripDataRS.getDouble("pickuplat");
    			Double lng = tripDataRS.getDouble("pickuplong");
    			String gaddress = tripDataRS.getString("pickupaddress");
    			String special = tripDataRS.getString("specialneeds");
    			String status = tripDataRS.getString("status");
    			String destination = tripDataRS.getString("destination");
    			String passengerGender = tripDataRS.getString("gender");
    			String passengerName = tripDataRS.getString("name");
    			String passengerMobile = tripDataRS.getString("mobile");
    			Long passengerId = tripDataRS.getLong("passengerid");
    			/* TODO AFTER THE CAMPAIGN ENDS, MODIFY THIS CODE TO ALWAYS SET isDiscounted TO FALSE */
    			boolean isDiscounted = tripDataRS.getLong("discountrides") > 0;
    			String locale = tripDataRS.getString("locale");
    			String extra = tripDataRS.getString("extra");
    			String softver = tripDataRS.getString("softwareversion");
    			Long taxiCompanyid = tripDataRS.getLong("taxicompanyid");
    			String companyName = tripDataRS.getString("companyname");
    			Double fare = tripDataRS.getDouble("fare");
    			String paymentType = tripDataRS.getString("paymenttype");
    			String bookagent = tripDataRS.getString("bookagent");
    			String origin = tripDataRS.getString("origincountry");
    			String conPhone = tripDataRS.getString("contactPhone");
    			//format the XML message
        
    			CallType callType = DataHelper.getTripCallType(tripId);
    			
    			CallInfoXML callInfoXML = new CallInfoXML();
    			callInfoXML.setLat(lat);
    			callInfoXML.setLng(lng);
    			callInfoXML.setGaddress(gaddress);
    			callInfoXML.setHailTime(hailtime);
    			callInfoXML.setSpecial(special);
    			callInfoXML.setStatus(status);
    			callInfoXML.setName(passengerName);
    			callInfoXML.setDestination(destination);
    			callInfoXML.setGender(passengerGender);
    			callInfoXML.setMobile(passengerMobile);
    			callInfoXML.setRemarks(isDiscounted ? "-" + Util.getProperty("discount_max_amount", "lang") :null);
    			callInfoXML.setFare(fare);
    			callInfoXML.setPaymenttype(paymentType);
    			callInfoXML.setCallTypeName(callType.getName());
    			callInfoXML.setClearBiddingTime(callType.getClearBiddingTime());
    			callInfoXML.setRequestWaitTime(callType.getRequestWaitTime());
    			
    			String assignmentMsg = 
    					String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.place, tripId,serializeObject(callInfoXML));
    				
    			/**
    			String.format(getXML("CALL_INFO"),
    							lat, lng, gaddress != null ? gaddress : "", "", hailtime, special != null ? special : "",
    									status != null ? status : "", passengerName != null ? passengerName : "",
    											destination != null ? destination : "", passengerGender != null ? passengerGender : "",
    													passengerMobile != null ? passengerMobile : "",
    														isDiscounted ? "-" + Util.getProperty("discount_max_amount", "lang") :"",
    																//HERE MODIFY THE MESSAGE SEEN BY THE DRIVER (IN THE TOP RIGHT CORNER, CAMPAIGN DEPENDENT)
    																companyName != null ? companyName : "", fare, paymentType)+"<callTypeName>"+DataHelper.getTripCallType(tripId).getName()+"</callTypeName>");
    			*/
    																
    			fetchedData.setPassengerCountry(origin);
    			fetchedData.setMobile(passengerMobile);
    			fetchedData.setContactPhone(conPhone);
    			fetchedData.setAssignmentMsg(assignmentMsg);
    			fetchedData.setStatus(status);
    			fetchedData.setGaddress(gaddress);
    			fetchedData.setPassengerId(passengerId);
    			fetchedData.setIsDiscounted(isDiscounted);
    			fetchedData.setLocale(locale);
    			fetchedData.setBookagent(bookagent);
    			fetchedData.setTaxicompanyId(taxiCompanyid);
    			fetchedData.setLat(lat);
    			fetchedData.setLng(lng);
    			fetchedData.setHailTime(hailtime);
    			fetchedData.setSpecial(special);
    			fetchedData.setPassengerName(passengerName);
    			fetchedData.setDestination(destination);
    			fetchedData.setGender(passengerGender);
    			fetchedData.setCompanyName(companyName);
    			fetchedData.setFare(fare);
    			fetchedData.setPaymentType(paymentType);
	    
    			fetchedData.setExtra(extra == null ? null : extra + "@" + softver);
            
    		}
    		tripDataRS.close();
    		return fetchedData;
    	} finally {
    		if(fetchTripDataStmt!=null) fetchTripDataStmt.close();
    		if(connection!=null) connection.close();
    	}
    }
    
    /**
     * Process Assignment and sent it to the executor service
     * @param tripId
     * @param biddingTime
     */
    private void processAssignment(final long tripId,final BiddingTime biddingTime) {
    	
    	dispatchingExecutor.execute(new Runnable() {
			
			@Override
			public void run() {
				
				LOGGER.info("Dispatching thread id: "+Thread.currentThread().getId());
				
				if(!callTypeAssignment(tripId)) {
					getLastChanceTrips().put(tripId, biddingTime);
				}
				
				/*
				if(!transactionalAssignment(tripId)) {
					getLastChanceTrips().put(tripId, biddingTime);
				}
				*/
				
			}
			
		});
    }
    
    public boolean callTypeAssignment(long tripId) {
    	
    	boolean outcome = false;
    
    	CallType callType = null;
    	
    	try {
			callType = DataHelper.getTripCallType(tripId);
		} catch (SQLException e) {
			LOGGER.error("Problem fetching trip calltype ",e);
		}
    	
    	if(callType!=null) {
    		if(callType.getForceAssignment()) {
    			outcome = transactionalCallTypeForceAssignment(tripId,callType);
    		} else {
    			outcome = transactionalCallTypeAssignment(tripId,callType);
    		}
    	} else {
    		outcome = transactionalAssignment(tripId);
    	}
    	
    	
    	return outcome;
    }
    
    public boolean transactionalCallTypeForceAssignment(long tripId,CallType callType) {
    	
    	boolean outcome = false;
    	
    	try {
    		Long deviceId = DataHelper.transactionalForceCallTypeAssignment(tripId,callType);
    		
    		LOGGER.info("Transactional assignment finised tripid: "+tripId);
    		
			if(deviceId!=null) {
				try {
					TripData fetchedData = fetchTripData(tripId);
					String passengerInformXML = null;
					String discountMsg = getDiscountMessage(fetchedData);
					Driver driver = DataHelper.driverFromDeviceAndTrip(deviceId, tripId);
					
					if(comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), driver.getDevice().getImei())) {
						
						if(driver.getDevice().getExtra()!=null) {
    						try {
    							String messageDrId = "PassengerAssign";
                                String localizedMessageToDriver = Util.getProperty(messageDrId + "_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                                String localizedTitleToDriver = Util.getProperty(messageDrId + "_title_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE),"lang");
                                PushSender.getInstanceDriver(driver.getBrandName()).sendPushNotification(driver.getDevice().getExtra(), localizedMessageToDriver, messageDrId,localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));
    						} catch (Exception ex) {
    							LOGGER.error("Error sending push",ex);
    						}
    					}
    					
    					outcome = true;
    					
    					String assignedDriverInfo = null;
    					
    					if (fetchedData.getBookagent() == null ||(!fetchedData.getBookagent().equals("w"))) {
    						assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_XML"), deviceId, driver.getDevice().getCurrentLat(),
    								driver.getDevice().getCurrentLng(), driver.getDevice().getVehicle().getPlateno(), driver.getFirstName() + " " +
                                            driver.getLastName(), driver.getMobile(), driver.getDevice().getVehicle().getModel(), fetchedData.getGaddress(), driver.getDistance());
    					} else {
                            assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_MULTICALL_XML"), tripId, deviceId, driver.getDevice().getCurrentLat(),
                            		driver.getDevice().getCurrentLng(), driver.getDevice().getVehicle().getPlateno(),driver.getFirstName() + " " + 
                            				driver.getLastName(), driver.getMobile(), driver.getDevice().getVehicle().getModel(), fetchedData.getGaddress(), driver.getDistance());
    					}
    					
    					passengerInformXML = String.format(getXML("PASSENGER_INFORM"), assignedDriverInfo, discountMsg);
                        comm.sendMessageToPassenger(passengerInformXML, fetchedData.getPassengerId());
                        //send a push notification to the passenger for any driver's action
                        //and only if the call was made from a smartphone
                        String messageId = "DriverAssign";
                        String localizedMessageToPassenger = null;
                        
                        try {
                            localizedMessageToPassenger = Util.getProperty(messageId + "_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                        } catch (Exception e) {
                        	LOGGER.info("Problem sending push to passenger",e);
                        }
                        
                        LOGGER.info("Sending assignment notification");

                        if(fetchedData.getBookagent()!=null&&(fetchedData.getBookagent().equals("w"))){
                        		//||fetchedData.getBookagent().equals("d"))) {

                        	String smsMobile = getSmsMobile(fetchedData);
                        	
                        	if(smsMobile!=null) {
                        		LOGGER.debug("SMD Will send sms from "+fetchedData.getTaxicompanyId()+" to "+fetchedData.getPassengerId()+" "+localizedMessageToPassenger);
                        		SMSSUtil smssUtil = new SMSSUtil(fetchedData.getTaxicompanyId());
                        		smssUtil.setPassengerId(fetchedData.getPassengerId());
                        		smssUtil.sendSmsAsync(smsMobile, localizedMessageToPassenger);
                        	} else {
                        		LOGGER.debug("SMD Will not send sms no mobile field");
                        	}
                        }
                        
                        if (fetchedData.getExtra() != null && fetchedData.getBookagent() != null && (fetchedData.getBookagent().equals("s"))) {
                        
                        	try {
                        		String pushSound = Util.getProperty("push_sound", "config");
                        		String localizedTitleToPassenger = Util.getProperty(messageId + "_title_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                        		if(fetchedData.getBookagent().equals("s")) {
                        			PushSender.getInstance(driver.getBrandName()).sendPushNotification(fetchedData.getExtra(), localizedMessageToPassenger, messageId,localizedTitleToPassenger, null, 0,pushSound);
                        		}
                        	} catch (Exception e) {
                        		LOGGER.error("Seding push to driver");
                        	}
                        }

                        String themessage = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove, tripId,assignedDriverInfo);
                        fetchedData.setAssignmentMsg(themessage);

                        //ekos 04/04/2012 added the following line cause zones dont work well in android
                        //Only for test
                        LOGGER.error("Kick the driver from zone "+driver.getDriverId() );
                        Zones.getInstance().updateDriverZone(null, driver.getDriverId(), false,null,null);
                        
                        List<String> imeis = null;
                        imeis = DataHelper.getPickLostDriversImei(tripId, deviceId);
                        
                        for(String lostimei :imeis ) {
                        	comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), lostimei);
                        	//Dispatcher.getInstance().sendOperationOnCallMessage(tripId, lostimei);
                        }
                        
                        imeis = DataHelper.getDriversNotInterestedImei(tripId);
                        
                        for(String noInterestImei:imeis) {
                        	Dispatcher.getInstance().sendOperationOnCallMessage(tripId, noInterestImei);
                        }
                        
					}
				
				} catch(Exception e) {
					LOGGER.error("An exception ",e);
	    		}
			}
			
			
    	} catch (SQLException e) {
			LOGGER.error("Problem inserting transactional call",e);
		}
    	    		
    	return outcome;
    	
    }
    
    public boolean transactionalCallTypeAssignment(long tripId,CallType callType) {
    	
    	boolean outcome = false;
    	
    	try {
			Long deviceId = DataHelper.transactionalCallTypeAssignment(tripId, callType);
		
			LOGGER.info("Transactional assignment finised tripid: "+tripId);
    		
			if(deviceId!=null) {
				try {
					TripData fetchedData = fetchTripData(tripId);
					String passengerInformXML = null;
					String discountMsg = getDiscountMessage(fetchedData);
					Driver driver = DataHelper.driverFromDeviceAndTrip(deviceId, tripId);
					
					if(comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), driver.getDevice().getImei())) {
						
						if(driver.getDevice().getExtra()!=null) {
    						try {
    							String messageDrId = "PassengerAssign";
                                String localizedMessageToDriver = Util.getProperty(messageDrId + "_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                                String localizedTitleToDriver = Util.getProperty(messageDrId + "_title_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE),"lang");
                                PushSender.getInstanceDriver(driver.getBrandName()).sendPushNotification(driver.getDevice().getExtra(), localizedMessageToDriver, messageDrId,localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));
    						} catch (Exception ex) {
    							LOGGER.error("Error sending push",ex);
    						}
    					}
    					
    					outcome = true;
    					
    					String assignedDriverInfo = null;
    					
    					if (fetchedData.getBookagent() == null ||(!fetchedData.getBookagent().equals("w"))) {
    						assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_XML"), deviceId, driver.getDevice().getCurrentLat(),
    								driver.getDevice().getCurrentLng(), driver.getDevice().getVehicle().getPlateno(), driver.getFirstName() + " " +
                                            driver.getLastName(), driver.getMobile(), driver.getDevice().getVehicle().getModel(), fetchedData.getGaddress(), driver.getDistance());
    					} else {
                            assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_MULTICALL_XML"), tripId, deviceId, driver.getDevice().getCurrentLat(),
                            		driver.getDevice().getCurrentLng(), driver.getDevice().getVehicle().getPlateno(),driver.getFirstName() + " " + 
                            				driver.getLastName(), driver.getMobile(), driver.getDevice().getVehicle().getModel(), fetchedData.getGaddress(), driver.getDistance());
    					}
    					
    					passengerInformXML = String.format(getXML("PASSENGER_INFORM"), assignedDriverInfo, discountMsg);
                        comm.sendMessageToPassenger(passengerInformXML, fetchedData.getPassengerId());
                        //send a push notification to the passenger for any driver's action
                        //and only if the call was made from a smartphone
                        String messageId = "DriverAssign";
                        String localizedMessageToPassenger = null;
                        
                        try {
                            localizedMessageToPassenger = Util.getProperty(messageId + "_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                        } catch (Exception e) {
                        	LOGGER.info("Problem sending push to passenger",e);
                        }
                        
                        LOGGER.info("Sending assignment notification");

                        if(fetchedData.getBookagent()!=null&&(fetchedData.getBookagent().equals("w"))) {
                        		//||fetchedData.getBookagent().equals("d"))) {
                        	
                        	String smsMobile = getSmsMobile(fetchedData);
                        	
                        	if(smsMobile!=null) {
                        	
                        		LOGGER.debug("SMD Will send sms from "+fetchedData.getTaxicompanyId()+" to "+smsMobile+" "+localizedMessageToPassenger);
                        		
                        		SMSSUtil smssUtil = new SMSSUtil(fetchedData.getTaxicompanyId());
                        		smssUtil.setPassengerId(fetchedData.getPassengerId());
                        		smssUtil.sendSmsAsync(smsMobile, localizedMessageToPassenger);
                            	
                        	} else {
                        		LOGGER.debug("SMD Will not send sms no mobile field");
                            }
                        }
                        
                        if (fetchedData.getExtra() != null && fetchedData.getBookagent() != null && (fetchedData.getBookagent().equals("s"))) {
                        
                        	try {
                        		String pushSound = Util.getProperty("push_sound", "config");
                        		String localizedTitleToPassenger = Util.getProperty(messageId + "_title_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                        		if(fetchedData.getBookagent().equals("s")) {
                        			PushSender.getInstance(driver.getBrandName()).sendPushNotification(fetchedData.getExtra(), localizedMessageToPassenger, messageId,localizedTitleToPassenger, null, 0,pushSound);
                        		}
                        	} catch (Exception e) {
                        		LOGGER.error("Seding push to driver");
                        	}
                        }

                        String themessage = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove, tripId,assignedDriverInfo);
                        fetchedData.setAssignmentMsg(themessage);

                        //ekos 04/04/2012 added the following line cause zones dont work well in android
                        //Only for test
                        LOGGER.error("Will kick the driver from the zone "+driver.getDriverId());
                        Zones.getInstance().updateDriverZone(null, driver.getDriverId(), false,null,null);
                        
                        List<String> imeis = null;
                        imeis = DataHelper.getPickLostDriversImei(tripId, deviceId);
                        
                        for(String lostimei :imeis ) {
                        	comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), lostimei);
                        	//Dispatcher.getInstance().sendOperationOnCallMessage(tripId, lostimei);
                        }
                        
                        imeis = DataHelper.getDriversNotInterestedImei(tripId);
                        
                        for(String noInterestImei:imeis) {
                        	Dispatcher.getInstance().sendOperationOnCallMessage(tripId, noInterestImei);
                        }
                        
					}
				
				} catch(Exception e) {
					LOGGER.error("An exception ",e);
	    		}
			}
			
			
    	} catch (SQLException e) {
			LOGGER.error("Problem inserting transactional call",e);
		}
    	
    	return outcome;
    
    }
    
    /*
     * Making a transactional call with table locking
     */
    public boolean transactionalAssignment(long tripId) {
    	
    	boolean outcome = false;
    	
    	try {
    		
    		Long deviceId = DataHelper.transactionalAssignment(tripId);
    		
    		LOGGER.info("Transactional assignment finised tripid: "+tripId);
    		
    		if(deviceId!=null) {
    			
    			try {
    				
    				TripData fetchedData = fetchTripData(tripId);
    				String passengerInformXML = null;
    				String discountMsg = getDiscountMessage(fetchedData);
    				Driver driver = DataHelper.driverFromDeviceAndTrip(deviceId, tripId);
    				
    				if(comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), driver.getDevice().getImei())) {
    					
    					//Sending push notification to driver
    					if(driver.getDevice().getExtra()!=null) {
    						try {
    							String messageDrId = "PassengerAssign";
                                String localizedMessageToDriver = Util.getProperty(messageDrId + "_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                                String localizedTitleToDriver = Util.getProperty(messageDrId + "_title_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE),"lang");
                                PushSender.getInstanceDriver(driver.getBrandName()).sendPushNotification(driver.getDevice().getExtra(), localizedMessageToDriver, messageDrId,localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));
    						} catch (Exception ex) {
    							LOGGER.error("Error sending push",ex);
    						}
    					}
    					
    					outcome = true;
    					
    					String assignedDriverInfo = null;
    					
    					if (fetchedData.getBookagent() == null ||(!fetchedData.getBookagent().equals("w"))) {
    						assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_XML"), deviceId, driver.getDevice().getCurrentLat(),
    								driver.getDevice().getCurrentLng(), driver.getDevice().getVehicle().getPlateno(), driver.getFirstName() + " " +
                                            driver.getLastName(), driver.getMobile(), driver.getDevice().getVehicle().getModel(), fetchedData.getGaddress(), driver.getDistance());
    					} else {
                            assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_MULTICALL_XML"), tripId, deviceId, driver.getDevice().getCurrentLat(),
                            		driver.getDevice().getCurrentLng(), driver.getDevice().getVehicle().getPlateno(),driver.getFirstName() + " " + 
                            				driver.getLastName(), driver.getMobile(), driver.getDevice().getVehicle().getModel(), fetchedData.getGaddress(), driver.getDistance());
    					}
    					
    					passengerInformXML = String.format(getXML("PASSENGER_INFORM"), assignedDriverInfo, discountMsg);
                        comm.sendMessageToPassenger(passengerInformXML, fetchedData.getPassengerId());
                        //send a push notification to the passenger for any driver's action
                        //and only if the call was made from a smartphone
                        String messageId = "DriverAssign";
                        String localizedMessageToPassenger = null;
                        
                        try {
                            localizedMessageToPassenger = Util.getProperty(messageId + "_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                        } catch (Exception e) {
                        	LOGGER.info("Problem sending push to passenger",e);
                        }
                        
                        LOGGER.info("Sending assignment notification");

                        if(fetchedData.getBookagent()!=null&&(fetchedData.getBookagent().equals("w"))) {
                        		//&&fetchedData.getBookagent().equals("d"))) {
                        	
                        	String smsMobile = getSmsMobile(fetchedData);
                        	
                        	if(smsMobile!=null) {
                        		LOGGER.debug("MSD Will send sms from "+fetchedData.getTaxicompanyId()+" to "+smsMobile+" "+localizedMessageToPassenger);
                        		SMSSUtil smssUtil = new SMSSUtil(fetchedData.getTaxicompanyId());
                                smssUtil.setPassengerId(fetchedData.getPassengerId());
                            	smssUtil.sendSmsAsync(smsMobile, localizedMessageToPassenger);
                            } else {
                        		LOGGER.debug("SMD Will not send sms no mobile field");
                        	}
                        	
                        }
                        
                        if (fetchedData.getExtra() != null && fetchedData.getBookagent() != null && (fetchedData.getBookagent().equals("s"))) {
                        
                        	try {
                        		String pushSound = Util.getProperty("push_sound", "config");
                        		String localizedTitleToPassenger = Util.getProperty(messageId + "_title_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
                        		if(fetchedData.getBookagent().equals("s")) {
                        			PushSender.getInstance(driver.getBrandName()).sendPushNotification(fetchedData.getExtra(), localizedMessageToPassenger, messageId,localizedTitleToPassenger, null, 0,pushSound);
                        		}
                        	} catch (Exception e) {
                        		LOGGER.error("Seding push to driver");
                        	}
                        }

                        String themessage = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove, tripId,assignedDriverInfo);
                        fetchedData.setAssignmentMsg(themessage);

                        //ekos 04/04/2012 added the following line cause zones dont work well in android
                        //Only for test
                        LOGGER.error("Will kick the driver from the zone "+driver.getDriverId());
                        Zones.getInstance().updateDriverZone(null, driver.getDriverId(), false,null,null);
                        
                        List<String> imeis = null;
                        imeis = DataHelper.getPickLostDriversImei(tripId, deviceId);
                        
                        for(String lostimei :imeis ) {
                        	comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), lostimei);
                        	//Dispatcher.getInstance().sendOperationOnCallMessage(tripId, lostimei);
                        }
                        
                        imeis = DataHelper.getDriversNotInterestedImei(tripId);
                        
                        for(String noInterestImei:imeis) {
                        	Dispatcher.getInstance().sendOperationOnCallMessage(tripId, noInterestImei);
                        }
                        
    				}
    				
    				
    			} catch(Exception e) {
    				LOGGER.error("An exception ",e);
    			}
    			
    		}
    		
    	} catch(SQLException e) {
    		LOGGER.error("Problem inserting transactional call",e);
    	}
    	
    	
    	
    	return outcome;
    	
    	/*
    	try {
			
    		Long deviceId = DataHelper.transactionalAssignment(tripId);
			
    		if(deviceId!=null) {
    			
    			Driver driver = DataHelper.driverFromDeviceAndTrip(deviceId, tripId);
    			try {
					TripData fetchedData = fetchTripData(tripId);
					String passengerInformXML = null;
					String discountMsg = getDiscountMessage(fetchedData);
					if (comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), driver.getDevice().getImei())) {
					
						if(driver.getDevice().getExtra()!=null) {
							
							try {
							    String messageDrId = "PassengerAssign";
							    String localizedMessageToDriver = Util.getProperty(messageDrId + "_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
							    String localizedTitleToDriver = Util.getProperty(messageDrId + "_title_" + (driver.getDevice().getLocale() != null ? driver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE),"lang");
							    PushSender.getInstanceDriver(driver.getBrandName()).sendPushNotification(driver.getDevice().getExtra(), localizedMessageToDriver, messageDrId,localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));								
							} catch(Exception ex) {
								ex.printStackTrace();
							}

							String assignedDriverInfo = null;
						    if (fetchedData.getBookagent() == null ||(!fetchedData.getBookagent().equals("w"))) {
						    	assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_XML"), 
						    			driver.getDevice().getDeviceId(),
						    			driver.getDevice().getCurrentLat(),
						    			driver.getDevice().getCurrentLng(),
						    			driver.getDevice().getVehicle().getPlateno(),
						    			driver.getFirstName()+" "+driver.getLastName(),
						    			driver.getMobile(), driver.getDevice().getVehicle().getModel(),
						    			fetchedData.getGaddress(), driver.getDistance());
						    } else { 
						    	assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_MULTICALL_XML"), 
						    			tripId, driver.getDevice().getDeviceId(),
						    			driver.getDevice().getCurrentLat(),
						    			driver.getDevice().getCurrentLng(),
						    			driver.getDevice().getVehicle().getPlateno(),
						    			driver.getFirstName()+ " " +driver.getLastName(),
						    			driver.getMobile(), driver.getDevice().getVehicle().getModel(), 
						    			fetchedData.getGaddress(), driver.getDistance());
						    
						    }
				
						    passengerInformXML = String.format(getXML("PASSENGER_INFORM"), assignedDriverInfo, discountMsg);
						    comm.sendMessageToPassenger(passengerInformXML, fetchedData.getPassengerId());
						    //send a push notification to the passenger for any driver's action
						    //and only if the call was made from a smartphone
						    String messageId = "DriverAssign";
						    String localizedMessageToPassenger = null;

						    try {
						    	localizedMessageToPassenger = Util.getProperty(messageId + "_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() :
								Push.DEFAULT_PASSENGER_LOCALE), "lang");
						    } catch (Exception e) {
							//do nothing
						    }
						    LOGGER.info("Sending assignment notification");
						    
						    if(fetchedData.getBookagent()!=null&&fetchedData.getBookagent().equals("w")) {
				    		
						    	LOGGER.info("Message will send from company "+fetchedData.getTaxicompanyId()+" and passengerid "+fetchedData.getPassengerId());
						    	
				    			SMSSUtil smssUtil = new SMSSUtil(fetchedData.getTaxicompanyId());
				    			smssUtil.setPassengerId(fetchedData.getPassengerId());
				    			smssUtil.sendSmsAsync(fetchedData.getMobile(), localizedMessageToPassenger);
				    		}
						    
						    if (fetchedData.getExtra() != null && fetchedData.getBookagent() != null && (fetchedData.getBookagent().equals("s"))) {
						    	try {
						    	
						    		String pushSound = Util.getProperty("push_sound", "config");
						    		String localizedTitleToPassenger = Util.getProperty(messageId + "_title_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
						    		
						    		if(fetchedData.getBookagent().equals("s")) {
						    			PushSender.getInstance(driver.getBrandName()).sendPushNotification(fetchedData.getExtra(),
						    					localizedMessageToPassenger, messageId,localizedTitleToPassenger, null, 0, pushSound);
						    		}
						    		
						    	} catch (Exception e) {
						    		e.printStackTrace();
						    	}
						    }
		                                    
		                    String themessage = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove, tripId,assignedDriverInfo);
		                                     
						    fetchedData.setAssignmentMsg(themessage);

						    //ekos 04/04/2012 added the following line cause zones dont work well in android
						    Zones.getInstance().updateDriverZone(null, driver.getDriverId(), false,null,null);
						    						    
						}
					}
					
    			} catch (Exception e) {
					e.printStackTrace();
				}
    			
    		}
			
    		return true;
    		
    	} catch (SQLException e) {
			LOGGER.error("Problem with transactional assignment",e);
		}
    	
    	return false;
    	*/
    }
    
    public boolean dispatchNewTripAssignment(Long tripId) {
    	
    	LOGGER.info("*** MAKING THE DISPATCHING THROUGH THE NEW TRIP ASSIGNMENT ***");
    	
    	boolean outcome = false;
    	
    	try {
			
    		List<Driver> drivers = DataHelper.findPickedDriversForTrip(tripId);
    		TripData fetchedData = fetchTripData(tripId);
			String passengerInformXML = null;
		    
			Driver selectedDriver = null;
			
			Iterator<Driver> iterator = drivers.iterator();
			
			while (iterator.hasNext()) {
				Driver driver = iterator.next();
				
				if(selectedDriver==null) {
					if(!driver.getDevice().equals("occ")||!driver.getDevice().equals("una")) {
						selectedDriver = driver;
						iterator.remove();
					}
				}
				
			}
		
			if(selectedDriver!=null) {
				
				String discountMsg = getDiscountMessage(fetchedData);
				
				if (comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), selectedDriver.getDevice().getImei())) {
					prepareAssignTripToDeviceStmt();
					int i = 1;
					assignTripToDeviceStmt.setLong(i++, selectedDriver.getDevice().getDeviceId());
					assignTripToDeviceStmt.setLong(i++, selectedDriver.getDriverId());
					assignTripToDeviceStmt.setLong(i++, selectedDriver.getDevice().getVehicle().getVehicleId());
					assignTripToDeviceStmt.setLong(i++, tripId);
				
					try {
					    
						assignTripToDeviceStmt.executeUpdate();
					    
						if (selectedDriver.getDevice().getExtra() != null) {
							try {
							    String messageDrId = "PassengerAssign";
							    String localizedMessageToDriver = Util.getProperty(messageDrId + "_" + (selectedDriver.getDevice().getLocale() != null ? selectedDriver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
							    String localizedTitleToDriver = Util.getProperty(messageDrId + "_title_" + (selectedDriver.getDevice().getLocale() != null ? selectedDriver.getDevice().getLocale() : Push.DEFAULT_PASSENGER_LOCALE),"lang");
							    PushSender.getInstanceDriver(selectedDriver.getBrandName()).sendPushNotification(selectedDriver.getDevice().getExtra(), localizedMessageToDriver, messageDrId,localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));

							} catch (Exception ex) {
							    ex.printStackTrace();
							}
					    }
					    
						
						outcome = true;
					    String assignedDriverInfo = null;
					    
					    if (fetchedData.getBookagent() == null ||(!fetchedData.getBookagent().equals("w"))) {
					    	assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_XML"), 
					    			selectedDriver.getDevice().getDeviceId(),
					    			selectedDriver.getDevice().getCurrentLat(),
					    			selectedDriver.getDevice().getCurrentLng(),
					    			selectedDriver.getDevice().getVehicle().getPlateno(),
					    			selectedDriver.getFirstName()+" "+selectedDriver.getLastName(),
					    			selectedDriver.getMobile(), selectedDriver.getDevice().getVehicle().getModel(),
					    			fetchedData.getGaddress(), selectedDriver.getDistance());
					    } else { 
					    	assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_MULTICALL_XML"), 
					    			tripId, selectedDriver.getDevice().getDeviceId(),
					    			selectedDriver.getDevice().getCurrentLat(),
					    			selectedDriver.getDevice().getCurrentLng(),
					    			selectedDriver.getDevice().getVehicle().getPlateno(),
					    			selectedDriver.getFirstName()+ " " + selectedDriver.getLastName(),
					    			selectedDriver.getMobile(), selectedDriver.getDevice().getVehicle().getModel(), 
					    			fetchedData.getGaddress(), selectedDriver.getDistance());
					    	
					    }
					    
					    passengerInformXML = String.format(getXML("PASSENGER_INFORM"), assignedDriverInfo, discountMsg);
					    comm.sendMessageToPassenger(passengerInformXML, fetchedData.getPassengerId());
					    //send a push notification to the passenger for any driver's action
					    //and only if the call was made from a smartphone
					    String messageId = "DriverAssign";
					    String localizedMessageToPassenger = null;
			
					    try {
					    	localizedMessageToPassenger = Util.getProperty(messageId + "_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() :
							Push.DEFAULT_PASSENGER_LOCALE), "lang");
					    } catch (Exception e) {
						//do nothing
					    }
					    
					    LOGGER.info("Sending assignment notification");
					    
					    if(fetchedData.getBookagent()!=null&&(fetchedData.getBookagent().equals("w"))) {
					    		//||fetchedData.getBookagent().equals("d"))) {
			    		
					    	
					    	String smsMobile = getSmsMobile(fetchedData);
                        	
                        	if(smsMobile!=null) {
                        		LOGGER.debug("SMD Will send sms from "+fetchedData.getTaxicompanyId()+" to "+smsMobile+" "+localizedMessageToPassenger);
                        		SMSSUtil smssUtil = new SMSSUtil(fetchedData.getTaxicompanyId());
                        		smssUtil.setPassengerId(fetchedData.getPassengerId());
                        		smssUtil.sendSmsAsync(smsMobile, localizedMessageToPassenger);
                        	} else {
                        		LOGGER.debug("SMD Will not send sms no mobile field");
                        	}
					    
					    }
					    
					    if (fetchedData.getExtra() != null && fetchedData.getBookagent() != null && (fetchedData.getBookagent().equals("s"))) {
					    	try {
					    	
					    		String pushSound = Util.getProperty("push_sound", "config");
					    		String localizedTitleToPassenger = Util.getProperty(messageId + "_title_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
					    		
					    		if(fetchedData.getBookagent().equals("s")) {
					    			PushSender.getInstance(selectedDriver.getBrandName()).sendPushNotification(fetchedData.getExtra(),
					    					localizedMessageToPassenger, messageId,localizedTitleToPassenger, null, 0, pushSound);
					    		}
					    		
					    	} catch (Exception e) {
					    		e.printStackTrace();
					    	}
					    }
	                                    
	                    String themessage = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove, tripId,assignedDriverInfo);
	                                     
					    fetchedData.setAssignmentMsg(themessage);

					    //ekos 04/04/2012 added the following line cause zones dont work well in android
					    LOGGER.error("Will kick the driver from the zone "+selectedDriver.getDriverId());
					    Zones.getInstance().updateDriverZone(null, selectedDriver.getDriverId(), false,null,null);
					    
					    DataHelper.setStatusOnTripAssign(tripId,selectedDriver.getDevice().getDeviceId(), selectedDriver.getDevice().getStatus());
				    	
					    /*
					     * Informing all the other drivers about the situation
					     */
					    for(Driver driver:drivers) {
					    	
					    	DataHelper.setStatusOnTripAssign(tripId,driver.getDevice().getDeviceId(), driver.getDevice().getStatus());
					    	
					    	comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), driver.getDevice().getImei());
		                }
					    
					 
					} catch (Exception e) { //MySQLIntegrityConstraintViolationException
					    System.err.println(tripId + " Failed to assign trip to device due to constraint violation due to exception: " +
							       e.getMessage());
					}
				}
			
				LOGGER.info("*** Will sent cancellation message to the rest of the drivers***");
		    		
				try {
					List<String> imeis = DataHelper.findNonInterestedImeisForTrip(tripId);
					for(String imei:imeis) {
						try {
							comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), imei);
						} catch (XMPPException e) {
							LOGGER.error("Throwing an xmpp error",e);
						}
					}
		    	} catch (SQLException e) {
					LOGGER.error("Problem fetching imeis",e);
				}
			}
			
    	} catch (SQLException e) {
			LOGGER.error("An sql exception occured",e);
			//e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	
    	return outcome;
    }
    
    public boolean dispatchTripAssignment(Long tripId) {
    	
    	boolean outcome = false;
    	try {
    		prepareFindDriversForTripStmt();
    		findDriversForTripStmt.setLong("tripid", tripId);
    		TripData fetchedData = fetchTripData(tripId);
    		//find the drivers
    		ResultSet rs = findDriversForTripStmt.executeQuery();
    		String passengerInformXML = null;
    		while (rs.next()) {
    			//String driverImei = closestDriverRS.getString("imei");
    			long deviceId = rs.getLong("deviceid");
    			String deviceImei = rs.getString("imei");
    			try {
    				if (!outcome) {
    					long driverId = rs.getLong("driverid");
    					long vehicleId = rs.getLong("vehicleid");
    					String driverFirstName = rs.getString("firstname");
    					String driverLastName = rs.getString("lastname");
    					String driverMobile = rs.getString("mobile");
    					String driverCarmodel = rs.getString("carmodel");
    					String plateNo = rs.getString("platenumber");
    					Double deviceLat = rs.getDouble("currentlat");
    					Double deviceLng = rs.getDouble("currentlong");
    					Double distance = rs.getDouble("distance");
    					String drlocale = rs.getString("locale");
    					String drextra = rs.getString("extra");
    					String brand = rs.getString("brandname");
    					
    					if (driverId != 0) {
			    
    						String discountMsg = getDiscountMessage(fetchedData);				
				
    						if (comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), deviceImei)) {

    							prepareAssignTripToDeviceStmt();
    							//update trip with assigned deviceid, driverid and update device, set occupied
    							int i = 1;
    							assignTripToDeviceStmt.setLong(i++, deviceId);
    							assignTripToDeviceStmt.setLong(i++, driverId);
    							assignTripToDeviceStmt.setLong(i++, vehicleId);
    							assignTripToDeviceStmt.setLong(i++, tripId);
    							try {
    								assignTripToDeviceStmt.executeUpdate();
    								//ekos driver push notif for iphone on assigment
    								if (drextra != null) {
    									try {
    										String messageDrId = "PassengerAssign";
    										String localizedMessageToDriver =
    												Util.getProperty(messageDrId + "_" + (drlocale != null ? drlocale : Push.DEFAULT_PASSENGER_LOCALE), "lang");
    										String localizedTitleToDriver =
    												Util.getProperty(messageDrId + "_title_" + (drlocale != null ? drlocale : Push.DEFAULT_PASSENGER_LOCALE),
    														"lang");
    										PushSender.getInstanceDriver(brand).sendPushNotification(drextra, localizedMessageToDriver, messageDrId,
    												localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));

    									} catch (Exception ex) {
    										ex.printStackTrace();
    									}

    								}
    								outcome = true;
    								//inform the passenger with driver data
    								String assignedDriverInfo = null;

                                    //System.out.println("The device id follows");                                    
                                    //System.out.println(deviceId);
                        
    								if (fetchedData.getBookagent() == null ||(!fetchedData.getBookagent().equals("w"))) {
    									assignedDriverInfo = String.format(getXML("ASSIGNED_DRIVER_XML"), deviceId, deviceLat, deviceLng, plateNo, driverFirstName + " " +
    											driverLastName, driverMobile, driverCarmodel, fetchedData.getGaddress(), distance);
    								} else {
    									assignedDriverInfo =
    											String.format(getXML("ASSIGNED_DRIVER_MULTICALL_XML"), tripId, deviceId, deviceLat, deviceLng, plateNo,
    													driverFirstName + " " + driverLastName, driverMobile, driverCarmodel, fetchedData.getGaddress(),
    													distance);
    								}
                                    
    								//String discountMsg = WhitePages.getDiscountMessage(fetchedData.getLocale(), fetchedData.getIsDiscounted());
				       
    								passengerInformXML = String.format(getXML("PASSENGER_INFORM"), assignedDriverInfo, discountMsg);
    								comm.sendMessageToPassenger(passengerInformXML, fetchedData.getPassengerId());
    								//send a push notification to the passenger for any driver's action
    								//and only if the call was made from a smartphone
    								String messageId = "DriverAssign";
    								String localizedMessageToPassenger = null;
				    
    								try {
    									localizedMessageToPassenger = Util.getProperty(messageId + "_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() :
    										Push.DEFAULT_PASSENGER_LOCALE), "lang");
    								} catch (Exception e) {
    									//do nothing
    								}
				    
    								LOGGER.info("Sending assignment notification");
				    
    								if(fetchedData.getBookagent()!=null&&(fetchedData.getBookagent().equals("w"))) {
    										//||fetchedData.getBookagent().equals("d"))) {
		    		
    									String smsMobile = getSmsMobile(fetchedData);
    		                        	
    		                        	if(smsMobile!=null) {
    		                        		LOGGER.debug("SMD Will send sms from "+fetchedData.getTaxicompanyId()+" to "+smsMobile+" "+localizedMessageToPassenger);
    		                        		SMSSUtil smssUtil = new SMSSUtil(fetchedData.getTaxicompanyId());
    		                        		smssUtil.setPassengerId(fetchedData.getPassengerId());
    		                        		smssUtil.sendSmsAsync(smsMobile, localizedMessageToPassenger);
    		                        	} else {
    		                        		LOGGER.debug("SMD Will not send sms no mobile field");
    		                        	}
    									
    								}
				    
    								if (fetchedData.getExtra() != null && fetchedData.getBookagent() != null && (fetchedData.getBookagent().equals("s"))) {
    									try {
				    	
    										String pushSound = Util.getProperty("push_sound", "config");
    										String localizedTitleToPassenger = 
    												Util.getProperty(messageId + "_title_" + (fetchedData.getLocale() != null ? fetchedData.getLocale() : Push.DEFAULT_PASSENGER_LOCALE), "lang");
				    		
    										if(fetchedData.getBookagent().equals("s")) {
    											PushSender.getInstance(brand).sendPushNotification(fetchedData.getExtra(), localizedMessageToPassenger, messageId,
    													localizedTitleToPassenger, null, 0,pushSound);
    										}
				    		
    									} catch (Exception e) {
    										e.printStackTrace();
    									}
    								}
                                    
    								String themessage = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove, tripId,assignedDriverInfo);
                                    fetchedData.setAssignmentMsg(themessage);

                                    //ekos 04/04/2012 added the following line cause zones dont work well in android
                                    LOGGER.error("WIll kick the driver from the zone "+driverId);
                                    Zones.getInstance().updateDriverZone(null, driverId, false,null,null);


    							} catch (Exception e) { //MySQLIntegrityConstraintViolationException
    								System.err.println(tripId + " Failed to assign trip to device due to constraint violation due to exception: " +
    										e.getMessage());
    							}
    						}
    					}
    				}
    				//inform the rest of drivers that the fare has been given to another dude
    				else {
                        
    					//String agoodMessage = fetchedData.getAssignmentMsg().replace("<Info>","<Info><deviceId>"+Long.toString(deviceId)+"</deviceId>");
                        //comm.sendMessageToDriver(agoodMessage, deviceImei);
                        comm.sendMessageToDriver(fetchedData.getAssignmentMsg(), deviceImei);
                    }
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    	
    		rs.close();
    		dataPool.closeConnection();
    	
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    
    	return outcome;
    }

    public void expireTrip(Long tripId, String reason) throws Exception {
    	prepareMarkTripAsExpiredStmt();
    	markTripAsExpiredStmt.setLong(1, tripId);
    	markTripAsExpiredStmt.registerOutParameter(2, Types.BIGINT);
    	int affectedRowsAmount = markTripAsExpiredStmt.executeUpdate();
    	dataPool.closeConnection();
    	informPassengerOfShortage(tripId, reason);
    	if (affectedRowsAmount > 0) { //TODO test whether this works with the Stored Procedure
    		Dispatcher.getInstance().unlinkDevices(tripId, true);
    	}
    	
    	/**
    	 * Zone checking
    	 */
    	try {
			if(tripId!=null) {
				
				CallType callType = DataHelper.getTripCallType(tripId);
				if(callType!=null&&callType.getZoneOrder().compareTo(0.0)==1) {
					Long zoneId = DataHelper.getTripZoneId(tripId);
					if(zoneId!=null&&zoneId.compareTo(0L)!=0) {
						Zone zone = DataHelper.getZone(zoneId);
						if(zone.getKickDrivers()) {
							LOGGER.error("Will kick all the drivers from the zone");
							Zones.getInstance().kickDriversFromZone(DataHelper.getTripAssignDrivers(tripId), zoneId);
						}
					}
				}
				
				//PenaltyManager.executeExpirationPenalty(tripId);
				
			}
		} catch (SQLException e) {
			LOGGER.error("Problem removing drivers from zone",e);
		} catch (NullPointerException e) {
			LOGGER.error("No need to remove drivers from zone something is null",e);
		}
    	
    }

    private void informPassengerOfShortage(Long tripId, String reason) {
    	try {
    		prepareFetchPassengerInfoForTripStmt();
    		fetchPassengerInfoForTripStmt.setLong(1, tripId);
    		ResultSet passengerRow = fetchPassengerInfoForTripStmt.executeQuery();
    		if (passengerRow.next()) { //one row result
    			comm.sendMessageToPassenger(String.format(getXML("PASSENGER_SHORTAGE"), tripId, reason),
					    passengerRow.getLong("passengerid"));
    		}
    		passengerRow.close();
    		dataPool.closeConnection();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

    public Map<Long, BiddingTime> getTripsToDispatch() {
	return tripsToDispatch;
    }

    public Map<Long, BiddingTime> getProcessingTrips() {
	return processingTrips;
    }

    public Map<Long, BiddingTime> getLastChanceTrips() {
	return lastChanceTrips;
    }

    public String getDiscountMessage(TripData tripData) {
    
    	OfferProvider offerProvider = new OfferProvider(tripData.getTaxicompanyId(), tripData.getPassengerId());
    	Double discount = offerProvider.getAnyDiscount(tripData.getBookagent());
    	
    	if(discount!=0.0) {
    		
    		return String.format(
    				Util.getProperty("discount_msg_2_" + Push.DEFAULT_PASSENGER_LOCALE, "lang"),
    				String.valueOf(discount.intValue())+"\u20AC");
    		
    	} else {
    		return "";
    	}
    
    }
    
    private String getSmsMobile(TripData tripData) {
    	
    	if(tripData.getContactPhone()!=null) {
    		return tripData.getContactPhone();
    	}
    	if(tripData.getMobile()!=null) {
    		return tripData.getMobile();
    	} 
    	
    	return null;
    }
    
}
