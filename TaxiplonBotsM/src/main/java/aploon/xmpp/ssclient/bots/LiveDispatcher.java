package aploon.xmpp.ssclient.bots;


import aploon.IRes;
import aploon.Main;

import com.taxiplon.Util;

import aploon.xmpp.ssclient.Hotspots;

import com.esri.core.geometry.Point;

import com.google.gson.Gson;

import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class LiveDispatcher extends Dispatcher {

	public static final String LIVE_DATA_URL = "http://www.livemap.com.gr/LiveMapApi.ashx?%s&input=&catsub=%s";
	public static final String PHARMACY_CATSUB = "maincatid$$1000@@subcatid$$-1";
	public static final String HOSPITAL_CATSUB = "maincatid$$2000@@subcatid$$-1";
	public static final String COORD_PARAMS = "&lat=%s&lng=%s"; //lat and lng in unknown spatial reference as a string representation

	public static final int DEFAULT_SLEEP_TIME_MIN = 15;
	public static final int DEFAULT_NODES_TO_KEEP = 20;

	private static LiveDispatcher instance;
	private Util util;

	public static synchronized LiveDispatcher getInstance() {
		if(instance == null) {
			instance = new LiveDispatcher();
		}
		return instance;
	}
	
	PreparedStatement fetchDeviceCoordinatesByImeiStmt;
	
	private void prepareFetchDeviceCoordinatesByImeiStmt() throws Exception {
		if(fetchDeviceCoordinatesByImeiStmt == null || fetchDeviceCoordinatesByImeiStmt.isClosed()) {
			fetchDeviceCoordinatesByImeiStmt = dataPool.prepareStatement(getQuery("fetchDeviceCoordinatesByImeiStmt"));
		}
	}

	public LiveDispatcher() {
		super();
		util = new Util();
		//don't bother to run the threads, these data will be fetched on demand due to excessive bandwidth allocation
//		new PharmaciesFetcher().start();
//		new HospitalsFetcher().start();
	}

	protected String iResToEventsXML(String json, Hotspots.EventTypes type, int nodesToKeep) {
		String xml = "";
		IRes iRes = new Gson().fromJson(json, IRes.class);
		if(iRes.iResItems.size() > 0) {
			iRes.iResItems.get(0).sortByDistanceAsc();
		}
		String title = "", timeFrame = "", day = "", address = "", phone = null;
		Long id = -1000L;
		Float lat = 0f, lng = 0f;
		try {
			for(IRes.IResItem item : iRes.iResItems.get(0).iC) {
				if(nodesToKeep-- > 0) {
					//from the service lat/lng come in wkid 2100
					lat = item.iLat;
					lng = item.iLon;
					Point p = locator.projectPointFrom2100ToGPS(lat, lng);
					for(IRes.IResItemProps props : item.iA) {
						if(props.aF.equals("aTitle")) {
							title = props.aV;
						}
						else if(props.aF.equals("aDesc")) {
							timeFrame = props.aV;
						}
						else if(props.aF.equals("aDay")) {
							day = props.aV;
						}
						else if(props.aF.equals("aAddress")) {
							address = props.aV;
						}
						else if(props.aF.equals("aTel")) {
							phone = props.aV;
						}
					}
					xml += String.format(getXML("EVENT_XML"), id++, 
										 normalizeTextForXML(title + (phone != null ? " (" + phone + ")" : "")), 
										 normalizeTextForXML(day + " " + timeFrame), "", "", 
										 p.getY(), p.getX(), //the lat and lng in ArcGIS Geometry are reversed: lat = y, lng = x
										 type, normalizeTextForXML(address), "", System.currentTimeMillis());
				}
				else {
					break;
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	private String prepareCoordinates(Float lat, Float lng) {
	    //attempt to figure out current location of this driver and return that
	    if(lat == null || lng == null) {
			try {
			    prepareFetchDeviceCoordinatesByImeiStmt();
				fetchDeviceCoordinatesByImeiStmt.setString(1, caller);
				ResultSet rs = fetchDeviceCoordinatesByImeiStmt.executeQuery();
				if(rs.next()) {
					lat = rs.getFloat("currentlat");
				    lng = rs.getFloat("currentlong");
				}
				rs.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	    //if coordinates are specified, then transcode them into the accepted ones and modify the coordinates param
	    if(lat != null && lng != null) {
	        Point transcodedCoords = locator.projectPointFromGPSTo2100(lat, lng);
	        return String.format(COORD_PARAMS, transcodedCoords.getY(), transcodedCoords.getX());
	    }
		return null;
	}

	/**
	 * Fetches live poi (pharmacies, hospitals) for specified coordinates (or all, if no coordinates are specified) and returns them as an XML ready to be returned as a response.
	 * @param type
	 * @param lat
	 * @param lng
	 * @return
	 * @throws Exception
	 */
	public String fetchLivePoiXML(Hotspots.EventTypes type, Float lat, Float lng) throws Exception {
		String coordinates = prepareCoordinates(lat, lng);
		try {
			String catsub = null;
			int nodesToKeep = 0;
			switch (type) {
				case pharmacy:
					catsub = PHARMACY_CATSUB;
					nodesToKeep = getNumberFromProps("max_pharmacies_to_fetch", DEFAULT_NODES_TO_KEEP);
					break;
				case hospital:
					catsub = HOSPITAL_CATSUB;
					nodesToKeep = getNumberFromProps("max_hospitals_to_fetch", DEFAULT_NODES_TO_KEEP);
					break;
				default:
					throw new RuntimeException("category_not_supported");
			}
			//only run this when coordinates are provided
			String liveJson = util.getContent(String.format(LIVE_DATA_URL, (coordinates != null? coordinates : ""), catsub), "UTF8");
			return iResToEventsXML(liveJson, type, nodesToKeep);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	int pharmacySleepTimeMin = 0;
	String pharmacies;
	@Deprecated
	class PharmaciesFetcher extends Thread {

		public void run() {
			boolean crawl = Main.RUNNING;
			while(crawl) {
				pharmacySleepTimeMin = getNumberFromProps("pharmacy_update_delay", DEFAULT_SLEEP_TIME_MIN);
				crawl = pharmacySleepTimeMin > -1 ? Main.RUNNING : false;
				if(crawl) {
					try {
						long sleepTime = 10; //retry in 10 seconds
						if(comm != null) {
							pharmacies = "";
							pharmacies = fetchPharmaciesXML(null, null);
							//messages to drivers are sent by EventDispatcher
							sleepTime = pharmacySleepTimeMin * 60;
						}
						sleep(sleepTime * 1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Fetches pharmacies for specified coordinates (or all, if no coordinates are specified) and returns them as an XML ready to be returned as a response.
	 * @param lat
	 * @param lng
	 * @return
	 * @throws Exception
	 * @deprecated use fetchLivePoiXML
	 */
	@Deprecated
	private String fetchPharmaciesXML(Float lat, Float lng) throws Exception {
		String coordinates = prepareCoordinates(lat, lng);
		//if coordinates are specified, then transcode them into the accepted ones and modify the coordinates param
		if(lat != null && lng != null) {
		    Point transcodedCoords = locator.projectPointFromGPSTo2100(lat, lng);
		    coordinates = String.format(COORD_PARAMS, transcodedCoords.getY(), transcodedCoords.getX());
		}
		try {
			//only run this when coordinates are provided
			String livePharmaciesJson = util.getContent(String.format(LIVE_DATA_URL, (coordinates != null? coordinates : ""), PHARMACY_CATSUB), 
														"UTF8");
			return iResToEventsXML(livePharmaciesJson, Hotspots.EventTypes.pharmacy, DEFAULT_NODES_TO_KEEP);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	int hospitalSleepTimeMin = 0;
	String hospitals;
	@Deprecated
	class HospitalsFetcher extends Thread {

		public void run() {
			boolean crawl = Main.RUNNING;
			while(crawl) {
				hospitalSleepTimeMin =
									getNumberFromProps("hospital_update_delay", DEFAULT_SLEEP_TIME_MIN);
				crawl = hospitalSleepTimeMin > -1 ? Main.RUNNING : false;
				if(crawl) {
					try {
						long sleepTime = 10; //retry in 10 seconds
						if(comm != null) {
							hospitals = "";
							String liveHospitalsJson = util.getContent(LIVE_DATA_URL + HOSPITAL_CATSUB, "UTF8");
							hospitals = iResToEventsXML(liveHospitalsJson, Hotspots.EventTypes.hospital, DEFAULT_NODES_TO_KEEP);
							//messages to drivers are sent by EventDispatcher
							sleepTime = hospitalSleepTimeMin * 60;
						}
						sleep(sleepTime * 1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	@Deprecated
	public String getPharmacies() {
		return pharmacies;
	}
	
	@Deprecated
	public String getHospitals() {
		return hospitals;
	}
}
