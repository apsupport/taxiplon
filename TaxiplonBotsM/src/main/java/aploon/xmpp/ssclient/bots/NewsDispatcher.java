package aploon.xmpp.ssclient.bots;


import aploon.Main;
import aploon.xmpp.ssclient.Messages;

import com.taxiplon.rss.InGr;
import com.taxiplon.rss.model.FeedMessage;

import java.sql.Timestamp;
import java.util.List;


public class NewsDispatcher extends Dispatcher {
	
	static final int DEFAULT_SLEEP_TIME_MIN = 10;
	
	private static NewsDispatcher instance;
	
	public static synchronized NewsDispatcher getInstance() {
		if(instance == null) {
			instance = new NewsDispatcher();
		}
		return instance;
	}
	
	int inGrSleepTimeMin = 0;
	String feedMessages;
	
	private NewsDispatcher() {
		super();
		new Thread(new InGrRssAtomFeed(),"ingrparser_scheduler").start();
	}

	class InGrRssAtomFeed implements Runnable {

		public void run() {
		    while(Main.RUNNING) {
			    inGrSleepTimeMin = getNumberFromProps("in_gr_update_delay", DEFAULT_SLEEP_TIME_MIN);
				try {
				    int sentToCounter = 0;
					int msgSentCounter = 0;
					List<FeedMessage> feed = new InGr().parseRSSandReturnList();
					feedMessages = "";
					msgSentCounter = feed.size();
					for(FeedMessage i : feed) {
						long pubDate = i.getPubDate().getTime();
						feedMessages += String.format(getXML("MSG"), pubDate, pubDate, normalizeTextForXML(i.getTitle()), null, Messages.Statuses.act.toString());
					}
				    if(comm != null) {                      
						try {
							sentToCounter = Messages.getInstance().sendMessageToDriver(null, null, 
																					   String.format(getXML("MULTIPLE_MSGS_WRAPPER"), feedMessages), false).size();
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					}
					reports.put(this.getClass().getSimpleName(), (msgSentCounter > 0? msgSentCounter + " news were sent to " + sentToCounter + " devices. " : "") +
																 "<br/>Next dispatch at " + TIME_FORMAT.format(new Timestamp(System.currentTimeMillis() + inGrSleepTimeMin * 60 * 1000)));
					Thread.sleep(inGrSleepTimeMin * 60 * 1000);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getFeedMessages() {
		return feedMessages == null? "" : feedMessages;
	}
}
