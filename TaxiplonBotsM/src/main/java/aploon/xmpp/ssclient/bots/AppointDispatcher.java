package aploon.xmpp.ssclient.bots;


import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.apache.log4j.Logger;

import aploon.Main;
import aploon.datahelper.DataHelper;
import aploon.sms.SMSSUtil;
import aploon.xmpp.ssclient.StatusCheck;
import aploon.xmpp.ssclient.iphone.Push;
import aploon.xmpp.ssclient.iphone.PushSender;
import aploon.xmpp.ssclient.web.ListAppoints;

import com.taxiplon.Util;


public class AppointDispatcher extends Dispatcher {

    public static final String APPOINT_PROPS_KEY_PREFIX = "appoint";

    private static AppointDispatcher instance;

    private static final Logger LOGGER = Logger.getLogger(AppointDispatcher.class);
    
    public static synchronized AppointDispatcher getInstance() throws Exception {
    	if (instance == null) {
    		instance = new AppointDispatcher();
    	}
    	return instance;
    }

    public static final int DEFAULT_SLEEP_INTERVAL = 5;
    public static final int DEFAULT_REMIND_AMOUNT = 2;
    int dispatchSleepTimeMin = 0;
    int informSleepTimeMin = 0;
    int remindSleepTimeMin = 0;
    int numberOfDriverPush = 0;
    int informUnassignedSleepTimeMin = 0;

    private PreparedStatement findAppointsPendingDispatchStmt;
    private PreparedStatement findAppointsPendingInformStmt;
    private PreparedStatement findAppointsUnassignedPendingInformStmt;
    private PreparedStatement findAppointsPendingRemindStmt;
    private PreparedStatement getAppointDataStmt;
    private CallableStatement makeAppointTrip;

    private AppointDispatcher() throws Exception {
    	super();
    	Thread appointDispatch = new AppointDispatch();//.start();
    	appointDispatch.setName("appointdispatch_scheduler");
    	appointDispatch.start();
    	
    	Thread appointInform = new AppointInform();//.start();
    	appointInform.setName("appointinform_scheduler");
    	appointInform.start();
    	
    	Thread appointRemind = new AppointRemind();//.start();
    	appointRemind.setName("appointremind_scheduler");
    	appointRemind.start();
    	
    	Thread appointUnassigned = new AppointUnassignedInform();//.start();
    	appointUnassigned.setName("appointunassigned_scheduler");
    	appointUnassigned.start();
    }

    private void prepareGetAppointDataStmt() throws Exception {
    	if (getAppointDataStmt == null || getAppointDataStmt.isClosed()) {
    		getAppointDataStmt = dataPool.prepareStatement(getQuery("AppointInfoById"));
    	}
    }

    private void prepareMakeAppointTripCallStmt() throws Exception {
    	if (makeAppointTrip == null || makeAppointTrip.isClosed()) {
    		makeAppointTrip = dataPool.getConnection().prepareCall(getQuery("makeAppointTrip"));
    	}
    }

    public int markAppointsAsInformed(String listOfInformedAppointId) throws Exception {
    	//mark all processed appointments as informed
    	if (listOfInformedAppointId.length() != 0) {
    		return dataPool.executeUpdate(String.format(getQuery("markProcessedAsInformed"), listOfInformedAppointId));
    	}
    	return 0;
    }

    public static void sendPushNotificationForAppoint(String extra, String locale, String status,String brand,
						      String... messageFormatArgs) throws Exception {
    	if (extra != null) {
    		String localizedMessageToPassenger =
    				String.format(Util.getProperty(APPOINT_PROPS_KEY_PREFIX + "_" + status + "_" +
    						(locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE), "lang"),
    						messageFormatArgs);
    		String localizedTitleToPassenger =
    				Util.getProperty(APPOINT_PROPS_KEY_PREFIX + "_" + status + "_title_" + (locale != null ? locale :
											       Push.DEFAULT_PASSENGER_LOCALE),"lang");
    		
    		PushSender.getInstance(brand).sendPushNotification(extra, localizedMessageToPassenger, APPOINT_PROPS_KEY_PREFIX + status,
						    localizedTitleToPassenger, null, 1, null);
    	}
    }
    
    public static void sendPushNotificationForAppointDriver(String extra, String locale, String status,String brand,
							    String... messageFormatArgs) throws Exception {
    	try {
    		if (extra != null) {
    			String localizedMessageToDriver = 
    					String.format(Util.getProperty(APPOINT_PROPS_KEY_PREFIX + "_driver_" + status + "_" +
    							(locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE), "lang"),
    							messageFormatArgs);
    			String localizedTitleToDriver =
    					Util.getProperty(APPOINT_PROPS_KEY_PREFIX + "_driver_" + status + "_title_" + (locale != null ? locale :
														 Push.DEFAULT_PASSENGER_LOCALE),
						   "lang");
    			PushSender.getInstanceDriver(brand).sendPushNotification(extra, localizedMessageToDriver, APPOINT_PROPS_KEY_PREFIX + status,
    					localizedTitleToDriver, null, 1, Util.getProperty("push_sound", "config"));
    		}
    	} catch (Exception e) {
    		LOGGER.error("Appoint error",e);
    	}
    }

    public static void sendSMSNotificationForAppoint(Long companyId,String extra, String locale, String mobFrom, String mobTo,
						     String status,String country,String abrev,String... messageFormatArgs) {
    	//ekos check extra so we don't send sms to iphone , they will only get push notif
    	
    	LOGGER.debug("SMD Will send "+companyId+" "+extra+" "+locale+" "+mobFrom+" "+mobTo+" "+
			     status+" "+country+" "+abrev+" ");
    	
    	if (mobTo == null){
    		LOGGER.debug("SMD sms no set return");
    		return;
    	}
    		
    	String localizedMessageToPassenger = String.format(Util.getProperty(APPOINT_PROPS_KEY_PREFIX + "_" + status + "_" + (locale != null ? locale :
											   Push.DEFAULT_PASSENGER_LOCALE), "lang"), messageFormatArgs);

    	LOGGER.debug("SMD Will send sms from "+companyId+" "+mobTo+" "+localizedMessageToPassenger);
    		
    	SMSSUtil smsProvider = new SMSSUtil(companyId);
    	smsProvider.setCountryCode(country);
    		
    	if(smsProvider.isEnabled()) {
    			
    		smsProvider.sendSmsAsync(mobTo, localizedMessageToPassenger);
    	} else {
    		//SMSSender sms=SMSSender.getInstance(abrev);
    		//sms.sendSMS(mobTo, mobFrom, localizedMessageToPassenger,country);
    		SMSSUtil.sendDefaultSmsAsync(mobTo, localizedMessageToPassenger, mobFrom);
    	}
        
    }
   

    public void makeAppointTrip(Long appointId) throws Exception {
    	prepareMakeAppointTripCallStmt();
    	String response = null;
    	int i = 1;
    	makeAppointTrip.setLong(i++, appointId);
    	makeAppointTrip.registerOutParameter(i++, Types.NUMERIC); //driver
    	makeAppointTrip.registerOutParameter(i++, Types.NUMERIC); //trip
    	makeAppointTrip.registerOutParameter(i++, Types.NUMERIC); //passenger
    	makeAppointTrip.registerOutParameter(i++, Types.NUMERIC); //vehicle
    	makeAppointTrip.registerOutParameter(i++, Types.NUMERIC); //device
    	makeAppointTrip.registerOutParameter(i++, Types.VARCHAR); //imei
    	makeAppointTrip.execute();
    	Long tripid = makeAppointTrip.getLong("tripIdOut");
    	Long driverid = makeAppointTrip.getLong("driverIdOut");
    	Long passId = makeAppointTrip.getLong("passIdOut");

    	if (tripid == 0L || driverid == 0L) {
    		makeAppointTrip.close();
    		dataPool.closeConnection();
    		throw new Exception("invalid_operation");
    	}

    	Long appointCallType = DataHelper.getAppointCallType(appointId);
    	
    	try {
    		DataHelper.setTripCallType(tripid, appointCallType);
    	} catch(SQLException e) {
    		LOGGER.info("There was an sql exception",e);
    	}
    	
    	FareDispatcher.getInstance().dispatchTripAssignment(tripid);
    	StatusCheck.PassengerStatus pass = StatusCheck.getInstance().checkPassengerStatus(passId);
    	String statusPass = String.format(getXML("PASS_STATUS_XML"), StatusCheck.getInstance().getAppURL(),Boolean.toString(pass.isProper()), pass.getBlacklistIn(), pass.getDiscountRides(),
				 pass.getStars(), 0,pass.getCompanyEmail(),pass.getCompanyFacebook(),pass.getCompanyTwitter(), pass.getCallXML(), "");

    	sendAppointInformToPassenger(appointId, "trip", statusPass);
    	makeAppointTrip.close();
    	dataPool.closeConnection();

    }

    public void sendAppointInformToPassenger(Long appointId, String status, String... callInfo) throws Exception {
    	prepareGetAppointDataStmt();
    	getAppointDataStmt.setLong(1, appointId);
    	ResultSet rs = getAppointDataStmt.executeQuery();
    	if (rs.next()) {
    		Long passId = rs.getLong("passengerid");
    		String extra = rs.getString("extra"); //this will hold the token if the user has iphone
    		String softver = rs.getString("softwareversion");
    		String locale = rs.getString("locale"); //this will hold the locale of the passenger application
    		String drivername = rs.getString("drivername");
    		String driversurname = rs.getString("driversurname");
    		String mobile = rs.getString("mobile");
    		String conPhone = rs.getString("contactphone");
    		Timestamp requestdate = rs.getTimestamp("requestdatetime");
    		String country=rs.getString("OriginCountry");
    		String brand=rs.getString("brandname");
    		Long company=rs.getLong("taxicompanyid");
    		comm.sendMessageToPassenger(String.format(getXML("APPOINT_PENDING_INFORM"), appointId, requestdate.getTime(),
    				status, drivername, driversurname, callInfo == null ? "" : callInfo[0]),
					passId);
    		sendPushNotificationForAppoint(extra == null ? null : extra + "@" + softver, locale, status,brand);
    		//ekos added sms notifs for appoint
    		LOGGER.debug("SMD Will send");
    		sendSMSNotificationForAppoint(company ,extra, locale, null, getSmsMobile(conPhone, mobile), status,country,country+((brand==null || company==null)?"":company));
        }
    	rs.close();
    	getAppointDataStmt.close();
    }

    public void sendAppointInformToDriver(String imei, Long appointId, Long requestTime, 
    		String status, String name,String surname) throws Exception {
    	comm.sendMessageToDriver(String.format(getXML("APPOINT_PENDING_INFORM"), appointId, requestTime, status, name,surname, ""), imei);
    }
    /*
	 * Thread for expiring unassigned appointments
	*/

    class AppointUnassignedInform extends Thread {

	public void run() {

	    informUnassignedSleepTimeMin =
			   getNumberFromProps("appoint_informunassigned_delay", DEFAULT_SLEEP_INTERVAL);

	    while (Main.RUNNING) {
		try {
		    if (comm != null) {

			findAppointsUnassignedPendingInformStmt =
						     dataPool.prepareStatement(getQuery("findAppointsUnassignedPendingInform"));
			ResultSet rs = findAppointsUnassignedPendingInformStmt.executeQuery();
			String listOfAppointId = "";
			while (rs.next()) {
			    long appointid = rs.getLong("appointmentid");
			    listOfAppointId += (listOfAppointId.length() == 0 ? "" : ", ") + appointid;

			}
			rs.close();
			findAppointsUnassignedPendingInformStmt.close();
			if (listOfAppointId.length() != 0) {
			    int rowsAffected =dataPool.executeUpdate(String.format(getQuery("markPendingAsCancelled"), listOfAppointId));
			    reports.put(getClass().getSimpleName(),
					"Marked as expired " + rowsAffected + " unassigned appointments (id's: " + listOfAppointId +
					")");
			}
			dataPool.closeConnection();

		    }
		    Thread.sleep(informUnassignedSleepTimeMin * 60 * 1000);


		} catch (Exception e) {
		    e.printStackTrace();
		    LOGGER.error("Appoint error",e);
		}
	    }

		}
    }
    
    /*
	 * Thread for dispatching pending appointments to the drivers.
	 * The appointment is sent to the driver selected by the passenger.
	 */

    class AppointDispatch extends Thread {
    	public void run() {
    		dispatchSleepTimeMin = getNumberFromProps("appoint_dispatch_delay", DEFAULT_SLEEP_INTERVAL);
    		numberOfDriverPush = getNumberFromProps("appoint_driver_push_times", 1);
    		while (Main.RUNNING) {
    			try {
    				if (comm != null) {
    					findAppointsPendingDispatchStmt = dataPool.prepareStatement(getQuery("findAppointsPendingDispatch"));
    					ResultSet rs = findAppointsPendingDispatchStmt.executeQuery();
    					String listOfPushedAppointId = "";

    					while (rs.next()) {
    						long appointid = rs.getLong("appointmentid");
    						long passid = rs.getLong("passengerid");
    						float lat = rs.getFloat("pickuplat");
    						float lng = rs.getFloat("pickuplong");
    						String address = rs.getString("pickupaddress");
    						String destination = rs.getString("destination");
    						Timestamp requestdate = rs.getTimestamp("requestdatetime");
    						String driverimei = rs.getString("imei");
    						String special = rs.getString("specialneeds");
    						String passname = rs.getString("name");
    						String passsurname = rs.getString("lastname");
    						String drextra = rs.getString("extra");
    						String drlocale = rs.getString("locale");
    						int sentPush = rs.getInt("sentpush");
    						String brand =rs.getString("brandname");
    						String paymentType = rs.getString("paymenttype");
    						try {
    							comm.sendMessageToDriver(String.format(getXML("APPOINT_PENDING_DISPATCH"), appointid, lat, lng,
    									address != null ? address : "", destination != null ? destination : "", requestdate.getTime(),
    											special != null ? special : "", passname != null ? passname : "",
    													passsurname != null ? passsurname : "", paymentType != null ? paymentType : ""),
    													driverimei);
    							if (sentPush < numberOfDriverPush) {
    								//ekos send push notif for driver for appoint pending
    								sendPushNotificationForAppointDriver(drextra, drlocale, "pen",brand);
    								listOfPushedAppointId += (listOfPushedAppointId.length() == 0 ? "" : ", ") + appointid;

    							}
    						} catch (Exception e) {
    							e.printStackTrace();
    							LOGGER.error("Problem notifying ",e);
    						}
    					}
    					rs.close();
    					findAppointsPendingDispatchStmt.close();
    					if (listOfPushedAppointId.length() > 0) {
    						int rowsAffected = dataPool.executeUpdate(String.format(getQuery("markProcessedAsPushed"), listOfPushedAppointId));
    					}
    					dataPool.closeConnection();
    				}
    				//TODO 
    				//mandatory fix it 
    				//Thread.sleep(dispatchSleepTimeMin * 60 * 1000);
    				LOGGER.info("the time is "+dispatchSleepTimeMin * 1 * 1000);
    				Thread.sleep(dispatchSleepTimeMin * 1 * 1000);
    			} catch (Exception e) {
    				LOGGER.error("Problem dispatching",e);
    				e.printStackTrace();
    			}
    		}
    	}
    }

    /*
	 * Thread for informing the passenger at some point about the status of his request, whether the driver accepted his appointment or not.
	 */

    class AppointInform extends Thread {
	public void run() {
	    informSleepTimeMin = getNumberFromProps("appoint_inform_delay", DEFAULT_SLEEP_INTERVAL);
	    while (Main.RUNNING) {
		try {
		    try {
			if (comm != null) {
			    //only find 'pen'ding because these are the expired ones
			    findAppointsPendingInformStmt =
							 dataPool.prepareStatement(getQuery("findAppointsPendingInform"));
			    ResultSet rs = findAppointsPendingInformStmt.executeQuery();
			    String listOfInformedAppointId = "";
			    String pendingAppointsList = "";
			    while (rs.next()) {
				long appointid = rs.getLong("appointmentid");
				long passid = rs.getLong("passengerid");
				Timestamp requestdate = rs.getTimestamp("requestdatetime");
				String status = rs.getString("status");
				//								ListAppoints.Statuses status = ListAppoints.Statuses.valueOf(rs.getString("status"));
				String driverimei = rs.getString("imei");
				String drivername = rs.getString("drivername");
				String driversurname = rs.getString("driversurname");
				String passname = rs.getString("passname");
				String passsurname = rs.getString("passsurname");
				String extra = rs.getString("extra");
				String softver = rs.getString("softwareversion");

				String locale = rs.getString("locale");
				String mobile = rs.getString("mobile");
				String conPhone = rs.getString("contactphone");
				String country =rs.getString("OriginCountry");
				Long company=rs.getLong("taxicompanyid");
				String brand=rs.getString("brandname");
				//if the status at the stage of INFORM is still pen(ding) (which means that the driver didn't take an action on it), the appointment is marked as expired
				try {
				    //									if(status == ListAppoints.Statuses.pen || status == ListAppoints.Statuses.acc || status == ListAppoints.Statuses.dec) {
				    //										//in the first three cases, inform passenger
				    //										if(status == ListAppoints.Statuses.pen) {//as of 25/9/2011 upon accept or decline the passengerid is informed straight away
				    //these appoints will be marked as expired
				    pendingAppointsList += (pendingAppointsList.equals("") ? "" : ", ") + appointid;
				    status = ListAppoints.Statuses.exp.toString();
				    comm.sendMessageToPassenger(String.format(getXML("APPOINT_PENDING_INFORM"), appointid, requestdate.getTime(),
									      status, drivername, driversurname, ""),
								passid);
				    //										}
				    //									}
				    sendPushNotificationForAppoint(extra == null ? null : extra + "@" + softver, locale,
								   status,brand);
				    //ekos added sms notifs for appoint
				    LOGGER.debug("SMD Will send");
				    sendSMSNotificationForAppoint(company ,extra, locale, null, getSmsMobile(conPhone, mobile),
						  status,country,country+((brand==null || company==null)?"":company));
				    /*
									 * Driver will be informed anyway
									 *
									else if(status == ListAppoints.Statuses.can) {
										//if cancelled, inform the driver
										comm.sendMessageToDriver(String.format(getXML("APPOINT_PENDING_INFORM"), appointid,
																			   requestdate.getTime(), status.toString(), passname, passsurname),
																 driverimei);
									}
									 */
				} catch (Exception e) {
				    e.printStackTrace();
				    LOGGER.error("Problem informing ",e);
				}
				/*
								 * Sync driver device with (dis)appointment
								 */
				try {
				    sendAppointInformToDriver(driverimei, appointid, requestdate.getTime(), status, passname,
							      passsurname);
				    listOfInformedAppointId +=
	(listOfInformedAppointId.length() == 0 ? "" : ", ") + appointid;
				} catch (Exception e) {
				    e.printStackTrace();
				    LOGGER.error("Problem ",e);
				}
			    }
			    rs.close();
			    findAppointsPendingInformStmt.close();
			    //mark pending entries as exp(ired)
			    if (pendingAppointsList.length() != 0) {
				int rowsAffected =
	    dataPool.executeUpdate(String.format(getQuery("markPendingAsCancelled"), pendingAppointsList));
				reports.put(getClass().getSimpleName(),
					    "Marked as expired " + rowsAffected + " pending appointments (id's: " + pendingAppointsList +
					    ")");
			    }
			    try {
				int rowsAffected = markAppointsAsInformed(listOfInformedAppointId);
				reports.put(getClass().getSimpleName(),
					    "Marked as informed " + rowsAffected + " appointments (id's: " + listOfInformedAppointId +
					    ")");
			    } catch (Exception e) {
			    	e.printStackTrace();
			    	LOGGER.error("Problem marking informed ",e);
			    }
			    dataPool.closeConnection();
			}
		    } catch (Exception e) {
		    	e.printStackTrace();
		    	LOGGER.error("Problem with sleep ",e);
		    }
		 
		    Thread.sleep(informSleepTimeMin * 60 * 1000);
		    
		} catch (Exception e) {
		    e.printStackTrace();
		    LOGGER.error("Problem witgh exception ",e);
		}
	    }
	}
    }

    /*
    * Thread for reminding the driver about an upcoming appointment.
    */
    class AppointRemind extends Thread {
    	public void run() {
    		remindSleepTimeMin = getNumberFromProps("appoint_remind_delay", DEFAULT_SLEEP_INTERVAL);
    			while (Main.RUNNING) {
    				try {
    					try {
    						if (comm != null) {
    							findAppointsPendingRemindStmt =
    									dataPool.prepareStatement(getQuery("findAppointsPendingRemind"));
    							findAppointsPendingRemindStmt.setLong(1,
    									getNumberFromProps("remind_amount", DEFAULT_REMIND_AMOUNT));
    							ResultSet rs = findAppointsPendingRemindStmt.executeQuery();
    							String listOfRemindedAppointId = "";
    							while (rs.next()) {
    								long appointid = rs.getLong("appointmentid");
    								Timestamp requestdate = rs.getTimestamp("requestdatetime");
    								Timestamp localdate = rs.getTimestamp("localdatetime");

    								float lat = rs.getFloat("pickuplat");
    								float lng = rs.getFloat("pickuplong");
    								String address = rs.getString("pickupaddress");
    								String destination = rs.getString("destination");
    								String special = rs.getString("specialneeds");
    								String driverimei = rs.getString("imei");
    								Long passengerid = rs.getLong("passengerid");
    								String passname = rs.getString("passname");
    								String passsurname = rs.getString("passsurname");
    								String drivername = rs.getString("drivername");
    								String driversurname = rs.getString("driversurname");
    								String extra = rs.getString("extra");
    								String softver = rs.getString("softwareversion");

    								String locale = rs.getString("locale");
    								String dextra = rs.getString("drextra");
    								String dlocale = rs.getString("drlocale");
    								String mobile = rs.getString("mobile");
    								String conPhone = rs.getString("contactphone");
    								String country = rs.getString("origincountry");
    								Long company=rs.getLong("taxicompanyid");
    								String brand=rs.getString("brandname");

    								try {
    									comm.sendMessageToDriver(String.format(getXML("APPOINT_PENDING_REMIND"), appointid, lat, lng,
    											address != null ? address : "", destination == null ? "" : destination, requestdate.getTime(),
    													special != null ? special : "", passname != null ? passname : "", passsurname != null ? passsurname : ""),
    													driverimei);
    									comm.sendMessageToPassenger(String.format(getXML("APPOINT_PENDING_REMIND"), appointid, lat, lng,
    											address != null ? address : "", destination == null ? "" : destination, requestdate.getTime(),
    													special != null ? special : "", drivername != null ? drivername : "",
    															driversurname != null ? driversurname : ""),
    															passengerid);
    									sendPushNotificationForAppoint(extra == null ? null : extra + "@" + softver, locale, "rem",
    											TIME_FORMAT.format(requestdate));
    									//ekos added sms notifs for appoint
    									LOGGER.debug("SMD Will send");
    									sendSMSNotificationForAppoint(company ,extra, locale, null, getSmsMobile(conPhone,mobile), "rem",
    											country,country+((brand==null || company==null)?"":company),TIME_FORMAT.format(localdate==null?requestdate:localdate));
    									
    									//ekos added push notif for driver reminder
    									sendPushNotificationForAppointDriver(dextra, dlocale, "rem", TIME_FORMAT.format(localdate==null?requestdate:localdate),
    											address);

    									listOfRemindedAppointId +=
    											(listOfRemindedAppointId.length() == 0 ? "" : ", ") + appointid;
    								} catch (Exception e) {
    									e.printStackTrace();
    									LOGGER.error("",e);
    								}
    							}
    							rs.close();
    							findAppointsPendingRemindStmt.close();
    							//mark all processed appointments as informed
    							if (listOfRemindedAppointId.length() != 0) {
    								int rowsAffected =
    										dataPool.executeUpdate(String.format(getQuery("markProcessedAsReminded"), listOfRemindedAppointId));
    								reports.put(getClass().getSimpleName(),
    										"Marked as informed " + rowsAffected + " appointments (id's: " + listOfRemindedAppointId +
    										")");
    							}
    							dataPool.closeConnection();
    						}
    					} catch (Exception e) {
    						e.printStackTrace();
    						LOGGER.error(e);
    					}
    					Thread.sleep(remindSleepTimeMin * 60 * 1000);
    				} catch (Exception e) {
    					e.printStackTrace();
    					LOGGER.error(e);
    				}
    			}
			}
    }
    
    private String getSmsMobile(String contactPhone,String mobile) {
    	
    	if(contactPhone!=null) {
    		return contactPhone;
    	} else if(mobile!=null) {
    		return mobile;
    	}
    	
    	return null;
    }
    
}
