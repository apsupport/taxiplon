package aploon.xmpp.ssclient;


import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;
import org.json.JSONArray;
import org.json.JSONException;

import com.taxiplon.Util;

import aploon.concurrency.util.NamingThreadFactory;
import aploon.datahelper.DataHelper;
import aploon.model.Device;
import aploon.model.Driver;
import aploon.model.Message;
import aploon.xmpp.Communicator;
import aploon.xmpp.ServerSideClient;

/*
 * When web intreface sends messages to drivers
 */

public class Messages extends ServerSideClient {
    
    private static Messages instance;
    private Executor executor;
    //= Executors.newFixedThreadPool(1);

    private static final Logger LOGGER = Logger.getLogger(Messages.class);
    
    public static synchronized Messages getInstance() throws Exception {
    	
    	if (instance == null) {
    		instance = new Messages();
    	}
    	
    	String messagesThreadsStr = Util.getProperty("message_threads", "config");
    	
    	Integer messagesThreadsNumber = 10;
    	
    	if(messagesThreadsStr!=null) {
    		messagesThreadsNumber = Integer.parseInt(messagesThreadsStr);
    	}
    	
    	LOGGER.info("The messages threads "+messagesThreadsNumber);
    	
    	instance.executor = Executors.newFixedThreadPool(messagesThreadsNumber, new NamingThreadFactory("messages_pool"));
    	
    	return instance;
    }

    private Messages() throws Exception {
    	super();
    }

    public static enum Action {
    	multiple,single
    }
    
    public static enum Statuses {
    	act,old,ack,del;
    }

    /*
    private void prepareFilteredMessagesPerCompany() throws Exception {
    	if (filteredMessagesPerCompany == null || filteredMessagesPerCompany.isClosed()) {
    		filteredMessagesPerCompany = new NamedParameterStatement(dataPool,String.format( getQuery("filteredMessagesPerCompany"),"0"));
    	}
    }

    private void prepareGetDriversDeviceStmt() throws Exception {
    	if (getDriversDeviceStmt == null || getDriversDeviceStmt.isClosed()) {
    		getDriversDeviceStmt = dataPool.prepareStatement(getQuery("getDriversDevice"));
    	}
    }*/
    
    public static String formatPrivateMessageToDriver(Long msgId, String text, Long driverId,String status) throws Exception {
    	return String.format(getXML("MSG"), msgId, System.currentTimeMillis(), normalizeTextForXML(text), driverId,status);
    }

    public void processRequest() {
    	
    	String failure = null;
    	Long msgId = null;
    
    	try {
    	   		
    		String actionStr = parseOptionalField("action", String.class);
    		Action action = null;
    		
    		if(actionStr!=null) action = Action.valueOf(actionStr);
    		else action = Action.single;
    		
    		Long taxicompanyId = Long.parseLong(request.get("compid"));
			msgId = parseOptionalField("msgid", Long.class);
			String status = request.get("status");
			String text = request.get("text");
			
    		if(action.equals(Action.multiple)) {
    			
    			String drivers = request.get("drivers");
    			
    			if(drivers!=null) {
    				
    				messageToMultipleDrivers(drivers,taxicompanyId, msgId, status, text);
               	} else {
               		response = String.format(getXML("UPDATE_MSG_OUTCOME"), msgId, failure == null ? "true" : "false", failure);       		
               		return;
               	}

    		} else if(action.equals(Action.single)) {
    		
    			Long driverId = parseOptionalField("driverid", Long.class);
    			
    			SingleDriverMessage singleDriverMessage = new SingleDriverMessage(msgId, taxicompanyId, driverId, status, text);
    			executor.execute(singleDriverMessage);
    			
    			/**
    			executor.execute(new Runnable() {
					
					@Override
					public void run() {
						
						LOGGER.info("The thread is "+Thread.currentThread().getId());
						
						
					}
				});*/
    			
    			/**
    			LOGGER.info("Trying to do some work with messages "+taxicompanyId+" "+msgId+" "+driverId+" "+status+" "+text);
    		
    			msgId = DataHelper.createOrUpdateMessage(msgId, taxicompanyId, driverId, status, text, false);
    		
    			if (status == null || Statuses.valueOf(status) != Statuses.ack) {
    				sendMessageToDriver(taxicompanyId, driverId, formatPrivateMessageToDriver(msgId, text, driverId, status),false);
    			}
    			*/
    		
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		failure = e.getMessage();
    	}
	
    	response = String.format(getXML("UPDATE_MSG_OUTCOME"), msgId, failure == null ? "true" : "false", failure);
    	
    }

    private class SingleDriverMessage implements Runnable {

    	private Long messageId;
    	private Long taxiCompanyId;
    	private Long driverId;
    	private String status;
    	private String text;
    	
    	public SingleDriverMessage(Long messageId,Long taxiCompanyId,Long driverId,String status,String text) {
    		
    		this.messageId = messageId;
    		this.taxiCompanyId = taxiCompanyId;
    		this.driverId = driverId;
    		this.text = text;
    		this.status = status;
    	}
    	
		@Override
		public void run() {
			
			LOGGER.info("The thread is "+Thread.currentThread().getId());
					
			try {
				messageId = DataHelper.createOrUpdateMessage(messageId, taxiCompanyId, driverId, status, text, false);
			
				if (status == null || Statuses.valueOf(status) != Statuses.ack) {
					try {
						sendMessageToDriver(taxiCompanyId, driverId, 
								formatPrivateMessageToDriver(messageId, text, driverId, status),
								false);
					} catch (Exception e) {
						LOGGER.error("There was an exception ",e);
					}
				}	
				
			} catch (SQLException e) {
				LOGGER.error("There was an exception",e);
			}
			
		}
  
    }
    
    private void messageToMultipleDrivers(final String drivers,final Long taxiCompanyId,final Long msgId,final String status,final String text) {
    
    	executor.execute(new Runnable() {
			@Override
			public void run() {
				
				try {
					
					LOGGER.info("The thread is "+Thread.currentThread().getId());
					
					LOGGER.info("The drivers are "+drivers);
					
					JSONArray jsonArray = new JSONArray(drivers);
					
					for(int i=0;i<jsonArray.length();i++) {
						String driverIdStr = jsonArray.getString(i);
						Long driverId = Long.parseLong(driverIdStr);
						LOGGER.info("Trying to do some work with messages "+taxiCompanyId+" "+msgId+" "+driverId+" "+status+" "+text);
						Long msgId = DataHelper.createOrUpdateMessage(null, taxiCompanyId, driverId, status, text, false);
						//msgId = DataHelper.createOrUpdateMessage(msgId, taxiCompanyId, driverId, status, text, false);
		    		
		    			if (status == null || Statuses.valueOf(status) != Statuses.ack) {
		    				sendMessageToDriver(
		    						taxiCompanyId, driverId, 
		    						formatPrivateMessageToDriver(msgId, text, driverId, status),false);
		    			}
		    			
		    			Thread.sleep(500L);
					}
				
				} catch(SQLException e) {
					LOGGER.error("SQL query problem",e);
				} catch (JSONException e) {
					LOGGER.error("Problem with json encoding",e);
				} catch(Exception e) {
					LOGGER.error("Unknown exception",e);
				}
				
			}
		});
	
    }
    
    /*
    private Long createOrUpdateMessage(Long msgId, Long taxicompanyId, Long driverId, String status, String text,boolean isOffline) throws Exception {
    	
    	prepareCreateOrUpdateMessage();
    	if (msgId != null) {
    		createOrUpdateMessage.setLong(1, msgId);
    	} else {
    		createOrUpdateMessage.setString(1, null);
    	}
    	if (taxicompanyId != null) {
    		createOrUpdateMessage.setLong(2, taxicompanyId);
    	} else {
    		createOrUpdateMessage.setString(2, null);
    	}
    	if (driverId != null) {
    		createOrUpdateMessage.setLong(3, driverId);
    	} else {
    		createOrUpdateMessage.setString(3, null);
    	}
    	createOrUpdateMessage.setString(4, text);
    	createOrUpdateMessage.setString(5, status);
    	createOrUpdateMessage.setString(6, isOffline ? "true" : "false");

    	createOrUpdateMessage.registerOutParameter(7, Types.NUMERIC);
    	createOrUpdateMessage.registerOutParameter(8, Types.TIMESTAMP);
    	createOrUpdateMessage.execute();
    	msgId = createOrUpdateMessage.getLong("messageIdOut");
    	dataPool.closeConnection();
    	
    	return msgId;
    
    }
    */

    public void dispatchMessage(Long msgId) throws Exception {
    	
    	try {
    		Message message = DataHelper.getMessageInfo(msgId);
    		//Statuses status = Statuses.valueOf(message.getStatus());
			String messageXml = String.format(getXML("MSG"), msgId,
					message.getMessageTime()!=null?message.getMessageTime().getTime():null,
					message.getMessageBody(),message.getDriverId(),message.getStatus());
			sendMessageToDriver(message.getTaxiCompanyId(), message.getDriverId(), messageXml, false);
    	} catch(SQLException e) {
    		LOGGER.error("Could not retrive message",e);
    	}
    	
    	/*
    	prepareFilteredMessagesPerCompany();
    	
    	String message = "";
    	//messageid must match exactly
    	filteredMessagesPerCompany.setLong("messageidArg", msgId);
    	//rest of parameters is null
    	//filteredMessagesPerCompany.setString("taxicompanyidArg", null);
    	filteredMessagesPerCompany.setString("driveridArg", null);
    	filteredMessagesPerCompany.setString("statusArg", null);
    	filteredMessagesPerCompany.setString("msgBodyArg", null);
    	filteredMessagesPerCompany.setString("msgTimeFromArg", null);
    	filteredMessagesPerCompany.setString("msgTimeToArg", null);
    	ResultSet rs = filteredMessagesPerCompany.executeQuery();
    	Long companyId = null;
    	Long msgDriverId = null;
    	if (rs.next()) { //single row result
    		try {
    			companyId = rs.getLong("taxicompanyid");
    			String msgBody = rs.getString("messagebody");
    			msgDriverId = rs.getLong("recipientdriverid");
    			Timestamp msgTime = rs.getTimestamp("messagetime");
    			Statuses status = Statuses.valueOf(rs.getString("status"));
    			message = String.format(getXML("MSG"), msgId, msgTime != null ? msgTime.getTime() : null, msgBody, msgDriverId, status.toString());
    			sendMessageToDriver(companyId, msgDriverId, message, false);
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	
    	rs.close();
    	dataPool.closeConnection();
    	*/
    }

    public void dispatchOfflineMessages(Long DriverID) throws Exception {
    	
    	List<Long> offlineMessageIds = DataHelper.getOfflineMessages(DriverID);
    	
    	String messageIds = "";
    	
    	for(Long messageId:offlineMessageIds) {
    		
    		dispatchMessage(messageId);
    		messageIds += (messageIds.equals("") ? "" : ", ") + messageId;
    		Thread.sleep(2000);
    	}

    	/*
    	prepareOfflineMessage();
    	String messageIds = "";
    	getOfflineMessagesStmt.setLong(1, DriverID);
    	ResultSet rs = getOfflineMessagesStmt.executeQuery();
    	long mesID = 0L;
    	while (rs.next()) {
    		try {
    			mesID = rs.getLong("MessageID");
    			dispatchMessage(mesID);
    			messageIds += (messageIds.equals("") ? "" : ", ") + mesID;
    			Thread.sleep(2000);
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	
    	rs.close();
    	*/
    	
    	if (!messageIds.equals("")) {
    		CallableStatement offMessages = dataPool.getConnection().prepareCall(String.format(getQuery("updateOfflineMessages"), messageIds));
    		offMessages.execute();
    	}
    	
    	dataPool.closeConnection();
    
    }

    public ArrayList<Long> sendMessageToDriver(Long companyId, Long driverId, String msgXML,boolean storeInDB) throws Exception {
	
    	ArrayList<Long> deviceIds = new ArrayList<Long>();
    	//prepareGetDriversDeviceStmt();
    	Long deviceId = null;
    	Long mesId = -1L;
    	String finalMessage = null;
    	//String deviceImei = null;
    	
    	if (driverId != null) { //private message to a driver
    	
    		Driver driver = DataHelper.getDriversDevice(driverId);
    		
    		//getDriversDeviceStmt.setLong(1, driverId);
    		//ResultSet rs = getDriversDeviceStmt.executeQuery();
    		
    		int iStart = 0, iEnd = 0;
    		
    		if(driver!=null) {
    			
    			if(storeInDB) {
    				
    				iStart = msgXML.indexOf("<text>") + "<text>".length();
    				iEnd = msgXML.indexOf("</text>");
    				
    				if (iStart != -1 && iEnd != -1 && iEnd > iStart) {
    				
    					String txt = msgXML.substring(iStart, iEnd);
    					
    					try {
    						mesId = DataHelper.createOrUpdateMessage(null, companyId, driverId, Statuses.act.toString(), txt,
    								driver.getDevice().getStatus().equals("off") 
    								||driver.getDevice().getStatus().equals("false"));
    								
    								/*
    								createOrUpdateMessage(null, companyId, driverId, Statuses.act.toString(), txt,
    								driver.getDevice().getStatus().equals("off") 
    								||driver.getDevice().getStatus().equals("false"));
    								*/
    								
    						finalMessage = String.format(getXML("MSG"), mesId, System.currentTimeMillis(), txt, driverId, Statuses.act.toString());
    					} catch (Exception e) {
    						e.printStackTrace();
    					}
    				}
    				
    			} else {
    			
    				iStart = msgXML.indexOf("<msgid>") + "<msgid>".length();
    				iEnd = msgXML.indexOf("</msgid>");
    				String msgIdio = msgXML.substring(iStart, iEnd);
    				
    				DataHelper.createOrUpdateMessage(Long.parseLong(msgIdio), null, null, null, null,
    						driver.getDevice().getStatus().equals("off") ||
    						driver.getDevice().getStatus().equals("false"));
    				
    				/*
    				createOrUpdateMessage(Long.parseLong(msgIdio), null, null, null, null,
    						driver.getDevice().getStatus().equals("off") ||
    						driver.getDevice().getStatus().equals("false"));
    				*/
    				
    			}
    			
    			LOGGER.info("trying to sent the message to driver "+driver.getDevice().getImei()+" f");
    			
    			if (comm.sendMessageToDriver(finalMessage == null ? msgXML : finalMessage, driver.getDevice().getImei())) {
    				deviceIds.add(deviceId);
    			}
    		} 
    		
    		/*
    		if (rs.next()) { //single row result
    			deviceId = rs.getLong("deviceid");
    			deviceImei = rs.getString("imei");
    		
    			if (storeInDB) {
    			
    				iStart = msgXML.indexOf("<text>") + "<text>".length();
    				iEnd = msgXML.indexOf("</text>");
    				
    				if (iStart != -1 && iEnd != -1 && iEnd > iStart) {
    				
    					String txt = msgXML.substring(iStart, iEnd);
    					
    					try {
    						mesId = createOrUpdateMessage(null, companyId, driverId, Statuses.act.toString(), txt, rs.getString("status").equals("off") ||rs.getString("active").equals("false"));
    						finalMessage = String.format(getXML("MSG"), mesId, System.currentTimeMillis(), txt, driverId, Statuses.act.toString());
    					} catch (Exception e) {
    						e.printStackTrace();
    					}
    				}
    			
    			} else {
    			
    				iStart = msgXML.indexOf("<msgid>") + "<msgid>".length();
    				iEnd = msgXML.indexOf("</msgid>");
    				String msgIdio = msgXML.substring(iStart, iEnd);
    				createOrUpdateMessage(Long.parseLong(msgIdio), null, null, null, null,rs.getString("status").equals("off") || rs.getString("active").equals("false"));
    			}
    			
    			if (comm.sendMessageToDriver(finalMessage == null ? msgXML : finalMessage, deviceImei)) {
    				deviceIds.add(deviceId);
    			}
    		}
    		
    		rs.close();
    		*/
    		
    	} else { //this is a public message to all drivers
    		
    		LOGGER.info("Will sent to all drivers ");
    		
    		List<Device> devices = DataHelper.getDriversDevicePerCompany(companyId);
    		
    		for(Device device:devices) {
    			
    			LOGGER.info("Will sent message to devices "+device.getImei()+" "+device.getDeviceId());
    			
    			if(comm.sendMessageToDriver(msgXML, device.getImei())) {
    				deviceIds.add(device.getDeviceId());
    			}
    		}
    		
    		/*
    		prepareDriversPerCompany();
    		driversPerCompanyStmt.setString(1, companyId != null ? companyId.toString() : null);
    		ResultSet rs = driversPerCompanyStmt.executeQuery();
    		
    		while (rs.next()) {
    			deviceId = rs.getLong("deviceid");
    			deviceImei = rs.getString("imei");
    			if (comm.sendMessageToDriver(msgXML, deviceImei)) {
    				deviceIds.add(deviceId);
    			}
    		}
    	
    		rs.close();
    		*/
    	}
    	
    	dataPool.closeConnection();
    	return deviceIds;
    
    }

    /**
     *
     * @param msg The message to be sent. Not more than 2048 char, everything over 500 char will not fit on one screen.
     * @throws Exception
     */
    private void sendMassMessage(String msg) throws Exception {
    	Communicator c = new Communicator("d2", "d2", "administrative");
    	ResultSet rs = dataPool.executeQuery("select driverid, imei from device d, driver dr where dr.deviceid = d.deviceid");
    	while (rs.next()) {
    		Long driverid = rs.getLong("driverid");
    		String message = String.format("compid=1&text=%s&driverid=%d&status=%s", URLEncoder.encode(msg, "UTF8"), driverid, "act");
    		c.sendMessage(message,"messages@driver.taxiplon.com"); //rs.getString("imei")//getXML("MSG"), System.currentTimeMillis(), System.currentTimeMillis()
    	}
    }

    //	protected String getResource() {
    //	    return "administrative";
    //	}

    public static void main(String[] args) {
    	try {
    		Messages m = new Messages();
    		//			m.listenForMessages();
    		m.sendMassMessage("���� �������������� ���������� �������������� ������������������ �������� ������ ������������������ ������ ������������ �� taxiplon: ���������� 10 �������������� �� �������������� ���������� (���������� 5 ��������)! " +
			      "��������������: �� ������������������ ������ ���������� �������������� ������ �������� ������������������������. ���������� �������������� ������ �������� ����������������������!");
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
    
}
