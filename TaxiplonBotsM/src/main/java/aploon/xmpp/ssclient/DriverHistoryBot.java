package aploon.xmpp.ssclient;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;

import aploon.concurrency.util.NamingThreadFactory;
import aploon.datahelper.DataHelper;
import aploon.model.DriverAssignedTrip;
import aploon.model.DriverClaimedTrip;
import aploon.model.TripAssignPicked;
import aploon.model.TripSummary;
import aploon.xml.model.DriverAssignedResponseXML;
import aploon.xml.model.DriverAssignedTripXml;
import aploon.xml.model.DriverClaimedTripXml;
import aploon.xml.model.LostTripsResponseXML;
import aploon.xml.model.TripPickedResponseXML;
import aploon.xml.model.TripPickedXML;
import aploon.xml.model.TripSummaryResponseXML;
import aploon.xml.model.TripSummaryXML;
import aploon.xmpp.ServerSideClient;

public class DriverHistoryBot extends ServerSideClient {

	private enum Action {
		assigned,claimed,trippicked,summary
	}; 
	
	private final ExecutorService executorService;
	
	private static final Logger LOGGER = Logger.getLogger(DriverHistoryBot.class);
	
	public DriverHistoryBot() {
		executorService = Executors.newFixedThreadPool(40,new NamingThreadFactory("driverhistory_pool"));
		//executorService = Executors.newCachedThreadPool();
		
	}
	
	@Override
	public void processRequest() {
		executorService.execute(new RequestController(request,from));
	}

	public static String getAssignedRides(Long driverId) throws Exception {
		
		DriverAssignedResponseXML responseXML = new DriverAssignedResponseXML();
		
		if(driverId!=null) {
			
			try {
			
				List<DriverAssignedTrip> driverAssignedTrips = DataHelper.getDriverAssignedTrips(driverId);	
				List<DriverAssignedTripXml> driverAssignedTripXmls = new ArrayList<DriverAssignedTripXml>();
				
				for(DriverAssignedTrip driverAssignedTrip:driverAssignedTrips) {
					
					DriverAssignedTripXml driverAssignedTripXML = new DriverAssignedTripXml(
							driverAssignedTrip.getTripId(),driverAssignedTrip.getHailTime().getTime(),
							driverAssignedTrip.getPickUpAddress(),driverAssignedTrip.getSpecialNeeds(),
							driverAssignedTrip.getStatus(),driverAssignedTrip.getFare(),
							driverAssignedTrip.getPaymentType(),driverAssignedTrip.getShortName());
					driverAssignedTripXmls.add(driverAssignedTripXML);
				}
				
				responseXML.setSuccess(true);
				responseXML.setMessage("Recieved the trips assigned");
				responseXML.setContent(driverAssignedTripXmls);
				
			} catch (SQLException e) {
				LOGGER.error("Problem with driver assign exceptions",e);
				responseXML.setSuccess(false);
				responseXML.setMessage("Problem with your query");
			}
		}
		
		return ServerSideClient.serializeObject(responseXML);
		
	}
	
	public static String getClaimedRides(Long driverId) throws Exception {

		LostTripsResponseXML responseXML = new LostTripsResponseXML();
		
		if(driverId!=null) {
			
			try {
				
				List<DriverClaimedTrip> driverClaimedTrips = DataHelper.getDriverClaimedTrips(driverId);
				List<DriverClaimedTripXml> driverClaimedXmls = new ArrayList<DriverClaimedTripXml>();
				
				for(DriverClaimedTrip driverClaimedTrip:driverClaimedTrips) {
					
					DriverClaimedTripXml driverLostTripXML = new DriverClaimedTripXml(
							driverClaimedTrip.getTripId(),
							driverClaimedTrip.getHailTime().getTime(),
							driverClaimedTrip.getPickUpAddress(),
							driverClaimedTrip.getShortName(),
							driverClaimedTrip.getComments(),
							driverClaimedTrip.getLatitude(),
							driverClaimedTrip.getLongtitude(),
							driverClaimedTrip.getSpecialNeeds());
					
					driverClaimedXmls.add(driverLostTripXML);
					
				}
				
				responseXML.setMessage("Request successfull");
				responseXML.setContent(driverClaimedXmls);
				responseXML.setSuccess(true);
				
			} catch (SQLException e) {
				LOGGER.error("Problem with sql query",e);
				responseXML.setSuccess(false);
				responseXML.setMessage("Problem with your query");	
			}
		}
	
		return ServerSideClient.serializeObject(responseXML);
	}
	
	public static String getTripPicked(Long tripId) throws Exception {
		
		TripPickedResponseXML responseXML = new TripPickedResponseXML();
		
		if(tripId!=null) {
			
			try {
			
				List<TripAssignPicked> tripAssignPickeds = DataHelper.getTripAssignPicked(tripId);
				List<TripPickedXML> tripPickedXMLs = new ArrayList<TripPickedXML>();
				
				for(TripAssignPicked tripAssignPicked:tripAssignPickeds) {
					
					
					TripPickedXML tripPickedXML = new TripPickedXML(
							tripAssignPicked.getTripId(), tripAssignPicked.getDeviceId(), tripAssignPicked.getDriverId(),
							tripAssignPicked.getCodeName(), tripAssignPicked.getLastName(), tripAssignPicked.getFirstName(),
							tripAssignPicked.getLatitude(), tripAssignPicked.getLongtitude(),
							tripAssignPicked.getDistance(), tripAssignPicked.getDistanceAssigned(),tripAssignPicked.getComments());
					
					tripPickedXMLs.add(tripPickedXML);
					
				}
				
				responseXML.setSuccess(true);
				responseXML.setMessage("Request Successful");
				responseXML.setContent(tripPickedXMLs);
				
			} catch (SQLException e) {
				LOGGER.error("Problem with sql query",e);
				responseXML.setSuccess(false);
				responseXML.setMessage("Problem with your query");
			}
		}
		
		return ServerSideClient.serializeObject(responseXML);
	}
	
	public static String getSummary(Long driverId) throws Exception {
	
		TripSummaryResponseXML responseXML = new TripSummaryResponseXML();
		
		if(driverId!=null) {
			
			try {
				List<TripSummary> tripSummaries = DataHelper.getDriverTripSummary(driverId);
				List<TripSummaryXML> tripSummaryXMLs = new ArrayList<>();
			
				for(TripSummary tripSummary:tripSummaries) {
				
					TripSummaryXML tripSummaryXML = new TripSummaryXML(
							tripSummary.getAssigned(),
							tripSummary.getLost(),
							tripSummary.getMonth(),
							tripSummary.getYear());
				
					tripSummaryXMLs.add(tripSummaryXML);
				}
		
				responseXML.setSuccess(true);
				responseXML.setMessage("Request Successful");
				responseXML.setContent(tripSummaryXMLs);
				
			} catch(SQLException e) {
				LOGGER.error("Problem with sql query",e);
				responseXML.setSuccess(false);
				responseXML.setMessage("Problem with your query");
			}
			
		}
		
		return ServerSideClient.serializeObject(responseXML);
	}
	
	private class RequestController implements Runnable {

		private HashMap<String, String> requestArguements;
		private String from;
		
		public RequestController(HashMap<String, String> requestArguments,String from) {
			this.requestArguements = requestArguments;
			this.from = from;
		}
		
		@Override
		public void run() {
			try {
				
				String response = null;
				
				if(requestArguements.containsKey("action")) {
			
					String actionStr = requestArguements.get("action");	
					Action action = Action.valueOf(actionStr);
				
					switch (action) {
					case assigned:
						response = DriverHistoryBot.getAssignedRides(Long.parseLong(requestArguements.get("driverId")));
								//parseOptionalField("driverId",Long.class));
						break;
					case claimed:
						response = DriverHistoryBot.getClaimedRides(Long.parseLong(requestArguements.get("driverId")));
								//parseOptionalField("driverId",Long.class));
						break;
					case trippicked:
						response = DriverHistoryBot.getTripPicked(Long.parseLong(requestArguements.get("tripId")));
								//parseOptionalField("tripId", Long.class));
						break;
					case summary:
						response = DriverHistoryBot.getSummary(Long.parseLong(requestArguements.get("driverId")));
								//parseOptionalField("driverId", Long.class));
						break;
					default:
						response = null;
						break;
					}
				}
				
				comm.sendMessage(response, from);
				
			} catch (XMPPException e) {
				LOGGER.error("Fatal cannot send message",e);
			} catch (JAXBException e) {
				LOGGER.error("Extreme problem jaxb problem",e);
			} catch(Exception e) {
				LOGGER.error("Problem with your data",e);
			}
		}
		
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
	
}
