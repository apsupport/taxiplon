package aploon.xmpp.ssclient;


import aploon.xmpp.ServerSideClient;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.jivesoftware.smack.packet.IQ;
/*
 * Only for passenger. And blocked drivers
 */
public class FavoriteDrivers extends ServerSideClient {

    PreparedStatement findFavoriteDriversPerPassengerStmt;
    PreparedStatement findAllDriversPerPassengerStmt;

    PreparedStatement findBlockedDriversPerPassengerStmt;

    PreparedStatement removeFavoriteDriverStmt;
    PreparedStatement removeBlockedDriverStmt;

    CallableStatement addFavoriteDriverStmt;
    CallableStatement blockDriverStmt;

    public FavoriteDrivers() {
    	super();
    }

    private void prepareFindFavoriteDriversPerPassengerStmt() throws Exception {
    	if (findFavoriteDriversPerPassengerStmt == null || findFavoriteDriversPerPassengerStmt.isClosed()) {
    		findFavoriteDriversPerPassengerStmt =
			   dataPool.prepareStatement(getQuery("findFavoriteDriversPerPassenger"));
    	}
    }

    private void prepareFindAllDriversPerPassengerStmt() throws Exception {
    	if (findAllDriversPerPassengerStmt == null || findAllDriversPerPassengerStmt.isClosed()) {
    		findAllDriversPerPassengerStmt = dataPool.prepareStatement(getQuery("findAllDriversPerPassenger"));
    	}
    }

    private void prepareFindBlockedDriversPerPassengerStmt() throws Exception {
    	if (findBlockedDriversPerPassengerStmt == null || findBlockedDriversPerPassengerStmt.isClosed()) {
    		findBlockedDriversPerPassengerStmt = dataPool.prepareStatement(getQuery("findBlockedDriversPerPassenger"));
    	}
    }

    private void prepareRemoveFavoriteDriverStmt() throws Exception {
    	if (removeFavoriteDriverStmt == null || removeFavoriteDriverStmt.isClosed()) {
    		removeFavoriteDriverStmt = dataPool.prepareStatement(getQuery("removeFavoriteDriver"));
    	}
    }

    private void prepareRemoveBlockedDriverStmt() throws Exception {
    	if (removeBlockedDriverStmt == null || removeBlockedDriverStmt.isClosed()) {
    		removeBlockedDriverStmt = dataPool.prepareStatement(getQuery("unblockDriver"));
    	}
    }

    private void prepareAddFavoriteDriverStmt() throws Exception {
    	if (addFavoriteDriverStmt == null || addFavoriteDriverStmt.isClosed()) {
    		addFavoriteDriverStmt = dataPool.getConnection().prepareCall(getQuery("addFavoriteDriver"));
    	}
    }

    private void prepareBlockDriverStmt() throws Exception {
    	if (blockDriverStmt == null || blockDriverStmt.isClosed()) {
    		blockDriverStmt = dataPool.getConnection().prepareCall(getQuery("blockDriver"));
		}
    }

    private String listBlockedDrivers(Integer passId) throws Exception {
    	String result = "";
    	prepareFindBlockedDriversPerPassengerStmt();
    	findBlockedDriversPerPassengerStmt.setInt(1, passId);
    	findBlockedDriversPerPassengerStmt.setInt(2, passId);
    	ResultSet rs = findBlockedDriversPerPassengerStmt.executeQuery();
    	Long prevDriver = 0L;
	    int iTot = 0;
	    while (rs.next()) {
	    	Long driverId = rs.getLong("driverid");
	        if (prevDriver == driverId)
	        	continue;
	        prevDriver = driverId;
	        String codename = rs.getString("codename");
	        String name = rs.getString("firstname");
	        String surname = rs.getString("lastname");
	        String platenumber = rs.getString("plateno");
	        String carmodel = rs.getString("model");
	        
	        result += String.format(getXML("BLACKLISTED_DRIVER"), 
	        		driverId, codename != null ? codename : "",surname != null ? surname : "", name != null ? name : "",
	        		platenumber == null ? "" : platenumber, carmodel == null ? "" : carmodel,
	        		"false", rs.getString("speaksenglish"),rs.getString("newvehicle"), rs.getString("bigteamveh"),
	        		rs.getString("supportdisabled"), rs.getString("hybrid"),rs.getString("wifi"), 
	        		rs.getString("smoker"), rs.getString("allowpets"), rs.getString("takecreditcards"),
	        		rs.getLong("totnofavs")).replace("null", "false") + "\n";
	            iTot++;
	    }
	    rs.close();
	    return result;
    }

    private String removeBlockedDriver(Integer passid, Integer driverid) throws Exception {
    	prepareRemoveBlockedDriverStmt();
    	removeBlockedDriverStmt.setInt(1, passid);
    	removeBlockedDriverStmt.setInt(2, driverid);
    	int affectedRows = 0;
    	try {
    		affectedRows = removeBlockedDriverStmt.executeUpdate();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	if (affectedRows == 0) {
    		throw new RuntimeException("no_rows_affected");
    	}
    	return String.format(getXML("REMOVE_BLACKLISTED_DRIVER"), listBlockedDrivers(passid)); //affectedRows
    }

    private String blockDriver(Integer passid, String codename) throws Exception {
    	prepareBlockDriverStmt();
    	int i = 1;
    	blockDriverStmt.setInt(i++, passid);
    	blockDriverStmt.setString(i++, codename);
    	blockDriverStmt.registerOutParameter(i++, Types.NUMERIC);
    	blockDriverStmt.registerOutParameter(i++, Types.VARCHAR);
    	blockDriverStmt.registerOutParameter(i++, Types.VARCHAR);
    	blockDriverStmt.registerOutParameter(i++, Types.VARCHAR);
    	blockDriverStmt.registerOutParameter(i++, Types.VARCHAR);
    	blockDriverStmt.registerOutParameter(i++, Types.VARCHAR);
    	try {
    		blockDriverStmt.execute();
    		long driverId = blockDriverStmt.getLong("driveridOut");
    		if (driverId == 0) { // driverid is null
    			throw new RuntimeException("no_rows_affected");
    		} else { // requested data was actually fetched
    			codename = blockDriverStmt.getString("codenameOut");
    			String firstname = blockDriverStmt.getString("firstnameOut");
    			String lastname = blockDriverStmt.getString("lastnameOut");
    			String plate = blockDriverStmt.getString("plateOut");
    			String model = blockDriverStmt.getString("modelOut");
    			return String.format(getXML("ADD_BLACKLISTED_DRIVER"),
				     String.format(getXML("BLACKLISTED_DRIVER"), driverId, codename, lastname, firstname,
						   plate == null ? "" : plate, model == null ? "" : model,"false","false","false","false","false","false","false","false","false","false",0));
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    		throw e;
    	}
    }

    enum Action {
    	add,
    	remove,
    	list,
    	listblocked,
    	block,
    	unblock,
    	listall;
    }

    public void processRequest() {
    	Boolean useBlocked = false;

    	try {
    		String passIdStr = request.get("passid");
    		if (passIdStr == null) {
    			throw new IllegalArgumentException("passid_missing");
    		}
    		Integer passId = null;
    		try {
    			passId = Integer.parseInt(passIdStr);
    		} catch (Exception e) {
    			throw new IllegalArgumentException("wrong_passid");
    		}
    		String action = request.get("action");
    		if (action == null) {
    			throw new IllegalArgumentException("action_missing");
    		} else if (Action.add.toString().equals(action)) {
    			String codename = request.get("code");
    			if (codename == null) {
    				throw new IllegalArgumentException("code_missing");
    			}
    			response = addFavoriteDriver(passId, codename);
    		} else if (Action.remove.toString().equals(action)) {
    			String driverIdStr = request.get("driverid");
    			if (driverIdStr == null) {
    				throw new IllegalArgumentException("driverid_missing");
    			}
    			Integer driverId = null;
    			try {
    				driverId = Integer.parseInt(driverIdStr);
    			} catch (Exception e) {
    				throw new IllegalArgumentException("wrong_driverid");
    			}
    			response = removeFavoriteDriver(passId, driverId);
    		} else if (Action.list.toString().equals(action)) {
    			response = listFavoriteDrivers(passId);
    		} else if (Action.listall.toString().equals(action)) {
    			response = listAllDrivers(passId);
    		} else if (Action.listblocked.toString().equals(action)) {
    			useBlocked = true;
    			response = listBlockedDrivers(passId);
    		} else if (Action.unblock.toString().equals(action)) {
    			useBlocked = true;
    			String driverIdStr = request.get("driverid");
    			if (driverIdStr == null) {
    				throw new IllegalArgumentException("driverid_missing");
    			}
    			Integer driverId = null;
    			try {
    				driverId = Integer.parseInt(driverIdStr);
    			} catch (Exception e) {
    				throw new IllegalArgumentException("wrong_driverid");
    			}
    			response = removeBlockedDriver(passId, driverId);
    		} else if (Action.block.toString().equals(action)) {
    			useBlocked = true;
    			String codename = request.get("code");
    			if (codename == null) {
    				throw new IllegalArgumentException("code_missing");
    			}
    			response = blockDriver(passId, codename);
    		} else {
    			throw new IllegalArgumentException("action_not_supported");
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    		response = String.format(getXML("GENERIC_FAILURE"), e.getMessage());
    	}
    	response = String.format(getXML(useBlocked ? "BLACKLISTED_DRIVERS" : "FAVORITE_DRIVERS"), response);
    }

    protected String listFavoriteDrivers(int passId) throws Exception {
	String result = "";
	prepareFindFavoriteDriversPerPassengerStmt();
	findFavoriteDriversPerPassengerStmt.setInt(1, passId);
	findFavoriteDriversPerPassengerStmt.setInt(2, passId);
	ResultSet rs = findFavoriteDriversPerPassengerStmt.executeQuery();
	Long prevDriver = 0L;
	int iTot = 0;
	while (rs.next()) {
	    Long driverId = rs.getLong("driverid");
	    if (prevDriver == driverId)
		continue;
	    prevDriver = driverId;
	    String codename = rs.getString("codename");
	    String name = rs.getString("firstname");
	    String surname = rs.getString("lastname");
	    String platenumber = rs.getString("plateno");
	    String carmodel = rs.getString("model");
	   
	    result +=
    String.format(getXML("FAVORITE_DRIVER"), driverId, codename != null ? codename : "", surname != null ? surname :
											 "", name != null ? name : "", platenumber == null ? "" : platenumber,
		  carmodel == null ? "" : carmodel,  "true", rs.getString("speaksenglish"),
		  rs.getString("newvehicle"), rs.getString("bigteamveh"), rs.getString("supportdisabled"), rs.getString("hybrid"),
		  rs.getString("wifi"), rs.getString("smoker"), rs.getString("allowpets"), rs.getString("takecreditcards"),
		  rs.getLong("totnofavs")).replace("null", "false") + "\n";
	    iTot++;
	}

	rs.close();
	return result;
    }

    protected String listAllDrivers(int passId) throws Exception {
	return listFavoriteDrivers(passId) + listBlockedDrivers(passId);
    }

    protected String addFavoriteDriver(int passid, String codename) throws Exception {
	prepareAddFavoriteDriverStmt();
	int i = 1;
	addFavoriteDriverStmt.setInt(i++, passid);
	addFavoriteDriverStmt.setString(i++, codename);
	addFavoriteDriverStmt.registerOutParameter(i++, Types.NUMERIC);
	addFavoriteDriverStmt.registerOutParameter(i++, Types.VARCHAR);
	addFavoriteDriverStmt.registerOutParameter(i++, Types.VARCHAR);
	addFavoriteDriverStmt.registerOutParameter(i++, Types.VARCHAR);
	addFavoriteDriverStmt.registerOutParameter(i++, Types.VARCHAR);
	addFavoriteDriverStmt.registerOutParameter(i++, Types.VARCHAR);
	try {
	    addFavoriteDriverStmt.execute();
	    long driverId = addFavoriteDriverStmt.getLong("driveridOut");
	    if (driverId == 0) { // driverid is null
		throw new RuntimeException("no_rows_affected");
	    } else { // requested data was actually fetched
		codename = addFavoriteDriverStmt.getString("codenameOut");
		String firstname = addFavoriteDriverStmt.getString("firstnameOut");
		String lastname = addFavoriteDriverStmt.getString("lastnameOut");
		String plate = addFavoriteDriverStmt.getString("plateOut");
		String model = addFavoriteDriverStmt.getString("modelOut");
		return String.format(getXML("ADD_FAVORITE_DRIVER"),
				     String.format(getXML("FAVORITE_DRIVER"), driverId, codename, lastname, firstname, plate == null ? "" : plate,
						   model == null ? "" : model,"true","false","false","false","false","false","false","false","false","false",1));
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	}
    }

    protected String removeFavoriteDriver(int passid, int driverid) throws Exception {
	prepareRemoveFavoriteDriverStmt();
	removeFavoriteDriverStmt.setInt(1, passid);
	removeFavoriteDriverStmt.setInt(2, driverid);
	int affectedRows = 0;
	try {
	    affectedRows = removeFavoriteDriverStmt.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	}
	if (affectedRows == 0) {
	    throw new RuntimeException("no_rows_affected");
	}
	return String.format(getXML("REMOVE_FAVORITE_DRIVER"), listFavoriteDrivers(passid)); //affectedRows
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

}
