package aploon.xmpp.ssclient.entities;


import aploon.datahelper.DataHelper;
import aploon.model.Passenger;
import aploon.xmpp.Communicator;
import aploon.xmpp.ServerSideClient;

import com.taxiplon.Util;

import java.sql.CallableStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

public class Entity extends ServerSideClient {

    private static Entity instance;

    public static synchronized Entity getInstance() {
	if (instance == null) {
	    instance = new Entity();
	}
	return instance;
    }

    private static final Logger LOGGER = Logger.getLogger(Entity.class);
    
    private Entity() {
	super();
    }

    CallableStatement createOrUpdateVehicleStmt;
    CallableStatement createOrUpdateSimStmt;
    CallableStatement createOrUpdateDeviceStmt;
    CallableStatement createOrUpdateDriverStmt;
    CallableStatement createOrUpdateUserStmt;
    CallableStatement createOrUpdateZoneStmt;

    private void prepareStmts() throws Exception {
	//	if(createOrUpdateVehicleStmt == null || createOrUpdateVehicleStmt.isClosed()) {
	//	    createOrUpdateVehicleStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateVehicle"));
	//	}
	if (createOrUpdateSimStmt == null || createOrUpdateSimStmt.isClosed()) {
	    createOrUpdateSimStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateSim"));
	}
	//	if(createOrUpdateDriverStmt == null || createOrUpdateDriverStmt.isClosed()) {
	//		createOrUpdateDriverStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateDriver"));
	//	}
	//	if(createOrUpdateDeviceStmt == null || createOrUpdateDeviceStmt.isClosed()) {
	//		    createOrUpdateDeviceStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateDevice"));
	//		}
	if (createOrUpdateUserStmt == null || createOrUpdateUserStmt.isClosed()) {
	    createOrUpdateUserStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateUser"));
	}
	if (createOrUpdateZoneStmt == null || createOrUpdateZoneStmt.isClosed()) {
	    createOrUpdateZoneStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateZone"));
	}
    }

    public enum Action {
	vehicle,
	device,
	sim,
	driver,
	user,
        makemobile,
	zone;

	public static Action parseAction(String s) {
	    try {
		return valueOf(s);
	    } catch (Exception e) {
		throw new RuntimeException("action_not_defined");
	    }
	}
    }

    public void processRequest() {
	String failure = null;
	String additionalOutput = null;
	int updatedRows = 0;
	try {
	    prepareStmts();
	    Action updateWhat = Action.parseAction(request.get("act"));
	    //one bot for all - all fields are optional
	    /*
			 * depending on what's provided, update stuff in the DB
			 */
	    String vehicleid = request.get("vehicleid");
	    String deviceid = request.get("deviceid");
	    String driverid = request.get("driverid");
	    String simid = request.get("simid");
	    String userid = request.get("userid");
	    String status = request.get("status");
	    String companyId = request.get("compid");
	    int i = 1;
	    switch (updateWhat) {
		/*case vehicle:
					/*
					 * Updatable fields in the vehicle table
					 *  plateno
						model
					 */
		/*String plateno = request.get("plateno");
					String model = request.get("model");
			
					createOrUpdateVehicleStmt.setString(i++, vehicleid);
					createOrUpdateVehicleStmt.setString(i++, plateno);
					createOrUpdateVehicleStmt.setString(i++, model);
					createOrUpdateVehicleStmt.setString(i++, status);
					createOrUpdateVehicleStmt.setString(i++, companyId);
			
					updatedRows = createOrUpdateVehicleStmt.executeUpdate();
					break;*/
	    case sim:
		/*
					 * Updatable fields in the sim table
					 * 	CARRIER
						MSISDN
						ICCID
						IP
					 */
		String carrier = request.get("carrier");
		String msisdn = request.get("msisdn");
		String iccid = request.get("iccid");
		String ip = request.get("ip");
		createOrUpdateSimStmt.setString(i++, simid);
		createOrUpdateSimStmt.setString(i++, carrier);
		createOrUpdateSimStmt.setString(i++, msisdn);
		createOrUpdateSimStmt.setString(i++, iccid);
		createOrUpdateSimStmt.setString(i++, ip);
		createOrUpdateSimStmt.setString(i++, status);
		updatedRows = createOrUpdateSimStmt.executeUpdate();
		break;
		/*case device:
					
					 * Updatable fields in the device table
					 * 	IMEI
					 *  VEHICLEID
					 *  SIMID
					
					String imei = request.get("imei");
					String shortname = request.get("short");
			
					createOrUpdateDeviceStmt.setString(i++, deviceid);
					createOrUpdateDeviceStmt.setString(i++, simid);
					createOrUpdateDeviceStmt.setString(i++, vehicleid);
					createOrUpdateDeviceStmt.setString(i++, imei);
					createOrUpdateDeviceStmt.setString(i++, shortname);
					createOrUpdateDeviceStmt.setString(i++, status);
					createOrUpdateDeviceStmt.setString(i++, companyId);
			
					updatedRows = createOrUpdateDeviceStmt.executeUpdate();
					break;*/
		/*case driver:
					
					 * Updatable fields in the driver table
					 *  DeviceId
						Pin
						Codename
						LastName
						FirstName
						Address
						Telephone
						Mobile
						Email
						Gender
						Comments
						Status
						PaymentDetails
						InvoiceNumber
						CustomerNumber
						+ a row for this CompanyId will be inserted into the DriverCompanies
					
					
					/*String companyid = request.get("compid");
					String pin = request.get("pin");
					String codename = request.get("codename");
					String name = request.get("name");
					String surname = request.get("surname");
					String address = request.get("address");
					String phone = request.get("phone");
					String mobile = request.get("mobile");
					String email = request.get("email");
					String sex = request.get("sex");
					String comments = request.get("comments");
					String paymentDetails = request.get("paydet");
					String invoiceNo = request.get("invno");
					String customerNo = request.get("custno");
					String typeid = request.get("typeid");
					String isvip = request.get("isvip");
					String subscriptionid = request.get("subid");
			    			
//					RegisterDevice.getInstance().registerDriver(Long.parseLong(deviceid), driverid != null? Long.parseLong(driverid) : null,
//																name, surname, sex, address, phone, mobile, email, Long.parseLong(companyid),
//																pin, status, invoiceNo, customerNo, comments);
					Long generatedDriverId = null;
					if(pin == null) {
						Random pinGenerator = new Random();
						pin = String.format("%04d", pinGenerator.nextInt(9999));
					}
//					codename = (driverid == null? surname.substring(0, surname.length() < 3? surname.length() : 3) + name.substring(0, name.length() < 3? name.length() : 3) : null);
					createOrUpdateDriverStmt.setString(i++, deviceid);
					createOrUpdateDriverStmt.setString(i++, driverid);
					createOrUpdateDriverStmt.setString(i++, name);
					createOrUpdateDriverStmt.setString(i++, surname);
					createOrUpdateDriverStmt.setString(i++, sex);
					createOrUpdateDriverStmt.setString(i++, address);
					createOrUpdateDriverStmt.setString(i++, phone);
					createOrUpdateDriverStmt.setString(i++, mobile);
					createOrUpdateDriverStmt.setString(i++, email);
					createOrUpdateDriverStmt.setString(i++, companyid);
					createOrUpdateDriverStmt.setString(i++, pin);
					createOrUpdateDriverStmt.setString(i++, comments);
					createOrUpdateDriverStmt.setString(i++, paymentDetails);
					createOrUpdateDriverStmt.setString(i++, codename);
					createOrUpdateDriverStmt.setString(i++, invoiceNo);
					createOrUpdateDriverStmt.setString(i++, customerNo);
					createOrUpdateDriverStmt.setString(i++, status);
					createOrUpdateDriverStmt.setString(i++, isvip);
					createOrUpdateDriverStmt.setString(i++, typeid);
					createOrUpdateDriverStmt.setString(i++, subscriptionid);
			
			
			
			
//					
//	createOrUpdateDriverStmt.registerOutParameter(i++, Types.NUMERIC);
//					createOrUpdateDriverStmt.registerOutParameter(i++, Types.VARCHAR);
//					try {
						updatedRows = createOrUpdateDriverStmt.executeUpdate();
//					}
//					catch(SQLException e) {
//						e.printStackTrace();
//						throw new RuntimeException("driver_fk_violation");
//					}
//					generatedDriverId = createOrUpdateDriverStmt.getLong("driverIdOut");
//					codename = createOrUpdateDriverStmt.getString("codenameVar");
//					additionalOutput = String.format(getXML("DRIVER_UPDATE"), generatedDriverId, codename);
					break;*/
	    case user:
		/*
					 * Updatable fields in the users table
					 *  companyid
					 *  username
					 *  password
					 *  level
					 *  surname
					 *  name
					 *  phone
					 *  status
					 */
		createOrUpdateUserStmt.setString(i++, userid);
		createOrUpdateUserStmt.setString(i++, request.get("compid"));
		createOrUpdateUserStmt.setString(i++, request.get("login"));
		if (request.get("pass") != null) {
		    String hashedPassword = Util.digest(request.get("pass"));
		    createOrUpdateUserStmt.setString(i++, hashedPassword);
		} else {
		    createOrUpdateUserStmt.setString(i++, null);
		}
		createOrUpdateUserStmt.setString(i++, request.get("level"));
		createOrUpdateUserStmt.setString(i++, request.get("surname"));
		createOrUpdateUserStmt.setString(i++, request.get("name"));
		createOrUpdateUserStmt.setString(i++, request.get("phone"));
		createOrUpdateUserStmt.setString(i++, status);
		createOrUpdateUserStmt.setString(i++, request.get("loc"));
		updatedRows = createOrUpdateUserStmt.executeUpdate();
		break;
            case makemobile:
                
	        //response = makeMobile(webAccount);
                response = makeMobile(Long.parseLong(userid));
                return;
                
            case zone:
		/*
					 * Updatable fields in the zones table
					 *  taxicompanyid
					 *  zonename
					 */
		createOrUpdateZoneStmt.setString(i++, request.get("zoneid"));
		createOrUpdateZoneStmt.setString(i++, request.get("compid"));
		createOrUpdateZoneStmt.setString(i++, request.get("zonename"));
		createOrUpdateZoneStmt.setString(i++, status);
		updatedRows = createOrUpdateZoneStmt.executeUpdate();
		break;
	    case vehicle:
	    case driver:
	    case device:

		throw new RuntimeException("deprecated");
	    default:
		break;
	    }
	    dataPool.closeConnection();
	} catch (Exception e) {
	    e.printStackTrace();
	    failure = e.getMessage();
	}
	response =
   String.format(getXML("GENERIC_UPDATE"), updatedRows, (additionalOutput != null ? additionalOutput + "\n" : "") +
		 String.format(getXML("UPDATE_STATUS"), Boolean.toString(failure == null),
			       failure != null ? failure : ""));

    }

    private String makeMobile(Long passengerId) {
    
        String errorMessage = "";
    
        try {
            
            Passenger passenger = DataHelper.getPassenger(dataPool.getConnection(), passengerId);
            if(passenger!=null) {
                try {
                    createXMPPAccount(Communicator.PASS_UNAME_PREFIX + passengerId, passenger.getMobile(), "", "Passenger");
                    //return String.format(getXML("REGISTRATION_PIN"),"true","Account set to mobile successfully",passengerId,passenger.getMobile(),"false","","","");
                
                    return String.format(getXML("GENERIC_UPDATE"), 0, 
                                  String.format(getXML("UPDATE_STATUS"), Boolean.toString(true),errorMessage));
                } catch(Exception e) {
                    e.printStackTrace();    
                    errorMessage = e.getMessage();
                }
            }
        } catch(SQLException e) {
            LOGGER.error("The sql exception ",e);    
            errorMessage = e.getMessage();
        }
    
    
        return String.format(getXML("GENERIC_UPDATE"), 0, String.format(getXML("UPDATE_STATUS"), Boolean.toString(false),errorMessage));
        
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

}
