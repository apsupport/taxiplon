package aploon.xmpp.ssclient.entities;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.lang.reflect.Type;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class CountryCodes {
    private Map<String, CountryJSON> countries;
    private static CountryCodes codes;

    public CountryCodes() {
	super();
	try {
	    String tmp = readFileAsString("Countries.txt");
	    Gson gson = new Gson();
	    countries = new ConcurrentHashMap<String, CountryJSON>();
	    Type collectionType = new TypeToken<List<CountryJSON>>() {
	    }.getType();
	    List<CountryJSON> details = gson.fromJson(tmp, collectionType);
	    for (CountryJSON co : details) {
		countries.put(co.getCountry_code(), co);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    public static CountryCodes getInstance() {
	if (codes == null) {
	    codes = new CountryCodes();
	}
	return codes;
    }

    private String readFileAsString(String filePath) throws java.io.IOException {
	StringBuffer fileData = new StringBuffer(1000);
	BufferedReader reader =
		   new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(filePath)));
	char[] buf = new char[1024];
	int numRead = 0;
	while ((numRead = reader.read(buf)) != -1) {
	    String readData = String.valueOf(buf, 0, numRead);
	    fileData.append(readData);
	    buf = new char[1024];
	}
	reader.close();
	return fileData.toString();
    }

    public static void main(String[] args) {
	System.out.println(CountryCodes.getInstance().getCountries().get("GR").getCo_tel_prefix());

    }

    public Map<String, CountryCodes.CountryJSON> getCountries() {
	return countries;
    }

    public String getInternationalMobile(String country, String mobi) {
	
	String code = getCountries().get(country).getCo_tel_prefix();
	if (mobi.startsWith("00" + code)) {
	    return mobi;
	} else if (mobi.startsWith("code")) {
	    return "00" + mobi;
	} else if (mobi.startsWith("+" + code)) {
	    return mobi.replace("+", "00");
	} else
	    return "00" + code + mobi;
    }


    class CountryJSON {
	private String country_id, country_name, co_tel_prefix, country_code;

	public void setCountry_id(String country_id) {
	    this.country_id = country_id;
	}

	public String getCountry_id() {
	    return country_id;
	}

	public void setCountry_name(String country_name) {
	    this.country_name = country_name;
	}

	public String getCountry_name() {
	    return country_name;
	}

	public void setCo_tel_prefix(String co_tel_prefix) {
	    this.co_tel_prefix = co_tel_prefix;
	}

	public String getCo_tel_prefix() {
	    return co_tel_prefix;
	}

	public void setCountry_code(String country_code) {
	    this.country_code = country_code;
	}

	public String getCountry_code() {
	    return country_code;
	}
    }
}
