package aploon.xmpp.ssclient.entities;

import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.NamedParameterStatement;

import java.util.HashMap;

import org.jivesoftware.smack.packet.IQ;

/**
 * @Deprecated use Entity instead.
 */
public class Updater extends ServerSideClient {
	
	NamedParameterStatement updateVehicleStmt;
	NamedParameterStatement updateDeviceStmt;
	NamedParameterStatement updateDriverStmt;
	
	private static Updater instance;
	
	public static synchronized Updater getInstance() {
		if(instance == null) {
			instance = new Updater();
		}
		return instance;
	}
	
	private Updater() {
		super();
	}
	
	private void prepareStmts() throws Exception {
		if(updateVehicleStmt == null || updateVehicleStmt.isClosed()) {
		    updateVehicleStmt = new NamedParameterStatement(dataPool, getQuery("updateVehicle"));
		}
		if(updateDeviceStmt == null || updateDeviceStmt.isClosed()) {
		    updateDeviceStmt = new NamedParameterStatement(dataPool, getQuery("updateDevice"));
		}
		if(updateDriverStmt == null || updateDriverStmt.isClosed()) {
		    updateDriverStmt = new NamedParameterStatement(dataPool, getQuery("updateDriver"));
		}
	}
	
	public void processRequest(HashMap<String, String> requestParams, Long deviceId, Long driverId) {
		request = requestParams;
	    proceedWithRequest(Long.toString(driverId), Long.toString(deviceId));
	}
	
	public void processRequest() {
		/*
	    String driverId = request.get("driverid");
	    String deviceId = request.get("deviceid");
		processRequest(driverId, deviceId);
		 */
	    response = "Deprecated. Use Entity instead.";
	}
	
	public void proceedWithRequest(String driverId, String deviceId) {
		String failure = null;
		int updatedRows = 0;
		try {
			//one bot for all - all fields are optional
		    String vehicleId = request.get("vehicleid");
			/*
			 * depending on what's given, update stuff in the DB
			 */
			//update driver-device-vehicle situation
			if(driverId != null || deviceId != null || vehicleId != null) {
				/*
				 * Updatable fields in the vehicle table
				 * 	plateno
					model
				 * Vehicle can be updated if any of device, driver or vehicle id's is provided
				 */
			    String plateno = parseOptionalField("plateno", String.class);
			    String model = parseOptionalField("model", String.class);
				/*
				 * Updatable fields in the device table
				 * 	IMEI
					MSISDN
					ICCID
					IP
				 * Device can be updated if any of device or driver id's is provided
				 */
				String imei = parseOptionalField("imei", String.class);
				String msisdn = parseOptionalField("msisdn", String.class);
			    String iccid = parseOptionalField("iccid", String.class);
				String ip = parseOptionalField("ip", String.class);
				/*
				 * Updatable fields in the driver table
				 * 	Pin
					Codename
					LastName
					FirstName
					Address
					Telephone
					Mobile
					Email
					Gender
					Comments
					Status
				 * Driver can be updated if driverid is provided
				 */
				String pin = parseOptionalField("pin", String.class);
				String codename = parseOptionalField("codename", String.class);
				String name = parseOptionalField("name", String.class);
				String surname = parseOptionalField("surname", String.class);
				String address = parseOptionalField("address", String.class);
				String phone = parseOptionalField("phone", String.class);
				String mobile = parseOptionalField("mobile", String.class);
				String email = parseOptionalField("email", String.class);
				String sex = parseOptionalField("sex", String.class);
				String comments = parseOptionalField("comments", String.class);
				String status = parseOptionalField("status", String.class);
				//Invoke the update statements. It doesn't matter whether some fields or even keys might be NULL; it will update what it can.
				try {
					prepareStmts();
					//update vehicle
					updateVehicleStmt.setString("plateno", plateno);
				    updateVehicleStmt.setString("model", model);
					updateVehicleStmt.setString("vehicleid", vehicleId);
				    updateVehicleStmt.setString("deviceid", deviceId);
				    updateVehicleStmt.setString("driverid", driverId);
				    updatedRows = updateVehicleStmt.executeUpdate();
					//update device
					updateDeviceStmt.setString("imei", imei);
//					updateDeviceStmt.setString("msisdn", msisdn);
//					updateDeviceStmt.setString("iccid", iccid);
//				    updateDeviceStmt.setString("ip", ip);
				    updateDeviceStmt.setString("deviceid", deviceId);
				    updateDeviceStmt.setString("driverid", driverId);
				    updatedRows = updateDeviceStmt.executeUpdate();
					//update driver
					updateDriverStmt.setString("pin", pin);
					updateDriverStmt.setString("codename", codename);
					updateDriverStmt.setString("lastname", surname);
					updateDriverStmt.setString("firstname", name);
					updateDriverStmt.setString("address", address);
					updateDriverStmt.setString("telephone", phone);
					updateDriverStmt.setString("mobile", mobile);
					updateDriverStmt.setString("email", email);
					updateDriverStmt.setString("gender", sex);
					updateDriverStmt.setString("comments", comments);
					updateDriverStmt.setString("status", status);
 				    updateDriverStmt.setString("driverid", driverId);
				    updatedRows = updateDriverStmt.executeUpdate();
					dataPool.closeConnection();
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		    Long passengerId = parseOptionalField("passid", Long.class);
		    Long taxicompanyId = parseOptionalField("compid", Long.class);
			//update passenger
			if(passengerId != null) {
				failure = "Passenger update not implemented.";
			}
			//update company
			if(taxicompanyId != null) {
				failure = "Company update not implemented.";
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		response = String.format(getXML("GENERIC_UPDATE"), updatedRows, 
								 String.format(getXML("UPDATE_STATUS"), 
											   Boolean.toString(failure == null), failure != null? failure : ""));
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
