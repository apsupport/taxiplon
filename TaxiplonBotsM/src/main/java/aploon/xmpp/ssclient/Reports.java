package aploon.xmpp.ssclient;

import aploon.xmpp.ServerSideClient;

import java.math.BigDecimal;

import org.jivesoftware.smack.packet.IQ;
/*
 * Report events (has traffic, etc )
 */
public class Reports extends ServerSideClient {

	static final int DEFAULT_REPORTED_EVENT_DURATION_MIN = 30;
	int reportedEventDurationMin =
		   getNumberFromProps("reported_event_duration", DEFAULT_REPORTED_EVENT_DURATION_MIN);

	public Reports() {
		super();
	}

	public void processRequest() {
		String failure = null;
		try {
			Double lat = Double.parseDouble(request.get("lat").replace(',', '.'));
			Double lng = Double.parseDouble(request.get("lng").replace(',', '.'));
			String description = request.get("desc");
			Hotspots.EventTypes type = Hotspots.EventTypes.valueOf(request.get("type"));
			if(Hotspots.getInstance().createOrUpdateHotspot(null, //eventid
															System.currentTimeMillis(),
															System.currentTimeMillis() + reportedEventDurationMin * 60000, //TODO fix wrong assumption, because the bot and db server calendars can be not in sync
															null, lat, lng,
															null, //status
															description, type, null, //address
															"User Report") != null) {
				reports.put(getClass().getSimpleName(), "User " + caller + " just reported a " + type + " at [" + lat + "," + lng + "]");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			failure = e.getMessage();
		}
		response = String.format(getXML("USER_REPORT"), 
								 String.format(getXML("UPDATE_STATUS"), 
											   Boolean.toString(failure == null),
											   failure == null ? "" : failure));
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
