package aploon.xmpp.ssclient;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;

import aploon.datahelper.DataHelper;
import aploon.model.CallType;
import aploon.model.Coordinates;
import aploon.model.DriverToDispatch;
import aploon.model.TripData;
import aploon.xmpp.Communicator;
import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.bots.FareDispatcher;
import aploon.xmpp.ssclient.iphone.Push;
import aploon.xmpp.ssclient.iphone.PushSender;

import com.aploon.geocoding.AploonGeoProvider;
import com.aploon.geocoding.GeoProvider;
import com.aploon.geocoding.GeoProviderManager;
import com.aploon.mbean.DispatcherMBean;
import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;


public class Dispatcher extends ServerSideClient {

    public static final int DEGREES_TO_KMS_COEFF = 125;
    public static final int DEFAULT_NUMBER_OF_DEVICES_TO_SEND_TRIP_TO = 3;

    private PreparedStatement findLinkedDevicesStmt;
    private NamedParameterStatement findNearbyDevicesStmt;
    private NamedParameterStatement findNearbyDevicesByTypeStmt;
    
    private static Dispatcher instance;

    private boolean googleDistanceEnabled = false;
    
    @Override
    public void processRequest() {
    	//TODO notAllowedToCallDirectly();
    	
    	String heartBeat = request.get("heartbeat");
    	
    	if(heartBeat!=null) {
    	
    		LOGGER.info("Received Response "+new Date().toString());
    		response = "ok";
    	} else {
    		response = null;
    		notAllowedToCallDirectly();
    	}

    }

    public enum Action {
    	place,
    	remove,
    	passengerComplete;
    }

    private static final Logger LOGGER = Logger.getLogger(Dispatcher.class);
    
    public static synchronized Dispatcher getInstance() throws Exception {
    	if (instance == null) {
    		instance = new Dispatcher();
    	}
    	return instance;
    }
    
    private Dispatcher() throws Exception {
    	super();
    }

    private void prepareFindLinkedDevicesStmt() throws Exception {
    	if (findLinkedDevicesStmt == null || findLinkedDevicesStmt.isClosed()) {
    		findLinkedDevicesStmt = dataPool.prepareStatement(getQuery("findLinkedDevices"));
    	}
    }

    private void prepareFindNearbyDevicesStmt() throws Exception {
    	
    	if (findNearbyDevicesStmt == null || findNearbyDevicesStmt.isClosed()) {
    		findNearbyDevicesStmt = new NamedParameterStatement(dataPool, 
    				"SELECT * FROM (SELECT DISTINCT(d.deviceid), "
    						+ "d.currentlat,d.currentlong, "
    							+ "d.imei, d.locale, d.extra, dr.driverId, "
    							+ "distance_between(d.currentlat, d.currentlong, t.pickuplat, t.pickuplong) as distance, "
    							+ "IF(t.zoneid IS NULL,NULL,dr.zoneJoinTime) as zoneJoinTime,"
    							+ "dc.taxicompanyid "
    							+ "FROM device d, driver dr, "
    							+ "drivercompanies dc, trip t, "
    							+ "passenger p WHERE t.tripid = :tripidArg "
    							+ "AND p.passengerid = t.passengerid "
    							+ "AND dc.driverid = dr.driverid "
    							+ "AND dc.taxicompanyid = t.taxicompanyid "
    							+ "AND dr.deviceid = d.deviceid "
    							+ "AND d.status = 'ava' AND dr.active = 'true' "
    							+ "AND dr.status = 'act' "
    				
    							//TODO
    							//+ "AND dr.OnRide = 'false' "
    							//+ "AND (case when ((SELECT calltype.PickedRideDelay FROM calltype WHERE calltype.calltypeid=t.calltypeid)>0) then dr.OnRide = 'false' else TRUE end) "
    							+ "AND (case when (t.zoneid IS NULL) then dr.OnRide = 'false' else TRUE end) "
    				
    							+ "AND (case when (t.zoneid IS NULL) then TRUE else ( dr.zoneid=t.zoneid AND dr.zoneJoinTime IS NOT NULL) end)"
    							+ "AND (case when ( "
    							+ " (SELECT calltype.ForceAssignment FROM calltype WHERE calltype.calltypeid=t.calltypeid ) IS FALSE ) "
    							+ "then TRUE else (dr.zoneid IS NULL AND dr.zoneJoinTime IS NULL ) end) "
    							+ "AND (dr.driverid NOT IN "
    							+ "(SELECT bla.DriverID FROM blacklisted bla WHERE bla.PassengerID = t.passengerid ))) a"
    							+ ", taxicompany tc WHERE a.taxicompanyid = tc.taxicompanyid "
    							+ "AND ((:multi IS NULL AND distance <= tc.hailrange) "
    							+ "OR (:multi IS NOT NULL AND distance <= (tc.hailrange * :multi))) "
    							+ "ORDER BY zoneJoinTime,distance ASC LIMIT :numberOfDevices");
    		}
    	}

    	private void prepareFindNearbyDevicesByTypeStmt() throws Exception {
        
    		if(findNearbyDevicesByTypeStmt==null||findNearbyDevicesByTypeStmt.isClosed()) {
        
    			findNearbyDevicesByTypeStmt = new NamedParameterStatement(dataPool,
    					"SELECT * FROM (SELECT DISTINCT(d.deviceid), "
    							+ "d.currentlat,d.currentlong, "
    							+ "d.imei, d.locale, d.extra, dr.driverId,"
    							+ "distance_between(d.currentlat, d.currentlong, t.pickuplat, t.pickuplong) as distance, "
    							+ "IF(t.zoneid IS NULL,NULL,dr.zoneJoinTime) as zoneJoinTime,"
    							+ "dc.taxicompanyid FROM device d, driver dr, drivercompanies dc, trip t,"
    							+ "passenger p "
    							+ "WHERE t.tripid = :tripidArg "
    							+ "AND p.passengerid = t.passengerid "
    							+ "AND dc.driverid = dr.driverid "
    							+ "AND dr.TypeId = :typeid "
        			
        						//TODO
        						//+ "AND dr.OnRide = 'false' "
        						//+ "AND (case when ((SELECT calltype.PickedRideDelay FROM calltype WHERE calltype.calltypeid=t.calltypeid)>0) then dr.OnRide = 'false' else TRUE end)"
        						+ "AND (case when (t.zoneid IS NULL) then dr.OnRide = 'false' else TRUE end) "
    				
    				
        						+ "AND dc.taxicompanyid = t.taxicompanyid "
        						+ "AND dr.deviceid = d.deviceid AND d.status = 'ava' "
        						+ "AND (case when ( "
        						+ " (SELECT calltype.ForceAssignment FROM calltype WHERE calltype.calltypeid=t.calltypeid ) IS FALSE ) "
        						+ "then TRUE else (dr.zoneid IS NULL AND dr.zoneJoinTime IS NULL ) end) "
        						+ "AND dr.active = 'true' AND dr.status = 'act' "
        						+ "AND (case when (t.zoneid IS NULL) then TRUE else ( dr.zoneid=t.zoneid AND dr.zoneJoinTime IS NOT NULL) end)"
        						+ "AND (dr.driverid NOT IN "
        						+ "(SELECT bla.DriverID FROM blacklisted bla WHERE bla.PassengerID = t.passengerid ))) a,"
        						+ " taxicompany tc WHERE a.taxicompanyid = tc.taxicompanyid "
        						+ "AND ((:multi IS NULL AND distance <= tc.hailrange) "
        						+ "OR (:multi IS NOT NULL AND distance <= (tc.hailrange * :multi))) "
        						+ "ORDER BY zoneJoinTime,distance ASC LIMIT :numberOfDevices");
        
    		}    
    	}

    	//	public void dispatchNewCall(long tripId, long hailtime, int candidateNumber, Coordinates coord, Long passId, String special) {
    	//		dispatchNewCall(tripId, null, hailtime, candidateNumber, coord, passId, special, false);
    	//	}

    	public void dispatchUpdate(Long tripId, String imei) throws Exception {
    	
    		TripData td = FareDispatcher.getInstance().fetchTripData(tripId);
    		//update status only for a waiting passenger
    		if ("wai".equals(td.getStatus()) || "req".equals(td.getStatus())) {
    			comm.sendMessageToDriver(td.getAssignmentMsg(), imei);
    		}
    	}

    	/**
    	 * Method called every time a customer places a call.
    	 * Devices within range their companies allow to serve, or the unique driver with the driverid specified, are informed.
    	 * @param tripId The ID of the trip that has just been stored in the DB.
    	 * @param coord The coordinates and the address of the call.
    	 * @param passId The ID of the passenger who just placed the call.
    	 * @param special String with notes dest send dest the driver.
    	 * @param resend Is a flag whether this call is a repeating one (in case a driver has taken and cancelled it before).
    	 * @return true if the call was dispatched to any drivers, false if there was nobody to pick it.
    	 */
    	public boolean dispatchNewCall(long tripId, Long driverId, long hailtime, int candidateNumber, Coordinates coord,
				   Long passId, String special, Boolean resend, Double mult, String multiDrivers, Long type, CallType callType) {
		
    		System.out.println("Dispatching the new call");
        
    		String deviceIds = "";
        
    		List<Long> driverIds = new ArrayList<>();
        
    		int candidates = candidateNumber;
    		
    		LOGGER.info("The passenger days "+callType.getSamePassengerDays());
    		
    		try {
    			if(callType.getSamePassengerDays()!=null&&callType.getSamePassengerDays()>0) {
    				driverIds = DataHelper.getPassengerDriversLastDays(passId, callType.getSamePassengerDays());
    				candidates = candidateNumber*2;
    			
    				LOGGER.info("The canidates are going to be "+candidates);
    			}
    		} catch (SQLException e1) {
    			e1.printStackTrace();
    		} 
        
    		try {
    			
    			ResultSet rs = null;
        		String informMessage = FareDispatcher.getInstance().fetchTripData(tripId).getAssignmentMsg();
        		    		
        		if (driverId == null && multiDrivers == null && type == null) {
        			prepareFindNearbyDevicesStmt();
        			findNearbyDevicesStmt.setLong("tripidArg", tripId);
        			findNearbyDevicesStmt.setLong("numberOfDevices", candidates);
        			if (mult == null)
        				findNearbyDevicesStmt.setString("multi", null);
        			else
        				findNearbyDevicesStmt.setDouble("multi", mult);

        			rs = findNearbyDevicesStmt.executeQuery();

        		} else if(driverId == null && multiDrivers == null && type != null) {
                
	        		prepareFindNearbyDevicesByTypeStmt();
	        		findNearbyDevicesByTypeStmt.setLong("tripidArg", tripId);
	        		findNearbyDevicesByTypeStmt.setLong("numberOfDevices", candidates);
	        		if (mult == null)
	        			findNearbyDevicesByTypeStmt.setString("multi", null);
	        		else
	        			findNearbyDevicesByTypeStmt.setDouble("multi", mult);
	        		findNearbyDevicesByTypeStmt.setLong("typeid", type);
                
	        		rs = findNearbyDevicesByTypeStmt.executeQuery();
                
        		} else { //targeted dispatch where a trip is only sent to one driver

        			NamedParameterStatement trpDisp = new NamedParameterStatement(dataPool, 
        					String.format(getQuery("findImeiByDriverIdForTrip"), 
        							multiDrivers == null ? "NULL" :multiDrivers));
        			
        			trpDisp.setLong("tripid", tripId);
        			
        			if (driverId == null) {
        				trpDisp.setString("driverid", null);
        			} else {
        				trpDisp.setLong("driverid", driverId);
        			}
            	
        			trpDisp.setString("multidriver", driverId == null ? "1" : null);
        			rs = trpDisp.executeQuery();

        		}
        		
        		Map<Long, Coordinates> driverCoordinates = new HashMap<Long, Coordinates>();
        		List<DriverToDispatch> driversToDispatch = new ArrayList<>();
        		
        		while (rs.next()) {
        			
        			DriverToDispatch driverToDispatch = new DriverToDispatch();
        			driverToDispatch.setDeviceId(rs.getLong("deviceid"));
        			driverToDispatch.setqDriverId(rs.getLong("driverId"));
        			driverToDispatch.setLat(rs.getBigDecimal("currentlat"));
        			driverToDispatch.setLng(rs.getBigDecimal("currentlong"));
        			driverToDispatch.setDeviceImei(rs.getString("imei"));
        			driverToDispatch.setDistance(rs.getDouble("distance"));
        			driverToDispatch.setDrlocale(rs.getString("locale"));
        			driverToDispatch.setDrextra(rs.getString("extra"));
        			driverToDispatch.setBrandname(rs.getString("brandname"));
        			driverCoordinates.put(driverToDispatch.getDeviceId(), 
        					new Coordinates(driverToDispatch.getLat(), driverToDispatch.getLng()));
				
        			/**
        			LOGGER.info("The parrent messages "+driverToDispatch.getDeviceImei()
        					+" "+driverToDispatch.getqDriverId()+
        					" "+driverToDispatch.getLat()+" "+driverToDispatch.getLng());
        			*/
        			
        			LOGGER.info("The candidates for the dispatch for trip "+tripId+" "+
        					driverToDispatch.getqDriverId()+" "+driverToDispatch.getDeviceId()+" "+
        					driverToDispatch.getDistance());
        			
        			driversToDispatch.add(driverToDispatch);
        		}
        		
        		rs.close();
        		
        		AploonGeoProvider aploonGeoProvider = getTaxiCompanyGeoProvider(1L);
        		Map<Long, BigDecimal> driverDistances = null;
        		
        		if(aploonGeoProvider!=null) {
        			LOGGER.info("The geoprovider is not null "+aploonGeoProvider);
        			driverDistances = aploonGeoProvider.orderedCoordinates(driverCoordinates, coord);
        		} else {
        			LOGGER.info("The geoprovider is null ");
        		}
        		
        		int driversDispatched = 0;
        		
        		for(DriverToDispatch driverToDispatch:driversToDispatch) {
        			
        			boolean canBeDispatched = true;
        		
        			LOGGER.info("The driverId is "+driverToDispatch.getqDriverId()+" already dispatched "+driversDispatched);
        			
        			if(driverId==null&&multiDrivers==null) {
        			
        				boolean driverExists = driverExists(driverToDispatch.getqDriverId(),driverIds);
        				boolean driverOffRange = driverOffRange(driverToDispatch, callType); 
        				
        				if(driverExists) {
        					
        					LOGGER.info("Cannot dispatch "+driverToDispatch.getqDriverId()+" already exists in previous days");
        					
        					canBeDispatched = false;
        				} else if(driverOffRange) {
        					
        					LOGGER.debug("Cannot dispatch range was "+callType.getHailRange()+
        							" Driver "+driverToDispatch.getqDriverId()+" "+
        							"was at "+driverToDispatch.getDistance());
        					
        					canBeDispatched = false;
        				} else if(driversDispatched>=candidateNumber ) {
        					
        					LOGGER.info("Cannot dispatch candidate number was surpussed "+
        							driverToDispatch.getqDriverId()+" "+
        							driversDispatched+" "+candidateNumber);
        					
        					canBeDispatched = false;
        				} 
        			} else {
        				canBeDispatched = true;
        			}
        			
        			LOGGER.info("The trip"+tripId+" can be dispatched  to "+
        					driverToDispatch.getqDriverId()+" "+
        					canBeDispatched+" "+driverId+" "+multiDrivers);
        		
        			if(canBeDispatched) {
        				 try {
        					 String placeCallMessage = String.format(getXML("CALL_PLACE_WRAPPER"),
        							 driverDistances!=null?driverDistances.get(driverToDispatch.getDeviceId())!=null?driverDistances.get(driverToDispatch.getDeviceId()):driverToDispatch.getDistance():driverToDispatch.getDistance(),informMessage);
        					 if (comm.sendMessageToDriver(placeCallMessage, driverToDispatch.getDeviceImei())) {
        						 deviceIds += (deviceIds.equals("") ? "" : ", ") + driverToDispatch.getDeviceId();
        						 driversDispatched++;
        					 }
        					 if (driverToDispatch.getDrextra() != null) {
        						 try {
        							 String messageDrId = "PassengerNewCall";
        							 String localizedMessageToDriver = Util.getProperty(
        									 messageDrId + "_" + (driverToDispatch.getDrlocale() != null ? driverToDispatch.getDrlocale() : Push.DEFAULT_PASSENGER_LOCALE),"lang");
        							 String localizedTitleToDriver = Util.getProperty(messageDrId + "_title_" + (driverToDispatch.getDrlocale() != null ? driverToDispatch.getDrlocale() : Push.DEFAULT_PASSENGER_LOCALE),"lang");
        							 PushSender.getInstanceDriver(driverToDispatch.getBrandname()).sendPushNotification(driverToDispatch.getDrextra(), localizedMessageToDriver, messageDrId,
        									 localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));
        						 } catch (Exception ex) {
        							 ex.printStackTrace();
        						 }
        					 }
        				 } catch (Exception e) {
        					 e.printStackTrace();
        				 }
        			} else {
        				if(driverDistances!=null) {
        					LOGGER.info("Driver not suitable for dispatch "+
        							driverToDispatch.getDeviceId()+" "+
        							driverToDispatch.getqDriverId());
        					driverDistances.remove(driverToDispatch.getDeviceId());
        				}
        			}
        		
        		}
        		
        		//TODO old and crappy way
        		/**
        		while (rs.next()) {
        		
        			long deviceId = rs.getLong("deviceid");
        			long qDriverId = rs.getLong("driverId");
        			String deviceImei = rs.getString("imei");
        			Double distance = rs.getDouble("distance");
        			String drlocale = rs.getString("locale");
        			String drextra = rs.getString("extra");
        			String brandname=rs.getString("brandname");
        			boolean canBeDispatched = true;
        			
        			if(driverId==null&&multiDrivers==null) {
        				if(driverExists(qDriverId,driverIds)) {
        					canBeDispatched = false;
        				}
        				if(driverIds.size()>0&&driversDispatched>=candidateNumber) {
        					canBeDispatched = false;
        				}
        			} else {
        				canBeDispatched = true;
        			}
        			
        			LOGGER.info("The can be dispatched is "+canBeDispatched+" "+driverId+" "+multiDrivers);
        			
        			if(canBeDispatched) {
        			
        				try {
        					String placeCallMessage = String.format(getXML("CALL_PLACE_WRAPPER"), distance, informMessage);
        					if (comm.sendMessageToDriver(placeCallMessage, deviceImei)) {
        						//changed back to the old method to see if it fixes the problem
        						deviceIds += (deviceIds.equals("") ? "" : ", ") + deviceId;
        						driversDispatched++;
        					}
        					if (drextra != null) {
        						try {
        							String messageDrId = "PassengerNewCall";
        							String localizedMessageToDriver = Util.getProperty(
        									messageDrId + "_" + (drlocale != null ? drlocale : Push.DEFAULT_PASSENGER_LOCALE),"lang");
        							String localizedTitleToDriver = Util.getProperty(
        									messageDrId + "_title_" + (drlocale != null ? drlocale : Push.DEFAULT_PASSENGER_LOCALE),"lang");
        							PushSender.getInstanceDriver(brandname).sendPushNotification(drextra, localizedMessageToDriver, messageDrId,
        									localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));
        						} catch (Exception ex) {
        							ex.printStackTrace();
        						}
        					}
        				} catch (Exception e) {
        					e.printStackTrace();
        				}
        			
        			}
        			
        		}
        		rs.close();
        		*/
        		
        		try {
        			
        			if(driverDistances!=null&&driverDistances.size()>0) {
        				DataHelper.insertTripDispatchForTrip(driverDistances, tripId, new Timestamp(hailtime), coord.getLat(), coord.getLng());
        			} else {
        				if (!deviceIds.equals("")) {
        					NamedParameterStatement insertTripAssignmentsStmt = new NamedParameterStatement(
        							dataPool, String.format(getQuery("insertTripDispatchForTrip"),deviceIds));
        					insertTripAssignmentsStmt.setLong("tripid", tripId);
        					insertTripAssignmentsStmt.setTimestamp("hailtime", new Timestamp(hailtime));
        					insertTripAssignmentsStmt.setDouble("triplat", coord.getLat());
        					insertTripAssignmentsStmt.setDouble("triplng", coord.getLng());
        					try {
        						insertTripAssignmentsStmt.executeUpdate();
        					} catch (Exception e) {
        						e.printStackTrace();
        					}
        					insertTripAssignmentsStmt.close();
        				} else { //as of 16/11/2011 if the trip wasn't dispatched to any device, then send shortage to the passenger straight away
        					//                                      FareDispatcher.getInstance().expireTrip(tripId);
        				}
        			}
        		} catch (Exception e) {
        			LOGGER.error(e);
        		}
        		dataPool.closeConnection();
        } catch (Exception e) {
        	LOGGER.error(e);
        }
        return !deviceIds.equals("");
    }

    public void passengerComplete(String deviceId, Long tripId) throws Exception {
    	if (deviceId != null) {
    		String msg =
		       String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.passengerComplete.toString(), tripId, "");
    		comm.sendMessageToDriver(msg, deviceId);
    	}

    }

    public void sendOperationOnCallMessage(Long tripId,String imei) throws Exception{
    	String msg = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove.toString(), tripId, "");
    	comm.sendMessageToDriver(msg, imei);
    }
    
    public void unlinkDevices(Long tripId, Boolean alsoUnlinkAssignedDevice) throws Exception {
    	prepareFindLinkedDevicesStmt();
    	findLinkedDevicesStmt.setLong(1, tripId);
    	findLinkedDevicesStmt.setString(2, alsoUnlinkAssignedDevice.toString());
    	ResultSet linkedDevices = findLinkedDevicesStmt.executeQuery();
    	String msg = String.format(getXML("OPERATION_ON_CALL"), Dispatcher.Action.remove.toString(), tripId, "");
    	String cancelledDevices = "";
	
    	while (linkedDevices.next()) {
    		long deviceId = linkedDevices.getLong("deviceid");
    		String deviceImei = linkedDevices.getString("imei");
    		String drlocale = linkedDevices.getString("locale");
    		String drextra = linkedDevices.getString("extra");
    		String brand=linkedDevices.getString("brandname");
    		//ekos added push notification for iphone drivers
    		if (drextra != null && alsoUnlinkAssignedDevice) {
    			try {
    				String messageDrId = "PassengerCancel";
    				String localizedMessageToDriver = Util.getProperty(messageDrId + "_" + (drlocale != null ? drlocale : Push.DEFAULT_PASSENGER_LOCALE),"lang");
    				String localizedTitleToDriver =  Util.getProperty(messageDrId + "_title_" + (drlocale != null ? drlocale : Push.DEFAULT_PASSENGER_LOCALE),"lang");
    				PushSender.getInstanceDriver(brand).sendPushNotification(drextra, localizedMessageToDriver, messageDrId,localizedTitleToDriver, null, 0,Util.getProperty("push_sound", "config"));
    			} catch (Exception ex) {
    				ex.printStackTrace();
    			}
    		}
    		
    		comm.sendMessageToDriver(msg, deviceImei);
    		cancelledDevices += (cancelledDevices.equals("") ? "" : ", ") + deviceId;
    	}
	
    	linkedDevices.close();
    	/*
		 * @deprecated the usage of the table trip_assign
		if(!cancelledDevices.equals("")) {
			String updateCancelledDevicesQuery = String.format(getQuery("updateCancelledDevices"), cancelledDevices);
			PreparedStatement updateCancelledDevicesStmt = dataPool.prepareStatement(updateCancelledDevicesQuery);
			updateCancelledDevicesStmt.setLong(1, tripId);
			updateCancelledDevicesStmt.executeUpdate();
		    updateCancelledDevicesStmt.close();
		}
		 */
    	dataPool.closeConnection();
    }
    
    private boolean driverExists(Long driverId,List<Long> drivers) {
    	
    	for(Long ldriverId:drivers) {
    		
    		LOGGER.info("Chek if driver exists "+ldriverId+" "+driverId);
    		
    		if(ldriverId.compareTo(driverId)==0) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    /**
     * Checking if the driver is inside the calltype HailRangeRange
     * @param driverToDispatch
     * @param callType
     * @return
     */
    private boolean driverOffRange(DriverToDispatch driverToDispatch,CallType callType) {
    	
    	String driverDistanceStr = driverToDispatch.getDistance().toString();
    	
    	if(callType.getHailRange()==null) {
    		LOGGER.debug("No range specified trip can be dispatched to driver "+driverToDispatch.getqDriverId());
    		return false;
    	}
    	
    	String callTypeRangeStr = callType.getHailRange().toString();
    	
    	BigDecimal driverDistance = new BigDecimal(driverDistanceStr);
    	BigDecimal hailRange = new BigDecimal(callTypeRangeStr);
    	
    	LOGGER.info("Driver "+driverToDispatch.getqDriverId()+" distance "+driverDistanceStr+" allowed hailrange "+callTypeRangeStr);
    	
    	return driverDistance.compareTo(hailRange)==1;
    }
    
    public AploonGeoProvider getTaxiCompanyGeoProvider(Long taxiCompanyId) {
    	
    	AploonGeoProvider aploonGeoProvider = null;
    	
    	/**
    	if(taxiCompanyId!=null) {
    		Integer geoProviderNum = 3;
    					//DataHelper.getGeoProvider(taxiCompanyId);
    		aploonGeoProvider = GeoProviderManager.getGeoProvider(taxiCompanyId,GeoProvider.getGeoProvider(geoProviderNum));	
    	}
    	*/
    	
    	if(googleDistanceEnabled) {
    		Integer geoProviderNum = 3;
    		aploonGeoProvider = GeoProviderManager.getGeoProvider(taxiCompanyId,GeoProvider.getGeoProvider(geoProviderNum));	
        }
    	
    	return aploonGeoProvider;
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

    
    /**
	@Override
	public void openConnection() {
		try {
			comm = new Communicator(getUsername(), getPassword(), getResource());
		} catch (XMPPException e) {
			e.printStackTrace();
		}
    	
	}

	@Override
	public void closeConnection() {
		try {
			System.out.println("The close connection");
			comm.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enableGoogleDistance() {
		googleDistanceEnabled = true;
	}

	@Override
	public void disableGoogleDistance() {
		googleDistanceEnabled = false;
	}
    */
}
