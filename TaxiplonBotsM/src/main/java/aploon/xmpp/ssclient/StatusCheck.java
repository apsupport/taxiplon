package aploon.xmpp.ssclient;


import aploon.concurrency.util.NamingThreadFactory;
import aploon.datahelper.StatusDatasourceProvider;
import aploon.model.BiddingTime;
import aploon.model.TripData;
import aploon.xmpp.Communicator;
import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.bots.FareDispatcher;
import aploon.xmpp.ssclient.web.WhitePages;

import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

/*
 * Is for all entities in order to check thir status (passenger is on call etc)
 */

public class StatusCheck extends ServerSideClient {

	/**
	private static String sAppUrl;

	private static StatusCheck instance;

    private static final Logger LOGGER = Logger.getLogger(StatusCheck.class);
    
    public static synchronized StatusCheck getInstance() throws Exception {
    	if (instance == null) {
    		instance = new StatusCheck();
    	}
    	return instance;
    }
    
    private StatusCheck() {
    	sAppUrl = Util.getProperty("new_app_url", "config");
    }
    
    public String getAppURL() {
    	return ((sAppUrl == null || sAppUrl.equals("")) ? "false" : sAppUrl);
    }
    
	public PassengerStatus checkPassengerStatus(String locale, String version, Long passId,
			String Country) throws Exception {
		return checkPassengerStatus(locale, version, passId, null, Country);
	}

	public PassengerStatus checkPassengerStatus(Long passId) throws Exception {
		return checkPassengerStatus(null, null, passId, null, null);
	}
	
	public PassengerStatus checkPassengerStatus(String locale, String version, Long passId, PerformanceTimes t,
            String Country) throws Exception {

		Connection connection = null;

		PreparedStatement passengerInfoStmt = null;
		PreparedStatement passengerStatusStmt = null;
		PreparedStatement passengerStatusCheckStmt = null;

		try {

			connection = StatusDatasourceProvider.getConnection();
			//DatasourceProvider.getConnection();

			long mark = System.currentTimeMillis();
			PassengerStatus output = new PassengerStatus(false, 0, 0, 0, "");
			boolean isMobileClient = false;

			if ((locale != null && !locale.equals("")) || (version != null && !version.equals("")) ||
					(Country != null && !Country.equals(""))) {

				isMobileClient = (version != null && (!version.equals("")));
				passengerInfoStmt = connection.prepareStatement(Util.getProperty("passengerInfo", "Queries"));

				//TODO newimpl getQuery("passengerInfo"));
				int i = 1;
				passengerInfoStmt.setString(i++, locale);
				passengerInfoStmt.setString(i++, version);
				passengerInfoStmt.setString(i++, Country);
				passengerInfoStmt.setString(i++, Country);
				passengerInfoStmt.setLong(i++, passId);
				passengerInfoStmt.executeUpdate();

				if (t != null) {
					t.locVerUpdateTime = System.currentTimeMillis() - mark;
				}

			}

			passengerStatusStmt = connection.prepareStatement(Util.getProperty("passengerStatus", "Queries"));

			//TODO getQuery("passengerStatus"));
			passengerStatusStmt.setLong(1, passId);
			ResultSet rs = passengerStatusStmt.executeQuery();

			if (rs.next()) {
				output.setProper(Boolean.parseBoolean(rs.getString("regged"))); //24/10/2011 registration is optional, but the same field is now renamed to 'regged' (for registered)

				int cancelsRemaining = Integer.parseInt(Util.getProperty("cancels_to_blacklist", "config"));
				//TODO getNumberFromProps("cancels_to_blacklist", DEFAULT_CANCELS_TO_BLACKLIST);

				try {
					cancelsRemaining -= rs.getInt("cancels");
				} catch (Exception e) {
					e.printStackTrace();
				}

				output.setBlacklistIn(cancelsRemaining);
				output.setDiscountRides(rs.getInt("discountrides"));
				output.setStars(rs.getInt("stars"));
				output.setCompanyTwitter(rs.getString("twitter") == null ? "" : rs.getString("twitter"));
				output.setCompanyFacebook(rs.getString("facebook") == null ? "" : rs.getString("facebook"));
				output.setCompanyEmail(rs.getString("email") == null ? "" : rs.getString("email"));

			}
			rs.close();
			//mark the time if necessary
			if (t != null) {
				t.mainInfoTime = System.currentTimeMillis() - mark - t.locVerUpdateTime;
			}
			//fetch trip data (if any)
			//preparePassengerStatusCheckStmt();

			passengerStatusCheckStmt = connection.prepareStatement(
					Util.getProperty("statusCheckPassenger", "Queries"));

			passengerStatusCheckStmt.setLong(1, passId);
			ResultSet rs2 = passengerStatusCheckStmt.executeQuery();
			//single result (if any)
			//ekos 31/7/12 changed from if to while cause of webooking multicall request
			String accum = "";
			while (rs2.next()) {
				Long tripId = rs2.getLong("tripid");
				Long deviceId = rs2.getLong("deviceid");
				Long driverId = rs2.getLong("driverid");
				Long hailtime = rs2.getTimestamp("hailtime").getTime();
				String status = rs2.getString("status");
				Double pickupLat = rs2.getDouble("pickuplat");
				Double pickupLong = rs2.getDouble("pickuplong");
				String pickupAddress = rs2.getString("pickupaddress");
				String driverinfo = rs2.getString("specialneeds");
				Double dropLat = rs2.getDouble("dropofflat");
				Double dropLong = rs2.getDouble("dropofflong");

				Double fare = rs2.getDouble("fare");

				boolean isDiscounted = rs2.getLong("discountrides") > 0;
				String discountMsg = WhitePages.getDiscountMessage(locale, isDiscounted);
				String destination = rs2.getString("destination");

				//check whether a driver is assigned
				String assignedDriver = "";
				if (deviceId != null && deviceId != 0) { //0 is returned when the trip is not assigned
					Double driverLat = rs2.getDouble("currentlat");
					Double driverLng = rs2.getDouble("currentlong");

					String assignedDriverPlateNo = rs2.getString("platenumber");
					String assignedDriverName = rs2.getString("driverName");
					String assignedDriverMobile = rs2.getString("driverMobile");
					String assignedDriverCarmodel = rs2.getString("driverCarmodel");
					Double assignedDriverDistanceTo = rs2.getDouble("distance");
					String comptel = rs2.getString("telephone");

					if (isMobileClient) {
						assignedDriver = 

								String.format(
										Util.getProperty("ASSIGNED_DRIVER_XML", "XMLs"),
										//getXML("ASSIGNED_DRIVER_XML"),
	
										deviceId, driverLat, driverLng,
										assignedDriverPlateNo,assignedDriverName, assignedDriverMobile,
										assignedDriverCarmodel, pickupAddress,assignedDriverDistanceTo,comptel);
					} else {
						assignedDriver =
								String.format(
										Util.getProperty("ASSIGNED_DRIVER_MULTICALL_XML", "XMLs"),
	
										//getXML("ASSIGNED_DRIVER_MULTICALL_XML"), 
	
										tripId, deviceId, driverLat, driverLng, assignedDriverPlateNo,
										assignedDriverName, assignedDriverMobile, assignedDriverCarmodel, pickupAddress,
										assignedDriverDistanceTo,comptel);
					}
				}
				//mark the time if necessary
				if (t != null) {
					t.tripInfoTime = System.currentTimeMillis() - mark - t.locVerUpdateTime - t.mainInfoTime;
				}
				//get bidding times to make passenger app able to restore the countdown
				Long currentTimeMillis = System.currentTimeMillis();
				BiddingTime tripBiddingTime = FareDispatcher.getInstance().getTripBiddingTime(tripId, currentTimeMillis);
				int biddingExpireTime = (int)((tripBiddingTime.getBiddingTime() - currentTimeMillis) / 1000);
				if (isMobileClient) {
					accum = String.format(
							//getXML("STATUS_ASSIGNED_XML"), 
							Util.getProperty("STATUS_ASSIGNED_XML", "XMLs"),

							tripId, deviceId, driverId,fare, hailtime,
							biddingExpireTime, ((tripBiddingTime.getWaitTime() - currentTimeMillis) / 1000) - (biddingExpireTime > 0 ? biddingExpireTime : 0),
							currentTimeMillis, status, pickupLat, pickupLong, dropLat, dropLong, destination == null ? "" : destination,
									discountMsg == null ? "" : discountMsg, driverinfo == null ? "" : driverinfo, pickupAddress,assignedDriver);
				} else {
					accum += String.format("<trip>\n%s\n</trip>\n", String.format(
							//getXML("STATUS_ASSIGNED_XML"), 
							Util.getProperty("STATUS_ASSIGNED_XML", "XMLs"),
							tripId, deviceId, 
							driverId,fare, hailtime, biddingExpireTime,
							((tripBiddingTime.getWaitTime() - currentTimeMillis) / 1000) - (biddingExpireTime > 0 ? biddingExpireTime : 0),
							currentTimeMillis, status, pickupLat, pickupLong, dropLat, dropLong,
							destination == null ? "" : destination, discountMsg == null ? "" : discountMsg,
									driverinfo == null ? "" : driverinfo, pickupAddress, assignedDriver));
				}
				//mark the time if necessary
				if (t != null) {
					t.countdownInfoTime = System.currentTimeMillis() - mark - t.locVerUpdateTime - t.mainInfoTime - t.tripInfoTime;
				}
			}
			rs2.close();

			output.setCallXML(isMobileClient ? accum : String.format("<trips>\n%s\n</trips>\n", accum));
			//TODO dataPool.closeConnection();
			return output;

		} finally {
			if(passengerInfoStmt!=null) passengerInfoStmt.close();
			if(passengerStatusStmt!=null) passengerStatusStmt.close();
			if(passengerStatusCheckStmt!=null) passengerStatusCheckStmt.close();
			if(connection!=null) connection.close();
		}	

	}
	
	public class PassengerStatus {

    	boolean proper;
    	int blacklistIn;
    	int discountRides;
    	String callXML;
    	int stars;
    	String companyEmail;
    	String companyFacebook;
    	String companyTwitter;

    	public PassengerStatus(boolean proper, int blacklistIn, int discountRides, int stars, String callXML) {
    		super();
    		this.proper = proper;
    		this.blacklistIn = blacklistIn;
    		this.discountRides = discountRides;
    		this.callXML = callXML;
    		this.stars = stars;
    	}


    	public void setProper(boolean proper) {
    		this.proper = proper;
    	}

    	public boolean isProper() {
    		return proper;
    	}

    	public void setBlacklistIn(int blacklistIn) {
    		this.blacklistIn = blacklistIn;
    	}

    	public int getBlacklistIn() {
    		return blacklistIn;
    	}

    	public void setDiscountRides(int discountRides) {
    		this.discountRides = discountRides;
    	}

    	public int getDiscountRides() {
    		return discountRides;
    	}

    	public void setCallXML(String callXML) {
    		this.callXML = callXML;
    	}

    	public String getCallXML() {
    		return callXML;
    	}

    	public void setStars(int stars) {
    		this.stars = stars;
    	}

    	public int getStars() {
    		return stars;
    	}

    	public void setCompanyEmail(String companyEmail) {
    		this.companyEmail = companyEmail;
    	}

    	public String getCompanyEmail() {
    		return companyEmail;
		}

    	public void setCompanyFacebook(String companyFacebook) {
    		this.companyFacebook = companyFacebook;
    	}

    	public String getCompanyFacebook() {
    		return companyFacebook;
    	}

    	public void setCompanyTwitter(String companyTwitter) {
    		this.companyTwitter = companyTwitter;
		}

    	public String getCompanyTwitter() {
    		return companyTwitter;
    	}
    }
	
	class PerformanceTimes {
    	long started = 0L;
    	long argumentParsingTime = 0L;
    	long fullProcessTime = 0L;
    	long locVerUpdateTime = 0L;
    	long mainInfoTime = 0L;
    	long tripInfoTime = 0L;
    	long countdownInfoTime = 0L;

    	public String toString() {
    		return "started on: " + started + ", argumentParsingTime: " + argumentParsingTime + ", fullProcessTime: " +
    				fullProcessTime + ", locVerUpdateTime: " + locVerUpdateTime + ", mainInfoTime: " + mainInfoTime +
    				", tripInfoTime: " + tripInfoTime + ", countdownInfoTime: " + countdownInfoTime;
    	}
    }
	*/
	
	//TODO OLDIMPL
	
//extends ServerSideClient {

	public static final int DEFAULT_DRIVER_APP_BUILD = 0;
    public static final int DEFAULT_PASS_APP_BUILD = 0;
    public static final int DEFAULT_CANCELS_TO_BLACKLIST = 0;
    
    private static String sAppUrl;
    private static StatusCheck instance;

    private static final Logger LOGGER = Logger.getLogger(StatusCheck.class);
    
    public static synchronized StatusCheck getInstance() throws Exception {
    	if (instance == null) {
    		instance = new StatusCheck();
    	}
    	return instance;
    }
    
    private ExecutorService executorService;

	//TODO Connection connection = null;
	
    private StatusCheck() throws Exception {
    	super();
    	sAppUrl = Util.getProperty("new_app_url", "config");
    	executorService = Executors.newFixedThreadPool(20,new NamingThreadFactory("statuscheck_pool"));

    	/**
    	String statusCheckAsyncStr = Util.getProperty("status_check_async", "config");
    	
    	if(statusCheckAsyncStr!=null) {
    		Boolean statusCheckAsync = Boolean.parseBoolean(statusCheckAsyncStr);
    		if(statusCheckAsync) {
    		
    			String statusCheckIntervalStr = Util.getProperty("status_check_interval", "config");
    	    	final int statusCheckInterval;
    	    	
    	    	if(statusCheckIntervalStr!=null) {
    	    		statusCheckInterval = Integer.parseInt(statusCheckIntervalStr);
    	    	} else {
    	    		statusCheckInterval = 1000;
    	    	}
    			
    			asynchronousRequestProcessing = new Thread(new Runnable() {
					
					@Override
					public void run() {
						while (true) {
							try {
								LinkedHashMap<String, String> requests = substituteRequests();
		        				if (requests != null && !requests.isEmpty()) {
		        					for (Map.Entry<String, String> entry : requests.entrySet()) {
		        						String from = entry.getKey();
		        						String message = entry.getValue();
		        						proceedWithRequest(from, message);
		        					}
		        				} else {
		        					Thread.sleep(statusCheckInterval);
		            			}
							} catch (Exception e) {
		        				e.printStackTrace();
		        			}
						}
					}
				});
    			asynchronousRequestProcessing.start();
    		}
    		
    	}
    	*/
    	
    	//TODO connection = DatasourceProvider.getConnection();
    }

    class PerformanceTimes {
    	long started = 0L;
    	long argumentParsingTime = 0L;
    	long fullProcessTime = 0L;
    	long locVerUpdateTime = 0L;
    	long mainInfoTime = 0L;
    	long tripInfoTime = 0L;
    	long countdownInfoTime = 0L;

    	public String toString() {
    		return "started on: " + started + ", argumentParsingTime: " + argumentParsingTime + ", fullProcessTime: " +
    				fullProcessTime + ", locVerUpdateTime: " + locVerUpdateTime + ", mainInfoTime: " + mainInfoTime +
    				", tripInfoTime: " + tripInfoTime + ", countdownInfoTime: " + countdownInfoTime;
    	}
    }

    public void processRequest() {

    	executorService.execute(new RequestController(request,from,new String(caller)));    	
    
    	response = null;
    }

    private class RequestController implements Runnable {
    	
    	private final HashMap<String, String> requestArguements;
    	private String fromuser;
    	private final String callerStr;
    	
    	public RequestController(HashMap<String, String> requestArguments,String from,String callerStr) {
    		this.requestArguements = requestArguments;
            this.fromuser = from;
            this.callerStr = callerStr;
    	
            LOGGER.debug("The caller "+callerStr+" the from "+fromuser);
            
        }
    
    	@Override
        public void run() {
 
    		String responseStr = null;
    		
    		LOGGER.info("The thread id is "+Thread.currentThread().getId());
    		
    		try {

    			PerformanceTimes times = new PerformanceTimes();
    	    	times.started = System.currentTimeMillis();
    	    
    	    	try {
    	    	
    	    		String uid = requestArguements.get("uid"); //call from a smartphone client
    	    		String heartBeat = requestArguements.get("heartbeat");
    	    		
    	    		if (uid == null) {
    	    			uid = requestArguements.get("passid"); //call from the WEB client
    	    		}
    	    		
    	    		
    	    		String locale = requestArguements.get("loc"); //locale of the passenger application (to know what language to send push notifications in)
    	    		String version = requestArguements.get("ver");
    	    		
    	    		boolean isMobileClient = (version != null && (!version.equals("")));

    	    		if(heartBeat!=null) {    
    	    			LOGGER.info("Responding to a heartbeat check "+heartBeat);
    	    			responseStr = "ok";
    	    		} else if (uid != null && uid.contains(Communicator.PASS_UNAME_PREFIX)) {
    	    		
    	    			uid = uid.replaceAll(Communicator.PASS_UNAME_PREFIX, "");
    	    			String country = requestArguements.get("country");
    	    			Long passId = Long.parseLong(uid);
    	    			times.argumentParsingTime = System.currentTimeMillis() - times.started;
    	    			PassengerStatus passStatus = checkPassengerStatus(locale, version, passId, times, country);
    	    			times.fullProcessTime = System.currentTimeMillis() - times.started;
    	    			responseStr = String.format(getXML("PASS_STATUS_XML"), isMobileClient ? getAppURL() : "false",
    	    					Boolean.toString(passStatus.isProper()),passStatus.getBlacklistIn(), passStatus.getDiscountRides(), 
    					passStatus.getStars(), getLastPassAppBuild(version),passStatus.getCompanyEmail(),
    					passStatus.getCompanyFacebook(), passStatus.getCompanyTwitter(),
    					passStatus.getCallXML(), times.toString());
    	    		
    	    			if(fromuser.contains("/android")) fromuser = fromuser.replace("/android", "");
    	    			
    	    		} else {

    	    			String driverId = requestArguements.get("driverid");
    	    			responseStr = checkDeviceStatus(callerStr, driverId, version); 
    	    			//responseStr = checkDeviceStatus(caller, driverId, version); caller comes from the serversideclient super class and has to be the imei of the device
    	    			
    	    		}
    	    

    	    	} catch (Exception e) {
    	    		//e.printStackTrace();
    	    		LOGGER.error("Status check problem ",e);
    	    		responseStr = "Status check failed due to exception: " + e.getMessage();
    	    	}
    	    	
    	    	comm.sendMessage(responseStr, fromuser);
    	    	
    	    } catch(Exception e) {
    			// e.printStackTrace();
    			 LOGGER.error("General Status check problem ",e);
    		 }
    	}
    }
    
    public PassengerStatus checkPassengerStatus(String locale, String version, Long passId,
						String Country) throws Exception {
    	return checkPassengerStatus(locale, version, passId, null, Country);
    }

    public PassengerStatus checkPassengerStatus(Long passId) throws Exception {
    	return checkPassengerStatus(null, null, passId, null, null);
    }

    public PassengerStatus checkPassengerStatus(String locale, String version, Long passId, PerformanceTimes t,
                                                String Country) throws Exception {
    
    	Connection connection = null;
    	
    	PreparedStatement passengerInfoStmt = null;
    	PreparedStatement passengerStatusStmt = null;
    	PreparedStatement passengerStatusCheckStmt = null;
    	
    	try {
    	
    		connection = StatusDatasourceProvider.getConnection();
    				//DatasourceProvider.getConnection();
    		
    		long mark = System.currentTimeMillis();
    		PassengerStatus output = new PassengerStatus(false, 0, 0, 0, "");
    		boolean isMobileClient = false;

    		if ((locale != null && !locale.equals("")) || (version != null && !version.equals("")) ||
    				(Country != null && !Country.equals(""))) {
            
    			isMobileClient = (version != null && (!version.equals("")));
    			passengerInfoStmt = connection.prepareStatement(Util.getProperty("passengerInfo", "Queries"));
    				
    			//TODO newimpl getQuery("passengerInfo"));
    			int i = 1;
    			passengerInfoStmt.setString(i++, locale);
    			passengerInfoStmt.setString(i++, version);
    			passengerInfoStmt.setString(i++, Country);
    			passengerInfoStmt.setString(i++, Country);
    			passengerInfoStmt.setLong(i++, passId);
    			passengerInfoStmt.executeUpdate();
    			
    			if (t != null) {
    				t.locVerUpdateTime = System.currentTimeMillis() - mark;
    			}

    		}
    		
    		passengerStatusStmt = connection.prepareStatement(Util.getProperty("passengerStatus", "Queries"));
    				
    				//TODO getQuery("passengerStatus"));
    		passengerStatusStmt.setLong(1, passId);
    		ResultSet rs = passengerStatusStmt.executeQuery();
    	
    		if (rs.next()) {
    			output.setProper(Boolean.parseBoolean(rs.getString("regged"))); //24/10/2011 registration is optional, but the same field is now renamed to 'regged' (for registered)
            
    			int cancelsRemaining = Integer.parseInt(Util.getProperty("cancels_to_blacklist", "config"));
    					//TODO getNumberFromProps("cancels_to_blacklist", DEFAULT_CANCELS_TO_BLACKLIST);
            
    			try {
    				
    				int passengerCancels = rs.getInt("cancels");
    				
    				cancelsRemaining -= rs.getInt("cancels");
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    			
    			output.setBlacklistIn(cancelsRemaining);
    			output.setDiscountRides(rs.getInt("discountrides"));
    			output.setStars(rs.getInt("stars"));
    			output.setCompanyTwitter(rs.getString("twitter") == null ? "" : rs.getString("twitter"));
    			output.setCompanyFacebook(rs.getString("facebook") == null ? "" : rs.getString("facebook"));
    			output.setCompanyEmail(rs.getString("email") == null ? "" : rs.getString("email"));

    		}
    		rs.close();
    		//mark the time if necessary
    		if (t != null) {
    			t.mainInfoTime = System.currentTimeMillis() - mark - t.locVerUpdateTime;
    		}
    		//fetch trip data (if any)
    		//preparePassengerStatusCheckStmt();
    		
    		passengerStatusCheckStmt = connection.prepareStatement(
    				Util.getProperty("statusCheckPassenger", "Queries"));
    		
    		passengerStatusCheckStmt.setLong(1, passId);
    		ResultSet rs2 = passengerStatusCheckStmt.executeQuery();
    		//single result (if any)
    		//ekos 31/7/12 changed from if to while cause of webooking multicall request
    		String accum = "";
    		while (rs2.next()) {
    			Long tripId = rs2.getLong("tripid");
    			Long deviceId = rs2.getLong("deviceid");
    			Long driverId = rs2.getLong("driverid");
    			Long hailtime = rs2.getTimestamp("hailtime").getTime();
    			String status = rs2.getString("status");
    			Double pickupLat = rs2.getDouble("pickuplat");
    			Double pickupLong = rs2.getDouble("pickuplong");
    			String pickupAddress = rs2.getString("pickupaddress");
    			String driverinfo = rs2.getString("specialneeds");
    			Double dropLat = rs2.getDouble("dropofflat");
    			Double dropLong = rs2.getDouble("dropofflong");
            
    			Double fare = rs2.getDouble("fare");
            
    			boolean isDiscounted = rs2.getLong("discountrides") > 0;
    			String discountMsg = WhitePages.getDiscountMessage(locale, isDiscounted);
    			String destination = rs2.getString("destination");

    			//check whether a driver is assigned
    			String assignedDriver = "";
    			if (deviceId != null && deviceId != 0) { //0 is returned when the trip is not assigned
    				Double driverLat = rs2.getDouble("currentlat");
    				Double driverLng = rs2.getDouble("currentlong");

    				String assignedDriverPlateNo = rs2.getString("platenumber");
    				String assignedDriverName = rs2.getString("driverName");
    				String assignedDriverMobile = rs2.getString("driverMobile");
    				String assignedDriverCarmodel = rs2.getString("driverCarmodel");
    				Double assignedDriverDistanceTo = rs2.getDouble("distance");
    				String comptel = rs2.getString("telephone");
                
    				if (isMobileClient) {
    					assignedDriver = 
    							
    							String.format(
    									Util.getProperty("ASSIGNED_DRIVER_XML", "XMLs"),
    									//getXML("ASSIGNED_DRIVER_XML"),
    									
    									deviceId, driverLat, driverLng,
    									assignedDriverPlateNo,assignedDriverName, assignedDriverMobile,
    									assignedDriverCarmodel, pickupAddress,assignedDriverDistanceTo,comptel);
    				} else {
    					assignedDriver =
    							String.format(
    									Util.getProperty("ASSIGNED_DRIVER_MULTICALL_XML", "XMLs"),
    									
    									//getXML("ASSIGNED_DRIVER_MULTICALL_XML"), 
    									
    									tripId, deviceId, driverLat, driverLng, assignedDriverPlateNo,
    									assignedDriverName, assignedDriverMobile, assignedDriverCarmodel, pickupAddress,
    									assignedDriverDistanceTo,comptel);
    				}
    			}
    			//mark the time if necessary
    			if (t != null) {
    				t.tripInfoTime = System.currentTimeMillis() - mark - t.locVerUpdateTime - t.mainInfoTime;
    			}
    			//get bidding times to make passenger app able to restore the countdown
    			Long currentTimeMillis = System.currentTimeMillis();
    			BiddingTime tripBiddingTime = FareDispatcher.getInstance().getTripBiddingTime(tripId, currentTimeMillis);
    			int biddingExpireTime = (int)((tripBiddingTime.getBiddingTime() - currentTimeMillis) / 1000);
    			if (isMobileClient) {
    				accum = String.format(
    						//getXML("STATUS_ASSIGNED_XML"), 
    						Util.getProperty("STATUS_ASSIGNED_XML", "XMLs"),
							
    						tripId, deviceId, driverId,fare, hailtime,
    						biddingExpireTime, ((tripBiddingTime.getWaitTime() - currentTimeMillis) / 1000) - (biddingExpireTime > 0 ? biddingExpireTime : 0),
    						currentTimeMillis, status, pickupLat, pickupLong, dropLat, dropLong, destination == null ? "" : destination,
    								discountMsg == null ? "" : discountMsg, driverinfo == null ? "" : driverinfo, pickupAddress,assignedDriver);
    			} else {
    				accum += String.format("<trip>\n%s\n</trip>\n", String.format(
    						//getXML("STATUS_ASSIGNED_XML"), 
    						Util.getProperty("STATUS_ASSIGNED_XML", "XMLs"),
    						tripId, deviceId, 
    						driverId,fare, hailtime, biddingExpireTime,
    						((tripBiddingTime.getWaitTime() - currentTimeMillis) / 1000) - (biddingExpireTime > 0 ? biddingExpireTime : 0),
    						currentTimeMillis, status, pickupLat, pickupLong, dropLat, dropLong,
    						destination == null ? "" : destination, discountMsg == null ? "" : discountMsg,
    								driverinfo == null ? "" : driverinfo, pickupAddress, assignedDriver));
    			}
    			//mark the time if necessary
    			if (t != null) {
    				t.countdownInfoTime = System.currentTimeMillis() - mark - t.locVerUpdateTime - t.mainInfoTime - t.tripInfoTime;
    			}
    		}
    		rs2.close();

    		output.setCallXML(isMobileClient ? accum : String.format("<trips>\n%s\n</trips>\n", accum));
    		//TODO dataPool.closeConnection();
    		return output;
    	
    	} finally {
    		if(passengerInfoStmt!=null) passengerInfoStmt.close();
    		if(passengerStatusStmt!=null) passengerStatusStmt.close();
    		if(passengerStatusCheckStmt!=null) passengerStatusCheckStmt.close();
    		if(connection!=null) connection.close();
    	}	
    	
    }
    
    public String checkDeviceStatus(String deviceImei, String driverId, String version) throws Exception {
    	
    	Connection connection = null;
    	NamedParameterStatement deviceStatusCheckStmt = null;
    	NamedParameterStatement updateDeviceSoftwareversionStmt = null;
    	
    	try {
    		
    		LOGGER.info("Received request for device "+deviceImei+" driver "+driverId+" version "+version);
    		
    		if(driverId==null) {
    			Long id = fetchDriverId(null, deviceImei);
    			if(id!=null) driverId = id.toString();
    		}
    		
    		connection = StatusDatasourceProvider.getConnection(); 
    				//DatasourceProvider.getConnection();
    		
    		String output = null;
    		
    		deviceStatusCheckStmt = new NamedParameterStatement(connection, "SELECT CASE "
    				+ "WHEN t.tripid IS NULL THEN d.status ELSE 'occ' END AS devicestatus, "
    				+ "d.deviceid, d.sos, t.tripid, "
    				+ "CASE WHEN z.zoneid IS NOT NULL THEN (SELECT COUNT(*) + 1 FROM driver dr2 WHERE dr2.zoneid = z.zoneid "
    				+ "AND dr2.zonejointime < dr.zonejointime) ELSE NULL END AS zoneposition, z.zoneid, z.zonename, "
    				+ "z.ZoneLat, z.ZoneLong, z.ZoneRange, z.StrictBorders, z.ZonePoints "
    				+ "FROM device d LEFT OUTER JOIN trip t ON t.deviceid = d.deviceid "
    				+ "AND t.driverid=:driverid "
    				+ "AND t.status IN ('wai', 'act'), "
    				+ "driver dr LEFT OUTER JOIN zones z ON z.zoneid = dr.zoneid AND dr.zonejointime IS NOT NULL WHERE dr.deviceid = "
    				+ "d.deviceid AND (dr.active = 'true' OR (:driverid IS NOT NULL AND dr.driverid = :driverid)) "
    				+ "AND d.imei = :imei");
    		
    		updateDeviceSoftwareversionStmt =  new NamedParameterStatement(connection, 
    				Util.getProperty("updateDeviceSoftwareversion", "Queries"));
    				//TODO getQuery("updateDeviceSoftwareversion"));
    		
    		
    	
    		
    		//TODO 
    		deviceStatusCheckStmt.setString("imei", deviceImei);
    		deviceStatusCheckStmt.setString("driverid", driverId);
    		
    		LOGGER.info("Will get the status for "+deviceImei+" "+driverId);
    		
    		ResultSet rs = deviceStatusCheckStmt.executeQuery();
    		if (rs.next()) {
    		
    			//if a tripid exists, then fetch the trip data associated with it
    			String deviceStatus = rs.getString("devicestatus");
    			String sos = rs.getString("sos");
    			Long tripId = rs.getLong("tripid"); //optional
    			TripData fetchedData = null;
    			//ekos 23/01/2013
    			//Very strange bug and strange circumstances even though the trip
    			//should be null , sometimes this returnes trip assign
    			//must be closely monitored for now i have inserted the extra control
    			// that tripid must be greater than 0
    			if (tripId != null && tripId > 0L) {
    				fetchedData = FareDispatcher.getInstance().fetchTripData(tripId);
    			
    				LOGGER.info("The fetched data is "+fetchedData);
    			}
    		
    			Integer zonePosition = rs.getInt("zoneposition"); //optional
    			Integer zoneId = rs.getInt("zoneid"); //optional
    			String zoneName = rs.getString("zonename"); //optional
    			String zoneData = (zonePosition != null && zonePosition != 0 ? zonePosition : "") + 
    					"," + (zoneId != null && zoneId != 0 ?zoneId : "") + "," + (zoneName != null ? zoneName : "");
    		
    			String zoneInfo = null;
    			
    			if(zoneId.compareTo(0)>0) {
    				Double zoneLat = rs.getDouble("ZoneLat");
    				Double zoneLng = rs.getDouble("ZoneLong");
    				Double zoneRange = rs.getDouble("ZoneRange");
    				Boolean strictBorders = rs.getBoolean("StrictBorders");
    				String zonePoints = rs.getString("ZonePoints");
    				
    				zoneInfo = String.format("<zoneInfo>"
    						+ "<zoneLat>%s</zoneLat>"
    						+ "<zoneLng>%s</zoneLng>"
    						+ "<zoneRange>%s</zoneRange>"
    						+ "<strictBorders>%s</strictBorders>"
    						+ "<zonePoints>%s</zonePoints></zoneInfo>",
    						zoneLat!=null?zoneLat.toString():"",
    						zoneLng!=null?zoneLng.toString():"",
    						zoneRange!=null?zoneRange.toString():"",
    						strictBorders!=null?strictBorders.toString():"",
    						zonePoints!=null?zonePoints.toString():"");
    			}
    			
    			String trdata = fetchedData == null ? "" : (fetchedData.getAssignmentMsg() == null ? "" : fetchedData.getAssignmentMsg());
    			output = String.format(
    					getXML("DRIVER_STATUS_XML"),
    					deviceStatus, getDriverAppBuild(version), sos, zoneData,trdata+(zoneInfo!=null?zoneInfo:""));
    			Integer deviceId = rs.getInt("deviceid"); //optional
    			//Update the softwareversion of the invoking device (if it's been supplied). Used to be done from the stored procedure, now the logic is transferred to the bot.
    			if (version != null && deviceId != null) {
    				updateDeviceSoftwareversionStmt.setString("version", version);
    				updateDeviceSoftwareversionStmt.setInt("deviceid", deviceId);
    				updateDeviceSoftwareversionStmt.executeUpdate();
    			}
    		}
    		rs.close();
    		//TODO dataPool.closeConnection();
    		if (output == null) {
    			throw new RuntimeException("failed_lookup");
    		}
    		return output;
    	} finally {
    		if(deviceStatusCheckStmt!=null) deviceStatusCheckStmt.close();
    		if(updateDeviceSoftwareversionStmt!=null) updateDeviceSoftwareversionStmt.close();
    		if(connection!=null) connection.close();
    	}
    }

    private Long fetchDriverId(Long driverId,String imei) {
    	
    	if(driverId==null) {
    		try {
				driverId = fetchImeiDriver(imei);
			} catch (SQLException e) {
				LOGGER.error("Problem fetching driverid through imei");
			}
    	}
    	
    	return driverId;
    }
    
    private Long fetchImeiDriver(String imei) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement fetchImeiDriverStmt = null;
    	
    	try {
    		
    		connection = StatusDatasourceProvider.getConnection();
    		fetchImeiDriverStmt = connection.prepareStatement("SELECT driver.driverid FROM driver,device "
    				+ "WHERE driver.deviceid=device.deviceid "
    				+ "AND device.imei=? ");
    		fetchImeiDriverStmt.setString(1, imei);
    		
    		ResultSet resultSet = fetchImeiDriverStmt.executeQuery();
    		
    		Long driverId = null;
    		
    		if(resultSet.next()) {
    			driverId = resultSet.getLong("driverid");
    			if(resultSet.wasNull()) {
    				driverId = null;
    			} 
    		}
    		
    		return driverId;
    		
    	} finally {
    		if(fetchImeiDriverStmt!=null) fetchImeiDriverStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    long lastVersionUpdate = 0L;
    int versionUpdateInterval = 10 * 60000; //every ten minutes

    public int getLastPassAppBuild(String reported) {
    	final String appBuildPropertyKey = "_pass_build";
    	//return getNumberFromProps((reported != null ? reported.substring(0, 1) : "i") + appBuildPropertyKey,DEFAULT_PASS_APP_BUILD);
    	return Integer.parseInt(Util.getProperty((reported != null ? reported.substring(0, 1) : "i") + appBuildPropertyKey, "config"));
    }

    public int getDriverAppBuild(String version) {

    	final String appBuildPropertyKey = "driver_%s_build";
    	String tmp = (version == null ? "android" : version.indexOf("i") != -1 ? "ios" : "android");
    	return new Integer(Util.getProperty(String.format(appBuildPropertyKey, tmp), "config"));
    }

    public String getAppURL() {
    	return ((sAppUrl == null || sAppUrl.equals("")) ? "false" : sAppUrl);
    }

    public class PassengerStatus {

    	boolean proper;
    	int blacklistIn;
    	int discountRides;
    	String callXML;
    	int stars;
    	String companyEmail;
    	String companyFacebook;
    	String companyTwitter;

    	public PassengerStatus(boolean proper, int blacklistIn, int discountRides, int stars, String callXML) {
    		super();
    		this.proper = proper;
    		this.blacklistIn = blacklistIn;
    		this.discountRides = discountRides;
    		this.callXML = callXML;
    		this.stars = stars;
    	}


    	public void setProper(boolean proper) {
    		this.proper = proper;
    	}

    	public boolean isProper() {
    		return proper;
    	}

    	public void setBlacklistIn(int blacklistIn) {
    		this.blacklistIn = blacklistIn;
    	}

    	public int getBlacklistIn() {
    		return blacklistIn;
    	}

    	public void setDiscountRides(int discountRides) {
    		this.discountRides = discountRides;
    	}

    	public int getDiscountRides() {
    		return discountRides;
    	}

    	public void setCallXML(String callXML) {
    		this.callXML = callXML;
    	}

    	public String getCallXML() {
    		return callXML;
    	}

    	public void setStars(int stars) {
    		this.stars = stars;
    	}

    	public int getStars() {
    		return stars;
    	}

    	public void setCompanyEmail(String companyEmail) {
    		this.companyEmail = companyEmail;
    	}

    	public String getCompanyEmail() {
    		return companyEmail;
		}

    	public void setCompanyFacebook(String companyFacebook) {
    		this.companyFacebook = companyFacebook;
    	}

    	public String getCompanyFacebook() {
    		return companyFacebook;
    	}

    	public void setCompanyTwitter(String companyTwitter) {
    		this.companyTwitter = companyTwitter;
		}

    	public String getCompanyTwitter() {
    		return companyTwitter;
    	}
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

}
