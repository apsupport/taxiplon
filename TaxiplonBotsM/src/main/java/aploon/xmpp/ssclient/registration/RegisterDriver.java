package aploon.xmpp.ssclient.registration;


import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
/*
 * From android app
 */



import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;

public class RegisterDriver extends ServerSideClient {

    PreparedStatement driverRegistrationInfoStmt;
    NamedParameterStatement driverPreregStmt;

    Format formatter = new SimpleDateFormat("dd/MM/yyyy");


    public enum Actions {
    	listCountries,list,reg;
    }

    private static Logger LOGGER = Logger.getLogger(RegisterDriver.class);
    
    private void prepareDriverRegistrationInfoStmt() throws Exception {
    	if (driverRegistrationInfoStmt == null || driverRegistrationInfoStmt.isClosed()) {
    		driverRegistrationInfoStmt = dataPool.prepareStatement(getQuery("DriverRegistrationInfo"));
    	}
    }

    private void prepareDriverPreregStmt() throws Exception {
    	if (driverPreregStmt == null || driverPreregStmt.isClosed()) {
    		driverPreregStmt = new NamedParameterStatement(dataPool, getQuery("DriverPreregInsert"));
    	}
    }

    public void processRequest() {
    	try {
    		String act = request.get("act");
    		Actions myaction = Actions.valueOf(act);
    		switch (myaction) {
    			case listCountries:
    				response = RegisterPassenger.getCountries();
    				break;
    			case list:
    				response = getRegistrationInfo(request.get("imei"));
    				break;
    			case reg:
    				response = preRegisterDriver();
    				break;
    			default:
    				throw new IllegalArgumentException("action_not_supported");
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    		LOGGER.error("Unknown exception ",e);
    		response = String.format(getXML(("PREREGISTRATION_WRAPPER")), 
    				String.format(getXML("UPDATE_STATUS"), "false", e.getMessage()));		
    	}

    	try {
			comm.sendClearImeiMessageToDriver(response, caller);
		} catch (XMPPException e) {
			LOGGER.error("Xmpp error");
		}
		response = null;
		
    }


    private String preRegisterDriver() throws Exception {
    	prepareDriverPreregStmt();
    	driverPreregStmt.setString("fname", request.get("name"));
    	driverPreregStmt.setString("surname", request.get("surname"));
    	driverPreregStmt.setString("fathersname", request.get("fathersname"));
    	driverPreregStmt.setString("address", request.get("address"));
    	driverPreregStmt.setString("city", request.get("city"));
		driverPreregStmt.setString("state", request.get("state"));
		driverPreregStmt.setString("country", request.get("country"));
		driverPreregStmt.setString("pocode", request.get("po"));
		driverPreregStmt.setString("tel", request.get("tel"));
		driverPreregStmt.setString("idio", request.get("idno"));
		driverPreregStmt.setString("idoffice", request.get("idoffice"));
		driverPreregStmt.setString("vat", request.get("vat"));
		driverPreregStmt.setString("vatoffice", request.get("vatoffice"));
		driverPreregStmt.setString("plate", request.get("plate"));
		driverPreregStmt.setString("model", request.get("model"));
		driverPreregStmt.setString("licno", request.get("licno"));
		driverPreregStmt.setString("speclicno", request.get("speclicno"));
		if (request.get("speclicexp") == null)
			driverPreregStmt.setString("speclicexp", null);
		else
			driverPreregStmt.setTimestamp("speclicexp", new Timestamp(Long.parseLong(request.get("speclicexp"))));
		driverPreregStmt.setString("email", request.get("email"));
		driverPreregStmt.setString("mandate", request.get("mandate"));
		driverPreregStmt.setString("imei", request.get("imei"));
		driverPreregStmt.setString("softver", request.get("softver"));
		driverPreregStmt.setString("compid", request.get("compid"));
		driverPreregStmt.setString("mob", request.get("mob"));

		driverPreregStmt.executeUpdate();
		driverPreregStmt.close();
		return String.format(getXML("PREREGISTRATION_WRAPPER"), String.format(getXML("UPDATE_STATUS"), "true", ""));
    }

    public String getRegistrationInfo(String DeviceIMEI) throws Exception {
    	prepareDriverRegistrationInfoStmt();
    	driverRegistrationInfoStmt.setString(1, DeviceIMEI);
    	ResultSet rs = driverRegistrationInfoStmt.executeQuery();
    	String preDriver = "";
    	if (rs.next()) {
    		long id = rs.getLong("preregisterid");
    		String name = getEmptyForNull(rs, "FirstName");
    		String lastname = getEmptyForNull(rs, "LastName");
    		String fathersname = getEmptyForNull(rs, "FathersName");
    		String address = getEmptyForNull(rs, "Address");
    		String city = getEmptyForNull(rs, "City");
    		String state = getEmptyForNull(rs, "State");
    		String country = getEmptyForNull(rs, "Country");
    		String pocode = getEmptyForNull(rs, "POCode");
    		String telephone = getEmptyForNull(rs, "Telephone");
    		String email = getEmptyForNull(rs, "email");

    		String mobile = getEmptyForNull(rs, "Mobile");
    		String idno = getEmptyForNull(rs, "idnumber");
    		String idoffice = getEmptyForNull(rs, "idnumberoffice");
    		String vatcode = getEmptyForNull(rs, "vatcode");
    		String vatoffice = getEmptyForNull(rs, "vatoffice");
    		String plate = getEmptyForNull(rs, "plateno");
    		String model = getEmptyForNull(rs, "model");
    		String lic = getEmptyForNull(rs, "licenseno");
    		String speciallic = getEmptyForNull(rs, "speciallicenseno");
    		Timestamp expdate = rs.getTimestamp("LicenseExpiration");
    		Timestamp regdate = rs.getTimestamp("regdate");
    		String mandate=getEmptyForNull(rs,"ManufactureDate");
    		String comp = "";
    		try {
    			comp = getCompanyInfo(rs);
    		} catch (Exception e) {
    			//propably no company given so ignore it
    		}
    		preDriver = String.format(getXML("DRIVER_PREREGISTRATION"), id, lastname,
    				name, fathersname, address, state, city, country,
    				telephone, mobile, email, pocode, plate, model,
    				vatcode, vatoffice, idno, idoffice, lic, speciallic,
    				getRepoPath(),expdate == null ? 0 : expdate.getTime(), 
    				regdate == null ? 0 : regdate.getTime(),mandate, comp);
    	}
    	
    	rs.close();

    	return String.format(getXML("PREREGISTRATION_WRAPPER"),
    			String.format("%s\n%s", RegisterPassenger.getCountries(), preDriver));
    }

    private String getCompanyInfo(ResultSet rs) throws Exception {
    	long taxicompanyid = rs.getLong("taxicompanyid");
    	String shortname = rs.getString("shortname");
    	String officialname = rs.getString("officialname");
    	String companytype = rs.getString("companytype");
    	String address = rs.getString("address");
    	String telephone = rs.getString("telephone");
    	String fax = rs.getString("fax");
    	String email = rs.getString("email");
    	String website = rs.getString("website");
    	double hailrange = rs.getDouble("hailrange");
    	int clearbiddingtime = rs.getInt("clearbiddingtime");
    	int requestwaittime = rs.getInt("requestwaittime");
    	int appointmentremind = rs.getInt("appointmentremind");
    	int candnumber = rs.getInt("candidatenumber");
    	int avadrivers = 0;
    	double mindistance = 0;

    	//              act=list&lat=38.054024&lng=23.807006
    	String companyInfo = rs.getString("companyinfo");
    	String rect = String.format(getXML("COMPANY_EXTRA_XML"),
    			rs.getDouble("swlat"), rs.getDouble("nelat"), rs.getDouble("swlng"),
    			rs.getDouble("nelng"), "true");
    	rect += "\n" +String.format(getXML("COMPANY_ADDITIONAL_INFO_XML"),
    			rs.getDouble("Lat"), rs.getDouble("Lng"),
    			rs.getString("Country"), rs.getString("Facebook"), rs.getString("Twitter"), rs.getString("CompanyTimeZone"),
    			rs.getString("CustomerSelectable"));
    	return String.format(getXML("COMPANY_XML"), taxicompanyid, shortname, officialname,
    			companytype, address, telephone,fax, email, website, hailrange, clearbiddingtime,
    			requestwaittime, appointmentremind, candnumber, companyInfo,avadrivers, mindistance, rect);
    }

    private static RegisterDriver instance;

    public static synchronized RegisterDriver getInstance() {
    	if (instance == null) {
    		instance = new RegisterDriver();

    	}
    	return instance;
    }

    public RegisterDriver() {
    	super();
    	//setRepoPath(Util.getProperty("driver_images", "config"));
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

}
