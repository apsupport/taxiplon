package aploon.xmpp.ssclient.registration;

/*
 * Is the operation tha the passenger does in order to register.
 */


import aploon.datahelper.DataHelper;
import aploon.model.Passenger;
import aploon.sms.SMSSUtil;
import aploon.xmpp.Communicator;
import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.iphone.Push;

import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Random;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;


public class RegisterPassenger extends ServerSideClient {

    private CallableStatement silentRegisterPassengerStmt;
    private NamedParameterStatement passengerRequestStmt;
    private NamedParameterStatement passengerRenewPinStmt;
    private static String countryCodes;
    private PreparedStatement countriesStmt;

    private Logger LOGGER = Logger.getLogger(RegisterPassenger.class);

    public RegisterPassenger() throws Exception {
    	super();
    	countryCodes = buildCountryCodes();
    }

    private void prepareSilentRegisterPassengerStmt() throws SQLException {
    	if (silentRegisterPassengerStmt == null || silentRegisterPassengerStmt.isClosed()) {
    		silentRegisterPassengerStmt = dataPool.getConnection().prepareCall("CALL registerPassengerOld(?, ?, ?, ?, ?, ?, ?, ?, ?)");
    	}
    }

    private void preparePassengerRenewPinStmt() throws Exception {
    	if (passengerRenewPinStmt == null || passengerRenewPinStmt.isClosed()) {
    		passengerRenewPinStmt = new NamedParameterStatement(dataPool, getQuery("passengerRenewPin"));
    	}
    }

    private void preparepassengerRequestStmt() throws Exception {
    	if (passengerRequestStmt == null || passengerRequestStmt.isClosed()) {
    		passengerRequestStmt = new NamedParameterStatement(dataPool, getQuery("passengerRequestPin"));
    	}
    }

    public static String getCountries() {
    	return countryCodes;
    }

    private enum Action {
        pin,forget,country,register,mob,makemobile,acc
    }

    public void processRequest() {
	
    	Long companyId = parseOptionalField("compid", Long.class);
    	String softwareVersion = request.get("ver");
    	String imei = request.get("imei");
    	String mobile = parseOptionalField("mobile", String.class);
    	Long webAccount = parseOptionalField("webid", Long.class);
    	String act = parseOptionalField("act", String.class);
    	String countryCode = request.get("origin");
    	String locale = parseOptionalField("loc", String.class);
    	String pinInput = parseOptionalField("passpin", String.class);
    	String failure = null;
    	Long generatedPassengerId = null;
    	String tmp = null;
	
        Action action = null;
        
        if(act!=null) {
            action = Action.valueOf(act);
        
            LOGGER.info("Received action "+action);
        
            switch(action) {
            case pin:
                    try {
                        
                        LOGGER.info("Did you gave a taxicompanyid "+companyId);
                        response = getRegistrationPin(mobile, imei, countryCode, locale, companyId);
                    } catch (Exception e) {
                        LOGGER.error("Registration error",e);
                    }
                break;
            case forget:
                    try {
                        response = renewPin(webAccount, locale, mobile, countryCode, companyId);
                    } catch (Exception e) {
                        LOGGER.error("Renew pin error",e);
                    }
                    break;
            case country:
                LOGGER.info("You got some country codes "+countryCodes);
                response = countryCodes;
                break;
            case mob:
                response = passengerAccountUpdate(webAccount);
                break;
            case makemobile:
                response = makeMobile(webAccount);
                break;
            case acc:
                response = createOrUpdatePassenger(companyId,softwareVersion,countryCode,mobile,pinInput);
                break;
            }
        } else {

            try {

                LOGGER.info("Registering passenger the old way");

                Passenger passenger = registerPassengerOld(companyId, imei, softwareVersion, countryCode, mobile, webAccount, pinInput);
                response = String.format(getXML("REGISTRATION"),"true","",Communicator.PASS_UNAME_PREFIX+passenger.getPassengerId(),System.currentTimeMillis(),"");
                
            } catch(Exception e) {
                LOGGER.error(e);   
            }
            
        }
        
        LOGGER.info("Send back to action "+response);
        
    }

    private String passengerAccountUpdate(Long passengerId) {
        
        try {
            Passenger passenger = DataHelper.getPassenger(dataPool.getConnection(), passengerId);
            
            if(passenger!=null) {
                
                if(!passenger.getMobile().equals(passenger.getImei())) 
                    updatePassengerImeiandXMPP(passenger.getPassengerId(), passenger.getImei(), passenger.getMobile());
            
                return String.format(getXML("REGISTRATION"),
                                     "true","",
                                     Communicator.PASS_UNAME_PREFIX+passengerId,
                                     System.currentTimeMillis(),String.format("<mobile>%s</mobile>",passenger.getMobile()));
            } 
        
        } catch(SQLException e) {
            LOGGER.error(e);    
        }
        
        return null;
    }

    private void updatePassengerImeiandXMPP(Long passengerId,String oldimei,String mobile) {
        
        try {
            DataHelper.updateImeiAsMobile(dataPool.getConnection(), passengerId, mobile);
            
            try {
                String encryptedMobile = mobile;
                    //Util.encrypt(mobile);
                LOGGER.info("The encrypted mobile "+encryptedMobile);
                comm.changeAnAccountPassword(Communicator.PASS_UNAME_PREFIX+passengerId, oldimei, encryptedMobile);
            } catch(Exception e) {
                LOGGER.info("Already catching due to fixed issue");
            } 
                
        } catch(SQLException e) {
            LOGGER.error("Exception updating",e);
        }
    }

    private String createOrUpdatePassenger(Long companyId,
                                         String softwareVersion,String countryCode,
                                         String mobile,String pin) {
        
        try {
            
            LOGGER.info("Check the helper "+mobile+" "+companyId);
            
            Passenger passenger = DataHelper.mobileExists(dataPool.getConnection(), mobile, companyId, countryCode);
            
            LOGGER.info("Saw if mobile exists ");
            
            if(passenger!=null) {
                
            	if(passenger.getImei()!=null) {
            		if(!passenger.getMobile().equals(passenger.getImei())) {
            			updatePassengerImeiandXMPP(passenger.getPassengerId(), passenger.getImei(), passenger.getMobile());       
            		}
            	} else {
            		try {
            			
            			LOGGER.info("Imei was null creating the passenger");
            			createXMPPAccount(Communicator.PASS_UNAME_PREFIX + passenger.getPassengerId(), passenger.getMobile(), "", "Passenger");
            			try {
							DataHelper.setPassengerImeiAndPin(passenger.getPassengerId(), mobile,Util.digest(pin));
						} catch (Exception e) {
							LOGGER.error("Problem digesting pin",e);
						}
  
            		} catch (XMPPException e) {
						LOGGER.error("Problem creating passenger ",e);
					}
            	}
                    
            } else {
            	
            	LOGGER.info("Passenger does not exist");
            	
                passenger = registerPassenger(companyId, softwareVersion, countryCode, mobile, pin);
            }
            
            return String.format(getXML("REGISTRATION"),
                                     "true", "", 
                                     Communicator.PASS_UNAME_PREFIX + passenger.getPassengerId(),
                                     System.currentTimeMillis(),String.format("<mobile>%s</mobile>",passenger.getMobile()));        
            
        } catch(SQLException e) {
            LOGGER.error(e);    
        }
        
        return null;        
    }

    private void recreateXMMPPAccounts() {
    	/**
    	try {
    		new Thread(new Runnable() {
    			public void run() {
    	*/
    				try {
   		
    					ArrayList<Communicator> acco = new ArrayList();
    					String acc = "";
    					PreparedStatement passengerRecreateStmt = dataPool.getConnection().prepareStatement(getQuery("passengerMobile"));
    					ResultSet rs = passengerRecreateStmt.executeQuery();
    					long i = 0;
    					while (rs.next()) {
    						try {
    							Thread.sleep(10);
    							createXMPPAccount("p"+rs.getString("passengerid"),rs.getString("imei"), "", "pass");
    							System.out.println(++i);
    						} catch (Exception e) {
    							e.printStackTrace();
    						}
    					}
    					rs.close();
    					dataPool.closeConnection();
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
    	/**
    			}
    		}).start();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	*/
    }
    
    @Deprecated
    private Passenger registerPassengerOld(Long companyId, String imei, String softwareVersion, String countryCode,String mob, Long webid, String pin) throws Exception {
     
        LOGGER.info("The imei "+imei+" mobile "+mob+" webid "+webid);
        
        prepareSilentRegisterPassengerStmt();
        Long generatedPassengerId = null;
        String errMessage = null;
        int i = 1;
        String passx = "";
        try {
            
            LOGGER.info("Check the val ");
            
            dataPool.getConnection().setAutoCommit(false);
            silentRegisterPassengerStmt.setString(1, imei);
            silentRegisterPassengerStmt.setString(2, companyId == null ? null : companyId.toString());
            silentRegisterPassengerStmt.setString(3, softwareVersion);
            silentRegisterPassengerStmt.setString(4, countryCode);
            silentRegisterPassengerStmt.setString(5, mob);
            if (webid == null) {
                silentRegisterPassengerStmt.setString(6, null);
            } else {
                silentRegisterPassengerStmt.setLong(6, webid);
            }
            silentRegisterPassengerStmt.setString(7, pin == null ? null : Util.digest(pin));
            
            LOGGER.info("The pin is "+Util.digest(pin));
            
            silentRegisterPassengerStmt.registerOutParameter(8, Types.NUMERIC);
            silentRegisterPassengerStmt.registerOutParameter(9, Types.VARCHAR);
            silentRegisterPassengerStmt.executeUpdate();
            dataPool.getConnection().commit();
            generatedPassengerId = silentRegisterPassengerStmt.getLong("passengerIdVar");
            passx = silentRegisterPassengerStmt.getString("passIMEIVar");
        } catch(SQLException e) {
            
            errMessage = e.getMessage() == null?"sql_error":e.getMessage();
            dataPool.getConnection().rollback();
        } finally {
            dataPool.getConnection().setAutoCommit(true);
            if (errMessage != null) {
                throw new Exception(errMessage);
            }
        }
        
        try {
            createXMPPAccount(Communicator.PASS_UNAME_PREFIX + generatedPassengerId, imei, "", "Passenger");
        } catch(Exception e) {
            System.err.println("Failed to create an account with ID " + Communicator.PASS_UNAME_PREFIX +
                                           generatedPassengerId + " due to exception: " + e.getMessage() +
                                           ". Most likely this device has been registered before.");
        }
    
        Passenger passenger = new Passenger();
        passenger.setPassengerId(generatedPassengerId);
        passenger.setImei(passx);
    
        return passenger;
    }

    private Passenger registerPassenger(Long companyId, String softwareVersion, String countryCode,
					  String mob,String pin) throws SQLException {

        
    	LOGGER.info("Registering passenger through the procedure");
    	
        Passenger passenger = null;
        CallableStatement registerPassengerStmt = null;
        
        try {
        
            registerPassengerStmt = dataPool.getConnection().prepareCall("CALL registerPassenger(?, ?, ?, ?, ?, ?)");
            dataPool.getConnection().setAutoCommit(false);
            
            registerPassengerStmt.setString(1, companyId == null ? null : companyId.toString());
            registerPassengerStmt.setString(2, softwareVersion);
            registerPassengerStmt.setString(3, countryCode);
            registerPassengerStmt.setString(4, mob);
            
            
            String encryptPin = "";
            
            try {
                encryptPin = Util.digest(pin);
            } catch(Exception e) {
                LOGGER.error("Digest exception",e);
            }
            
            registerPassengerStmt.setString(5, encryptPin);
            registerPassengerStmt.registerOutParameter(6, Types.NUMERIC);
            
            registerPassengerStmt.executeUpdate();
            passenger = new Passenger();
            passenger.setPassengerId(registerPassengerStmt.getLong("passengerIdVar"));
            passenger.setMobile(mob);
            dataPool.getConnection().commit();        

            LOGGER.info("Will try to create the xmpp account");
            
            try {
                createXMPPAccount(Communicator.PASS_UNAME_PREFIX + passenger.getPassengerId(),mob, "", "Passenger");
            } catch(Exception e){
                LOGGER.error(e);
            }

            passenger.setMobile(mob);
            passenger.setImei(mob);
            
            return passenger;
            
        } catch(SQLException e) {
            LOGGER.error("THe exception on rollback",e);
            dataPool.getConnection().rollback();
            return null;
        } finally {
            if(registerPassengerStmt!=null) registerPassengerStmt.close();
            dataPool.getConnection().setAutoCommit(true);
        }

    }


    String getRegistrationPin(String mob, String imei, String country, String loc, Long companyid) throws Exception {
    	
    	LOGGER.info("We are going to get a registration pin");

    	if (mob == null ) {
    		throw new RuntimeException("mobile_or_imei_not_valid");
    	}
        
        LOGGER.info("Getting the driver id "+country+" the companyyid "+companyid);
        
        preparepassengerRequestStmt();

        //passengerRequestStmt.setString("phoneid", imei);
        passengerRequestStmt.setString("mobilephone", mob);
        passengerRequestStmt.setString("orig", country);
        passengerRequestStmt.setString("comp", companyid == null ? null : companyid + "");


        ResultSet rs = passengerRequestStmt.executeQuery();
        int iAccountNum = 0;
        Long webaccount = 0L;
        String pin = null, passname = null, passlastname = null;
        while (rs.next()) {
        	String origC = rs.getString("origincountry");
        	Long passId = rs.getLong("passengerid");
        	String mobile = rs.getString("mobile");
        	pin = rs.getString("pin");
        	if (pin != null && mobile != null &&
        			((mob.contains(mobile) && origC == null) || ((origC != null && mob.contains(mobile) &&
        			(country!=null&&country.equals(origC)) )))) {
        		webaccount = passId;
        		passname = rs.getString("name");
        		passlastname = rs.getString("lastname");
        	}
        	iAccountNum++;
        }
	
        rs.close();
        dataPool.closeConnection();
        if (pin != null && webaccount == 0L) {
        	pin = null;
        	passname = null;
	    	passlastname = null;
        }	
	
        if (pin == null) {
        	//This is a new pin client
        	int iPin = new Random().nextInt(1000);
        	String tempo;
        	try {
        		tempo =
        				String.format(Util.getProperty("sms_pin_" + (loc != null ? loc : Push.DEFAULT_PASSENGER_LOCALE), "lang"), iPin);
        	} catch (Exception e) {
        		tempo = String.format(Util.getProperty("sms_pin_en", "lang"), iPin);
        	}
        	
        	Long compid = getCompany(companyid, country);
        	
        	LOGGER.debug("SMD Will send sms from "+compid+" to "+mob+" "+tempo);
	    
            SMSSUtil smsProvider = new SMSSUtil(compid);
            smsProvider.setCountryCode(country);
            
            if(smsProvider.isEnabled()) {
                smsProvider.sendSmsAsync(mob, tempo);
                    //sendSms(mob, tempo);
            } else {
                LOGGER.info("No sms provider");
                //SMSSender sms = SMSSender.getInstance(country + (companyid == null ? "" : companyid));
                //sms.sendSMS(mob, null, tempo, country);
                SMSSUtil.sendDefaultSmsAsync(mob, tempo, null);
            }
            
            pin = String.format("%03d", iPin);
            iAccountNum = 0;
        }
        
        LOGGER.info("Due to already having an account will send the already in account");
        
        String answear = String.format(getXML("REGISTRATION_PIN"), "true", "", webaccount, pin == null ? "" : pin,
			     iAccountNum > 0 ? "true" : "false", passname == null ? "" : passname, passlastname == null ? "" : passlastname,
			     "");
        
        LOGGER.info("Just check the answear "+answear);
        
        return answear;
    }

    private String renewPin(Long webid, String locale, String mobo, String country, Long company) throws Exception {
    	if (webid == null) {
    		throw new Exception("no_pass_id");
    	}
    	if (mobo == null || country == null) {
    		throw new Exception("no_mobile_or_country");
    	}

    	preparePassengerRenewPinStmt();

    	int iPin = new Random().nextInt(1000);
    	String pin = String.format("%03d", iPin);
    	passengerRenewPinStmt.setString("pinvalue", Util.digest(pin));
    	passengerRenewPinStmt.setLong("passid", webid);
    	passengerRenewPinStmt.executeUpdate();
    	dataPool.closeConnection();
	
    	LOGGER.info("Getting the passenger pin "+iPin+" "+"sms_pin_" + (locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE));
    	LOGGER.info("Pockemon gotta catch em all "+Util.getProperty("sms_pin_" + (locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE), "lang"));
    	
    	
        String tempo = String.format(Util.getProperty("sms_pin_" + (locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE), "lang"),iPin);
        
        SMSSUtil smsProvider = new SMSSUtil(getCompany(company, country));
        smsProvider.setCountryCode(country);
        
        if(smsProvider.isEnabled()) {
            smsProvider.sendSmsAsync(mobo, tempo);
        } else {
            LOGGER.info("No sms provider");
            //SMSSender sms = SMSSender.getInstance(country + (company == null ? "" : company));
            //sms.sendSMS(mobo, null, tempo, country);
            SMSSUtil.sendDefaultSmsAsync(mobo, tempo, null);
        }
        
        return String.format(getXML("REGISTRATION_PIN"), "true", "", webid, pin, "false", "", "", "");
    }

    private String makeMobile(Long passengerId) {
    
        try {
            
            Passenger passenger = DataHelper.getPassenger(dataPool.getConnection(), passengerId);
            if(passenger!=null) {
                try {
                    createXMPPAccount(Communicator.PASS_UNAME_PREFIX + passengerId, passenger.getMobile(), "", "Passenger");
                    return String.format(getXML("REGISTRATION_PIN"),"true","Account set to mobile successfully",passengerId,passenger.getMobile(),"false","","","");
                } catch(Exception e) {
                    e.printStackTrace();    
                }
            }
        } catch(SQLException e) {
            LOGGER.error("The sql exception ",e);    
        }
    
        return String.format(getXML("REGISTRATION_PIN"),"false","Fola",passengerId,"","false","","","");
    }

    private String buildCountryCodes() throws Exception {
    	
    	String acc = "";
    	if (countriesStmt == null || countriesStmt.isClosed()) {
    		countriesStmt = dataPool.getConnection().prepareStatement(getQuery("passengerCountryCodes"));
    	}
    	ResultSet rs = countriesStmt.executeQuery();
    	while (rs.next()) {
    		acc += String.format(getXML("REGISTRATION_COUNTRY"), rs.getString("Country"), rs.getString("ISOCode"), rs.getString("CALLCode")) +"\n";
    	}
    	
    	rs.close();
    	dataPool.closeConnection();
    	return String.format(getXML("REGISTRATION_COUNTRYCODES"), acc);
    }


    protected String getPassword() {

    	return this.getClass().getSimpleName();
    }

    public static void main(String[] args) {
    	try {
    		RegisterPassenger pa = new RegisterPassenger();
    		pa.listenForMessages();

    		pa.recreateXMMPPAccounts();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private Long getCompany(Long companyId,String origin) {
        
        if(companyId==null) {

            try {
                companyId = DataHelper.getDefaultCompany(dataPool.getConnection(), origin);
            } catch (SQLException e) {
                LOGGER.error("Problem fetching taxi company",e);
            }
        }
        
        return companyId;
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
    
}