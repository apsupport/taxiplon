package aploon.xmpp.ssclient.registration;

import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.entities.Updater;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Random;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;

/**
 * Deprecated use the Creator interface instead.
 */
public class RegisterDevice extends ServerSideClient {
	
	private static RegisterDevice instance;
	
	public static synchronized RegisterDevice getInstance() throws Exception {
		if(instance == null) {
			instance = new RegisterDevice();
		}
		return instance;
	}
	
	private CallableStatement silentRegisterDeviceStmt;
	private PreparedStatement findImeiByPlatenoStmt;
	private CallableStatement createOrUpdateDriverStmt;
	
	protected Random pinGenerator;
	
	private static Logger LOGGER = Logger.getLogger(RegisterDevice.class);
	
	private RegisterDevice() throws Exception {
		super();
	    pinGenerator = new Random();
	}
	
	private void prepareSilentRegisterDeviceStmt() throws Exception {
		if(silentRegisterDeviceStmt == null || silentRegisterDeviceStmt.isClosed()) {
		    silentRegisterDeviceStmt = dataPool.getConnection().prepareCall(getQuery("silentRegisterDevice"));
		}
	}
	
	private void prepareFindImeiByPlatenoStmt() throws Exception {
		if(findImeiByPlatenoStmt == null || findImeiByPlatenoStmt.isClosed()) {
		    findImeiByPlatenoStmt = dataPool.getConnection().prepareCall(getQuery("findImeiByPlateno"));
		}
	}
	
	private void prepareCreateOrUpdateDriverStmt() throws Exception {
		if(createOrUpdateDriverStmt == null || createOrUpdateDriverStmt.isClosed()) {
		    createOrUpdateDriverStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateDriver"));
		}
	}

	public void processRequest() {
	    String failure = null;
//	    String softwareVersion = request.get("ver");//this is automatically sent by the software itself
		Long driverId = parseOptionalField("driverid", Long.class);//if not null will driver update
//		try {
//			driverId = Long.parseLong(request.get("driverid"));
//		}
//		catch(Exception e) {
//			driverId = null;
//		}
		Long deviceId = parseOptionalField("deviceid", Long.class);//for device update, use this
//		try {
//			deviceId = Long.parseLong(request.get("deviceid"));
//		}
//		catch(Exception e) {
//			//do nothing, this must be the case of driver update
//		}
	    GeneratedData driverGeneratedData = null;
	    try {
			String plateNo = request.get("plateno");
			if((plateNo == null || "".equals(plateNo)) && driverId == null) {
				throw new RuntimeException("plateno_empty");
			}
			else if(driverId != null){
				//update driver
			    driverGeneratedData = processRegisterDriverRequest(request, deviceId, driverId);
			}
			else {
				String imei = request.get("imei");
				String carModel = request.get("mod");
				deviceId = registerDevice(imei, plateNo, carModel);
				if(deviceId == 0) {//this means that this platenumber is already in the DB with a different IMEI
					throw new RuntimeException("imei_diff");
				}
				else {
					driverGeneratedData = processRegisterDriverRequest(request, deviceId, null);
				}
			}
		}
		catch(Exception e) {
		    e.printStackTrace();
		    failure = e.getMessage();
		}
	    response = String.format(getXML("DRIVER_REGISTRATION"),  
								 (failure == null ? "true" : "false"), 
								 failure == null? "" : failure, 
								 failure == null && deviceId != null? deviceId : 0,
								 driverGeneratedData != null? driverGeneratedData.getDriverId() : 0,
								 driverGeneratedData != null? driverGeneratedData.getPin() : "",
								 driverGeneratedData != null && driverGeneratedData.getCodename() != null? driverGeneratedData.getCodename() : "",
								 System.currentTimeMillis());
	    
	    try {
			comm.sendClearImeiMessageToDriver(response, caller);
		} catch (XMPPException e) {
			//e.printStackTrace();
			LOGGER.error("Problem sending message",e);
		}
	    
	}
	
	private Long registerDevice(String imei, String plateNo, String carModel) throws Exception {
		prepareSilentRegisterDeviceStmt();
	    prepareFindImeiByPlatenoStmt();
	    Long generatedDeviceId = null;
		if(imei == null || "".equals(imei)) {
			findImeiByPlatenoStmt.setString(1, plateNo);
			ResultSet rs = findImeiByPlatenoStmt.executeQuery();
			if(rs.next()) {
				imei = rs.getString("imei");
			}
			else {
				rs.close();
				throw new RuntimeException("imei_empty");
			}
			rs.close();
		}
		int i = 1;
		silentRegisterDeviceStmt.setString(i++, imei);
		silentRegisterDeviceStmt.setString(i++, plateNo.toUpperCase());
		silentRegisterDeviceStmt.setString(i++, carModel);
		silentRegisterDeviceStmt.registerOutParameter(i++, Types.NUMERIC);
		silentRegisterDeviceStmt.executeUpdate();
		generatedDeviceId = silentRegisterDeviceStmt.getLong("deviceIdVar");
		dataPool.closeConnection();
		//fix for the issue #0000012
//		try {
//			createXMPPAccount(Long.toString(generatedDeviceId), imei, "", "Driver");
//		}
//		catch(Exception e) {
//			//this will mostly handle the conflict in creation
//			e.printStackTrace();
//		}
	    return generatedDeviceId;
	}

	public GeneratedData registerDriver(Long deviceId, Long driverId, String name, String surname, String sex, String address, String landline, String mobile, String email, Long companyId, String pin, String status, String invoiceNumber, String customerNumber, String comments) throws Exception {
		prepareCreateOrUpdateDriverStmt();
		int updatedRows = 0;
		Long generatedDriverId = null;
		int i = 1;
		if(pin == null && driverId == null) {
			pin = String.format("%04d", pinGenerator.nextInt(9999));
		}
		String codename = (driverId == null? surname.substring(0, surname.length() < 3? surname.length() : 3) + name.substring(0, name.length() < 3? name.length() : 3) : null);
		createOrUpdateDriverStmt.setString(i++, (deviceId == null? null : deviceId.toString()));
	    createOrUpdateDriverStmt.setString(i++, (driverId == null? null : driverId.toString()));
		createOrUpdateDriverStmt.setString(i++, name);
		createOrUpdateDriverStmt.setString(i++, surname);
		createOrUpdateDriverStmt.setString(i++, sex);
		createOrUpdateDriverStmt.setString(i++, address);
		createOrUpdateDriverStmt.setString(i++, landline);
		createOrUpdateDriverStmt.setString(i++, mobile);
		createOrUpdateDriverStmt.setString(i++, email);
		createOrUpdateDriverStmt.setLong(i++, (companyId == null? 1 : companyId));
	    createOrUpdateDriverStmt.setString(i++, pin);
	    createOrUpdateDriverStmt.setString(i++, comments);
	    createOrUpdateDriverStmt.setString(i++, codename);
	    createOrUpdateDriverStmt.setString(i++, invoiceNumber);
	    createOrUpdateDriverStmt.setString(i++, customerNumber);
	    createOrUpdateDriverStmt.setString(i++, status);
//	    createOrUpdateDriverStmt.registerOutParameter(i++, Types.NUMERIC);
//	    createOrUpdateDriverStmt.registerOutParameter(i++, Types.VARCHAR);
		try {
			updatedRows = createOrUpdateDriverStmt.executeUpdate();
		}
		catch(SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("driver_fk_violation");
		}
		generatedDriverId = createOrUpdateDriverStmt.getLong("driverIdOut");
		codename = createOrUpdateDriverStmt.getString("codenameVar");
	    dataPool.closeConnection();
		return new GeneratedData(updatedRows, driverId == null? generatedDriverId : driverId, pin, codename);
	}
	
	/**
	 *
	 * @param requestParams
	 * @param deviceId
	 * @param driverId
	 * @return
	 * @throws Exception
	 * @deprecated use the Entity bot now to insert manually each of device/vehicle/sim/driver
	 */
	public GeneratedData processRegisterDriverRequest(HashMap<String, String> requestParams, Long deviceId, Long driverId) throws Exception {
		if(deviceId == null && driverId == null) {
			throw new RuntimeException("device_id_empty");
		}
		Long companyId = parseOptionalField("compid", Long.class);
//		try {
//			companyId = Long.parseLong(requestParams.get("compid"));
//		}
//		catch (Exception e) {
//			companyId = null;
//		}
	    String name = requestParams.get("name");
		String surname = requestParams.get("surname");
		String sex = requestParams.get("sex");
		String address = requestParams.get("address");
		String landline = requestParams.get("phone");
		String mobile = requestParams.get("mobile");
		String email = requestParams.get("email");
	    String pin = requestParams.get("pin");
		String status = requestParams.get("status");
		String invoiceNumber = requestParams.get("invno");
		String customerNumber = requestParams.get("custno");
		String comments = requestParams.get("comments");
	    GeneratedData generatedData = null;
		generatedData = registerDriver(deviceId, driverId, name, surname, sex, address, landline, mobile, email, companyId, pin, status, invoiceNumber, customerNumber, comments);
		//update more data with the same request
		//TODO merge these procedures into one solid register/update
		Updater.getInstance().processRequest(requestParams, deviceId, generatedData.getDriverId());
		return generatedData;
	}

	public class GeneratedData {
		
		int updatedRows;
		long driverId;
		String pin;
		String codename;
		
		GeneratedData(int updatedRows, long driverId, String pin, String codename) {
			this.driverId = driverId;
			this.pin = pin;
			this.codename = codename;
		}

		public void setDriverId(long driverId) {
			this.driverId = driverId;
		}

		public long getDriverId() {
			return driverId;
		}

		public void setPin(String pin) {
			this.pin = pin;
		}

		public String getPin() {
			return pin;
		}

		public void setCodename(String codename) {
			this.codename = codename;
		}

		public String getCodename() {
			return codename;
		}

		public void setUpdatedRows(int updatedRows) {
			this.updatedRows = updatedRows;
		}

		public int getUpdatedRows() {
			return updatedRows;
		}
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
