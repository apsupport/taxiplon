package aploon.xmpp.ssclient;

import com.taxiplon.db.NamedParameterStatement;

import aploon.xmpp.ServerSideClient;

import com.google.gson.Gson;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.jivesoftware.smack.packet.IQ;

/**
 * The first bot dest try with JSON response messages. (OBSOLETE)
 */
@Deprecated
public class Statistics extends ServerSideClient {
	
	NamedParameterStatement getStatsPerDriverForDaysStmt;
	NamedParameterStatement getLikesFavsPerDriverStmt;
	
	public Statistics() {
		super();
	}
	
	private void prepareGetStatsPerDriverForDaysStmt() throws Exception {
		if(getStatsPerDriverForDaysStmt == null || getStatsPerDriverForDaysStmt.isClosed()) {
		    getStatsPerDriverForDaysStmt = new NamedParameterStatement(dataPool, getQuery("getStatsPerDriverForDays"));
		}
	}
	
	private void prepareGetLikesFavsPerDriverStmt() throws Exception {
	    if(getLikesFavsPerDriverStmt == null || getLikesFavsPerDriverStmt.isClosed()) {
	        getLikesFavsPerDriverStmt = new NamedParameterStatement(dataPool, getQuery("getLikesFavsPerDriver"));
	    }
	}

	public void processRequest() {
		String failure = null;
		try {
			String driverid = request.get("driverid");
			long[] statsPerDriver = getStatsPerDriver(driverid);
			long[] likesFavs = getLikesFavsPerDriver(driverid);
		    long[] globalStats = getStatsPerDriver(null);
			response = String.format(getJSON("CALLS_STATS"), driverid, 
										 statsPerDriver[0], statsPerDriver[1], statsPerDriver[2],
										 likesFavs[0], likesFavs[1], likesFavs[2]) 
						   + "," +
						   String.format(getJSON("CALLS_STATS"), "", 
										 globalStats[0], globalStats[1], globalStats[2],
										 0, 0, 0);
		}
		catch(Exception e) {
			e.printStackTrace();
			failure = e.getMessage();
		}
		response = String.format(getJSON("STATS"), failure == null? response : String.format(getJSON("GENERIC_FAILURE"), failure));
	}
	
	protected long[] getStatsPerDriver(String driverid) throws Exception {
		long[] inputOutput = new long[]{1L, 7L, 31L};
		prepareGetStatsPerDriverForDaysStmt();
		getStatsPerDriverForDaysStmt.setString("driverid", driverid);
		//execute for each case (daily, weekly, monthly stats, for himself and for everybody)
		ResultSet rs = null;
		for(int i = 0; i < inputOutput.length; i++) {
			getStatsPerDriverForDaysStmt.setLong("days", inputOutput[i]);
			rs = getStatsPerDriverForDaysStmt.executeQuery();
			if(rs.next()) {
				inputOutput[i] = rs.getLong(1);
			}
		}
		if(rs != null) {
			rs.close();
		}
		dataPool.closeConnection();
		return inputOutput;
	}
	
	protected long[] getLikesFavsPerDriver(String driverid) throws Exception {
	    long[] inputOutput = new long[3];
	    prepareGetLikesFavsPerDriverStmt();
	    getLikesFavsPerDriverStmt.setString("driverid", driverid);
		ResultSet rs = getLikesFavsPerDriverStmt.executeQuery();
		if(rs.next()) {
			inputOutput[0] = rs.getLong("likes");
		    inputOutput[1] = rs.getLong("trips");
		    inputOutput[2] = rs.getLong("favs");
		}
		rs.close();
	    dataPool.closeConnection();
		return inputOutput;
	}
	
	class Stats {
		public ArrayList<CallsStats> CallsStats;
	}
	
	class CallsStats {
	    public String driverid;
		public Long daily;
		public Long weekly;
		public Long monthly;
		public Long likes;
		public Long trips;
		public Long favs;
	}
	
	/**
	 * test case
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Statistics s = new Statistics();
			String driverid = "46";
			long[] statsPerDriver = s.getStatsPerDriver(driverid);
			long[] likesFavs = s.getLikesFavsPerDriver(driverid);
			long[] globalStats = s.getStatsPerDriver(null);
			String content = String.format(s.getJSON("STATS"), String.format(s.getJSON("CALLS_STATS"), driverid, 
										 statsPerDriver[0], statsPerDriver[1], statsPerDriver[2],
										 likesFavs[0], likesFavs[1], likesFavs[2]) 
						   + "," +
						   String.format(s.getJSON("CALLS_STATS"), "", 
										 globalStats[0], globalStats[1], globalStats[2],
										 0, 0, 0));
			Stats st = new Gson().fromJson(content, Stats.class);
			System.out.println(st);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
