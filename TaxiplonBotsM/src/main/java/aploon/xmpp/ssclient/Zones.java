package aploon.xmpp.ssclient;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;

import com.taxiplon.Util;

import aploon.model.Driver;
import aploon.model.Zone;
import aploon.datahelper.DataHelper;
import aploon.xmpp.ServerSideClient;

/*
 * Zones => taxi zones, enter in a zone, leave from a zone, list of all
 * taxi zones. (piatsa)
 */
public class Zones extends ServerSideClient {
       
	private static Zones instance;
	
	private static List<Zone> loadedZones;
	
	public static synchronized Zones getInstance() {
		if(instance == null) {
			instance = new Zones();
		}
		return instance;
	}
	
	private static final Logger LOGGER = Logger.getLogger(Zones.class);
	
	private Zones() {
		super();
	}

	/*
	private NamedParameterStatement updateDriverWithZoneStmt;
	*/
	
	/*
	private NamedParameterStatement findDriversInZoneStmt;
	private NamedParameterStatement getLastDriverZoneStmt;
	private CallableStatement makeDriverFirst;
	*/
	
	/*
	private void prepareStatements() throws Exception {
	    
		if(updateDriverWithZoneStmt == null || updateDriverWithZoneStmt.isClosed()) {
		updateDriverWithZoneStmt = new NamedParameterStatement(dataPool, getQuery("updateDriverWithZone"));
	    }
	    
		if(findDriversInZoneStmt == null || findDriversInZoneStmt.isClosed()) {
	        findDriversInZoneStmt = new NamedParameterStatement(dataPool, getQuery("findDriversInZone"));
	    }
	    
	    if(getLastDriverZoneStmt == null || getLastDriverZoneStmt.isClosed()) {
	        getLastDriverZoneStmt = new NamedParameterStatement(dataPool, getQuery("getLastDriverZone"));
	    }
	    
	}
	*/

	@Override
	public void processRequest() {
		try {
			Long zoneId = parseOptionalField("zoneid", Long.class);
            Long driverId = Long.parseLong(request.get("driverid"));
            String act = request.get("action");
            
            Double lat = parseOptionalField("lat", Double.class);
            Double lng = parseOptionalField("lng", Double.class);
            
            updateDriverZone(zoneId, driverId, act != null,lat,lng);//in case the driver left, the updateDriverZone method will return the zone he used to be in
            response = String.format(getXML("DRIVER_ZONE"), "true", null);
		} catch(Exception e) {
			e.printStackTrace();
			response = String.format(getXML("DRIVER_ZONE"), "false", e.getMessage());
		}
	}
	
	public void updateDriverZone(Long newZoneId, Long driverId, boolean setFirst,Double lat,Double lng) throws Exception {
		
		LOGGER.info("Will now update the driver with the zone "+newZoneId+" "+driverId);
	
		//TODO
		/**
		if(!DataHelper.isDriverActive(driverId)) {
			throw new RuntimeException("driverid_wrong");
		}
		*/
		
		//prepareStatements();
		//get driver's last zone
		
		/*
		getLastDriverZoneStmt.setInt("driverid", driverId);
		ResultSet rs = getLastDriverZoneStmt.executeQuery();
		Integer lastZoneId = null;
		String lastZoneName = "";
		String deviceImei = "";
		if(rs.next()) {
			lastZoneId = rs.getInt("zoneid");
			lastZoneName = rs.getString("zonename");
			deviceImei = rs.getString("imei");
		}
		rs.close();
		*/
		
		Long lastZoneId = null;
		String deviceImei = "";
		
		Map<String, Object> lastDriverZone = DataHelper.getLastDriverZone(driverId);
		
		if(lastDriverZone!=null) {
			
			lastZoneId = (Long) lastDriverZone.get("zoneid");
			deviceImei = (String) lastDriverZone.get("imei");
			
			LOGGER.info("The last driver zone "+lastZoneId+" "+deviceImei);
		
		}
		
		if (setFirst){
		    if (lastZoneId == null)
		        throw new RuntimeException("previous_zone_emty");
		    
		    DataHelper.setDriverFirst(lastZoneId, driverId);
		    newZoneId = lastZoneId;
		    
		}else{
		    //update this driver with the new zoneid
		    
			/*
			 * Old and bad code
			 */
			
			/*
			updateDriverWithZoneStmt.setString("zoneid", newZoneId != null? newZoneId.toString() : null);
		    updateDriverWithZoneStmt.setInt("driverid", driverId);
	
		    int result = updateDriverWithZoneStmt.executeUpdate();
		    */
		    
			int result = 0;
			
			
			if(lat!=null&&lng!=null) {
			
				boolean insideZone= insideZone(lat,lng,lastZoneId);
				if(!insideZone) {
					LOGGER.info("Removing the driver from zone due to not in range");
					result = DataHelper.removeDriverFromZone(driverId);
					return;
				} else {
					return;
				}
			} 
			
			if(newZoneId!=null) {
				result = DataHelper.updateDriverWithZone(newZoneId, driverId);
			} else {
				result = DataHelper.removeDriverFromZone(driverId);
			}
			
			if(result==0) {
				throw new RuntimeException("driverid_wrong");
			}
			/*
			else if(result==-1) {
				throw new RuntimeException("You are not in the zone range.");
			}
			*/
		}
		//proceed to inform all drivers from the same (new) zone about their queue number
		if(newZoneId == null) {//zoneid is null means the driver left the zone
		    LOGGER.info("Yes new zoneid is null");
			//send a message to the device going out (get out of the zone, means zoneid = null) of the zone with order -1
		    comm.sendMessageToDriver(String.format(getXML("ZONE_QUEUE_POSITION"), driverId, -1),  deviceImei);
		}
		else {
		    propagateQueuePosition(newZoneId);
		}
		if(lastZoneId != null){
		    propagateQueuePosition(lastZoneId);
		}
	}
	
	private void propagateQueuePosition(long zoneId) throws Exception {
	
		/*
		ArrayList<Driver> driversInZone = getDriversByZone(zoneId);
	    for(Driver d : driversInZone) {
	        //send to this imei a message with the new number in the queue
	        comm.sendMessageToDriver(String.format(getXML("ZONE_QUEUE_POSITION"), d.getId(), d.getOrder()),  d.getImei());
	    }
	    */
		
		List<Driver> driversInZone = DataHelper.getDriversByZone(zoneId);
		
		int order = 0;
		for(Driver driver:driversInZone) {
			
			order++;
			LOGGER.info("Sending messages to drivers for zones "+driver.getDriverId()+" "+order+" "+driver.getDevice().getImei());
			comm.sendMessageToDriver(String.format(getXML("ZONE_QUEUE_POSITION"), driver.getDriverId(),order),  driver.getDevice().getImei());
		
		}
	}

	private boolean insideZone(Double lat,Double lng,Long zoneId) {
			
		Zone zone;
		try {
				
			if(zoneId!=null) {
				zone = DataHelper.getZone(zoneId);
				Double driverDistance = Util.getDistance(lat, lng, zone.getZoneLat(), zone.getZoneLong());
				
				LOGGER.info("The zone is "+zone.getZoneLat()+" "+zone.getZoneLong()+" "+lat+" "+lng+" "+driverDistance);
				
				return driverDistance<=zone.getZoneRange();
			}
		} catch (SQLException e) {
			LOGGER.error("SQL fatal exception",e);
		}
		
		return true;
	}
	
	/*
	private void setDriverFirst(int zone , int driver) throws Exception{
	    
		if (makeDriverFirst == null || makeDriverFirst.isClosed()){
	        makeDriverFirst = dataPool.getConnection().prepareCall(getQuery("makeDriverFirstInZone"));
	    }
	   
		int i=1;
	    makeDriverFirst.setInt(i++, zone);
	    makeDriverFirst.setInt(i++, driver);
	    makeDriverFirst.registerOutParameter(i++, Types.DATE);//driver
	    makeDriverFirst.execute();
	    Date dt=makeDriverFirst.getDate("lastzone");   
	    makeDriverFirst.close();
	    dataPool.getConnection().close();
	    
	    if (dt == null) {
	    	throw new RuntimeException("driver_not_in_zone");
	    }

	}
	*/
	
	public void kickDriversFromZone(List<Long> driverIds,Long zoneId) {
		
		for(Long driverId:driverIds) {
			
			try {
				DataHelper.removeDriverFromZone(driverId);
				Driver driver = DataHelper.getDriversDevice(driverId);
				try {
					comm.sendMessageToDriver(String.format(getXML("ZONE_QUEUE_POSITION"), driverId,-1),driver.getDevice().getImei());
				} catch (XMPPException e) {
					LOGGER.error("Xmpp exception while kicking driver from zone",e);
				}
			} catch (SQLException e) {
				LOGGER.error("Problem removing driver from zone ",e);
			}
		}
		
		try {
			propagateQueuePosition(zoneId);
		} catch (Exception e) {
			LOGGER.error("rror propagating que positions");
		}
		
	}
	
	public static Long useFirstDriverFromZone(long zoneId) throws SQLException {
		
		List<Driver> driversInZone = DataHelper.getDriversByZone(zoneId);
		if(driversInZone.isEmpty()) {
			throw new RuntimeException("zone_empty");
		}
		
		Driver driver = driversInZone.get(0);
		//DataHelper.removeDriverFromZone(driver.getDriverId());
		return driver.getDriverId();
		
		//ArrayList<Driver> driversInZone = getDriversByZone(zoneId);
		
		//if(driversInZone.isEmpty()) {
		//    throw new RuntimeException("zone_empty");
		//}
		
		//Driver firstDriverInZone = driversInZone.get(0);
		//int driverId = firstDriverInZone.getId();
		//ekos this was causing the problem that the driver was removed from the zone before he would get the 
		//assigment, if it causes any problems it should be uncommented
		//updateDriverZone(null, driverId);
		//return driverId;
		
	}

	private void setZonesFromDatabase() {

        LOGGER.debug("Setting the zones from database");

        try {
                Zones.loadedZones = DataHelper.fetchAllZones();
        } catch (SQLException e) {
                e.printStackTrace();
        }

	}
    
	private void updateLoadedZone(long zoneId) {

        LOGGER.debug("Fetching and updating the zone id");
        Zone zone = null;
        
        try {
                zone = DataHelper.getZone(zoneId);
        } catch (SQLException e) {
                e.printStackTrace();
        }   

        if(loadedZones.size()>0) {
        	for(int i=0;i<loadedZones.size();i++) {
        		if(loadedZones.get(i).getZoneId().equals(zoneId)) {
        			loadedZones.set(i, zone);
                }   
        	}   
        }   

    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
