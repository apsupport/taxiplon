package aploon.xmpp.ssclient;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.IQ.Type;
import org.jivesoftware.smackx.packet.Version;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import aploon.concurrency.util.NamingThreadFactory;
import aploon.datahelper.DataHelper;
import aploon.model.Passenger;
import aploon.sms.SMSSUtil;
import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.iphone.Push;
import aploon.xmpp.ssclient.registration.RegisterDriver;

import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;


public class Login extends ServerSideClient {

    PreparedStatement findDriversPerDeviceStmt;
    PreparedStatement driverLoginLogStmt;

    NamedParameterStatement driverLoginAttemptStmt;
    NamedParameterStatement userLoginAttemptStmt;
    NamedParameterStatement userLoginAttemptStmtOld;

    NamedParameterStatement passLoginAttemptStmt;
    NamedParameterStatement passLoginAttemptcStmt;
    
    private Executor dispatchOfflineMessagesExecutor = null;
    
    boolean isIOSTest = false;
    int iosBuildDriver = -1;

    private Map<String, Map<String, String>> pendingLogins = new HashMap<String, Map<String,String>>();
    
    private static final Logger LOGGER = Logger.getLogger(Login.class);
    
    public Login() throws Exception {
    	super();
    	try {
    		String tmp = Util.getProperty("ios_test_device", "config");
    		isIOSTest = Boolean.parseBoolean(tmp);
    		iosBuildDriver = getNumberFromProps("driver_ios_build", -1);
    	
    		String offlineMessagesProcessesStr = Util.getProperty("offlinemessages_processes", "processes");
    		
    		Integer offlineProcesses = null;
    		
    		if(offlineMessagesProcessesStr!=null) {
    			offlineProcesses = Integer.parseInt(offlineMessagesProcessesStr);
    		} else {
    			offlineProcesses = 4;
    		}
    		
    		dispatchOfflineMessagesExecutor = Executors.newFixedThreadPool(offlineProcesses, new NamingThreadFactory("offlinemessages_pool"));
    		/**
    		comm.getConn().addPacketListener(new PacketListener() {
    			
    			@Override
    			public void processPacket(Packet pack) {
    				LOGGER.debug("The iq is "+pack.toString());
    	    		processIqPacket((IQ) pack);
    	    	}
    		},new PacketFilter() {
				
				@Override
				public boolean accept(Packet packet) {
				
					LOGGER.debug("Will accept "+packet.toString());
					return packet.toString().contains("iq");
				}
			});
			*/
    		
    	} catch (Exception e) {
    		isIOSTest = false;
    	}
    }

    public enum ErrorCode {
    	anotherdrivertrip,
    	blocked,
    	wrongpass
    }
    
    public enum Action {

    	list,
    	login,
    	userLogin,
    	userLoginNew,
    	passLogin;

    	public static Action parseAction(String act) throws IllegalArgumentException {
    		try {
    			return valueOf(act);
    		} catch (Exception e) {
    			throw new IllegalArgumentException("Action not supported: " + act);
    		}
    	}
    	
    }

    private void prepareDriverLoginLogStmt() throws Exception {
    	if (driverLoginLogStmt == null || driverLoginLogStmt.isClosed()) {
    		driverLoginLogStmt = dataPool.prepareStatement(getQuery("driverLoginLog"));
    	}
    }

    private void prepareFindDriversPerDeviceStmt() throws Exception {
    	if (findDriversPerDeviceStmt == null || findDriversPerDeviceStmt.isClosed()) {
    		findDriversPerDeviceStmt = dataPool.prepareStatement(getQuery("findDriversPerDevice"));
    	}
    }

    private void prepareDriverLoginAttemptStmt() throws Exception {
    	if (driverLoginAttemptStmt == null || driverLoginAttemptStmt.isClosed()) {
    		driverLoginAttemptStmt = new NamedParameterStatement(dataPool, getQuery("loginAttempt"));
    	}
    }

    private void prepareUserLoginAttemptStmt() throws Exception {
    	if (userLoginAttemptStmt == null || userLoginAttemptStmt.isClosed()) {
    		userLoginAttemptStmt = new NamedParameterStatement(dataPool, getQuery("userLoginAttempt"));
    	}
    }

    private void prepareUserLoginAttemptStmtOld() throws Exception {
    	if (userLoginAttemptStmtOld == null || userLoginAttemptStmtOld.isClosed()) {
    		userLoginAttemptStmtOld = new NamedParameterStatement(dataPool, getQuery("userLoginAttemptOld"));
		}
    }

    private void preparePassLoginAttemptStmt() throws Exception {
    	if (passLoginAttemptStmt == null || passLoginAttemptStmt.isClosed()) {
    		passLoginAttemptStmt = new NamedParameterStatement(dataPool, getQuery("passLoginAttempt"));
    	}
    }
    
    private void preparePassLoginAttemptcStmt() throws Exception {
    	if(passLoginAttemptcStmt==null||passLoginAttemptcStmt.isClosed()) {
    		passLoginAttemptcStmt = new NamedParameterStatement(dataPool, getQuery("passLoginAttemptc"));
    	}
    }

    public void processRequest() {
    	String failure = null;
    	Action action = null;
    	
    	try {
    		action = Login.Action.parseAction(request.get("act"));
    		
    		switch (action) {
	    		case list:
	    			
	    			try {
	    				String imei = request.get("imei");
	    				String buildNumber = request.get("build");
	    				response = getDriversOnDevice(imei, buildNumber);
	    			} catch (Exception e) {
	    	    		e.printStackTrace();
	    	    		failure = e.getMessage();
	    	    	}
	    			
	    	    	response = String.format(getXML("LOGIN_ROOT"),
	    	    			(failure == null) ? response : String.format(getXML("ACTION_FAILURE"), 
	    	    			action !=null ? action.toString() : "",failure));
	    	    	
	    			//TODO comm.sendMessageToDriverImei(response, caller);
	    			
	    	    	//comm.sendMessageToDriver(response, caller);
	    	    	comm.sendClearImeiMessageToDriver(response, caller);
	    	    	
	    			response = null;
	    			return;
	    			
	    		case login:
	    		
	    			//TODO
	    			try {
	    				String codename = request.get("codename");
	    				String pin = request.get("pin");
	    				String locale = request.get("loc");
	    				response = driverLoginAttempt(codename, pin, locale);
	    			} catch (Exception e) {
	    	    		e.printStackTrace();
	    	    		failure = e.getMessage();
	    	    	}
	    			
	    	    	response = String.format(getXML("LOGIN_ROOT"), 
	    	    			(failure == null) ? response : String.format(getXML("ACTION_FAILURE"), 
	    	    			action !=null ? action.toString() : "",failure));
	    	    	
	    	    	comm.sendClearImeiMessageToDriver(response, caller);
	    			response = null;
	    			
	    			/**
	    			pendingLogins.put(from, request);
	    			
	    			//comm.sendMessageToDriverImei(createVersionIQ(), caller);
	    			comm.sendClearImeiMessageToDriver(createVersionIQ(), caller);
	    	    	
	    			response = null;
	    			*/
	    			return;
	    		case userLogin:
	    			response = userLoginAttemptOld(request.get("username"), request.get("password"));
	    			break;
	    		case userLoginNew:
	    			response = userLoginAttempt(request.get("username"), request.get("password"), request.get("compid"));
	    			break;
	    		case passLogin:
	    		
	    			String country = request.get("country");
	    			if (country == null) {
	    				throw new Exception("no_country");
	    			}
	    			response = passLoginAttempt(request.get("username"), request.get("password"), country,request.get("compid"));
	    			break;
	    		default:
	    			break;
    		}
    	} catch (Exception e) {
    		LOGGER.error("Problem sending message ",e);
    		
    		failure = e.getMessage();
    	}
    	
    	response = String.format(getXML("LOGIN_ROOT"), 
    			(failure == null) ? response : String.format(getXML("ACTION_FAILURE"), 
    			action !=null ? action.toString() : "",failure));
    	
    	if(failure!=null&&(action==Action.login||action==Action.list)) {
    		try {
				comm.sendClearImeiMessageToDriver(response, caller);			
				response = null;
    		} catch (XMPPException e) {
				LOGGER.error("Problem sending failure message ",e);
			}
    	}
    }

    private String userLoginAttemptOld(String username, String plainTextPassword) throws Exception {
    	
    	String output = "";
    	prepareUserLoginAttemptStmtOld();
    	userLoginAttemptStmtOld.setString("username", username);
    	String hashedPassword = Util.digest(plainTextPassword);
    	userLoginAttemptStmtOld.setString("password", hashedPassword);

    	ResultSet rs = userLoginAttemptStmtOld.executeQuery();
    	while (rs.next()) {
    		Long userid = rs.getLong("userid");
    		Long companyid = rs.getLong("companyid");
    		String companyName = rs.getString("companyname");
    		Long level = rs.getLong("level");
    		String surname = rs.getString("surname");
    		String name = rs.getString("name");
    		String phone = rs.getString("phone");
    		String status = rs.getString("status");
    		String locale = rs.getString("locale");
    		//ekos added after request of webbooking , i kept the same xml perhaps we should change it
    		//this cause the issue with the login, added a blank field in order to get the format
    		output = String.format(getXML("USER_AUTH_INFO"), userid,
    				companyid, companyName, level, surname, name,
    				phone, status, "",locale, "");
    	}
    	rs.close();
    	return output;
    }

    private String userLoginAttempt(String username, String plainTextPassword, String company) throws Exception {
    	String output = "";
    	prepareUserLoginAttemptStmt();
    	userLoginAttemptStmt.setString("username", username);
    	String hashedPassword = Util.digest(plainTextPassword);
    	userLoginAttemptStmt.setString("password", hashedPassword);
    	userLoginAttemptStmt.setString("comp", company);

    	ResultSet rs = userLoginAttemptStmt.executeQuery();
    	boolean glob = false;
    	boolean admin = false;
    	String name = "", surname = "", phone = "", status = "", locale = "";
    	Long userid = 0L;
    	String accAll = "";
    	String accComp = "";
    	int total = 0;
    	boolean tool = false;
    	while (rs.next()) {
    		total++;
    		if (!admin) {
    			admin = rs.getBoolean("admin");
    		}
    		if (!glob) {
    			userid = rs.getLong("userid");
    			name = rs.getString("name");
    			surname = rs.getString("surname");
    			phone = rs.getString("phone");
    			status = rs.getString("status");
    			locale = rs.getString("locale");
    			tool = rs.getBoolean("tooltipsenabled");
    			glob = true;
    		}

    		Long companyid = rs.getLong("companyid");
    		Long level = rs.getLong("level");
    		String companyName = rs.getString("companyname");
    		String companyType= rs.getString("companytype");
    		accAll +=
    				String.format(getXML("ADMIN_COMPANY"), companyid, companyName,companyType, level, rs.getString("companytimezone"), rs.getString("country")) +
    				"\n";
    		accComp += (companyid + ",");
    	}
    		if (total > 0) {
    			accComp = accComp.substring(0, accComp.length() - 1);
    			output = String.format(getXML("NEW_USER_AUTH_INFO"), userid, surname, name, phone, status, locale, admin ? "true" :
											       "false", tool ? "true" : "false",
											    		   String.format(getXML("ADMIN_COMPANIES_WRAPPER"), accComp, total, accAll));
    		}
    	rs.close();
    	return output;
    }

    private String passLoginAttempt(String username, String plainTextPassword, String country, String compid) throws Exception {
    	
    	String output = "";
    	
    	LOGGER.info("The username is "+username+" the pass is "+plainTextPassword+" the country is "+country+" and the compid is "+compid);
    	
    	NamedParameterStatement namedParameterStatement = null;
    	
    	if(compid!=null) {
    		preparePassLoginAttemptcStmt();
    		namedParameterStatement = passLoginAttemptcStmt;
    		namedParameterStatement.setString("compid", compid);
    	} else {
    		preparePassLoginAttemptStmt();
        	namedParameterStatement = passLoginAttemptStmt;	
    	}
    	
    	namedParameterStatement.setString("username", username);
    	String hashedPassword = Util.digest(plainTextPassword);
    	namedParameterStatement.setString("password", hashedPassword);
    	namedParameterStatement.setString("country", country);

    	Passenger passenger = null;
    	
    	ResultSet rs = namedParameterStatement.executeQuery();
    	if (rs.next()) {
    		
    		passenger = new Passenger();
    		
    		//p.passengerid, p.taxicompanyid, p.lastname, p.name, p.phone, p.status,p.imei
    		Long userid = rs.getLong("passengerid");
    		Long taxicompanyid = rs.getLong("taxicompanyid");
    		passenger.setTaxiCompanyId(taxicompanyid);
    		String surname = rs.getString("lastname");
    		String name = rs.getString("name");
    		String phone = rs.getString("phone");
    		String status = rs.getString("status");
    		//ekos added after request of webbooking
    		passenger.setMobile(rs.getString("mobile"));
    		String imei = rs.getString("imei");
    		Long id = rs.getLong("addressid");
    		String isdef = rs.getString("isDefault");
    		Double lat = rs.getDouble("lat");
    		Double lng = rs.getDouble("lng");
    		String desc = rs.getString("description");
    		String addr = rs.getString("fulladdress");

    		String passaddr = rs.getString("address");
    		String favouriteAddress =
	        String.format(getXML("FAV_ADDRESS"), id, addr == null ? "" : addr, desc == null ? "" : desc, lat, lng,
	                             isdef) + "\n";
    		String compmail=rs.getString("email");
    		String allAddress=String.format("<companyemail>%s</companyemail>\n<homeAddr><![CDATA[%s]]></homeAddr>\n%s",compmail, passaddr,favouriteAddress);
	    
    		output =
    				String.format(getXML("USER_AUTH_INFO"), userid, taxicompanyid, "", 0, surname, name, phone, status, imei, "",
	           allAddress);
    	}
	
    	rs.close();
	
    	//if(passenger!=null) sendLoginSMS(passenger,country,plainTextPassword);
    	
    	return output;
    }

    private void sendLoginSMS(Passenger passenger,String country,String password) {
    	
    	
    	
    	
    	SMSSUtil smssUtil = new SMSSUtil(passenger.getTaxiCompanyId());
    	smssUtil.setCountryCode(country);
    	String message = Util.getProperty("welcomeSMS_"+ (country != null ? country.toLowerCase() : Push.DEFAULT_PASSENGER_LOCALE), "lang")+password;
    	LOGGER.debug("SMD Will send sms from "+passenger.getTaxiCompanyId()+" to "+passenger.getMobile()+" "+message);
	    
    	LOGGER.info("The message is "+message);
    	
    	smssUtil.sendSmsAsync(passenger.getMobile(), message);
    }
    
    private String driverLoginAttempt(String codename, String pin, String locale) throws Exception {
    
    	prepareDriverLoginAttemptStmt();
    	driverLoginAttemptStmt.setString("codename", codename);
    	driverLoginAttemptStmt.setString("pin", pin);
    	ResultSet rs = driverLoginAttemptStmt.executeQuery();
    	Long deviceId = null;
    	Long driverId = null;
    	String driverName = null;
    	String taxiCompanyIds = "";
    	double nelat = 0.0;
    	double nelng = 0.0;
    	double swlat = 0.0;
    	double swlng = 0.0;
    	String logistics = "";
    	String status = null;
    	while (rs.next()) {
    		//d.deviceid, dr.driverid, dc.taxicompanyid, dr.codename, dr.pin
    		if (deviceId == null) {
    			deviceId = rs.getLong("deviceid");
    			driverId = rs.getLong("driverid");
    			driverName = rs.getString("firstname") + " " + rs.getString("lastname");
    		}
    		
    		taxiCompanyIds += (taxiCompanyIds.length() == 0 ? "" : ",") + rs.getLong("taxicompanyid");
    		Double tempNelat = rs.getDouble("nelat");
    		Double tempNelng = rs.getDouble("nelng");
    		Double tempSwlat = rs.getDouble("swlat");
    		Double tempSwlng = rs.getDouble("swlng");
    		status = rs.getString("status");
    		
    		if (rs.getString("merchantcurrency") != null) {
    			logistics += String.format(getXML("LOGISTICS_INFO"),
    					rs.getLong("taxicompanyid"), rs.getString("officialname"), rs.getString("shortname"),
    					rs.getString("merchantaccount"), rs.getString("merchantcurrency")) + "\n";
    		}
    		
    		if (tempNelat != null && (nelat == 0 || Math.abs(tempNelat) > Math.abs(nelat))) {
    			nelat = tempNelat;
    		}
    		
    		if (tempNelng != null && (nelng == 0 || Math.abs(tempNelng) > Math.abs(nelng))) {
    			nelng = tempNelng;
    		}
	    
    		if (tempSwlat != null && (swlat == 0 || Math.abs(tempSwlat) < Math.abs(swlat))) {
    			swlat = tempSwlat;
    		}
    		
    		if (tempSwlng != null && (swlng == 0 || Math.abs(tempSwlng) < Math.abs(swlng))) {
    			swlng = tempSwlng;
    		}
    	}
    	rs.close();
    	//set driver as active
    	if (driverId != null && deviceId != null) {
	  
    		Long onTripDriverId = DataHelper.deviceOnTripDriverId(deviceId);
    		LOGGER.info("Driverid on trip "+onTripDriverId);
    		if(onTripDriverId!=null&&onTripDriverId.compareTo(driverId)!=0) {
    			LOGGER.error("Could not login "+driverId+" another driver "+onTripDriverId);
    			throw new Exception(ErrorCode.anotherdrivertrip.toString());
    		}
			
    		if(status!=null&&status.equals("blo")) {
    			throw new Exception(ErrorCode.blocked.toString());
        	}
    		
    		prepareDriverLoginLogStmt();
    		driverLoginLogStmt.setLong(1, deviceId);
    		driverLoginLogStmt.setLong(2, driverId);
    		driverLoginLogStmt.setString(3, locale);
    		driverLoginLogStmt.execute();
    		final long tmp = driverId;
    	
    		dispatchOfflineMessagesExecutor.execute(new Runnable() {
    			
    			@Override
    			public void run() {
    				try {
    					Thread.sleep(1000);
    					Messages.getInstance().dispatchOfflineMessages(tmp);
    				} catch (Exception e) {
    					// TODO: Add catch code
    					e.printStackTrace();
    				}
    			}
    			
    		});
    		/**
    		new Thread(new Runnable() {
    			public void run() {
    				try {
    					Thread.sleep(1000);
    					Messages.getInstance().dispatchOfflineMessages(tmp);
    				} catch (Exception e) {
    					// TODO: Add catch code
    					e.printStackTrace();
    				}
    			}
    		}).start();
    		*/

    	}
    	
    	return String.format(getXML("DRIVER_AUTH_INFO"), driverId, deviceId, driverName, codename, taxiCompanyIds, nelat,
			     nelng, swlat, swlng, logistics);
    }

    private String getDriversOnDevice(String imei, String build) throws Exception {
    	prepareFindDriversPerDeviceStmt();
    	String drivers = "";
    	int driverCount = 0;

    	findDriversPerDeviceStmt.setString(1, imei);
    	ResultSet rs = findDriversPerDeviceStmt.executeQuery();

    	while (rs.next()) {
    		driverCount++;
    		//platenumber, codename, firstname, lastname, gender, dr.status
    		String plateno = rs.getString("platenumber");
    		String codename = rs.getString("codename");
    		String firstname = rs.getString("firstname");
    		String lastname = rs.getString("lastname");
    		String status = rs.getString("status");
    		drivers += String.format(getXML("DRIVER_LOGIN_INFO"), plateno, codename, firstname, lastname, status) + '\n';
    	}
    
    	rs.close();
    	if (build != null && isIOSTest && driverCount == 0) {
    		int buildNumber = Integer.parseInt(build);
    		if (iosBuildDriver <= buildNumber) {
    			imei = "testIOSDevice";
    			findDriversPerDeviceStmt.setString(1, imei);
    			rs = findDriversPerDeviceStmt.executeQuery();
    			while (rs.next()) {
    				driverCount++;
    				//platenumber, codename, firstname, lastname, gender, dr.status
    				String plateno = rs.getString("platenumber");
    				String codename = rs.getString("codename");
    				String firstname = rs.getString("firstname");
    				String lastname = rs.getString("lastname");
    				String status = rs.getString("status");
    				drivers += String.format(getXML("DRIVER_LOGIN_INFO"), plateno, codename, firstname, lastname, status) + '\n';
    			}
    			rs.close();
    		}
    	}
    	String temp = "";
    	if (driverCount == 0) {
    		temp = RegisterDriver.getInstance().getRegistrationInfo(imei) + "\n";
    	}

    	return (temp + String.format(getXML("MULTIPLE_DRIVER_WRAPPER"), driverCount, drivers));
    }
    
    @Override
	public void processIqPacket(IQ iq) {
		
		LOGGER.info("Check stuff "+iq.getChildElementXML());
    
		Document document = getXMLDocument(iq.getChildElementXML());
		if(document.getNodeName().equals("query")) {
			Version version = documentToVersion(document);
			proceedWithLogin(iq.getFrom(),version);
		}
		
    }
    
    private IQ createVersionIQ() {
    	
    	IQ version = new IQ() {
			
			@Override
			public String getChildElementXML() {
				return "<query xmlns='jabber:iq:version'/>";
			}
		};
    	
		version.setType(Type.GET);
		
		return version;
    }
    
    private Document getXMLDocument(String xmlText) {
    	
    	try {
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
			Document document = dbBuilder.parse(new ByteArrayInputStream(xmlText.getBytes("UTF-8")));
		
			return document;
		
		} catch (ParserConfigurationException e) {
			LOGGER.error("Problem parsing xml",e);
		} catch (SAXException e) {
			LOGGER.error("Problem parsing xml",e);
		} catch (IOException e) {
			LOGGER.error("Problem parsing xml",e);
		}
    	
    
    	return null;
    }
   
    private Version documentToVersion(Document document) {
    	
    	Version version = new Version();
    	
    	NodeList nodeList = document.getChildNodes();
    	
    	for(int i=0;i<nodeList.getLength();i++) {
    		
    		Node node = nodeList.item(i);
    		if(node.getNodeName().equals("name")) {
    			version.setName(node.getNodeValue());
    		} else if(node.getNodeName().equals("version")) {
    			version.setVersion(node.getNodeValue());
    		} else if(node.getNodeName().equals("os")) {
    			version.setOs(node.getNodeValue());
    		}
    		
    	}
    	
    	return version;
    }
    
    private void proceedWithLogin(String to,Version version) {
    	
    	Map<String, String> pendingLogin = pendingLogins.get(to);
    	
    	if(pendingLogin!=null) {
    		
    		pendingLogins.remove(to);
    		
    		LOGGER.info("There was a pending loggin for the request");
    		
    		try {
				String codename = pendingLogin.get("codename");
				String pin = pendingLogin.get("pin");
				String locale = pendingLogin.get("loc");
				String loginResponse = driverLoginAttempt(codename, pin, locale);
			
				String imei = from.substring(from.indexOf(SLASH_CODE_POINT) + 1);
				
				if(imei!=null) {
					comm.sendClearImeiMessageToDriver(loginResponse, imei);
				}
				
    		} catch (Exception e) {
				LOGGER.error("There was an exception during the login faze");
			}
    	}
    	
    }
    
}
