package aploon.xmpp.ssclient;


import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.datahelper.DataHelper;
import aploon.model.BiddingTime;
import aploon.model.CallType;
import aploon.model.Coordinates;
import aploon.xmpp.Communicator;
import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.bots.AppointDispatcher;
import aploon.xmpp.ssclient.bots.FareDispatcher;


/*
 * Insert calls and appointemnets. Is for for everyone.
 */
public class UpdateCall extends ServerSideClient {

    public static final int DEFAULT_CANCELS_TO_BLACKLIST = 3;

    private PreparedStatement findCompanyTimesStmt;
    private PreparedStatement checkBlacklistStmt;
    private CallableStatement updateAppointStmt;
    private CallableStatement insertOrUpdateCallStmt;

    enum BookAgent {
    	s,d,w;

    	public static BookAgent parseAgent(String agentStr, Long userId) {
    		try {
    			return valueOf(agentStr);
    		} catch (Exception e) {
    			return userId != null ? d : s;
    		}
    	}
    }

    private static UpdateCall instance;

    public static synchronized UpdateCall getInstance() throws Exception {
    	if (instance == null) {
    		instance = new UpdateCall();
    	}
    	return instance;
    }

    private static final Logger LOGGER = Logger.getLogger(UpdateCall.class);
    
    private UpdateCall() throws Exception {
    	super();
    }

    private void prepareInsertOrUpdateCallStmt() throws Exception {
    	if (insertOrUpdateCallStmt == null || insertOrUpdateCallStmt.isClosed()) {
    		//insertOrUpdateCallStmt = dataPool.getConnection().prepareCall(getQuery("insertOrUpdateCall"));
    		insertOrUpdateCallStmt = dataPool.getConnection().prepareCall("CALL insertOrUpdateCall(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    	}
    }

    private void prepareUpdateAppointStmt() throws Exception {
    	if (updateAppointStmt == null || updateAppointStmt.isClosed()) {
    		updateAppointStmt = dataPool.getConnection().prepareCall(getQuery("insertOrUpdateAppoint"));
    	}
    }

    private void prepareFindCompanyTimesStmt() throws Exception {
    	if (findCompanyTimesStmt == null || findCompanyTimesStmt.isClosed()) {
    		findCompanyTimesStmt = dataPool.prepareStatement(getQuery("findCompanyTimes"));
    	}
    }

    private void prepareCheckBlacklistStmt() throws Exception {
    	if (checkBlacklistStmt == null || checkBlacklistStmt.isClosed()) {
    		checkBlacklistStmt = dataPool.prepareStatement(getQuery("checkBlacklist"));
    	}
    }

    public void processRequest() {
    	String failure = null;
    	String driversIds = null;
    	Long appointmentDate = parseOptionalField("appdate", Long.class);
    	try {
    		//tripid when it's a trip, appointid, when its an appoimentment
    		Long id = parseOptionalField("id", Long.class); //must be null on insert, must be provided on update
    		Long passId = null;
    		//optional on update, required on insert
    		try {
    			String passid = request.get("uid"); //call from a smartphone client
    			if (passid == null) {
    				passid = request.get("passid"); //legacy call from the WEB client
    			}
    			passid = passid.replaceAll(Communicator.PASS_UNAME_PREFIX, "");
    			passId = Long.parseLong(passid);
    		} catch (Exception e) {
    			e.printStackTrace();
    			throw new RuntimeException("passid_missing");
    		}
    		Long userId = parseOptionalField("userid", Long.class);
    		driversIds = request.get("driverids");
    		//default value for bookagent (s, unless userid is specified, then d)
    		BookAgent bookAgent = BookAgent.parseAgent(request.get("agent"), userId);
    		//multiplier for range growth
    		Double multi = parseOptionalField("rangemulti", Double.class);
    		//on insert, if the passenger is blacklisted, return an angry message and don't proceed with the call
    		//if the passenger  is from the dispatcher ignore the blacklist flag (ekos 29/3)
	    
            if (id == null && isPassengerBlacklisted(passId) && bookAgent != BookAgent.d) {
            	throw new RuntimeException("pass_blacklisted");
            }
	    
            Long taxicompanyId = parseOptionalField("compid", Long.class);
            String countryCode = request.get("country");
            //determine missing parts (coordinates vs address) from provided ones
            Coordinates coord = null;
            //get address (if provided, which would be the case of a dispatcher invocation)
            String address = request.get("addr");
            //get new coordinates
            String lat = request.get("lat");
            String lng = request.get("lng");
	    
            Long callTypeId = parseOptionalField("calltype", Long.class);
           
            LOGGER.info("Calltype received "+callTypeId);
            
            try {
            	if(callTypeId==null) {
            		if(taxicompanyId==null) {
            			taxicompanyId = getDefaultTaxiCompany(countryCode);
            		}
            		callTypeId = DataHelper.getTaxiCompanyCallTypeId(taxicompanyId);
            		LOGGER.info("Calltype from taxi company "+callTypeId);
                    
            	}
            } catch(SQLException e) {
            	LOGGER.info("Could not retrieve taxi company and calltype ",e);
            }
            
            //construct a Coordinates object holding coordinates and the address from given data
            try {
            	String latlng = request.get("latlng");
            	if (latlng != null) {
            		String[] latlngParts = latlng.split(",");
            		try {
            			lat = latlngParts[0].trim();
            			lng = latlngParts[1].trim();
            		} catch (Exception e) {
            			e.printStackTrace();
            			failure = e.getMessage();
            		}
            	}
            	
            	if (lat == null || lng == null) {
            		if (address != null) {
            			//coordinates failed
            			try {
            				coord = locator.getCoordinatesByAddress(address); //, PRIMARY_LOCATOR_REGION);
            			} catch (Exception ex) {
            				//location is required on insert
            				if (id == null) {
            					ex.printStackTrace();
            					throw ex;
            				}
            			}
            		}
            	} else {
            		try {
            			coord = new Coordinates(new BigDecimal(lat), new BigDecimal(lng));
            			coord.setNormalizedAddress(address);
            			//ekos 25/2/13 after major crash of terra reverse geocoding 
            			//this one became obsolete
                       // if (!(bookAgent==BookAgent.d)){
                       //     locator.getAddressByCoordinates(coord); //, false);
                       // }
            		} catch (Exception e) {
            			e.printStackTrace();
            			failure = e.getMessage();
            			coord.setNormalizedAddress(address);
            		}
            	}
            } catch (Exception e) {
            	e.printStackTrace();
            	throw new RuntimeException("pickup_location_missing_error:" + e.getMessage());
            }	
	    
            String droplatlng;
            Coordinates dropCoord = null;
            try {
            	droplatlng = request.get("droplatlng");
            	String[] droplatlngParts = droplatlng.split(",");
            	dropCoord = new Coordinates(new BigDecimal(droplatlngParts[0].trim()), new BigDecimal(droplatlngParts[1].trim()));
            } catch (Exception e) {
            }
	    
            //optional fields
            //both passenger app and dispatcher
            String special = request.get("special");
            String contactPhone = request.get("phone");
            String destination = request.get("dest");
            Long driverId = parseOptionalField("driverid", Long.class);
            Integer zoneId = parseOptionalField("zoneid", Integer.class);
            //dispatcher specific
            Double price = parseOptionalField("price", Double.class);
            String paymentType = request.get("paytype");
            Integer attempts = parseOptionalField("attempts", Integer.class);
            
            Long type = parseOptionalField("type", Long.class);
            
            if(type!=null) System.out.println("The type is "+type.toString());
            else System.out.println("There is no type");
            
            //update the appointment
            try {

            	if (appointmentDate == null) {
            		//in case of an update, the method returns a null value
            		response = updateCallInDb(id, coord, passId, userId, taxicompanyId, countryCode, driverId,
            				zoneId, address, special,contactPhone, bookAgent, destination, price, paymentType,
            				dropCoord, multi, driversIds,type,attempts,callTypeId);
            	} else {
            		try {
            			//appointment specific fields
            			//ekos 20/3/12
            			if (driverId == null && taxicompanyId == null) {
            				throw new RuntimeException("driverid_missing");
            			}
			response =
		 updateAppointInDb(id, coord, passId, userId, taxicompanyId, countryCode, address, driverId, special, contactPhone,
				   bookAgent, destination, price, appointmentDate, paymentType,callTypeId,type);
		    } catch (Exception e) {
			e.printStackTrace();
			failure = e.getMessage();
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
		failure = e.getMessage();
	    }
	} catch (Exception e) {
	    failure = e.getMessage();
	}
	if (response == null || response.equals("")) {
	    if (appointmentDate == null) {
		response =
	  String.format(getXML("UPDATE_CALL_STATE_XML"), Boolean.toString(failure == null || failure.equals("")), failure);
	    } else {
		response =
	  String.format(getXML("UPDATE_APPOINT_STATE_XML"), Boolean.toString(failure == null || failure.equals("")), failure);
	    }
	}
    }

    private String updateCallInDb(Long tripId, Coordinates coord, Long passId, Long userId, Long taxicompanyId,
				  String countryCode, Long driverId, Integer zoneId, String address, String special, String contactPhone,
				  BookAgent bookAgent, String destination, Double price, String payType, Coordinates drop, Double multip,
				  String multidriver,Long type,Integer attempts,Long callTypeId) throws Exception {
    	
    	CallType callType = null;
    	
    	/**
    	 * Will retrieve the call type
    	 */
    	try {
    		if(callTypeId!=null) {
    			callType = DataHelper.getCallType(callTypeId);
    		}
    	} catch(SQLException|NullPointerException e) {
    		LOGGER.error("Could not get calltype ",e);
    	}
    	
    	//&&callType.getZoneOrder().compareTo(new Double(0.0))>0
    	if(zoneId!=null) {
    		
    		List<Long> driversInsideZone = DataHelper.getDriversFromZoneForCall(zoneId,type);
    		if(driversInsideZone.size()>0) {
    			if(callType.getCandidateNumber().compareTo(new Integer(0))>0) {
    				driverId = driversInsideZone.get(0);
    			}
    		}
    	}
    	
    	
    	/**
    	 * Will get the first driver only on special occasion
    	 */
    	/**
    	if(zoneId!=null) {
    		List<Long> driversInsideZone = DataHelper.getDriversFromZoneForCall(zoneId,type);
    		if(!(callType!=null&&callType.getZoneOrder().compareTo(new Double(0.0))>0&&callType.getCandidateNumber().compareTo(new Integer(0))>0)) {
    			driverId = driversInsideZone.get(0);
    		}
    	}
    	*/
    	
    	LOGGER.info("The zone is "+zoneId+" "+driverId);
    	
    	prepareInsertOrUpdateCallStmt();
	
    	//if(attempts==null) attempts = 0;
	
    	String response = null;
    	int i = 1;
    	insertOrUpdateCallStmt.setString(1, tripId != null ? tripId.toString() : null);
    	insertOrUpdateCallStmt.setString(2, passId != null ? passId.toString() : null);
    	insertOrUpdateCallStmt.setString(3, userId != null ? userId.toString() : null);
    	insertOrUpdateCallStmt.setString(4, taxicompanyId != null ? taxicompanyId.toString() : null);
    	insertOrUpdateCallStmt.setString(5, countryCode);
    	insertOrUpdateCallStmt.setString(6, coord != null && coord.getLat() != null ? Double.toString(coord.getLat()) : null);
    	insertOrUpdateCallStmt.setString(7, coord != null && coord.getLng() != null ? Double.toString(coord.getLng()) : null);
    	insertOrUpdateCallStmt.setString(8, address != null ? address : (coord != null ? coord.getNormalizedAddress() : null));
    	insertOrUpdateCallStmt.setString(9, special);
    	insertOrUpdateCallStmt.setString(10, contactPhone);
    	insertOrUpdateCallStmt.setString(11, bookAgent.toString());
    	insertOrUpdateCallStmt.setString(12, destination);
    	insertOrUpdateCallStmt.setString(13, price != null ? price.toString() : null);
    	insertOrUpdateCallStmt.setString(14, payType);
    	insertOrUpdateCallStmt.setString(15, zoneId == null ? null : zoneId.toString());
    	
    	LOGGER.info("The driverid is "+driverId);
    	
    	insertOrUpdateCallStmt.setString(16, driverId!=null?driverId.toString():null);
    	insertOrUpdateCallStmt.setString(17, drop != null && drop.getLat() != null ? Double.toString(drop.getLat()) : null);
    	insertOrUpdateCallStmt.setString(18, drop != null && drop.getLng() != null ? Double.toString(drop.getLng()) : null);
    	insertOrUpdateCallStmt.setInt(19, attempts!=null?attempts:1);
    	insertOrUpdateCallStmt.registerOutParameter(20, Types.NUMERIC);
    	insertOrUpdateCallStmt.registerOutParameter(21, Types.NUMERIC);
    	insertOrUpdateCallStmt.registerOutParameter(22, Types.VARCHAR);
    	insertOrUpdateCallStmt.registerOutParameter(23, Types.TIMESTAMP);
    	insertOrUpdateCallStmt.execute();
    	Long tripIdOut = insertOrUpdateCallStmt.getLong("tripIdOut");
    	Timestamp hailtimeOut = insertOrUpdateCallStmt.getTimestamp("hailtimeOut");
    	if (tripId != null) { //the call was updated
    		//if the call was updated, then dispatch the new location to the driver
    		Long deviceIdOut = insertOrUpdateCallStmt.getLong("deviceIdOut");
    		String imeiOut = insertOrUpdateCallStmt.getString("imeiOut");
    		Dispatcher.getInstance().dispatchUpdate(tripId, imeiOut);
    	} else if (hailtimeOut != null) { //if imei is null, it means that there's an active trip but the passenger managed to request another one
    		/*
			 * DON'T USE THIS VALUE, BECAUSE JVM AND DB TIMES ARE NOT SYNCHRONIZED. USE System.currentTimeMillis() INSTEAD.
			 * Long hailtimeOut = insertOrUpdateCallStmt.getTimestamp("hailtimeOut").getTime();
			 */
	  
    		//TODO 
    		/**
    		 * Adding calltypes to trip
    		 */
		
    		try {
    			if(callTypeId!=null) {
    				DataHelper.setTripCallType(tripIdOut, callTypeId);
    			
    				LOGGER.info("The calltype that was set is "+callTypeId);
    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
		
    		try {
    			if(type!=null) DataHelper.setTripDriverType(tripIdOut, type);
    		} catch(SQLException e) {
    			LOGGER.error("Problem setting type id ",e);
    		}	
		
    		response = String.format(getXML("PLACE_CALL_XML"), 
    				dispatchCall(tripIdOut, driverId, coord, passId, special, false,
    						System.currentTimeMillis(), multip, multidriver,type,callTypeId,zoneId));
    	}
    	dataPool.closeConnection();
    	return response;
    }

    private String updateAppointInDb(Long appointId, Coordinates coord, Long passId, Long userId, Long taxicompanyId,
				     String countryCode, String address, Long driverId, String special, String contactPhone, BookAgent bookAgent,
				     String destination, Double price, Long appointmentDate,
				     String payType,Long callTypeId,Long driverType) throws Exception {
	
    	prepareUpdateAppointStmt();
	
    	String response = null;
	
    	Long generatedAppointmentId = null;
	
    	updateAppointStmt.setString(1, appointId == null ? null : appointId.toString());
	
    	updateAppointStmt.setString(2, passId == null ? null : passId.toString());
    	updateAppointStmt.setString(3, userId == null ? null : userId.toString());
    	updateAppointStmt.setString(4, taxicompanyId == null ? null : taxicompanyId.toString());
    	updateAppointStmt.setString(5, countryCode);
    	updateAppointStmt.setString(6, coord == null || coord.getLat() == null ? null : coord.getLat().toString());
    	updateAppointStmt.setString(7, coord == null || coord.getLng() == null ? null : coord.getLng().toString());
    	//store the address provided by the passenger/dispatcher, unless none is provided, then store the google address
    	updateAppointStmt.setString(8,address != null ? address : (coord != null ? coord.getNormalizedAddress() : null));
    	updateAppointStmt.setString(9, destination);
    	updateAppointStmt.setTimestamp(10, appointmentDate == null ? null : new Timestamp(appointmentDate));
    	
    	LOGGER.info("The driverid is "+driverId);
    	
    	updateAppointStmt.setString(11, driverId == null ? null : driverId.toString());
    	updateAppointStmt.setString(12, special);
    	updateAppointStmt.setString(13, contactPhone);
    	updateAppointStmt.setString(14, bookAgent.toString());
    	updateAppointStmt.setString(15, price == null ? null : price.toString());
    	updateAppointStmt.setString(16, payType);

    	updateAppointStmt.registerOutParameter(17, Types.NUMERIC);
    	updateAppointStmt.registerOutParameter(18, Types.TIMESTAMP);
    	updateAppointStmt.registerOutParameter(19, Types.VARCHAR);
    	updateAppointStmt.execute();
    	Long appointIdOut = updateAppointStmt.getLong("appointidOut");
	
    	if (appointId != null) { //the appointment was updated
    		//if the appointment was updated, there's no information to fetch from the DB
    	} else { //appointment was inserted
    		Long regtimeOut = updateAppointStmt.getTimestamp("regOrReqtimeOut").getTime();
    		
    		/**
    		 * Adding calltype to appointments
    		 */
    		try {
    			
    			LOGGER.info("THe calltype to set on appointment is "+callTypeId);
    			
				if(callTypeId!=null) {
					DataHelper.setAppointmentCallType(appointIdOut, callTypeId);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		
    		try {
    			if(driverType!=null) DataHelper.setAppointmentDriverType(appointIdOut, driverType);
    		} catch(SQLException e) {
    			LOGGER.error("Problem setting type id",e);
    		}
    		
    		response =
    				String.format(getXML("PLACE_APPOINT_SUCCESS"), coord.getNormalizedAddress(), coord.getLat(), coord.getLng(),appointIdOut, regtimeOut);
    	}
    	if (passId == 0) { //the appointment was cancelled by the passenger
    		String imei = updateAppointStmt.getString("imeiOut");
    		if (imei !=
    				null) { //imei still can be null if the appointment was declined by the driver BEFORE it was cancelled
    			Long reqtimeOut = updateAppointStmt.getTimestamp("regOrReqtimeOut").getTime();
    			try {
    				AppointDispatcher.getInstance().sendAppointInformToDriver(imei, appointId, reqtimeOut, "can", "",
    						"");
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	dataPool.closeConnection();
    	return response;
    }

    public boolean isPassengerBlacklisted(long passid) throws Exception {
    	prepareCheckBlacklistStmt();
    	checkBlacklistStmt.setLong(1, passid);
    	long cancels = 0;
    	ResultSet rs = checkBlacklistStmt.executeQuery();
    	try {
    		if (rs.next()) { //single row result
    			cancels = rs.getLong("cancels");
    		} else {
    			return false;
	    	//throw new RuntimeException("no_match");
    		}
	} finally {
	    rs.close();
	    dataPool.closeConnection();
	}
	return cancels >= getNumberFromProps("cancels_to_blacklist", DEFAULT_CANCELS_TO_BLACKLIST);
    }

    public String dispatchCall(long tripId, Long driverId, Coordinates coord, Long passId, 
    		String special,Boolean redispatch, long hailTime, Double multi, String Drivers, 
    		Long type,Long callTypeId,Integer zoneId) throws Exception {
    	
    	Integer biddingTime = null;
    	Integer waitTime = null;
    	Integer candidateNumber = null;
    	
    	CallType callType = null;
    	
    	if(callTypeId!=null) {
    		try {
				callType = DataHelper.getCallType(callTypeId);
				
    		} catch (SQLException e) {
				LOGGER.error("Problem getting calltype ",e);
			}
    	} 
    	
    	if(callType!=null) {
    		biddingTime = callType.getClearBiddingTime();
			waitTime = callType.getRequestWaitTime();
			candidateNumber = callType.getCandidateNumber();
			
			if(callType.getTimeToPickUp()!=null&&callType.getTimeToPickUp().compareTo(0)!=0) {
				DataHelper.setTimeToPickUp(tripId, callType.getTimeToPickUp());
			}
    	} else {
    		prepareFindCompanyTimesStmt();
        	findCompanyTimesStmt.setLong(1, tripId);
        	ResultSet timesRS = findCompanyTimesStmt.executeQuery();
        	timesRS.next(); //single row result
        	biddingTime = timesRS.getInt("clearbiddingtime");
        	waitTime = timesRS.getInt("requestwaittime");
        	candidateNumber = timesRS.getInt("candidateNumber"); //limit of devices the trip SHOULD BE dispatched to
        	timesRS.close();
        		
    	}
    	
    	//inform the drivers
        
        System.out.println("Dispatching the call and giving the type");
        
        if(type!=null) System.out.println("Type on "+type.toString());
        else  System.out.println("No type at allo");
        
        boolean dispatched = false;
        
        LOGGER.info("Actions considering a force assignment");
        
        if(callType!=null&&callType.getForceAssignment()) {
        	dispatched = true;
        	LOGGER.info("Direct forcing dispatch");
        } else {
        	LOGGER.info("An ok dispatch");
            dispatched = Dispatcher.getInstance().dispatchNewCall(
        			tripId, driverId, hailTime, candidateNumber, coord, 
        			passId, special,redispatch, multi,Drivers, type, callType); 
        	//getNumberFromProps("number_of_devices", DEFAULT_NUMBER_OF_DEVICES_TO_SEND_TRIP_TO)
        }
        
        //inform the passenger
        //add this call to the queue for voting after timeout
        BiddingTime placedCallInfo = new BiddingTime(biddingTime, dispatched ? waitTime : 5, dispatched ? hailTime : 0, dispatched);
        FareDispatcher.getInstance().addTrip(tripId, placedCallInfo);
        String message = String.format(getXML("PLACE_CALL_SUCCESS_XML"), 
        		coord.getNormalizedAddress(), tripId, biddingTime, waitTime,hailTime);
        return message;
        
    }
    
    public String dispatchWithCancelCall(long tripId, Coordinates coord, 
			        long hailTime) throws Exception {
    	prepareFindCompanyTimesStmt();
    	findCompanyTimesStmt.setLong(1, tripId);
    	ResultSet timesRS = findCompanyTimesStmt.executeQuery();
    	timesRS.next(); //single row result
    	Integer biddingTime = timesRS.getInt("clearbiddingtime");
    	Integer waitTime = timesRS.getInt("requestwaittime");
    	timesRS.close();
    	//inform the drivers
    	boolean dispatched =false;
    	
    	//inform the passenger
    	//add this call to the queue for voting after timeout
    	BiddingTime placedCallInfo =
    			new BiddingTime(biddingTime, 5, 0L, false);
    	FareDispatcher.getInstance().addTrip(tripId, placedCallInfo);
    	String message =
    			String.format(getXML("PLACE_CALL_SUCCESS_XML"), coord.getNormalizedAddress(), tripId, biddingTime, waitTime,hailTime);
    	return message;
    }


    public String dispatchCancelCall(long tripId, Coordinates coord, long hailTime) throws Exception {
    	prepareFindCompanyTimesStmt();
    	findCompanyTimesStmt.setLong(1, tripId);
    	ResultSet timesRS = findCompanyTimesStmt.executeQuery();
    	timesRS.next(); //single row result
    	Integer biddingTime = timesRS.getInt("clearbiddingtime");
    	Integer waitTime = timesRS.getInt("requestwaittime");
    	timesRS.close();

    	//add this call to the queue for voting after timeout
    	BiddingTime placedCallInfo = new BiddingTime(biddingTime, 5, 0L, false);
    	FareDispatcher.getInstance().addTrip(tripId, placedCallInfo);
    	String message =
    			String.format(getXML("PLACE_CALL_SUCCESS_XML"), coord.getNormalizedAddress(), tripId, biddingTime, waitTime,
    					hailTime);
    	return message;
    }

    protected String getPassword() {
    	return getClass().getSimpleName();
    }

    private Long getDefaultTaxiCompany(String countryCode) {
    	
    	
    	if(countryCode!=null) {
    		try {
				Long taxiCompanyId = DataHelper.getDefaultCompany(countryCode);
				if(taxiCompanyId!=null) {
					return taxiCompanyId;
				}
    		} catch (SQLException e) {
				LOGGER.error("problem fetching taxicompany ");
			}
    	}
    	
    	return 1L;
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
    
}
