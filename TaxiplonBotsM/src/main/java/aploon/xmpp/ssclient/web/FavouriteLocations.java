package aploon.xmpp.ssclient.web;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.jivesoftware.smack.packet.IQ;

/*
 * Only for passenger (webbooking)
 */
public class FavouriteLocations extends PagedList {
    private CallableStatement callFav;

    public FavouriteLocations() {
    	super();
    }

    enum Action {
    	edit,del,list,listdef,makedef,undef;
    }

    private void prepareCallFavStmt() throws Exception {
    	if (callFav == null || callFav.isClosed()) {
    		callFav = dataPool.getConnection().prepareCall(getQuery("callFavouriteLocation"));
    	}
    }

    @Override
    public void processRequest() {
    	try {
    		Long passid = parseOptionalField("passid", Long.class);
    		String action = request.get("act");
    		Long id = parseOptionalField("id", Long.class);
    		Double lat = parseOptionalField("lat", Double.class);
    		Double lng = parseOptionalField("lng", Double.class);
    		String desc = request.get("desc");
    		Boolean isDefault = parseOptionalField("isdefault", Boolean.class);
    		String address = request.get("addr");
    		if (Action.list.toString().equals(action)) {
    			response = listFavourites(passid, false, null);
    		} else if (Action.listdef.toString().equals(action)) {
    			response = listFavourites(passid, true, null);
    		} else if (Action.del.toString().equals(action)) {
    			response = delFavouriteLocation(id);
    		} else if (Action.makedef.toString().equals(action)) {
	    	response = makeDefault(id, "def");
    		} else if (Action.undef.toString().equals(action)) {
    			response = makeDefault(id, "undef");
    		} else if (Action.edit.toString().equals(action)) {
    			response = addUpdateAddress(id, passid, address, desc, lat, lng, isDefault);
    		} else {
    			throw new RuntimeException("Unsupported action");
    		}

    	} catch (Exception e) {
    		response = String.format(getXML("FAV_ADDRESS_WRAPPER"), 0, String.format(getXML("OPERATION_FAILURE_XML"), e.getMessage()));
    	}

    }

    public String listFavourites(Long passID, boolean getDef, Long addrId) throws Exception {
    	if (passID == null) {
    		throw new RuntimeException("Passenger id missing");
    	}
    	PagedResult paged = getPagedResult(getQuery("filteredFavLocationsSelect"), getQuery("filteredFavLocationsFrom"), null, getQuery("filteredFavLocationsFilter").replaceAll(":pass",
																					passID + "") + (getDef ? " AND isDefault='true'" : "") +
				  (addrId == null ? "" : (" AND addressid=" + addrId)), null, new PagedResult() {
		@Override
		protected String processResult(ResultSet rs) throws SQLException {
		    String result = "";
		    while (rs.next()) {
			try {
			    Long id = rs.getLong("addressid");
			    String isdef = rs.getString("isDefault");
			    Double lat = rs.getDouble("lat");
			    Double lng = rs.getDouble("lng");
			    String desc = rs.getString("description");
			    String addr = rs.getString("fulladdress");

			    result +=
		  String.format(getXML("FAV_ADDRESS"), id, addr == null ? "" : addr, desc == null ? "" : desc, lat, lng, isdef) +
				     "\n";

			} catch (Exception e) {
			    e.printStackTrace();
			}
		    }
		    return result;
		}
	    });
	return String.format(getXML("FAV_ADDRESS_WRAPPER"), paged.getTotalCount(),
			     String.format(getXML("OPERATION_SUSSECCFUL_XML"), "") + "\n" +
		paged.getResult());

    }

    String delFavouriteLocation(Long idio) throws Exception {
	if (idio == null) {
	    throw new RuntimeException("Address id missing");
	}
	prepareCallFavStmt();
	int i = 1;
	callFav.setString(i++, "del");
	callFav.setLong(i++, idio);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.registerOutParameter(i++, Types.NUMERIC); //driver
	callFav.execute();
	Long adrid = callFav.getLong("addressOut");

	return String.format(getXML("FAV_ADDRESS_WRAPPER"), 1, String.format(getXML("OPERATION_SUSSECCFUL_XML"), ""));
    }

    String makeDefault(Long idio, String act) throws Exception {
	if (idio == null) {
	    throw new RuntimeException("Address id missing");
	}
	prepareCallFavStmt();
	int i = 1;
	callFav.setString(i++, act);
	callFav.setLong(i++, idio);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.setString(i++, null);
	callFav.registerOutParameter(i++, Types.NUMERIC); //driver
	callFav.execute();
	Long adrid = callFav.getLong("addressOut");
	return String.format(getXML("FAV_ADDRESS_WRAPPER"), 1, String.format(getXML("OPERATION_SUSSECCFUL_XML"), ""));

    }


    String addUpdateAddress(Long idio, Long pass, String fullAddr, String description, Double lat, Double lng,
			    boolean isDefault) throws Exception {


	if (pass == null && idio == null) {
	    throw new RuntimeException("Passenger and address id missing");
	} else if (pass == null) {
	    throw new RuntimeException("Passenger id missing");
	}

	prepareCallFavStmt();
	int i = 1;
	callFav.setString(i++, "other");
	callFav.setString(i++, idio == null ? null : idio + "");
	callFav.setString(i++, pass == null ? null : pass + "");
	callFav.setString(i++, fullAddr);
	callFav.setString(i++, description);
	callFav.setDouble(i++, lat);
	callFav.setDouble(i++, lng);
	callFav.setString(i++, isDefault ? "true" : "false");
	callFav.registerOutParameter(i++, Types.NUMERIC); //driver
	callFav.execute();
	Long adrid = callFav.getLong("addressOut");
	return listFavourites(pass, false, adrid);

    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

}
