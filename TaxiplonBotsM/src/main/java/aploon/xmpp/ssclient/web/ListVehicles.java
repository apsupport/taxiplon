package aploon.xmpp.ssclient.web;


import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jivesoftware.smack.packet.IQ;

public class ListVehicles extends PagedList {

	public ListVehicles() throws Exception {
		super();
	}
	
	private NamedParameterStatement filteredVehicleListStmt;

	private void prepareFilteredVehicleListStmt() throws Exception {
		if(filteredVehicleListStmt == null || filteredVehicleListStmt.isClosed()) {
			filteredVehicleListStmt =
							 new NamedParameterStatement(dataPool, getQuery("filteredVehicleList"));
		}
	}

	public void processRequest() {
		try {
			Long vehicleId = parseOptionalField("id", Long.class);
			String plateno = request.get("plateno");
		    String model = request.get("model");
			response = getVehicleList(vehicleId, plateno, model);
		}
		catch(Exception e) {
			e.printStackTrace();
			response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
		}
	}

	private String getVehicleList(Long vehicleIdArg, String platenoArg, String modelArg) throws Exception {
	    PagedResult paged = getPagedResult(getQuery("filteredVehicleListSelect"), 
	                                       getQuery("filteredVehicleListFrom"), 
	                                       null, 
	                                       getQuery("filteredVehicleListFilter")
	                                       .replaceAll(":vehicleidArg", nonStringObjectToString(vehicleIdArg))
	                                       .replaceAll(":platenoArg", stringToSQLString(platenoArg, true))
	                                       .replaceAll(":modelArg", stringToSQLString(modelArg, true)), 
	                                       getQuery("filteredVehicleListOrder"),
										   new PagedResult() {
			@Override
			protected String processResult(ResultSet rs) throws SQLException {
				String result = "";
				while(rs.next()) {
					Long vehicleId = rs.getLong("vehicleid");
					String plateno = rs.getString("platenumber");
					String model = rs.getString("carmodel");
					//format the XML message
					result += String.format(getXML("VEHICLE_LIST_ITEM"), vehicleId, 
												   plateno != null? plateno : "", 
												   model != null? model : "") + "\n";
				}
				return result;
			}
		});
		return String.format(getXML("MULTIPLE_VEHICLE_WRAPPER"), paged.getTotalCount(), paged.getResult());
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
