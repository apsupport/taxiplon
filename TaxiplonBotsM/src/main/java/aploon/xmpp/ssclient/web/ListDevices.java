package aploon.xmpp.ssclient.web;


import aploon.xmpp.ssclient.web.PagedList.PagedResult;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jivesoftware.smack.packet.IQ;

public class ListDevices extends PagedList {
	
	private NamedParameterStatement filteredDeviceListStmt;

	public ListDevices() throws Exception {
		super();
	}

	private void prepareFilteredDeviceListStmt() throws Exception {
		if(filteredDeviceListStmt == null || filteredDeviceListStmt.isClosed()) {
			filteredDeviceListStmt =
							 new NamedParameterStatement(dataPool, getQuery("filteredDeviceList"));
		}
	}

	public void processRequest() {
		try {
			Long deviceId = parseOptionalField("id", Long.class);
			String imei = request.get("imei");
			String plateno = request.get("plateno");
			response = getDeviceList(deviceId, imei, plateno);
		}
		catch(Exception e) {
			e.printStackTrace();
			response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
		}
	}

	private String getDeviceList(Long deviceIdArg, String imeiArg, String platenoArg) throws Exception {
		PagedResult paged = getPagedResult(getQuery("filteredDeviceListSelect"), 
										   getQuery("filteredDeviceListFrom"), 
										   null, 
										   getQuery("filteredDeviceListFilter")
										   .replaceAll(":deviceidArg", nonStringObjectToString(deviceIdArg))
										   .replaceAll(":imeiArg", stringToSQLString(imeiArg, true))
										   .replaceAll(":platenoArg", stringToSQLString(platenoArg, true)), 
										   getQuery("filteredDeviceListOrder"),
										   new PagedResult() {
			@Override
			protected String processResult(ResultSet rs) throws SQLException {
				String result = "";
				while(rs.next()) {
					Long deviceId = rs.getLong("deviceid");
					Long simId = rs.getLong("simid");
					Long vehicleId = rs.getLong("vehicleid");
					Float currentlat = rs.getFloat("currentlat");
					Float currentlng = rs.getFloat("currentlong");
					String status = rs.getString("devicestatus");
					String imei = rs.getString("imei");
					String shortname = rs.getString("shortname");
					String version = rs.getString("softwareversion");
					String sos = rs.getString("sos");
					String simCarrier = rs.getString("carrier");
					String simMsisdn = rs.getString("msisdn");
					String simIccid = rs.getString("iccid");
					String simIp = rs.getString("ip");
					String vehPlateno = rs.getString("plateno");
					String vehModel = rs.getString("model");
					//format the XML message
					result += String.format(getXML("DEVICE_LIST_ITEM"), 
											   deviceId, simId, vehicleId, currentlat, currentlng, 
											   status != null? status : "",
											   imei != null? imei : "", 
											   shortname != null? shortname : "", 
											   version != null? version : "",
											   sos,
											   simCarrier != null? simCarrier : "",
											   simMsisdn != null? simMsisdn : "",
											   simIccid != null? simIccid : "",
											   simIp != null? simIp : "",
											   vehPlateno != null? vehPlateno : "",
											   vehModel != null? vehModel : ""
											   ) + "\n";
				}
				return result;
			}
		});
	    /*
		prepareFilteredDeviceListStmt();
	    filteredDeviceListStmt.setString("deviceidArg", deviceIdArg == null ? null : deviceIdArg.toString());
	    filteredDeviceListStmt.setString("imeiArg", imeiArg != null && !imeiArg.equals("")? imeiArg.replaceAll("\\*", "%") : null);
	    filteredDeviceListStmt.setString("platenoArg", platenoArg != null && !platenoArg.equals("")? platenoArg.replaceAll("\\*", "%") : null);
		filteredDeviceListStmt.setLong("first", first);
		filteredDeviceListStmt.setLong("last", last);
		ResultSet rs = filteredDeviceListStmt.executeQuery();
		 */
		return String.format(getXML("MULTIPLE_DEVICE_WRAPPER"), paged.getTotalCount(), paged.getResult());
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
