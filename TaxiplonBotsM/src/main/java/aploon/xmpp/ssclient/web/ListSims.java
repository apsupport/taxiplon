package aploon.xmpp.ssclient.web;


import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;

import org.jivesoftware.smack.packet.IQ;

public class ListSims extends PagedList {
	
	public ListSims() {
		super();
	}
	
	private NamedParameterStatement filteredSimListStmt;

	private void prepareFilteredSimListStmt() throws Exception {
		if(filteredSimListStmt == null || filteredSimListStmt.isClosed()) {
			filteredSimListStmt = new NamedParameterStatement(dataPool, getQuery("filteredSimList"));
		}
	}

	public void processRequest() {
		try {
			Long simId = parseOptionalField("id", Long.class);
			String carrier = request.get("carrier");
			String msisdn = request.get("msisdn");
			String iccid = request.get("iccid");
		    String ip = request.get("ip");
			Long first = parseOptionalField("first", Long.class);
			if(first == null) {
				first = DEFAULT_FIRST;
			}
		    Long last = parseOptionalField("last", Long.class);
			if(last == null) {
				last = DEFAULT_LAST;
			}
			response = String.format(getXML("MULTIPLE_SIM_WRAPPER"), 
									 getSimList(simId, carrier, msisdn, iccid, ip, first, last));
		}
		catch(Exception e) {
			e.printStackTrace();
			response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
		}
	}

	private String getSimList(Long simIdArg, String carrierArg, String msisdArg, String iccidArg, String ipArg, Long first, Long last) throws Exception {
		prepareFilteredSimListStmt();
		String outputXML = "";
		filteredSimListStmt.setString("simidArg", simIdArg == null ? null : simIdArg.toString());
		filteredSimListStmt.setString("carrierArg", carrierArg != null && !carrierArg.equals("")? carrierArg.replaceAll("\\*", "%") : null);
		filteredSimListStmt.setString("msisdnArg", msisdArg != null && !msisdArg.equals("")? msisdArg.replaceAll("\\*", "%") : null);
		filteredSimListStmt.setString("iccidArg", iccidArg != null && !iccidArg.equals("")? iccidArg.replaceAll("\\*", "%") : null);
		filteredSimListStmt.setString("ipArg", ipArg != null && !ipArg.equals("")? ipArg.replaceAll("\\*", "%") : null);
		filteredSimListStmt.setLong("first", first);
		filteredSimListStmt.setLong("last", last);
		ResultSet rs = filteredSimListStmt.executeQuery();
		while(rs.next()) {
			Long simId = rs.getLong("simid");
		    String carrier = rs.getString("carrier");
		    String msisdn = rs.getString("msisdn");
		    String iccid = rs.getString("iccid");
		    String ip = rs.getString("ip");
			//format the XML message
			outputXML += String.format(getXML("SIM_LIST_ITEM"), simId, 
									   carrier != null? carrier : "", 
									   msisdn != null? msisdn : "",
									   iccid != null? iccid : "", 
									   ip != null? ip : ""
									   ) + "\n";
		}
		rs.close();
		dataPool.closeConnection();
		return outputXML;
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
