package aploon.xmpp.ssclient.web;


import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.bots.NewsDispatcher;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;
import java.sql.Timestamp;

import org.jivesoftware.smack.packet.IQ;

public class ListMessages extends ServerSideClient {

	private NamedParameterStatement filteredMessagesPerCompanyStmt;

	public ListMessages() throws Exception {
		super();
	}

	private void prepareFilteredMessagesPerCompanyStmt() throws Exception {
		if (filteredMessagesPerCompanyStmt == null || filteredMessagesPerCompanyStmt.isClosed()) {
			filteredMessagesPerCompanyStmt =
							 new NamedParameterStatement(dataPool, getQuery("filteredMessagesPerCompany"));
		}
	}

	public void processRequest() {
		try {
			String taxicompanyIDs = request.get("compid");
			//sanitize the ids provided
			String[] temp = taxicompanyIDs.split(",");
			taxicompanyIDs = "";
			for (String s : temp) {
				try {
					taxicompanyIDs += (taxicompanyIDs.length() == 0 ? "" : ",") + Integer.parseInt(s);
				} catch (Exception e) {
					//e.printStackTrace();
				}
			}
			Long driverId = parseOptionalField("driverid", Long.class);
			String status = request.get("status");
			String text = request.get("text");
			String news = request.get("news");
			Long msgTimeFrom = parseOptionalField("msgtimefrom", Long.class);
			Long msgTimeTo = parseOptionalField("msgtimeto", Long.class);
			response = String.format(getXML("MULTIPLE_MSGS_WRAPPER"), 
									//ekos 12/9/2012 this query was horribly wrong if oyu uncoment fix the query first
									// fetchMessages(taxicompanyIDs, driverId, status, text, msgTimeFrom, msgTimeTo) + 
									 (news != null && news.equals("off")? "" : "\n" + NewsDispatcher.getInstance().getFeedMessages()));
		} catch (Exception e) {
			e.printStackTrace();
			response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
		}
	}

	public String fetchMessages(String taxicompanyidsArg, Long driveridArg, String statusArg, String msgBodyArg,
								Long msgTimeFromArg, Long msgTimeToArg) throws Exception {
		String messages = "";
		filteredMessagesPerCompanyStmt =
					  new NamedParameterStatement(dataPool, String.format(getQuery("filteredMessagesPerCompany"),
																		  taxicompanyidsArg != null && taxicompanyidsArg.length() > 0 ? taxicompanyidsArg :
																		  "SELECT taxicompanyid FROM drivercompanies WHERE driverid = (SELECT driverid FROM driver WHERE deviceid = (SELECT deviceid FROM device WHERE imei = " +
																		  sanitize(caller) + "))"));
		//EXACT MATCH
		//        filteredMessagesPerCompanyStmt.setLong("taxicompanyidArg", taxicompanyidArg);
		filteredMessagesPerCompanyStmt.setString("messageidArg", null);
		filteredMessagesPerCompanyStmt.setString("driveridArg", driveridArg != null ? driveridArg.toString() : null);
		filteredMessagesPerCompanyStmt.setString("statusArg", statusArg);
		//LIKE
		filteredMessagesPerCompanyStmt.setString("msgBodyArg",
												 msgBodyArg != null && !msgBodyArg.equals("") ? msgBodyArg.replaceAll("\\*", "%") :
												 null);
		//RANGE
		if (msgTimeFromArg != null) {
			filteredMessagesPerCompanyStmt.setTimestamp("msgTimeFromArg", new Timestamp(msgTimeFromArg));
		} else {
			filteredMessagesPerCompanyStmt.setString("msgTimeFromArg", null);
		}
		if (msgTimeToArg != null) {
			filteredMessagesPerCompanyStmt.setTimestamp("msgTimeToArg", new Timestamp(msgTimeToArg));
		} else {
			filteredMessagesPerCompanyStmt.setString("msgTimeToArg", null);
		}
		ResultSet rs = filteredMessagesPerCompanyStmt.executeQuery();
		while (rs.next()) {
			Long msgId = rs.getLong("messageid");
			String msgBody = rs.getString("messagebody");
			Long msgDriverId = rs.getLong("recipientdriverid");
			Timestamp msgTime = rs.getTimestamp("messagetime");
			String status = rs.getString("status");
			messages +=
   String.format(getXML("MSG"), msgId, msgTime != null ? msgTime.getTime() : null, msgBody, msgDriverId, status) +
		"\n";
		}
		rs.close();
		dataPool.closeConnection();
		return messages;
	}

	protected String getPassword() {
		return getClass().getSimpleName();
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
