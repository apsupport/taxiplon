package aploon.xmpp.ssclient.web;

import aploon.xmpp.ServerSideClient;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import org.jivesoftware.smack.packet.IQ;

public class ListCalls extends ServerSideClient {
	
	private PreparedStatement callListByCompanyIdStmt;
	
	public ListCalls() throws Exception {
		super();
	}
	
	private void prepareCallListByCompanyIdStmt() throws Exception {
		if(callListByCompanyIdStmt == null || callListByCompanyIdStmt.isClosed()) {
		    callListByCompanyIdStmt = dataPool.prepareStatement(getQuery("callListByCompanyId"));
		}
	}

	public void processRequest() {
	    try {
	        Long taxicompanyID = Long.parseLong(request.get("compid"));
	        Long tripId = parseOptionalField("tripid", Long.class);
	        response = String.format(getXML("MULTIPLE_CALL_WRAPPER"), getCallList(taxicompanyID, tripId));
	    }
	    catch (Exception e) {
	        response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
	    }
	}
	
	private String getCallList(Long taxicompanyID, Long tripId) throws Exception {
		prepareCallListByCompanyIdStmt();
		String callListXML = "";
		callListByCompanyIdStmt.setLong(1, taxicompanyID);
		callListByCompanyIdStmt.setString(2, tripId != null? tripId.toString() : null);
		ResultSet callList = callListByCompanyIdStmt.executeQuery();
		while (callList.next()) {
			tripId = callList.getLong("tripid");
			Double pickuplat = callList.getDouble("pickuplat");
			Double pickuplng = callList.getDouble("pickuplong");
			String address = callList.getString("pickupaddress");
			Timestamp hailtime = callList.getTimestamp("hailtime");
			String special = callList.getString("specialneeds");
			String destination = callList.getString("destination");
			Double fare = callList.getDouble("fare");
		    String status = callList.getString("status");
		    Integer pokedTimes = callList.getInt("poked");
		    String passengerGender = callList.getString("passgender");
			String passengerName = callList.getString("passname");
			String passengerSurname = callList.getString("passsurname");
			String passengerMobile = callList.getString("passmobile");
			String passengerImei = callList.getString("passimei");
			String deviceLat = callList.getString("currentlat");
		    String deviceLng = callList.getString("currentlong");
		    String plateno = callList.getString("plateno");
		    String carmodel = callList.getString("model");
		    String codename = callList.getString("codename");
		    String driverGender = callList.getString("drivergender");
		    String driverSurname = callList.getString("driversurname");
		    String driverName = callList.getString("drivername");
		    String driverMobile = callList.getString("drivermobile");
		    //format the XML message
			callListXML += String.format(getXML("CALL_LIST_ITEM"), tripId, pickuplat, pickuplng, 
                                         address, hailtime.getTime(), special, destination, fare, status, pokedTimes, 
                                         passengerGender, passengerName, passengerSurname, passengerMobile, passengerImei, 
										 deviceLat, deviceLng, plateno, carmodel, codename, driverGender, driverName, driverSurname,
										 driverMobile != null? driverMobile : "")
                + "\n";
		}
		callList.close();
		dataPool.closeConnection();
		return callListXML;
	}

	protected String getPassword() {
	    return getClass().getSimpleName();
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
