package aploon.xmpp.ssclient.web;

/*
 * Only for passenger.
 */


import aploon.datahelper.DataHelper;
import aploon.model.TripHistoryModel;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;


public class TripHistory extends PagedList {

    NamedParameterStatement getPassTripHistoryStmt;
    NamedParameterStatement getDriverTripHistoryStmt;
    NamedParameterStatement rateTripStmt;
    NamedParameterStatement hideTripStmt;

    private static final Logger logger = Logger.getLogger(TripHistory.class);

    public TripHistory() {
	super();
    }

    private void prepareGetPassTripHistoryStmt() throws Exception {
	if (getPassTripHistoryStmt == null || getPassTripHistoryStmt.isClosed()) {
	    getPassTripHistoryStmt = new NamedParameterStatement(dataPool, getQuery("getPassTripHistory"));
	}
    }

    private void prepareGetDriverTripHistoryStmt() throws Exception {
	if (getDriverTripHistoryStmt == null || getDriverTripHistoryStmt.isClosed()) {
	    getDriverTripHistoryStmt = new NamedParameterStatement(dataPool, getQuery("getDriverTripHistory"));
	}
    }

    private void prepareRateTripStmt() throws Exception {
	if (rateTripStmt == null || rateTripStmt.isClosed()) {
	    rateTripStmt = new NamedParameterStatement(dataPool, getQuery("rateTrip"));
	}
    }

    private void prepareHideTripStmt() throws Exception {
	if (hideTripStmt == null || hideTripStmt.isClosed()) {
	    hideTripStmt = new NamedParameterStatement(dataPool, getQuery("hideTrip"));
	}
    }

    public void processRequest() {
	String imei = request.get("imei");
	String action = request.get("act");
	if (action == null) { //request for a list of trip history
	    try {
		Long passId = parseOptionalField("passid", Long.class);
		Long compId = parseOptionalField("compid", Long.class);
		if (passId == null && compId == null) {
		    throw new RuntimeException("miss_id_param");
		} else {
		    if (passId != null) {
			//						Integer firstRow = Integer.parseInt(request.get("first"));
			//						Integer lastRow = Integer.parseInt(request.get("last"));
			
                        logger.info("Getting the passenger trip history");
                        
                        response = getPassTripHistory(passId);
                    
                    } else {
			Long driverId = parseOptionalField("driverid", Long.class);
			String passName = request.get("passname");
			String passSurname = request.get("passsurname");
			Long hailtimeFrom = parseOptionalField("from", Long.class);
			Long hailtimeTo = parseOptionalField("to", Long.class);
			response =
		 getFilteredCompanyTripHistory(compId, driverId, passName, passSurname, hailtimeFrom, hailtimeTo);
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
		response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
	    }
	    response = String.format(getXML("TRIP_HISTORY"), response);
	} else if (action != null) {
	    Long tripId = 0L;
	    boolean status;
	    String failure = null;
	    try {
		tripId = Long.parseLong(request.get("tripid"));
		if (action.toString().equals("rate")) {
		    //rating request
		    Integer rating = 0;
		    rating = Integer.parseInt(request.get("rating"));
		    status = rateTrip(imei, tripId, rating);
		    response =
	  String.format(getXML("RATING_XML"), tripId, rating, Boolean.toString(status), failure == null ? "" : failure);
		} else if (action.toString().equals("hide")) {
		    status = hideTrip(imei, tripId);
		    response =
	  String.format(getXML("HIDE_TRIP"), tripId, Boolean.toString(status), failure == null ? "" : failure);
		}
	    } catch (Exception e) {
		e.printStackTrace();
		status = false;
		failure = e.getMessage();
	    }
	}
    }

    /**
     *
     * @param passId
     * @param imei should be provided for the smartphone passengers
     * @param mobile should be provided for the WEB passengers
     * @return
     * @throws Exception
     */
    public String getPassTripHistory(Long passengerId) throws Exception {
        
        
        logger.info("Will get the trip history for passid "+passengerId);
        
        List<TripHistoryModel> tripHistoryModels = DataHelper.getTripHistory(dataPool.getConnection(), passengerId);
        
        StringBuilder tripHistoryModelsXML = new StringBuilder();
        
        for(TripHistoryModel tripHistoryModel:tripHistoryModels) {
        
            String tripHistoryXml = String.format(getXML("PASS_TRIP_INFO"), 
                                                  tripHistoryModel.getTripid(),
                                                  tripHistoryModel.getHailtime().getTime(),
                                                  tripHistoryModel.getPickuptime()!=null? tripHistoryModel.getPickuptime().getTime() : 0,
                                                  tripHistoryModel.getFrom()!=null? tripHistoryModel.getFrom() : "",
                                                  tripHistoryModel.getTo()!= null ? tripHistoryModel.getTo() : "",
                                                  tripHistoryModel.getSpecial()!= null ? tripHistoryModel.getSpecial() : "",
                                                  tripHistoryModel.getBookAgent()!= null ? tripHistoryModel.getBookAgent() : "",
                                                  tripHistoryModel.getStatus() != null ? tripHistoryModel.getStatus() : "",
                                                  tripHistoryModel.getRating() ,
                                                  tripHistoryModel.getPlateno()!=null? tripHistoryModel.getPlateno():"",
                                                  tripHistoryModel.getCarmodel()!=null? tripHistoryModel.getCarmodel(): "",
                                                  tripHistoryModel.getDrivername()!=null? tripHistoryModel.getDrivername() : "",
                                                  tripHistoryModel.getDriversurname()!=null? tripHistoryModel.getDriversurname() : "",
                                                  tripHistoryModel.getDrivercodename() != null ? tripHistoryModel.getDrivercodename() : "",
                                                  tripHistoryModel.getDriverId(),
                                                  Boolean.toString(tripHistoryModel.getIsFavorite() != null),
                                                  Boolean.toString(tripHistoryModel.getIsblocked() !=null),
                                                  tripHistoryModel.getAmount()== null ? 0.0 : tripHistoryModel.getAmount(),
                                                  tripHistoryModel.getCurr() == null ? "" : tripHistoryModel.getCurr(),
                                                  tripHistoryModel.getChan() == null ? "" : tripHistoryModel.getChan() ,
                                                  tripHistoryModel.getEmail() == null ? "" : tripHistoryModel.getEmail() ,
                                                  tripHistoryModel.getFacebook() ==null?"":tripHistoryModel.getFacebook() ,
                                                  tripHistoryModel.getTwitter() ==null?"": tripHistoryModel.getTwitter());
                
            tripHistoryModelsXML.append(tripHistoryXml);
        }
        
	return String.format(getXML("TRIP_WRAPPER"), tripHistoryModels.size(), tripHistoryModelsXML.toString());
                
    }

    public String getFilteredCompanyTripHistory(Long compIdArg, Long driverIdArg, String passNameArg,
						String passSurnameArg, Long hailtimeFromArg,
						Long hailtimeToArg) throws Exception {
	String filterCriteria =
		   String.format(getQuery("getDriverTripHistoryFilter"), compIdArg, driverIdArg != null ? driverIdArg.toString() :
										    null, (passNameArg != null ? stringToSQLString(passNameArg.replaceAll("\\*", "%")) : null),
				 (passSurnameArg != null ? stringToSQLString(passSurnameArg.replaceAll("\\*", "%")) : null),
				 timestampToSQLString(hailtimeFromArg), timestampToSQLString(hailtimeToArg));
	PagedResult paged =
		   getPagedResult(getQuery("getDriverTripHistorySelect"), getQuery("getDriverTripHistoryFrom"), getQuery("getDriverTripHistoryJoin"),
				  filterCriteria, getQuery("getDriverTripHistoryOrder"), new PagedResult() {
		@Override
		protected String processResult(ResultSet rs) throws SQLException {
		    String result = "";
		    while (rs.next()) {
			/*
					 * The query returns
					 * t.tripid, t.hailtime, t.assigntime, t.pickuptime, t.pickupaddress, t.destination, t.specialneeds, t.status as tripstatus,
					 * dr.firstname as drivername, dr.lastname as driversurname, dr.codename as drivercodename,
					 * p.name as passname, p.lastname as passsurname, p.email as passemail, p.mobile as passmobile, t.rating, f.passengerid as isfaved
					 */
			Long tripid = rs.getLong("tripid");
			Timestamp hailtime = rs.getTimestamp("hailtime");
			Timestamp assigntime = rs.getTimestamp("assigntime");
			Timestamp pickuptime = rs.getTimestamp("pickuptime");
			String from = rs.getString("pickupaddress");
			String to = rs.getString("destination");
			String special = rs.getString("specialneeds");
			String bookAgent = rs.getString("bookagent");
			String tripStatus = rs.getString("tripstatus");
			//vehicle/driver data
			String plateno = rs.getString("platenumber");
			String carModel = rs.getString("carmodel");
			Long driverId = rs.getLong("driverid");
			String driverName = rs.getString("drivername");
			String driverSurname = rs.getString("driversurname");
			String driverCodename = rs.getString("drivercodename");
			//passenger data
			String passName = rs.getString("passname");
			String passSurname = rs.getString("passsurname");
			String passEmail = rs.getString("passemail");
			String passMobile = rs.getString("passmobile");
			Integer rating = rs.getInt("rating");
			String isFavorite = rs.getString("isfaved");
		        String isblocked = rs.getString("isblocked");
		        
			/*
					 * The response XML contains the following fields
					 * <Trip>\n\
						<id>%d</id>\n\
						<hailtime>%d</hailtime>\n\
						<assigntime>%d</assigntime>\n\
						<pickuptime>%d</pickuptime>\n\
						<from>%s</from>\n\
						<to>%s</to>\n\
						<special>%s</special>\n\
						<agent>%s</agent>\n\
						<status>%s</status>\n\
						<driverid>%d</driverid>\n\
						<drivername>%s</drivername>\n\
						<driversurname>%s</driversurname>\n\
						<drivercodename>%s</drivercodename>\n\
						<plateno>%s</plateno>\n\
						<carmodel>%s</carmodel>\n\
						<passname>%s</passname>\n\
						<passsurname>%s</passsurname>\n\
						<passemail>%s</passemail>\n\
						<passmobile>%s</passmobile>\n\
						<rating>%d</rating>\n\
						<isfaved>%s</isfaved>\n\
					</Trip>
					 */
			result +=
		  String.format(getXML("DRIVER_TRIP_INFO"), tripid, hailtime.getTime(), assigntime != null ? assigntime.getTime() :
											0, pickuptime != null ? pickuptime.getTime() : 0, from, to != null ? to : "",
				special != null ? special : "", bookAgent != null ? bookAgent : "", tripStatus, plateno != null ? plateno : "",
				carModel != null ? carModel : "", driverId, driverName != null ? driverName : "",
				driverSurname != null ? driverSurname : "", driverCodename != null ? driverCodename : "",
				passName != null ? passName : "", passSurname != null ? passSurname : "", passEmail != null ? passEmail : "",
				passMobile != null ? passMobile : "", rating, Boolean.toString(isFavorite != null),Boolean.toString(isblocked!=null));
		    }
		    return result;
		}
	    });
	//		prepareGetDriverTripHistoryStmt();
	//		getDriverTripHistoryStmt.setLong("compid", compIdArg);//required
	//		getDriverTripHistoryStmt.setString("driverid", driverIdArg != null? driverIdArg.toString() : null);
	//		getDriverTripHistoryStmt.setString("passname", passNameArg != null? passNameArg.replaceAll("\\*", "%") : null);
	//		getDriverTripHistoryStmt.setString("passsurname", passSurnameArg != null? passSurnameArg.replaceAll("\\*", "%") : null);
	//	    getDriverTripHistoryStmt.setTimestamp("hailfrom", hailtimeFromArg != null ? new Timestamp(hailtimeFromArg) : null);
	//	    getDriverTripHistoryStmt.setTimestamp("hailto", hailtimeToArg != null ? new Timestamp(hailtimeToArg) : null);
	//		ResultSet rs = getDriverTripHistoryStmt.executeQuery();
	return String.format(getXML("TRIP_WRAPPER"), paged.getTotalCount(), paged.getResult());
    }

    protected boolean rateTrip(String imei, long tripid, int rating) throws Exception {
	prepareRateTripStmt();
	rateTripStmt.setString("imei", imei);
	rateTripStmt.setLong("tripid", tripid);
	rateTripStmt.setInt("rating", rating);
	if (rateTripStmt.executeUpdate() == 1) {
	    return true;
	} else {
	    throw new RuntimeException("no_rows_affected");
	}
    }

    protected boolean hideTrip(String imei, long tripid) throws Exception {
	prepareHideTripStmt();
	hideTripStmt.setString("imei", imei);
	hideTripStmt.setLong("tripid", tripid);
	if (hideTripStmt.executeUpdate() == 1) {
	    return true;
	} else {
	    throw new RuntimeException("no_rows_affected");
	}
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

}
