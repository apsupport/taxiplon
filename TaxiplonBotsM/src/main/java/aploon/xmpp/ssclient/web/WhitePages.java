package aploon.xmpp.ssclient.web;


import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.datahelper.DataHelper;
import aploon.sms.SMSSUtil;
import aploon.xmpp.ssclient.iphone.Push;

import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;

/*
 * Passenger data. crud.
 */
public class WhitePages extends PagedList {

    private CallableStatement createPassengerStmt;
    private NamedParameterStatement filteredPassengerListStmt;
    private PreparedStatement passengerStatsStmt;

    private static final Logger LOGGER = Logger.getLogger(WhitePages.class);
    
    public WhitePages() throws Exception {
    	super();
    }

    private void prepareFilteredPassengerListStmt() throws Exception {
    	if (filteredPassengerListStmt == null || filteredPassengerListStmt.isClosed()) {
    		filteredPassengerListStmt = new NamedParameterStatement(dataPool, getQuery("filteredPassengerList"));
    	}
    }

    private void preparePassengerStatsStmt() throws Exception {
    	if (passengerStatsStmt == null || passengerStatsStmt.isClosed()) {
    		passengerStatsStmt = dataPool.prepareStatement(getQuery("passengerStats"));
    	}
    }

    private void prepareCreatePassengerStmt() throws Exception {
    	if (createPassengerStmt == null || createPassengerStmt.isClosed()) {
    		createPassengerStmt = dataPool.getConnection().prepareCall(getQuery("createPassenger"));
    	}
    }

    private enum Actions {
    	filterPass,createPass;
    }

    public enum PassStatus {
    	act,pen,bla,del,none;

    	public static PassStatus parsePassStatus(String s) {
    		try {
    			return valueOf(s);
    		} catch (Exception e) {
    			return none;
    		}
    	}
    }

    public void processRequest() {
    	Actions action = null;
    	try {
    		try {
    			action = Actions.valueOf(request.get("action"));
    		} catch (IllegalArgumentException e) {
    			throw new RuntimeException("action_not_supported");
    		}
	    		Long taxicompanyId = parseOptionalField("compid", Long.class);
	    		Long passengerId = parseOptionalField("passid", Long.class);
	    		String imei = request.get("imei");
	    		String gender = request.get("gender");
	    		String name = request.get("name");
	    		String surname = request.get("surname");
	    		String address = request.get("address");
	    		String area = request.get("area");
	    		String phone = request.get("phone");
	    		String mobile = request.get("mobile");
	    		String email = request.get("email");
	    		String nick = request.get("nick");
	    		String plainTextPin = request.get("pin");
	    		String status = request.get("status");
	    		String softwareversion = request.get("ver");
	    		Long cancelsFrom = parseOptionalField("cancelsfrom", Long.class);
	    		Long cancelsTo = parseOptionalField("cancelsto", Long.class);
	    		Long regdateFrom = parseOptionalField("regdatefrom", Long.class);
	    		Long regdateTo = parseOptionalField("regdateto", Long.class);
	    		Long first = parseOptionalField("first", Long.class);
	    		String country = request.get("country");
	    		
	    	if (first == null) {
	    		first = DEFAULT_FIRST;
	    	}
	    
	    	Long last = parseOptionalField("last", Long.class);
	    		
	    	if (last == null) {
	    		last = DEFAULT_LAST;
	    	}
	    	//register passenger
	    	switch (action) {
	    		case createPass:
	    			String failure = null;
	    			String comments = request.get("comments");
	    			String typeid = request.get("typeid");
	    			
	    			Long generatedPassengerId = null;
	    			try {
	    				generatedPassengerId =
	    						createPassenger(passengerId, taxicompanyId, imei, gender, name, surname, address, area, mobile, phone, email,
	    								nick, plainTextPin, status, softwareversion, country, comments,typeid);
	    				if (generatedPassengerId == 0) {
	    					LOGGER.info("There is a dublicate record");
	    					throw new RuntimeException("duplicate_record");
	    				}
	    			} catch (Exception e) {
	    				//ekos added after request to show a specific message on this error
	    				//TODO here we should implement a mechanism that connects the two accounts
	    				if (e instanceof SQLException) {
	    					
	    					LOGGER.error("SQLException ",e);
	    					
	    					if (((SQLException)e).getSQLState().startsWith("23")) {
	    						failure = "unique_constraint";
	    						if (e.getMessage().toLowerCase().contains("phone")) {
	    							failure += "_phone";
	    						} else if (e.getMessage().toLowerCase().contains("nick")) {
	    							failure += "_nickname";
	    						}
	    					} else {
	    						failure = e.getMessage();
	    					}

	    				} else {
	    					failure = e.getMessage();
	    				}
	    			}
	    			response =
	    					String.format(getXML("REGISTRATION"), (failure == null ? "true" : "false"), failure, generatedPassengerId != null ?
											       generatedPassengerId.toString() : null, System.currentTimeMillis(),
	    							"");
	    			break;
	    		case filterPass:
	    			Long active = 0L;
	    			Long pending = 0L;
	    			Long blacklisted = 0L;
	    			Long inactive = 0L;
	    			//see if it is called from web and make url encodes
	    			boolean isWeb = request.get("webclient") != null;

	    			//get the totals if the passengerid is not specified (list requested)
	    			if (passengerId == null) {
	    				preparePassengerStatsStmt();
	    				passengerStatsStmt.setString(1, taxicompanyId != null ? taxicompanyId.toString() : null);
	    				ResultSet rs = passengerStatsStmt.executeQuery();
	    				while (rs.next()) {
	    					PassStatus passStatus = PassStatus.parsePassStatus(rs.getString("status"));
	    					Long temp = rs.getLong("amount");
	    					switch (passStatus) {
	    					case none:
	    						inactive = temp;
	    						break;
	    					case act:
	    						active = temp;
	    						break;
	    					case bla:
	    						blacklisted = temp;
	    						break;
	    					case pen:
	    						pending = temp;
	    						break;
	    					default:
	    						break;
	    					}
	    				}
	    			}
	    			//format the response with the stats and list of passengers
	    			response =
	    					String.format(getXML("WHITE_PAGES_WRAPPER"), active + pending + blacklisted + inactive, active, pending, blacklisted,
	    							inactive,
	    							filterPassenger(passengerId, taxicompanyId, imei, gender, name, surname, address, area, phone, mobile, email, nick,
	    									status, softwareversion, regdateFrom, regdateTo, cancelsFrom, cancelsTo, first, last, isWeb,
	    									country));
	    			break;
	    default:
		break;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    response =
   String.format(getXML("WHITE_PAGES_WRAPPER"), String.format(getXML("ACTION_FAILURE"), action, e.getMessage()));
	}
    }

    private String filterPassenger(Long passengerIdArg, Long taxicompanyIdArg, String imeiArg, String genderArg,
				   String nameArg, String lastnameArg, String addressArg, String areaArg, String phoneArg, String mobileArg,
				   String emailArg, String nickArg, String statusArg, String softwareversionArg, Long regdateFromArg,
				   Long regdateToArg, Long cancelsFromArg, Long cancelsToArg, Long first, Long last, boolean isweb,
				   String countryArg) throws Exception {
	prepareFilteredPassengerListStmt();
	String passengerInfoXML = "";
	//FIXED VALUES (compared using an equation)
	if (taxicompanyIdArg != null) {
	    filteredPassengerListStmt.setLong("taxicompanyIdArg", taxicompanyIdArg);
	} else {
	    filteredPassengerListStmt.setString("taxicompanyIdArg", null);
	}

	if (passengerIdArg != null) {
	    filteredPassengerListStmt.setLong("passengerIdArg", passengerIdArg);
	} else {
	    filteredPassengerListStmt.setString("passengerIdArg", null);
	}
	filteredPassengerListStmt.setString("imeiArg", imeiArg);
	filteredPassengerListStmt.setString("genderArg", genderArg);
	filteredPassengerListStmt.setString("statusArg", statusArg);
	//STRING VALUES (compared using like, need to replace the * with %)
	filteredPassengerListStmt.setString("nameArg",
					    nameArg != null && !nameArg.equals("") ? nameArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("lastnameArg",
					    lastnameArg != null && !lastnameArg.equals("") ? lastnameArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("addressArg",
					    addressArg != null && !addressArg.equals("") ? addressArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("areaArg",
					    areaArg != null && !areaArg.equals("") ? areaArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("phoneArg",
					    phoneArg != null && !phoneArg.equals("") ? phoneArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("mobileArg",
					    mobileArg != null && !mobileArg.equals("") ? mobileArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("emailArg",
					    emailArg != null && !emailArg.equals("") ? emailArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("nickArg",
					    nickArg != null && !nickArg.equals("") ? nickArg.replaceAll("\\*", "%") :
					    null);
	filteredPassengerListStmt.setString("softwareversionArg",
					    softwareversionArg != null && !softwareversionArg.equals("") ? softwareversionArg.replaceAll("\\*", "%") :
					    "");
	filteredPassengerListStmt.setString("countryArg", countryArg);

	//RANGE VALUES (compared using between)
	if (cancelsFromArg != null) {
	    filteredPassengerListStmt.setLong("cancelsFromArg", cancelsFromArg);
	} else {
	    filteredPassengerListStmt.setString("cancelsFromArg", null);
	}
	if (cancelsToArg != null) {
	    filteredPassengerListStmt.setLong("cancelsToArg", cancelsToArg);
	} else {
	    filteredPassengerListStmt.setString("cancelsToArg", null);
	}
	filteredPassengerListStmt.setTimestamp("regdateFromArg",
					       regdateFromArg != null ? new Timestamp(regdateFromArg) : null);
	filteredPassengerListStmt.setTimestamp("regdateToArg",
					       regdateToArg != null ? new Timestamp(regdateToArg) : null);
	filteredPassengerListStmt.setLong("firstArg", first);
	filteredPassengerListStmt.setLong("lastArg", last);
	ResultSet rs = filteredPassengerListStmt.executeQuery();
	while (rs.next()) {
	    Long passId = rs.getLong("passengerid");
	    Timestamp regdate = rs.getTimestamp("regdate");
	    Long compId = rs.getLong("taxicompanyid");
	    String imei = rs.getString("imei");
	    String gender = rs.getString("gender");
	    String name = rs.getString("name");
	    String surname = rs.getString("surname");
	    String address = rs.getString("address");
	    String area = rs.getString("area");
	    String mobile = rs.getString("mobile");
	    String phone = rs.getString("phone");
	    String email = rs.getString("email");
	    String nick = rs.getString("nick");
	    String pin = rs.getString("pin");
	    String status = rs.getString("status");
	    Integer cancels = rs.getInt("cancels");
	    String softwareVersion = rs.getString("softwareversion");
	    String locale = rs.getString("locale");
	    Long discountRides = rs.getLong("discountrides");
	    String country = rs.getString("OriginCountry");
	    String discountMsg = getDiscountMessage(locale, discountRides);
	    passengerInfoXML +=
			   String.format(getXML("PASSENGER_INFO"), passId, (regdate != null ? regdate.getTime() : null), compId, imei,
					 gender != null ? gender : "", name != null ? (isweb ? URLEncoder.encode(name, "UTF8") : name) : "",
					 surname != null ? (isweb ? URLEncoder.encode(surname, "UTF8") : surname) : "",
					 address != null ? (isweb ? URLEncoder.encode(address, "UTF8") : address) : "", area != null ? area : "",
					 mobile != null ? mobile : "", phone != null ? phone : "", email != null ? email : "", nick != null ? nick : "",
					 pin != null ? pin : "", status, cancels, softwareVersion, discountRides, discountMsg,
					 isweb ? (country == null ? "<country></country>" : "<country>" + country + "</country>") : "") +
			   "\n";
	}
	rs.close();
	dataPool.closeConnection();
	return passengerInfoXML;

    }

    private Long createPassenger(Long passengerId, Long taxicompanyId, String imei, String gender, String name,
				 String surname, String address, String area, String mobile, String phone, String email, String nick,
				 String plainTextPin, String status, String softwareversion, String country, String comments,
				 String typio) throws Exception {
    	prepareCreatePassengerStmt();
    	
    	Long generatedPassengerId = null;
    	//taxicompanyid, imei, gender, name, lastname, address, area, mobile, phone, email, softwareversion, passengerIdOUT
    	int i = 1;
    	if (passengerId != null) {
    		createPassengerStmt.setLong(i++, passengerId);
    	} else {
    		createPassengerStmt.setString(i++, null);
    	}
    	if (taxicompanyId != null) {
    		createPassengerStmt.setLong(i++, taxicompanyId);
    	} else {
    		createPassengerStmt.setString(i++, null);
    	}
    	createPassengerStmt.setString(i++, imei);
    	createPassengerStmt.setString(i++, gender);
    	createPassengerStmt.setString(i++, name);
    	createPassengerStmt.setString(i++, surname);
    	createPassengerStmt.setString(i++, address);
    	createPassengerStmt.setString(i++, area);
    	String hashedPin = plainTextPin != null ? Util.digest(plainTextPin) : null;
    	createPassengerStmt.setString(i++, mobile);
    	createPassengerStmt.setString(i++,phone != null || hashedPin == null ? phone : mobile); //if hashed pin exists, then this is the case of web booking and phone is never provided at the beginning
    	createPassengerStmt.setString(i++, email);
    	createPassengerStmt.setString(i++, nick);
    	createPassengerStmt.setString(i++, hashedPin);
    	createPassengerStmt.setString(i++, status);
    	createPassengerStmt.setString(i++, softwareversion);
    	createPassengerStmt.setString(i++, country);
    	createPassengerStmt.setString(i++, typio);
    	createPassengerStmt.setString(i++, comments);
    	createPassengerStmt.setString(i++, null); //cancels arg
    	createPassengerStmt.setString(i++, null);
    	
    	createPassengerStmt.registerOutParameter(i++, Types.NUMERIC);
    	createPassengerStmt.execute();
    	generatedPassengerId = createPassengerStmt.getLong("passengerIdOut");
    	dataPool.closeConnection();
    	
    	LOGGER.info(" the companyid "+taxicompanyId+" the country "+country+" mobile "+mobile+" plain pin "+plainTextPin);
    	
    	
    	
    	if(plainTextPin!=null&&country!=null) sendPinSms(taxicompanyId, country, mobile, plainTextPin);
    	
    	return generatedPassengerId;
    }

    private void sendPinSms(Long taxiCompanyId, String country, String mobile,String plainTextPin) {
    	
    	try {
    		if(taxiCompanyId==null) taxiCompanyId = DataHelper.getDefaultCompany(country);
    	
    		LOGGER.info("Fetched default country "+taxiCompanyId);
    		
    		SMSSUtil smsProvider = new SMSSUtil(taxiCompanyId);
    		if(country!=null) smsProvider.setCountryCode(country);
    		String message = Util.getProperty("welcomeSMS_" + (country != null ? country : Push.DEFAULT_PASSENGER_LOCALE), "lang")+plainTextPin; 
    		
    		String formatedMessage = String.format(message,DataHelper.getTaxiCompany(taxiCompanyId).getShortName());
    		
    		LOGGER.debug("SMD Will send sms from "+taxiCompanyId+" to "+mobile+" "+formatedMessage);
    	    
    		smsProvider.sendSmsAsync(mobile, formatedMessage); 		
    	
    	} catch (SQLException e) {
    		LOGGER.info("Problem retieving default company from database",e);
    	}
    }
     
    //TODO MODIFY WHEN THE CAMPAIGN CHANGES (MESSAGES SENT TO THE PASSENGER HERE)
    //ALWAYS SEND IN ENGLISH, BECAUSE THERE'S A COMPATIBILITY ISSUE WITH THE iphone

    public static String getDiscountMessage(String locale, Long discountRides) {
	String output = "";
	/* DON'T SEND ANYTHING TO THE PASSENGER BEFORE THE DISCOUNTED RIDE
		try {
			URLEncoder.encode(discountRides > 0 ?
				   String.format(Util.getProperty("discount_msg_1_" + Push.DEFAULT_PASSENGER_LOCALE,//(locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE)
												  "lang"), Util.getProperty("discount_max_amount", "lang")) : output, "UTF8");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		 */
	return output;
    }

    public static String getDiscountMessage(String locale, boolean isDiscounted) {
    	String output = "";
    	try {
    		return isDiscounted ? //(locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE)
    				String.format(Util.getProperty("discount_msg_2_" + Push.DEFAULT_PASSENGER_LOCALE, "lang"),
			      Util.getProperty("discount_max_amount", "lang")) : output;
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return output;
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
    
}
