package aploon.xmpp.ssclient.web;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import aploon.xmpp.ServerSideClient;

import com.taxiplon.Util;


abstract public class PagedList extends ServerSideClient {
	
	public static final Long DEFAULT_FIRST = Long.parseLong(Util.getProperty("default_first", "config"));
	public static final Long DEFAULT_LAST = Long.parseLong(Util.getProperty("default_last", "config"));
	
	private static final Logger LOGGER = Logger.getLogger(PagedList.class);
	
	public PagedList() {
		super();
	}
	
	protected PagedResult getPagedResult(String selectFields, String fromClause, 
										 String joinConditions, String filterCriteria,
										 String orderClause,
										 PagedResult output) throws SQLException {
		return getPagedResult(selectFields, fromClause, joinConditions, filterCriteria, null, orderClause, "*", output);
	}
	
        protected PagedResult getPagedResult(Long passengerId,PagedResult output) throws SQLException {
            
            PreparedStatement preparedStmt = dataPool.prepareStatement("select t.tripid, t.hailtime, t.pickuptime, t.pickupaddress, t.destination, t.specialneeds, t.status, t.bookagent, t.rating, d.driverid, d.lastname, d.firstname, d.codename, v.plateno as platenumber, v.model as carmodel, f.driverid as isfaved, b.driverid as isblocked, pa.amount, pa.currency, pa.channel, tc.email, tc.facebook, tc.twitter from trip t " + 
            "  left outer join payments pa on t.paymentid = pa.paymentid and pa.origin = 'trip'," + 
            "  vehicle v, driver d" + 
            "  left outer join favourites f on f.driverid = d.driverid and f.passengerid = ?" + 
            "  left outer join blacklisted b on b.driverid = d.driverid and b.passengerid = ?," + 
            "  taxicompany tc" + 
            " where t.passengerid = ?" + 
            " and t.vehicleid = v.vehicleid" + 
            " and t.driverid = d.driverid" + 
            " and t.taxicompanyid = tc.taxicompanyid ORDER BY t.pickuptime DESC LIMIT 10");
            
            preparedStmt.setLong(1, passengerId);
            preparedStmt.setLong(2, passengerId);
            preparedStmt.setLong(3, passengerId);
            
            ResultSet resultSet = preparedStmt.executeQuery();           
            resultSet.last();
            output.setTotalCount(resultSet.getRow());
            resultSet.first();
            output.setResult(resultSet);
            resultSet.close();
                
            return output;
        }
        
	protected PagedResult getPagedResult(String selectFields, String fromClause, 
										 String joinConditions, String filterCriteria,
										 String groupByClause, String orderClause,
										 String columnToCount,
										 PagedResult output) throws SQLException {
		
		//first fetch the total
		ResultSet rs = dataPool.executeQuery("SELECT COUNT(" + columnToCount + ") as total FROM " + fromClause + 
											 " WHERE " + 
											 (joinConditions != null? joinConditions : "") + 
											 (joinConditions != null && joinConditions.length() > 0? " AND " : "") + filterCriteria);
		
		if(rs.next()) {
			output.setTotalCount(rs.getLong("total"));
		
		//is SELECT COUNT(*) as total FROM driver dr LEFT OUTER JOIN device d ON d.deviceid = dr.deviceid  LEFT OUTER JOIN sim s ON s.simid = d.simid LEFT OUTER JOIN vehicle v ON v.vehicleid = d.vehicleid, drivercompanies dc,taxicompany tc  WHERE dc.driverid = dr.driverid AND dc.taxicompanyid=tc.taxicompanyid AND ((dc.taxicompanyid = NULL AND NULL IS NOT NULL) OR (NULL IS NULL AND NULL IS NULL) OR (dc.taxicompanyid = default_company(NULL) AND NULL IS NOT NULL))  AND d.status = 'ava'  AND dr.active = ifnull('true', active) AND dr.driverid = ifnull(NULL, dr.driverid) AND ( (dr.driverid NOT IN (SELECT bla.driverid FROM blacklisted bla WHERE bla.passengerid=198491) AND 198491 IS NOT NULL ) OR (198491 IS NULL)  ) AND ((dr.lastname like NULL AND NULL IS NOT NULL AND NULL != '') OR NULL IS NULL OR NULL = '') AND ((v.plateno like NULL AND NULL IS NOT NULL AND NULL != '') OR NULL IS NULL OR NULL = '') AND ((d.shortname like NULL AND NULL IS NOT NULL AND NULL != '') OR NULL IS NULL OR NULL = '') AND ((tc.customerselectable='true' AND NULL IS NULL) OR (NULL IS NOT NULL))

		}	
	    rs.close();
		//use limits anyway
		Long first = parseOptionalField("first", Long.class);
		if (first == null) {
			first = DEFAULT_FIRST;
		}
		Long last = parseOptionalField("last", Long.class);
		if (last == null) {
			last = DEFAULT_LAST;
		}
		//an alternative to count(*) can be found here http://www.arraystudio.com/as-workshop/mysql-get-total-number-of-rows-when-using-limit.html
		String query = "SELECT " + selectFields + 
					   " FROM " + fromClause + 
					   " WHERE " + 
					   (joinConditions != null? joinConditions : "") + 
					   (joinConditions != null && joinConditions.length() > 0? " AND " : "") + filterCriteria +
					   (groupByClause != null && groupByClause.length() > 0? " GROUP BY " + groupByClause : "") +
					   (orderClause != null && orderClause.length() > 0? " ORDER BY " + orderClause : "") +
					   " LIMIT " + first + ", " + last;
		
		ResultSet result = dataPool.executeQuery(query);
		output.setResult(result);
	    result.close();
	    dataPool.closeConnection();
		return output;
	}
	
	/**
	 * Date format used by the MySQL for dates.
	 */
	SimpleDateFormat sqlDdateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	protected String timestampToSQLString(Long millis) {
		return millis != null? stringToSQLString(sqlDdateFormat.format(new Timestamp(millis)), false) : "NULL";
	}
	
	protected String stringToSQLString(String s) {
		return stringToSQLString(s, false);
	}
	
	protected String stringToSQLString(String s, boolean allowWildchar) {
		s = sanitize(s);
		if(allowWildchar) {
			s = s.replaceAll("\\*", "%");//replace the usual * with SQL %
		}
		return s;
	}
	
	protected String nonStringObjectToString(Object o) {
		return o != null? "" + o : "NULL";
	}

	@Override
	public void processRequest() {
		notAllowedToCallDirectly();
	}
	
	abstract class PagedResult {

	    /**
	     * The implementing class should describe a way to process the ResultSet to build the outcoming XML.
	     * @param rs
	     * @return
	     * @throws SQLException
	     */
	    protected abstract String processResult(ResultSet rs) throws SQLException;
		
		String result;
		long totalCount;
		
		PagedResult() {
			super();
			this.result = null;
			this.totalCount = 0L;
		}

		void setResult(ResultSet resultSet) throws SQLException {
			this.result = processResult(resultSet);
		}

		public String getResult() {
			return result;
		}

		public void setTotalCount(long totalCount) {
			this.totalCount = totalCount;
		}

		public long getTotalCount() {
			return totalCount;
		}
	}
}
