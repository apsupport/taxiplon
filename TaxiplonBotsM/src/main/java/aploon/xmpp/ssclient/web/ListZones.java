package aploon.xmpp.ssclient.web;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.concurrency.util.NamingThreadFactory;
import aploon.datahelper.DataHelper;
import aploon.model.Zone;
import aploon.xml.model.ListZonesResponseXML;
import aploon.xml.model.ZoneXML;
import aploon.xmpp.ServerSideClient;
import aploon.zone.ZoneUtil;

public class ListZones extends ServerSideClient {
	
	private ExecutorService zonelistService = null;
	
	private static final Logger LOGGER = Logger.getLogger(ListZones.class);
	
	public ListZones() {
		super();
		
		zonelistService = Executors.newFixedThreadPool(4,new NamingThreadFactory("zoneslist_pool"));
	}
	
	public void processRequest() {
		
		String companyId = parseOptionalField("compid", String.class); 
		Long driverId = parseOptionalField("driverid", Long.class);
		
		Double lat = parseOptionalField("lat", Double.class);
		Double lng = parseOptionalField("lng", Double.class);
		
		Boolean allInfo = parseOptionalField("allInfo", Boolean.class);
		if(allInfo==null) {
			allInfo = false;
		}
		
		LOGGER.info("Processing the request compid "+request.get("compid")+" driverid "+request.get("driverid"));
		
		zonelistService.execute(new ListZonesRunnable(from, companyId, driverId, lat, lng, allInfo));
		
		response = null;
		
	}
	
	private class ListZonesRunnable implements Runnable {

		private String toUser;
		private String companyId;
		private Long driverId;
		private Double lat;
		private Double lng;
		private Boolean allInfo;
		
		public ListZonesRunnable(String toUser,String companyId,Long driverId,Double lat,Double lng,Boolean allInfo) {
			this.toUser = toUser;
			this.companyId = companyId;
			this.driverId = driverId;
			this.lat = lat;
			this.lng = lng;
			this.allInfo = allInfo;
		}
		
		@Override
		public void run() {
			try {
				
				String responseStr = null;
				if(allInfo) {
					responseStr = getCompanyZonesFullInfo(companyId,driverId);
				} else {
					responseStr = getCompanyZones(companyId, driverId, lat, lng);
				}
				
				comm.sendMessage(responseStr, toUser);
				
			} catch(Exception e) {
				LOGGER.error("serialization problem",e);
			}
		}
		
	}
	
	public static String getCompanyZonesFullInfo(String companyId,Long driverId) throws Exception {
		
		ListZonesResponseXML listZonesResponseXML = new ListZonesResponseXML();
		
		try {
			
			List<Zone> zones = DataHelper.getDriverFullZones(driverId,companyId);
			List<ZoneXML> zoneXMLs = new ArrayList<>();
			for(Zone zone:zones) {
				
				ZoneXML zoneXML = new ZoneXML(zone.getZoneId(), zone.getTaxiCompanyId(),
						zone.getZoneName(), zone.getZoneLat(), zone.getZoneLong(),
						zone.getZoneRange(), zone.getDriversInZone(),
						zone.getZonePoints(),zone.getStrictBorders());
				zoneXMLs.add(zoneXML);
			}
			
			listZonesResponseXML.setSuccess(true);
			listZonesResponseXML.setTotal(zoneXMLs.size());
			listZonesResponseXML.setMessage("Successfully fetched rows");
			listZonesResponseXML.setContent(zoneXMLs);
			
		} catch(SQLException e) {
			LOGGER.error("Fatal mysql error",e);
			listZonesResponseXML.setSuccess(false);
			listZonesResponseXML.setMessage("problem with response data");
		}
		
		return ServerSideClient.serializeObject(listZonesResponseXML).replace("</content>", "").replace("<content class=\"java.util.ArrayList\">", "");
	}
	
	/**
	public static String getCompanyZones(String companyId,Long driverId) throws Exception {
		
		ListZonesResponseXML listZonesResponseXML = new ListZonesResponseXML();
		
		try {
			
			LOGGER.info("Getting the zones "+companyId+" "+driverId);
			List<Zone> zones = DataHelper.getCompanyZones(driverId,companyId);
			List<ZoneXML> zoneXMLs = new ArrayList<ZoneXML>();
			
			for(Zone zone:zones) {
				
				ZoneXML zoneXML = new ZoneXML(zone.getZoneId(), zone.getTaxiCompanyId(), zone.getZoneName(),
						zone.getZoneLat(), zone.getZoneLong(), zone.getZoneRange(), zone.getDriversInZone(),
						zone.getZonePoints(),zone.getStrictBorders());
			
				zoneXMLs.add(zoneXML);
			
			}
			
			listZonesResponseXML.setSuccess(true);
			listZonesResponseXML.setTotal(zoneXMLs.size());
			listZonesResponseXML.setMessage("Successfully fetched rows");
			listZonesResponseXML.setContent(zoneXMLs);
			
		} catch (SQLException e) {
			LOGGER.error("Fatal mysql error",e);
			listZonesResponseXML.setSuccess(false);
			listZonesResponseXML.setMessage("problem with response data");
		}
		
		return ServerSideClient.serializeObject(listZonesResponseXML).replace("</content>", "").replace("<content class=\"java.util.ArrayList\">", "");
	
	}
	*/
	
	public static String getCompanyZones(String companyId,Long driverId,Double lat,Double lng) throws Exception {
		
		ListZonesResponseXML listZonesResponseXML = new ListZonesResponseXML();
		
		try {
		
			LOGGER.info("Getting the company zones "+companyId+" "+driverId);
			List<Zone> zones = DataHelper.getCompanyZones(driverId,companyId);
			List<ZoneXML> zoneXMLs = new ArrayList<ZoneXML>();
			
			for(Zone zone:zones) {
				
				ZoneXML zoneXML = new ZoneXML(zone.getZoneId(), zone.getTaxiCompanyId(), 
						zone.getZoneName(), zone.getZoneLat(), zone.getZoneLong(), 
						zone.getZoneRange(), zone.getDriversInZone(),zone.getZonePoints(),
						zone.getStrictBorders());
				
				LOGGER.info("The lat lng is "+lat+" "+lng);
				
				if(lat!=null&&lng!=null&&zone.getStrictBorders()) {
				
					if(ZoneUtil.coordinatesInsideZone(lat, lng, zone)) {
							zoneXMLs.add(zoneXML);
					}
				
				} else {
					zoneXMLs.add(zoneXML);
				}
			
			}
			
			listZonesResponseXML.setSuccess(true);
			listZonesResponseXML.setTotal(zoneXMLs.size());
			listZonesResponseXML.setMessage("Successfully fetched rows");
			listZonesResponseXML.setContent(zoneXMLs);
			
		} catch (SQLException e) {
			LOGGER.error("Fatal mysql error",e);
			listZonesResponseXML.setSuccess(false);
			listZonesResponseXML.setMessage("problem with response data");
		}
		
		return ServerSideClient.serializeObject(listZonesResponseXML).replace("</content>", "").replace("<content class=\"java.util.ArrayList\">", "");
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
	
}
