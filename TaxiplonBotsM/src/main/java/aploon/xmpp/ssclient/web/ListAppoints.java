package aploon.xmpp.ssclient.web;


import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;
import java.sql.Timestamp;

import org.jivesoftware.smack.packet.IQ;

public class ListAppoints extends ServerSideClient {

    private NamedParameterStatement appointListByCompanyIdStmt;

    public ListAppoints() throws Exception {
    	super();
    }

    private void prepareAppointListByCompanyIdStmt() throws Exception {
    	if (appointListByCompanyIdStmt == null || appointListByCompanyIdStmt.isClosed()) {
    		appointListByCompanyIdStmt = new NamedParameterStatement(dataPool, getQuery("appointListByCompanyId"));
    	}
    }

    public enum Statuses {
    	pen,acc,dec,trip,can,exp,una;
    }

    public void processRequest() {
    	try {
    		//pending appointments
    		Long taxicompanyID = parseOptionalField("compid", Long.class);
    		//taken appointments
    		Long driverID = parseOptionalField("driverid", Long.class);
    		Long passID = parseOptionalField("passid", Long.class);
    		Statuses status = parseOptionalField("status", Statuses.class);
    		response = String.format(getXML("MULTIPLE_APPOINT_WRAPPER"), getAppointList(taxicompanyID, driverID, passID, status));
    	} catch (Exception e) {
    		e.printStackTrace();
    		response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
    	}
    }

    private String getAppointList(Long taxicompanyID, Long driverID, Long passID, Statuses requestedStatus) throws Exception {
	
    	prepareAppointListByCompanyIdStmt();
    	String appointListXML = "";
    	appointListByCompanyIdStmt.setString("taxicompanyid", taxicompanyID == null ? null : taxicompanyID.toString());
    	appointListByCompanyIdStmt.setString("driverid", driverID == null ? null : driverID.toString());
    	appointListByCompanyIdStmt.setString("passid", passID == null ? null : passID.toString());
    	appointListByCompanyIdStmt.setString("status", requestedStatus == null ? null : requestedStatus.toString());
    	ResultSet appointList = appointListByCompanyIdStmt.executeQuery();
    	while (appointList.next()) {
    		Long appointId = appointList.getLong("appointmentid");
    		Long userId = appointList.getLong("userid");
    		Long driverId = appointList.getLong("driverid");
    		Double lat = appointList.getDouble("pickuplat");
    		Double lng = appointList.getDouble("pickuplong");
    		String address = appointList.getString("pickupaddress");
    		Timestamp regdate = appointList.getTimestamp("registrationdatetime");
    		Long reqdate;
    		try {
    			reqdate = appointList.getTimestamp("requestdatetime").getTime();
    		} catch (Exception e) {
    			reqdate = 0L;
    		}
    		String destination = appointList.getString("destination");
    		String special = appointList.getString("specialneeds");
    		String status = appointList.getString("status");
    		Double fare = appointList.getDouble("fare");
    		String passengerGender = appointList.getString("gender");
    		String passengerName = appointList.getString("name");
    		String passengerSurname = appointList.getString("surname");
    		String passengerMobile = appointList.getString("mobile");
    		String passengerImei = appointList.getString("imei");
    		String driverSurname = appointList.getString("driversurname");
    		String driverName = appointList.getString("drivername");
    		String driverCode = appointList.getString("drivercodename");
    		String driverMobile = appointList.getString("drivermobile");
    		String passengerPhone = appointList.getString("phone");
    		String contactPhone = appointList.getString("contactPhone");
    		String appointPhone = (!(contactPhone == null || contactPhone.equals(""))) ? contactPhone : (!(passengerPhone == null ||
												passengerPhone.equals(""))) ? passengerPhone :passengerMobile;
    		Long compId = appointList.getLong("taxicompanyid");
    		String shortName = appointList.getString("shortname");
    		String officialName = appointList.getString("officialname");
    		String telephone = appointList.getString("compphone");
    		//format the XML message
    		appointListXML += String.format(getXML("APPOINT_LIST_ITEM"), appointId, userId, regdate.getTime(), lat, lng, address != null ?
														      address : "", destination != null ? destination : "", reqdate, driverId,
					 special != null ? special : "", status, fare, passengerGender, passengerName, passengerSurname, appointPhone,
					 passengerImei, driverName != null ? driverName : "", driverSurname != null ? driverSurname : "",
					 driverCode != null ? driverCode : "", driverMobile != null ? driverMobile : "", compId,
					 shortName == null ? "" : shortName, officialName == null ? "" : officialName,
					 telephone == null ? "" : telephone);
    	}
    	appointList.close();
    	dataPool.closeConnection();
    	return appointListXML;
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
