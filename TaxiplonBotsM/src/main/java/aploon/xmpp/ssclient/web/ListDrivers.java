package aploon.xmpp.ssclient.web;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.Main;
import aploon.cache.DriverCache;
import aploon.datahelper.DataHelper;
import aploon.statusmanager.OnRideStatusManager;

import com.taxiplon.db.NamedParameterStatement;


public class ListDrivers extends PagedList {

    private NamedParameterStatement filteredDriverListStmt;

    private static final Logger LOGGER = Logger.getLogger(ListDrivers.class);
    
    //TODO private final Thread onRideDriversCheck;
    private final Timer onRideDriversTimer;
    private final Timer driverCacheScheduler;
    
    private Timer blockedDriversCheckTimer;
    
    
    public ListDrivers() throws Exception {
    	super();
    	onRideDriversTimer = new Timer("onridedrivers_scheduler");
    	driverCacheScheduler = new Timer("drivercache_scheduler");
    	
    	//TODO onRideDriversCheck = new Thread(new OnRideDriversCheckRunnable());
    	if (!Main.DEBUG) {
    		Thread deviceStatus = new DeviceStatusCheck();
    		deviceStatus.setName("devicestatus_scheduler");
    		deviceStatus.start();
    		//TODO onRideDriversCheck.start();
    		onRideDriversTimer.schedule(new OnRideDriversCheckTimerTask(), 0L, 60*1000);
    		driverCacheScheduler.schedule(new DriverCacheRefreshTask(), 0L,60*1000*5);
    	}
    	
    	blockedDriversCheckTimer = new Timer("blockeddrivers_scheduler");
    	blockedDriversCheckTimer.schedule(new BlockedDriversCheckTask() ,new Date() ,1000*60);
    
    }
    
    class BlockedDriversCheckTask extends TimerTask {
		@Override
		public void run() {
			try {
				//TODO 
				//PenaltyManager.checkAndFixPenalties();
			} catch (Exception e) {
				LOGGER.error("Unknown exception when trying to fix penalties",e);
			}
		}
    }

    class DriverCacheRefreshTask extends TimerTask {

		@Override
		public void run() {
			try {
				DriverCache.setDrivers(DataHelper.fetchDriverForCache());
			} catch (SQLException e) {
				LOGGER.error("problem while fetching drivers for caching ",e);
			}
		}
    
    }
    
    /**
     * Will check if a driver is on ride and when to stop it
     * @author gkatzioura
     *
     */
    class OnRideDriversCheckTimerTask extends TimerTask {

		@Override
		public void run() {
			try {
				try {
					List<Long> driversOnRide = DataHelper.getDriversOnRide();
				
					for(Long driverId:driversOnRide) {
						OnRideStatusManager onRideStatusManager = new OnRideStatusManager(driverId);
						if(onRideStatusManager!=null) {
							onRideStatusManager.undoOnRideStatus();
						}
					}
					
				} catch (SQLException e) {
					LOGGER.error(e);
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
			
		}
    
    }
    
    /**
    class OnRideDriversCheckRunnable implements Runnable {

		@Override
		public void run() {
			
			while(Main.RUNNING) {
			
				while(true) {
						try {
							List<Long> driversOnRide = DataHelper.getDriversOnRide();
						    
							for(Long driverId:driversOnRide) {
								OnRideStatusManager onRideStatusManager = new OnRideStatusManager(driverId);
								if(onRideStatusManager!=null) {
									onRideStatusManager.undoOnRideStatus();
								} 
							}
						} catch (SQLException e) {
							LOGGER.error("A ride exception ",e);
						}
					
					try {
					    Thread.sleep(60000);
					} catch(InterruptedException e) {
					    LOGGER.error("Problem sleeping",e);
					} 

					LOGGER.debug("On ride status manager iteration continued ");
				}
			}
			
		}
    
    }
    */
    
    int sleepTimeMin = 0;
    static final int DEFAULT_SLEEP_TIME_SEC = 15;

    /**
     * This thread will synchronize the XMPP server statuses with taxiplon statuses.
     */
    class DeviceStatusCheck extends Thread {
    	
    	public void run() {
    		try {
    			while (Main.RUNNING) {
		    
	    		try {
	    			if (comm != null && comm.getConn() != null) {
	    				//on every run fetch update delay time
	    				sleepTimeMin = getNumberFromProps("driver_status_check_interval", DEFAULT_SLEEP_TIME_SEC);
	    				//update driver statuses in the DB according to their XMPP status (if one is offline, then set device.status = 'off')
	    				//get all the available devices (these are the ones to check status of)
	    				ResultSet rs = dataPool.getConnection().createStatement().executeQuery(getQuery("getAvailableDevices"));
	    				String deviceids = "";
	    				//check for each of results whether or not they are online
	    				while (rs.next()) {
	    					String imei = rs.getString("imei");
	    					Long deviceid = rs.getLong("deviceid");
	    				
	    					boolean offline = comm.isDriverOffline(imei);
	    					
	    					if (offline) {
	    						//this is the case of conflict (Db returned a status like 'ava' or 'occ', but the XMPP server sees him offline)
	    						deviceids += (deviceids.length() == 0 ? "" : ", ") + deviceid;
	    					}
	    					
	    					//LOGGER.info("The imei "+imei+" is "+offline);
	    				}
	    				
	    				rs.close();
	    				dataPool.closeConnection();
	    				//if devices with conflicting status were found, update the DB
	    				String report = "";
	    				if (deviceids.length() > 0) {
	    					int updatedRows = dataPool.getConnection().createStatement().executeUpdate(String.format(getQuery("updateStatusForDevices"),deviceids));
	    					report = "Updated status for " + updatedRows + " devices (" + deviceids + ")";
	    				} else {
	    					report = "No conflicts in statuses were found";
	    				}
	    				
	    				reports.put(this.getClass().getSimpleName(), report);
	    			}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
		   
	    		Thread.sleep(sleepTimeMin * 1000);
	    		}
	    	} catch (Exception e) {
	    	e.printStackTrace();
	    }
		}
    }

    private void prepareFilteredDriverListStmt() throws Exception {
    	if (filteredDriverListStmt == null || filteredDriverListStmt.isClosed()) {
    		filteredDriverListStmt = new NamedParameterStatement(dataPool, getQuery("filteredDriverList"));
    	}
    }

    public void processRequest() {
    	try {
    		Long taxicompanyID = parseOptionalField("compid", Long.class);
    		Long driverId = parseOptionalField("driverid", Long.class);
    		String lastname = request.get("surname");
    		String plateno = request.get("plateno");
    		String shortname = request.get("shortname");
    		String active = request.get("active");
    		String country = request.get("country");
    		Long passID = parseOptionalField("passid", Long.class);
    		Boolean light = parseOptionalField("light", Boolean.class);
    		//for the appointment without a favorite driver, we need to get a list with drivers around the desired location
    		String aroundCoordinates = request.get("around");
    		Double lat = null;
    		Double lng = null;
    		if (aroundCoordinates != null) {
    			//has the format around=lat,lng
    			String[] parts = aroundCoordinates.split(",");
    			try {
    				lat = Double.parseDouble(parts[0]);
    				lng = Double.parseDouble(parts[1]);
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    		
    		/*
    		LOGGER.info("The taxicompanyid "+taxicompanyID+" driverid "+driverId+" lastname "+lastname+" plateno "+plateno
    				+" shortname "+shortname+" active "+active+" country "+country+" passId "+passID+" light "+light
    				+" lat "+lat+" lng "+lng);
    		*/
    		
    		response = getDriverList(taxicompanyID, driverId, lastname, active, plateno, shortname, lat, lng, country, light, passID);
    	} catch (Exception e) {
    		e.printStackTrace();
    		response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
    	}
    }

    private String getDriverList(Long taxicompanyID, Long driverIdArg, String lastnameArg, String activeArg,
				 String platenoArg, String shortnameArg, Double lat, Double lng, String country, Boolean litte,
				 Long PassengerID) throws Exception {
    	boolean fetchClosestDriversOnly = lat != null && lng != null;
    	
    	if (fetchClosestDriversOnly && request.get("last") == null) {
    		request.put("last", "5");
    	}
    	
    	final Boolean uselight = (litte != null && litte);
    	if (uselight) {
    		///ekos this is here for backwards compatibility
    		country = null;
    	}
    	
    	PagedResult paged =
		   getPagedResult(getQuery("filteredDriverListSelect") + (fetchClosestDriversOnly ? getQuery("filteredDriverListAddDistance").replaceAll(":lat",
																			 nonStringObjectToString(lat)).replaceAll(":lng", nonStringObjectToString(lng)) : ""),
				  getQuery("filteredDriverListFrom"), getQuery("filteredDriverListJoin"),
				  getQuery("filteredDriverListFilter").replaceAll(":passIdArg",
										  nonStringObjectToString(PassengerID)).replaceAll(":statusClause",
																   fetchClosestDriversOnly ? (uselight ? " AND d.status = 'ava' " : " AND d.status != 'off' ") :
																   "").replaceAll(":companyidArg", nonStringObjectToString(taxicompanyID)).replaceAll(":activeArg",
																										      stringToSQLString(activeArg)).replaceAll(":countryArg",
																															       stringToSQLString(country)).replaceAll(":driveridArg",
																																				      nonStringObjectToString(driverIdArg)).replaceAll(":lastnameArg",
																																										       stringToSQLString(lastnameArg, true)).replaceAll(":platenoArg",
																																																	stringToSQLString(platenoArg, true)).replaceAll(":shortnameArg",
																																																							stringToSQLString(shortnameArg, true)),
				  fetchClosestDriversOnly ? getQuery("filteredDriverListForAppointOrder") : getQuery("filteredDriverListOrder"),
				  new PagedResult() {
					  
					  @Override
					  protected String processResult(ResultSet rs) throws SQLException {
						  String result = "";
						  Long prevDriver = 0L;
						  while (rs.next()) {
							  try {
								  Long driverId = rs.getLong("driverid");
								  if (prevDriver == driverId) {
									  continue;
								  }
								  prevDriver = driverId;
								  String codename = rs.getString("codename");
								  Double lat = rs.getDouble("currentlat");
								  Double lng = rs.getDouble("currentlong");
								  String name = rs.getString("name");
								  String surname = rs.getString("surname");
								  String platenumber = rs.getString("platenumber");
								  String carmodel = rs.getString("carmodel");
								  if (uselight) {
									  Double hailR = rs.getDouble("hailrange");
									  Double dist = rs.getDouble("dist_driver");
									  result +=
											  String.format(getXML("DRIVER_LIST_ITEM_LIGHT"), driverId, codename != null ? codename : "", surname != null ?
														     surname : "", name != null ? name : "", lat, lng, platenumber == null ? "" : platenumber,
														    		 carmodel == null ? "" : carmodel, dist, hailR);
								  } else {
									  String gender = rs.getString("gender");
									  String address = rs.getString("address");
									  String telephone = rs.getString("telephone");
				String mobile = rs.getString("mobile");
				String email = rs.getString("email");
				String paymentdetails = rs.getString("paymentdetails");
				String comments = rs.getString("comments");
				Timestamp originaljoindate = rs.getTimestamp("originaljoindate");
				Timestamp expirydate = rs.getTimestamp("expirydate");
				String driverstatus = rs.getString("driverstatus");
				String rateperiod = rs.getString("rateperiod");
				Double rateamount = rs.getDouble("rateamount");
				String active = rs.getString("active");
				String pin = rs.getString("pin");
				String devicestatus = rs.getString("devicestatus");
				String imei = rs.getString("imei");
				String deviceShortname = rs.getString("shortname");
				String msisdn = rs.getString("msisdn");
				String iccid = rs.getString("iccid");
				String ip = rs.getString("ip");
				String softwareversion = rs.getString("softwareversion");
				String sos = rs.getString("sos");


				//format the XML message
				result +=
			 String.format(getXML("DRIVER_LIST_ITEM"), driverId, codename != null ? codename : "", gender != null ? gender : "",
				       surname != null ? surname : "", name != null ? name : "", address != null ? address : "",
				       telephone != null ? telephone : "", mobile != null ? mobile : "", email != null ? email : "",
				       paymentdetails != null ? paymentdetails : "", comments != null ? comments : "",
				       originaljoindate != null ? originaljoindate.getTime() : originaljoindate,
				       expirydate != null ? expirydate.getTime() : expirydate, driverstatus != null ? driverstatus : "",
				       rateperiod != null ? rateperiod : "", rateamount != null ? rateamount : "", active != null ? active : "",
				       pin, platenumber != null ? platenumber : "", carmodel != null ? carmodel : "", lat, lng, devicestatus, imei,
				       deviceShortname != null ? deviceShortname : "", msisdn != null ? msisdn : "", iccid != null ? iccid : "",
				       ip != null ? ip : "", softwareversion != null ? softwareversion : "", sos) +
						   "\n";
			    }
			} catch (Exception e) {
			    e.printStackTrace();
			}
		    }
		    return result;
		}
	    });
	/*
		prepareFilteredDriverListStmt();
		//FIXED VALUES (compared using an equation)
        filteredDriverListStmt.setLong("companyidArg", taxicompanyID);
        filteredDriverListStmt.setString("driveridArg", driverIdArg == null ? null : driverIdArg.toString());
        filteredDriverListStmt.setString("genderArg", genderArg);
        filteredDriverListStmt.setString("statusArg", statusArg);
        filteredDriverListStmt.setString("rateperiodArg", ratePeriodArg);
        //STRING VALUES (compared using like, need to replace the * with %)
        filteredDriverListStmt.setString("codenameArg",
                                         codenameArg != null && !codenameArg.equals("") ? codenameArg.replaceAll("\\*",
                                                                                                                 "%") :
                                         null);
        filteredDriverListStmt.setString("lastnameArg",
                                         lastnameArg != null && !lastnameArg.equals("") ? lastnameArg.replaceAll("\\*",
                                                                                                                 "%") :
                                         null);
        filteredDriverListStmt.setString("firstnameArg",
                                         firstnameArg != null && !firstnameArg.equals("") ? firstnameArg.replaceAll("\\*",
                                                                                                                    "%") :
                                         null);
        filteredDriverListStmt.setString("addressArg",
                                         addressArg != null && !addressArg.equals("") ? addressArg.replaceAll("\\*",
                                                                                                              "%") :
                                         null);
        filteredDriverListStmt.setString("telephoneArg",
                                         telephoneArg != null && !telephoneArg.equals("") ? telephoneArg.replaceAll("\\*",
                                                                                                                    "%") :
                                         null);
        filteredDriverListStmt.setString("mobileArg",
                                         mobileArg != null && !mobileArg.equals("") ? mobileArg.replaceAll("\\*",
                                                                                                           "%") :
                                         null);
        filteredDriverListStmt.setString("emailArg",
                                         emailArg != null && !emailArg.equals("") ? emailArg.replaceAll("\\*", "%") :
                                         null);
        filteredDriverListStmt.setString("commentsArg",
                                         commentsArg != null && !commentsArg.equals("") ? commentsArg.replaceAll("\\*",
                                                                                                                 "%") :
                                         null);
        filteredDriverListStmt.setString("platenoArg",
                                         platenoArg != null && !platenoArg.equals("") ? platenoArg.replaceAll("\\*",
                                                                                                              "%") :
                                         null);
        filteredDriverListStmt.setString("imeiArg",
                                         imeiArg != null && !imeiArg.equals("") ? imeiArg.replaceAll("\\*", "%") :
                                         null);
        filteredDriverListStmt.setString("shortnameArg",
                                         shortnameArg != null && !shortnameArg.equals("") ? shortnameArg.replaceAll("\\*",
                                                                                                                    "%") :
                                         null);
        filteredDriverListStmt.setString("msisdnArg",
                                         msisdnArg != null && !msisdnArg.equals("") ? msisdnArg.replaceAll("\\*",
                                                                                                           "%") :
                                         null);
        filteredDriverListStmt.setString("iccidArg",
                                         iccidArg != null && !iccidArg.equals("") ? iccidArg.replaceAll("\\*", "%") :
                                         null);
        filteredDriverListStmt.setString("ipArg",
                                         ipArg != null && !ipArg.equals("") ? ipArg.replaceAll("\\*", "%") : null);
        filteredDriverListStmt.setString("activeArg", activeArg);
        //RANGE VALUES (compared using between)
        if (rateAmountFromArg != null) {
            filteredDriverListStmt.setDouble("rateamountFromArg", rateAmountFromArg);
        } else {
            filteredDriverListStmt.setString("rateamountFromArg", null);
        }
        if (rateAmountToArg != null) {
            filteredDriverListStmt.setDouble("rateamountToArg", rateAmountToArg);
        } else {
            filteredDriverListStmt.setString("rateamountToArg", null);
        }
        filteredDriverListStmt.setTimestamp("originaljoindateFromArg",
                                            hireDateFromArg != null ? new Timestamp(hireDateFromArg) : null);
        filteredDriverListStmt.setTimestamp("originaljoindateToArg",
                                            hireDateToArg != null ? new Timestamp(hireDateToArg) : null);
        filteredDriverListStmt.setTimestamp("expirydateFromArg",
                                            fireDateFromArg != null ? new Timestamp(fireDateFromArg) : null);
        filteredDriverListStmt.setTimestamp("expirydateToArg",
                                            fireDateToArg != null ? new Timestamp(fireDateToArg) : null);
        filteredDriverListStmt.setLong("firstArg", first);
        filteredDriverListStmt.setLong("lastArg", last);
        ResultSet driverList = filteredDriverListStmt.executeQuery();
		 */
    	return String.format(getXML("MULTIPLE_DRIVER_WRAPPER"), paged.getTotalCount(), paged.getResult());
    }

    public static void main(String[] args) {
    	try {
    		new ListDrivers().listenForMessages();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
