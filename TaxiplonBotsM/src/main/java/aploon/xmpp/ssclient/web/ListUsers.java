package aploon.xmpp.ssclient.web;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.jivesoftware.smack.packet.IQ;

public class ListUsers extends PagedList {
	
	public ListUsers() {
		super();
	}

	public void processRequest() {
		try {
			Long userid = parseOptionalField("userid", Long.class);
		    Long compid = parseOptionalField("compid", Long.class);
			response = getUserList(compid, userid);
		}
		catch(Exception e) {
			e.printStackTrace();
			response = String.format(getXML("LISTS_FAILURE"), e.getMessage());
		}
	}

	private String getUserList(Long companyIdArg, Long userIdArg) throws Exception {
		PagedResult paged = getPagedResult(getQuery("filteredUserListSelect"), 
										   getQuery("filteredUserListFrom"), 
										   null, 
										   getQuery("filteredUserListFilter")
										   .replaceAll(":companyid", nonStringObjectToString(companyIdArg))
										   .replaceAll(":userid", nonStringObjectToString(userIdArg)), 
										   getQuery("filteredUserListOrder"),
										   new PagedResult() {
			@Override
			protected String processResult(ResultSet rs) throws SQLException {
				String result = "";
				while(rs.next()) {
					//u.userid, u.companyid, u.username, u.level, u.surname, u.name, u.phone, u.status
					Long userid = rs.getLong("userid");
					Long companyid = rs.getLong("companyid");
					String username = rs.getString("username");
					String level = rs.getString("level");
					String surname = rs.getString("surname");
					String name = rs.getString("name");
					String phone = rs.getString("phone");
					String status = rs.getString("status");
					//format the XML message
					result += String.format(getXML("USER_LIST_ITEM"), 
											   userid, companyid, 
											   username != null? username : "", 
											   level != null? level : "", 
											   surname != null? surname : "", 
											   name != null? name : "",
											   phone != null? phone : "", 
											   status) + "\n";
				}
				return result;
			}
		});
		return String.format(getXML("MULTIPLE_USER_WRAPPER"), paged.getTotalCount(), paged.getResult());
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
