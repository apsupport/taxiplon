package aploon.xmpp.ssclient;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.datahelper.DataHelper;
import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.bots.AppointDispatcher;
import aploon.xmpp.ssclient.bots.FareDispatcher;

/**
 * Handles pick-a-call action initiated src the device for trips and appointments. (Is all about driver)
 */
public class PickCall extends ServerSideClient {

    private PreparedStatement storeAppointPickStmt;
    private PreparedStatement getAppointDataStmt;

    private static final Logger LOGGER = Logger.getLogger(PickCall.class);
    
    public PickCall() throws Exception {
    	super();
    }

    private void prepareStoreAppointPickStmt() throws Exception {
    	if (storeAppointPickStmt == null || storeAppointPickStmt.isClosed()) {
    		storeAppointPickStmt = dataPool.prepareStatement(getQuery("storeAppointPick"));
    	}
    }

    public void processRequest() {
    	
    	try {
    		
    		Thread.sleep(getNumberFromProps("sleep_o_pick", 0));
    		
    		Long driverId = Long.parseLong(request.get("driverid"));
    		Long timesent = parseOptionalField("timesent", Long.class);
    		
    		LOGGER.equals("The timesent is "+timesent);
    		
    		Boolean acc = parseOptionalField("acc", Boolean.class);
			
			if(acc==null) acc = true;
			
    		//trip
    		Long tripId = null;
    		try {
    			tripId = Long.parseLong(request.get("tripid"));
    			Long deviceId = Long.parseLong(request.get("deviceid"));
    				
    			if(acc==true){
    				storeCallPickInDb(tripId, deviceId, driverId);
    			} else {
    				LOGGER.info("driver "+driverId+" "+from+" "+deviceId+" declined call");
    			}
    			
    		} catch (Exception e) {
    			//				e.printStackTrace();
    		}
    		Long appId = null;
    		//or appointment
    		try {
    			appId = Long.parseLong(request.get("appid"));
    			String status = Boolean.parseBoolean(request.get("acc")) ? "acc" : "dec";
    			storeAppointmentPickInDb(appId, driverId, status);
    		} catch (Exception e) {
    			//		        e.printStackTrace();
    		}
    		if (tripId == null && appId == null) {
    			throw new RuntimeException("no_id_param");
    		}

    		if(appId!=null&&acc==false) {
    			response = "OK";
    		} else if(timesent!=null||acc==false) {
    			//response = "OK&tripId="+tripId+"&appId="+appId+"&time="+(new Date()).getTime();
    			//LOGGER.info("full response");
    			response = null;
    		} else {
    			response = "OK";
    		}
    		
    	} catch (Exception e) {
    		response = "Failed to pick up the call due to exception: " + e.getMessage();
    	}
    }
    
    private void storeCallPickInDb(Long tripId, Long deviceId, Long driverId) {
    	
    	try {
    		int updatedRows = DataHelper.storeCallPick(tripId, driverId, deviceId);
    		if(updatedRows>0) {
    			
    			LOGGER.info("Rows changed "+driverId+" "+tripId);
    			try {
    				if(FareDispatcher.getInstance().isLastChanceForTripId(tripId)) {
    					FareDispatcher.getInstance().getLastChanceTrips().remove(tripId);
            			FareDispatcher.getInstance().dispatchTripAssignment(tripId);
            		}
    			}catch(Exception e) {
    				LOGGER.error("Error on dispatcher interaction",e);
    			}
    			
    		} else {
    			LOGGER.info("No rows Changed for "+driverId+" "+tripId);
    		}
    		
    	} catch (SQLException e) {
			LOGGER.info("Problem adding call pick ",e);
		}
    	
    }

    private void storeAppointmentPickInDb(Long appId, Long driverId, String status) throws Exception {
    	try {
    		prepareStoreAppointPickStmt();
    		int i = 1;
    		//			storeAppointPickStmt.setLong(i++, driverId);
    		storeAppointPickStmt.setString(i++, status);
    		storeAppointPickStmt.setLong(i++, appId);
    		if (storeAppointPickStmt.executeUpdate() > 0) {
    			try {
    				AppointDispatcher.getInstance().sendAppointInformToPassenger(appId, status,null);
    				//mark informed appointments
    				AppointDispatcher.getInstance().markAppointsAsInformed(appId + "");
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    		dataPool.closeConnection();
    	} catch (Exception e) {
    		e.printStackTrace();
    		throw e;
    	}
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
