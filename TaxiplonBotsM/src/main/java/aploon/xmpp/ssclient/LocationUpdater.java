package aploon.xmpp.ssclient;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONException;

import aploon.concurrency.util.NamingThreadFactory;
import aploon.datahelper.DataHelper;
import aploon.model.Coordinates;
import aploon.model.Zone;
import aploon.ngi.gpsprobe.GpsProbeClient;
import aploon.xmpp.ServerSideClient;

import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;

/*
 * Is about drivers
 */
public class LocationUpdater extends ServerSideClient {

    private CallableStatement updateLocationStmt;
    private NamedParameterStatement fetchDriverDataStmt;
    private GpsProbeClient gpsProbeClient;
    
    private static final Timer informOnRideTimer = new Timer("informonride_scheduler");

    private Map<Long,TripInformation> tripInformations = new HashMap<Long,TripInformation>();
    
    private static final Logger LOGGER = Logger.getLogger(LocationUpdater.class);
    
    private ExecutorService zonesExecutorService = null;
    //private ExecutorService driverStatusService = null;
    
    public LocationUpdater() throws Exception {
    	super();
    	
    	gpsProbeClient = new GpsProbeClient();
    	initializeZoneExecutor();
    	//initializeDriverStatus();
        /*
        LinkedHashMap<String, String> requests = substituteRequests();
        if (requests != null && !requests.isEmpty()) {
            //                                                      System.out.println(getClass().getSimpleName() + " queue for processing: " + requests.size());
            for (Map.Entry<String, String> entry : requests.entrySet()) {
                String from = entry.getKey();
                String message = entry.getValue();
                proceedWithRequest(from, message);
            }
        } else {
        }
        */
        
        asynchronousRequestProcessing = new Thread(new Runnable() {
        	public void run() {
        		while (true) {
        			try {
        				LinkedHashMap<String, String> requests = substituteRequests();
        				if (requests != null && !requests.isEmpty()) {
        					//							System.out.println(getClass().getSimpleName() + " queue for processing: " + requests.size());
        					for (Map.Entry<String, String> entry : requests.entrySet()) {
        						String from = entry.getKey();
        						String message = entry.getValue();
        						proceedWithRequest(from, message);
        					}
        				} else {
        					Thread.sleep(1000);
        				}
        			} catch (Exception e) {
        				e.printStackTrace();
        			}
        		}
        	}
	    });
        asynchronousRequestProcessing.setName("locationupdater_processing_scheduler");
        asynchronousRequestProcessing.start();

        informOnRideTimer.schedule(new InformOnRideTask(), 0,10000);
        
    }

    protected void recordInboundTraffic(Message msg, String from) {
    	//do nothing, don't record stats for this bot
    }

    private void prepareUpdateLocationStmt() throws Exception {
                                                        
        if (updateLocationStmt == null || updateLocationStmt.isClosed()) {
            updateLocationStmt = dataPool.getConnection().prepareCall(getQuery("updateLocation"));
        }
    }

    private void prepareFetchDriverDataStmt() throws Exception {
    
        if (fetchDriverDataStmt == null || fetchDriverDataStmt.isClosed()) {
            fetchDriverDataStmt = new NamedParameterStatement(dataPool, getQuery("fetchDriverData"));
        }
    }

    enum Status {
    	
    	ava,una,occ,sos,off,none;

    	static Status parseStatus(String s) {
    		try {
    			return valueOf(s);
    		} catch (Exception e) {
    			return none;
    		}
    	}
    	
    }

    public void processRequest() {
    	try {
            
    		Long deviceid = Long.parseLong(request.get("deviceid"));
    		Double curlat = Double.parseDouble(request.get("curlat"));
    		Double curlng = Double.parseDouble(request.get("curlng"));
    		Status status = Status.parseStatus(request.get("status"));
    		String version = request.get("ver");
    		Integer driverId = parseOptionalField("driverid", Integer.class);
    		LOGGER.info("Additional info "+request.get("speed")+" "+request.get("altitude")+" "+request.get("heading")+" "+request.get("timestamp")+" "+request.get("satelites"));
    		
    		/*
    		ProbeData probeData = new ProbeData();
    		probeData.setTimestamp(parseOptionalField("timestamp", Long.class));
    		probeData.setSpeed(parseOptionalField("speed", Double.class));
    		probeData.setLatitude(curlat);
            probeData.setLongitude(curlng);
    		probeData.setAltitude(parseOptionalField("altitude", Double.class));
            probeData.setHeading(parseOptionalField("heading", Double.class));
            probeData.setNumberOfSatellites(parseOptionalField("satelites", Integer.class));
            gpsProbeClient.postToProbe(deviceid, probeData);            
    		*/
            
            response = updateLocation(deviceid, curlat, curlng, status, version, driverId);
    		
            if (status == Status.sos) {
    			SOS.getInstance().sendSOS(driverId == null ? null : driverId.longValue(), deviceid, curlat, curlng, null, null, null,null, null, true);
    		}
            
    	} catch (Exception e) {
    		e.printStackTrace();
    		response = e.getMessage();
    	}
    }

    public String updateLocation(Long deviceid, Double curlat, Double curlng, Status status, String version,
			       Integer driverId) throws Exception {
    	//prepareStmts();
    	prepareUpdateLocationStmt();
        int i = 1;
        updateLocationStmt.setDouble(i++, curlat);
        updateLocationStmt.setDouble(i++, curlng);
        updateLocationStmt.setString(i++, status != Status.none ? status.toString() : null);
        updateLocationStmt.setString(i++, version);
        updateLocationStmt.setLong(i++, deviceid);
        updateLocationStmt.registerOutParameter(i++, Types.NUMERIC);
        updateLocationStmt.execute();
	
        LOGGER.info(curlat.toString()+" "+curlng.toString()+" "+status.toString()+" "+version+" "+deviceid.toString());
        
        //if this cab has assigned fares, its new location is forwarded to the passenger app
        Long tripid = updateLocationStmt.getLong("tripidOut");
    
        if(tripid!=null&&tripid!=0) {
            
            TripInformation tripInformation = new TripInformation();
            tripInformation.setTripId(tripid);
            tripInformation.setDeviceId(deviceid);
            tripInformation.setCurLat(curlat);
            tripInformation.setCurLng(curlng);
            tripInformation.setStatus(status);
            tripInformation.setVersion(version);
            tripInformation.setDriverId(driverId);

            tripInformations.put(tripid, tripInformation);
        }

        //update the zone situation (in case this device status has been changed)
        
        if (driverId != null && status != Status.ava &&
        		status != Status.sos) { //driverid has been added only to handle the zones
        	Zones.getInstance().updateDriverZone(null, (long)driverId, false,null,null);
        }
        
        
        if (driverId != null && status != Status.ava &&status != Status.sos) {
        	zonesExecutorService.execute(new ZonesUpdater(DataHelper.getDriverIdFromDevice(deviceid), null, null));	
        } else {
        	zonesExecutorService.execute(new ZonesUpdater(DataHelper.getDriverIdFromDevice(deviceid), curlat, curlng));
        }
        
        
        
        return null;
        		//"OK:"+(new Date().getTime());
    }

    
    /**
     * Get from the properties files the zone_processes
     * or else get the default
     */
    private void initializeZoneExecutor() {
    	
    	String zoneProcesses = Util.getProperty("zone_processes", "processes");
    	
    	if(zoneProcesses!=null) {
    	
    		LOGGER.info("The zoneProcesses are "+zoneProcesses+" ");
    		
    		zonesExecutorService = Executors.newFixedThreadPool(Integer.parseInt(zoneProcesses),new NamingThreadFactory("zonescheck_pool"));
    	} else {
    		
    		LOGGER.info("Will goit to have the default input 4");
    		zonesExecutorService = Executors.newFixedThreadPool(4);
    		
    	}
    	
    }
    
    /**
    private class StatusInformer implements Runnable {
    	
    	private Long deviceId;
    	
    	public StatusInformer(Long deviceId) {
    		this.deviceId = deviceId;
    	}

		@Override
		public void run() {
			
			
			try {
			
				
				String fromImei = DataHelper.getDeviceImei(deviceId);
				
				LOGGER.info("The imei is "+fromImei);
				
				String msg = StatusCheck.getInstance().checkDeviceStatus(fromImei, "", "");
				
				LOGGER.info("Sending the message "+msg+" imei "+fromImei);
				
				//StatusCheck.getInstance().messageToDriver(fromImei, msg);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
    	
    }
    */
    
    private class ZonesUpdater implements Runnable {

    	private Long driverId;
    	private Double curLat;
    	private Double curLng;
    	
    	public ZonesUpdater(Long driverId,Double curLat,Double curLng) {
    		this.driverId = driverId;
    		this.curLat = curLat;
    		this.curLng = curLng;
    	
    		LOGGER.info("The zone updater is "+driverId+" "+curLat+" "+curLng);
    		
    	}
    	
		@Override
		public void run() {
			try {
				checkAndRemoveDriverFromZone(driverId,curLat,curLng);
			} catch (Exception e) {
				LOGGER.error("Problemo updating zone",e);
			}
		}
    	
    };
    
    private class InformOnRideTask extends TimerTask {

        @Override
        public void run() {
            
            try {
                
                Map<Long,TripInformation> copyOfInformation = new HashMap<Long,TripInformation>(tripInformations);
            
                for(Long copyTrip: copyOfInformation.keySet()) {
                    
                    TripInformation tripInformation = copyOfInformation.get(copyTrip);            
                    
                    try {
                    
                        if(tripInformation!=null) {
                            
                            prepareFetchDriverDataStmt();
                    
                            fetchDriverDataStmt.setDouble("lat", tripInformation.getCurLat());
                            fetchDriverDataStmt.setDouble("lng", tripInformation.getCurLng());
                            fetchDriverDataStmt.setLong("tripid", tripInformation.getTripId());
                            ResultSet rs = fetchDriverDataStmt.executeQuery();
                            if (rs.next()) {
                                Long passengerId = rs.getLong("passengerid");
                                String bookagent = rs.getString("bookagent");
                                String assignedDriverPlateno = rs.getString("plateno");
                                String assignedDriverName = rs.getString("firstname");
                                String assignedDriverSurname = rs.getString("lastname");
                                String assignedDriverMobile = rs.getString("mobile");
                                String assignedDriverCarmodel = rs.getString("model");
                                String pickupAddress = rs.getString("pickupaddress");
                                Double distanceToAssignedDriver = rs.getDouble("distance");
                                Long assignedDriverId = rs.getLong("driverid");
                                String assignedDriverImei = rs.getString("imei");
                                
                                
                                //TODO
                                /**
                                if(assignedDriverId!=null&&assignedDriverImei!=null) {
                                	String driverMessage = StatusCheck.getInstance().checkDeviceStatus(
                                			assignedDriverImei,assignedDriverId.toString(),null);
                                	
                                	LOGGER.info("Sending message to driver for status update "+driverMessage);
                                	
                                	comm.sendMessageToDriver(driverMessage, assignedDriverImei);
                                }
                                */
                                
                                LOGGER.info("Inform on rider task "+passengerId+" "+bookagent+" "+assignedDriverPlateno+
                                            " "+assignedDriverName+" "+assignedDriverSurname+" "+assignedDriverMobile
                                            +" "+assignedDriverCarmodel+" "+pickupAddress+" "+distanceToAssignedDriver);
                                
                                if (bookagent == null || (!bookagent.equals("w")) || isMobileCaller()) {
                                    comm.sendMessageToPassenger(String.format(getXML("ASSIGNED_DRIVER_XML"),
                                                                      tripInformation.getDeviceId(),
                                                                      tripInformation.getCurLat(),
                                                                      tripInformation.getCurLng() ,
                                                                      assignedDriverPlateno,
                                                                      assignedDriverName + " " + assignedDriverSurname,
                                                                      assignedDriverMobile,
                                                                      assignedDriverCarmodel,
                                                                      pickupAddress,
                                                                      distanceToAssignedDriver),
                                                        passengerId);
                                } else {
                                    comm.sendMessageToPassenger(String.format(getXML("ASSIGNED_DRIVER_MULTICALL_XML"),
                                                                      tripInformation.getTripId(),
                                                                      tripInformation.getDeviceId(),
                                                                      tripInformation.getCurLat(),
                                                                      tripInformation.getCurLng(),
                                                                      assignedDriverPlateno,
                                                                      assignedDriverName + " " + assignedDriverSurname,
                                                                      assignedDriverMobile,
                                                                      assignedDriverCarmodel,
                                                                      pickupAddress,
                                                                      distanceToAssignedDriver),
                                                        passengerId);
                                }
                                
                                //StatusCheck.getInstance()
                            
                            } 
                            fetchDriverDataStmt.close();
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                    }   
                    tripInformations.remove(copyTrip);
                }
            
            } catch(Exception e) {
                e.printStackTrace();                
            }
            
        }
        
    };
   
    public void checkAndRemoveDriverFromZone(long driverId,Double lat,Double lon) {
    	
    	try {
            Long zoneId = DataHelper.getDriverZoneId(driverId);
            
            LOGGER.info("Getting the driver zone "+zoneId);
            
            if(zoneId!=null) {
            	
            	LOGGER.info("The zoneid is not null "+zoneId);
            	
            	if(!coordinateInsideZone(lat, lon, zoneId)) {
            		try {
            			Zones.getInstance().updateDriverZone(null, driverId, false, null,null);
            		} catch(Exception e) {
            			e.printStackTrace();
            		}
            	}
            }
    	
    	} catch (SQLException e) {
            LOGGER.error("Problem checking and removing driver ",e);
    	}
    	
    }
    
    private boolean coordinateInsideZone(Double lat,Double lon,Long zoneId) {
    	try {
    		Zone zone = DataHelper.getZone(zoneId);
            if(zone.getStrictBorders()){
            	if(zone.getZonePoints()!=null) {
            		List<Coordinates> coordinates = zonePointsToCoordinates(zone.getZonePoints());
            		if(coordinates.size()>0) {
            			LOGGER.info("Yes there are coordinates");
            			return isPointInPolygon(lat, lon, coordinates);
            		}
            	} else if(zone.getZoneRange()!=null) {
            		Double driverDistance = Util.getDistance(lat, lon, zone.getZoneLat(), zone.getZoneLong());
            		LOGGER.info("Getting by distance "+driverDistance+" "+" "+zone.getZoneRange()+" "+lat+" "+
            		lon+" "+zone.getZoneLat()+" "+zone.getZoneLong());
            		return driverDistance<=zone.getZoneRange();            		
            	}
            	
            } else {
            	return true;
            }
             
    	 } catch (SQLException e) {
             LOGGER.error("Problem retrieving zone information ",e);
    	 }
    	 
    	 return false;
    }
    

    
    public List<Coordinates> zonePointsToCoordinates(String zonePoints) {

        List<Coordinates> coordinates = new ArrayList<>();

        try {
                JSONArray jsonArray = new JSONArray(zonePoints);
                if(jsonArray!=null&&jsonArray.length()>0) {
                        coordinates = new ArrayList<>();
                        for(int i=0;i<jsonArray.length();i++) {
                                JSONArray jsonArray2 = jsonArray.getJSONArray(i);
                                Coordinates coordinate = new Coordinates(jsonArray2.getDouble(0), jsonArray2.getDouble(1));
                                coordinates.add(coordinate);
                        }
                }
        } catch (JSONException e) {
                LOGGER.error("Problem parsing",e);
        }

        return coordinates;

    }
    
    private boolean isPointInPolygon(Double driverLat,Double driverLng, List<Coordinates> coordinates) {
        int intersectCount = 0;
        for(int j=0; j<coordinates.size()-1; j++) {
            if( rayCastIntersect(driverLat,driverLng, coordinates.get(j), coordinates.get(j+1)) ) {
                intersectCount++;
            }
        }

        return (intersectCount%2) == 1; // odd = inside, even = outside;
    }
    
    private boolean rayCastIntersect(Double driverLat,Double driverLng, Coordinates vertA, Coordinates vertB) {

        double aY = vertA.getLat();
                    //getLatitude();
        double bY = vertB.getLat();
                    //getLatitude();
        double aX = vertA.getLng();
                    //getLongitude();
        double bX = vertB.getLng();
        //getLongitude();
        double pY = driverLat;
        double pX = driverLng;

        if ( (aY>pY && bY>pY) || (aY<pY && bY<pY) || (aX<pX && bX<pX) ) {
            return false; // a and b can't both be above or below pt.y, and a or b must be east of pt.x
        }

        double m = (aY-bY) / (aX-bX);               // Rise over run
        double bee = (-aX) * m + aY;                // y = mx + b
        double x = (pY - bee) / m;                  // algebra is neat!

        return x > pX;
    }
    
    private class TripInformation {
    
        private Long tripId;
        private Long deviceId;
        private Double curLat;
        private Double curLng;
        private Status status;
        private String version;
        private Integer driverId;

        public void setTripId(Long tripId) {
            this.tripId = tripId;
        }

        public Long getTripId() {
            return tripId;
        }

        public void setDeviceId(Long deviceId) {
            this.deviceId = deviceId;
        }

        public Long getDeviceId() {
            return deviceId;
        }

        public void setCurLat(Double curLat) {
            this.curLat = curLat;
        }

        public Double getCurLat() {
            return curLat;
        }

        public void setCurLng(Double curLng) {
            this.curLng = curLng;
        }

        public Double getCurLng() {
            return curLng;
        }

        public void setStatus(LocationUpdater.Status status) {
            this.status = status;
        }

        public LocationUpdater.Status getStatus() {
            return status;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getVersion() {
            return version;
        }

        public void setDriverId(Integer driverId) {
            this.driverId = driverId;
        }

        public Integer getDriverId() {
            return driverId;
        }
        
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	};

}
