package aploon.xmpp.ssclient;

import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jivesoftware.smack.packet.IQ;


public class PredefinedMessages extends ServerSideClient {

    private PreparedStatement getdriverpredefinedStmt;
    private NamedParameterStatement adddriverPredefinedNStmt;
    private PreparedStatement getdriverHistoryStmt;
    
    private String RESPONSE_XML = "<PredefinedMessages>%s</PredefinedMessages>";
    
    private String PREDEFINED_MESSAGE_XML = "<PrededinedMessage>" +
                                            "<messageid>%d</messageid>" +
                                            "<taxicompanyid>%d</taxicompanyid>" +
                                            "<message>%s</message>"+
                                            "</PrededinedMessage>";
    
    private String PREDEFINED_HISTORY_MESSAGE_XML = "<PredefinedHistoryMessage>" +
                                                    "<timesent>%d</timesent>" +
                                                    "<message>%s</message>" +
                                                    "<company>%s</company>" +
                                                    "</PredefinedHistoryMessage>";
    
    private String STATUS_XML = "<status>%s</status><message>%s</message>";
    
    private static enum Actions {
        list,
        send,
        history       
    }

    private void prepareGetdriverpredefinedStmt() throws Exception {
        
        if(getdriverpredefinedStmt==null||getdriverpredefinedStmt.isClosed()) {
            getdriverpredefinedStmt = dataPool.prepareStatement("SELECT MessageID,TaxiCompanyID,Message,Active FROM predefined WHERE TaxiCompanyID= ANY (SELECT TaxicompanyId FROM drivercompanies WHERE driverid=?) AND Active=true");
        }
    }

    private void prepareAddriverPredefinedStmt() throws Exception {
    
        if(adddriverPredefinedNStmt==null||adddriverPredefinedNStmt.isClosed()) {
            adddriverPredefinedNStmt = new NamedParameterStatement(dataPool,"INSERT INTO predefinedhistory (DriverID,MessageID,TaxiCompanyID) SELECT :driverid,:messageid,:taxicompanyid FROM predefined,drivercompanies WHERE predefined.TaxiCompanyID=:taxicompanyid AND predefined.MessageID=:messageid AND drivercompanies.DriverID=:driverid AND drivercompanies.TaxiCompanyID=:taxicompanyid");
        }
    }

    private void prepareGetdriverHistoryStmt() throws Exception {

        if(getdriverHistoryStmt==null||getdriverHistoryStmt.isClosed()) {
            getdriverHistoryStmt = dataPool.prepareStatement("SELECT TimeSent,Message,ShortName FROM predefinedhistory ph,predefined pr,taxicompany ta WHERE ph.DriverId= ? AND pr.MessageID=ph.MessageID AND ta.TaxicompanyID = ph.TaxicompanyId ORDER BY ph.TimeSent DESC");
        }        
    }

    public void processRequest() {
    
        Actions actions = Actions.valueOf(request.get("status"));
        
        try {
            
            if(actions.equals(Actions.list)) {
                
                long driverId = parseOptionalField("driverid", Long.class);
                listpredifinedmessages(driverId);
                
            } else if(actions.equals(Actions.send)) {
            
                long driverId = parseOptionalField("driverid",Long.class);
                long taxicompanyId = parseOptionalField("companyid", Long.class);
                long messageId = parseOptionalField("messageid", Long.class);
                sendpredifinedMessage(driverId, taxicompanyId, messageId);
                
            } else if(actions.equals(Actions.history)) {
            
                long driverId = parseOptionalField("driverid", Long.class);
                sendmessageHistory(driverId);
                
            } else {
                throw new RuntimeException("Not a valid Action");    
            }
            
        } catch(IllegalArgumentException e) {
            e.printStackTrace();    
            response = String.format(RESPONSE_XML, String.format(STATUS_XML,"false","Invalid parameters"));            
        } catch(RuntimeException e) {
            e.printStackTrace();
            response = String.format(RESPONSE_XML, String.format(STATUS_XML,"false","Invalid action"));
        }    
    }
    
    private void listpredifinedmessages(long driverId) {
        
        if(driverId==0) throw new IllegalArgumentException("Not valid parameters");

        try {
            prepareGetdriverpredefinedStmt();
            getdriverpredefinedStmt.setLong(1, driverId);
            ResultSet resultSet = getdriverpredefinedStmt.executeQuery();
            resultSet.beforeFirst();
            
            StringBuilder stringBuilder = new StringBuilder();

            while(resultSet.next()) {

                String predefinedMessage = String.format(PREDEFINED_MESSAGE_XML, resultSet.getLong(1),resultSet.getLong(2),resultSet.getString(3));
                stringBuilder.append(predefinedMessage);
            }

            response = String.format(RESPONSE_XML, stringBuilder.toString());
                        
            resultSet.close();
            getdriverpredefinedStmt.close();
             
        } catch (Exception e) {
            e.printStackTrace();
            response = String.format(RESPONSE_XML, String.format(STATUS_XML,"false","Problem with your arguements"));
        }
    }
    
    private void  sendpredifinedMessage(long driverId,long taxicompanyId,long messageId) {
        
        if(driverId==0||taxicompanyId==0||messageId==0) throw new IllegalArgumentException("Not valid parameters");

        try {
            prepareAddriverPredefinedStmt();
            adddriverPredefinedNStmt.setLong("driverid", driverId);
            adddriverPredefinedNStmt.setLong("taxicompanyid", taxicompanyId);
            adddriverPredefinedNStmt.setLong("messageid", messageId);
            adddriverPredefinedNStmt.execute();
            adddriverPredefinedNStmt.close();
            
            response = String.format(RESPONSE_XML, String.format(STATUS_XML,"true","Your request was successfull"));
        
        } catch (Exception e) {
            e.printStackTrace();
            response = String.format(RESPONSE_XML, String.format(STATUS_XML,"false","Problem with your arguements"));
        }
    }
    
    private void sendmessageHistory(long driverId) {
        
        if(driverId==0) throw new IllegalArgumentException("Not valid parameters");

        try {
            
            prepareGetdriverHistoryStmt();
            getdriverHistoryStmt.setLong(1, driverId);             
            ResultSet resultSet = getdriverHistoryStmt.executeQuery();
            resultSet.beforeFirst();
            
            StringBuilder stringBuilder = new StringBuilder();
            
            while(resultSet.next()) {
                
                stringBuilder.append(String.format(PREDEFINED_HISTORY_MESSAGE_XML, resultSet.getTimestamp(1).getTime(),resultSet.getString(2),resultSet.getString(3)));
            }
            
            response = String.format(RESPONSE_XML, stringBuilder.toString());
            resultSet.close();
            getdriverHistoryStmt.close();
            
        } catch (Exception e) {
            e.printStackTrace();
            response = String.format(RESPONSE_XML, String.format(STATUS_XML,"false","Problem with your arguements"));
        }
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	} 
    
}
