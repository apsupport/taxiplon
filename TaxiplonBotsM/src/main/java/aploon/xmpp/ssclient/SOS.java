package aploon.xmpp.ssclient;

/*
 * Is sos engine for the driver.
 */

import aploon.model.Coordinates;
import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.jivesoftware.smack.packet.IQ;

public class SOS extends ServerSideClient {

    private NamedParameterStatement findNearbyDriversToHelpStmt;
    private CallableStatement updateDeviceForSOSStmt;

    private static SOS instance;

    public static synchronized SOS getInstance() throws Exception {
	if (instance == null) {
	    instance = new SOS();
	}
	return instance;
    }

    private SOS() throws Exception {
	super();
    }

    private void prepareUpdateDeviceForSOSStmt() throws Exception {
	if (updateDeviceForSOSStmt == null || updateDeviceForSOSStmt.isClosed()) {
	    updateDeviceForSOSStmt = dataPool.getConnection().prepareCall(getQuery("updateDeviceForSOS"));
	}
    }

    private void prepareFindNearbyDriversToHelpStmt() throws Exception {
	if (findNearbyDriversToHelpStmt == null || findNearbyDriversToHelpStmt.isClosed()) {
	    findNearbyDriversToHelpStmt = new NamedParameterStatement(dataPool, getQuery("findNearbyDriversToHelp"));
	}
    }

    public enum Switched {
	on,
	off;
    }

    public void processRequest() {
	try {
	    Long deviceId = Long.parseLong(request.get("deviceid"));
	    Long driverId = Long.parseLong(request.get("driverid"));
	    boolean switched = Switched.on.toString().equals(request.get("switched"));
	    toggleSOS(deviceId, driverId, switched);
	    response = String.format(getXML("SOS_SWITCHED"), "true", null);
	} catch (Exception e) {
	    e.printStackTrace();
	    response = String.format(getXML("SOS_SWITCHED"), "false", e.getMessage());
	}
    }

    public void toggleSOS(Long deviceId, Long driverId, boolean switched) throws Exception {
	prepareUpdateDeviceForSOSStmt();
	//call requestSos(:deviceIdVar, :statusVar, :currentLatOut, :currentLongOut, :plateNumberOut, :driverNameOut, :serverTimeOut)
	int i = 1;
	updateDeviceForSOSStmt.setLong(i++, deviceId);
	updateDeviceForSOSStmt.setLong(i++, driverId);
	updateDeviceForSOSStmt.setString(i++, (switched ? Switched.on : Switched.off).toString());
	updateDeviceForSOSStmt.registerOutParameter(i++, Types.DOUBLE);
	updateDeviceForSOSStmt.registerOutParameter(i++, Types.DOUBLE);
	updateDeviceForSOSStmt.registerOutParameter(i++, Types.VARCHAR);
	updateDeviceForSOSStmt.registerOutParameter(i++, Types.VARCHAR);
	updateDeviceForSOSStmt.registerOutParameter(i++, Types.VARCHAR);
	updateDeviceForSOSStmt.registerOutParameter(i++, Types.TIMESTAMP);
	updateDeviceForSOSStmt.execute();
	Double lat = updateDeviceForSOSStmt.getDouble("currentLatOut");
	Double lng = updateDeviceForSOSStmt.getDouble("currentLongOut");
	String plateno = updateDeviceForSOSStmt.getString("plateNumberOut");
	String driverName = updateDeviceForSOSStmt.getString("driverNameOut");
	String driverPhone = updateDeviceForSOSStmt.getString("driverPhoneOut");
	String driverAddress="";
	try {
	    driverAddress = locator.getAddressByCoordinates(new Coordinates(lat, lng));
	} catch (Exception e) {
	    //not greece , reverse geocoding should be done on device to avoid to many requests
	    //to google
	}
	Long time = updateDeviceForSOSStmt.getTimestamp("serverTimeOut").getTime();
	sendSOS(driverId, deviceId, lat, lng, plateno, driverName, driverPhone, driverAddress, time, switched);
	dataPool.closeConnection();
    }

    public void sendSOS(Long driverId, Long deviceId, Double lat, Double lng, String plateno, String driverName,
			String driverPhone, String driverAddress, Long time, boolean switched) throws Exception {
	String sosRequestedBy =
		   String.format(getXML("SOS_REQUESTED_BY"), deviceId, driverName != null ? driverName : "", //TODO: temporary patch for phone and address integration
		driverPhone != null ? driverPhone : "", plateno != null ? plateno : "",
			      driverAddress != null ? driverAddress.replace('|', ',') : "", time != null ? time.toString() : "", lat, lng,
			      (switched ? Switched.on : Switched.off).toString());
	prepareFindNearbyDriversToHelpStmt();
	findNearbyDriversToHelpStmt.setDouble("sosLat", lat);
	findNearbyDriversToHelpStmt.setDouble("sosLng", lng);
	if (driverId == null) {
	    findNearbyDriversToHelpStmt.setString("drvID", null);
	} else {
	    findNearbyDriversToHelpStmt.setLong("drvID", driverId);
	}

	ResultSet rs = findNearbyDriversToHelpStmt.executeQuery();
	while (rs.next()) {
	    Long deviceIdToRequestHelp = rs.getLong("deviceid");
	    String deviceImeiToRequestHelp = rs.getString("imei");
	    comm.sendMessageToDriver(sosRequestedBy, deviceImeiToRequestHelp);
	}
	rs.close();

    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
