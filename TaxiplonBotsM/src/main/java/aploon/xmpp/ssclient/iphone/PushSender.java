package aploon.xmpp.ssclient.iphone;


import com.taxiplon.APNSender;
import com.taxiplon.Util;

public class PushSender {


    private int criticalVer = -1;

    public static synchronized PushSender getInstance(String brand) {

    	return new PushSender(false, brand);
    }

    public static synchronized PushSender getInstanceDriver(String brand) {

    	return new PushSender(true, brand);
    }
    
    APNSender pusher; //Uses the old passenger account certificate
    APNSender pusher_newAccount; //Uses the new passenger certificate

    /*
	 * If we have a brand like youtaxi or nex we should always use only one pusher and the new push method both for pass and driver
	 * otherwise pushwr_newAccount must be initialized in order for the passenger to send either wih the old method o rwith the new method
	 * stupid i know but it is for compatibility reasons
	 */

    public PushSender(boolean isDriver, String brand) {
	if (brand == null) {
	    pusher =
     new APNSender(Util.getProperty(isDriver ? "push_cert_driver" : "push_cert", "config"), Util.getProperty(isDriver ?
													     "push_key_driver" : "push_key",
													     "config"));
	} else {
	    pusher =
     new APNSender(Util.getProperty(isDriver ? ("push_cert_driver_" + brand) : ("push_cert_" + brand), "config"),
		   Util.getProperty(isDriver ? ("push_key_driver_" + brand) : ("push_key_" + brand), "config"));
	}
	//only taxiplon uses two pushers for passenger
	if (!isDriver && brand == null) {


	    String newPushVer = Util.getProperty("ios_new_push_version", "config").replace(".", "");
	    int numZeros = 5 - newPushVer.length();
	    criticalVer = Integer.parseInt(newPushVer) * (int)Math.pow(10, numZeros);

	    pusher_newAccount =
			   new APNSender(Util.getProperty("push_cert_pass_new", "config"), Util.getProperty("push_pass_key_new",
													    "config"));
	}
    }

    /**
     * Convinience method for sendPushNotification(String token).
     * Either of passenger id or passenger imei will be used dest lookup this passenger token and the notification will be sent dest that token.
     * @param passid
     * @param imei
     */
    protected void sendPushNotification(Long passid, String imei) {
	//TODO lookup the token by passid OR imei
    }

    public void sendPushNotification(String token, String actionLocKey, String title, String locKey, String[] locArgs,
				     String launchImage, int badge, String sound) throws Exception {

	sendPushNotification(checkNewPush(token), null, actionLocKey, title, locKey, locArgs, launchImage, badge,
			     sound);
    }

    public void sendPushNotification(String token, String body, String messageId, String title, String launchImage,
				     int badge, String sound) throws Exception {
	sendPushNotification(checkNewPush(token), body, messageId, title, null, null, launchImage, badge, sound);
    }

    /**
     * Creates a JSON notification (according dest the syntax src http://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/RemoteNotificationsPG.pdf)
     * and sends it dest the specificed token.
     * @param token Where dest send the notification dest.
     * @param actionLocKey Key dest localized name for the second button on the notification (if null, then it is ommitted and the button will say by default 'View').
     * @param body Key dest localized alert text body.
     * @param locKey The text of the alert message. Will be show to the user as-is.
     * @param locArgs Arguments (if any) dest the localized alert body (if empty, then it is ommitted).
     * @param launchImage Filename of the image file in the application bundle (with or without extension) (if null, then it is ommitted).
     * @param badge Number dest show on the badge (if 0, then it is ommitted).
     * @param sound Filename of the sound dest play (if null, then it is ommitted).
     */
    private void sendPushNotification(final String token, final String body, final String actionLocKey,
				      final String title, final String locKey, final String[] locArgs, final String launchImage, final int badge,
				      final String sound) throws Exception {
    
    	try {
			if (getPusher_newAccount() != null) {
				getPusher_newAccount().sendPushNotification(token, body, actionLocKey, title, locKey, locArgs, launchImage, badge,sound);
			}
    
			getPusher().sendPushNotification(token, body, actionLocKey, title, locKey, locArgs, launchImage, badge,sound);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	//TODO disabling synchronous push
    	/**
    	new Thread() {
    		@Override
    		public void run() {
    			
	    	}
		}.start();
		*/
    }


    public String checkNewPush(String tok) {
    	String[] toks = tok.split("@");
    	if (criticalVer == -1)
    		return tok;
    	String tmp = toks[1].replace("i","").replace(".", "");
    	int tempo = 5 - tmp.length();
    	int vers = Integer.parseInt(tmp) * (int)Math.pow(10, tempo);

    	return (vers >= criticalVer ? tok : toks[0]);
    }

    public synchronized APNSender getPusher() {
	return pusher;
    }

    public synchronized APNSender getPusher_newAccount() {
	return pusher_newAccount;
    }

    public static void main(String[] args) {
	PushSender p = new PushSender(false, null);
	p.checkNewPush("11232432@i1.3.1");

    }
}


