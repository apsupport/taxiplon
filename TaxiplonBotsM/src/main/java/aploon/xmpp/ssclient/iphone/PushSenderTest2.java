package aploon.xmpp.ssclient.iphone;

import org.apache.log4j.Logger;

import com.taxiplon.Util;

public class PushSenderTest2 {

	private static final Logger LOGGER = Logger.getLogger(PushSenderTest2.class);
	
	public static void main(String args[]) throws Exception {
		
		LOGGER.info("Testing the second push sender getting instance driver");
	
		PushSender.getInstanceDriver(null).sendPushNotification(
				"8059d20f d2fe3cab 4398a173 c8e46d36 28b4e966 8896264d 1d897daa 7b58e210@i3.2.0",
				"Tralalalala", 
				"DriverAssign", 
				"Trololololo", null, 0, Util.getProperty("push_sound", "config"));
	
		
		while (true);
	
	}
	
}
