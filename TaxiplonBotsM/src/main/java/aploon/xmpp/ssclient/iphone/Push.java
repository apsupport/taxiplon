package aploon.xmpp.ssclient.iphone;


import java.sql.PreparedStatement;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.xmpp.ServerSideClient;


public class Push extends ServerSideClient {

	private static final Logger LOGGER = Logger.getLogger(Push.class);

    public Push() {
    	super();
    }

    PreparedStatement saveTokenStmt;
    PreparedStatement saveTokenDriverStmt;

    protected void prepareSaveTokenStmt(boolean isDriver) throws Exception {
	if (isDriver) {
	    if (saveTokenDriverStmt == null || saveTokenDriverStmt.isClosed()) {
		saveTokenDriverStmt = dataPool.prepareStatement(getQuery("saveTokenDriver"));
	    }
	} else {
	    if (saveTokenStmt == null || saveTokenStmt.isClosed()) {
		saveTokenStmt = dataPool.prepareStatement(getQuery("saveToken"));
	    }
	}
    }

    /**
     * The only request this bot can process is to update the token associated with a passenger account.
     */
    public void processRequest() {
	String failure = null;
	try {
	    String imei = request.get("imei");
	    Long passId = 0L;
	    try {
		passId = Long.parseLong(request.get("passid"));
	    } catch (Exception e) {
		e.printStackTrace();
		throw new RuntimeException("invalid_pass_id");
	    }
	    String token = request.get("token");
	    //ekos this was added in a hurry in order to do it quicly passId=-1 means driver
	    if (passId == -1L) {
		saveDriverToken(token, imei);
	    } else {
		savePassengerToken(token, imei, passId);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    failure = e.getMessage();
	}
	response =
   String.format(getXML("TOKEN_UPDATE"), String.format(getXML("UPDATE_STATUS"), Boolean.toString(failure == null),
						       failure == null ? "" : failure));
    }

    protected void savePassengerToken(String token, String imei, long passId) throws Exception {
    	
    	prepareSaveTokenStmt(false);
    	
    	/**
    	 * saveTokenStmt.setString(1, token);
    	 * saveTokenStmt.setString(2, imei);
    	 * saveTokenStmt.setLong(3, passId);
    	 */
    	
    	saveTokenStmt.setString(1, token);
    	saveTokenStmt.setLong(2, passId);
    	
    	int affectedRows = saveTokenStmt.executeUpdate();
    	saveTokenStmt.close();
    	dataPool.closeConnection();
    	if (affectedRows == 0) {
    		throw new RuntimeException("no_rows_updated");
    	}
    }

    protected void saveDriverToken(String token, String imei) throws Exception {
    	prepareSaveTokenStmt(true);
    	saveTokenDriverStmt.setString(1, token);
    	saveTokenDriverStmt.setString(2, imei);
    	int affectedRows = saveTokenDriverStmt.executeUpdate();
    	saveTokenDriverStmt.close();
    	dataPool.closeConnection();
    	if (affectedRows == 0) {
    		throw new RuntimeException("no_rows_updated");
    	}
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
