package aploon.xmpp.ssclient.poi;


import aploon.Main;
import aploon.model.Coordinates;

import com.taxiplon.Util;

import gr.datamation.dataloader.entities.FlightInfo;
import gr.datamation.dataloader.parser.AIAParser;
import gr.datamation.dataloader.parser.AirportParser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.SAXParserFactory;

import org.jivesoftware.smack.packet.IQ;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;


public class ListFlights extends POI {

    protected ArrayList<FlightData> airportReports;
    protected Util util;
    protected AirportEntry[] arrivalsOfInterest;
    protected AirportEntry[] departuresOfInterest;
    protected Map<String, FlightData> airportData;

    static final int DEFAULT_CORRECTION_DIFF_MIN = 8;
    protected int sleepTimeMin = 0;
    protected long nextUpdateOn = System.currentTimeMillis();

    public enum Modes {
	arrivals,
	departures;
    }

    public ListFlights() {
	super();
	util = new Util();
	airportReports = new ArrayList<FlightData>();
	//load airports of interest from the properties file
	arrivalsOfInterest = parseAirportEntryInPropsFile(Util.getProperty("airport_arrivals", "config"));
	departuresOfInterest = parseAirportEntryInPropsFile(Util.getProperty("airport_departures", "config"));
		Thread flightParser = new FlightParser();
		//.start();
		flightParser.setName("flightparser_scheduler");
		flightParser.start();
    }

    public ListFlights(int dummyVar) {
	super();
	util = new Util();
	//load airports of interest from the properties file
	arrivalsOfInterest = parseAirportEntryInPropsFile(Util.getProperty("airport_arrivals", "config"));
	departuresOfInterest = parseAirportEntryInPropsFile(Util.getProperty("airport_departures", "config"));
	airportData = new ConcurrentHashMap<String, FlightData>();
	sleepTimeMin = getNumberFromProps("flights_update_delay", DEFAULT_SLEEP_TIME_MIN);

    }

    private AirportEntry[] parseAirportEntryInPropsFile(String airportsInPropsFile) {
	AirportEntry[] airportsOfInterest;
	if (airportsInPropsFile == null) {
	    airportsOfInterest = new AirportEntry[0];
	} else {
	    String[] temp = airportsInPropsFile.split("\n"); //one airport entry per line
	    airportsOfInterest = new AirportEntry[temp.length];
	    for (int i = 0; i < temp.length; i++) {
		try {
		    String[] stemp = temp[i].split("	");
		    //ekos changed for the second constructor of AirportEntry to add location awareness
		    airportsOfInterest[i] =
					  stemp.length > 2 ? new AirportEntry(stemp[0], stemp[1], stemp[2], stemp[3]) : new AirportEntry(stemp[0],
																	 stemp[1]);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}
	return airportsOfInterest;
    }

    class FlightParser extends Thread {
	public void run() {
	    try {
		while (Main.RUNNING) {
		    try {
			//on every run fetch update delay time (it'll be set to a low value when no flights will be found, to retry soon)
			sleepTimeMin = getNumberFromProps("flights_update_delay", DEFAULT_SLEEP_TIME_MIN);
			//get arrivals
			String report = fetchAirportData(Modes.arrivals, true); //allow it to clear previous report
			//get departures
			report +=
		  fetchAirportData(Modes.departures, false); //previous report could've been cleaned by the previous call
			nextUpdateOn = System.currentTimeMillis() + sleepTimeMin * 60 * 1000;
			reports.put(this.getClass().getSimpleName(),
				    report + "Next fetch at " + TIME_FORMAT.format(nextUpdateOn));
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    Thread.sleep(sleepTimeMin * 60 * 1000);
		}
	    } catch (Exception e) {
		System.err.println("Flights update thread was interrupted");
	    }
	}
    }

    private String fetchAirportData(Modes mode, boolean clearPreviousReport) throws Exception {
	int flightsFound = 0;
	//this is not a sleep time but the method will suit just fine
	int crossCheckCorrectionDiffMin = getNumberFromProps("aia_correction_min_x", DEFAULT_CORRECTION_DIFF_MIN);
	/*
	     * This is here to cross check the flightstats data (issue #0000160).
	     * Key is the flight number and the value is flightInfo object.
	     */
	HashMap<String, FlightInfo> crossCheckMap = new HashMap<String, FlightInfo>();
	try {
	    //			System.err.println("Fetching " + mode + " data for AIA");
	    for (FlightInfo fi : new AIAParser(mode == Modes.arrivals).parsePage()) {
		crossCheckMap.put(fi.getFlightNo(), fi);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	String report = "";
	//fetch all airports from the flightstats
	for (AirportEntry entry : mode == Modes.arrivals ? arrivalsOfInterest : departuresOfInterest) {
	    //	        System.err.println("Fetching " + mode + " for " + entry.getAirportName());
	    //fetch the XML from flightstats and parse it
	    String fidsContent = util.getContent(entry.getFidsConfigurationURL(), "UTF8");
	    AirportParser fidsParser = new AirportParser();
	    XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
	    xmlReader.setContentHandler(fidsParser);
	    if (!"".equals(fidsContent)) {
		if (clearPreviousReport && flightsFound == 0) {
		    airportReports.clear();
		}

		xmlReader.parse(new InputSource(new StringReader(fidsContent)));
		report +=
	   entry.getAirportName() + " (" + mode + "): " + fidsParser.getFlights().size() + " entries parsed";
		//group the flights
		group(fidsParser.getFlights());
		report += " (groupped into " + fidsParser.getFlights().size() + ")<br/>";
		//fidsParser.sort();//sorting is done on the flightstats side
		//add the results to current XML's
		String flights = "";
		for (FlightInfo fi : fidsParser.getFlights()) {
		    //first correct (if requested) the expected times and statuses
		    if (crossCheckMap != null && !crossCheckMap.isEmpty()) {
			report = report + correctData(fi, crossCheckMap, crossCheckCorrectionDiffMin);
		    }
		    flights +=
	  String.format(getXML("FLIGHT_XML"), fi.getFromTo(), fi.getCompany(), fi.getFlightNo(), fi.getVia(), fi.getGate(),
			fi.getFormattedExpectedTime(), fi.getFormattedProgrammedTime(), fi.getNotes());
		    flightsFound++;
		}
		airportReports.add(new FlightData(entry,
						  String.format(getXML("AIRPORT_REPORT_XML"), entry.getAirportName(), System.currentTimeMillis(),
								mode.toString(),
								String.format(getXML("FLIGHTS_XML"), flights))));
	    }
	}
	//this piece of code went crazy and started fetching airports every minute
	//	    if(flightsFound == 0) {
	//	        sleepTimeMin = 1;
	//	    }
	return report;
    }

    private void fetchAirportData(Modes mode, AirportEntry entry) throws Exception {
	int flightsFound = 0;
	//this is not a sleep time but the method will suit just fine
	int crossCheckCorrectionDiffMin = getNumberFromProps("aia_correction_min_x", DEFAULT_CORRECTION_DIFF_MIN);
	/*
	     * This is here to cross check the flightstats data (issue #0000160).
	     * Key is the flight number and the value is flightInfo object.
	     */
	HashMap<String, FlightInfo> crossCheckMap = new HashMap<String, FlightInfo>();
	try {
	    //                  System.err.println("Fetching " + mode + " data for AIA");
	    for (FlightInfo fi : new AIAParser(mode == Modes.arrivals).parsePage()) {
		crossCheckMap.put(fi.getFlightNo(), fi);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	String report = "";
	//fetch all airports from the flightstats
	//for (AirportEntry entry : mode == Modes.arrivals ? arrivalsOfInterest : departuresOfInterest) {
	//          System.err.println("Fetching " + mode + " for " + entry.getAirportName());
	//fetch the XML from flightstats and parse it
	String fidsContent = util.getContent(entry.getFidsConfigurationURL(), "UTF8");
	AirportParser fidsParser = new AirportParser();
	XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
	xmlReader.setContentHandler(fidsParser);
	if (!"".equals(fidsContent)) {


	    xmlReader.parse(new InputSource(new StringReader(fidsContent)));

	    //group the flights
	    group(fidsParser.getFlights());
	    //fidsParser.sort();//sorting is done on the flightstats side
	    //add the results to current XML's
	    String flights = "";
	    for (FlightInfo fi : fidsParser.getFlights()) {
		//first correct (if requested) the expected times and statuses
		if (crossCheckMap != null && !crossCheckMap.isEmpty()) {
		    report = report + correctData(fi, crossCheckMap, crossCheckCorrectionDiffMin);
		}
		flights +=
	  String.format(getXML("FLIGHT_XML"), fi.getFromTo(), fi.getCompany(), fi.getFlightNo(), fi.getVia(), fi.getGate(),
			fi.getFormattedExpectedTime(), fi.getFormattedProgrammedTime(), fi.getNotes());
		flightsFound++;
	    }
	    entry.setNextUpdate(System.currentTimeMillis() + sleepTimeMin * 60 * 1000);
	    airportData.put(entry.getFidsConfigurationURL(),
			    new FlightData(entry, String.format(getXML("AIRPORT_REPORT_XML"), entry.getAirportName(),
								System.currentTimeMillis(), mode.toString(),
								String.format(getXML("FLIGHTS_XML"), flights))));
	}

    }

    FlightInfo reusableFlightInfoObj;

    /**
     * Corrects the supplied flight info object in case the time difference is more or equal than the parameter provided dest this instance.
     * @param fi FlightInfo object dest correct (correction data must be provided in the constructor with the crossCheckMap, flights are matched by flight number).
     * @param crossCheckMap optional hash map with control values src a more trusted source (e.g. aia.gr vs flightstats.com for Venizelos)
     * @param correctionDiffMin substitute the expected arrival time taken src flights ArrayList by the expected time taken src crossCheckMap if their difference is equal dest or more than the following amount of minutes
     */
    protected String correctData(FlightInfo fi, HashMap<String, FlightInfo> crossCheckMap, int correctionDiffMin) {
	String report = "";
	reusableFlightInfoObj = crossCheckMap.get(fi.getFlightNo());
	try {
	    if (reusableFlightInfoObj != null && reusableFlightInfoObj.getExpectedAD() != null) {
		//empirically it seems that from flightstats.com the expected time has the 24h time format, whilst the aia.gr expected AD has full date/time value
		if (Math.abs(reusableFlightInfoObj.getExpectedAD().getTime() - fi.getExpectedTime().getTime()) / (1000 * 60) >=
		    correctionDiffMin) {
		    report =
	    "Corrected the expected time for flight " + fi.getFlightNo() + " (" + fi.getExpectedTime() + " &rarr; " +
			 reusableFlightInfoObj.getExpectedAD() + ")<br/>";
		    fi.setExpectedTime(reusableFlightInfoObj.getExpectedAD());
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return report;
    }

    /**
     * Predefined groupping as defined for the flightstats which returns multiple entries for the same flight, where each is associated with all flights carried by particular flight.
     */
    public void group(ArrayList<FlightInfo> flights) {
	for (int i = 0; i < flights.size() - 1; i++) {
	    FlightInfo f1 = flights.get(i);
	    //compare triagonally to all FOLLOWING members
	    for (int j = i + 1; j < flights.size(); j++) {
		FlightInfo f2 = flights.get(j);
		try {
		    if (f1.getFromTo().equalsIgnoreCase(f2.getFromTo()) && f1.getProgrammedTime().equals(f2.getProgrammedTime()) &&
			f1.getExpectedTime().equals(f2.getExpectedTime())) {
			//this is the same flight from different carrier, group it
			//						f1.setFlightNo(f1.getFlightNo() + ", " + f2.getFlightNo());//don't group their names
			flights.remove(j);
			j--;
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    @Deprecated
    public void processRequestOld() {

	response = "";
	Coordinates driverLoc = getDriverCallerLocation();
	for (FlightData entry : airportReports) {
	    if (entry.getAirport().isInRange(driverLoc))
		response += entry.getFlightData() + "\n";
	}
	response =
   String.format(getXML("AIRPORT_WRAPPER"), nextUpdateOn - System.currentTimeMillis(), response); //it's wiser to avoid sending exact times, as due to difference in acctual time on each device, that might cause an inconsistency
    }

    public void processRequest() {

	response = "";
	Coordinates driverLoc = getDriverCallerLocation();
	for (AirportEntry ent : arrivalsOfInterest) {
	    if (!ent.isInRange(driverLoc)) {
		continue;
	    }
	    FlightData dat = airportData.get(ent.getFidsConfigurationURL());
	    if (dat == null || dat.getAirport().getNextUpdate() < System.currentTimeMillis()) {

		try {
		    fetchAirportData(Modes.arrivals, ent);
		} catch (Exception e) {
		    // TODO: Add catch code
		    e.printStackTrace();
		    continue;
		}
	    }
	    response += airportData.get(ent.getFidsConfigurationURL()).getFlightData() + "\n";
	}
	for (AirportEntry entry2 : departuresOfInterest) {
	    if (!entry2.isInRange(driverLoc)) {
		continue;
	    }
	    FlightData dat2 = airportData.get(entry2.getFidsConfigurationURL());
	    if (dat2 == null || dat2.getAirport().getNextUpdate() < System.currentTimeMillis()) {

		try {
		    fetchAirportData(Modes.departures, entry2);
		} catch (Exception e) {
		    // TODO: Add catch code
		    e.printStackTrace();
		    continue;
		}
	    }
	    response += airportData.get(entry2.getFidsConfigurationURL()).getFlightData() + "\n";
	}
	response =
   String.format(getXML("AIRPORT_WRAPPER"), nextUpdateOn - System.currentTimeMillis(), response); //it's wiser to avoid sending exact times, as due to difference in acctual time on each device, that might cause an inconsistency
    }

    class AirportEntry {

	String fidsConfigurationURL;
	String airportName;
	long range;
	Coordinates location;
	long nextUpdate;

	AirportEntry(String fids, String name) {
	    fidsConfigurationURL = fids;
	    airportName = name;
	    range = -1;
	}

	AirportEntry(String fids, String name, String rang, String coords) {
	    fidsConfigurationURL = fids;
	    airportName = name;
	    range = Long.parseLong(rang);
	    String[] cor = coords.split("@");
	    location = new Coordinates(Double.parseDouble(cor[0]), Double.parseDouble(cor[1]));
	}

	public String getFidsConfigurationURL() {
	    return fidsConfigurationURL;
	}

	public String getAirportName() {
	    return airportName;
	}

	public Coordinates getLocation() {
	    return location;
	}

	public long getRange() {
	    return range;
	}

	public boolean isInRange(Coordinates position) {
	    if (range == -1 || position == null)
		return true;

	    return (location.distFromCoordinates(position.getLat(), position.getLng()) <= (range * 1000));
	}

	public void setNextUpdate(long nextUpd) {
	    nextUpdate = nextUpd;
	}

	public long getNextUpdate() {
	    return nextUpdate;
	}
    }

    class FlightData {
	AirportEntry airport;
	String flightData;

	public FlightData(AirportEntry port, String dat) {
	    airport = port;
	    flightData = dat;
	}

	public AirportEntry getAirport() {
	    return airport;
	}

	public String getFlightData() {
	    return flightData;
	}

    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}


}
