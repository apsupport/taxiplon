package aploon.xmpp.ssclient.poi;


import aploon.Main;
import aploon.model.Coordinates;
import aploon.xmpp.ssclient.poi.ListFlights.AirportEntry;

import com.taxiplon.Util;

import gr.datamation.dataloader.entities.ShipInfo;
import gr.datamation.dataloader.parser.MarineTrafficParser;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.packet.IQ;


public class ListBoats extends POI {

    private ArrayList<PortData> boatReports;
    private PortEntry[] portsOfInterest;
    int sleepTimeMin = 0;

    public ListBoats() {
    	super();
    	boatReports = new ArrayList<PortData>();
    	//load ports of interest from the properties file
    	String portsInPropsFile = Util.getProperty("ports", "config");
    	portsOfInterest = parsePortEntryInPropsFile(portsInPropsFile);
    	sleepTimeMin = getNumberFromProps("boats_update_delay", DEFAULT_SLEEP_TIME_MIN);
    	//new BoatParser().start();
    	
    	Thread boatParser = new BoatParser();
    	boatParser.setName("boatParser_scheduler");
    	boatParser.start();
    }

    private PortEntry[] parsePortEntryInPropsFile(String portsInPropsFile) {
    	PortEntry[] ports;
    	if (portsInPropsFile == null) {
    		ports = new PortEntry[0];
    	} else {
    		String[] temp = portsInPropsFile.split("\n"); //one port entry per line
    		ports = new PortEntry[temp.length];
    		for (int i = 0; i < temp.length; i++) {
    			try {
    				String[] stemp = temp[i].split("	");
    				ports[i] = stemp.length > 1 ? new PortEntry(stemp[0], stemp[1], stemp[2]) : new PortEntry(stemp[0]);
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	
    	return ports;
    }

    class BoatParser extends Thread {
	public void run() {
	    try {
		while (Main.RUNNING) {
		    String report = "";
		    try {
		    	boatReports.clear();
		    	
		    	for (PortEntry port : portsOfInterest) {
			    
		    		MarineTrafficParser parser = new MarineTrafficParser(port.getPort());
		    		List<ShipInfo> list = parser.getEstimatedArrivals();
		    				//getArrivals();
		    		String boats = "";
			   
		    		for (ShipInfo bi : list) {
		    			boats += String.format(getXML("BOAT_XML"), bi.getVesselName(), 
		    					bi.getArrival() == null ? "" : TIME_FORMAT.format(bi.getArrival()),
		    					bi.getEstimatedArrival() == null ? "" : TIME_FORMAT.format(bi.getEstimatedArrival()),
		    							bi.getProgrammedArrival() == null ? "" :TIME_FORMAT.format(bi.getProgrammedArrival()));
		    		}
			    
		    		boatReports.add(new PortData(port,String.format(getXML("PORT_REPORT_XML"),
		    				port.getPort(), System.currentTimeMillis(),String.format(getXML("BOATS_XML"), boats))));
		    				report += port.getPort() + ": " + list.size() + " entries parsed<br/>";
				}
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		    
		    reports.put(this.getClass().getSimpleName(),
		    		report + "Next fetch at " + TIME_FORMAT.format(new Timestamp(System.currentTimeMillis() +
											     sleepTimeMin * 60 * 1000)));
		    Thread.sleep(sleepTimeMin * 60 * 1000);
		}
	    } catch (Exception e) {
		System.err.println("Boats update thread was interrupted");
	    }
	}
    }

    public void processRequest() {
	response = "";
	Coordinates driverLoc = getDriverCallerLocation();
	for (PortData entry : boatReports) {
	    if (entry.getPort().isInRange(driverLoc)) {
		response += entry.getPortData() + "\n";
	    }
	}
	response = String.format(getXML("PORT_WRAPPER"), response);
    }

    class PortEntry {

	String portName;
	long range;
	Coordinates location;

	PortEntry(String name) {
	    portName = name;
	    range = -1;
	}

	PortEntry(String name, String rang, String coords) {
	    portName = name;
	    range = Long.parseLong(rang);
	    String[] cor = coords.split("@");
	    location = new Coordinates(Double.parseDouble(cor[0]), Double.parseDouble(cor[1]));
	}


	public String getPort() {
	    return portName;
	}

	public Coordinates getLocation() {
	    return location;
	}

	public long getRange() {
	    return range;
	}

	public boolean isInRange(Coordinates position) {
	    if (range == -1 || position == null)
		return true;

	    return (location.distFromCoordinates(position.getLat(), position.getLng()) <= (range * 1000));
	}
    }

    class PortData {
    	
    	PortEntry boatport;
		String portData;

		public PortData(PortEntry port, String dat) {
			boatport = port;
			portData = dat;
		}

		public PortEntry getPort() {
			return boatport;
		}

		public String getPortData() {
			return portData;
		}

    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}

}
