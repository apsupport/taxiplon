package aploon.xmpp.ssclient.poi;

import aploon.Main;
import aploon.model.TripData;
import aploon.xmpp.ServerSideClient;

import com.taxiplon.Util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.jivesoftware.smack.packet.IQ;

public class TripSheet extends ServerSideClient {

    protected ArrayList<TripData> tripData;
    protected Util util;
    static final int DEFAULT_CORRECTION_DIFF_MIN = 8;
    protected int sleepTimeMin = 0;
    protected long nextUpdateOn = System.currentTimeMillis();

    private PreparedStatement findLastTripsStm;
    private PreparedStatement findDriverLastTripsStm;

    public TripSheet() {
        super();
    }

    @Override
    public void processRequest() {

        try {
            response = listTripData();
        } catch (Exception e) {
            response = String.format(getXML("MAINTENANCE"), "It's a todo");
        }
    }
    
    private void preparefetchTripDataStatement() throws Exception {
        
        if(findLastTripsStm ==null|findLastTripsStm.isClosed()) {
            findLastTripsStm = dataPool.getConnection().prepareCall(getQuery("blockDriver"));
        }
    }
    
    private String listTripData() throws Exception {
    
        String result = "";
    
        preparefetchTripDataStatement();
        findLastTripsStm.setInt(1, 10);
        
        ResultSet resultSet = findLastTripsStm.executeQuery();
        resultSet.close();
        
        result = "<Port>\\n\\\n" + 
        "	<name>Something</name>\\n\\\n" + 
        "	<time>A time</time>\\n\\\n" + 
        "	\\n\\\n" + 
        "</Port>";
    
        return result;
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
