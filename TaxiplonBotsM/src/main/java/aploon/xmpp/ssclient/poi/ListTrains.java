package aploon.xmpp.ssclient.poi;


import aploon.Main;

import com.taxiplon.Util;

import gr.datamation.dataloader.entities.TrainInfo;
import gr.datamation.dataloader.parser.RailroadStationParser;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.packet.IQ;


public class ListTrains extends POI {

	private ArrayList<String> trainReports;
	private String[] rrstationsOfInterest;
	int sleepTimeMin = 0;
	
	public ListTrains() {
		super();
		trainReports = new ArrayList<String>();
		//load railroad stations of interest from the properties file
		String rrstationsInPropsFile = Util.getProperty("rrstations", "config");
		rrstationsOfInterest = rrstationsInPropsFile != null? rrstationsInPropsFile.split("\n") : new String[0];
	    sleepTimeMin = getNumberFromProps("trains_update_delay", DEFAULT_SLEEP_TIME_MIN);
	    Thread trainParser = new TrainParser();
	    trainParser.setName("trainparser_scheduler");
	    trainParser.start();
	    //.start();
	}
	
	class TrainParser extends Thread {
	    public void run() {
	        try {
	            while(Main.RUNNING) {
					String report = "";
	                try {
	                    trainReports.clear();
	                    for(String rrstation : rrstationsOfInterest) {
							try {//try to find the class requested
								RailroadStationParser parser = (RailroadStationParser) Class.forName(rrstation).newInstance();
								List<TrainInfo> list = parser.parsePage();
								String trains = "";
								for(TrainInfo ti : list) {
									trains += String.format(getXML("TRAIN_XML"), ti.getFrom(), ti.getTrainCode(), 
															ti.getArrivalDateTime() == null? "" : TIME_FORMAT.format(ti.getArrivalDateTime().getTime()));
								}
								trainReports.add(String.format(getXML("RRSTATION_REPORT_XML"), parser.getName(), System.currentTimeMillis(), String.format(getXML("TRAINS_XML"), trains)));
								report += rrstation + ": " + list.size() + " entries parsed<br/>";
							}
							catch(ClassNotFoundException e) {
								e.printStackTrace();
							}
	                    }
	                }
	                catch(Exception e) {
	                    e.printStackTrace();
	                }
					reports.put(this.getClass().getSimpleName(), report + 
																 "Next fetch at " + TIME_FORMAT.format(new Timestamp(System.currentTimeMillis() + sleepTimeMin * 60 * 1000)));
	                Thread.sleep(sleepTimeMin * 60 * 1000);
	            }
	        }
	        catch(Exception e) {
	            System.err.println("Trains update thread was interrupted");
	        }
	    }
	}

	public void processRequest() {
		response = "";
		for(String entry : trainReports) {
			response += entry + "\n";
		}
		response = String.format(getXML("RRSTATION_WRAPPER"), response);
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
