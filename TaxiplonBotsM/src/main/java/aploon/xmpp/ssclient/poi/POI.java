package aploon.xmpp.ssclient.poi;

import aploon.model.Coordinates;

import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;

import java.util.HashMap;
import java.util.Map;

public abstract class POI extends ServerSideClient {

	static final int DEFAULT_SLEEP_TIME_MIN = 60;
	private NamedParameterStatement findDriverLocationFromCaller;

	public POI() {
		super();
	}

	private void PreparefindDriverLocationCaller() throws Exception {
		if (findDriverLocationFromCaller == null || findDriverLocationFromCaller.isClosed()) {
			findDriverLocationFromCaller =
							 new NamedParameterStatement(dataPool, getQuery("findDriverLocationFromCaller"));
		}
	}

	protected Coordinates getDriverCallerLocation() {
		try {
			Double lat = parseOptionalField("lat", Double.class);
			Double lon = parseOptionalField("lng", Double.class);
			if (lat == null || lon == null) {
				lat = 0.0;
				lon = 0.0;
				PreparefindDriverLocationCaller();
				findDriverLocationFromCaller.setString("caller", caller);
				ResultSet rs = findDriverLocationFromCaller.executeQuery();
				while (rs.next()) {
					lat = rs.getDouble("CurrentLat");
					lon = rs.getDouble("CurrentLong");
				}
				rs.close();
			}
			return new Coordinates(lat, lon);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
