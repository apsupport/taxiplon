package aploon.xmpp.ssclient.poi;


import aploon.xmpp.ssclient.Hotspots;
import aploon.xmpp.ssclient.bots.LiveDispatcher;

import com.taxiplon.db.NamedParameterStatement;

import java.sql.ResultSet;
import java.sql.Timestamp;

import org.jivesoftware.smack.packet.IQ;


public class ListHotspots extends POI {
	
	private static ListHotspots instance;
        private int max_hotspot_radious=50;

	public static synchronized ListHotspots getInstance() throws Exception {
		if(instance == null) {
			instance = new ListHotspots();
	
		}
		return instance;
	}
	
	private NamedParameterStatement findHotspotsInAreaForTimeStmt;
	
	private ListHotspots() throws Exception {
		super();
		max_hotspot_radious = getNumberFromProps("max_hotspot_radious_km", 50);
	}
	
	private void prepareFindHotspotsInAreaForTimeStmt() throws Exception {
		if(findHotspotsInAreaForTimeStmt == null || findHotspotsInAreaForTimeStmt.isClosed()) {
		    findHotspotsInAreaForTimeStmt = new NamedParameterStatement(dataPool, getQuery("findHotspotsInAreaForTime"));
		}
	}

	public void processRequest() {
		String failure = null;
		try {
		    Long time = Long.parseLong(request.get("time"));
		    String hasTo = request.get("to");//hasTo indicates something, nobody knows exactly... (msgs interface spec says "timestamp of the latest time of interest")
			String includeLiveData = request.get("live");
			Float lat = parseOptionalField("lat", Float.class);
		    Float lng = parseOptionalField("lng", Float.class);
		    response = String.format(getXML("HOTSPOTS_WRAPPER_XML"), 
									 getHotspots(time, hasTo, 
												 includeLiveData != null && includeLiveData.equals("true"),
												 lat, lng));
		}
		catch(Exception e) {
			failure = e.getMessage();
		}
		if(failure != null) {
			response = String.format(getXML("LISTS_FAILURE"), failure);
		}
	}
	
	public String getHotspots(long time, String hasTo) throws Exception {
		return getHotspots(time, hasTo, false, null, null);
	}
	
	public String getHotspots(long time, String hasTo, boolean includeLiveData, Float lat, Float lng) throws Exception {
	    prepareFindHotspotsInAreaForTimeStmt();
		String results = "";
	    findHotspotsInAreaForTimeStmt.setTimestamp("time", new Timestamp(time));
	    findHotspotsInAreaForTimeStmt.setString("hasto", hasTo);
	    findHotspotsInAreaForTimeStmt.setString("lat", (lat==null?null:lat+"")) ;
	    findHotspotsInAreaForTimeStmt.setString("lng", (lng==null?null:lng+"")) ;
	    findHotspotsInAreaForTimeStmt.setInt("rangeKM", max_hotspot_radious) ;
//		findHotspotsInAreaForTimeStmt.setInt("maxFetch", getNumberFromProps("max_hotspots_to_fetch", LiveDispatcher.DEFAULT_NODES_TO_KEEP));
		ResultSet rs = findHotspotsInAreaForTimeStmt.executeQuery();
		while(rs.next()) {
			Long eventid = rs.getLong("eventid");
			String description = rs.getString("description");
			Timestamp startingtime = rs.getTimestamp("startingtime");
		    Timestamp endingtime = rs.getTimestamp("endingtime");
			String datesVerbatim = rs.getString("datesverbatim");
			Double hotspotlat = rs.getDouble("hotspotlat");
		    Double hotspotlong = rs.getDouble("hotspotlong");
			String type = rs.getString("type");
			String address = rs.getString("address");
			String extra = rs.getString("notes");
			results += String.format(getXML("EVENT_XML"), eventid, normalizeTextForXML(description), 
									 Long.toString(startingtime.getTime()), 
									 endingtime != null? Long.toString(endingtime.getTime()) : "", 
									 datesVerbatim != null? datesVerbatim : "", 
									 hotspotlat, hotspotlong, type, 
									 address != null? normalizeTextForXML(address) : "", 
									 extra != null? extra : "",
									 System.currentTimeMillis() - startingtime.getTime());
		}
		rs.close();
		dataPool.closeConnection();
		//embed into the resulting XML the live data (pharmacies and hospitals)
		if(includeLiveData) {
		    results += LiveDispatcher.getInstance().fetchLivePoiXML(Hotspots.EventTypes.hospital, lat, lng);
			results += LiveDispatcher.getInstance().fetchLivePoiXML(Hotspots.EventTypes.pharmacy, lat, lng);
		}
		return results;
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
