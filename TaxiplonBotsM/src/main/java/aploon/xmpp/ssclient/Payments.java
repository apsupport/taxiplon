package aploon.xmpp.ssclient;


import aploon.xmpp.ServerSideClient;

import com.taxiplon.Util;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;

import org.jivesoftware.smack.packet.IQ;

/*
 * payments engine
 */
public class Payments extends ServerSideClient {
    private CallableStatement updateTripTransaction;
    private PreparedStatement fetchTripData;
    private PreparedStatement getDriverTripHistoryStmt;
    private PreparedStatement getDriversDeviceStmt;

    private void prepareFetchTripData() throws Exception {
	if (fetchTripData == null || fetchTripData.isClosed()) {
	    //String query =String.format("SELECT %s FROM %s WHERE %s AND %s" , getQuery("getPassTripHistorySelectPayments"),getQuery("getPassTripHistoryFromPayments"),
	    //			    getQuery("getPassTripHistoryJoinPayments"), getQuery("getPassTripHistoryFilterPayments"));
	    fetchTripData = dataPool.prepareStatement(getQuery("getPassTripHistory"));
	}
    }

    private void prepareGetDriverTripHistoryStmt() throws Exception {
	if (getDriverTripHistoryStmt == null || getDriverTripHistoryStmt.isClosed()) {
	    getDriverTripHistoryStmt = dataPool.prepareStatement(getQuery("getDriverTripHistory"));
	}
    }

    private void prepareUpdateTripTransaction() throws Exception {
	if (updateTripTransaction == null || updateTripTransaction.isClosed()) {
	    updateTripTransaction = dataPool.getConnection().prepareCall(getQuery("updateTripPayment"));
	}
    }


    private void prepareGetDriversDeviceStmt() throws Exception {
	if (getDriversDeviceStmt == null || getDriversDeviceStmt.isClosed()) {
	    getDriversDeviceStmt = dataPool.prepareStatement(getQuery("getDriversDevice"));
	}
    }

    public Payments() throws Exception {
	super();
    }

    public enum PayActions {
	notifTrip,
	notifAppoint,
	payTrip,
	payAppoint,
	cancel;
    }

    public void processRequest() {
	String tmp = request.get("op");
	Long givenId = 0L, driverId = 0L, companyId = 0L, passId = 0L,company=0L;
	try {
	    PayActions operation = null;
	    Double amount = 0.0, tipAmount = 0.0;
	    String transaction = "", paypal = "", merchant = "", currency = "", channel = "";
	    try {
		operation = PayActions.valueOf(tmp);
	    } catch (Exception e) {
		throw new RuntimeException("PayAction " + tmp + " not supported");
	    }
	    amount = parseOptionalField("amount", Double.class);
	    givenId = parseOptionalField("id",Long.class);
	    transaction = parseOptionalField("transaction", String.class);
	    paypal = parseOptionalField("account", String.class);
	    merchant = parseOptionalField("merchant", String.class);
	    driverId = parseOptionalField("driverid", Long.class);
	    currency = parseOptionalField("currency", String.class);
	    channel = parseOptionalField("channel", String.class);
	    tipAmount = parseOptionalField("tip", Double.class);
	    company=parseOptionalField("compid",Long.class);
	    response =
   handlePayment(givenId, operation, amount, transaction, driverId, paypal, merchant, currency, channel,
		 tipAmount,company);
	} catch (Exception e) {
	    e.printStackTrace();
	    response =
   String.format(getXML("PAYPAL_PAYMENT_INFO"), String.format(getXML("OPERATION_FAILURE_XML"), e.getMessage()), "", "",
		 0.0,"");
	}

    }

    private String handlePayment(Long trip, PayActions pay, Double amount, String trans, Long driver, String account,
				 String merchantAcc, String CurrencyArg, String channelArg,
				 Double tipArg, Long company) throws Exception {
	switch (pay) {
	case notifTrip:
	    return sendPassNotification(trip,amount);
	case payTrip:
	    return sendTripPayment(trip, trans, account, amount, pay, merchantAcc, CurrencyArg, channelArg,
				   tipArg ,company,driver );
	case payAppoint:
	    return sendAppointPayment(trip, trans, account, amount, pay, merchantAcc, CurrencyArg, channelArg);
	case cancel:
	    return cancelTripPayment( driver, company);
	default:
	    throw new RuntimeException("not_supported");
	}

    }

    private String cancelTripPayment(Long driverId, Long companyId) throws Exception {

	if (companyId == null || driverId == null ){
	    throw new RuntimeException("invalid_argument");
	}
	Long deviceId = 0L;
	String deviceImei = null,tmp=null,loc=null;
	prepareGetDriversDeviceStmt();
	getDriversDeviceStmt.setLong(1, driverId);
	ResultSet rs = getDriversDeviceStmt.executeQuery();
	if (rs.next()) {
	    deviceId = rs.getLong("deviceid");
	    deviceImei = rs.getString("imei");
	    loc=(rs.getString("locale")==null?"en":rs.getString("locale"));
	    tmp=Messages.formatPrivateMessageToDriver(System.currentTimeMillis(), 
	                                                  Util.getProperty("payment_canceled_"+loc, "lang"), 
	                                                  driverId, Messages.Statuses.act.toString());
	    Messages.getInstance().sendMessageToDriver(companyId, driverId, tmp, true);
	  //  comm.sendMessageToDriver(String.format(getXML("PAYPAL_PAYMENT_INFO"),
	  //					   String.format("<id>%d</id>", trip), "cancel"),
	  //			     deviceImei);
	}
	rs.close();
	dataPool.closeConnection();
	return "OK";
    }
/*
 * <Payment>
<Trip>
<id>144556136</id>
<hailtime>1337685991000</hailtime>
<pickuptime>1337686236000</pickuptime>
<from>?????????????? 114 | ???????</from>
<to></to>
<special></special>
<agent>s</agent>
<status>com</status>
<rating>0</rating>
<plateno>???4356</plateno>
<carmodel>Ferrari Testarossa</carmodel>
<drivername>sotos</drivername>
<driversurname>nossis</driversurname>
<drivercode>NOSSOT01</drivercode>
<isfaved>true</isfaved>
</Trip>
<driverid>%d</driverid>
<companyid>%s</companyid>
<merchant>assd</merchant>
<amount>30.000000</amount>
<currency>EUR</currency>
<action>notifTrip</action>
<Payment>
 */
    private String sendPassNotification(Long trip,  Double amount) throws Exception {
	if (trip == null  || amount == null){
	    throw new RuntimeException("invalid_arguments");
	}
	prepareFetchTripData();
	fetchTripData.setString(1, "trip");
	fetchTripData.setLong(2, trip);
	ResultSet rs = fetchTripData.executeQuery();
	String tripResult = "";
	if (rs.next()) {
	    Long tripid = rs.getLong("tripid");
	    Timestamp hailtime = rs.getTimestamp("hailtime");
	    Timestamp pickuptime = rs.getTimestamp("pickuptime");
	    String from = rs.getString("pickupaddress");
	    String to = rs.getString("destination");
	    String special = rs.getString("specialneeds");
	    String status = rs.getString("status");
	    String bookAgent = rs.getString("bookagent");
	    Integer rating = rs.getInt("rating");
	    String plateno = rs.getString("platenumber");
	    String carmodel = rs.getString("carmodel");
	    String drivername = rs.getString("firstname");
	    String driversurname = rs.getString("lastname");
	    String drivercodename = rs.getString("codename");
	    String isFavorite = rs.getString("isfaved");
	    Long driverId = rs.getLong("driverid");
	    Long passId = rs.getLong("passengerid");
	    Long comp = rs.getLong("taxicompanyid");
	    Double amount2= rs.getDouble("amount");
	    String curr = rs.getString("currency");
	    String chan = rs.getString("channel");
	    String email =null;// rs.getString("email");
	    String facebook =null;// rs.getString("facebook");
	    String twitter = null;//rs.getString("twitter");
            Long driverid=rs.getLong("driverid");
	    String isblocked =rs.getString("isblocked");
	 //   String extra = rs.getString("extra"); //this will hold the token if the user has iphone
	 //   String softver = rs.getString("softwareversion");
	 //   String locale = rs.getString("locale"); //this will hold the locale of the passenger application
	    String account = rs.getString("merchantaccount"); //this will hold the company's paypal account
	    String currency = rs.getString("merchantcurrency"); //this will hold the company's paypal currency

	    String transa = rs.getString("transactId");
	    if (account == null || currency == null || account.trim().equals("") || currency.trim().equals("")) {
		rs.close();
		dataPool.closeConnection();
		throw new RuntimeException("payment_not_valid");
	    }
	    if (transa != null) {
		rs.close();
	        dataPool.closeConnection();
		throw new RuntimeException("payment_made");
	    }
	    tripResult =
 String.format(getXML("PASS_TRIP_INFO"), tripid, hailtime.getTime(), pickuptime != null ? pickuptime.getTime() : 0,
	       from != null ? from : "", to != null ? to : "", special != null ? special : "",
	       bookAgent != null ? bookAgent : "", status != null ? status : "", rating, plateno != null ? plateno : "",
	       carmodel != null ? carmodel : "", drivername != null ? drivername : "",
	       driversurname != null ? driversurname : "", drivercodename != null ? drivercodename : "",driverid,
	       Boolean.toString(isFavorite != null),Boolean.toString(isblocked!=null),
	       amount2 == null? 0.0:amount2,
	       curr == null?"":curr,
	       chan == null ? "" : chan,email==null?"":email,facebook==null?"":facebook,twitter==null?"":twitter);

	    comm.sendMessageToPassenger(String.format(getXML("PAYPAL_PAYMENT_INFO"),
						      tripResult + String.format(getXML("PAYPAL_NOTIF"), driverId, comp ,account,amount,currency), "notifTrip"),
					passId);
	    

	} else {
	    rs.close();
	    throw new RuntimeException("wrong_status_or_trip");

	}

	rs.close();
	dataPool.closeConnection();
	return "OK";
    }
    /*
     * <Payment>
<Trip>
<id>144556136</id>
<hailtime>1337685991000</hailtime>
<assigntime>1337686004000</assigntime>
<pickuptime>1337686236000</pickuptime>
<from>?????????????? 114 | ???????</from>
<to></to>
<special></special>
<agent>s</agent>
<status>com</status>
<plateno>???4356</plateno>
<carmodel>Ferrari Testarossa</carmodel>
<driverid>102</driverid>
<drivername>sotos</drivername>
<driversurname>nossis</driversurname>
<drivercodename>NOSSOT01</drivercodename>
<passname>George</passname>
<passsurname>BuggyArt</passsurname>
<passemail>vbagiartakis@taxiplon.com</passemail>
<passmobile></passmobile>
<rating>0</rating>
<isfaved>true</isfaved>
</Trip>
<companyid>%s</companyid>
<paymentID>2</paymentID>
<account>zczxcxzc</account>
<merchant>sdfsd</merchant>
<amount>30.000000</amount>
<tip>34.000000</tip>
<currency>EUR</currency>
<channel>paypal</channel>
<action>payTrip</action>
<Payment>
    tripPayment(in tripidArg bigint(20), 
				in originArg enum('trip','appoint'),
				in channelArg VARCHAR(50), 
				in transactionArg VARCHAR(100) ,
				in passAccountArg VARCHAR(150), 
				in amountArg DECIMAL (7,3), 
				in amountTipArg DECIMAL (7,3), 
				in merchantAccountArg VARCHAR(150), 
				in CurrencyArg VARCHAR(5))
     */

    private String sendTripPayment(Long trId, String transId, String account, Double fare, PayActions pay,
				    String merchant, String currency, String channel,
				   Double tip , Long taxicompanyid , Long driverid) throws Exception {
	if (trId == null ||  transId ==null || fare == null || currency == null || taxicompanyid == null || channel == null || driverid ==null){
	    throw new RuntimeException("invalid_arguments");
	}
	String informMessage = "",locale="", msgXML="" ;
	prepareUpdateTripTransaction();
	prepareGetDriversDeviceStmt();
	int i = 0;
	updateTripTransaction.setLong(++i, trId);
	updateTripTransaction.setString(++i, "trip");
	updateTripTransaction.setString(++i, channel);
	updateTripTransaction.setString(++i, transId);
	updateTripTransaction.setString(++i, account);
	updateTripTransaction.setDouble(++i, fare);
	if (tip == null)
	    updateTripTransaction.setString(++i, null);
	else
	    updateTripTransaction.setDouble(++i, tip);
	updateTripTransaction.setString(++i, merchant);
	updateTripTransaction.setString(++i, currency);
	updateTripTransaction.registerOutParameter(++i, Types.NUMERIC);
	updateTripTransaction.execute();
	Long payid = updateTripTransaction.getLong("payIDout");
	if (payid == null || payid==0L){
	    updateTripTransaction.close();
	    dataPool.closeConnection();
	    throw new RuntimeException("invalid_payment");
	}
	Long deviceId = 0L;
	String deviceImei = null;
	getDriversDeviceStmt.setLong(1, driverid);
	ResultSet rs = getDriversDeviceStmt.executeQuery();	
	if (rs.next()) {
	    deviceId = rs.getLong("deviceid");
	    deviceImei = rs.getString("imei");
	    locale = (rs.getString("locale")==null?"en":rs.getString("locale"));
	    msgXML=Messages.formatPrivateMessageToDriver(System.currentTimeMillis(), 
	                                                  String.format(Util.getProperty("payment_received_"+locale, "lang"),fare,currency,tip), 
	                                                  driverid, Messages.Statuses.act.toString());
	    Messages.getInstance().sendMessageToDriver(taxicompanyid, driverid, msgXML, true);
	    
	  //  comm.sendMessageToDriver(String.format(getXML("PAYPAL_PAYMENT_INFO"), informMessage +
	  //				String.format(getXML("PAYPAL_PAYMENT_TRIP"),payid,account,merchant,fare,tip,currency,(channel==null?"Paypal":channel)), pay),
	  //			     deviceImei);
	}
	rs.close();
	dataPool.closeConnection();
	return "OK";
    }

    private String sendAppointPayment(Long trId, String transId, String account, Double fare, PayActions pay,
				      String merchant, String currency, String channel) throws Exception {
	prepareUpdateTripTransaction();
	int i = 0;
	updateTripTransaction.setLong(++i, trId);
	updateTripTransaction.setString(++i, "appoint");
	updateTripTransaction.setString(++i, channel);
	updateTripTransaction.setString(++i, transId);
	updateTripTransaction.setString(++i, account);
	updateTripTransaction.setDouble(++i, fare);
	updateTripTransaction.setString(++i, null);
	updateTripTransaction.setString(++i, merchant);
	updateTripTransaction.setString(++i, currency);
	updateTripTransaction.registerOutParameter(++i, Types.NUMERIC);
	updateTripTransaction.execute();
	Long payid = updateTripTransaction.getLong("payIDout");
	updateTripTransaction.close();
	dataPool.closeConnection();
	if (payid == null || payid==0L){
	    throw new RuntimeException("invalid_payment");
	}
	return "OK";
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
