package aploon.xmpp.ssclient;


import aploon.xmpp.ServerSideClient;

import java.sql.CallableStatement;
import java.sql.Timestamp;
import java.sql.Types;

import org.jivesoftware.smack.packet.IQ;
/*
 * Hotspot points in the map of the driver
 */
public class Hotspots extends ServerSideClient {
	
	CallableStatement createOrUpdateHotspotStmt;
	
	private static Hotspots instance;
	
	public static synchronized Hotspots getInstance() {
		if(instance == null) {
			instance = new Hotspots();
		}
		return instance;
	}
	
	public enum Statuses {
		del;
	}
	
	public enum EventTypes {
	    sport, business, music, show, exhibition, hospital, pharmacy, other,
		r_traffic, r_accident, r_obstacle, r_police, r_clients;
	}
	
	private Hotspots() {
		super();
	}
	
	private void prepareCreateOrUpdateHotspotStmt() throws Exception {
		if(createOrUpdateHotspotStmt == null || createOrUpdateHotspotStmt.isClosed()) {
		    createOrUpdateHotspotStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateHotspot"));
		}
	}

	public void processRequest() {
	    String failure = null;
	    Long eventId = parseOptionalField("eventid", Long.class);
	    try {
	        Long startTime = parseOptionalField("start", Long.class);
	        Long endTime = parseOptionalField("end", Long.class);
	        Double lat = parseOptionalField("lat", Double.class);
	        Double lng = parseOptionalField("lng", Double.class);
	        Statuses status = parseOptionalField("status", Statuses.class);
	        String description = request.get("desc");
	        EventTypes type = parseOptionalField("type", EventTypes.class);;
	        eventId = createOrUpdateHotspot(eventId, startTime, endTime, null, lat, lng, status, description, type, null, null);
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	        failure = e.getMessage();
	    }
	    response = String.format(getXML("UPDATE_EVENT_OUTCOME"), eventId, failure == null? "true" : "false", failure);
	}
	
	public Long createOrUpdateHotspot(Long eventId, Long startTime, Long endTime, String datesVerbatim, 
									  Double lat, Double lng, Statuses status, String description, EventTypes type, String address, String notes) throws Exception {
		prepareCreateOrUpdateHotspotStmt();
		int i = 1;
	    createOrUpdateHotspotStmt.setString(i++, eventId != null? eventId.toString() : null);
	    createOrUpdateHotspotStmt.setString(i++, description);
		createOrUpdateHotspotStmt.setTimestamp(i++, startTime != null? new Timestamp(startTime) : null);
		createOrUpdateHotspotStmt.setTimestamp(i++, endTime != null? new Timestamp(endTime) : null);//zero time will be transformed into 1-1-1970 and this will instruct the DB procedure to set it to null
	    createOrUpdateHotspotStmt.setString(i++, datesVerbatim);
	    createOrUpdateHotspotStmt.setString(i++, lat != null? lat.toString() : null);
	    createOrUpdateHotspotStmt.setString(i++, lng != null? lng.toString() : null);
		createOrUpdateHotspotStmt.setString(i++, type != null? type.toString() : null);
		createOrUpdateHotspotStmt.setString(i++, status != null? status.toString() : null);
		if(address == null && lat != null && lng != null) {
			try {//this will throw "address not located" exception
				address = locator.getAddressByCoordinates(lat, lng);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		createOrUpdateHotspotStmt.setString(i++, address);
		createOrUpdateHotspotStmt.setString(i++, notes);
		createOrUpdateHotspotStmt.registerOutParameter(i, Types.NUMERIC);
		createOrUpdateHotspotStmt.execute();
	    eventId = createOrUpdateHotspotStmt.getLong("eventIdOut");
	    dataPool.closeConnection();
	    return eventId;
	}

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
