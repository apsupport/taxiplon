package aploon.xmpp.ssclient;


import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
/*
 * See enum.
 */



import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.datahelper.DataHelper;
import aploon.model.Coordinates;
import aploon.model.TripData;
import aploon.offer.OfferProvider;
import aploon.sms.SMSSUtil;
import aploon.statusmanager.OnRideStatusManager;
import aploon.xmpp.ServerSideClient;
import aploon.xmpp.ssclient.bots.AppointDispatcher;
import aploon.xmpp.ssclient.bots.FareDispatcher;
import aploon.xmpp.ssclient.iphone.Push;
import aploon.xmpp.ssclient.iphone.PushSender;

import com.taxiplon.Util;

public class FareControl extends ServerSideClient {

    public enum Operations {
    	PassengerCancel,
    	DriverCancel,
    	Complete,
    	Pickup,
    	PassengerNoShow,
    	Poke,
    	AppointPoke,
    	AppointTrip,
    	PassengerComplete;
    }

    private PreparedStatement fetchTripData;

    private static final Logger LOGGER = Logger.getLogger(FareControl.class);
    
    public FareControl() throws Exception {
    	super();
    }

    private void prepareFetchTripData() throws Exception {
    	if (fetchTripData == null || fetchTripData.isClosed()) {
    		fetchTripData = dataPool.prepareStatement(getQuery("fetchTripData"));
    	}
    }

    public void processRequest() {
    	String tmp = request.get("op");
    	Long givenId = 0L;
    	try {
    		
    		Operations operation = null;
    		try {
    			operation = Operations.valueOf(tmp);
    		} catch (Exception e) {
    			throw new RuntimeException("Operation " + tmp + " not supported");
    		}
    		givenId = Long.parseLong(request.get("id"));
    		response = updateTripCallInDb(givenId, operation);
    	} catch (Exception e) {
    		response = String.format(getXML("FARE_CONTROL_XML"), givenId, tmp, String.format(getXML("OPERATION_FAILURE_XML"), e.getMessage()));
    	}
    }

    /**
     * @param givenId ID of the device/trip (trip is only in case of passenger cancellation or passenger poke) to perform this operation for
     * (an operation can be requested by the user only for current device/trip)
     * @param operation Operation type (links it directly to the respective stored procedure)
     * @throws Exception
     */
    private String updateTripCallInDb(Long givenId, Operations operation) throws Exception {
    
    	try {
    		DataHelper.insertToBackendLogRec(null,operation.toString(), givenId, operation.toString(),from);
    	} catch(SQLException e) {
    		LOGGER.error(e);
    	}
    	
    	Long tripId = null;
    	Long deviceId = null;
    	String drIMEI = null;
    	try {
    		StatusCheck.PassengerStatus infoAboutTransaction = StatusCheck.getInstance().new PassengerStatus(false, 0, 0, 0, "");
    		String passengerInformMsg = "";
    		if (!(operation == Operations.Poke || operation == Operations.AppointPoke || operation == Operations.AppointTrip)) {
    			String updateTripStatusQuery = getQuery("fareControl" + operation);
    			CallableStatement updateTripStatusStmt = dataPool.getConnection().prepareCall(updateTripStatusQuery);
    			updateTripStatusStmt.setLong(1, givenId);
    			updateTripStatusStmt.registerOutParameter(2, Types.NUMERIC);
    			//				if(operation == Operations.PassengerNoShow || operation == Operations.PassengerCancel || operation == Operations.Complete) {
    			try {
    				//PassengerComplete ,PassengerNoShow, PassengerCancel, Complete operations will return the passengerid in third parameter
    				updateTripStatusStmt.registerOutParameter(3, Types.NUMERIC);
    				updateTripStatusStmt.registerOutParameter(4, Types.VARCHAR);

    			} catch (Exception e) {
    				// e.printStackTrace();
    			}
    			//fetch passenger status when it's supported by this operation (passid is returned)
    			try {
    				updateTripStatusStmt.execute();
    				if (operation == Operations.PassengerComplete) {
    					tripId = givenId;
    					deviceId = updateTripStatusStmt.getLong(2);
    					drIMEI = updateTripStatusStmt.getString(4);
    				} else {
    					tripId = updateTripStatusStmt.getLong(2);
    				}

    				try {
    					//third parameter doesn't exist in some transactions
    					Long passId = updateTripStatusStmt.getLong(3);
    					infoAboutTransaction =
    							StatusCheck.getInstance().checkPassengerStatus(null, null, passId, null); //TODO populate information about the transaction (such as remaining cancel attempts before disabling etc)
    				} catch (Exception e) {
    					//						e.printStackTrace();
    				}
    			} catch (Exception e) {
    				System.err.println("Tried and failed to execute the procedure: " + updateTripStatusQuery);
    				e.printStackTrace();
    			}
    			updateTripStatusStmt.close();
    			dataPool.closeConnection();
    			
    			try {
    				DataHelper.insertToBackendLogExe(null,operation.toString(), givenId, operation.toString());
        		} catch(SQLException e) {
    				LOGGER.error(e);
    			}
    			
    		} else if (operation.equals(Operations.Poke)) {
    			
    			tripId = givenId;
    			dataPool.executeUpdate(String.format(getQuery("fareControlPoke"), tripId));
    			
    			try {
    				DataHelper.insertToBackendLogExe(null,operation.toString(), tripId, operation.toString());
        		} catch(SQLException e) {
    				LOGGER.error(e);
    			}
    			
    			try {
                    DataHelper.setTripArrivalTime(givenId);
    			} catch(SQLException e) {
                    LOGGER.error("Problem setting arrival time",e);
    			}
    			
    		}
	    
    		if (operation == Operations.PassengerComplete && (deviceId == null || (deviceId != null && deviceId == 0L))) {
    			try {
    				DataHelper.insertToBackendLogExe(null,operation.toString(), givenId, operation.toString());
        		} catch(SQLException e) {
    				LOGGER.error(e);
    			}
    			throw new RuntimeException("trip_illegal_state");
    		} else if (operation == Operations.PassengerComplete) {
    			operation = Operations.Complete;
    			try {
    				try {
        				DataHelper.insertToBackendLogExe(null,operation.toString(), givenId, operation.toString());
            		} catch(SQLException e) {
        				LOGGER.error(e);
        			}
    				Dispatcher.getInstance().passengerComplete(drIMEI, tripId);
    			} catch (Exception e) {
    				e.printStackTrace();
    			}

    		}

	    String statusCheck =
		       String.format(getXML("PASS_STATUS_XML"), StatusCheck.getInstance().getAppURL(), Boolean.toString(infoAboutTransaction.isProper()),
				     infoAboutTransaction.getBlacklistIn(), infoAboutTransaction.getDiscountRides(),
				     infoAboutTransaction.getStars(), 0,
				     infoAboutTransaction.getCompanyEmail() == null ? "" : infoAboutTransaction.getCompanyEmail(),
				     infoAboutTransaction.getCompanyFacebook() == null ? "" : infoAboutTransaction.getCompanyFacebook(),
				     infoAboutTransaction.getCompanyTwitter() == null ? "" : infoAboutTransaction.getCompanyTwitter(),
				     infoAboutTransaction.getCallXML(), ""); //empty performance time info
	    //depending on the type of operation perform some extra stuff
	    if (operation == Operations.PassengerCancel) {
	    	//remove the trip from the voting mechanism
	    	FareDispatcher.getInstance().cancelTrip(givenId);
	    	//remove the pin from all devices where it was sent (the driver allocated is released on the DB level, his device doesn't know about it though)
	    	try {
	    		Dispatcher.getInstance().unlinkDevices(givenId, true);
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	}
	    } else if (operation.equals(Operations.AppointPoke)) {
		try {
			
			try {
				DataHelper.insertToBackendLogExe(null,operation.toString(), givenId, operation.toString());
    		} catch(SQLException e) {
				LOGGER.error(e);
			}
		    AppointDispatcher.getInstance().sendAppointInformToPassenger(givenId, operation.toString());
		    dataPool.executeUpdate(String.format(getQuery("fareControlAppointPoke"), givenId));

		} catch (Exception e) {
		    e.printStackTrace();
		}

	    } else if (operation.equals(Operations.AppointTrip)) {
	    	try {
				DataHelper.insertToBackendLogExe(null,operation.toString(), givenId, operation.toString());
    		} catch(SQLException e) {
				LOGGER.error(e);
			}
	    	AppointDispatcher.getInstance().makeAppointTrip(givenId);
	    } else {
	    	prepareFetchTripData();
	    	//the rest of cases (except for passengerCancel require an information message to the passenger device)
	    	fetchTripData.setLong(1, tripId);
	    	ResultSet rs = fetchTripData.executeQuery();
	    	if (rs.next()) { // single row result
	    		Long passId = rs.getLong("passengerid");
	    		String extra = rs.getString("extra"); //this will hold the token if the user has iphone
	    		String softver = rs.getString("softwareversion");
	    		String locale = rs.getString("locale"); //this will hold the locale of the passenger application
	    		String bookagent = rs.getString("bookagent");
	    		String mobile = rs.getString("mobile");
	    		String conphone = rs.getString("contactphone");
	    		String country = rs.getString("origincountry");
	    		String brand = rs.getString("brandname");
	    		Long companyid = rs.getLong("taxicompanyid");
	    		int poked = rs.getInt("poked");
	    		if (operation == Operations.DriverCancel) {
	    			//                                              Long hailtime = System.currentTimeMillis();
	    			//remove the trip from the voting mechanism
	    			Coordinates coord = new Coordinates(rs.getDouble("pickuplat"), rs.getDouble("pickuplong"), rs.getString("pickupaddress"));
	    			//even if the call was targeted, on redispatch use the normal procedure
	    			passengerInformMsg = UpdateCall.getInstance().dispatchCancelCall(tripId, coord, System.currentTimeMillis());

		    } else if (operation == Operations.Pickup) {
		    	//TODO AFTER DISCOUNT CAMPAIGN ENDS REMOVE THIS: send a private message to the driver that this trip is discounted
		    	/* TODO AFTER THE CAMPAIGN ENDS, MODIFY THIS CODE TO ALWAYS SET isDiscounted TO FALSE */
		    	boolean isDiscounted = rs.getLong("discountrides") > 0;
		    	
		    	Long driverId = rs.getLong("driverid");
	    		Long compId = rs.getLong("taxicompanyid");
	    		
	    		OnRideStatusManager onRideStatusManager = new OnRideStatusManager(driverId);
	    		onRideStatusManager.setOnRide(tripId);
				
		    	OfferProvider offerProvider = new OfferProvider(compId, passId);
		    	Double discount = offerProvider.getAnyDiscount(bookagent);
		    	
		    	if(discount!=0.0) {
		    		
		    		long time = System.currentTimeMillis();
		    		
		    		Messages.getInstance().sendMessageToDriver(compId, driverId,
						       Messages.formatPrivateMessageToDriver(time, String.format(Util.getProperty("discount_msg_3_"+locale, "lang"),
														 String.valueOf(discount.intValue())+" \u20AC"), driverId, Messages.Statuses.act.toString()),
						       true);
		    	
		    		//Thank you for using our service. This ride has a discount of %s 
		    	}
		    	
		    	try {
                    DataHelper.setTripArrivalTime(givenId);
		    	} catch(SQLException e) {
                    LOGGER.error("Problem setting arrival time",e);
		    	}
		    	
		    } else if (operation == Operations.Complete) {
		    	
		    	OfferProvider offerProvider = new OfferProvider(companyid, passId);
		    	offerProvider.completeDiscount(bookagent,tripId);

		    }
		    
		    	String localizedMessageToPassenger =
				      Util.getProperty(operation + "_" + (locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE),
						       "lang");
		    	String localizedTitleToPassenger =
				      Util.getProperty(operation + "_title_" + (locale != null ? locale : Push.DEFAULT_PASSENGER_LOCALE),
						       "lang");

		    String msg = null;
		    if (bookagent == null || (!bookagent.equals("w") || isMobileCaller())) {
			msg =
		      String.format(getXML("PASS_FARE_UPDATE"), operation, localizedMessageToPassenger != null ? localizedMessageToPassenger :
									   "", localizedTitleToPassenger != null ? localizedTitleToPassenger : "", passengerInformMsg,
				    statusCheck);

		    } else {
			if (passengerInformMsg != null) {
			    passengerInformMsg =
       passengerInformMsg.replace(String.format("<tripid>%d</tripid>", tripId), "");
			}
			msg =
		      String.format(getXML("PASS_FARE_UPDATE_MULTICALL"), tripId, operation, localizedMessageToPassenger != null ? localizedMessageToPassenger :
											     "", localizedTitleToPassenger != null ? localizedTitleToPassenger : "", passengerInformMsg,
				    statusCheck);
		    }
		    comm.sendMessageToPassenger(msg, passId);
		    //send a push notification to the passenger for any driver's action
		    if (extra != null && bookagent != null && bookagent.equals("s")) {
			try {
			    String pushSound = Util.getProperty("push_sound", "config");
			    if (localizedMessageToPassenger != null) {
				PushSender.getInstance(brand).sendPushNotification(extra + "@" + softver, localizedMessageToPassenger, operation.toString(),
									localizedTitleToPassenger, null, 0, pushSound);
			    } else {
				PushSender.getInstance(brand).sendPushNotification(extra + "@" + softver, null, operation.toString(),
									localizedTitleToPassenger, new String[0], null, 0,
									pushSound);
			    }
			} catch (Exception e) {
			    e.printStackTrace();
			}
		    }
		    if (bookagent != null && (!bookagent.equals("s")) && localizedMessageToPassenger != null) {
		    
			    boolean sendSms = false;
		    	
			    switch (operation) {
			    
			    case DriverCancel: 		    	
			    	sendSms = bookagent.equals("w");
			    	break;	
			    case PassengerNoShow:
			    	
			    	sendSms = true;
			    	
			    	break;
			    case Poke:
			    	// && conphone != null
			    	if ((bookagent.equals("d")||bookagent.equals("w"))) {
			    		if (poked == 1 || (poked % 4) == 0) {
			    			sendSms = true;
			    		}
			    	}
			    	break;
			    default:
			    	break;
			    }
			    
			    if(sendSms) {
			    
			    	
			    	SMSSUtil smsProvider = new SMSSUtil(companyid);
			    	smsProvider.setPassengerId(passId);
                
			    	String smsMobile = getSmsMobile(conphone, mobile);
			    	
			    	if(smsMobile!=null) {
			    		LOGGER.debug("SMD sending message "+companyid+" "+smsMobile+" "+operation+localizedMessageToPassenger);
			    		if(smsProvider.isEnabled()) {
			    			smsProvider.sendSms(smsMobile, localizedMessageToPassenger);
			    		} else {
			    			SMSSUtil.sendDefaultSmsAsync(smsMobile, localizedMessageToPassenger, null);
			    		}
			    	} else {
			    		LOGGER.debug("No mobile set for sms "+smsMobile);
			    	}
			    		
			    }
			    
		    }
		
		    	//PenaltyManager.executeFareControlPenalty(operation, tripId);
	    	}
	    	
	    	rs.close();
	    	dataPool.closeConnection();
	    }
	    //message sent to the driver
	    return String.format(getXML("FARE_CONTROL_XML"), givenId, operation,
				 operation == Operations.PassengerCancel ? statusCheck : "");
    	} catch (Exception e) {
    		LOGGER.error("Unknown exception ",e);
    		//e.printStackTrace();
    		throw e;
    	}
    }
    
    private String getSmsMobile(String contactPhone,String mobile) {
    	
    	if(contactPhone!=null) {
    		return contactPhone;
    	} else if(mobile!=null) {
    		return mobile;
    	}
    	
    	return null;
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
    
}
