package aploon.xmpp.ssclient;


import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.IQ;

import aploon.datahelper.DataHelper;
import aploon.xml.model.NearByDriverXML;
import aploon.xml.model.NearbyCompanyDriversResponseXML;
import aploon.xml.model.NearbyCompanyXML;
import aploon.xmpp.ServerSideClient;

import com.taxiplon.Util;
import com.taxiplon.db.NamedParameterStatement;

/*
 * Is for passenger
 * list taxi companies(communication),list countries, crud from company's site. 
 */
public class YellowPages extends ServerSideClient {
	
	// act=listnew&lat=44.435142&lng=26.102290&passid=198491
    private NamedParameterStatement availableCompaniesNearbyStmt;

    private NamedParameterStatement availableCompaniesTaxiNearbyStmt;
    private NamedParameterStatement findNearbyCompanyDevicesStmt;
    private NamedParameterStatement availableCompaniesNearbyAppointStmt;
    private PreparedStatement availableCompaniesStmt;
    private PreparedStatement availableCountryCompaniesStmt;

    private CallableStatement createOrUpdateCompanyStmt;

    private enum Actions {
    	list,
    	listnew,
    	listdrivercompanies,
    	update,
    	listAppoint,
    	listInfo,
    	listCountry;
    }

    private static final Logger LOGGER = Logger.getLogger(YellowPages.class);
    
    public YellowPages() throws Exception {
    	super();
    }

    private void prepareAvailableCountryCompaniesStmt() throws Exception {
    	if (availableCountryCompaniesStmt == null || availableCountryCompaniesStmt.isClosed()) {
    		availableCountryCompaniesStmt = dataPool.getConnection().prepareCall(getQuery("fetchAllCountryCompanies"));
    	}
    }

    private void prepareNearbyCompanyDevicesStmt() throws Exception {
    	if (findNearbyCompanyDevicesStmt == null || findNearbyCompanyDevicesStmt.isClosed()) {
    		findNearbyCompanyDevicesStmt = new NamedParameterStatement(dataPool, getQuery("findNearbyCompanyDevices"));
    	}
    }

    private void prepareAvailableCompaniesNearbyStmt() throws Exception {
    	if (availableCompaniesNearbyStmt == null || availableCompaniesNearbyStmt.isClosed()) {
    		availableCompaniesNearbyStmt = 
    				new NamedParameterStatement(dataPool, getQuery("findAvailableCompaniesNearby"));
    	}
    }

    private void prepareAvailableCompaniesTaxiNearbyStmt() throws Exception {
    	if (availableCompaniesTaxiNearbyStmt == null || availableCompaniesTaxiNearbyStmt.isClosed()) {
    		availableCompaniesTaxiNearbyStmt = 
    				new NamedParameterStatement(dataPool, getQuery("findAvailableCompaniesTaxiNearby"));
    	}
    }

    private void prepareAvailableCompaniesStmt() throws Exception {
    	if (availableCompaniesStmt == null || availableCompaniesStmt.isClosed()) {
    		availableCompaniesStmt = dataPool.getConnection().prepareCall(getQuery("fetchAllDefaultCompanies"));
    	}
    }

    private void prepareAvailableCompaniesNearbyAppointStmt() throws Exception {
    	if (availableCompaniesNearbyAppointStmt == null || availableCompaniesNearbyAppointStmt.isClosed()) {
    		availableCompaniesNearbyAppointStmt = 
    				new NamedParameterStatement(dataPool, getQuery("findAvailableCompaniesNearbyAppoint"));
    	}
    }

    private void prepareCreateOrUpdateCompany() throws Exception {
    	if (createOrUpdateCompanyStmt == null || createOrUpdateCompanyStmt.isClosed()) {
    		createOrUpdateCompanyStmt = dataPool.getConnection().prepareCall(getQuery("createOrUpdateCompany"));
    	}
    }


    public void processRequest() {
    	try {
    		Long taxicompanyid = null;
    		try {
    			taxicompanyid = Long.parseLong(request.get("compid"));
    		} catch (Exception e) {
    			//do nothing
    		}
    		String action = request.get("act");
    		if (Actions.list.toString().equals(action)) {
    			String lat = request.get("lat");
    			String lng = request.get("lng");
    			Double multi = parseOptionalField("rangemulti", Double.class);
    			Long passid = parseOptionalField("passid", Long.class);
    			response = fetchNearbyCompaniesByCoordinates(lat, lng, taxicompanyid, multi, passid);
    		} else if(Actions.listdrivercompanies.toString().equals(action)) {
    			Double lat = parseOptionalField("lat", Double.class);
    			Double lng = parseOptionalField("lng", Double.class);
    			Long passid = parseOptionalField("passid", Long.class);
    			response = fetchNearbyDriversFromAllCompanies(lat,lng,passid);
    		} else if (Actions.listnew.toString().equals(action)) {
    			String lat = request.get("lat");
    			String lng = request.get("lng");
    			Double multi = parseOptionalField("rangemulti", Double.class);
    			Long passid = parseOptionalField("passid", Long.class);
    			//Added in order to provide webbooking always with a driver list 
    			//should go as soon as possible otherwise ASAP
    			String agent=request.get("agent");
    			response = fetchNearbyCompaniesOrDriversByCoordinates(lat, lng, taxicompanyid, multi, passid,agent);
    		} else if (Actions.listInfo.toString().equals(action)) {
    			response = fetchAllCompanies(null);
    		} else if (Actions.listCountry.toString().equals(action)) {
    			response = fetchAllCompanies(request.get("country"));
    		} else if (Actions.listAppoint.toString().equals(action)) {
    			String lat = request.get("lat");
    			String lng = request.get("lng");
    			response = fetchNearbyCompaniesByCoordinatesAppoint(lat, lng, taxicompanyid);
		    } else if (Actions.update.toString().equals(action)) {

		    	String shortname = request.get("short");
		    	String officialname = request.get("full");
		    	String companytype = request.get("type");
		    	String address = request.get("addr");
		    	String telephone = request.get("phone");
		    	String fax = request.get("fax");
		    	String email = request.get("email");
		    	String website = request.get("web");
		    	String hailrange = request.get("hail");
		    	String clearbiddingtime = request.get("bidtime");
		    	String requestwaittime = request.get("waittime");
		    	String candidateNumber = request.get("candnum");
		    	String appointmentremind = request.get("appremind");
		    	String isdefault = request.get("def");
		    	String nelat = request.get("nelat");
		    	String nelng = request.get("nelng");
		    	String swlat = request.get("swlat");
		    	String swlng = request.get("swlng");
		    	String user = request.get("userid");
		    	String locale = request.get("loc");
		    	
		    	taxicompanyid = createOrUpdateCompany(taxicompanyid, shortname, officialname, 
		    			companytype, address, telephone, fax, email, website, hailrange, 
		    			clearbiddingtime, requestwaittime, candidateNumber, appointmentremind, 
		    			isdefault, nelat, nelng, swlat, swlng, locale, user);
		    	response = String.format(getXML("OPERATION_SUSSECCFUL_XML"), taxicompanyid.toString());
		    }
    	} catch (Exception e) {
    		response = String.format(getXML("OPERATION_FAILURE_XML"), e.getMessage());
    		try {
    			dataPool.closeConnection();
    		} catch (Exception ex) {
    			ex.printStackTrace();
    		}
    	}
    	response = String.format(getXML("COMPANIES_WRAPPER_XML"), response);
    }

    private String fetchNearbyCompaniesOrDriversByCoordinates(String lat, String lng, Long compId, Double multi,
							      Long PassengerID, String bookAgent) throws Exception {

    	//bookAgent is here as temporary in order to provide always drivers  list to webbooking
    	//should go as soon as webbooking is fixed
    	
    	LOGGER.info("Trying to find the new pages");
    	
    	prepareAvailableCompaniesTaxiNearbyStmt();
    	availableCompaniesTaxiNearbyStmt.setString("lat", lat);
    	availableCompaniesTaxiNearbyStmt.setString("multi", multi == null ? null : multi.toString());
    	availableCompaniesTaxiNearbyStmt.setString("lng", lng);
    	availableCompaniesTaxiNearbyStmt.setString("passID", PassengerID == null ? null : PassengerID + "");
    	availableCompaniesTaxiNearbyStmt.setString("comp", compId == null ? null : (compId + ""));
    	
    	ResultSet rs = availableCompaniesTaxiNearbyStmt.executeQuery();
    	String ans = "";
    	if (!rs.next()) {
    		rs.close();
    		dataPool.closeConnection();
    		throw new RuntimeException("shortage");
    	} else {
    		rs.last();
    		int iCount = rs.getRow();
    		rs.beforeFirst();
    		if (iCount > 1) {
    			if (bookAgent == null) {
    				ans = getCompanies(rs);
    			} else {
    				ans = getDrivers(lat, lng, null, multi, PassengerID, Long.parseLong(Util.getProperty("number_of_devices","config")));
    			}
    		} else if (iCount == 1) {
    			rs.next();
    			String comptype = rs.getString("companytype");
    			long comp = rs.getLong("taxicompanyid");
    			long cand = rs.getLong("candidatenumber");
    			if ((comptype == null || comptype.equals("clu")) && compId == null) {
    				rs.beforeFirst();
    				if (bookAgent == null) {
    					ans = getCompanies(rs);
    				} else {
    					ans = getDrivers(lat, lng, comp, multi, PassengerID,cand);
    				}
    				//ans = getCompanies(rs);
    			} else {
    				ans = getDrivers(lat, lng, comp, multi, PassengerID, cand);
    			}
    		}
    		rs.close();
    		dataPool.closeConnection();
    	}
    	return ans;
    }
    
    public static String fetchNearbyDriversFromAllCompanies(Double lat,Double lng,Long passengerId) {
    	
    	NearbyCompanyDriversResponseXML nearbyCompanyDriversResponseXML = new NearbyCompanyDriversResponseXML();
    	
    	try {
			List<NearbyCompanyXML> nearbyCompanyXMLs = DataHelper.findNearByCompanies(lat, lng, passengerId);
			
			for(NearbyCompanyXML nearbyCompanyXML: nearbyCompanyXMLs) {
			
				List<NearByDriverXML> nearByDriverXMLs = DataHelper.findNearByDrivers(lat, lng, passengerId, nearbyCompanyXML.getTaxiCompanyId(), nearbyCompanyXML.getCandidateNumber());
				nearbyCompanyXML.setAvailableDrivers(nearByDriverXMLs.size());
				nearbyCompanyXML.setNearByDriverXMLs(nearByDriverXMLs);
			}
			
			nearbyCompanyDriversResponseXML.setMessage("Successfull fetching data");
			nearbyCompanyDriversResponseXML.setSuccess(true);
			nearbyCompanyDriversResponseXML.setContent(nearbyCompanyXMLs);
			
    	} catch (SQLException e) {
			LOGGER.error("Fatal sql exception",e);
			nearbyCompanyDriversResponseXML.setMessage("SQL problem");
		}
    	
    	try {
			return ServerSideClient.serializeObject(nearbyCompanyDriversResponseXML);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("Eisai xlapatsas",e);
		}
    	
    	return null;
    	
    }    
    
    /*
    public static String fetchNearbyDriversFromAllCompanies(Double lat,Double lng,Long passengerId) {
    	
    	NearbyCompanyDriversResponseXML nearbyCompanyDriversResponseXML = new NearbyCompanyDriversResponseXML();
    	
    	try {
			
    		List<Map<String, Object>> nearbyDrivers = DataHelper.findNearbyCompanyAllDevices(lat,lng,passengerId);
    		List<NearbyCompanyXML> nearbyCompanyXMLs = new ArrayList<>();
    		NearbyCompanyXML nearbyCompanyXML = new NearbyCompanyXML();
			
    		for(Map<String, Object> nearbydriver:nearbyDrivers) {
    		
    			LOGGER.info("The taxicompanyid is "+(Long)nearbydriver.get("taxicompanyid"));
    			
    			if(nearbyCompanyXML.getTaxiCompanyId()!=(Long)nearbydriver.get("taxicompanyid")) {
    			  				
    				nearbyCompanyXML = new NearbyCompanyXML();
    				nearbyCompanyXMLs.add(nearbyCompanyXML);
    				nearbyCompanyXML.setTaxiCompanyId((Long)nearbydriver.get("taxicompanyid"));
    				nearbyCompanyXML.setShortName((String)nearbydriver.get("shortname"));
    				nearbyCompanyXML.setOfficialName((String)nearbydriver.get("officialname"));
    				nearbyCompanyXML.setCompanyType((String)nearbydriver.get("companytype"));
    				nearbyCompanyXML.setAddress((String)nearbydriver.get("address"));
    				nearbyCompanyXML.setTelephone((String)nearbydriver.get("telephone"));
    				nearbyCompanyXML.setCompanyInfo((String)nearbydriver.get("companyinfo"));
    				nearbyCompanyXML.setFax((String)nearbydriver.get("fax"));
    				nearbyCompanyXML.setEmail((String)nearbydriver.get("email"));
    				nearbyCompanyXML.setWebSite((String)nearbydriver.get("website"));
    				nearbyCompanyXML.setHailRange((Double)nearbydriver.get("hailrange"));
    				nearbyCompanyXML.setClearBiddingTime((Integer)nearbydriver.get("clearbiddingtime"));
    				nearbyCompanyXML.setRequestWaitTime((Integer)nearbydriver.get("requestwaittime"));
    				nearbyCompanyXML.setAppointmentRemind((Integer)nearbydriver.get("appointmentremind"));
    				nearbyCompanyXML.setCandidateNumber((Integer)nearbydriver.get("candidatenumber"));
    				nearbyCompanyXML.setMinimumDistance((Double) nearbydriver.get("distance"));
    				nearbyCompanyXML.setNearByDriverXMLs(new ArrayList<NearByDriverXML>());
    			}
    			
    			NearByDriverXML nearByDriverXML = new NearByDriverXML();
    			nearByDriverXML.setDriverId((Long)nearbydriver.get("driverid"));
    			nearByDriverXML.setDeviceId((Long)nearbydriver.get("deviceid"));
    			nearByDriverXML.setCodeName((String)nearbydriver.get("codename"));
    			nearByDriverXML.setSurName((String)nearbydriver.get("lastname"));
    			nearByDriverXML.setName((String) nearbydriver.get("firstname"));
    			nearByDriverXML.setCurrentLat((Double)nearbydriver.get("currentLat"));
    			nearByDriverXML.setCurrentLong((Double)nearbydriver.get("currentLong"));
    			nearByDriverXML.setPlateNumber((String)nearbydriver.get("plateno"));
    			nearByDriverXML.setCarModel((String)nearbydriver.get("model"));
    			nearByDriverXML.setDistance((Double)nearbydriver.get("distance"));
    			nearByDriverXML.setIsFaved((Boolean)nearbydriver.get("faved"));
    			nearByDriverXML.setSpeaksEnglish((Boolean)nearbydriver.get("speaksenglish"));
    			nearByDriverXML.setNewVehicle((Boolean)nearbydriver.get("newvehicle"));
    			nearByDriverXML.setTotalFavs((Integer)nearbydriver.get("totalfavourites"));
    			nearByDriverXML.setHybrid((Boolean)nearbydriver.get("hybrid"));
    			nearByDriverXML.setHasWifi((Boolean)nearbydriver.get("wifi"));
    			nearByDriverXML.setIsSmoker((Boolean)nearbydriver.get("smoker"));
    			nearByDriverXML.setPetsAllowed((Boolean)nearbydriver.get("allowpets"));
    			nearByDriverXML.setBigTeamVeh((Boolean)nearbydriver.get("bigteamveh"));
    			nearByDriverXML.setTakeCreditCards((Boolean)nearbydriver.get("takecreditcards"));
    			nearByDriverXML.setSupportDisabled((Boolean)nearbydriver.get("supportdisabled"));
    			nearbyCompanyXML.getNearByDriverXMLs().add(nearByDriverXML);
    			nearbyCompanyXML.setAvailableDrivers(nearbyCompanyXML.getNearByDriverXMLs().size());
    			
    		}
    		
    		nearbyCompanyDriversResponseXML.setContent(nearbyCompanyXMLs);
    	
    	} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	try {
			return ServerSideClient.serializeObject(nearbyCompanyDriversResponseXML);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("Eisai xlapatsas",e);
		}
    	
    	return null;
    
    }
    */
    
    private String getDrivers(String lat, String lng, Long compId, Double multi, Long PassengerID,
			      Long candNo) throws Exception {
    	prepareNearbyCompanyDevicesStmt();
    	findNearbyCompanyDevicesStmt.setString("lat", lat);
    	findNearbyCompanyDevicesStmt.setString("multi", multi == null ? null : multi.toString());
    	findNearbyCompanyDevicesStmt.setString("lng", lng);
    	findNearbyCompanyDevicesStmt.setString("passid", PassengerID == null ? null : PassengerID + "");
    	findNearbyCompanyDevicesStmt.setString("compid", compId == null ? null : (compId + ""));
    	findNearbyCompanyDevicesStmt.setLong("numberOfDevices", candNo);
    	ResultSet rs = findNearbyCompanyDevicesStmt.executeQuery();
    	String acc = "";
    	Long prevDriver = 0L;
    	Long iTot = 0L;
    	while (rs.next()) {
    		Long driverId = rs.getLong("driverid");
    		if (prevDriver == driverId) {
    			continue;
    		}
    		prevDriver = driverId;
    		String codename = rs.getString("codename");
    		String name = rs.getString("firstname");
    		String surname = rs.getString("lastname");
    		String platenumber = rs.getString("plateno");
    		String carmodel = rs.getString("model");
    		Double lati = rs.getDouble("currentlat");
    		Double lngi = rs.getDouble("currentlong");
    		Double hailR = rs.getDouble("hailrange");
    		Double dist = rs.getDouble("distance");

    		acc +=
    				String.format(getXML("DRIVER_LIST_ITEM_LIGHT2"), driverId, codename != null ? codename : "", surname != null ?
												    surname : "", name != null ? name : "", lati, lngi, platenumber == null ? "" : platenumber,
												    		carmodel == null ? "" : carmodel, dist, hailR, rs.getString("faved"), rs.getString("speaksenglish"),
		     rs.getString("newvehicle"), rs.getString("bigteamveh"), rs.getString("supportdisabled"), rs.getString("hybrid"),
		     rs.getString("wifi"), rs.getString("smoker"), rs.getString("allowpets"), rs.getString("takecreditcards"),
		     rs.getLong("totnofavs"),rs.getLong("taxicompanyid"),rs.getString("companyinfo"),rs.getString("officialname"),rs.getString("shortname")).replace("null", "false");
    		iTot++;
    	}
    	
    	rs.close();
    	if (iTot == 0) {
    		throw new RuntimeException("shortage");

    	}
    	
    	String tmp = String.format("<companysel>%d</companysel>\n<listtype>drvlist</listtype>\n", compId == null ? 0 : compId);
    	return (tmp + String.format(getXML("MULTIPLE_DRIVER_WRAPPER"), iTot, acc));
    }


    private String getCompanies(ResultSet rs) throws Exception {
    	
    	String acc = "";
    	while (rs.next()) {
    		long taxicompanyid = rs.getLong("taxicompanyid");
    		String shortname = rs.getString("shortname");
    		String officialname = rs.getString("officialname");
    		String companytype = rs.getString("companytype");
    		String address = rs.getString("address");
    		String telephone = rs.getString("telephone");
    		String fax = rs.getString("fax");
    		String email = rs.getString("email");
    		String website = rs.getString("website");
    		double hailrange = rs.getDouble("hailrange");
    		int clearbiddingtime = rs.getInt("clearbiddingtime");
    		int requestwaittime = rs.getInt("requestwaittime");
    		int appointmentremind = rs.getInt("appointmentremind");
    		int candnumber = rs.getInt("candidatenumber");
    		int avadrivers = rs.getInt("avadrivers");
    		double mindistance = rs.getDouble("mindist");
    		//              act=list&lat=38.054024&lng=23.807006
    		String companyInfo = rs.getString("companyinfo");
    		acc +=
    				String.format(getXML("COMPANY_XML"), taxicompanyid, shortname, officialname, companytype, address, telephone,
    						fax, email, website, hailrange, clearbiddingtime, requestwaittime, appointmentremind, candnumber, companyInfo,
    						avadrivers, mindistance, "");
    	}
    	
    	return ("<listtype>complist</listtype>\n" +acc);
    }

    private String fetchNearbyCompaniesByCoordinates(String lat, String lng, Long compId, Double multi,Long PassengerID) throws Exception {
    	prepareAvailableCompaniesNearbyStmt();
    	String companiesXML = "";
    	availableCompaniesNearbyStmt.setString("lat", lat);
    	availableCompaniesNearbyStmt.setString("multi", multi == null ? null : multi.toString());
    	availableCompaniesNearbyStmt.setString("lng", lng);
    	availableCompaniesNearbyStmt.setString("passID", PassengerID == null ? null : PassengerID + "");
    	availableCompaniesNearbyStmt.setString("comp", compId == null ? null : (compId + ""));

    	ResultSet rs = availableCompaniesNearbyStmt.executeQuery();
    	companiesXML = getCompanies(rs);
    	rs.close();
    	dataPool.closeConnection();
    	return companiesXML;
    }

    private String fetchNearbyCompaniesByCoordinatesAppoint(String lat, String lng, Long compId) throws Exception {
    	prepareAvailableCompaniesNearbyAppointStmt();
    	String companiesXML = "";
    	availableCompaniesNearbyAppointStmt.setString("lat", lat);
    	availableCompaniesNearbyAppointStmt.setString("lng", lng);
    	availableCompaniesNearbyAppointStmt.setString("comp", compId == null ? null : (compId + ""));
    	
    	ResultSet rs = availableCompaniesNearbyAppointStmt.executeQuery();
    	while (rs.next()) {
    		long taxicompanyid = rs.getLong("taxicompanyid");
    		String shortname = rs.getString("shortname");
    		String officialname = rs.getString("officialname");
    		String companytype = rs.getString("companytype");
    		String address = rs.getString("address");
    		String telephone = rs.getString("telephone");
    		String fax = rs.getString("fax");
    		String email = rs.getString("email");
    		String website = rs.getString("website");
    		double hailrange = rs.getDouble("hailrange");
    		int clearbiddingtime = rs.getInt("clearbiddingtime");
    		int requestwaittime = rs.getInt("requestwaittime");
    		int appointmentremind = rs.getInt("appointmentremind");
    		int candnumber = rs.getInt("candidatenumber");
    		int avadrivers = 0; //rs.getInt("avadrivers");
    		double mindistance = 0.0; //rs.getDouble("mindist");
    		//              act=list&lat=38.054024&lng=23.807006
    		Boolean isDefault = (rs.getString("isDefault") != null && rs.getString("isDefault").equals("yes"));
    		String rect =
		       String.format(getXML("COMPANY_EXTRA_XML"), rs.getDouble("swlat"), rs.getDouble("nelat"), rs.getDouble("swlng"),
				     rs.getDouble("nelng"), isDefault ? "true" : "false");

    		String companyInfo = rs.getString("companyinfo");

    		companiesXML +=
			   String.format(getXML("COMPANY_XML"), taxicompanyid, shortname, officialname, companytype, address, telephone,
					 fax, email, website, hailrange, clearbiddingtime, requestwaittime, appointmentremind, candnumber, companyInfo,
					 avadrivers, mindistance, rect);
    	}
    	rs.close();
    	dataPool.closeConnection();
    	return companiesXML;
    }

    private Long createOrUpdateCompany(Long taxicompanyid, String shortname, String officialname, String companytype,
				       String address, String telephone, String fax, String email, String website, String hailrange,
				       String clearbiddingtime, String requestwaittime, String candidateNumber, String appointmentremind,
				       String isdefault, String nelat, String nelng, String swlat, String swlng, String locale,
				       String user) throws Exception {
    	prepareCreateOrUpdateCompany();
    	int i = 1;
    	createOrUpdateCompanyStmt.setString(i++, taxicompanyid != null ? taxicompanyid.toString() : null);
    	createOrUpdateCompanyStmt.setString(i++, shortname);
    	createOrUpdateCompanyStmt.setString(i++, officialname);
    	createOrUpdateCompanyStmt.setString(i++, companytype);
    	createOrUpdateCompanyStmt.setString(i++, address);
    	createOrUpdateCompanyStmt.setString(i++, telephone);
    	createOrUpdateCompanyStmt.setString(i++, fax);
    	createOrUpdateCompanyStmt.setString(i++, email);
    	createOrUpdateCompanyStmt.setString(i++, website);
    	createOrUpdateCompanyStmt.setString(i++, hailrange);
    	createOrUpdateCompanyStmt.setString(i++, clearbiddingtime);
    	createOrUpdateCompanyStmt.setString(i++, requestwaittime);
    	createOrUpdateCompanyStmt.setString(i++, candidateNumber);
    	createOrUpdateCompanyStmt.setString(i++, appointmentremind);
    	createOrUpdateCompanyStmt.setString(i++, isdefault);
    	createOrUpdateCompanyStmt.setString(i++, nelat);
    	createOrUpdateCompanyStmt.setString(i++, nelng);
    	createOrUpdateCompanyStmt.setString(i++, swlat);
    	createOrUpdateCompanyStmt.setString(i++, swlng);
    	createOrUpdateCompanyStmt.setString(i++, locale);
    	createOrUpdateCompanyStmt.setString(i++, user);
    	
    	createOrUpdateCompanyStmt.registerOutParameter(i++, Types.NUMERIC);
    	createOrUpdateCompanyStmt.executeUpdate();
    	taxicompanyid = createOrUpdateCompanyStmt.getLong("taxicompanyidOut");
    	dataPool.closeConnection();
    	return taxicompanyid;
    }

    private String fetchAllCompanies(String country) throws Exception {
    	ResultSet rs = null;
    	if (country == null) {
    		prepareAvailableCompaniesStmt();
    		rs = availableCompaniesStmt.executeQuery();
    	} else {
    		prepareAvailableCountryCompaniesStmt();
    		availableCountryCompaniesStmt.setString(1, country);
    		rs = availableCountryCompaniesStmt.executeQuery();
    	}
    	String companiesXML = "";

    	while (rs.next()) {
    		long taxicompanyid = rs.getLong("taxicompanyid");
    		String shortname = rs.getString("shortname");
    		String officialname = rs.getString("officialname");
    		String companytype = rs.getString("companytype");
    		String address = rs.getString("address");
    		String telephone = rs.getString("telephone");
    		String fax = rs.getString("fax");
    		String email = rs.getString("email");
    		String website = rs.getString("website");
    		double hailrange = rs.getDouble("hailrange");
    		int clearbiddingtime = rs.getInt("clearbiddingtime");
    		int requestwaittime = rs.getInt("requestwaittime");
    		int appointmentremind = rs.getInt("appointmentremind");
    		int candnumber = rs.getInt("candidatenumber");
    		int avadrivers = 0;
    		double mindistance = 0;
    		
    		//              act=list&lat=38.054024&lng=23.807006
    		String companyInfo = rs.getString("companyinfo");
    		String rect =
		       String.format(getXML("COMPANY_EXTRA_XML"), rs.getDouble("swlat"), rs.getDouble("nelat"), rs.getDouble("swlng"),
				     rs.getDouble("nelng"), "true");
    		rect += "\n" +
    				String.format(getXML("COMPANY_ADDITIONAL_INFO_XML"), rs.getDouble("Lat"), rs.getDouble("Lng"),
    						rs.getString("Country"), rs.getString("Facebook"), rs.getString("Twitter"), rs.getString("CompanyTimeZone"),
    						rs.getString("CustomerSelectable"));
    		companiesXML +=
    				String.format(getXML("COMPANY_XML"), taxicompanyid, shortname, officialname, companytype, address, telephone,
    						fax, email, website, hailrange, clearbiddingtime, requestwaittime, appointmentremind, candnumber, companyInfo,
    						avadrivers, mindistance, rect);
    	}
    	
    	rs.close();
    	dataPool.closeConnection();
    	return companiesXML;
    
    }


    protected String getPassword() {
    	return getClass().getSimpleName();
    }

	@Override
	public void processIqPacket(IQ iq) {
		// TODO Auto-generated method stub
		
	}
}
