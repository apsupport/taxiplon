package aploon.xmpp;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Type;
import org.jivesoftware.smack.proxy.ProxyInfo;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;

import aploon.Usage;
import aploon.cache.DriverCache;
import aploon.model.Driver;

import com.taxiplon.Util;


public class Communicator {

    public static final String XMPP_SERVER = Util.getProperty("xmpp_server", "config");
    public static final String XMPP_SERVICE_NAME = Util.getProperty("xmpp_service_name", "config");
    public static final String XMPP_RESOURCE = Util.getProperty("xmpp_resource", "config");
    public static final int PACKET_REPLY_TIMEOUT = 500, DEFAULT_XMPP_SERVER_PORT = 5222;
    public static final String PASS_UNAME_PREFIX = "p";
    public static final String DRIVER_UNAME = "driver";

    private static final byte[] messageKey = new byte[] { 'm', 'e', 's', 's', 'a', 'g', 'e', 'i', 'd', '_', '_', '_', '_' ,'K','e','y'};

    private static final Logger LOGGER = Logger.getLogger(Communicator.class);
    
    public static String canonicalizeUsername(String username) {
    	
    	if (!username.contains("@")) {
    		username += "@" + XMPP_SERVICE_NAME;
    	}
	
    	return username;
    }
    
    private FileTransferManager ftm;
    private FileTransferListener ftl;
    private String repoPath;
    XMPPConnection conn;
    //	Roster buddyList;

    public Communicator(String username, String password) throws XMPPException {
    	this(username, password, XMPP_RESOURCE);
    }


    public void setRepoPath(String path) {
    	repoPath = path;
    }

    public void initFileTransferManager(String path) {
    	if (ftm == null) {
    		repoPath=path;
    		File dirpath = new File(path);
    		if (!dirpath.exists()) {
    			dirpath.mkdir();
    		}
	    
    		/**
    		ftm = new FileTransferManager(conn);	    
    		ftm.addFileTransferListener(new FileTransferListener() {
    			@Override
    			public void fileTransferRequest(FileTransferRequest request) {
    				System.out.println("XMPP FILE Request received");
    				if (true) // Check to see if the request should be accepted
    				{
    					// Accept it
    					System.out.println("XMPP FILE Entering FTListener because of FTRequest");
    					IncomingFileTransfer transfer = request.accept();
    					String id = request.getDescription().replaceAll(":", "");
    					String sep = System.getProperty("file.separator");
    					String path = repoPath + sep + id;
    					File dir = new File(path);
    					if (!dir.exists()) {
    						dir.mkdir();
    					}
    					path += sep + request.getFileName();
    					
    					try {
    						System.out.println("Receiving...");
    						transfer.recieveFile(new File(path));
    						// Information put in HashMap for later retrieval
    						System.out.println("IM - putting in path (" + id + "," + path + ")");

    					} catch (XMPPException e) {


    					}
    				} else {
    					// Reject it
    					request.reject();
    				}
    			}
    		});
			*/
			
    	}

    }

    public Communicator(String username, String password, String resource) throws XMPPException {
    	
    	
    	
    	this(XMPP_SERVER, DEFAULT_XMPP_SERVER_PORT, username, password, resource);
    }

    public Communicator(String serverAddress, Integer serverPort, String username, String password,
			String resource) throws XMPPException {
    	
    	LOGGER.info("The serverAddress "+serverAddress+" the port "+serverPort);
    	
    	System.out.println("Connecting bot " + username + "...");
    	SmackConfiguration.setPacketReplyTimeout(PACKET_REPLY_TIMEOUT);
    	SmackConfiguration.setKeepAliveInterval(1000 * 30);
    	ConnectionConfiguration config = 
    			new ConnectionConfiguration(serverAddress, serverPort != null ? serverPort : DEFAULT_XMPP_SERVER_PORT,
					       XMPP_SERVICE_NAME, ProxyInfo.forDefaultProxy());
    	config.setReconnectionAllowed(false);
    	config.setSASLAuthenticationEnabled(true);
    	//		config.setSecurityMode(SecurityMode.disabled);
    	SASLAuthentication.supportSASLMechanism("PLAIN");
    	conn = new XMPPConnection(config);
    	connect(username, password, resource);

    	System.out.println("Done");
    }

    public void connect(String username, String password, String resource) throws XMPPException {
    	conn.connect();
    	try {
    		Thread.sleep(2000);
    	} catch (Exception e) {

    	}
    	conn.login(username, password, resource);
    	setStatus(true, "ON");
    	//TODO
    	
    	LOGGER.debug("Enabling auto receipts");
    	
    	DeliveryReceiptManager.getInstanceFor(conn).enableAutoReceipts();
    }

    public void setStatus(boolean available, String status) {
    	Presence presence = new Presence(available ? Type.available : Type.unavailable);
    	if (available) {
    		presence.setMode(Presence.Mode.available);
    		presence.setPriority(99);
    	}
    	presence.setStatus(status);
    	conn.sendPacket(presence);
    }

    public void destroy() throws Exception {
	conn.disconnect();
    }

    String responseBody;

    public boolean sendClearImeiMessageToDriver(Packet packet,String resource) throws XMPPException {
    	
    	String messageId = clearImeiKey(resource);
    	
    	LOGGER.info("The messageid is "+messageId);
    	
    	return sendMessage(packet, getDriverUsername(resource), messageId);
    }
    
    public boolean sendClearImeiMessageToDriver(String msg,String resource) throws XMPPException {
    	
    	String messageId = clearImeiKey(resource);
    	
    	
    	return sendMessage(msg, getDriverUsername(resource), messageId);
    }
    
    public boolean sendClearMessageToDriver(String msg,String resource) {
    	
    	String messageId = clearDriverKey(resource);
    	return sendMessage(msg, getDriverUsername(resource),messageId);
    }
    
    /**
     * Convenience method for sending messages dest driver devices (where the username is the ID src the DB).
     * @param msg
     * @param resource The resource of the driver device (imei) dest form a string of type "driver@xmpp service name/imei".
     * @return
     * @throws XMPPException
     */
    public boolean sendMessageToDriver(String msg, String resource) throws XMPPException {
    	//		if(isDriverOffline(resource)) {
    	//			return false;
    	//		}
    	//		else {
    	//return sendMessage(msg, getDriverUsername(resource));
    	//		}
    	
    	String messageKey = generateDriverKey(resource);
    	
    	//LOGGER.info("The messageKey is "+messageKey);
    	
    	return sendMessage(msg, getDriverUsername(resource),messageKey);
    	
    	/**
    	if(messageKey!=null) {
    		return sendMessage(msg, getDriverUsername(resource),messageKey);
    	} else {
    		return sendMessage(msg, getDriverUsername(resource));
    	}
    	*/
    }

    public boolean sendMessageToDriverImei(Packet packet,String resource) throws XMPPException {
    	
    	String imeiKey = generateImeiKey(resource);
    	return sendMessage(packet, getDriverUsername(resource), imeiKey);
    }
    
    public boolean sendMessageToDriverImei(String msg,String resource) throws XMPPException {
    	
    	String imeiKey = generateImeiKey(resource);
    	
    	//LOGGER.info("The imei key is "+imeiKey);
   
    	return sendMessage(msg, getDriverUsername(resource), imeiKey);
    	
    	/**
    	if(imeiKey!=null) {
    		return sendMessage(msg, getDriverUsername(resource), imeiKey);
    	} else {
    		return sendMessage(msg, getDriverUsername(resource));
    	}
    	*/
    	
    }
    
    
    /**
     * Returns the username of the driver on XMPP server (created according to the rules).
     * If resource is not specified, then the generic driver name will be returned.
     * @param resource
     * @return
     */
    public String getDriverUsername(String resource) {
    	return canonicalizeUsername(DRIVER_UNAME) + (resource != null ? "/" + resource : "");
    }
    
    /**
     * Convenience method for sending messages dest passengers (where the username is the ID src the DB + a leading prefix).
     * @param msg
     * @param to Is parsed as a String and the call is then forwarded dest the main sending method.
     * @return
     * @throws XMPPException
     */
    public boolean sendMessageToPassenger(String msg, Long to) throws XMPPException {

    	String clientId = canonicalizeUsername(PASS_UNAME_PREFIX + to);
    
    	String messageId = generatePassengerKey(to);
    	
    	LOGGER.debug("Will sent message to "+messageId+" "+clientId+" "+msg);
    	
    	return sendMessage(msg, clientId, messageId);
    	//return sendMessage(msg, clientId,messageId);
    	
    	
    	//if passenger is not online, then don't send
    	//	    Roster roster = conn.getRoster();
    	//	    if(!roster.contains(clientId)) {
    	//	        roster.createEntry(clientId, "Passenger", new String[] { "default" });
    	//	    }
    	//		Presence p = roster.getPresence(clientId);
    	//		if(p.getType() == Presence.Type.unavailable) {
    	//			return false;
    	//		}
    	//return sendMessage(msg, clientId);

    }
    
    public boolean sendClearMessageToPassenger(String msg,Long to) throws XMPPException {
    	
    	String clientId = canonicalizeUsername(PASS_UNAME_PREFIX+to);
    	String messageId = clearPassengerKey(to);
    	return sendMessage(msg, clientId,messageId);
    }

    public boolean sendMessage(Packet packet,String to,String messsageId) {
    	
    	to = canonicalizeUsername(to);
    	packet.setFrom(conn.getConnectionID());
    	packet.setTo(to);
    	if(messsageId!=null) packet.setPacketID(messsageId);
    
    	conn.sendPacket(packet);
    	return true;
    }

    public boolean sendMessage(String msg,String to,String messageId) {
    	
    	to = canonicalizeUsername(to);
    	
    	LOGGER.info("The to is "+to);
    	
    	Message xmppMsg = new Message();
    	xmppMsg.setFrom(conn.getConnectionID());
    	xmppMsg.setBody(msg);
    	if(messageId!=null) xmppMsg.setPacketID(messageId);
    	xmppMsg.setTo(to);
    	conn.sendPacket(xmppMsg);
    	return true;
    	
    }
    
    /**
     * @param msg
     * @param to
     * @return
     * @throws XMPPException
     */
    public boolean sendMessage(String msg, String to) throws XMPPException {
    	
    	LOGGER.debug("Sending to "+to+" message "+msg);
    	
    	to = canonicalizeUsername(to);
    	
    	if(to.contains("driver@driver.taxiplon.com")) {
    		String imei = to.substring(to.indexOf("/")+1);
    		return sendMessageToDriver(msg, imei);
    	} else {
    		Long passengerId = extractPassengerId(to);
    		
    		LOGGER.info("THE passenger is "+passengerId+" to ");
    		
    		if(passengerId!=null) {
    			return sendMessageToPassenger(msg, passengerId);
    		} else {
    			return sendMessage(msg, to, null);
    		}
    	}
    	
    }

    public boolean isDriverOffline(String resource) throws XMPPException {
    	Roster roster = conn.getRoster();
    	String genericDriverName = getDriverUsername(null);
    	if (!roster.contains(genericDriverName)) {
    		roster.createEntry(genericDriverName, "Generic Driver", new String[] { "default" });
    	}
    	Presence p = roster.getPresenceResource(getDriverUsername(resource));
    	//unavailable is returned when offline or not subscribed to updates (option eliminated by the code above)
    	return (p.getType() == Presence.Type.unavailable);
    }

    public XMPPConnection getConn() {
    	return conn;
    }

    public String getResponseBody() {
    	return responseBody;
    }

    public void setResponseBody(String responseBody) {
    	this.responseBody = responseBody;
    }
    
    public void changeAnAccountPassword(String uname,String oldpass,String newpass) throws XMPPException {
        
    	String secretKey = Util.getProperty("openfire_key", "config");
    	String apiUrl = Util.getProperty("openfire_account_api", "config");
    	
    	AccountAdministrator accountAdministrator = new AccountAdministrator(apiUrl, secretKey);
    	accountAdministrator.changePassword(uname, newpass);
    	     
    }

    private String generatePassengerKey(long passengerId) {
    	
    	//return null;
    	
    	//TODO String toEncrypt = passengerId+"|"+getUnixTimestamp();

    	String toEncrypt = clearPassengerKey(passengerId);
    	
    	String messageId = null;
    	
    	LOGGER.debug("Will encrypt "+toEncrypt);
    	
    	try {
			messageId = Util.encrypt(toEncrypt,messageKey);
			LOGGER.debug("The message id "+messageId);
		} catch (InvalidKeyException e) {
			LOGGER.error(e);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error(e);
		} catch (NoSuchPaddingException e) {
			LOGGER.error(e);
		} catch (IllegalBlockSizeException e) {
			LOGGER.error(e);
		} catch (BadPaddingException e) {
			LOGGER.error(e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}
		
    	return messageId;
    	
    	//return toEncrypt;
    }
    
    private String clearPassengerKey(long passengerId) {
    	
    	String toEncrypt = passengerId+"|"+getUnixTimestamp();
    	
    	return toEncrypt;
    }
    
    private String generateImeiKey(String imei) {
    
    	//return null;
    	
    	String toEncrypt = clearImeiKey(imei);
    	String messageId = null;
    	
    	if(isIosDriver(imei)) {
    		return toEncrypt;
    	}
    	
    	LOGGER.debug("Will encrypt "+toEncrypt);
    	
    	try {
    		messageId = Util.encrypt(toEncrypt,messageKey);
    		LOGGER.debug("The message id "+messageId);
    	} catch (InvalidKeyException e) {
			LOGGER.error(e);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error(e);
		} catch (NoSuchPaddingException e) {
			LOGGER.error(e);
		} catch (IllegalBlockSizeException e) {
			LOGGER.error(e);
		} catch (BadPaddingException e) {
			LOGGER.error(e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}
    	
    	return messageId;
    	//return toEncrypt;
    }
    
    private String clearImeiKey(String imei) {
    	return "||"+getUnixTimestamp()+"|"+imei;
    }
    
    private String generateDriverKey(String imei) {
    	
    	String messageId = null;
    	String toEncrypt = clearDriverKey(imei);
    	
    	if(isIosDriver(imei)) {
    		return toEncrypt;
    	}
    	
    	if(toEncrypt!=null) {
    	
    		LOGGER.info("Will encrypt "+toEncrypt);
        	
    		try {
    			messageId = Util.encrypt(toEncrypt, messageKey);
    			LOGGER.info("The message id "+messageId);
    	    } catch (InvalidKeyException e) {
    			LOGGER.error(e);
    		} catch (NoSuchAlgorithmException e) {
    			LOGGER.error(e);
    		} catch (NoSuchPaddingException e) {
    			LOGGER.error(e);
    		} catch (IllegalBlockSizeException e) {
    			LOGGER.error(e);
    		} catch (BadPaddingException e) {
    			LOGGER.error(e);
    		} catch (UnsupportedEncodingException e) {
    			LOGGER.error(e);
    		}
    	}
    	
    	return messageId;
    	//return toEncrypt;
    }

    private String clearDriverKey(String imei) {
    	
    	Driver driver = DriverCache.getDriverByImei(imei);
    
    	if(driver!=null) {
    		return driver.getDriverId()+"|"+
    				driver.getDevice().getDeviceId()+"|"+
    				getUnixTimestamp();
    	} else {
    		return null;
    	}
    }
    
    private Long extractPassengerId(String xmppUser) {
    	
    	Long passengerId = null;
    	
    	Pattern pattern = Pattern.compile("p[0-9]*@");
    	Matcher matcher = pattern.matcher(xmppUser);
    	if(matcher.find()) {
    		String passengerString = matcher.group();
    		passengerString = passengerString.replace("p", "").replace("@", "");
    		passengerId = Long.valueOf(passengerString);
    	}
    	
    	return passengerId;
    }
    
    private long getUnixTimestamp() {
    	return System.currentTimeMillis()/1000;
    }
    
    private boolean isIosDriver(String imei) {
    	
    	Driver driver = DriverCache.getDriverByImei(imei);
    	if(driver!=null&&driver.getDevice()!=null&&driver.getDevice().getSoftwareVersion()!=null) {
    		return driver.getDevice().getSoftwareVersion().contains("i");
    	} else {
    		return false;
    	}
    }
    
}
