package aploon.xmpp;

public interface ServerSideClientMBean {

	public void openConnection();

	public void closeConnection();
	
}