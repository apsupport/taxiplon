package aploon.xmpp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import org.apache.log4j.Logger;

public class AccountAdministrator {

	private final String apiUrl;
	private final String secret;
	
	private static final Logger LOGGER = Logger.getLogger(AccountAdministrator.class);
	
	public AccountAdministrator(String apiUrl,String secret) {
		this.apiUrl = apiUrl;
		this.secret = secret;
	}
	
	public void createAccount(String username, String password) {
		
		LOGGER.info("Will create account "+username+" "+password);
		
		String urlString = apiUrl+"?type=add&secret="+secret+"&username="+username+
				"&password="+password;
		try {
			
			LOGGER.info("Request at "+urlString);
			
			URL url = new URL(urlString);
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestMethod("GET");
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(httpURLConnection.getInputStream()));
			String inputLine;
			
			LOGGER.info("Response code is "+httpURLConnection.getResponseCode());
			
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		
			LOGGER.info("The response is "+inputLine);
			
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public void changePassword(String username,String newpass) {
		
		LOGGER.info("Will change account password "+username+" "+newpass);
		
		deleteAccount(username);
		createAccount(username,newpass);
	}
	
	public void deleteAccount(String username) {
		
		String urlString = apiUrl+"?type=delete&secret="+secret+"&username="+username;
		
		LOGGER.info("Will delete account "+username);
		
		try {
			
			LOGGER.info("Request at "+urlString);
			
			URL url = new URL(urlString);
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestMethod("GET");
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(httpURLConnection.getInputStream()));
			
			LOGGER.info("Response code is "+httpURLConnection.getResponseCode());
			
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		
			LOGGER.info("The response is "+inputLine);
			
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
