package aploon.locationservice.test;

import aploon.locationservice.geointteligence.GeoIntteligence;

import aploon.locationservice.geointteligence.models.ReverseGeocodingResult;

import java.math.BigDecimal;

import java.util.List;

import javax.xml.soap.SOAPException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class TestGeoIntteligence {

    private GeoIntteligence geoIntelligence;

    public TestGeoIntteligence() throws SOAPException {
        super();
        geoIntelligence = new GeoIntteligence();
    }
    
    public static void main(String args[]) {
        String[] args2 = { TestGeoIntteligence.class.getName() };
        JUnitCore.main(args2);
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @Test
    public void getReverseCord() throws Exception {
        List<ReverseGeocodingResult> reverseResults =  geoIntelligence.getAddressByCoordinates(new BigDecimal(23.715141),new BigDecimal(37.994742));    
        
        for(ReverseGeocodingResult revRes:reverseResults) {
            System.out.println(revRes.getRoadZip());    
        }    
    }

    //@Test
    public void getCordAdrres() throws Exception {
        geoIntelligence.getCoordinatesByAddress();    
    }
    
    //@Test
    public void disconect() throws Exception {
        geoIntelligence.disconnect();
    }


    @After
    public void tearDown() throws Exception {
    }
    
}
