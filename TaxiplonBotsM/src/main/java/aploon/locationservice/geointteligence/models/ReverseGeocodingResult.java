package aploon.locationservice.geointteligence.models;

import java.math.BigDecimal;

public class ReverseGeocodingResult {
   
    private BigDecimal pointX;
    private BigDecimal pointY;
    private String roadName;
    private Integer roadNumber1;
    private Integer roadNumber2;
    private Integer roadZip; 
    private String roadMunicipality;
    private BigDecimal distanceFromInputPoint;

    public void setPointX(BigDecimal pointX) {
        this.pointX = pointX;
    }

    public BigDecimal getPointX() {
        return pointX;
    }

    public void setPointY(BigDecimal pointY) {
        this.pointY = pointY;
    }

    public BigDecimal getPointY() {
        return pointY;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadNumber1(Integer roadNumber1) {
        this.roadNumber1 = roadNumber1;
    }

    public Integer getRoadNumber1() {
        return roadNumber1;
    }

    public void setRoadNumber2(Integer roadNumber2) {
        this.roadNumber2 = roadNumber2;
    }

    public Integer getRoanNumber2() {
        return roadNumber2;
    }

    public void setRoadZip(Integer roadZip) {
        this.roadZip = roadZip;
    }

    public Integer getRoadZip() {
        return roadZip;
    }

    public void setRoadMunicipality(String roadMunicipality) {
        this.roadMunicipality = roadMunicipality;
    }

    public String getRoadMunicipality() {
        return roadMunicipality;
    }

    public void setDistanceFromInputPoint(BigDecimal distanceFromInputPoint) {
        this.distanceFromInputPoint = distanceFromInputPoint;
    }

    public BigDecimal getDistanceFromInputPoint() {
        return distanceFromInputPoint;
    }
}
