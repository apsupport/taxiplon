package aploon.locationservice.geointteligence.models;

import java.math.BigDecimal;

public class GeocodeResult {
    
    private String foundAs;
    private BigDecimal geocodingScore;
    private Integer zip;
    private String municipality;
    private String addressRegion;
    private BigDecimal pointX;
    private BigDecimal pointY;
    private Integer resultType;
    private String description;


    public void setFoundAs(String foundAs) {
        this.foundAs = foundAs;
    }

    public String getFoundAs() {
        return foundAs;
    }

    public void setGeocodingScore(BigDecimal geocodingScore) {
        this.geocodingScore = geocodingScore;
    }

    public BigDecimal getGeocodingScore() {
        return geocodingScore;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public Integer getZip() {
        return zip;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setAddressRegion(String addressRegion) {
        this.addressRegion = addressRegion;
    }

    public String getAddressRegion() {
        return addressRegion;
    }

    public void setPointX(BigDecimal pointX) {
        this.pointX = pointX;
    }

    public BigDecimal getPointX() {
        return pointX;
    }

    public void setPointY(BigDecimal pointY) {
        this.pointY = pointY;
    }

    public BigDecimal getPointY() {
        return pointY;
    }

    public void setResultType(Integer resultType) {
        this.resultType = resultType;
    }

    public Integer getResultType() {
        return resultType;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
