package aploon.locationservice.geointteligence;

import aploon.locationservice.geointteligence.parsers.GeoIntteligenceparser;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DisconnectResponseParser extends GeoIntteligenceparser {
    
    /*
     * @deprecated Bad idea to implement
     */
    public DisconnectResponseParser(String input) throws IOException,
                                                         ParserConfigurationException,
                                                         SAXException {

        InputSource inputSource = getInputSource(input);
        
    }
}
