package aploon.locationservice.geointteligence;

import aploon.locationservice.LocationService;

import aploon.locationservice.geointteligence.models.ReverseGeocodingResult;
import aploon.locationservice.geointteligence.parsers.AuthInfoParser;

import aploon.locationservice.geointteligence.parsers.GeocodeAddressParser;

import aploon.locationservice.geointteligence.parsers.ReverseGeocodeAddressParser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;


public class GeoIntteligence implements LocationService {
    
    private static final String serverUrl = "http://ws.ngi.gr/NGIMapServer/soap/INGIMapServer";
    private SOAPConnection soapConnection;    
    private String connectionHandle;

    private static final String CONNECT_XML = "<ConParams user=\"%s\" pass=\"%s\" enckey=\"%s\"/>";
    
    private static final String AUTHINFO_XML = "<AuthInfo connectionHandle=\"%s\"/>";
    
    private static final String REVERSE_GEOCODING_XML = "<ReverseGeocodingRequest pointX=\"%f\" pointY=\"%f\" pointAzimuth=\"\" returnRoadCentroid=\"\" distanceTolerance=\"\" azimuthTolerance=\"\" coordinateSystem=\"WGS84\" />";
    
    private static final String GEOCODE_ADDRESS_XML = "<GeocodeAddressRequest>"+
"<GeocodeAddress id=\"1\" addressName=\"??????\" addressNumber=\"5\" addressZip=\"\" addressLocation=\"?????\" minScore=\"0.8\" maxResults=\"3\"/>"+
"<GeocodeAddress id=\"2\" addressName=\"????????\" addressNumber=\"6\" addressZip=\"\" addressLocation=\"\" minScore=\"0.9\" maxResults=\"1\" coordinateSystem=\"WGS84\"/>"+
"</GeocodeAddressRequest>";
        
    public GeoIntteligence() throws SOAPException {
        super();
        
        soapConnection = SOAPConnectionFactory.newInstance().createConnection();
        login();
    }

    /*
     * Login and retrieve the connectionHandle string;
     */
    private void login() throws SOAPException {
        
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        SOAPElement bodyElement = formatSOAPMessage(soapMessage, "Connect");
        
        SOAPElement connectParameters = bodyElement.addChildElement("ConnectParameters");                                           
        connectParameters.setAttribute("xsi:type", "xsd:string");
        
        connectParameters.setTextContent(String.format(CONNECT_XML,"vbagiartakis@taxiplon.com","JK34bagia2XL",""));
        
        SOAPMessage retmes = soapConnection.call(soapMessage,serverUrl);
        SOAPBody retbody = retmes.getSOAPPart().getEnvelope().getBody();  
        
        Node connectResponse = retbody.getElementsByTagName("ns:ConnectResponse").item(0);
        
        Node authInfo = connectResponse.getLastChild();
            
        try {
            AuthInfoParser authInfoParser = new AuthInfoParser(authInfo.getTextContent());
            connectionHandle = authInfoParser.getConnectionHandle();
            
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void getAvailableLayers() {
        
    }
    
    /*
     * Disconnecting from the Geointteligence Service
     */
    public void disconnect() throws SOAPException {
    
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        SOAPElement bodyElement = formatSOAPMessage(soapMessage, "Disconnect");
        
        SOAPElement connectParameters = bodyElement.addChildElement("authInfo");
        connectParameters.setAttribute("xsi:type", "xsd:string");
        connectParameters.setTextContent(String.format(AUTHINFO_XML,connectionHandle));
        SOAPMessage retmes = soapConnection.call(soapMessage,serverUrl);
        
        soapConnection.close();    
    }
    
    public List<ReverseGeocodingResult> getAddressByCoordinates(BigDecimal lat, BigDecimal lng) throws SOAPException {
        
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        SOAPElement bodyElement = formatSOAPMessage(soapMessage, "ReverseGeocode");                                                      
        SOAPElement connectParameters = bodyElement.addChildElement("authInfo");
        connectParameters.setAttribute("xsi:type", "xsd:string");
        connectParameters.setTextContent(String.format(AUTHINFO_XML,connectionHandle));
        SOAPElement geocodeParameters = bodyElement.addChildElement("ReverseGeocodeParameters");
        geocodeParameters.setAttribute("xsi:type", "xsd:string");
        geocodeParameters.setTextContent(String.format(REVERSE_GEOCODING_XML,lat,lng));
        SOAPMessage returnMessage = soapConnection.call(soapMessage, serverUrl);
        
        SOAPBody returnBody = returnMessage.getSOAPPart().getEnvelope().getBody();
        Node reverseGeocodeResponse = returnBody.getElementsByTagName("ns:ReverseGeocodeResponse").item(0);
        Node geocodeResults = reverseGeocodeResponse.getLastChild();
        
        try {
            return new ReverseGeocodeAddressParser(geocodeResults.getTextContent()).getReverseGeocodingResults();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    /*
     * Giving the address in order to get the geocoordinates
     */
    public void getCoordinatesByAddress() throws SOAPException {
    
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        SOAPElement bodyElement = formatSOAPMessage(soapMessage, "GeocodeAddress");                                                      
        SOAPElement connectParameters = bodyElement.addChildElement("authInfo");
        connectParameters.setAttribute("xsi:type", "xsd:string");
        connectParameters.setTextContent(String.format(AUTHINFO_XML,connectionHandle));
        SOAPElement geocodeParameters = bodyElement.addChildElement("GeocodeAddressParameters");
        geocodeParameters.setAttribute("xsi:type", "xsd:string");
        geocodeParameters.setTextContent(GEOCODE_ADDRESS_XML);
        SOAPMessage returnMessage = soapConnection.call(soapMessage, serverUrl);
        
        SOAPBody returnBody = returnMessage.getSOAPPart().getEnvelope().getBody();
        Node connectResponse = returnBody.getElementsByTagName("ns:GeocodeAddressResponse").item(0);
        Node geocodeResults = connectResponse.getLastChild();
        
        try {
            new GeocodeAddressParser(geocodeResults.getTextContent());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
    
    /*
     * The formatSOAPMessage method helps to avoid the whole boilerplate
     */
    private SOAPElement formatSOAPMessage(SOAPMessage soapMessage,String bodyName) throws SOAPException {
        
        soapMessage.getSOAPHeader().detachNode();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.setAttribute("xmlns:ns1","urn:NGIMapServerIntf-INGIMapServer");
        envelope.setAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
        envelope.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance"); 
        envelope.setAttribute("SOAP-ENV:encodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        SOAPBody body = envelope.getBody();
        SOAPElement bodyElement = body.addChildElement(envelope.createName(bodyName , "ns1", ""));                                                      

        return bodyElement;
    }
    
    private static void printSoapMessage(SOAPMessage msg) {
        System.out.println(getXmlFromSOAPMessage(msg));
    }
    
    private static String getXmlFromSOAPMessage(SOAPMessage msg)  {
        try {
            ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
            msg.writeTo(byteArrayOS);
            return new String(byteArrayOS.toByteArray());
        } catch (SOAPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return null;
    }
}
