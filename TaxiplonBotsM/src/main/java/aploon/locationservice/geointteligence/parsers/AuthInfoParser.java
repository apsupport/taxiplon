package aploon.locationservice.geointteligence.parsers;


import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class AuthInfoParser extends GeoIntteligenceparser {
 
    private String connectionHandle;

    public AuthInfoParser(String input) throws IOException,
                                               ParserConfigurationException,
                                               SAXException {
        
        InputSource inputSource = getInputSource(input);
        parseXml(inputSource,new AuthInfoHandler());
    }

    private class AuthInfoHandler extends DefaultHandler {

        @Override
        public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
            for(int i=0;i<attributes.getLength();i++) {
                if(attributes.getLocalName(i)=="connectionHandle") {
                    connectionHandle = attributes.getValue(i);
                }
            }
        }
    }

    public String getConnectionHandle() {
        return connectionHandle;    
    }
}   
