package aploon.locationservice.geointteligence.parsers;

import aploon.locationservice.geointteligence.models.ReverseGeocodingResult;

import java.io.IOException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ReverseGeocodeAddressParser extends GeoIntteligenceparser {
    
    private List<ReverseGeocodingResult> reverseGeocodingResults;
    
    public ReverseGeocodeAddressParser(String input) throws IOException,
                                                            ParserConfigurationException,
                                                            SAXException {
    
        InputSource inputSource = getInputSource(input);
        parseXml(inputSource, new ReverseGeocodeAddressHandler());
    }
    
    private class ReverseGeocodeAddressHandler extends DefaultHandler {
     
        @Override
        public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
            
            if(qName.equals("ReverseGeocodingResponse")) {
                
                reverseGeocodingResults = new ArrayList<ReverseGeocodingResult>();    
            }
            
            if(qName.equals("ReverseGeocodingResult")) {
                
                ReverseGeocodingResult reverseGeocodingResult = new ReverseGeocodingResult();
                
                for(int i=0;i<attributes.getLength();i++) {
                    String type = attributes.getLocalName(i);
                    String value = attributes.getValue(i);
                    
                    if(type.equals("pointX ")) {
                        reverseGeocodingResult.setPointX(new BigDecimal(value));
                    } else if(type.equals("pointY")) {
                        reverseGeocodingResult.setPointY(new BigDecimal(value));    
                    } else if(type.equals("roadName")) {
                        reverseGeocodingResult.setRoadName(value);
                    } else if(type.equals("roadNumber1")) {
                        reverseGeocodingResult.setRoadNumber1(Integer.parseInt(value));    
                    } else if(type.equals("roadNumber2")) {
                        reverseGeocodingResult.setRoadNumber2(Integer.parseInt(value));    
                    } else if(type.equals("roadZip")) {
                        reverseGeocodingResult.setRoadZip(Integer.parseInt(value));  
                    } else if(type.equals("roadMunicipality")) {
                        reverseGeocodingResult.setRoadMunicipality(value);
                    } else if(type.equals("distanceFromInputPoint")) {
                        reverseGeocodingResult.setDistanceFromInputPoint(new BigDecimal(value));
                    }
                    
                    reverseGeocodingResults.add(reverseGeocodingResult);
                }    
            }
        }           
    }
    
    public List<ReverseGeocodingResult> getReverseGeocodingResults() {
        return reverseGeocodingResults;    
    }
}
