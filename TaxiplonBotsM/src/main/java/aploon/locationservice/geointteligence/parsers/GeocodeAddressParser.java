package aploon.locationservice.geointteligence.parsers;

import aploon.locationservice.geointteligence.models.GeocodeResult;

import java.io.IOException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class GeocodeAddressParser extends GeoIntteligenceparser {

    List<GeocodeResult> geocodeResults;

    public GeocodeAddressParser(String input) throws IOException,
                                                     ParserConfigurationException,
                                                     SAXException {
    
        InputSource inputSource = getInputSource(input);
        parseXml(inputSource, new GeocodeAddressHandler());
    }

    private class GeocodeAddressHandler extends DefaultHandler {
        
        @Override
        public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
               
            System.out.println("uri "+uri+" localname "+localName+" qname "+qName);
                  
            if(qName.equals("GeocodeResults")) {
                geocodeResults = new ArrayList<GeocodeResult>();    
            }
                  
            if(qName.equals("GeocodeResult")) {
                
                GeocodeResult geocodeResult = new GeocodeResult();
                
                for(int i=0;i<attributes.getLength();i++) {
                    String type = attributes.getType(i);
                    String value = attributes.getValue(i);
                    
                    if(type.equals("Description")) {
                        geocodeResult.setDescription(value);
                    } else if(type.equals("resultType")) {
                        geocodeResult.setResultType(Integer.parseInt(value));
                    } else if(type.equals("pointX")) {
                        geocodeResult.setPointX(new BigDecimal(value));    
                    } else if(type.equals("pointY")) {
                        geocodeResult.setPointY(new BigDecimal(value));
                    }
                    
                    if(geocodeResults!=null) {
                        geocodeResults.add(geocodeResult);       
                    }
                }    
            }
        }
    }
    
    public List<GeocodeResult> getGeocodeResults() {
    
        return geocodeResults;        
    }
}
