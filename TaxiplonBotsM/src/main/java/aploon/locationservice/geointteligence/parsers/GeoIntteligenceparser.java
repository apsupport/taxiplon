package aploon.locationservice.geointteligence.parsers;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class GeoIntteligenceparser {
    
    /*
     * The encoding is on windows-1253
     */
    protected InputSource getInputSource (String xmlString) throws IOException {       
        InputSource inputSource = new InputSource(new ByteArrayInputStream(xmlString.getBytes()));
        //inputSource.setEncoding("UTF-8");
        inputSource.setEncoding("windows-1253");
        return inputSource;
    }
    
    protected void parseXml(InputSource inputSource,DefaultHandler defaultHandler) throws ParserConfigurationException,
                                                            SAXException,
                                                                  IOException {
        SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
        saxParser.parse(inputSource, defaultHandler);
    }
}
