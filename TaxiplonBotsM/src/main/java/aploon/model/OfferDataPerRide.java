package aploon.model;

public class OfferDataPerRide {

	private Long offerDataPerRideID;
	private Long taxiCompanyID;
	private Long passengerID;
	private Integer dispatcherCalls;
	private Integer smartPhoneCalls;
	private Integer webCalls;
	
	public Long getOfferDataPerRideID() {
		return offerDataPerRideID;
	}
	
	public void setOfferDataPerRideID(Long offerDataPerRideID) {
		this.offerDataPerRideID = offerDataPerRideID;
	}
	
	public Long getTaxiCompanyID() {
		return taxiCompanyID;
	}
	
	public void setTaxiCompanyID(Long taxiCompanyID) {
		this.taxiCompanyID = taxiCompanyID;
	}
	
	public Long getPassengerID() {
		return passengerID;
	}
	
	public void setPassengerID(Long passengerID) {
		this.passengerID = passengerID;
	}
	
	public Integer getDispatcherCalls() {
		return dispatcherCalls;
	}
	
	public void setDispatcherCalls(Integer dispatcherCalls) {
		this.dispatcherCalls = dispatcherCalls;
	}
	
	public Integer getSmartPhoneCalls() {
		return smartPhoneCalls;
	}
	
	public void setSmartPhoneCalls(Integer smartPhoneCalls) {
		this.smartPhoneCalls = smartPhoneCalls;
	}
	
	public Integer getWebCalls() {
		return webCalls;
	}
	
	public void setWebCalls(Integer webCalls) {
		this.webCalls = webCalls;
	}
	
}
