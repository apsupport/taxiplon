package aploon.model;

public class TaxiCompany {

    private Long taxiCompanyId;
    private String shortName;
    private String SMSGateway;
    private String SMSFromName;
    private String SMSUsername;
    private String SMSPassword;

    public void setTaxiCompanyId(Long taxiCompanyId) {
        this.taxiCompanyId = taxiCompanyId;
    }

    public Long getTaxiCompanyId() {
        return taxiCompanyId;
    }
    
    public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public void setSMSGateway(String SMSGateway) {
        this.SMSGateway = SMSGateway;
    }

    public String getSMSGateway() {
        return SMSGateway;
    }

    public void setSMSFromName(String SMSFromName) {
        this.SMSFromName = SMSFromName;
    }

    public String getSMSFromName() {
        return SMSFromName;
    }

    public void setSMSUsername(String SMSUsername) {
        this.SMSUsername = SMSUsername;
    }

    public String getSMSUsername() {
        return SMSUsername;
    }

    public void setSMSPassword(String SMSPassword) {
        this.SMSPassword = SMSPassword;
    }

    public String getSMSPassword() {
        return SMSPassword;
    }

}
