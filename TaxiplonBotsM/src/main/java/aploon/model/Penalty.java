package aploon.model;

public class Penalty {

	private Long penaltyId;
	private Long taxiCompanyId;
	private String name;
	private Integer minutes;
	private Integer perMinutes;
	private Integer offSet;
	
	public Long getPenaltyId() {
		return penaltyId;
	}
	
	public void setPenaltyId(Long penaltyId) {
		this.penaltyId = penaltyId;
	}
	
	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}
	
	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getMinutes() {
		return minutes;
	}

	public void setMinutes(Integer minutes) {
		this.minutes = minutes;
	}

	public Integer getPerMinutes() {
		return perMinutes;
	}
	
	public void setPerMinutes(Integer perMinutes) {
		this.perMinutes = perMinutes;
	}

	public Integer getOffSet() {
		return offSet;
	}

	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}
	
	public enum Event {
		delay,expire,cancel
	}
	
}
