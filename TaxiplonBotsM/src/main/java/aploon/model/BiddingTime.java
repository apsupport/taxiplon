package aploon.model;

public class BiddingTime {
	
	private Long biddingTime;//bidding expiry time
	private Long waitTime;//request expiry time
	private Long hailTime;//current system time
	private boolean dispatched;//denotes whether the trip was actually dispatched
		
	public BiddingTime(Integer biddingTime, Integer waitTime, Long hailTime, boolean dispatched) {
		this.biddingTime = hailTime + biddingTime * 1000;
		this.waitTime = this.biddingTime + waitTime * 1000;
		this.hailTime = hailTime;
		this.dispatched = dispatched;
	}

	public void setBiddingTime(Long biddingTime) {
		this.biddingTime = biddingTime;
	}

	public Long getBiddingTime() {
		return biddingTime;
	}

	public void setWaitTime(Long waitTime) {
		this.waitTime = waitTime;
	}

	public Long getWaitTime() {
		return waitTime;
	}

	public void setHailTime(Long systemTime) {
		this.hailTime = systemTime;
	}

	public Long getHailTime() {
		return hailTime;
	}

	public void setDispatched(boolean dispatched) {
		this.dispatched = dispatched;
	}

	public boolean isDispatched() {
		return dispatched;
	}
}
