package aploon.model;

import java.sql.Timestamp;

public class TripHistoryModel {
    
    private Long tripid;
    private Timestamp hailtime;
    private Timestamp assigntime;
    private Timestamp pickuptime;
    private String from;
    private String to;
    private String special;
    private String status;
    private String bookAgent;
    private Integer rating;
    private String plateno;
    private String carmodel;
    private String drivername;
    private String driversurname;
    private String drivercodename;
    private String isFavorite;
    private Double amount;
    private String curr;
    private String chan;
    private String email;
    private String facebook;
    private String twitter;
    private Long driverId;
    private String isblocked;

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTo() {
        return to;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getSpecial() {
        return special;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setBookAgent(String bookAgent) {
        this.bookAgent = bookAgent;
    }

    public String getBookAgent() {
        return bookAgent;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getRating() {
        return rating;
    }

    public void setPlateno(String plateno) {
        this.plateno = plateno;
    }

    public String getPlateno() {
        return plateno;
    }

    public void setCarmodel(String carmodel) {
        this.carmodel = carmodel;
    }

    public String getCarmodel() {
        return carmodel;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDriversurname(String driversurname) {
        this.driversurname = driversurname;
    }

    public String getDriversurname() {
        return driversurname;
    }

    public void setDrivercodename(String drivercodename) {
        this.drivercodename = drivercodename;
    }

    public String getDrivercodename() {
        return drivercodename;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public String getCurr() {
        return curr;
    }

    public void setChan(String chan) {
        this.chan = chan;
    }

    public String getChan() {
        return chan;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setIsblocked(String isblocked) {
        this.isblocked = isblocked;
    }

    public String getIsblocked() {
        return isblocked;
    }

    public void setTripid(Long tripid) {
        this.tripid = tripid;
    }

    public Long getTripid() {
        return tripid;
    }

    public void setHailtime(Timestamp hailtime) {
        this.hailtime = hailtime;
    }

    public Timestamp getHailtime() {
        return hailtime;
    }

    public void setPickuptime(Timestamp pickuptime) {
        this.pickuptime = pickuptime;
    }

    public Timestamp getPickuptime() {
        return pickuptime;
    }

    public void setAssigntime(Timestamp assigntime) {
        this.assigntime = assigntime;
    }

    public Timestamp getAssigntime() {
        return assigntime;
    }
}
