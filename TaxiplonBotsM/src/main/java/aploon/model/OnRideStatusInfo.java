package aploon.model;

import java.sql.Timestamp;

public class OnRideStatusInfo {

	private Timestamp pickUpTime;
	private Integer pickedRideDelay;
	
	public Timestamp getPickUpTime() {
		return pickUpTime;
	}
	
	public void setPickUpTime(Timestamp pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	
	public Integer getPickedRideDelay() {
		return pickedRideDelay;
	}
	
	public void setPickedRideDelay(Integer pickedRideDelay) {
		this.pickedRideDelay = pickedRideDelay;
	}
	
}
