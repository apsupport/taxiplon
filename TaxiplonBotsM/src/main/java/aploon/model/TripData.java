package aploon.model;

import java.security.Timestamp;

public class TripData {

	Long taxicompanyId;
    String assignmentMsg;
    String gaddress;
    Long passengerId;
    String status;
    boolean isDiscounted;
    String locale;
    String extra;
    String bookagent;
    String country;
    String mobile;
    String contactPhone;
    String version;
    
    Double lat;
    Double lng;
    Long hailTime;
    String special;
    String passengerName;
    String destination;
    String gender;
    String companyName;
    Double fare;
    String paymentType;
    
    public void setAssignmentMsg(String assignmentMsg) {
	this.assignmentMsg = assignmentMsg;
    }

    public String getAssignmentMsg() {
	return assignmentMsg;
    }

    public void setGaddress(String gaddress) {
	this.gaddress = gaddress;
    }

    public String getGaddress() {
	return gaddress;
    }

    public void setPassengerId(Long passengerId) {
	this.passengerId = passengerId;
    }

    public Long getPassengerId() {
	return passengerId;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getStatus() {
	return status;
    }

    public void setIsDiscounted(boolean isDiscounted) {
	this.isDiscounted = isDiscounted;
    }

    public boolean getIsDiscounted() {
	return isDiscounted;
    }

    public void setLocale(String locale) {
	this.locale = locale;
    }

    public String getLocale() {
	return locale;
    }

    public void setExtra(String extra) {
	this.extra = extra;
    }

    public String getExtra() {
	return extra;
    }

    public void setBookagent(String bookagent) {
	this.bookagent = bookagent;
    }

    public String getBookagent() {
	return bookagent;
    }

    public void setPassengerCountry(String country) {
	this.country = country;
    }

    public String getPassengerCountry() {
	return country;
    }

    public void setMobile(String mobile) {
	this.mobile = mobile;
    }

    public String getMobile() {
	return mobile;
    }

    public void setContactPhone(String contactPhone) {
	this.contactPhone = contactPhone;
    }

    public String getContactPhone() {
	return contactPhone;
    }

	public Long getTaxicompanyId() {
		return taxicompanyId;
	}

	public void setTaxicompanyId(Long taxicompanyId) {
		this.taxicompanyId = taxicompanyId;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	

	public Long getHailTime() {
		return hailTime;
	}

	public void setHailTime(Long hailTime) {
		this.hailTime = hailTime;
	}

	public String getSpecial() {
		return special;
	}

	public void setSpecial(String special) {
		this.special = special;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Double getFare() {
		return fare;
	}

	public void setFare(Double fare) {
		this.fare = fare;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	
}
