package aploon.model;

import java.sql.Timestamp;

public class Offer {

	public static enum Type {
		perride
	}
	
	private Long offerId;
	private Long taxiCompanyId;
	private Type type;
	private Timestamp startDate;
	private Timestamp endDate;
	
	public Long getOfferId() {
		return offerId;
	}
	
	public void setOfferId(Long offerId) {
		this.offerId = offerId;
	}
	
	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}
	
	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public Timestamp getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	
	public Timestamp getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	
}
