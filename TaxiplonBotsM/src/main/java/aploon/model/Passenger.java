package aploon.model;

import java.sql.Timestamp;

public class Passenger {

    private Long passengerId;
    private String imei;
    private String mobile;
    private String pin;
    private Long taxiCompanyId;
    private Timestamp regDate;
    
    public void setPassengerId(Long passengerId) {
        this.passengerId = passengerId;
    }

    public Long getPassengerId() {
        return passengerId;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImei() {
        return imei;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}

	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}

	public Timestamp getRegDate() {
		return regDate;
	}

	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}
    
	
}
