package aploon.model;

public class OfferPerRide {

	public static enum CallType {
		s,w,d,sw,sd,wd,all	
	};
	
	private Long offerPerRideID;
	private Long taxiCompanyID;
	private CallType callType;
	private Integer rides;
	private Double discount;
	
	public Long getOfferPerRideID() {
		return offerPerRideID;
	}
	
	public void setOfferPerRideID(Long offerPerRideID) {
		this.offerPerRideID = offerPerRideID;
	}
	
	public Long getTaxiCompanyID() {
		return taxiCompanyID;
	}
	
	public void setTaxiCompanyID(Long taxiCompanyID) {
		this.taxiCompanyID = taxiCompanyID;
	}
	
	public CallType getCallType() {
		return callType;
	}
	
	public void setCallType(CallType callType) {
		this.callType = callType;
	}
	
	public Integer getRides() {
		return rides;
	}
	
	public void setRides(Integer rides) {
		this.rides = rides;
	}
	
	public Double getDiscount() {
		return discount;
	}
	
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
}
