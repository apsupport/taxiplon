package aploon.model;

import java.sql.Timestamp;

public class DriverClaimedTrip {

	private Long tripId;
	private Timestamp hailTime;
	private String pickUpAddress;
	private String shortName;
	private String comments;
	private Double latitude;
	private Double longtitude;
	private String specialNeeds;
	
	public Long getTripId() {
		return tripId;
	}
	
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	
	public Timestamp getHailTime() {
		return hailTime;
	}
	
	public void setHailTime(Timestamp hailTime) {
		this.hailTime = hailTime;
	}
	
	public String getPickUpAddress() {
		return pickUpAddress;
	}
	
	public void setPickUpAddress(String pickUpAddress) {
		this.pickUpAddress = pickUpAddress;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(Double longtitude) {
		this.longtitude = longtitude;
	}

	public String getSpecialNeeds() {
		return specialNeeds;
	}

	public void setSpecialNeeds(String specialNeeds) {
		this.specialNeeds = specialNeeds;
	}
	
}