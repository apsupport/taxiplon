package aploon.model;

public class Port {

	private String portName;
	private long range;
	private Coordinates location;
	private String marineTrafficId;
	
	public String getPortName() {
		return portName;
	}
	
	public void setPortName(String portName) {
		this.portName = portName;
	}
	
	public long getRange() {
		return range;
	}
	
	public void setRange(long range) {
		this.range = range;
	}
	
	public Coordinates getLocation() {
		return location;
	}
	
	public void setLocation(Coordinates location) {
		this.location = location;
	}

	public String getMarineTrafficId() {
		return marineTrafficId;
	}

	public void setMarineTrafficId(String marineTrafficId) {
		this.marineTrafficId = marineTrafficId;
	}
	
}
