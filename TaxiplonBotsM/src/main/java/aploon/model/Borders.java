package aploon.model;

import java.math.BigDecimal;

public class Borders {

	private Coordinates northEast;
	private Coordinates southWest;

	public Borders(Coordinates northEast, Coordinates southWest) {
		this.northEast = northEast;
		this.southWest = southWest;
	}

	public BigDecimal getLatSpan() {
		return new BigDecimal(northEast.getLat() - southWest.getLat());
	}

	public BigDecimal getLngSpan() {
		return new BigDecimal(northEast.getLng() - southWest.getLng());
	}

	public void setNorthEast(Coordinates northEast) {
		this.northEast = northEast;
	}

	public Coordinates getNorthEast() {
		return northEast;
	}

	public void setSouthWest(Coordinates southWest) {
		this.southWest = southWest;
	}

	public Coordinates getSouthWest() {
		return southWest;
	}
}
