package aploon.model;

import java.sql.Timestamp;

public class DriverPenalty {

	private Long driverPenaltyId;
	private Long penaltyId;
	private Long tripId;
	private Long driverId;
	private Long userId;
	private Status status;
	private Timestamp startTime;
	private Timestamp endTime;
	private String comment;
	
	public Long getDriverPenaltyId() {
		return driverPenaltyId;
	}

	public void setDriverPenaltyId(Long driverPenaltyId) {
		this.driverPenaltyId = driverPenaltyId;
	}

	public Long getPenaltyId() {
		return penaltyId;
	}

	public void setPenaltyId(Long penaltyId) {
		this.penaltyId = penaltyId;
	}

	public Long getTripId() {
		return tripId;
	}

	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public enum Status {
		active,inactive,removed
	}


}
