package aploon.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Trip {

	private Long tripId;
	private Timestamp arrivalTime;
	private Timestamp hailTime;
	private Timestamp assignTime;
	private String pickUpAddress;
    private Long callTypeId;
    private Long driverId;
    private Long zoneId;
    private String destination;
	private Long taxiCompanyId;
    private Timestamp cancelTime;
	private Timestamp pickUpTime;
    private BigDecimal creditCost;
	private Integer timeToPickUp;
    
    public Long getTripId() {
		return tripId;
	}
	
    public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
    
	public Timestamp getHailTime() {
		return hailTime;
	}

	public void setHailTime(Timestamp hailTime) {
		this.hailTime = hailTime;
	}
	
	public Timestamp getAssignTime() {
		return assignTime;
	}

	public void setAssignTime(Timestamp assignTime) {
		this.assignTime = assignTime;
	}

	public Timestamp getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Timestamp arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getPickUpAddress() {
		return pickUpAddress;
	}
	
	public void setPickUpAddress(String pickUpAddress) {
		this.pickUpAddress = pickUpAddress;
	}
	
	public Long getCallTypeId() {
		return callTypeId;
	}
	
	public void setCallTypeId(Long callTypeId) {
		this.callTypeId = callTypeId;
	}
	
	public Long getDriverId() {
		return driverId;
	}
	
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public Long getZoneId() {
		return zoneId;
	}
	
	public void setZoneId(Long zoneId) {
		this.zoneId = zoneId;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}

	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}

	public Timestamp getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(Timestamp cancelTime) {
		this.cancelTime = cancelTime;
	}

	public Timestamp getPickUpTime() {
		return pickUpTime;
	}

	public void setPickUpTime(Timestamp pickUpTime) {
		this.pickUpTime = pickUpTime;
	}

	public BigDecimal getCreditCost() {
		return creditCost;
	}

	public void setCreditCost(BigDecimal creditCost) {
		this.creditCost = creditCost;
	}

	public Integer getTimeToPickUp() {
		return timeToPickUp;
	}

	public void setTimeToPickUp(Integer timeToPickUp) {
		this.timeToPickUp = timeToPickUp;
	}
	
}
