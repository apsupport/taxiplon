package aploon.model;

import java.sql.Time;

public class Zone {
	
	private Long zoneId;
	private Long taxiCompanyId;
	private String zoneName;
	private Double zoneLat;
	private Double zoneLong;
	private Double zoneRange;
	private Integer driversInZone;
	private Boolean kickDrivers;
	private Boolean strictBorders;
    private Time opensAt;
    private Time closesAt;
    private Boolean zoneAdvantage;
    private Long mergesWithZoneId;
    private Boolean active;

	private String zonePoints;
	
	public Long getZoneId() {
		return zoneId;
	}
	
	public void setZoneId(Long zoneId) {
		this.zoneId = zoneId;
	}
	
	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}
	
	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}
	
	public String getZoneName() {
		return zoneName;
	}
	
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	
	public Double getZoneLat() {
		return zoneLat;
	}
	
	public void setZoneLat(Double zoneLat) {
		this.zoneLat = zoneLat;
	}
	
	public Double getZoneLong() {
		return zoneLong;
	}
	
	public void setZoneLong(Double zoneLong) {
		this.zoneLong = zoneLong;
	}
	
	public Double getZoneRange() {
		return zoneRange;
	}
	
	public void setZoneRange(Double zoneRange) {
		this.zoneRange = zoneRange;
	}

	public Integer getDriversInZone() {
		return driversInZone;
	}

	public void setDriversInZone(Integer driversInZone) {
		this.driversInZone = driversInZone;
	}

	public Boolean getKickDrivers() {
		return kickDrivers;
	}

	public void setKickDrivers(Boolean kickDrivers) {
		this.kickDrivers = kickDrivers;
	}

	public Boolean getStrictBorders() {
		return strictBorders;
	}

	public void setStrictBorders(Boolean strictBorders) {
		this.strictBorders = strictBorders;
	}

	public String getZonePoints() {
		return zonePoints;
	}

	public void setZonePoints(String zonePoints) {
		this.zonePoints = zonePoints;
	}

	public Time getOpensAt() {
		return opensAt;
	}

	public void setOpensAt(Time opensAt) {
		this.opensAt = opensAt;
	}

	public Time getClosesAt() {
		return closesAt;
	}

	public void setClosesAt(Time closesAt) {
		this.closesAt = closesAt;
	}

	public Long getMergesWithZoneId() {
		return mergesWithZoneId;
	}

	public void setMergesWithZoneId(Long mergesWithZoneId) {
		this.mergesWithZoneId = mergesWithZoneId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getZoneAdvantage() {
		return zoneAdvantage;
	}

	public void setZoneAdvantage(Boolean zoneAdvantage) {
		this.zoneAdvantage = zoneAdvantage;
	}
	
}
