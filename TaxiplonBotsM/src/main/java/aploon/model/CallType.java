package aploon.model;

public class CallType {

	private Long callTypeId;
	private Long taxiCompanyId;
	private String name;
	private Category category;
	private Boolean forceAssignment;
	private Double distance;
	private Double lastCall;
	private Double passengerFrequency;
	private Double zoneOrder;
	private Integer candidateNumber;
	private Double hailRange;
	private Integer clearBiddingTime;
	private Integer requestWaitTime;
	private Integer timeToPickUp;
	private Integer samePassengerDays;
	private Long alternateCallTypeId;
	private Integer expirationsAllowed;
	
	public Long getCallTypeId() {
		return callTypeId;
	}
	
	public void setCallTypeId(Long callTypeId) {
		this.callTypeId = callTypeId;
	}
	
	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}
	
	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getForceAssignment() {
		return forceAssignment;
	}

	public void setForceAssignment(Boolean forceAssignment) {
		this.forceAssignment = forceAssignment;
	}

	public Double getDistance() {
		return distance;
	}
	
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public Double getLastCall() {
		return lastCall;
	}
	
	public void setLastCall(Double lastCall) {
		this.lastCall = lastCall;
	}
	
	public Double getPassengerFrequency() {
		return passengerFrequency;
	}
	
	public void setPassengerFrequency(Double passengerFrequency) {
		this.passengerFrequency = passengerFrequency;
	}
	
	public Integer getCandidateNumber() {
		return candidateNumber;
	}
	
	public void setCandidateNumber(Integer candidateNumber) {
		this.candidateNumber = candidateNumber;
	}
	
	public Double getHailRange() {
		return hailRange;
	}
	
	public void setHailRange(Double hailRange) {
		this.hailRange = hailRange;
	}
	
	public Integer getClearBiddingTime() {
		return clearBiddingTime;
	}
	
	public void setClearBiddingTime(Integer clearBiddingTime) {
		this.clearBiddingTime = clearBiddingTime;
	}
	
	public Integer getRequestWaitTime() {
		return requestWaitTime;
	}
	
	public void setRequestWaitTime(Integer requestWaitTime) {
		this.requestWaitTime = requestWaitTime;
	}
	
	public Integer getTimeToPickUp() {
		return timeToPickUp;
	}
	
	public void setTimeToPickUp(Integer timeToPickUp) {
		this.timeToPickUp = timeToPickUp;
	}
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public Long getAlternateCallTypeId() {
		return alternateCallTypeId;
	}

	public void setAlternateCallTypeId(Long alternateCallTypeId) {
		this.alternateCallTypeId = alternateCallTypeId;
	}

	public Double getZoneOrder() {
		return zoneOrder;
	}

	public void setZoneOrder(Double zoneOrder) {
		this.zoneOrder = zoneOrder;
	}

	public enum Category {
		trip,res
	}

	public Integer getSamePassengerDays() {
		return samePassengerDays;
	}

	public void setSamePassengerDays(Integer samePassengerDays) {
		this.samePassengerDays = samePassengerDays;
	}

	public Integer getExpirationsAllowed() {
		return expirationsAllowed;
	}

	public void setExpirationsAllowed(Integer expirationsAllowed) {
		this.expirationsAllowed = expirationsAllowed;
	}
	
}
