package aploon.model;

import java.math.BigDecimal;

public class Coordinates {

    private double lng;
    private double lat;
    private String normalizedAddress;
    private String internationalAddress;
	private String countryCode;

    public Coordinates(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Coordinates(BigDecimal lat, BigDecimal lng) {
        if (lat != null) {
            this.lat = lat.doubleValue();
        }
        if (lng != null) {
            this.lng = lng.doubleValue();
        }
    }

    public String toString() {
        return "[" + lat + ", " + lng + "]";
    }

    public Coordinates(double lat, double lng, String normalizedAddress) {
        this.lat = lat;
        this.lng = lng;
        this.normalizedAddress = normalizedAddress;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Double getLng() {
        return (Double)lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Double getLat() {
        return (Double)lat;
    }

    public void setNormalizedAddress(String normalizedAddress) {
        this.normalizedAddress = normalizedAddress;
    }

    public String getNormalizedAddress() {
        return normalizedAddress;
    }

    public void setInternationalAddress(String internationalAddress) {
        this.internationalAddress = internationalAddress;
    }

    public String getInternationalAddress() {
        return internationalAddress;
    }

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

    /**
     * 
     * @param lat2
     * @param lng2
     * @return distance in meters
     */
    public double distFromCoordinates(double lat2, double lng2) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(lat2 - lat);
        double dLng = Math.toRadians(lng2 - lng);
        double a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(lat2)) *
            Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        int meterConversion = 1609;
        return new Double(dist * meterConversion).doubleValue();
    }
}
