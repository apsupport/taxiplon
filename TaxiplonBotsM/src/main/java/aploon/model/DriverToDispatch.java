package aploon.model;

import java.math.BigDecimal;

public class DriverToDispatch {

	private Long deviceId;
	private String deviceImei;
	private Long qDriverId;
	private Double distance;
	private String drlocale;
	private String drextra;
	private String brandname;
	private BigDecimal lat;
	private BigDecimal lng;
	
	public Long getDeviceId() {
		return deviceId;
	}
	
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	
	public String getDeviceImei() {
		return deviceImei;
	}
	
	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}
	
	public Long getqDriverId() {
		return qDriverId;
	}
	
	public void setqDriverId(Long qDriverId) {
		this.qDriverId = qDriverId;
	}
	
	public Double getDistance() {
		return distance;
	}
	
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public String getDrlocale() {
		return drlocale;
	}
	
	public void setDrlocale(String drlocale) {
		this.drlocale = drlocale;
	}
	
	public String getDrextra() {
		return drextra;
	}
	
	public void setDrextra(String drextra) {
		this.drextra = drextra;
	}
	
	public String getBrandname() {
		return brandname;
	}
	
	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}
	
	public BigDecimal getLat() {
		return lat;
	}
	
	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}
	
	public BigDecimal getLng() {
		return lng;
	}
	
	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}
	
}
