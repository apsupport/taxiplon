package aploon.model;

public class TripAssignPicked {
	
	private Long tripId;
	private Long deviceId;
	private Long driverId;
	private String codeName;
	private String lastName;
	private String firstName;
	private Double latitude;
	private Double longtitude;
	private Double distance;
	private Double distanceAssigned;
	private String comments;
	
	public Long getTripId() {
		return tripId;
	}
	
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	
	public Long getDeviceId() {
		return deviceId;
	}
	
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	
	public Long getDriverId() {
		return driverId;
	}
	
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongtitude() {
		return longtitude;
	}
	
	public void setLongtitude(Double longtitude) {
		this.longtitude = longtitude;
	}
	
	public Double getDistance() {
		return distance;
	}
	
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public Double getDistanceAssigned() {
		return distanceAssigned;
	}
	
	public void setDistanceAssigned(Double distanceAssigned) {
		this.distanceAssigned = distanceAssigned;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
