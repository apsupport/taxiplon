package aploon.model;

public class TripAssign {

	private Long tripId;
    private Long deviceId;
    private Long driverId;
    private Integer responseTime;
    private Double lat;
    private Double lng;
    private Double distance;
    private Double distanceAssigned;
    private String comments;
	
    public Long getTripId() {
		return tripId;
	}
	
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	
	public Long getDeviceId() {
		return deviceId;
	}
	
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	
	public Long getDriverId() {
		return driverId;
	}
	
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public Integer getResponseTime() {
		return responseTime;
	}
	
	public void setResponseTime(Integer responseTime) {
		this.responseTime = responseTime;
	}
	
	public Double getLat() {
		return lat;
	}
	
	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	public Double getLng() {
		return lng;
	}
	
	public void setLng(Double lng) {
		this.lng = lng;
	}
	
	public Double getDistance() {
		return distance;
	}
	
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	public Double getDistanceAssigned() {
		return distanceAssigned;
	}
	
	public void setDistanceAssigned(Double distanceAssigned) {
		this.distanceAssigned = distanceAssigned;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
