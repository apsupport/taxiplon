package aploon.model;

import java.sql.Timestamp;

public class DriverAssignedTrip {
	
	private Long tripId;
	private Timestamp hailTime;
	private String pickUpAddress;
	private String specialNeeds;
	private String status;
	private Double fare;
	private String paymentType;
	private String ShortName;
	
	public Long getTripId() {
		return tripId;
	}
	
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}
	
	public Timestamp getHailTime() {
		return hailTime;
	}
	
	public void setHailTime(Timestamp hailTime) {
		this.hailTime = hailTime;
	}
	
	public String getPickUpAddress() {
		return pickUpAddress;
	}
	
	public void setPickUpAddress(String pickUpAddress) {
		this.pickUpAddress = pickUpAddress;
	}
	
	public String getSpecialNeeds() {
		return specialNeeds;
	}
	
	public void setSpecialNeeds(String specialNeeds) {
		this.specialNeeds = specialNeeds;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Double getFare() {
		return fare;
	}

	public void setFare(Double fare) {
		this.fare = fare;
	}

	public String getPaymentType() {
		return paymentType;
	}
	
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	public String getShortName() {
		return ShortName;
	}
	
	public void setShortName(String shortName) {
		ShortName = shortName;
	}
	
}