package aploon.model;

public class TripSummary {

	private Integer assigned;
	private Integer lost;
	private Integer month;
	private Integer year;
	
	public Integer getAssigned() {
		return assigned;
	}
	
	public void setAssigned(Integer assigned) {
		this.assigned = assigned;
	}
	
	public Integer getLost() {
		return lost;
	}
	
	public void setLost(Integer lost) {
		this.lost = lost;
	}
	
	public Integer getMonth() {
		return month;
	}
	
	public void setMonth(Integer month) {
		this.month = month;
	}
	
	public Integer getYear() {
		return year;
	}
	
	public void setYear(Integer year) {
		this.year = year;
	}
	
}
