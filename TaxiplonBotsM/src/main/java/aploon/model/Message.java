package aploon.model;

import java.sql.Timestamp;

public class Message {

	private Long messageId;
	private Long taxiCompanyId;
	private String messageBody;
	private Long driverId;
	private Timestamp messageTime;
	private String status;
	
	public Long getMessageId() {
		return messageId;
	}
	
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	
	public Long getTaxiCompanyId() {
		return taxiCompanyId;
	}
	
	public void setTaxiCompanyId(Long taxiCompanyId) {
		this.taxiCompanyId = taxiCompanyId;
	}
	
	public String getMessageBody() {
		return messageBody;
	}
	
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
	public Long getDriverId() {
		return driverId;
	}
	
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public Timestamp getMessageTime() {
		return messageTime;
	}
	
	public void setMessageTime(Timestamp messageTime) {
		this.messageTime = messageTime;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
}
