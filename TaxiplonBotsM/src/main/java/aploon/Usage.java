package aploon;


import aploon.xmpp.ServerSideClient;

import com.taxiplon.db.DataPool;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Layer dest record usage statistics into the DB.
 * Keeps track of all incoming and outgoing traffic per bot and per user.
 */
public class Usage {

    private static Usage instance;

    public static synchronized Usage getInstance() throws SQLException {
	if (instance == null) {
	    instance = new Usage();
	}
	return instance;
    }

    protected StatExporter exp;

    private Usage() throws SQLException {
	if (ServerSideClient.DEBUG) {
	    exp = new StatExporter();
	    exp.setName("statexporter_scheduler");
	    exp.start();
	}
    }

    // global stats
    private static long inTotalDataCounter = 0;
    private static long outTotalDataCounter = 0;

    public class StatMap extends HashMap<String, Long> {
	public String toString() {
	    String result = "";
	    for (Map.Entry<String, Long> entry : entrySet()) {
		result +=
	   (!result.equals("") ? "\n" : "") + entry.getKey() + ": " + Main.coolFormat(entry.getValue(), 0);
	    }
	    return result;
	}
    }

    // maps to store stats per bot and per user
    private StatMap inDataPerUser = new StatMap();
    private StatMap outDataPerRecipient = new StatMap();
    private StatMap inDataPerBot = new StatMap();
    private StatMap outDataPerBot = new StatMap();

    public void recordOutboundTraffic(long messageSizeInBytes, String to, String from) {
	//update the global counter
	outTotalDataCounter += messageSizeInBytes;
	//update stats per recipient
	Long recipientAccData = outDataPerRecipient.get(to);
	if (recipientAccData == null) {
	    recipientAccData = 0L;
	}
	outDataPerRecipient.put(to, recipientAccData + messageSizeInBytes);
	//update stats per bot
	Long botAccData = outDataPerBot.get(from);
	if (botAccData == null) {
	    botAccData = 0L;
	}
	outDataPerBot.put(from, botAccData + messageSizeInBytes);
	//save stats for later export to DB
	usageEntries.add(exp.new UsageStat(from, to, 0, messageSizeInBytes));
    }

    public void recordInboundTraffic(long messageSizeInBytes, String to, String from) {
	//update the global counter
	inTotalDataCounter += messageSizeInBytes;
	//update stats per recipient
	Long recipientAccData = inDataPerUser.get(from);
	if (recipientAccData == null) {
	    recipientAccData = 0L;
	}
	inDataPerUser.put(from, recipientAccData + messageSizeInBytes);
	//update stats per bot
	Long botAccData = inDataPerBot.get(to);
	if (botAccData == null) {
	    botAccData = 0L;
	}
	inDataPerBot.put(to, botAccData + messageSizeInBytes);
	//save stats for later export to DB
	usageEntries.add(exp.new UsageStat(from, to, messageSizeInBytes, 0));
    }

    private ArrayList<StatExporter.UsageStat> usageEntries = new ArrayList<StatExporter.UsageStat>();

    /**
     * Initiate a separate thread dest export stats dest the DB.
     */
    public class StatExporter extends Thread {

    	protected DataPool dataPool;

    	public StatExporter() throws SQLException {
    		dataPool = new DataPool("UsageDB");
    	}

    	ArrayList<UsageStat> usage;

    	public void run() {
    		int i; //designated parameter index
    		/**
    		 * Bye bye usage
	    	while (true) {
	    		//this will prevent data loss within the time consumed to physically write data to the DB
	    		usage = usageEntries;
	    		usageEntries = new ArrayList<UsageStat>();
		
	    		try {
	    			prepareSaveUsageStmt();
	    			//save data per bot usage
	    			for (UsageStat p : usage) {
	    				i = 1;
	    				saveUsageStmt.setTimestamp(i++, new Timestamp(p.timeMillis)); //datetime
	    				saveUsageStmt.setString(i++, p.src); //from
	    				saveUsageStmt.setString(i++, p.dest); //to
	    				saveUsageStmt.setString(i++, p.srv); //service
	    				saveUsageStmt.setLong(i++, p.in); //in
	    				saveUsageStmt.setLong(i++, p.out); //out
	    				saveUsageStmt.executeUpdate();
	    			}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
	    		try {
	    			Thread.sleep(60000);
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
	    	}
    		 */
	}

	/**
	 * Entity object of the table stats.usage.
	 */
	public class UsageStat {

	    Long timeMillis;
	    String src;
	    String dest;
	    String srv;
	    long in;
	    long out;
	    String ip;

	    public UsageStat(String from, String to, long in, long out) {
		timeMillis = System.currentTimeMillis();
		//parse srv
		try {
		    String parts[] = from.split("@");
		    this.srv = parts[1].split("/")[0];
		} catch (Exception e) {
		    this.srv = null;
		}
		src = from;
		dest = to;
		this.in = in;
		this.out = out;
	    }
	}

	PreparedStatement saveUsageStmt;

	private void prepareSaveUsageStmt() throws Exception {
	    if (saveUsageStmt == null || saveUsageStmt.isClosed()) {
		saveUsageStmt =
     dataPool.prepareStatement("INSERT INTO `usage` (datetime, src, dest, srv, i, o) VALUES (?, ?, ?, ?, ?, ?)");
	    }
	}
    }

    //data counters

    public Long getPersonalOutDataCounter(String bot) {
	return outDataPerBot.containsKey(bot) ? outDataPerBot.get(bot) : 0L;
    }

    public Long getPersonalInDataCounter(String bot) {
	return inDataPerBot.containsKey(bot) ? inDataPerBot.get(bot) : 0L;
    }

    public static String getInTotalDataCounter() {
	return Main.coolFormat(inTotalDataCounter, 0);
    }

    public static String getOutTotalDataCounter() {
	return Main.coolFormat(outTotalDataCounter, 0);
    }

    public StatMap getInDataPerUser() {
	return inDataPerUser;
    }

    public StatMap getOutDataPerRecipient() {
	return outDataPerRecipient;
    }

    public StatMap getInDataPerBot() {
	return inDataPerBot;
    }

    public StatMap getOutDataPerBot() {
	return outDataPerBot;
    }

}
