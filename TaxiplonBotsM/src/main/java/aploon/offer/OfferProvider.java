package aploon.offer;

import java.sql.SQLException;

import java.util.List;

import org.apache.log4j.Logger;

import aploon.datahelper.DataHelper;
import aploon.model.Offer;
import aploon.model.OfferDataPerRide;
import aploon.model.Offer.Type;
import aploon.model.OfferPerRide;

public class OfferProvider {

	private static final Logger LOGGER = Logger.getLogger(OfferProvider.class);
	
	private Long taxiCompanyId;
	private Long passengerId;
	
	public OfferProvider(long taxiCompanyId,long passengerId) {
		this.taxiCompanyId = taxiCompanyId;
		this.passengerId = passengerId;
	
		//LOGGER.info("Just setted the taxiCompanyId and the passengerId "+taxiCompanyId+" "+passengerId);
	}
	
	private enum BookAgent {
		s,d,w
	};
	
	public boolean companyHasOffers() {
		
		try {
			return DataHelper.getTaxiCompanyOffers(taxiCompanyId).size()>0;
		} catch (SQLException e) {
			e.printStackTrace();
			LOGGER.error("Fatal error with mysql query",e);
		}
		
		return false;
	}
	
	/*
	 * Increasing Creating or adding to discount for passenger
	 */
	public void completeDiscount(String bookAgentStr,long tripId) {
		
		BookAgent bookAgent = BookAgent.valueOf(bookAgentStr);
		
		try {
			List<Offer> offers = DataHelper.getTaxiCompanyOffers(taxiCompanyId);
		
			for(Offer offer:offers) {
				
				Type type = offer.getType();

				switch (type) {
					case perride:

						OfferDataPerRide offerDataPerRide = DataHelper.getPassengerOfferDataPerRide(passengerId, taxiCompanyId);
						
						if(offerDataPerRide!=null) {
							
							OfferPerRide offerPerRide = matchOfferPerRide(offerDataPerRide, bookAgent);
							
							int dispatcherData = offerDataPerRide.getDispatcherCalls();
							int smartPhoneData = offerDataPerRide.getSmartPhoneCalls();
							int webData = offerDataPerRide.getWebCalls();
							
							if(offerPerRide!=null) {
							
								switch (bookAgent) {
								case d:
									dispatcherData = 0;
									break;
								case s:
									smartPhoneData = 0;
									break;
								case w:
									webData = 0;
									break;
								}
								
								DataHelper.updatePassengerOfferDataPerRide(passengerId, taxiCompanyId, dispatcherData, smartPhoneData, webData);
								DataHelper.setTripAsDiscount(tripId);
								
							} else {
								
								switch (bookAgent) {
									case d:
										dispatcherData++;
										break;
									case s:
										smartPhoneData++;
										break;
									case w:
										webData++;
										break;
								}
								
								DataHelper.updatePassengerOfferDataPerRide(passengerId, taxiCompanyId, dispatcherData, smartPhoneData, webData);
							}
						} else {
						
							int dispatcherCalls = 0;
							int smartPhoneCalls = 0;
							int webCalls = 0;
							
							switch (bookAgent) {
							case s:
								smartPhoneCalls++;
								break;
							case d:
								dispatcherCalls++;
								break;
							case w:
								webCalls++;
								break;
							}
							DataHelper.createPassengerOfferDataPerRide(taxiCompanyId, passengerId, dispatcherCalls, smartPhoneCalls, webCalls);						
						}
					
						break;	
				}
				
			}
			
		} catch (SQLException e) {
			LOGGER.error("Fatal sql error",e);
			e.printStackTrace();
		}
				
	}
	
	
	/*
	 * An abstract way to get discounts
	 */
	public Double getAnyDiscount(String bookAgentStr) {
		
		BookAgent bookAgent = BookAgent.valueOf(bookAgentStr);
		
		try {
			
			List<Offer> offers = DataHelper.getTaxiCompanyOffers(taxiCompanyId);
			
			
			for(Offer offer:offers) {
				
				Type type = offer.getType();
				
				switch (type) {
				case perride:
					
					OfferDataPerRide offerDataPerRide = DataHelper.getPassengerOfferDataPerRide(passengerId, taxiCompanyId);
					
					if(offerDataPerRide!=null) {
						
						OfferPerRide offerPerRide = matchOfferPerRide(offerDataPerRide, bookAgent);
						
						//int dispatcherData = offerDataPerRide.getDispatcherCalls();
						//int smartPhoneData = offerDataPerRide.getSmartPhoneCalls();
						//int webData = offerDataPerRide.getWebCalls();
						
						if(offerPerRide!=null) {
						
							/*
							switch (bookAgent) {
							case d:
								dispatcherData = 0;
								break;
							case s:
								smartPhoneData = 0;
								break;
							case w:
								webData = 0;
								break;
							}
							*/
							
							//DataHelper.updatePassengerOfferDataPerRide(passengerId, taxiCompanyId, dispatcherData, smartPhoneData, webData);
							return offerPerRide.getDiscount();
						
						} 
						
						/*
						else {
							
							switch (bookAgent) {
								case d:
									dispatcherData++;
									break;
								case s:
									smartPhoneData++;
									break;
								case w:
									webData++;
									break;
							}
							
							DataHelper.updatePassengerOfferDataPerRide(passengerId, taxiCompanyId, dispatcherData, smartPhoneData, webData);
						}
						*/
						
					}
					
					/*
					else {
					
						int dispatcherCalls = 0;
						int smartPhoneCalls = 0;
						int webCalls = 0;
						
						switch (bookAgent) {
						case s:
							smartPhoneCalls++;
							break;
						case d:
							dispatcherCalls++;
							break;
						case w:
							webCalls++;
							break;
						}
						
						DataHelper.createPassengerOfferDataPerRide(taxiCompanyId, passengerId, dispatcherCalls, smartPhoneCalls, webCalls);						
					}
					*/
					
					break;
				}
				
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
			LOGGER.error("Fatal sql qiery error",e);
		}
		
		return 0.0;
	}

	private OfferPerRide matchOfferPerRide(OfferDataPerRide offerDataPerRide,BookAgent bookAgent) throws SQLException {
		
		List<OfferPerRide> offerPerRides = DataHelper.getTaxiCompanyOfferPerRides(taxiCompanyId);
		
		LOGGER.info("Match per rides ferched from the company the bookagent is "+bookAgent);
		
		for(OfferPerRide offerPerRide: offerPerRides) {
		
			LOGGER.info("THe companyoffer per ride "+offerPerRide.getDiscount()+" "+offerPerRide.getCallType()+" "+offerPerRide.getRides());
			LOGGER.info("The passenger data "+offerDataPerRide.getDispatcherCalls()+" "+offerDataPerRide.getSmartPhoneCalls()+" "+offerDataPerRide.getWebCalls());
			
			LOGGER.info(" the call type "+offerPerRide.getCallType());
			
			switch (offerPerRide.getCallType()) {
			case all:
				if(offerPerRide.getRides()<=offerDataPerRide.getDispatcherCalls()||
				offerPerRide.getRides()<=offerDataPerRide.getSmartPhoneCalls()||
				offerPerRide.getRides()<=offerDataPerRide.getWebCalls()) {
					
					LOGGER.info("The offerperride "+bookAgent);
					
					return offerPerRide;	
				}
				break;
			case wd:
				if((offerPerRide.getRides()<=offerDataPerRide.getWebCalls()||
				offerPerRide.getRides()<=offerDataPerRide.getDispatcherCalls())) {
				
					LOGGER.info("The wd offerperride "+bookAgent);
					
					if(bookAgent.equals(BookAgent.w)||bookAgent.equals(BookAgent.d)) {
						return offerPerRide;	
					}
					
				}
				break;
			case sw:
				if((offerPerRide.getRides()<=offerDataPerRide.getWebCalls()||
				offerPerRide.getRides()<=offerDataPerRide.getSmartPhoneCalls())) {
				
					LOGGER.info("The sw offerperride "+bookAgent);
					
					if(bookAgent.equals(BookAgent.s)||bookAgent.equals(BookAgent.w)) {
						return offerPerRide;
					}
					
				}
				break;
			case sd:
				if((offerPerRide.getRides()<=offerDataPerRide.getDispatcherCalls()||
				offerPerRide.getRides()<=offerDataPerRide.getSmartPhoneCalls())) {
					
					LOGGER.info("The sd offerperride "+bookAgent);
					
					if(bookAgent.equals(BookAgent.s)||bookAgent.equals(BookAgent.d)) {
						return offerPerRide;
					}
				}
  				break;
			case d:
			
				if(offerPerRide.getRides()<=offerDataPerRide.getDispatcherCalls()) {
					
					if(bookAgent.equals(BookAgent.d)) {
						
						return offerPerRide;						
					}
					
				}

				break;
			case s:
				
				if(offerPerRide.getRides()<=offerDataPerRide.getSmartPhoneCalls()) {
					
					if(bookAgent.equals(BookAgent.s)) {
						
						return offerPerRide;						
					}
					
				}
				break;
			case w:
				if(offerPerRide.getRides()<=offerDataPerRide.getWebCalls()) {
					
					if(bookAgent.equals(BookAgent.w)) {
						
						return offerPerRide;						
					}
					
				}
				break;
			}
			
		}
		
		return null;
	}

}
