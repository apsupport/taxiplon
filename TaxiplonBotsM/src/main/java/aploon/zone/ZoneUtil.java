package aploon.zone;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import com.taxiplon.Util;

import aploon.model.Coordinates;
import aploon.model.Zone;

public class ZoneUtil {

	private static final Logger LOGGER = Logger.getLogger(ZoneUtil.class);
	
    public static Long coordinateInsideCompanyZone(Double lat,Double lon,List<Zone> zones,Long taxiCompanyId) {
        
    	LOGGER.debug("Lat Lng of call "+lat+" "+lon);
    	
    	for(Zone zone:zones) {
    		
    		if(zone.getTaxiCompanyId().equals(taxiCompanyId)&&ZoneUtil.validZoneTime(zone)) {
    			if(coordinatesInsideZone(lat, lon, zone)) {
    				return zone.getZoneId();
                }
    		}
    	}   

        return null;
    }  	

    /**
     * Checks weather the zone is active considering time
     * @param zone
     * @return
     */
    private static boolean validZoneTime(Zone zone) {

    	if(zone.getOpensAt()==null||zone.getClosesAt()==null) {
    		return true;
        }

    	Calendar calendar = Calendar.getInstance();

    	Time time = Time.valueOf(calendar.get(Calendar.HOUR_OF_DAY)+":"+
                            calendar.get(Calendar.MINUTE)+":"+
                            calendar.get(Calendar.SECOND));

    	if(zone.getClosesAt().after(zone.getOpensAt())) {
    		return zone.getOpensAt().after(time)&&zone.getClosesAt().before(time);
        } else if(zone.getClosesAt().before(zone.getOpensAt())) {
        	if(time.after(zone.getOpensAt())) {
        		return true;
            } else if(time.before(zone.getClosesAt())){
            	return true;
            }
        }

    	return false;   
    }
    
    /**
     * Checks if the latitude longtitude specified
     * is inside the zone
     * @param lat
     * @param lon
     * @param zone
     * @return
     */
    public static boolean coordinatesInsideZone(Double lat,Double lon,Zone zone) {
    	
    	if(zone.getStrictBorders()) {
    		if(zone.getZonePoints()!=null) {
    			List<Coordinates> coordinates = zonePointsToCoordinates(zone.getZonePoints());
    			LOGGER.debug("Zone has point coordinates");
    			if(coordinates.size()>0) {
    				boolean zoneInPolygon = pointInPolygon(lat, lon, coordinates);
    				LOGGER.debug("The zonein polygon "+zoneInPolygon);
    				return zoneInPolygon;
    			} 
    		} else {
    		Double driverDistance = Util.getDistance(lat, lon, zone.getZoneLat(), zone.getZoneLong());
    		return driverDistance<=zone.getZoneRange();
    		}
    	} else {
    		LOGGER.debug("Nothing fetched set as true");
    		return true;
    	}
    	
    	return false;
    }
    
    /**
     * Defines if point is inside polygon
     * @param lat
     * @param lng
     * @param coordinates
     * @return
     */
    public static boolean pointInPolygon(Double lat,Double lng,List<Coordinates> coordinates) {
    	
    	int i,j;
    	boolean c = false;
    	
    	BigDecimal y = new BigDecimal((lat.toString()));
    	BigDecimal x = new BigDecimal((lng.toString()));
    	
    	for (i = 0, j = coordinates.size()-1; i < coordinates.size(); j = i++) {
    		Coordinates coordinatei = coordinates.get(i);
    		Coordinates coordinatej = coordinates.get(j);
    		
    		BigDecimal yPoli = new BigDecimal((coordinatei.getLat().toString()));
    		BigDecimal xPoli = new BigDecimal((coordinatei.getLng().toString()));
    		BigDecimal yPolj = new BigDecimal((coordinatej.getLat().toString()));
    		BigDecimal xPolj = new BigDecimal((coordinatej.getLng().toString()));
    		
    		if( (yPoli.compareTo(y)>0) != (yPolj.compareTo(y)>0) &&
    				x.compareTo(xPoli.add(xPolj.subtract(xPoli).multiply(
    						y.subtract(yPoli).divide(yPolj.subtract(yPoli),MathContext.DECIMAL128))))<0) {
    			 c = !c;
    		}
    		
    	}
    	
    	return c;
    }
    
    /**
     * Old and non properly working method
     * 
    public static boolean coordinatesInsideZone(Double lat,Double lon,Zone zone) {
    
    	if(zone.getZonePoints()!=null) {
    		List<Coordinates> coordinates = zonePointsToCoordinates(zone.getZonePoints());
    		LOGGER.debug("the zone has coordinates");
    		if(coordinates.size()>0) {
                boolean zoneInPolygon = isPointInPolygon(lat, lon, coordinates);
                LOGGER.debug("The zonein polygon "+zoneInPolygon);
                return zoneInPolygon;
    		} else {
                return false;
        	}
    	} else {
            Double driverDistance = Util.getDistance(lat, lon, zone.getZoneLat(), zone.getZoneLong());
            return driverDistance<=zone.getZoneRange();	
    	}
    	
    }
    */
    
    /**
     * The zone Points that are in json format will be
     * parsed and returned to Coordinates
     * @param zonePoints
     * @return
     */
    public static List<Coordinates> zonePointsToCoordinates(String zonePoints) {
    	
    	List<Coordinates> coordinates = new ArrayList<>();
    	
    	try {
    		
    		JSONArray jsonArray = new JSONArray(zonePoints);
    		if(jsonArray!=null&&jsonArray.length()>0) {
                for(int i=0;i<jsonArray.length();i++) {
                    JSONArray jsonLoc = jsonArray.getJSONArray(i);
                    Coordinates coordinate = new Coordinates(jsonLoc.getDouble(0), jsonLoc.getDouble(1));
                    coordinates.add(coordinate);
                }   
    		}
    	} catch(JSONException e) {
    		LOGGER.error("Problem with json parsing ",e);
    	}
    	
    	return coordinates;
    }
    
    /**
     * Old and non properly working method
     * 
    private static boolean isPointInPolygon(Double driverLat,Double driverLng, List<Coordinates> coordinates) {

        int intersectCount = 0;

        for(int j=0; j<coordinates.size()-1; j++) {
        	if( ZoneUtil.rayCastIntersect(driverLat,driverLng, coordinates.get(j), coordinates.get(j+1)) ) {
        		intersectCount++;
        	}
        }

        return (intersectCount%2) == 1; // odd = inside, even = outside;
    }

    private static boolean rayCastIntersect(Double driverLat,Double driverLng, Coordinates vertA, Coordinates vertB) {

        double aY = vertA.getLat();
                    //getLatitude();
        double bY = vertB.getLat();
                    //getLatitude();
        double aX = vertA.getLng();
                    //getLongitude();
        double bX = vertB.getLng();
        //getLongitude();
        double pY = driverLat;
        double pX = driverLng;

        if ( (aY>pY && bY>pY) || (aY<pY && bY<pY) || (aX<pX && bX<pX) ) {
            return false; // a and b can't both be above or below pt.y, and a or b must be east of pt.x
        }

        double m = (aY-bY) / (aX-bX);               // Rise over run
        double bee = (-aX) * m + aY;                // y = mx + b
        double x = (pY - bee) / m;                  // algebra is neat!

        return x > pX;
    }
    */
}
    