package aploon.schedulers;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import aploon.concurrency.util.NamingThreadFactory;

public class DriverReinformScheduler {

	private static ScheduledExecutorService scheduledExecutorService = null;
	
	static {
		scheduledExecutorService = Executors.newScheduledThreadPool(10,new NamingThreadFactory("reinform_pool"));
	}
	
	private DriverReinformScheduler() {}
	
	
	public static class TripReinForm {
		
		
	}
	
	private void addJob() {
		
		scheduledExecutorService.schedule(
				new Callable<String>() {

					@Override
					public String call() throws Exception {
						return "reinform";
					}
				
				}, 1000, TimeUnit.SECONDS);
	}
	
}
