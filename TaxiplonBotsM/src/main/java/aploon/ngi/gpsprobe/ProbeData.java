package aploon.ngi.gpsprobe;

public class ProbeData {

	private Long Timestamp;
	private Double Speed;
	private Double Latitude;
	private Double Longitude;
	private Double Altitude;
	private Double Heading;
	private Integer NumberOfSatellites;
	
	public Long getTimestamp() {
		return Timestamp;
	}
	
	public void setTimestamp(Long timestamp) {
		Timestamp = timestamp;
	}
	
	public Double getSpeed() {
		return Speed;
	}
	
	public void setSpeed(Double speed) {
		Speed = speed;
	}
	
	public Double getLatitude() {
		return Latitude;
	}
	
	public void setLatitude(Double latitude) {
		Latitude = latitude;
	}
	
	public Double getLongitude() {
		return Longitude;
	}
	
	public void setLongitude(Double longitude) {
		Longitude = longitude;
	}
	
	public Double getAltitude() {
		return Altitude;
	}
	
	public void setAltitude(Double altitude) {
		Altitude = altitude;
	}
	
	public Double getHeading() {
		return Heading;
	}
	
	public void setHeading(Double heading) {
		Heading = heading;
	}
	
	public Integer getNumberOfSatellites() {
		return NumberOfSatellites;
	}
	
	public void setNumberOfSatellites(Integer numberOfSatellites) {
		NumberOfSatellites = numberOfSatellites;
	}
	
}
