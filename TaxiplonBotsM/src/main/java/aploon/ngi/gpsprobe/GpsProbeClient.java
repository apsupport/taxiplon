package aploon.ngi.gpsprobe;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;

import aploon.httpconnector.HttpConnectionProvider;

import com.google.gson.Gson;

public class GpsProbeClient {

	private static ExecutorService executorService = Executors.newFixedThreadPool(25);

	private static final String probeUrl = "https://gpsprobe.geointelligence.gr/api/probe";
	
	private static final Logger LOGGER = Logger.getLogger(GpsProbeClient.class);
	
	public void postToProbe(Long deviceId,ProbeData probeData){
		
		executorService.execute(new PostToProbeRunnable(deviceId, probeData));
	}
	
	private class PostToProbeRunnable implements Runnable {

		private Long deviceId;
		private ProbeData probeData;
		
		public PostToProbeRunnable(Long deviceId,ProbeData probeData) {
			this.deviceId = deviceId;
			this.probeData = probeData;
		}
		
		@Override
		public void run() {
			
			PostMethod postMethod = new PostMethod(probeUrl);
			HttpClient httpClient = HttpConnectionProvider.getHttpClient();
			httpClient.getParams().setAuthenticationPreemptive(true);
			httpClient.getState().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("taxiplon", "+@><1Pl)_$%on1542"));
		    StringRequestEntity stringRequestEntity;
			try {
				stringRequestEntity = new StringRequestEntity(getJson(), "application/json", "UTF-8");
				postMethod.setRequestEntity(stringRequestEntity);
				try {
					httpClient.executeMethod(postMethod);
					String response = new String(postMethod.getResponseBody());
					postMethod.releaseConnection();
					LOGGER.info("The response body is "+response);
				} catch (HttpException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	
		private String getJson() {
			return String.format("{\"DeviceId\": %d,\"ProbeData\": %s }", 12L,new Gson().toJson(new ProbeData[] { probeData} ));
		}
		
	};
	
	
}
