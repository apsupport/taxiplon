package aploon.penalties;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import aploon.datahelper.DataHelper;
import aploon.model.CallType;
import aploon.model.DriverPenalty;
import aploon.model.Penalty;
import aploon.model.Trip;
import aploon.xmpp.ssclient.FareControl;
import aploon.xmpp.ssclient.Zones;

/**
 * Class that handles penalties
 * @author gkatzioura
 *
 */
public class PenaltyManager {

	private static final Logger LOGGER = Logger.getLogger(PenaltyManager.class);
	
	private PenaltyManager() {}
	
	/**
	 * Executing penalty on fare control occassion
	 * Information retrieved through tripId
	 * @param operations
	 * @param tripId
	 */
	public static void executeFareControlPenalty(FareControl.Operations operations,Long tripId) {
	
		try {
			
			LOGGER.info("Executing the penalty "+tripId+" "+operations.toString());
			
			Trip trip = DataHelper.getTrip(tripId);
			
			if(trip.getDriverId()!=null&&trip.getDriverId()!=0) {
				if(operations==FareControl.Operations.Complete) {
					PenaltyManager.executeCompletePenalty(trip);
				} else if(operations==FareControl.Operations.DriverCancel) {
					PenaltyManager.executeCancelPenalty(trip);
				}
			}
			
		} catch(SQLException e) {
			LOGGER.error("Got an sql exception ",e);
		}
		
	}
	
	/**
	 * Execute penalty on trip expiration
	 * @param tripId
	 */
	public static void executeExpirationPenalty(Long tripId) {
	
		try {
			Trip trip = DataHelper.getTrip(tripId);
			
			if(trip.getDriverId()!=null&&trip.getDriverId().intValue()!=0) {
				
				LOGGER.info("Driver Exist going to have penalty "+trip.getDriverId()+" "+trip.getTripId());
				
				CallType callType = DataHelper.getCallType(trip.getCallTypeId());
				
				if(callType.getExpirationsAllowed()!=null) {
					Integer expirations = DataHelper.getTodaysDriversExpirations(callType.getCallTypeId(),trip.getDriverId());
					if(expirations>callType.getExpirationsAllowed()) {
						Penalty penalty = DataHelper.getCallTypeExpirationPenalty(callType.getCallTypeId());
						if(penalty!=null) {
							Timestamp startPenalty = new Timestamp(new Date().getTime());
							Timestamp endPenalty = PenaltyManager.addMinutesToTimeStamp(startPenalty, penalty.getMinutes());
							DataHelper.setDriverPenalty(trip.getDriverId(),penalty.getPenaltyId(),trip.getTripId(), null, startPenalty, endPenalty,Penalty.Event.expire.toString());
							try {
								//TODO Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null, false);
								Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null);
							} catch (Exception e) {
								LOGGER.error("Problem removing driver from zone");
							}
		    			}
					}
				}
			}
			
			
		} catch (SQLException e) {
			LOGGER.error("Problem expiring penalty "+e);
		}
		
	}
	
	public static void checkAndFixPenalties() {
		
		try {
			
			List<Long> driverIds = DataHelper.getDriversInPenalties();
			
			for(Long driverId:driverIds) {
				DataHelper.fixDriverPenalty(driverId);
			}
		
		} catch (SQLException e) {
			LOGGER.error("Problem getting driver penalties ",e);
		}
		
	}
	

	
	/**
	 * In case where the penalty has to be completed
	 * @param trip
	 * @throws SQLException
	 */
	private static void executeCompletePenalty(Trip trip) throws SQLException {
		
		LOGGER.info("Executing the complete penalty ");
		
		Integer timeToPickUp = trip.getTimeToPickUp(); 
		
		LOGGER.info("The time to pick up is "+timeToPickUp);
		
		if(timeToPickUp!=null&&timeToPickUp.compareTo(0)!=0) {
			
			if(trip.getArrivalTime()==null) trip.setArrivalTime(trip.getPickUpTime());
			
			Long minutesLate = PenaltyManager.timestampMinuteDifference(
					trip.getAssignTime(),trip.getArrivalTime());
		
			LOGGER.info("minutes late is "+timeToPickUp+" "+minutesLate);
			
			if(minutesLate>timeToPickUp) {
				Penalty penalty = DataHelper.getCallTypeDelayPenalty(trip.getCallTypeId());
				if(penalty!=null) {
					LOGGER.info("Yes there is a penalty and it is "+penalty.getName());
					if(penalty.getPerMinutes()!=null&&penalty.getPerMinutes().compareTo(0)!=0) {
						Integer penaltyMinutes = minutesLate.intValue()-timeToPickUp;
						BigDecimal banTimes = new BigDecimal(penaltyMinutes.toString()).divide(new BigDecimal(penalty.getPerMinutes().toString()));
						int banTimesInt = banTimes.intValue()+1;
						if(banTimesInt>0) {
							Timestamp startPenalty = new Timestamp(new Date().getTime());
		    				Timestamp endPenalty = PenaltyManager.addMinutesToTimeStamp(startPenalty, penalty.getMinutes()*banTimesInt);
		    				DataHelper.setDriverPenalty(trip.getDriverId(),penalty.getPenaltyId() ,trip.getTripId(),null, startPenalty, endPenalty,Penalty.Event.delay.toString());
		    				try {
								//TODO Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null, false);
		        				Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null);
		    					
		    				} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} else {
						Timestamp startPenalty = new Timestamp(new Date().getTime());
	    				Timestamp endPenalty = PenaltyManager.addMinutesToTimeStamp(startPenalty, penalty.getMinutes());
	    				DataHelper.setDriverPenalty(trip.getDriverId(),penalty.getPenaltyId(),trip.getTripId() , null, startPenalty, endPenalty,Penalty.Event.delay.toString());
	    				try {
	    					//TODO Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null, false);
	        				Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null);	
	    					
	    				} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Added penalty on cancel occasion
	 * @param trip
	 * @throws SQLException
	 */
	private static void executeCancelPenalty(Trip trip) throws SQLException {
		
		Long minuteDiff = timestampMinuteDifference(trip.getAssignTime(),trip.getCancelTime());
		Penalty penalty = DataHelper.getCallTypeCancelPenalty(trip.getCallTypeId(), minuteDiff.intValue());
		
		LOGGER.info("The minute diff "+minuteDiff);
		
		if(penalty!=null) {
			
			if(penalty.getPerMinutes()!=null&&penalty.getPerMinutes().compareTo(0)!=0) {
				
				Integer penaltyMinutes = minuteDiff.intValue()-penalty.getOffSet();
				BigDecimal banTimes = new BigDecimal(penaltyMinutes.toString()).divide(new BigDecimal(penalty.getPerMinutes().toString()));
				int banTimesInt = banTimes.intValue()+1;
				
				if(banTimesInt>0) {
					Timestamp startPenalty = new Timestamp(new Date().getTime());
    				Timestamp endPenalty = PenaltyManager.addMinutesToTimeStamp(startPenalty, penalty.getMinutes()*banTimesInt);
    				DataHelper.setDriverPenalty(trip.getDriverId(),penalty.getPenaltyId(), trip.getTripId(),null, startPenalty, endPenalty,Penalty.Event.cancel.toString());
    				try {
    					//TODO Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null, false);
    					Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null);
    				} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				Timestamp startPenalty = new Timestamp(new Date().getTime());
				Timestamp endPenalty = PenaltyManager.addMinutesToTimeStamp(startPenalty, penalty.getMinutes());
				
				LOGGER.info("The start penalty ");
				
				DataHelper.setDriverPenalty(trip.getDriverId(),penalty.getPenaltyId(),trip.getTripId(), null, startPenalty, endPenalty,Penalty.Event.cancel.toString());
				try {
					//TODO Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null, false);
					Zones.getInstance().updateDriverZone(null, trip.getDriverId(), false, null, null);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
	
	}
	
	
	/**
	 * Minutes in timestamp
	 * @param first
	 * @param last
	 * @return
	 */
	public static long timestampMinuteDifference(Timestamp first,Timestamp last) {
		long milliseconds1 = first.getTime();
		long milliseconds2 = last.getTime();
		long diff = milliseconds2 - milliseconds1;
		long diffSeconds = diff / 1000;
		long diffMinutes = diff / (60 * 1000);
		long diffHours = diff / (60 * 60 * 1000);
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffMinutes;
	}

	/**
	 * Add minutes to timestamp
	 * @param timestamp
	 * @param minutes
	 * @return
	 */
	public static Timestamp addMinutesToTimeStamp(Timestamp timestamp,Integer minutes) {
		
		long timeLong = timestamp.getTime();
		return new Timestamp(timeLong+minutes*60*1000) ;
	}
	
	/**
	 * Add hours to timestamp
	 * @param timestamp
	 * @param hours
	 * @return
	 */
	public static Timestamp addHoursToTimeStamp(Timestamp timestamp,Integer hours) {
		long nowtime = timestamp.getTime();
	    return new Timestamp(nowtime+hours*60*60*1000);
	}
	
}
