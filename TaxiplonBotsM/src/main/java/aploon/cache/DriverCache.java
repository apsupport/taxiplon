package aploon.cache;

import java.util.HashMap;
import java.util.Map;

import aploon.model.Driver;

public class DriverCache {

	private static DriverCache instance;
	
	private Map<Long, Driver> driverCache;
	private Map<String, Long> imeiCache;
	
	static {
		instance = new DriverCache();
	}
	
	private DriverCache() {}
	
	public static Driver getDriver(long driverId) {
		return instance.driverCache.get(driverId);	
	}

	public static Driver getDriverByImei(String imei) {
		
		Long driverId = instance.imeiCache.get(imei);
		Driver driver = instance.driverCache.get(driverId);
		return driver;
	}
	
	public static void setDrivers(Map<Long, Driver> drivers) {
			
		instance.imeiCache = new HashMap<String, Long>();
		
		for(Long driverId:drivers.keySet()) {
			Driver driver = drivers.get(driverId);
			instance.imeiCache.put(driver.getDevice().getImei(), driverId);
		}
		
		instance.driverCache = drivers;
	}
	
}