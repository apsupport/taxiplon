package aploon.datahelper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.jolbox.bonecp.BoneCPDataSource;

public class StatusDatasourceProvider {

	private static BoneCPDataSource dataSource;
	
	private static final Logger LOGGER = Logger.getLogger(StatusDatasourceProvider.class);
	
	private StatusDatasourceProvider() {};
	
	static {
		
		LOGGER.info("Initializing the status datasource");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			Properties properties = new Properties();
			properties.load(new FileInputStream("DB.properties"));
			
			BoneCPDataSource boneCPDataSource = new BoneCPDataSource();
			boneCPDataSource.setJdbcUrl(properties.getProperty("jdbcConnection"));
			boneCPDataSource.setUsername(properties.getProperty("user"));
			boneCPDataSource.setPassword(properties.getProperty("password"));
			
			Integer minConnections = null;
			
			if(properties.getProperty("status_minconnections")!=null) {
				String minConnectionsStr = properties.getProperty("status_minconnections");
				minConnections = Integer.parseInt(minConnectionsStr);
			} else {
				minConnections = 3;
			}
			
			boneCPDataSource.setMinConnectionsPerPartition(minConnections);
			
			Integer maxConnections = null;
			
			if(properties.getProperty("status_maxconnections")!=null) {
				String maxConnectionsStr = properties.getProperty("status_maxconnections");
				maxConnections = Integer.parseInt(maxConnectionsStr);
			} else {
				maxConnections = 5;
			}
			
			boneCPDataSource.setMaxConnectionsPerPartition(maxConnections);
			
			boneCPDataSource.setPartitionCount(1);
			
			dataSource = boneCPDataSource;
			
		} catch (ClassNotFoundException | FileNotFoundException e) {
			e.printStackTrace();
			LOGGER.error("Mysql class not loaded properties not found",e);
			throw new RuntimeException("Fatal Error",e);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("Problem with the properties file",e);
			throw new RuntimeException("Fatal Error",e);
		}
			
	}
	
	public static DataSource getDataSource() {
		
		LOGGER.debug("Giving status datasource");
		
		return dataSource;
	}
	
	public static Connection getConnection() throws SQLException {
	
		LOGGER.debug("Connection from status datasource "+dataSource.hashCode());
		
		return dataSource.getConnection();
	}

	public static void closeDatasource() {
	
		dataSource.close();
	}
}
