package aploon.datahelper;


import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import aploon.model.CallType;
import aploon.model.Coordinates;
import aploon.model.Device;
import aploon.model.Driver;
import aploon.model.DriverAssignedTrip;
import aploon.model.DriverClaimedTrip;
import aploon.model.Message;
import aploon.model.Offer;
import aploon.model.OfferDataPerRide;
import aploon.model.OfferPerRide;
import aploon.model.OnRideStatusInfo;
import aploon.model.Passenger;
import aploon.model.Penalty;
import aploon.model.TaxiCompany;
import aploon.model.Trip;
import aploon.model.TripAssignPicked;
import aploon.model.TripData;
import aploon.model.TripHistoryModel;
import aploon.model.TripSummary;
import aploon.model.Vehicle;
import aploon.model.Zone;
import aploon.xml.model.NearByDriverXML;
import aploon.xml.model.NearbyCompanyXML;

import com.taxiplon.sms.model.SMSSettings;


public class DataHelper {

    private static final Logger LOGGER = Logger.getLogger(DataHelper.class);

    private DataHelper() {}
    
    public static Long insertToSMSLog(Connection connection,long taxicompanyId,String mobile,String message,String status) throws SQLException {
        
        CallableStatement insertToSMSLogStmt = null;

        try {
            insertToSMSLogStmt = connection.prepareCall("CALL insertToSMSLog(?,?,?,?,?)");
            insertToSMSLogStmt.setLong(1, taxicompanyId);
            insertToSMSLogStmt.setString(2, mobile);
            insertToSMSLogStmt.setString(3, message);
            insertToSMSLogStmt.setString(4, status);
            insertToSMSLogStmt.registerOutParameter(5, Types.NUMERIC);
            insertToSMSLogStmt.execute();
            
            Long lastInsertId = insertToSMSLogStmt.getLong("smsIdOut");
            
            return lastInsertId;
            
        } finally {
            if(insertToSMSLogStmt!=null) insertToSMSLogStmt.close();    
        }
    
    }

    public static SMSSettings getSmsSettings(Connection connection ,Long taxiCompanyId) throws SQLException {
        
        PreparedStatement getSmsSettingsStmt = null;
        
        try {
    
            getSmsSettingsStmt = connection.prepareStatement("SELECT SMSFromName,Provider,Username,Password FROM taxicompany INNER JOIN smsgateway ON taxicompany.GatewayID=smsgateway.GatewayID WHERE taxicompanyid=?");
            getSmsSettingsStmt.setLong(1, taxiCompanyId);
            ResultSet resultSet = getSmsSettingsStmt.executeQuery();
            
            SMSSettings smsSettings = null;
            
            if(resultSet.next()) {
                try {
                    smsSettings = new SMSSettings();
                    smsSettings.setGateWay(resultSet.getString("Provider"));
                    smsSettings.setUserName(resultSet.getString("Username"));
                    smsSettings.setPassWord(resultSet.getString("Password"));
                    smsSettings.setFromName(resultSet.getString("SMSFromName"));
                } catch(IllegalArgumentException e) {
                    LOGGER.error("Holly Dump",e);
                }
            }
            
            resultSet.close();
            return smsSettings;
    
        } finally {
            if(getSmsSettingsStmt!=null) getSmsSettingsStmt.close();    
        }
        
    }

    public static boolean setPassengerPassword(Connection connection,long passengerId,long taxicompanyId,String mobile,String pin) throws SQLException {
        
        PreparedStatement setPassengerPasswordStmt = null;
        
        try {
            
            setPassengerPasswordStmt = connection.prepareStatement("UPDATE passenger SET pin=? WHERE PassengerID=? AND TaxiCompanyID=? AND mobile=?");
            setPassengerPasswordStmt.setString(1, pin);
            setPassengerPasswordStmt.setLong(2, passengerId);
            setPassengerPasswordStmt.setLong(3, taxicompanyId);
            setPassengerPasswordStmt.setString(4, mobile);
            
            return setPassengerPasswordStmt.execute();
    
        } finally {
            if(setPassengerPasswordStmt!=null) setPassengerPasswordStmt.close();    
        }
        
    }

    public static Passenger createPassenger(Connection connection ,Long passengerId) throws SQLException {
    
        PreparedStatement findPassengerStmt = null;
        
        try {
            findPassengerStmt = connection.prepareStatement("SELECT mobile,imei WHERE passengerID=?");
            findPassengerStmt.setLong(1,passengerId);
            ResultSet resultSet = findPassengerStmt.executeQuery();
            
            Passenger passenger = null;
            
            if(resultSet.next()) {
                
                passenger = new Passenger();
                passenger.setMobile(resultSet.getString("mobile"));
                passenger.setImei(resultSet.getString("imei"));
            }
            
            resultSet.close();
            return passenger;
         
        } finally {
            if(findPassengerStmt!=null) findPassengerStmt.close();    
        }
        
    }

    public static Passenger getPassenger(Connection connection,Long passid) throws SQLException {
        
        PreparedStatement getPassengerStmt = null;
        
        try {
        
            Passenger passenger = null;
        
            getPassengerStmt = connection.prepareStatement("SELECT mobile,imei FROM passenger WHERE passengerID=?");
            getPassengerStmt.setLong(1, passid);
            ResultSet resultSet = getPassengerStmt.executeQuery();
            
            if(resultSet.next()) {

                passenger = new Passenger();                
                passenger.setMobile(resultSet.getString("mobile"));
                passenger.setImei(resultSet.getString("imei"));
                passenger.setPassengerId(passid);
            }
            
            return passenger;            

        } finally {
            if(getPassengerStmt!=null) getPassengerStmt.close();
        }

    }

    public static Passenger mobileExists(Connection connection,String mobile,Long companyId, String countryCode) throws SQLException {
        
        PreparedStatement mobileExistsStmt = null;
        
        try {       
            
            if(companyId==null) companyId = getDefaultCompany(connection, countryCode);
            
            mobileExistsStmt = connection.prepareStatement("SELECT PassengerID,imei FROM passenger WHERE mobile=? and taxicompanyid=?");
            mobileExistsStmt.setString(1, mobile);
            mobileExistsStmt.setLong(2, companyId);

            ResultSet resultSet = mobileExistsStmt.executeQuery();
            
            Passenger passenger = null;
            
            if(resultSet.next()) {
                
                passenger = new Passenger();
                passenger.setPassengerId(resultSet.getLong("PassengerID"));
                passenger.setImei(resultSet.getString("imei"));;
                passenger.setMobile(mobile);
            }
            
            resultSet.close();
            
            return passenger;
            
        } finally {
            if(mobileExistsStmt!=null) mobileExistsStmt.close();
        }
   
    }

    public static List<Long> getTripAssignDrivers(long tripId) throws SQLException {
    
    	Connection connection = null;
        PreparedStatement getTripAssignDriversStmt = null;
        
        try {
        	connection = DatasourceProvider.getConnection();
        	getTripAssignDriversStmt = connection.prepareStatement("SELECT driver.driverid FROM trip_assign,driver "
                    + "WHERE tripid=? AND driver.deviceid=trip_assign.deviceid");
        	getTripAssignDriversStmt.setLong(1, tripId);
        	ResultSet resultSet = getTripAssignDriversStmt.executeQuery();
        	List<Long> driverIds = new ArrayList<>();
        	while (resultSet.next()) {
        		Long driverId = resultSet.getLong("driverid");
        		driverIds.add(driverId);
        	}
        	resultSet.close();
        	return driverIds;
        	
        } finally {
        	if(getTripAssignDriversStmt!=null) getTripAssignDriversStmt.close();
            if(connection!=null) connection.close();
        }
        
    }
    
    public static long getDefaultCompany(Connection connection, String countryCode) throws SQLException {
        
        PreparedStatement getDefaultCompanyStmt = null;
        
        try {
        
            getDefaultCompanyStmt = connection.prepareStatement("SELECT default_company(?) as TaxiCompanyID");
            getDefaultCompanyStmt.setString(1, countryCode);
            ResultSet resultSet = getDefaultCompanyStmt.executeQuery();
            
            long defaultCompany = -1L;
            
            if(resultSet.next()) {
                
                defaultCompany = resultSet.getLong("TaxiCompanyID");
            } 
            
            resultSet.close();
            return defaultCompany;
            
        } finally {
            if(getDefaultCompanyStmt!=null)getDefaultCompanyStmt.close();    
        }
        
    }

    public static boolean updateImeiAsMobile(Connection connection,Long passengerId,String mobile) throws SQLException {
        
        PreparedStatement preparedStmt = null;
        
        try {
            preparedStmt = connection.prepareStatement("UPDATE passenger SET imei=? WHERE passengerID=?");
            preparedStmt.setString(1, mobile);
            preparedStmt.setLong(2, passengerId);
            return preparedStmt.execute(); 
        } finally {
            if(preparedStmt!=null) preparedStmt.close();
        }
        
    }
    
    public static int setTripArrivalTime(long tripId) throws SQLException {

        Connection connection = null;
        PreparedStatement setTripArrivalTimeStmt = null;

        try {

                connection = DatasourceProvider.getConnection();
                setTripArrivalTimeStmt = connection.prepareStatement("UPDATE trip SET ArrivalTime=now() "
                                + "WHERE tripid=? AND ArrivalTime IS NULL");
                setTripArrivalTimeStmt.setLong(1, tripId);
                return setTripArrivalTimeStmt.executeUpdate();

        } finally {
                if(setTripArrivalTimeStmt!=null) setTripArrivalTimeStmt.close();
                if(connection!=null) connection.close();
        }

    }
    
    public static String imeiForMobile(Connection connection ,String mobile) throws SQLException {
        
        PreparedStatement preparedStmt = null;                
        try {
            preparedStmt = connection.prepareStatement("SELECT imei FROM passenger WHERE mobile=?");
            preparedStmt.setString(1, mobile);
            ResultSet resultSet = preparedStmt.executeQuery();
            
            String imei = null;
            
            while(resultSet.next()) {
                imei = resultSet.getString("imei");
            }
            
            resultSet.close();    
            return imei;

        } finally {
            if(preparedStmt!=null) preparedStmt.close();    
        }
    }
    
    public static List<TripHistoryModel> getTripHistory(Connection connection,long passengerId) throws SQLException {
        
        List<TripHistoryModel> tripHistoryModels = new ArrayList<TripHistoryModel>();
        
        PreparedStatement preparedStatement = null;
        
        try {
        
            preparedStatement = connection.prepareStatement("select t.tripid, t.hailtime, t.pickuptime, t.pickupaddress, t.destination, t.specialneeds, t.status, t.bookagent, t.rating, d.driverid, d.lastname, d.firstname, d.codename, v.plateno as platenumber, v.model as carmodel, f.driverid as isfaved, b.driverid as isblocked, pa.amount, pa.currency, pa.channel, tc.email, tc.facebook, tc.twitter from trip t " + 
                                                            "  left outer join payments pa on t.paymentid = pa.paymentid and pa.origin = 'trip'," + 
                                                            "  vehicle v, driver d" + 
                                                            "  left outer join favourites f on f.driverid = d.driverid and f.passengerid = ?" + 
                                                            "  left outer join blacklisted b on b.driverid = d.driverid and b.passengerid = ?," + 
                                                            "  taxicompany tc" + 
                                                            " where t.passengerid = ?" + 
                                                            " and t.status = 'com'"+
                                                            " and t.vehicleid = v.vehicleid" + 
                                                            " and t.driverid = d.driverid" + 
                                                            " and t.taxicompanyid = tc.taxicompanyid ORDER BY t.pickuptime DESC LIMIT 10");
            
            preparedStatement.setLong(1, passengerId);
            preparedStatement.setLong(2, passengerId);
            preparedStatement.setLong(3, passengerId);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()) {
                
                TripHistoryModel tripHistoryModel = new TripHistoryModel();
                tripHistoryModel.setTripid(resultSet.getLong("tripid"));
                tripHistoryModel.setHailtime(resultSet.getTimestamp("hailtime"));
                tripHistoryModel.setPickuptime(resultSet.getTimestamp("pickuptime"));
                tripHistoryModel.setFrom(resultSet.getString("pickupaddress"));
                tripHistoryModel.setTo(resultSet.getString("destination"));
                tripHistoryModel.setSpecial(resultSet.getString("specialneeds"));
                tripHistoryModel.setStatus(resultSet.getString("status"));
                tripHistoryModel.setBookAgent(resultSet.getString("bookagent"));
                tripHistoryModel.setRating(resultSet.getInt("rating"));
                tripHistoryModel.setPlateno(resultSet.getString("platenumber"));
                tripHistoryModel.setCarmodel(resultSet.getString("carmodel"));
                tripHistoryModel.setDrivername(resultSet.getString("firstname"));
                tripHistoryModel.setDriversurname(resultSet.getString("lastname"));
                tripHistoryModel.setDrivercodename(resultSet.getString("codename"));
                tripHistoryModel.setIsFavorite(resultSet.getString("isfaved"));
                tripHistoryModel.setAmount(resultSet.getDouble("amount"));
                tripHistoryModel.setCurr(resultSet.getString("currency"));
                tripHistoryModel.setChan(resultSet.getString("channel"));
                tripHistoryModel.setEmail(resultSet.getString("email"));
                tripHistoryModel.setFacebook(resultSet.getString("facebook"));
                tripHistoryModel.setTwitter(resultSet.getString("twitter"));
                tripHistoryModel.setDriverId(resultSet.getLong("driverid"));
                tripHistoryModel.setIsblocked(resultSet.getString("isblocked"));
                 
                tripHistoryModels.add(tripHistoryModel);
                
            }
            
            resultSet.close();
            
        
        } finally {
            if(preparedStatement!=null) preparedStatement.close();
        }
        
        return tripHistoryModels;
    }
    
    public static String ifImeiExistsUpdate(Connection connection,String imei) throws SQLException {
    
        PreparedStatement imeiExistsStmt = null;
        PreparedStatement updateImeiStmt = null;
    
        String newimei = null;
    
        try {
    
            imeiExistsStmt = connection.prepareStatement("SELECT imei FROM passenger WHERE imei=?");
            imeiExistsStmt.setString(1, imei);
            ResultSet imeiSet = imeiExistsStmt.executeQuery();    
            
            if(imeiSet.next()) {
            
                String oldImei = imeiSet.getString("imei"); 
                oldImei = "old_"+oldImei;
                
                updateImeiStmt = connection.prepareStatement("UPDATE passenger SET imei=? WHERE imei=?");
                updateImeiStmt.setString(1, oldImei);
                updateImeiStmt.setString(2, imei);
                if(updateImeiStmt.execute()) {
                    newimei = oldImei;
                }
                
                updateImeiStmt.close();
            }
        
            return newimei;
            
        } finally {
            if(imeiExistsStmt!=null) imeiExistsStmt.close();
            if(updateImeiStmt!=null) updateImeiStmt.close();
        }
    }
    
    public static String getPassengerCallCode(Connection connection,long passengerId) throws SQLException {
        
        PreparedStatement preparedStatement = null;
        
        try {
        
            preparedStatement = connection.prepareStatement("SELECT CALLcode FROM countrycodes WHERE ISOcode = (SELECT OriginCountry FROM passenger WHERE passengerid=?)");
            preparedStatement.setLong(1, passengerId);
            ResultSet resultSet = preparedStatement.executeQuery(); 
            String callCode = null;
            
            if(resultSet.next()) {
            
                callCode = resultSet.getString("CALLcode");        
            }
            resultSet.close();
            
            return callCode;
            
        } finally {
            if(preparedStatement!=null) preparedStatement.close();
        }
        
    }
    
    public static String getCompanyCallCode(Connection connection,long companyId) throws SQLException { 
                                         
        PreparedStatement preparedStatement = null;
        
        try {
            
            preparedStatement = connection.prepareStatement("");
            preparedStatement.setLong(1, companyId);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            String callCode = null;
            
            if(resultSet.next()) {
            
                callCode = resultSet.getString("CALLcode");    
            }
            
            resultSet.close();

            return callCode;
                
        } finally {
            if(preparedStatement!=null) preparedStatement.close();
        }

    }
    
    public static String getCallCode(Connection connection,String isoCode) throws SQLException {
        
        PreparedStatement preparedStatement = null;
        
        try {
            
            preparedStatement = connection.prepareStatement("SELECT CALLcode FROM countrycodes WHERE ISOcode=? ");
            preparedStatement.setString(1, isoCode);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            String callCode = null;
            
            if(resultSet.next()) {
                
                callCode = resultSet.getString("CALLcode");
            }
            
            resultSet.close();
            
            return callCode;
            
        } finally {
            if(preparedStatement!=null) preparedStatement.close();   
        }
           
    }

    public static List<DriverAssignedTrip> getDriverAssignedTrips(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement driverAssignedTripsStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		driverAssignedTripsStmt = connection.prepareStatement("SELECT TripID,HailTime,PickUpAddress,SpecialNeeds,Status,Fare,PaymentType,ShortName FROM trip "
    				+ "INNER join taxicompany ON trip.TaxiCompanyID=taxicompany.TaxiCompanyID  "
    				+ "WHERE driverid=? ORDER BY HailTime DESC LIMIT 25");
    		driverAssignedTripsStmt.setLong(1, driverId);
    		ResultSet resultSet = driverAssignedTripsStmt.executeQuery();
    		
    		List<DriverAssignedTrip> driverAssignedTrips = new ArrayList<DriverAssignedTrip>();
    		
    		while(resultSet.next()) {
    			
    			DriverAssignedTrip driverAssignedTrip = new DriverAssignedTrip();
    			
    			driverAssignedTrip.setTripId(resultSet.getLong("TripID"));
    			driverAssignedTrip.setHailTime(resultSet.getTimestamp("HailTime"));
    			driverAssignedTrip.setPickUpAddress(resultSet.getString("PickUpAddress"));
    			driverAssignedTrip.setSpecialNeeds(resultSet.getString("SpecialNeeds"));
    			driverAssignedTrip.setStatus(resultSet.getString("Status"));
    			driverAssignedTrip.setFare(resultSet.getDouble("Fare"));
    			driverAssignedTrip.setPaymentType(resultSet.getString("PaymentType"));
    			driverAssignedTrip.setShortName(resultSet.getString("ShortName"));
    			
    			driverAssignedTrips.add(driverAssignedTrip);
    
    		}
    		
    		resultSet.close();
    		return driverAssignedTrips;
    		
    	} finally {
    		if(driverAssignedTripsStmt!=null) driverAssignedTripsStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<DriverClaimedTrip> getDriverClaimedTrips(long driverId) throws SQLException {
		
    	Connection connection = null;
    	PreparedStatement driverClaimedTripsStmt = null;
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		
    		driverClaimedTripsStmt = connection.prepareStatement("SELECT ta.TripID, t.HailTime, t.PickUpAddress, tc.ShortName, ta.Comments, t.pickuplat, t.pickuplong, t.SpecialNeeds "
    				+ "FROM trip_assign ta, trip t, taxicompany tc "
    				+ "WHERE ta.TripID=t.TripID "
    				+ "AND t.TaxiCompanyID = tc.TaxiCompanyID "
    				+ "AND ta.driverid=? "
    				+ "AND t.hailtime > date_add(now(), interval -2 week)"
    				+ "ORDER BY t.tripid DESC LIMIT 25");
    		
    		driverClaimedTripsStmt.setLong(1, driverId);
    		
    		ResultSet resultSet = driverClaimedTripsStmt.executeQuery();
    		
    		List<DriverClaimedTrip> driverClaimedTrips = new ArrayList<DriverClaimedTrip>();
    		
    		while(resultSet.next()) {
    			
    			DriverClaimedTrip driverClaimedTrip = new DriverClaimedTrip();
    			driverClaimedTrip.setTripId(resultSet.getLong("TripID"));
    			driverClaimedTrip.setHailTime(resultSet.getTimestamp("HailTime"));
    			driverClaimedTrip.setPickUpAddress(resultSet.getString("PickUpAddress"));
    			driverClaimedTrip.setShortName(resultSet.getString("ShortName"));
    			driverClaimedTrip.setComments(resultSet.getString("Comments"));
    			driverClaimedTrip.setLatitude(resultSet.getDouble("pickuplat"));
    			driverClaimedTrip.setLongtitude(resultSet.getDouble("pickuplong"));
    			driverClaimedTrip.setSpecialNeeds(resultSet.getString("SpecialNeeds"));
    			
    			driverClaimedTrips.add(driverClaimedTrip);
    		}
    		
    		resultSet.close();
    		return driverClaimedTrips;
    		
    	} finally {
    		if(driverClaimedTripsStmt!=null) driverClaimedTripsStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
	}

    public static List<TripAssignPicked> getTripAssignPicked(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement tripAssignPickedStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		tripAssignPickedStmt = connection.prepareStatement("SELECT trip_assign.TripID,trip_assign.DeviceID,trip_assign.DriverID,"
    				+ "driver.Codename,driver.LastName,driver.FirstName,"
    				+ "trip_assign.lat,trip_assign.lng,trip_assign.distance,"
    				+ "trip_assign.distanceAssigned,trip_assign.Comments FROM "
    				+ "trip_assign INNER JOIN driver ON driver.DriverId=trip_assign.DriverID WHERE tripid=? ORDER BY trip_assign.distance");
    		
    		tripAssignPickedStmt.setLong(1, tripId);
    		
    		
    		ResultSet resultSet = tripAssignPickedStmt.executeQuery();
    		
    		List<TripAssignPicked> tripAssignPickeds = new ArrayList<>();
    		
    		while (resultSet.next()) {
				
    			TripAssignPicked tripAssignPicked = new TripAssignPicked();
    			tripAssignPicked.setTripId(resultSet.getLong("TripID"));
    			tripAssignPicked.setDeviceId(resultSet.getLong("DeviceID"));
    			tripAssignPicked.setDriverId(resultSet.getLong("DriverID"));
    			tripAssignPicked.setCodeName(resultSet.getString("Codename"));
    			tripAssignPicked.setLastName(resultSet.getString("LastName"));
    			tripAssignPicked.setFirstName(resultSet.getString("FirstName"));
    			tripAssignPicked.setLatitude(resultSet.getDouble("lat"));
    			tripAssignPicked.setLongtitude(resultSet.getDouble("lng"));
    			tripAssignPicked.setDistance(resultSet.getDouble("distance"));
    			tripAssignPicked.setDistanceAssigned(resultSet.getDouble("distanceAssigned"));
    			tripAssignPicked.setComments(resultSet.getString("Comments"));

    			tripAssignPickeds.add(tripAssignPicked);
			}
    		
    		resultSet.close();
    		return tripAssignPickeds;
    	    		
    	} finally {
    		if(tripAssignPickedStmt!=null)tripAssignPickedStmt.close();
    		if(connection!=null)connection.close();
    	}
    	
    }
    
    public static List<TripSummary> getDriverTripSummary(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement assignedTripsStmt = null;
    	PreparedStatement lostTripsStmt = null;
    	
    	try {

    		connection = DatasourceProvider.getConnection();
    		
    		assignedTripsStmt = connection.prepareStatement("SELECT  COUNT(tripid) as assigned, MONTH(hailtime) as month, YEAR(hailtime) as year "
    				+ "FROM trip WHERE driverid = ? AND status = 'com' "
    				+ "GROUP BY YEAR(hailtime), MONTH(hailtime) "
    				+ "ORDER BY YEAR(hailtime) DESC, MONTH(hailtime) DESC LIMIT 6");
    		
    		assignedTripsStmt.setLong(1, driverId);
    		
    		lostTripsStmt = connection.prepareStatement("SELECT  COUNT(trip.tripid) as claimed ,MONTH(trip.HailTime) as month , YEAR(trip.HailTime) as year "
    				+ "FROM trip_assign INNER JOIN trip ON trip.tripid=trip_assign.tripid "
    				+ "WHERE trip_assign.driverid=? "
    				+ "GROUP BY YEAR(trip.HailTime), MONTH(trip.HailTime) "
    				+ "ORDER BY YEAR(trip.HailTime) DESC, MONTH(trip.HailTime) DESC LIMIT 6");
    		
    		lostTripsStmt.setLong(1, driverId);
    		
    		ResultSet assignedTripsResultSet = assignedTripsStmt.executeQuery();
    		ResultSet lostTripsResultSet = lostTripsStmt.executeQuery();
    		
    		List<TripSummary> tripSummaries = new ArrayList<>();
    		
    		while(assignedTripsResultSet.next()&&lostTripsResultSet.next()) {
    			
    			TripSummary tripSummary = new TripSummary();
    			tripSummary.setAssigned(assignedTripsResultSet.getInt("assigned"));
    			tripSummary.setLost(lostTripsResultSet.getInt("claimed"));
    			tripSummary.setMonth(assignedTripsResultSet.getInt("month"));
    			tripSummary.setYear(assignedTripsResultSet.getInt("year"));
    			
    			tripSummaries.add(tripSummary);
    		}
    		
    		assignedTripsResultSet.close();
    		lostTripsResultSet.close();
    		
    		return tripSummaries;
    		
    	} finally {
    		if(lostTripsStmt!=null) lostTripsStmt.close();
    		if(assignedTripsStmt!=null) assignedTripsStmt.close();
    		if(connection!=null)connection.close();
    	}
    	
    }
    
    public static long getDefaultCompany(String countryCode) throws SQLException {
        
    	Connection connection = null;
        PreparedStatement getDefaultCompanyStmt = null;
        
        try {
        
        	connection = DatasourceProvider.getConnection();
        	
            getDefaultCompanyStmt = connection.prepareStatement("SELECT default_company(?) as TaxiCompanyID");
            getDefaultCompanyStmt.setString(1, countryCode);
            ResultSet resultSet = getDefaultCompanyStmt.executeQuery();
            
            long defaultCompany = -1L;
            
            if(resultSet.next()) {
                
                defaultCompany = resultSet.getLong("TaxiCompanyID");
            } 
            
            resultSet.close();
            return defaultCompany;
            
        } finally {
            if(getDefaultCompanyStmt!=null)getDefaultCompanyStmt.close();    
        	if(connection!=null) connection.close();
        }
        
    }    

    public static List<Offer> getTaxiCompanyOffers(long taxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCompanyOffersStmt = null;
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		getCompanyOffersStmt = connection.prepareStatement("SELECT OfferID,Type,StartDate,EndDate FROM offer "
    				+ "WHERE TaxiCompanyID=? AND StartDate<=now() AND (EndDate>=now() OR EndDate IS NULL)");
    		getCompanyOffersStmt.setLong(1, taxiCompanyId);
    		ResultSet resultSet = getCompanyOffersStmt.executeQuery();
    		
    		List<Offer> offers = new ArrayList<>();
    		
    		while (resultSet.next()) {
    			
    			Offer offer = new Offer();
    			offer.setOfferId(resultSet.getLong("OfferID"));
    			offer.setType(Offer.Type.valueOf(resultSet.getString("type")));
    			offer.setStartDate(resultSet.getTimestamp("StartDate"));
    			offer.setEndDate(resultSet.getTimestamp("EndDate"));
    			offer.setTaxiCompanyId(taxiCompanyId);
    			offers.add(offer);
			}
    		
    		resultSet.close();
    		return offers;
    		
    	} finally {
    		if(getCompanyOffersStmt!=null) getCompanyOffersStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }

    public static List<OfferPerRide> getTaxiCompanyOfferPerRides(long taxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getTaxiCompanyOfferPerRidesStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getTaxiCompanyOfferPerRidesStmt = connection.prepareStatement("SELECT OfferPerRideID,CallType,Rides,Discount FROM offerperride");
    		ResultSet resultSet = getTaxiCompanyOfferPerRidesStmt.executeQuery();
    	
    		List<OfferPerRide> offerPerRides = new ArrayList<>();
    		
    		while(resultSet.next()) {
    			
    			OfferPerRide offerPerRide = new OfferPerRide();
    			offerPerRide.setOfferPerRideID(resultSet.getLong("OfferPerRideID"));
    			offerPerRide.setCallType(OfferPerRide.CallType.valueOf(resultSet.getString("CallType")));
    			offerPerRide.setRides(resultSet.getInt("Rides"));
    			offerPerRide.setDiscount(resultSet.getDouble("Discount"));
    			offerPerRide.setTaxiCompanyID(taxiCompanyId);
    			offerPerRides.add(offerPerRide);
    		
    		}
    		
    		resultSet.close();
    		return offerPerRides;
    		
    	} finally {
    		if(getTaxiCompanyOfferPerRidesStmt!=null) getTaxiCompanyOfferPerRidesStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }

    public static void createOrUpdatePassenger(Long passengerId, Long taxicompanyId, String imei, String gender, String name,
			 String surname, String address, String area, String mobile, String phone, String email, String nick,
			 String plainTextPin, String status, String softwareversion, String country, String comments,
			 String typio) throws SQLException {
    
    	Connection connection = null;
    	PreparedStatement createOrUpdatePassengeStmt = null;
    	
    	try {
    		
    		
    		
    	} finally {
    		if(createOrUpdatePassengeStmt!=null) createOrUpdatePassengeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static void createPassengerOfferDataPerRide(long taxiCompanyId,long passengerId,int dispatcherCalls,int smartPhoneCalls,int webCalls) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement createPassengerOfferDataPerRideStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		createPassengerOfferDataPerRideStmt = connection.prepareStatement("INSERT INTO offerdataperride (TaxiCompanyID,PassengerID,DispatcherCalls,SmartPhoneCalls,WebCalls) VALUES(?,?,?,?,?)");
    		createPassengerOfferDataPerRideStmt.setLong(1, taxiCompanyId);
    		createPassengerOfferDataPerRideStmt.setLong(2, passengerId);
    		createPassengerOfferDataPerRideStmt.setInt(3, dispatcherCalls);
    		createPassengerOfferDataPerRideStmt.setInt(4, smartPhoneCalls);
    		createPassengerOfferDataPerRideStmt.setInt(5, webCalls);
    		createPassengerOfferDataPerRideStmt.executeUpdate();
    		
    	} finally {
    		if(createPassengerOfferDataPerRideStmt!=null) createPassengerOfferDataPerRideStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static OfferDataPerRide getPassengerOfferDataPerRide(long passengerId,long taxiCompanyId) throws SQLException {
 
    	Connection connection = null;
    	PreparedStatement getPassengerOfferDataPerRideStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getPassengerOfferDataPerRideStmt = connection.prepareStatement("SELECT OfferDataPerRideID,DispatcherCalls,SmartPhoneCalls,WebCalls "
    				+ "FROM offerdataperride WHERE passengerid=? AND taxicompanyid=?");
    		getPassengerOfferDataPerRideStmt.setLong(1, passengerId);
    		getPassengerOfferDataPerRideStmt.setLong(2, taxiCompanyId);
    		ResultSet resultSet = getPassengerOfferDataPerRideStmt.executeQuery();
    		
    		OfferDataPerRide offerDataPerRide = null;
    		
    		if(resultSet.next()) {
    			
    			offerDataPerRide = new OfferDataPerRide();
    			offerDataPerRide.setOfferDataPerRideID(resultSet.getLong("OfferDataPerRideID"));
    			offerDataPerRide.setDispatcherCalls(resultSet.getInt("DispatcherCalls"));
    			offerDataPerRide.setSmartPhoneCalls(resultSet.getInt("SmartPhoneCalls"));
    			offerDataPerRide.setWebCalls(resultSet.getInt("WebCalls"));
    			offerDataPerRide.setPassengerID(passengerId);
    			offerDataPerRide.setTaxiCompanyID(taxiCompanyId);
    		}
    		
    		resultSet.close();
    		
    		return offerDataPerRide;
    		
    	} finally {
    		if(getPassengerOfferDataPerRideStmt!=null) getPassengerOfferDataPerRideStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static void updatePassengerOfferDataPerRide(long passengerId,long taxiCompanyId,Integer dispatcherCalls,Integer smartPhoneCalls,Integer webCalls) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement updatePassengerDataStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		updatePassengerDataStmt = connection.prepareStatement("UPDATE offerdataperride SET DispatcherCalls=ifnull(?,DispatcherCalls), SmartPhoneCalls=ifnull(?,SmartPhoneCalls), WebCalls=ifnull(?,WebCalls) WHERE TaxiCompanyID=? AND PassengerID=?");
    		updatePassengerDataStmt.setInt(1, dispatcherCalls);
    		updatePassengerDataStmt.setInt(2, smartPhoneCalls);
    		updatePassengerDataStmt.setInt(3, webCalls);
    		updatePassengerDataStmt.setLong(4, taxiCompanyId);
    		updatePassengerDataStmt.setLong(5, passengerId);
    		
    		int updateStatus = updatePassengerDataStmt.executeUpdate();
    		
    		LOGGER.info("Passenger offer data per ride updated "+updateStatus);
    		
    	} finally {
    		if(updatePassengerDataStmt!=null) updatePassengerDataStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    	
    public static Coordinates getDriverLocation(Long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriverLocationStmt  = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriverLocationStmt = connection.prepareStatement("SELECT CurrentLat,CurrentLong FROM driver "
    				+ "INNER JOIN device ON driver.deviceid=device.deviceid "
    				+ "WHERE driverid=?");
    		getDriverLocationStmt.setLong(1, driverId);
    		ResultSet resultSet = getDriverLocationStmt.executeQuery();
   
    		Coordinates coordinates = null;
    		
    		if(resultSet.next()) {
    			
    			coordinates = new Coordinates(
    					resultSet.getBigDecimal("CurrentLat"), 
    					resultSet.getBigDecimal("CurrentLong"));
    		}
    	
    		resultSet.close();
    		return coordinates;
    		
    	} finally {
    		if(getDriverLocationStmt!=null)getDriverLocationStmt.close();
    		if(connection!=null) connection.close();
    	}
    
    }
    
    public static Map<String, Object> getLastDriverZone(long driverId) throws SQLException {
    
    	Connection connection = null;
    	PreparedStatement getLastDriverZoneStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getLastDriverZoneStmt = connection.prepareStatement("SELECT z.zoneid, z.zonename, d.imei FROM driver dr, zones z, device d WHERE dr.zoneid = z.zoneid AND d.deviceid = dr.deviceid AND dr.driverid = ?");
    		getLastDriverZoneStmt.setLong(1, driverId);
    		ResultSet resultSet = getLastDriverZoneStmt.executeQuery();
    		

    		Map<String, Object> driverZone = null; 
    		
    		if(resultSet.next()) {
    			
    			driverZone = new HashMap<>();

    			driverZone.put("zoneid", resultSet.getLong("zoneid"));
    			driverZone.put("zonename", resultSet.getString("zonename"));
    			driverZone.put("imei", resultSet.getString("imei"));
    		}
    		
    		resultSet.close();
    		return driverZone;
    		
    	} finally {
    		if(getLastDriverZoneStmt!=null) getLastDriverZoneStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<Zone> getDriverFullZones(long driverId,String taxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriverFullZonesStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		getDriverFullZonesStmt = connection.prepareStatement("SELECT z.zoneid, z.taxicompanyid, z.zonename,z.ZoneLat,z.ZoneLong,"
    				+ "z.ZoneRange,z.ZonePoints,z.StrictBorders,z.OpensAt,z.ClosesAt,count(dr.driverid) as drivers_in_zone "
    				+ "FROM zones z LEFT OUTER JOIN driver dr ON dr.zoneid = z.zoneid AND dr.zonejointime IS NOT NULL "
    				+ "WHERE (? IS NOT NULL AND ? = z.taxicompanyid ) OR (? IS NOT NULL AND z.taxicompanyid IN (SELECT dc.taxicompanyid FROM drivercompanies dc WHERE dc.driverid = ?)) "
    				+ "GROUP BY z.zoneid, z.taxicompanyid, z.zonename ORDER BY z.zoneid");
    		getDriverFullZonesStmt.setLong(1, driverId);
    		getDriverFullZonesStmt.setString(2, null);
    		getDriverFullZonesStmt.setLong(3, driverId);
    		getDriverFullZonesStmt.setLong(4, driverId);
    		
    		ResultSet resultSet = getDriverFullZonesStmt.executeQuery();
    		List<Zone> zones =  new ArrayList<>();
    		
    		while(resultSet.next()) {
    			Zone zone = new Zone();
    			zone.setZoneId(resultSet.getLong("zoneid"));
    			zone.setTaxiCompanyId(resultSet.getLong("taxicompanyid"));
    			zone.setZoneName(resultSet.getString("zonename"));
    			zone.setZoneLat(resultSet.getDouble("ZoneLat"));
    			zone.setZoneLong(resultSet.getDouble("ZoneLong"));
    			zone.setZoneRange(resultSet.getDouble("ZoneRange"));
    			//TODO
    			/**
    			zone.setOpensAt(resultSet.getTime("OpensAt"));
    			zone.setClosesAt(resultSet.getTime("ClosesAt"));
    			*/
    			zone.setDriversInZone(resultSet.getInt("drivers_in_zone"));
    			zone.setZonePoints(resultSet.getString("ZonePoints"));
    			zone.setStrictBorders(resultSet.getBoolean("StrictBorders"));
    			zones.add(zone);
    		}
    		
    		resultSet.close();
    		
    		return zones;
    	} finally {
    		if(getDriverFullZonesStmt!=null) getDriverFullZonesStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<Zone> getCompanyZones(long driverId,String taxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCompanyZonesStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		getCompanyZonesStmt = connection.prepareStatement("SELECT z.zoneid, z.taxicompanyid, z.zonename,z.ZoneLat,z.ZoneLong,z.ZoneRange,z.StrictBorders,z.ZonePoints, "
    				+ "count(dr.driverid) as drivers_in_zone "
    				+ "FROM zones z LEFT OUTER JOIN driver dr ON dr.zoneid = z.zoneid AND dr.zonejointime IS NOT NULL "
    				+ "WHERE (? IS NOT NULL AND ? = z.taxicompanyid) OR (? IS NOT NULL AND z.taxicompanyid IN (SELECT dc.taxicompanyid FROM drivercompanies dc WHERE dc.driverid = ?))"
    				+ "GROUP BY z.zoneid, z.taxicompanyid, z.zonename ORDER BY z.zonename");
    		getCompanyZonesStmt.setLong(1, driverId);
    		
    		//if(taxiCompanyId!=null) getCompanyZonesStmt.setLong(2, taxiCompanyId);
    		//else 
    		getCompanyZonesStmt.setString(2, null);
    		
    		getCompanyZonesStmt.setLong(3, driverId);
    		getCompanyZonesStmt.setLong(4, driverId);
    		ResultSet resultSet = getCompanyZonesStmt.executeQuery();
    		
    		List<Zone> zones =  new ArrayList<>();
    		
    		while (resultSet.next()) {
				
    			
    			Zone zone = new Zone();
    			
    			zone.setZoneId(resultSet.getLong("zoneid"));
    			zone.setTaxiCompanyId(resultSet.getLong("taxicompanyid"));
    			zone.setZoneName(resultSet.getString("zonename"));
    			zone.setZoneLat(resultSet.getDouble("ZoneLat"));
    			zone.setZoneLong(resultSet.getDouble("ZoneLong"));
    			zone.setZoneRange(resultSet.getDouble("ZoneRange"));
    			zone.setDriversInZone(resultSet.getInt("drivers_in_zone"));
    			zone.setStrictBorders(resultSet.getBoolean("StrictBorders"));
    			zone.setZonePoints(resultSet.getString("ZonePoints"));
    			
    			zones.add(zone);
			}
    		
    		resultSet.close();
    		return zones;
    		
    	} finally {
    		if(getCompanyZonesStmt!=null) getCompanyZonesStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Zone getZone(Long zoneId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getZoneInformationStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		getZoneInformationStmt = connection.prepareStatement("SELECT ZoneLat,ZoneLong,ZoneRange,KickDrivers,"
    				+ "ZonePoints,StrictBorders "
    				+ "FROM zones WHERE zoneid=?");
    		getZoneInformationStmt.setLong(1, zoneId);
    		ResultSet resultSet = getZoneInformationStmt.executeQuery();
    		
    		Zone zone = null;
    	
    		if(resultSet.next()) {
    			zone = new Zone();
    			zone.setZoneId(zoneId);
    			zone.setZoneLat(resultSet.getDouble("ZoneLat"));
    			zone.setZoneLong(resultSet.getDouble("ZoneLong"));
    			zone.setZoneRange(resultSet.getDouble("ZoneRange"));
    			zone.setKickDrivers(resultSet.getBoolean("KickDrivers"));
    			zone.setZonePoints(resultSet.getString("ZonePoints"));
    			zone.setStrictBorders(resultSet.getBoolean("StrictBorders"));
    		}
    		
    		resultSet.close();
    		return zone;
    		
    	} finally {
    		if(getZoneInformationStmt!=null) getZoneInformationStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int updateDriverWithZone(long zoneId,long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement updateDriverWithZoneStmt = null;
    	
    	try {
    	
    		LOGGER.info(" ");
    		
    		connection = DatasourceProvider.getConnection();
    		//updateDriverWithZoneStmt = connection.prepareStatement("UPDATE driver SET zoneid = ifnull(?, zoneid), zonejointime = CASE WHEN ? IS NULL THEN NULL ELSE NOW() END WHERE driverid = ?");
    		updateDriverWithZoneStmt = connection.prepareStatement("UPDATE driver SET zoneid = ?, zonejointime = NOW() WHERE driverid = ? AND Status='act'");
    		
    		updateDriverWithZoneStmt.setLong(1, zoneId);
    		updateDriverWithZoneStmt.setLong(2, driverId);
    		
    		/*
    		updateDriverWithZoneStmt.setString(1, zoneId!=null?zoneId.toString():null);
    		updateDriverWithZoneStmt.setLong(2, zoneId);
    		updateDriverWithZoneStmt.setLong(3, driverId);
    		*/
    		
    		int updateZone = updateDriverWithZoneStmt.executeUpdate();
    		
    		LOGGER.info("Passenger zone updated "+updateZone);
    		
    		return updateZone;
    		
    	} finally {
    		if(updateDriverWithZoneStmt!=null) updateDriverWithZoneStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int setTripAsDiscount(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setTripAsDiscountStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		setTripAsDiscountStmt = connection.prepareStatement("UPDATE trip SET discounted=true WHERE tripid=?");
    		setTripAsDiscountStmt.setLong(1, tripId);
    		int update = setTripAsDiscountStmt.executeUpdate();
    	
    		LOGGER.info("updated trip as discount "+update);
    		
    		return update;
    		
    	} finally {
    		if(setTripAsDiscountStmt!=null) setTripAsDiscountStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Date setDriverFirst(long zoneId,long driverId) throws SQLException {
    	
    	Connection connection = null;
    	CallableStatement setDriverFirstProc = null;
    	
    	try {
    		
    		LOGGER.info("Setting the driver first setting it");
    		
    		connection = DatasourceProvider.getConnection();
    		setDriverFirstProc = connection.prepareCall("CALL makeDriverFirstInZone (?, ?, ?)");
    		setDriverFirstProc.setLong(1, zoneId);
    		setDriverFirstProc.setLong(2, driverId);
    		setDriverFirstProc.registerOutParameter(3, Types.DATE);
    		setDriverFirstProc.execute();
    		Date dt=setDriverFirstProc.getDate("lastzone"); 
    		
    		LOGGER.info("From the last zone "+dt.toString());
    		
    		return dt;
    		
    	} finally {
    		if(setDriverFirstProc!=null) setDriverFirstProc.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int removeDriverFromZone(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement removeDriverFromZoneStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		LOGGER.info("Removing driver from zone where driverId "+driverId);
    		removeDriverFromZoneStmt = connection.prepareStatement("UPDATE driver SET zoneid = NULL, zonejointime=NULL WHERE driverid =?");
    		removeDriverFromZoneStmt.setLong(1, driverId);
    		return removeDriverFromZoneStmt.executeUpdate();
    		
    	} finally {
    		if(removeDriverFromZoneStmt!=null) removeDriverFromZoneStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<Driver> getDriversByZone(long zoneId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriversByZoneStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriversByZoneStmt = connection.prepareStatement("SELECT d.imei, dr.driverid, dr.lastname, dr.firstname FROM driver dr, device d WHERE dr.deviceid = d.deviceid AND dr.zonejointime IS NOT NULL AND dr.zoneid =? ORDER BY dr.zonejointime ASC");
    		getDriversByZoneStmt.setLong(1, zoneId);
    		ResultSet resultSet = getDriversByZoneStmt.executeQuery();
    		
    		List<Driver> drivers = new ArrayList<>();
    		
    		while (resultSet.next()) {
    			
    			Driver driver = new Driver();
    			driver.setDriverId(resultSet.getLong("driverid"));
    			driver.setFirstName(resultSet.getString("firstname"));
    			driver.setLastName(resultSet.getString("lastname"));
    			Device device = new Device();
    			device.setImei(resultSet.getString("imei"));
    			driver.setDevice(device);
    		
    			drivers.add(driver);
			}
    		
    		resultSet.close();
    		return drivers;
    		
    	} finally {
    		if(getDriversByZoneStmt!=null) getDriversByZoneStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int updateDriverZone(long zoneId,long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setDriverZoneStmt = null;
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		setDriverZoneStmt = connection.prepareStatement("UPDATE driver SET zoneid = ?, zonejointime=NOW() WHERE driverid =?");
    		setDriverZoneStmt.setLong(1, zoneId);
    		setDriverZoneStmt.setLong(2, driverId);
    		return setDriverZoneStmt.executeUpdate();  		
    		
    	} finally {
    		if(setDriverZoneStmt!=null) setDriverZoneStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Long createOrUpdateMessage(Long messageId,Long taxiCompanyId,Long driverId,String status,String text,boolean isOffline) throws SQLException {
    	
    	Connection connection = null;
    	CallableStatement createOrUpdateMessageStmt = null;
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		createOrUpdateMessageStmt = connection.prepareCall("CALL createOrUpdateMessage(?, ?, ?, ?, ?, ?, ?, ?)");
    		
    		if (messageId != null) createOrUpdateMessageStmt.setLong(1, messageId);
        	else createOrUpdateMessageStmt.setString(1, null);
        	if (taxiCompanyId != null) createOrUpdateMessageStmt.setLong(2, taxiCompanyId);
        	else createOrUpdateMessageStmt.setString(2, null);
        	if (driverId != null) createOrUpdateMessageStmt.setLong(3, driverId);
        	else createOrUpdateMessageStmt.setString(3, null);
        	
    		createOrUpdateMessageStmt.setString(4, text);
    		createOrUpdateMessageStmt.setString(5, status);
        	createOrUpdateMessageStmt.setString(6, isOffline ? "true" : "false");

        	createOrUpdateMessageStmt.registerOutParameter(7, Types.NUMERIC);
        	createOrUpdateMessageStmt.registerOutParameter(8, Types.TIMESTAMP);
        	createOrUpdateMessageStmt.execute();
        	messageId = createOrUpdateMessageStmt.getLong("messageIdOut");
        	
        	return messageId;
    		
    	} finally {
    		if(createOrUpdateMessageStmt!=null) createOrUpdateMessageStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static String getDeviceImei(long deviceId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDeviceImeiStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDeviceImeiStmt = connection.prepareStatement("SELECT imei FROM device WHERE deviceid=?");
    		getDeviceImeiStmt.setLong(1, deviceId);
    		
    		ResultSet resultSet = getDeviceImeiStmt.executeQuery();
    		
    		String imei = null;
    		
    		if(resultSet.next()) {
    			imei = resultSet.getString("imei");
    		}
    		
    		return imei;
    		
    	} finally {
    		if(getDeviceImeiStmt!=null) getDeviceImeiStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Driver getDriversDevice(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriversDeviceStmt = null;	
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriversDeviceStmt = connection.prepareStatement("SELECT d.status, dr.active, d.deviceid, d.imei, d.locale, d.extra FROM driver dr, device d WHERE dr.deviceid = d.deviceid AND dr.driverid = ?");
    		getDriversDeviceStmt.setLong(1, driverId);
    		ResultSet resultSet = getDriversDeviceStmt.executeQuery();
    		
    		Driver driver = null;
    		
    		if(resultSet.next()) {
    			
    			driver = new Driver();
    			driver.setDriverId(driverId);
    			driver.setActive(resultSet.getBoolean("active"));
    			Device device = new Device();
    			device.setStatus(resultSet.getString("status"));
    			device.setDeviceId(resultSet.getLong("deviceid"));
    			device.setImei(resultSet.getString("imei"));
    			device.setLocale(resultSet.getString("locale"));
    			device.setExtra(resultSet.getString("extra"));
    			driver.setDevice(device);
    		}
    		
    		return driver;
    		
    	} finally {
    		if(getDriversDeviceStmt!=null) getDriversDeviceStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<Device> getDriversDevicePerCompany(Long taxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriversPerCompanyStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriversPerCompanyStmt = connection.prepareStatement("SELECT d.deviceid, d.imei FROM driver dr, device d, drivercompanies dc WHERE d.deviceid = dr.deviceid AND dc.driverid = dr.driverid AND dc.taxicompanyid = ifnull(?, dc.taxicompanyid) AND dr.active = 'true' AND d.status != 'off'");
    		
    		if(taxiCompanyId!=null) getDriversPerCompanyStmt.setLong(1, taxiCompanyId);
    		else getDriversPerCompanyStmt.setString(1, null);
    		
    		ResultSet resultSet = getDriversPerCompanyStmt.executeQuery();
    		
    		List<Device> devices = new ArrayList<>();
    		
    		while(resultSet.next()) {
    		
    			Device device = new Device();
    			device.setDeviceId(resultSet.getLong("deviceid"));
    			device.setImei(resultSet.getString("imei"));
    			devices.add(device);
    		}
    		
    		resultSet.close();
    		return devices;
    		
    	} finally {
    		if(getDriversPerCompanyStmt!=null) getDriversPerCompanyStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }

    public static List<Long> getOfflineMessages(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getOfflineMessagesStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getOfflineMessagesStmt = connection.prepareStatement("SELECT messageid FROM messages WHERE recipientdriverid=? AND isOffline='true'");
    		getOfflineMessagesStmt.setLong(1, driverId);
    		ResultSet resultSet = getOfflineMessagesStmt.executeQuery();
    		
    		List<Long> offlineMessagesId = new ArrayList<>();
    		
    		while(resultSet.next()) {
    			offlineMessagesId.add(resultSet.getLong("messageid"));
    		}
    		
    		resultSet.close();
    		return offlineMessagesId;
    		
    	} finally {
    		if(getOfflineMessagesStmt!=null) getOfflineMessagesStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static void getFilteredMessagesPerCompany() throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getFilteredMessagesPerCompanyStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getFilteredMessagesPerCompanyStmt = connection.prepareStatement("SELECT m.taxicompanyid, m.messageid, m.messagebody, m.recipientdriverid, m.messagetime, m.status FROM messages m WHERE (m.taxicompanyid IN (0) OR m.messageid =?) AND ((m.messagebody like ? AND ? IS NOT NULL AND ? != '') OR ? IS NULL OR ? = '') AND ((m.recipientdriverid =? AND ? IS NOT NULL) OR (? IS NULL)) \nAND ((m.status =? AND ? IS NOT NULL) OR ? IS NULL) AND ((m.messagetime between ? AND ? AND ? IS NOT NULL AND ? IS NOT NULL AND ? != '' AND ? != '') OR ? IS NULL OR ? IS NULL OR = '' OR = '')");
    		
    	} finally {
    		if(getFilteredMessagesPerCompanyStmt!=null) getFilteredMessagesPerCompanyStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static TaxiCompany getTaxiCompany(long TaxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getTaxiCompanyStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getTaxiCompanyStmt = connection.prepareStatement("SELECT ShortName FROM taxicompany WHERE taxicompanyid=?");
    		getTaxiCompanyStmt.setLong(1, TaxiCompanyId);
    		ResultSet resultSet = getTaxiCompanyStmt.executeQuery();
    		
    		TaxiCompany taxiCompany = null;
    		
    		if(resultSet.next()) {
    			
    			taxiCompany = new TaxiCompany();
    			taxiCompany.setShortName(resultSet.getString("ShortName"));
    		}
    		
    		resultSet.close();
    		return taxiCompany;
    		
    	} finally {
    		if(getTaxiCompanyStmt!=null) getTaxiCompanyStmt.close();
    		if(connection!=null) connection.close();
    	}
    
    }
    
    /*
     * I don't want it either to be this way
     */
    public static List<NearbyCompanyXML> findNearByCompanies(Double lat,Double lng,Long passengerId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement findNearByCompaniesStmt = null;
    	
    	try {
			
    		connection = DatasourceProvider.getConnection();
    		findNearByCompaniesStmt = connection.prepareStatement("SELECT MIN(distance_between(?,?,d.currentLat,d.currentLong)) as mindist,"
    				+ "COUNT(1) as avadrivers , tc.taxicompanyid, tc.shortname, tc.officialname, "
    				+ "tc.companytype, tc.candidatenumber , tc.address, tc.telephone, tc.fax, "
    				+ "tc.email, tc.website, tc.hailrange, tc.clearbiddingtime, "
    				+ "tc.requestwaittime, tc.appointmentremind, tc.companyinfo "
    				+ "FROM taxicompany tc, drivercompanies dc, driver dr, device d "
    				+ "WHERE tc.taxicompanyid = dc.taxicompanyid AND dc.driverid = dr.driverid "
    				+ "AND dr.deviceid = d.deviceid "
    				+ "AND (?  IS NOT NULL AND (dr.driverid NOT IN (SELECT bla.driverid FROM blacklisted bla WHERE bla.passengerid=? ))) "
    				+ "AND (tc.customerSelectable='true')  "
    				+ "AND (? BETWEEN LEAST(tc.SWLat,tc.NELat) "
    				+ "AND GREATEST(tc.SWLat,tc.NELat)) "
    				+ "AND (? BETWEEN LEAST(tc.SWLng,tc.NELng) "
    				+ "AND GREATEST(tc.SWLng,tc.NELng)) "
    				+ "AND distance_between(?,?,d.currentLat,d.currentLong)<=tc.hailrange "
    				+ "AND d.status='ava' AND  dr.active='true' "
    				+ "GROUP BY tc.taxicompanyid, tc.shortname, "
    				+ "tc.officialname, tc.companytype, tc.candidatenumber, "
    				+ "tc.address, tc.telephone, tc.fax, tc.email, "
    				+ "tc.website, tc.hailrange, tc.clearbiddingtime, "
    				+ "tc.requestwaittime, tc.appointmentremind, tc.companyinfo ORDER BY mindist ASC");
    		
    		findNearByCompaniesStmt.setDouble(1, lat);
    		findNearByCompaniesStmt.setDouble(2, lng);
    		findNearByCompaniesStmt.setLong(3, passengerId);
    		findNearByCompaniesStmt.setLong(4, passengerId);
    		findNearByCompaniesStmt.setDouble(5, lat);
    		findNearByCompaniesStmt.setDouble(6, lng);
    		findNearByCompaniesStmt.setDouble(7, lat);
    		findNearByCompaniesStmt.setDouble(8, lng);
    		
    		ResultSet resultSet = findNearByCompaniesStmt.executeQuery();
    		
    		List<NearbyCompanyXML> nearbyCompanyXMLs = new ArrayList<>();
    		
    		while(resultSet.next()) {
    		
    			NearbyCompanyXML nearbyCompanyXML = new NearbyCompanyXML();
    			nearbyCompanyXML.setMinimumDistance(resultSet.getDouble("mindist"));
    			nearbyCompanyXML.setAvailableDrivers(resultSet.getInt("avadrivers"));
    			nearbyCompanyXML.setTaxiCompanyId(resultSet.getLong("taxicompanyid"));
    			nearbyCompanyXML.setShortName(resultSet.getString("shortname"));
    			nearbyCompanyXML.setOfficialName(resultSet.getString("officialname"));
    			nearbyCompanyXML.setCompanyType(resultSet.getString("companytype"));
    			nearbyCompanyXML.setCandidateNumber(resultSet.getInt("candidatenumber"));
    			nearbyCompanyXML.setAddress(resultSet.getString("address"));
    			nearbyCompanyXML.setTelephone(resultSet.getString("telephone"));
    			nearbyCompanyXML.setFax(resultSet.getString("fax"));
    			nearbyCompanyXML.setEmail(resultSet.getString("email"));
    			nearbyCompanyXML.setWebSite(resultSet.getString("website"));
    			nearbyCompanyXML.setHailRange(resultSet.getDouble("hailrange"));
    			nearbyCompanyXML.setClearBiddingTime(resultSet.getInt("clearbiddingtime"));
    			nearbyCompanyXML.setRequestWaitTime(resultSet.getInt("requestwaittime"));
    			nearbyCompanyXML.setAppointmentRemind(resultSet.getInt("appointmentremind"));
    			nearbyCompanyXML.setCompanyInfo(resultSet.getString("companyinfo"));
    			
    			nearbyCompanyXMLs.add(nearbyCompanyXML);
    		}
    		
    		resultSet.close();
    		return nearbyCompanyXMLs;
    		
		} finally {
			if(findNearByCompaniesStmt!=null)findNearByCompaniesStmt.close();
			if(connection!=null) connection.close();
		}
    	
    }
    
    public static List<NearByDriverXML> findNearByDrivers(Double lat,Double lng,Long passengerId,Long taxicompanyId,Integer taxiCompanyLimit) throws SQLException {
    
    	Connection connection = null;
    	PreparedStatement findNearByDriversStmt = null;
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		findNearByDriversStmt = connection.prepareStatement("SELECT * FROM (SELECT DISTINCT(d.deviceid),"
    				+ "d.currentLat,d.currentLong, distance_between(d.currentlat, d.currentlong, ?, ?) as distance, "
    				+ "if(fav.driverid is null,'false','true') "
    				+ "as faved,if(drl.driverid is null,'false','true') "
    				+ "as speaksenglish,if(veh.passengersno is not null and veh.passengersno>4,'true','false') "
    				+ "as bigteamveh,veh.plateno,veh.model,veh.hybrid,veh.wifi,if (veh.manufacturedate is not null "
    				+ "and (YEAR(CURDATE())-veh.manufacturedate)<3,'true','false') "
    				+ "as newvehicle,veh.supportdisabled,dr.driverid, dr.firstname,"
    				+ "dr.lastname,dr.codename,dr.smoker,dr.allowpets, "
    				+ "dr.takecreditcards,dc.taxicompanyid, "
    				+ "if(tfv.totfaved IS NULL,0,tfv.totfaved) "
    				+ "as totnofavs FROM vehicle veh, "
    				+ "device d, driver dr left outer join "
    				+ "favourites fav on dr.driverid=fav.driverid AND fav.passengerid=? "
    				+ "left outer join driverlanguages drl on dr.driverid=drl.driverid and "
    				+ "drl.languageid=2 left outer join "
    				+ "totalfavourite tfv on dr.driverid=tfv.driverid "
    				+ " AND (tfv.taxicompanyid = (select taxicompanyid from passenger where passengerid=?))"
    				+ " ,drivercompanies dc WHERE dc.driverid = dr.driverid AND veh.vehicleid=d.vehicleid "
    				+ " AND dr.deviceid = d.deviceid AND d.status = 'ava' AND dr.active = 'true' AND dr.status = 'act' "
    				+ " AND (dr.driverid NOT IN (SELECT bla.DriverID FROM blacklisted bla WHERE bla.PassengerID = ?))) "
    				+ " a   ,taxicompany tc WHERE a.taxicompanyid=? "
    				+ " AND a.taxicompanyid = tc.taxicompanyid "
    				+ " AND tc.customerSelectable='true'"
    				+ " AND distance <= tc.hailrange "
    				+ " ORDER BY a.taxicompanyid,distance ASC"
    				+ " LIMIT ?");
    		
    		findNearByDriversStmt.setDouble(1, lat);
    		findNearByDriversStmt.setDouble(2, lng);
    		findNearByDriversStmt.setLong(3, passengerId);
    		findNearByDriversStmt.setLong(4, passengerId);
    		findNearByDriversStmt.setLong(5, passengerId);
    		findNearByDriversStmt.setLong(6, taxicompanyId);
    		findNearByDriversStmt.setInt(7, taxiCompanyLimit);
    		
    		ResultSet resultSet = findNearByDriversStmt.executeQuery();
    		
    		List<NearByDriverXML> nearByDriverXMLs = new ArrayList<>();
    		
    		while(resultSet.next()) {
    			
    			NearByDriverXML nearByDriverXML = new NearByDriverXML();
    			nearByDriverXML.setDeviceId( resultSet.getLong("deviceid"));
    			nearByDriverXML.setCurrentLat(resultSet.getDouble("currentLat"));
    			nearByDriverXML.setCurrentLong( resultSet.getDouble("currentLong"));
    			nearByDriverXML.setDistance( resultSet.getDouble("distance"));
    			nearByDriverXML.setIsFaved(resultSet.getBoolean("faved"));
    			nearByDriverXML.setSpeaksEnglish(resultSet.getBoolean("speaksenglish"));
    			nearByDriverXML.setNewVehicle(resultSet.getBoolean("newvehicle"));
    			nearByDriverXML.setBigTeamVeh( resultSet.getBoolean("bigteamveh"));
    			nearByDriverXML.setPlateNumber(resultSet.getString("plateno"));
    			nearByDriverXML.setHybrid(resultSet.getBoolean("hybrid"));
    			nearByDriverXML.setHasWifi(resultSet.getBoolean("wifi"));
    			nearByDriverXML.setIsSmoker(resultSet.getBoolean("smoker"));
    			nearByDriverXML.setPetsAllowed(resultSet.getBoolean("allowpets"));
    			nearByDriverXML.setTakeCreditCards(resultSet.getBoolean("takecreditcards"));
    			nearByDriverXML.setTotalFavs(resultSet.getInt("totnofavs"));
    			nearByDriverXML.setNewVehicle( resultSet.getBoolean("newvehicle"));
    			nearByDriverXML.setSupportDisabled(resultSet.getBoolean("supportdisabled"));
    			nearByDriverXML.setDriverId(resultSet.getLong("driverid"));
    			nearByDriverXML.setName(resultSet.getString("firstname"));
    			nearByDriverXML.setSurName( resultSet.getString("lastname"));
    			nearByDriverXML.setCodeName(resultSet.getString("codename"));
    			nearByDriverXML.setCarModel(resultSet.getString("model"));
    			
    			nearByDriverXMLs.add(nearByDriverXML);
    		}
    		
    		resultSet.close();
    		return nearByDriverXMLs;
    		
    	} finally {
    		if(findNearByDriversStmt!=null) findNearByDriversStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<Map<String, Object>> findNearbyCompanyAllDevices(Double lat,Double lng,Long passengerId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement findNearbyCompanyAllDevicesStmt = null;
 
    	try {
 
    		LOGGER.info("Nearbycompanydrivers "+lat+" "+lng+" "+passengerId);
    		
    		connection = DatasourceProvider.getConnection();
    		findNearbyCompanyAllDevicesStmt = connection.prepareStatement("SELECT * FROM (SELECT DISTINCT(d.deviceid),"
    				+ "d.currentLat,d.currentLong, distance_between(d.currentlat, d.currentlong, ?, ?) as distance, "
    				+ "if(fav.driverid is null,'false','true') "
    				+ "as faved,if(drl.driverid is null,'false','true') "
    				+ "as speaksenglish,if(veh.passengersno is not null and veh.passengersno>4,'true','false') "
    				+ "as bigteamveh,veh.plateno,veh.model,veh.hybrid,veh.wifi,if (veh.manufacturedate is not null "
    				+ "and (YEAR(CURDATE())-veh.manufacturedate)<3,'true','false') "
    				+ "as newvehicle,veh.supportdisabled,dr.driverid, dr.firstname,"
    				+ "dr.lastname,dr.codename,dr.smoker,dr.allowpets, "
    				+ "dr.takecreditcards,dc.taxicompanyid, "
    				+ "if(tfv.totfaved IS NULL,0,tfv.totfaved) "
    				+ "as totnofavs FROM vehicle veh, "
    				+ "device d, driver dr left outer join "
    				+ "favourites fav on dr.driverid=fav.driverid AND fav.passengerid=? "
    				+ "left outer join driverlanguages drl on dr.driverid=drl.driverid and "
    				+ "drl.languageid=2 left outer join "
    				+ "totalfavourite tfv on dr.driverid=tfv.driverid "
    				+ " AND (tfv.taxicompanyid = (select taxicompanyid from passenger where passengerid=?))"
    				+ " ,drivercompanies dc WHERE dc.driverid = dr.driverid AND veh.vehicleid=d.vehicleid "
    				+ " AND dr.deviceid = d.deviceid AND d.status = 'ava' AND dr.active = 'true' AND dr.status = 'act' "
    				+ " AND (dr.driverid NOT IN (SELECT bla.DriverID FROM blacklisted bla WHERE bla.PassengerID = ?))) "
    				+ " a   ,taxicompany tc WHERE a.taxicompanyid = tc.taxicompanyid "
    				+ " AND tc.customerSelectable='true'"
    				+ " AND distance/1000 <= tc.hailrange "
    				+ " ORDER BY a.taxicompanyid,distance ASC");
    		
    		findNearbyCompanyAllDevicesStmt.setDouble(1, lat);
    		findNearbyCompanyAllDevicesStmt.setDouble(2, lng);
    		findNearbyCompanyAllDevicesStmt.setLong(3, passengerId);
    		findNearbyCompanyAllDevicesStmt.setLong(4, passengerId);
    		findNearbyCompanyAllDevicesStmt.setLong(5, passengerId);
    		
    		ResultSet resultSet = findNearbyCompanyAllDevicesStmt.executeQuery();
    		
    		List<Map<String, Object>> nearByDrivers = new ArrayList<>();
    		
    		while (resultSet.next()) {
				    			
    			Map<String, Object> nearByDriver = new HashMap<>();

    			nearByDriver.put("deviceid", resultSet.getLong("deviceid"));
    			nearByDriver.put("currentLat", resultSet.getDouble("currentLat"));
    			nearByDriver.put("currentLong", resultSet.getDouble("currentLong"));
    			nearByDriver.put("distance", resultSet.getDouble("distance"));
    			nearByDriver.put("faved",resultSet.getBoolean("faved"));
    			nearByDriver.put("speaksenglish", resultSet.getBoolean("speaksenglish"));
    			nearByDriver.put("newvehicle", resultSet.getBoolean("newvehicle"));
    			nearByDriver.put("bigteamveh", resultSet.getBoolean("bigteamveh"));
    			nearByDriver.put("plateno", resultSet.getString("plateno"));
    			nearByDriver.put("model", resultSet.getString("model"));
    			nearByDriver.put("hybrid",resultSet.getBoolean("hybrid"));
    			nearByDriver.put("wifi", resultSet.getBoolean("wifi"));
    			nearByDriver.put("smoker", resultSet.getBoolean("smoker"));
    			nearByDriver.put("allowpets", resultSet.getBoolean("allowpets"));
    			nearByDriver.put("takecreditcards",resultSet.getBoolean("takecreditcards"));
    			nearByDriver.put("totalfavourites", resultSet.getInt("totnofavs"));
    			nearByDriver.put("newvehicle", resultSet.getBoolean("newvehicle"));
    			nearByDriver.put("supportdisabled", resultSet.getBoolean("supportdisabled"));
    			nearByDriver.put("driverid", resultSet.getLong("driverid"));
    			nearByDriver.put("firstname", resultSet.getString("firstname"));
    			nearByDriver.put("lastname", resultSet.getString("lastname"));
    			nearByDriver.put("codename", resultSet.getString("codename"));
    			nearByDriver.put("hailrange", resultSet.getString("hailrange"));
    			nearByDriver.put("taxicompanyid",resultSet.getLong("taxicompanyid"));
    		    nearByDriver.put("shortname", resultSet.getString("shortname"));
    		    nearByDriver.put("officialname", resultSet.getString("officialname"));
    		    nearByDriver.put("companytype", resultSet.getString("companytype"));
    		    nearByDriver.put("companyinfo", resultSet.getString("CompanyInfo"));
    		    nearByDriver.put("address", resultSet.getString("address"));
    		    nearByDriver.put("telephone", resultSet.getString("Telephone"));
    		    nearByDriver.put("fax",resultSet.getString("Fax"));
    		    nearByDriver.put("email", resultSet.getString("email"));
    		    nearByDriver.put("website",resultSet.getString("website"));
    		    nearByDriver.put("hailrange",resultSet.getDouble("hailrange"));
    		    nearByDriver.put("clearbiddingtime", resultSet.getInt("clearbiddingtime"));
    		    nearByDriver.put("requestwaittime", resultSet.getInt("requestwaittime"));
    		    nearByDriver.put("appointmentremind",resultSet.getInt("appointmentremind"));
    		    nearByDriver.put("candidatenumber",resultSet.getInt("candidatenumber"));
    		    
    			nearByDrivers.add(nearByDriver);
			}
    		
    		resultSet.close();
    		return nearByDrivers;
    	    
    	} finally {
    		if(findNearbyCompanyAllDevicesStmt!=null) findNearbyCompanyAllDevicesStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Long getDriverIdFromDevice(long deviceid) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriverIdFromDeviceStmt = null;
    	
    	try {
 
    		connection = DatasourceProvider.getConnection();
    		getDriverIdFromDeviceStmt = connection.prepareStatement("SELECT driverid FROM driver WHERE deviceid=? AND active='true' LIMIT 1");
    		getDriverIdFromDeviceStmt.setLong(1, deviceid);
    		ResultSet resultSet = getDriverIdFromDeviceStmt.executeQuery();
    		
    		Long driverId = null;
    		
    		if(resultSet.next()) {
    			driverId = resultSet.getLong("driverid");
    		}
    		
    		return driverId;
    	} finally {
    		if(getDriverIdFromDeviceStmt!=null) getDriverIdFromDeviceStmt.close();
    		if(connection!=null) connection.close();
    	}
 
    }

    public static Long getTripZoneId(long tripId) throws SQLException {
		
    	Connection connection = null;
    	PreparedStatement getTripZoneIdStmt = null; 
    	
    	try {
			
    		connection = DatasourceProvider.getConnection();
    		getTripZoneIdStmt = connection.prepareStatement("SELECT trip.zoneid FROM trip "
    				+ "WHERE tripid=? AND zoneid IS NOT NULL");
    		getTripZoneIdStmt.setLong(1, tripId);
    		
    		Long zoneId = null;
    		
    		ResultSet resultSet = getTripZoneIdStmt.executeQuery();
    		
    		if(resultSet.next()) {
    			zoneId = resultSet.getLong("zoneid");
    		}
    		
    		return zoneId;
    		
		} finally {
			if(getTripZoneIdStmt!=null) getTripZoneIdStmt.close();
			if(connection!=null) connection.close();
		}
    	
	}
    
    public static void insertToBackendLogRec(Long taxiCompanyId,String action,Long tripId,String commandReceived,String from) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement insertToBackendLogStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		insertToBackendLogStmt = connection.prepareStatement("INSERT INTO backendlog (`TaxiCompanyID`,`Action`,`TripID`,`CommandReceived`,`FromUser`) VALUES (?,?,?,?,?)");
    		insertToBackendLogStmt.setString(1, taxiCompanyId!=null?taxiCompanyId.toString():null);
    		insertToBackendLogStmt.setString(2, action);
    		insertToBackendLogStmt.setLong(3, tripId);
    		insertToBackendLogStmt.setString(4, commandReceived);
    		insertToBackendLogStmt.setString(5, from);
    		insertToBackendLogStmt.executeUpdate();
    		
    	} finally {
    		if(insertToBackendLogStmt!=null) insertToBackendLogStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static void insertToBackendLogExe(Long taxiCompanyId,String action,Long tripId,String commandExe) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement insertToBackendLogStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		insertToBackendLogStmt = connection.prepareStatement("INSERT INTO backendlog (`TaxiCompanyID`,`Action`,`TripID`,`CommandExecuted`) VALUES (?,?,?,?)");
    		insertToBackendLogStmt.setString(1, taxiCompanyId!=null?taxiCompanyId.toString():null);
    		insertToBackendLogStmt.setString(2, action);
    		insertToBackendLogStmt.setLong(3, tripId);
    		insertToBackendLogStmt.setString(4, commandExe);
    		insertToBackendLogStmt.executeUpdate();
    	} finally {
    		if(insertToBackendLogStmt!=null) insertToBackendLogStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }

    public static Message getMessageInfo(long messageId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement messageInfoStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		messageInfoStmt = connection.prepareStatement("SELECT taxicompanyid,messageid,MessageBody,recipientdriverid,messagetime,status FROM messages WHERE messageid=?");
    		messageInfoStmt.setLong(1, messageId);
    		
    		ResultSet resultSet = messageInfoStmt.executeQuery();
    		
    		Message message = null;
    				
    		while(resultSet.next()) {
    			
    			message = new Message();
    			message.setMessageId(resultSet.getLong("messageid"));
    			message.setTaxiCompanyId(resultSet.getLong("taxicompanyid"));
    			message.setMessageBody(resultSet.getString("MessageBody"));
    			message.setDriverId(resultSet.getLong("recipientdriverid"));
    			message.setMessageTime(resultSet.getTimestamp("messagetime"));
    			message.setStatus(resultSet.getString("status"));
    		
    		}
    	
    		resultSet.close();	
    		return message;
    	
    	} finally {
    		if(messageInfoStmt!=null) messageInfoStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int setStatusOnTripAssign(long tripId,long deviceId,String status) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setStatusOnTripAssignStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		setStatusOnTripAssignStmt = connection.prepareStatement("UPDATE trip_assign SET status=? WHERE tripid=? AND deviceid=?");
    		setStatusOnTripAssignStmt.setString(1, status);
    		setStatusOnTripAssignStmt.setLong(2, tripId);
    		setStatusOnTripAssignStmt.setLong(3, deviceId);
    		return setStatusOnTripAssignStmt.executeUpdate();
    		
    	} finally {
    		if(setStatusOnTripAssignStmt!=null) setStatusOnTripAssignStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    private static Driver resultDriverMapper(ResultSet resultSet) throws SQLException {
    	
    	Driver driver = new Driver();
		driver.setDriverId(resultSet.getLong("driverid"));
		driver.setFirstName(resultSet.getString("firstname"));
		driver.setLastName(resultSet.getString("lastname"));
		driver.setMobile(resultSet.getString("mobile"));
		driver.setDistance(resultSet.getDouble("distance"));
		driver.setTaxiCompanyId(resultSet.getLong("taxicompanyid"));
		driver.setBrandName(resultSet.getString("brandname"));
		
		Device device = new Device();
		device.setImei(resultSet.getString("imei"));
		device.setStatus(resultSet.getString("status"));
		device.setDeviceId(resultSet.getLong("deviceid"));
		device.setCurrentLat(resultSet.getDouble("currentlat"));
		device.setCurrentLng(resultSet.getDouble("currentlong"));
		device.setLocale(resultSet.getString("locale"));
		device.setExtra(resultSet.getString("extra"));
		Vehicle vehicle = new Vehicle();
		vehicle.setPlateno(resultSet.getString("platenumber"));
		vehicle.setModel(resultSet.getString("carmodel"));
		vehicle.setVehicleId(resultSet.getLong("vehicleid"));
		
		device.setVehicle(vehicle);
		driver.setDevice(device);
    	
    	return driver;
    } 
    
    public static List<String> findNonInterestedImeisForTrip(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement findNonInterestedTripsStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		findNonInterestedTripsStmt = connection.prepareStatement("SELECT d.imei FROM "
    				+ "trip t, trip_assign ta, device d  "
    				+ "WHERE t.tripid = ta.tripid "
    				+ "AND d.deviceid = ta.deviceid "
    				+ "AND d.status = 'AVA' AND ta.driverid IS NULL  AND t.tripid=?");
    		findNonInterestedTripsStmt.setLong(1, tripId);
    		
    		ResultSet resultSet = findNonInterestedTripsStmt.executeQuery();
    		
    		List<String> imeis = new ArrayList<>();
    		
    		while(resultSet.next()) {
    			
    			String imei = resultSet.getString("imei");
    			imeis.add(imei);
    		}
    		
    		resultSet.close();
    		return imeis;
    		
    	} finally {
    		if(findNonInterestedTripsStmt!=null) findNonInterestedTripsStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    
    public static List<Driver> findPickedDriversForTrip(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement findDriversForTripStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		
    		findDriversForTripStmt = connection.prepareStatement("SELECT a.*,tc.brandname FROM( "+
    				"SELECT d.imei,d.status, d.deviceid, d.currentlat, d.currentlong, d.locale , "
    				+ "d.extra , v.plateno as platenumber, dr.driverid, dr.firstname, dr.lastname, "
    				+ "dr.mobile, v.model as carmodel, v.vehicleid, optimum_driver(ta.distance , ta.responsetime, dr.rating) AS distance, "
    				+ "t.taxicompanyid FROM trip t, trip_assign ta, driver dr, device d, vehicle v "
    				+ "WHERE t.tripid = ta.tripid AND d.deviceid = ta.deviceid AND ta.driverid = dr.driverid AND v.vehicleid = d.vehicleid AND t.tripid=? "+
    				")a, taxicompany tc WHERE tc.taxicompanyid=a.taxicompanyid AND distance/1000<tc.hailrange ORDER BY distance ASC");
    		findDriversForTripStmt.setLong(1, tripId);
    		
    		
    		
    		/*
    		findDriversForTripStmt = connection.prepareStatement("SELECT a.*,tc.brandname FROM( "
    				+ "SELECT d.imei,d.status, d.deviceid, d.currentlat, d.currentlong, d.locale , d.extra , v.plateno as platenumber, dr.driverid, dr.firstname, dr.lastname, dr.mobile, v.model as carmodel, v.vehicleid, optimum_driver(ta.distance , ta.responsetime, dr.rating) AS distance, t.taxicompanyid FROM trip t, trip_assign ta, driver dr, device d, vehicle v WHERE t.tripid = ta.tripid AND d.deviceid = ta.deviceid AND ta.driverid = dr.driverid AND v.vehicleid = d.vehicleid AND t.tripid=? "
    				+ "UNION "
    				+ "SELECT d.imei,d.status, d.deviceid, d.currentlat, d.currentlong, d.locale, d.extra , v.plateno as platenumber, null AS driverid, null AS firstname, null AS lastname, null as mobile, v.model as carmodel, v.vehicleid, 1000 AS distance, t.taxicompanyid FROM trip t, trip_assign ta, device d, vehicle v WHERE t.tripid = ta.tripid AND d.deviceid = ta.deviceid AND d.status = 'AVA' AND ta.driverid IS NULL AND v.vehicleid = d.vehicleid AND t.tripid =? "
    				+ ")a, taxicompany tc WHERE tc.taxicompanyid=a.taxicompanyid AND distance/1000<tc.hailrange ORDER BY distance ASC;");
    		findDriversForTripStmt.setLong(1, tripId);
    		findDriversForTripStmt.setLong(1, tripId);
    		*/
    		
    		ResultSet resultSet = findDriversForTripStmt.executeQuery();
    		
    		List<Driver> drivers = new ArrayList<>();
    		
    		while(resultSet.next()) {
    			
    			Driver driver = resultDriverMapper(resultSet);
    			drivers.add(driver);
    			
    		}
    		
    		resultSet.close();
    		return drivers;
    		
    	} finally {
    		if(findDriversForTripStmt!=null) findDriversForTripStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int storeCallPick(long tripId,long driverId,long deviceId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement storeCallPickStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		storeCallPickStmt  = connection.prepareStatement("UPDATE trip_assign "
    				+ "SET driverid=?,ClaimTime=?"
    				+ "WHERE tripid =? AND deviceid =? "
    				+ "AND (SELECT AssignTime FROM trip WHERE tripid=?) IS NULL");
    		storeCallPickStmt.setLong(1, driverId);
    		storeCallPickStmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
    		storeCallPickStmt.setLong(3, tripId);
    		storeCallPickStmt.setLong(4, deviceId);
    		storeCallPickStmt.setLong(5, tripId);
    		
    		return storeCallPickStmt.executeUpdate();
    		
    	} finally {
    		if(storeCallPickStmt!=null) storeCallPickStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Driver driverFromDeviceAndTrip(long deviceId,long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement findDriverFromDeviceStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		findDriverFromDeviceStmt = connection.prepareStatement("SELECT d.imei,d.status, d.deviceid, d.currentlat, d.currentlong, "
    				+ "d.locale , d.extra , v.plateno as platenumber, dr.driverid, dr.firstname, dr.lastname, "
    				+ "dr.mobile, v.model as carmodel, v.vehicleid, optimum_driver(ta.distance , ta.responsetime, dr.rating) AS distance, "
    				+ "t.taxicompanyid,tc.brandname FROM trip t, trip_assign ta, driver dr, device d, vehicle v,taxicompany tc WHERE "
    				+ "t.tripid = ta.tripid AND d.deviceid = ta.deviceid AND d.status !='UNA' "
    				+ "AND ta.driverid = dr.driverid AND v.vehicleid = d.vehicleid "
    				+ "AND tc.taxicompanyid=t.taxicompanyid AND d.deviceid=? AND ta.tripid=?");
    		findDriverFromDeviceStmt.setLong(1, deviceId);
    		findDriverFromDeviceStmt.setLong(2, tripId);
    		
    		ResultSet resultSet = findDriverFromDeviceStmt.executeQuery();
    		Driver driver = null;
    		
    		if(resultSet.next()) {
    			driver = resultDriverMapper(resultSet);
    		}
    		resultSet.close();
    		
    		return driver;
    		
    	} finally {
    		if(findDriverFromDeviceStmt!=null) findDriverFromDeviceStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<Driver> findDriversForTrip(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement findDriversForTripStmt = null;
    	PreparedStatement findDriversOccForTripStmt = null;
    	PreparedStatement findDevicesForTripStmt = null;
    	
    	try {
    	
    		
    		connection = DatasourceProvider.getConnection();
    		ResultSet resultSet = null;
    		
    		List<Driver> drivers = new ArrayList<>();
    		
    		findDriversForTripStmt = connection.prepareStatement("SELECT a.*,tc.brandname FROM(SELECT d.imei,d.status, d.deviceid, d.currentlat, d.currentlong, d.locale , d.extra , v.plateno as platenumber, dr.driverid, dr.firstname, dr.lastname, dr.mobile, v.model as carmodel, v.vehicleid, optimum_driver(ta.distance , ta.responsetime, dr.rating) AS distance, t.taxicompanyid FROM trip t, trip_assign ta, driver dr, device d, vehicle v WHERE t.tripid = ta.tripid AND d.deviceid = ta.deviceid AND d.status != 'OCC' AND d.status !='UNA' AND ta.driverid = dr.driverid AND v.vehicleid = d.vehicleid AND t.tripid = ?)a, taxicompany tc WHERE tc.taxicompanyid=a.taxicompanyid AND distance / 1000 < tc.hailrange ORDER BY distance ASC");
    		findDriversForTripStmt.setLong(1, tripId);
    		resultSet = findDriversForTripStmt.executeQuery();
    		
    		//This means no suitable drivers at all so get read of him
    		while (resultSet.next()) {
				drivers.add(resultDriverMapper(resultSet));
			}
    		
    		resultSet.close();
    		
    		if(drivers.size()==0) {
    			return drivers;
    		}
    		
    		findDriversOccForTripStmt = connection.prepareStatement("SELECT a.*,tc.brandname FROM(SELECT d.imei,d.status, d.deviceid, d.currentlat, d.currentlong, d.locale , d.extra , v.plateno as platenumber, dr.driverid, dr.firstname, dr.lastname, dr.mobile, v.model as carmodel, v.vehicleid, optimum_driver(ta.distance , ta.responsetime, dr.rating) AS distance, t.taxicompanyid FROM trip t, trip_assign ta, driver dr, device d, vehicle v WHERE t.tripid = ta.tripid AND d.deviceid = ta.deviceid AND (d.status = 'OCC' OR d.status ='UNA') AND ta.driverid = dr.driverid AND v.vehicleid = d.vehicleid AND t.tripid = ?)a, taxicompany tc WHERE tc.taxicompanyid=a.taxicompanyid AND distance / 1000 < tc.hailrange ORDER BY distance ASC");
    		findDriversOccForTripStmt.setLong(1, tripId);
    		resultSet = findDriversOccForTripStmt.executeQuery();
    		
    		while (resultSet.next()) {
				drivers.add(resultDriverMapper(resultSet));
			}
    		
    		resultSet.close();
    		
    		findDevicesForTripStmt = connection.prepareStatement("SELECT a.*,tc.brandname FROM(SELECT d.imei,d.status, d.deviceid, d.currentlat, d.currentlong, d.locale, d.extra , v.plateno as platenumber, null AS driverid, null AS firstname, null AS lastname, null as mobile, v.model as carmodel, v.vehicleid, 1000 AS distance, t.taxicompanyid FROM trip t, trip_assign ta, device d, vehicle v WHERE t.tripid = ta.tripid AND d.deviceid = ta.deviceid AND d.status = 'AVA' AND ta.driverid IS NULL AND v.vehicleid = d.vehicleid AND t.tripid = ?)a, taxicompany tc WHERE tc.taxicompanyid=a.taxicompanyid AND distance / 1000 < tc.hailrange ORDER BY distance ASC");
    		findDevicesForTripStmt.setLong(1, tripId);
    		resultSet = findDevicesForTripStmt.executeQuery();
    		
    		while (resultSet.next()) {
				drivers.add(resultDriverMapper(resultSet));
			}
    		
    		resultSet.close();
    		return drivers;
    		
    	} finally {
    		if(findDriversForTripStmt!=null) findDriversForTripStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    /*
     * Get the imei for the drivers not interested
     */
    public static List<String> getDriversNotInterestedImei(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement driversNotInterestedStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		driversNotInterestedStmt = connection.prepareStatement("SELECT device.imei FROM trip_assign "
    				+ "INNER JOIN device ON device.deviceid=trip_assign.deviceid "
    				+ "WHERE tripid=? AND driverid IS NULL");
    		driversNotInterestedStmt.setLong(1, tripId);
    		ResultSet resultSet = driversNotInterestedStmt.executeQuery();
    		
    		List<String> imeis = new ArrayList<>();
    		
    		while(resultSet.next()) {
    			imeis.add(resultSet.getString("imei"));
    		}
    		
    		resultSet.close();
    		return imeis;
    		
    	} finally {
    		if(driversNotInterestedStmt!=null) driversNotInterestedStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    
    /*
     * Gets the imei from the drivers that lost the trip
     */
    public static List<String> getPickLostDriversImei(long tripId,long assignedDevice) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getPickLostDriversImeiStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getPickLostDriversImeiStmt = connection.prepareStatement("SELECT device.imei FROM trip_assign "
    				+ "INNER JOIN device ON device.deviceid=trip_assign.deviceid "
    				+ "WHERE tripid=? AND driverid IS NOT NULL AND trip_assign.deviceid!=? ");
    		getPickLostDriversImeiStmt.setLong(1, tripId);
    		getPickLostDriversImeiStmt.setLong(2, assignedDevice);
    		
    		List<String> imeis = new ArrayList<>();
    		
    		ResultSet imeisResultSet = getPickLostDriversImeiStmt.executeQuery();
    		
    		while(imeisResultSet.next()) {
    			imeis.add(imeisResultSet.getString("imei"));
    		}
    		
    		imeisResultSet.close();
    		return imeis;
    		
    	} finally {
    		if(getPickLostDriversImeiStmt!=null) getPickLostDriversImeiStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static boolean setPassengerImeiAndPin(long passengerId,String imei,String pin) throws SQLException {
    	
    	LOGGER.info("Setting the passenger imei");
    	
    	Connection connection = null;
    	PreparedStatement updatePassengerImeiStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		updatePassengerImeiStmt = connection.prepareStatement("UPDATE passenger SET imei=?,pin=? WHERE passengerid=?");
    		updatePassengerImeiStmt.setString(1, imei);
    		updatePassengerImeiStmt.setString(2, pin);
    		updatePassengerImeiStmt.setLong(3, passengerId);
    		return updatePassengerImeiStmt.execute();
    	
    	} finally {
    		if(updatePassengerImeiStmt!=null) updatePassengerImeiStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Long getTaxiCompanyCallTypeId(long taxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCallTypeIdStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getCallTypeIdStmt = connection.prepareStatement("SELECT calltype.CalltypeID "
    				+ "FROM calltype,taxicompany WHERE taxicompany.DefaultCallTypeID=calltype.CalltypeID "
    				+ "AND taxicompany.taxicompanyid=?");
    		getCallTypeIdStmt.setLong(1, taxiCompanyId);
    		
    		ResultSet resultSet = getCallTypeIdStmt.executeQuery();
    		
    		Long callTypeId = null;
    		
    		if(resultSet.next()) {
    			callTypeId = resultSet.getLong("CalltypeID");
    		}
    		
    		resultSet.close();
    		return callTypeId;
    		
    	} finally {
    		if(getCallTypeIdStmt!=null) getCallTypeIdStmt.close();
    		if(connection!=null) connection.close();
    	}
    
    }
    
    public static List<Long> getDriversFromZoneForCall(int zoneId,Long type) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriversFromZoneForCallStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriversFromZoneForCallStmt = connection.prepareStatement("SELECT driver.driverid,driver.zonejointime "
    				+ "FROM driver,taxicompany,zones WHERE zonejointime IS NOT NULL "
    				+ "AND taxicompany.taxicompanyid=zones.taxicompanyid AND zones.zoneid=driver.zoneid "
    				+ "AND driver.status!='blo' AND driver.zoneid=? "
    				+ "AND ( CASE WHEN (? IS NULL) then TRUE else (driver.typeid=?) end) "
    				+ "ORDER BY zonejointime ASC");
    		getDriversFromZoneForCallStmt.setLong(1, zoneId);
    		getDriversFromZoneForCallStmt.setString(2, type!=null?type.toString():null);
    		getDriversFromZoneForCallStmt.setString(3, type!=null?type.toString():null);
    		
    		ResultSet resultSet = getDriversFromZoneForCallStmt.executeQuery();
    		
    		List<Long> driverIds = new ArrayList<>();
    		
    		while(resultSet.next()) {
    			
    			Long driverId = resultSet.getLong("driverid");
    			driverIds.add(driverId);
    		}
    		
    		resultSet.close();
    		
    		return driverIds;
    		
    	} finally {
    		if(getDriversFromZoneForCallStmt!=null) getDriversFromZoneForCallStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static CallType getTaxiCompanyCallType(long taxiCompanyId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCallTypeStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		getCallTypeStmt = connection.prepareStatement("SELECT calltype.CalltypeID,calltype.TaxiCompanyID,calltype.Name,calltype.Distance,"
    				+ "calltype.LastCall,calltype.PassengerFrequency,calltype.ZoneOrder,calltype.CandidateNumber,calltype.HailRange,"
    				+ "calltype.ClearBiddingTime,calltype.RequestWaitTime,calltype.TimetoPickUp,calltype.AlternateCallTypeId "
    				+ "FROM calltype,taxicompany WHERE taxicompany.DefaultCallTypeID=calltype.CalltypeID AND taxicompany.taxicompanyid=?");
    		getCallTypeStmt.setLong(1,taxiCompanyId);    		
    		
    		ResultSet resultSet = getCallTypeStmt.executeQuery();
    		CallType callType = null;
    		
    		if(resultSet.next()) {
    			callType = new CallType();
    			callType.setCallTypeId(resultSet.getLong("CalltypeID"));
                callType.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
                callType.setName(resultSet.getString("Name"));
                callType.setDistance(resultSet.getDouble("Distance"));
                callType.setLastCall(resultSet.getDouble("LastCall"));
                callType.setPassengerFrequency(resultSet.getDouble("PassengerFrequency"));
                callType.setCandidateNumber(resultSet.getInt("CandidateNumber"));
                callType.setHailRange(resultSet.getDouble("HailRange"));
                callType.setClearBiddingTime(resultSet.getInt("ClearBiddingTime"));
                callType.setRequestWaitTime(resultSet.getInt("RequestWaitTime"));
                callType.setTimeToPickUp(resultSet.getInt("TimetoPickUp"));
                callType.setAlternateCallTypeId(resultSet.getLong("AlternateCallTypeId"));
                callType.setZoneOrder(resultSet.getDouble("ZoneOrder"));
    		}
    		
    		resultSet.close();
    		return callType;
    	} finally {
    		if(getCallTypeStmt!=null) getCallTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static CallType getCallType(long callTypeId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCallTypeStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		getCallTypeStmt = connection.prepareStatement("SELECT CalltypeID,TaxiCompanyID,Name,Distance,"
    				+ "LastCall,PassengerFrequency,ZoneOrder,CandidateNumber,HailRange,ClearBiddingTime,RequestWaitTime,"
    				+ "TimetoPickUp,AlternateCallTypeId,ForceAssignment,SamePassengerDays,ExpirationsAllowed FROM calltype WHERE CalltypeID=?");
    		getCallTypeStmt.setLong(1,callTypeId);    		
    		
    		ResultSet resultSet = getCallTypeStmt.executeQuery();
    		CallType callType = null;
    		
    		if(resultSet.next()) {
    			callType = new CallType();
    			callType.setCallTypeId(resultSet.getLong("CalltypeID"));
                callType.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
                callType.setName(resultSet.getString("Name"));
                callType.setDistance(resultSet.getDouble("Distance"));
                callType.setLastCall(resultSet.getDouble("LastCall"));
                callType.setPassengerFrequency(resultSet.getDouble("PassengerFrequency"));
                callType.setCandidateNumber(resultSet.getInt("CandidateNumber"));
                callType.setHailRange(resultSet.getDouble("HailRange"));
                callType.setClearBiddingTime(resultSet.getInt("ClearBiddingTime"));
                callType.setRequestWaitTime(resultSet.getInt("RequestWaitTime"));
                callType.setTimeToPickUp(resultSet.getInt("TimetoPickUp"));
                callType.setAlternateCallTypeId(resultSet.getLong("AlternateCallTypeId"));
                callType.setForceAssignment(resultSet.getBoolean("ForceAssignment"));
                callType.setZoneOrder(resultSet.getDouble("ZoneOrder"));
                callType.setSamePassengerDays(resultSet.getInt("SamePassengerDays"));
    		
                Integer expirations = resultSet.getInt("ExpirationsAllowed");
                if(!resultSet.wasNull()) {
                	callType.setExpirationsAllowed(expirations);
                }
                
    		}
    		
    		resultSet.close();
    		return callType;
    	} finally {
    		if(getCallTypeStmt!=null) getCallTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static CallType getTaxiCompanyDefaultCallType(long taxiCompanyId) throws SQLException {
    
    	Connection connection = null;
        PreparedStatement getCallTypeStmt = null;
    
        try {
            connection = DatasourceProvider.getConnection();
            getCallTypeStmt = connection.prepareStatement("SELECT ca.CalltypeID,ca.TaxiCompanyID,ca.Name,ca.Distance,ca.LastCall,"
                            + "ca.PassengerFrequency,ca.CandidateNumber,ca.HailRange,"
                            + "ca.ClearBiddingTime,ca.RequestWaitTime,ca.TimetoPickUp "
                            + "FROM calltype as ca,taxicompany as ta WHERE ta.taxicompanyid=? AND ca.CallTypeId=ta.DefaultCallTypeid");
            getCallTypeStmt.setLong(1, taxiCompanyId);
        
            ResultSet resultSet = getCallTypeStmt.executeQuery();
            CallType callType = null;
            
            if(resultSet.next()) {
            	callType = new CallType();
            	callType.setCallTypeId(resultSet.getLong("CalltypeID"));
                callType.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
                callType.setName(resultSet.getString("Name"));
                callType.setDistance(resultSet.getDouble("Distance"));
                callType.setLastCall(resultSet.getDouble("LastCall"));
                callType.setPassengerFrequency(resultSet.getDouble("PassengerFrequency"));
                callType.setCandidateNumber(resultSet.getInt("CandidateNumber"));
                callType.setHailRange(resultSet.getDouble("HailRange"));
                callType.setClearBiddingTime(resultSet.getInt("ClearBiddingTime"));
                callType.setRequestWaitTime(resultSet.getInt("RequestWaitTime"));
                callType.setTimeToPickUp(resultSet.getInt("TimetoPickUp"));
            }
            
            resultSet.close();
            return callType;
        } finally {
        	if(getCallTypeStmt!=null) getCallTypeStmt.close();
            if(connection!=null) connection.close();
        }
    }
    
    /**
     * Make a transactional Forced calltype assignment
     * @param tripId
     * @param callType
     * @return
     * @throws SQLException
     */
    public static Long transactionalForceCallTypeAssignment(long tripId,CallType callType) throws SQLException {
    	
    	LOGGER.info("Forcing transactional call type assignment");
    	
    	DataHelper.setCandidateDrivers(tripId,callType);
    	
    	Long assignedDevice = transactionalCallTypeAssignment(tripId, callType);
    		
    	
    	return assignedDevice;
    }
    
    /**
     * Making a transactional Assignment using callTypes
     * @param tripId
     * @return
     * @throws SQLException
     */
    public static Long transactionalCallTypeAssignment(long tripId,CallType callType) throws SQLException {
    	
    	LOGGER.info("Transctional assignment with call types "+tripId);
    	
    	Long zoneId = DataHelper.getTripZoneId(tripId);
    	
    	Passenger passenger = DataHelper.passengerFromTrip(tripId);
    	
        TripData tripData = DataHelper.getTripData(tripId);
        
        
    	Connection connection = null;
    	PreparedStatement fetchFromTripStmt = null;
    	PreparedStatement fetchFromTripAssignStmt = null; 
    	PreparedStatement fetchOrderedDrivers = null;
    	CallableStatement assignTripToDevice = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		connection.setAutoCommit(false);
    		fetchFromTripStmt = connection.prepareStatement("SELECT*FROM trip WHERE tripid=? FOR UPDATE");
    		fetchFromTripStmt.setLong(1, tripId);
    		ResultSet fetchFromTripResultSet = fetchFromTripStmt.executeQuery();
    		
    		LOGGER.info("LOCKING trip for update in a succesfful way "+tripId);
    		
    		/*
    		 * No update on the trip assign
    		 */
    		fetchFromTripAssignStmt = connection.prepareStatement("SELECT deviceid,driverid,claimtime,distance "
    				+ "FROM trip_assign WHERE tripid=? ORDER BY DISTANCE ASC FOR UPDATE");
    		fetchFromTripAssignStmt.setLong(1, tripId);
    		ResultSet fetchFromTripAssignResultSet = fetchFromTripAssignStmt.executeQuery();
    		
    		/*
    		 * No other updates for the devices
    		 */
    		Long assignedDevice = null;
    		Long assignedDriver = null;
    		
    		fetchOrderedDrivers = connection.prepareStatement("SELECT trip_assign.deviceid,trip_assign.driverid,trip_assign.claimtime,trip_assign.distance,device.Status "
    				+ "FROM trip_assign INNER JOIN device ON device.deviceid=trip_assign.deviceid WHERE tripid=? "
    				+ "AND trip_assign.driverid IS NOT NULL ORDER BY DISTANCE DESC FOR UPDATE");
    		fetchOrderedDrivers.setLong(1, tripId);
    		
    		
    		Long maxPassengerPick = null;
            Long maxCallTypePick = null;
            Long maxZoneJoin = null;
    		BigDecimal maxDistance = null;
        
    		LinkedHashMap<Long, Timestamp> passengerPickUptimes = DataHelper.getPassengerPickFromTripAssign(passenger.getPassengerId(),tripId);
            LinkedHashMap<Long, Timestamp> callTypePickUpTimes = DataHelper.getCallTypePickFromTripAssign(callType.getCallTypeId(), tripId);
            LinkedHashMap<Long, Timestamp> zoneJoinTimes = DataHelper.getZoneJoinTimeFromTripAssign(zoneId, tripId);
            
            if(!passengerPickUptimes.isEmpty()) {
            	if(passengerPickUptimes.get(passengerPickUptimes.keySet().toArray()[0])!=null) {
            		LOGGER.info("The trip data hailtime "+tripData.getHailTime()+" passenger pick up "+passengerPickUptimes.get(passengerPickUptimes.keySet().toArray()[0]).getTime());
            		maxPassengerPick = tripData.getHailTime()-passengerPickUptimes.get(passengerPickUptimes.keySet().toArray()[0]).getTime();
            		if(maxPassengerPick<0) {
            			LOGGER.info("Max passengerpick below zero "+maxPassengerPick);
            			maxPassengerPick = 0L;
                    }
            		LOGGER.info("Get tha passenger max pick "+maxPassengerPick);
                }
            }
            
            if(maxPassengerPick==null) {
                maxPassengerPick = tripData.getHailTime()-passenger.getRegDate().getTime();
                LOGGER.info("Get tha passenger max pick because null "+maxPassengerPick+" "+tripData.getHailTime()+" "+passenger.getRegDate().getTime());
            }

            LOGGER.info("The max passenger pick is "+maxPassengerPick);

            if(!callTypePickUpTimes.isEmpty()) {
                maxCallTypePick = tripData.getHailTime()-callTypePickUpTimes.get(callTypePickUpTimes.keySet().toArray()[0]).getTime();
                if(maxCallTypePick<0) {
                	LOGGER.info("Max callTypePick below zero "+maxCallTypePick);
                	maxCallTypePick = 0L;
                }
            }

            LOGGER.info("The max call type pick is "+maxCallTypePick);
            
            if(!zoneJoinTimes.isEmpty()) {
            	if(zoneJoinTimes.get(zoneJoinTimes.keySet().toArray()[0])!=null) {
            		maxZoneJoin = tripData.getHailTime()-zoneJoinTimes.get(zoneJoinTimes.keySet().toArray()[0]).getTime();
            		
            		if(maxZoneJoin<0) {
            			maxZoneJoin = 0L;
            		}
            		
            	}
            }
            
            Long selectedDriver = null;
            Long selectedDevice = null;
            
            BigDecimal currentValue = null;
            //TripAssign selectedTripAssign = null;

            LOGGER.info("Checking the trip assign and the trips");
            
            
            ResultSet fetchSuitableResultSet = fetchOrderedDrivers.executeQuery();
			
    		while (fetchSuitableResultSet.next()) {
			
    			long deviceId = fetchSuitableResultSet.getLong("deviceid");
    			long driverId = fetchSuitableResultSet.getLong("driverid");
    			String status = fetchSuitableResultSet.getString("Status");
    			
    			
    			BigDecimal distance = fetchSuitableResultSet.getBigDecimal("distance");
    			
    			if(maxDistance==null) {
    				maxDistance = distance;
    			}
    			
    			LOGGER.info("Getting the max distance "+maxDistance);
    				
    			LOGGER.info("the distance "+distance.toString()+" the max distance "+maxDistance);
    			LOGGER.info("The divide distance "+distance.divide(maxDistance, MathContext.DECIMAL128));
                
    			BigDecimal adjustedDistance = (new BigDecimal("1.0")).add((distance.divide(maxDistance,MathContext.DECIMAL128).negate()));

    			LOGGER.info("The adjusted "+adjustedDistance.toString());
                
    			BigDecimal adjustedLastCall = new BigDecimal("0.0");
                
    			if(maxCallTypePick!=0) {
    				adjustedLastCall = (new BigDecimal(String.valueOf(tripData.getHailTime()-callTypePickUpTimes.get(driverId).getTime()))).divide(new BigDecimal(String.valueOf(maxCallTypePick)),MathContext.DECIMAL128);
    			}
    				
    			LOGGER.info("The adjusted call "+adjustedLastCall.toString()+" diver call "+callTypePickUpTimes.get(driverId).getTime()+" "+
    			" the  max call "+maxCallTypePick);

    			if(passengerPickUptimes.get(driverId)==null) {
    				passengerPickUptimes.put(driverId, passenger.getRegDate());
    			}

    			LOGGER.info("The adjusted freequency "+tripData.getHailTime()+" "+passengerPickUptimes.get(driverId).getTime()+" "+maxPassengerPick);

    			BigDecimal adjustedFrequency = new BigDecimal("0.0");

    			if(maxPassengerPick!=0) {
    				adjustedFrequency = (new BigDecimal(String.valueOf(tripData.getHailTime()-passengerPickUptimes.get(driverId).getTime()))).divide(new BigDecimal(String.valueOf(maxPassengerPick)),MathContext.DECIMAL128);
    			}
                
    			BigDecimal adjustedZoneJoin = new BigDecimal("0.0");
    			
    			if(maxZoneJoin!=0) {
    				adjustedZoneJoin = (new BigDecimal(String.valueOf(tripData.getHailTime()-zoneJoinTimes.get(driverId).getTime()))).
                            divide(new BigDecimal(String.valueOf(maxZoneJoin)),MathContext.DECIMAL128);
    			}
    			
    			
    			if(callType!=null) {
    				LOGGER.info("CallType not null "+callType.getDistance()+" "+callType.getCallTypeId());
    			} else {
    				LOGGER.info("Calltype null "+callType);
    			}
                
    			BigDecimal assignmentValue = adjustedDistance.multiply(new BigDecimal(String.valueOf(callType.getDistance())));
    			assignmentValue = assignmentValue.add(adjustedLastCall.multiply(new BigDecimal(String.valueOf(callType.getLastCall()))));
    			assignmentValue = assignmentValue.add(adjustedFrequency.multiply(new BigDecimal(String.valueOf(callType.getPassengerFrequency()))));
                assignmentValue = assignmentValue.add(adjustedZoneJoin.multiply(new BigDecimal(String.valueOf(callType.getZoneOrder()))));
    			
    			LOGGER.info("The current value is "+currentValue+" assignement values is "+assignmentValue);
                
    			if((currentValue==null||assignmentValue.compareTo(currentValue)==1)&&(!(status.equals("occ")||status.equals("una")))) {
    				currentValue = assignmentValue;
    				assignedDevice = deviceId;
    				assignedDriver = driverId;
    			} else {
    				LOGGER.info("Could not set the trip assign ");
    			}
                
                
    			LOGGER.info("The data for the driver "+driverId+" on "+tripId+" "+
    				" distance "+distance+" adjdist "+adjustedDistance+
    				" lastcall "+callTypePickUpTimes.get(driverId).toString()+" adjlastcall "+adjustedLastCall+
    				" passengerpick "+passengerPickUptimes.get(driverId)+" adjpass "+adjustedFrequency+
    				" zonejointime "+zoneJoinTimes.get(zoneJoinTimes.get(driverId))+" assignment Value "+assignmentValue);
                
    			LOGGER.info("The calltype is "+callType.getCallTypeId()+" "+callType.getDistance()+" "+callType.getLastCall()+" "+callType.getPassengerFrequency());
    			//DataHelper.insertAssignmentWeights(tripId,tripAssign.getDriverId(),adjustedDistance.doubleValue(), adjustedLastCall.doubleValue(), adjustedFrequency.doubleValue(),adjustedZoneJoin.doubleValue(), assignmentValue.doubleValue());
                
    			PreparedStatement insertAssignmentWeightStmt = connection.prepareStatement("UPDATE trip_assign "
    					+ "SET AdjustedDistance=?,AdjustedLastCall=?,AdjustedFrequency=?,"
    					+ "AssignmentValue=?,Status=?, AdjustedZoneJoin=? "
    					+ "WHERE TripID=? AND DriverID=?");
    			
    			insertAssignmentWeightStmt.setBigDecimal(1, adjustedDistance);
    			insertAssignmentWeightStmt.setBigDecimal(2, adjustedLastCall);
    			insertAssignmentWeightStmt.setBigDecimal(3, adjustedFrequency);
    			insertAssignmentWeightStmt.setBigDecimal(4, assignmentValue);
    			insertAssignmentWeightStmt.setString(5, status);
    			insertAssignmentWeightStmt.setBigDecimal(6, adjustedZoneJoin);
    			insertAssignmentWeightStmt.setLong(7, tripId);
    			insertAssignmentWeightStmt.setLong(8, driverId);
    			insertAssignmentWeightStmt.executeUpdate();
    			insertAssignmentWeightStmt.close();
                
    		}
    			
    		if(assignedDevice!=null) {
    			assignTripToDevice = connection.prepareCall("CALL assignTripToDevice(?, ?, ?, ?)");
    			assignTripToDevice.setLong(1, assignedDevice);
				assignTripToDevice.setLong(2, assignedDriver);
				assignTripToDevice.setString(3, null);
				assignTripToDevice.setLong(4, tripId);
				assignTripToDevice.execute();
    		}
    		
    		connection.commit();
    		
    		if(assignedDevice!=null) return assignedDevice;
    		
    	} catch(SQLException e) {
    		LOGGER.error("Got and exception ",e);
    		if(connection!=null) {
    			try {
    				LOGGER.error("Rolling back transaction");
    				connection.rollback();
    			} catch(SQLException ex) {
    				LOGGER.error("Prolem rolling back transaction",e);
    			}
    		} 
    		
    		return null;
    		
    	} finally {
    		
    		if(fetchFromTripStmt!=null) fetchFromTripStmt.close();
    		if(fetchFromTripAssignStmt!=null) fetchFromTripAssignStmt.close();
    		if(fetchOrderedDrivers!=null) fetchOrderedDrivers.close();
    		if(assignTripToDevice!=null) assignTripToDevice.close();
    		
    		if(connection!=null) {
    			connection.setAutoCommit(true);
    			connection.close();
    		}
    	}
    	
    	return null;
    }
    
    /*
     * Making a transactional assignment for the query
     */
    public static Long transactionalAssignment(long tripId) throws SQLException {
    	
    	LOGGER.info("Inserting transactional assignment with table locking");
    	
    	Connection connection = null;
    	PreparedStatement fetchFromTripStmt = null;
    	PreparedStatement fetchFromTripAssignStmt = null; 
    	PreparedStatement fetchOrderedDrivers = null;
    	CallableStatement assignTripToDevice = null;
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		connection.setAutoCommit(false);
    		
    		/*
    		 * Lock the trip row
    		 */
    		fetchFromTripStmt = connection.prepareStatement("SELECT*FROM trip WHERE tripid=? FOR UPDATE");
    		fetchFromTripStmt.setLong(1, tripId);
    		ResultSet fetchFromTripResultSet = fetchFromTripStmt.executeQuery();
    		
    		LOGGER.info("LOCKING trip for update in a succesfful way "+tripId);
    		
    		/*
    		 * No update on the trip assign
    		 */
    		fetchFromTripAssignStmt = connection.prepareStatement("SELECT deviceid,driverid,claimtime,distance "
    				+ "FROM trip_assign WHERE tripid=? ORDER BY DISTANCE ASC FOR UPDATE");
    		fetchFromTripAssignStmt.setLong(1, tripId);
    		ResultSet fetchFromTripAssignResultSet = fetchFromTripAssignStmt.executeQuery();
    		
    		/*
    		 * No other updates for the devices
    		 */
    		Long assignedDevice = null;
    	
    		fetchOrderedDrivers = connection.prepareStatement("SELECT trip_assign.deviceid,trip_assign.driverid,trip_assign.claimtime,trip_assign.distance,device.Status "
    				+ "FROM trip_assign INNER JOIN device ON device.deviceid=trip_assign.deviceid WHERE tripid=? "
    				+ "AND trip_assign.driverid IS NOT NULL ORDER BY DISTANCE ASC FOR UPDATE");
    		fetchOrderedDrivers.setLong(1, tripId);
    		//LOCK IN SHARE MODE
    		
    		LOGGER.info("Fetched successfully all the drivers");
    		
    		ResultSet fetchSuitableResultSet = fetchOrderedDrivers.executeQuery();
    				
    		while (fetchSuitableResultSet.next()) {
				
    			long deviceId = fetchSuitableResultSet.getLong("deviceid");
    			long driverId = fetchSuitableResultSet.getLong("driverid");
    			String status = fetchSuitableResultSet.getString("Status");
    			
    			//DataHelper.setStatusOnTripAssign(tripId, deviceId, status);
    			
    			LOGGER.info("From drivers get next "+deviceId+" "+driverId+" "+status);
    			
    			PreparedStatement setStatusOnTripAssignStmt = connection.prepareStatement("UPDATE trip_assign SET status=? WHERE tripid=? AND deviceid=?");
        		setStatusOnTripAssignStmt.setString(1, status);
        		setStatusOnTripAssignStmt.setLong(2, tripId);
        		setStatusOnTripAssignStmt.setLong(3, deviceId);
        		setStatusOnTripAssignStmt.executeUpdate();
        		setStatusOnTripAssignStmt.close();
    			
        		if(assignedDevice==null) {
        			LOGGER.info("The device is null");
        			if(!(status.equals("occ")||status.equals("una"))) {
        				assignedDevice = deviceId;
        				assignTripToDevice = connection.prepareCall("CALL assignTripToDevice(?, ?, ?, ?)");
        				assignTripToDevice.setLong(1, deviceId);
        				assignTripToDevice.setLong(2, driverId);
        				assignTripToDevice.setString(3, null);
        				assignTripToDevice.setLong(4, tripId);
        				assignTripToDevice.execute();
        			} else {
        				LOGGER.info("Somethign wrong with the status");
        			}
        		}
			}
    		
    		LOGGER.info("The drivers are going tobe send "+assignedDevice);
    		
    		fetchSuitableResultSet.close();
    		fetchFromTripAssignResultSet.close();
    		fetchFromTripResultSet.close();
    		
    		return assignedDevice;
    		
    	} catch(SQLException e) {
    		LOGGER.error("Got and exception ",e);
    		if(connection!=null) {
    			try {
    				LOGGER.error("Rolling back transaction");
    				connection.rollback();
    			} catch(SQLException ex) {
    				LOGGER.error("Prolem rolling back transaction",e);
    			}
    		} 
    		
    		return null;
    		
    	} finally {
    		
    		if(fetchFromTripStmt!=null) fetchFromTripStmt.close();
    		if(fetchFromTripAssignStmt!=null) fetchFromTripAssignStmt.close();
    		if(fetchOrderedDrivers!=null) fetchOrderedDrivers.close();
    		if(assignTripToDevice!=null) assignTripToDevice.close();
    		
    		if(connection!=null) {
    			connection.setAutoCommit(true);
    			connection.close();
    		}
    	}
    	
    }
    
    public static Passenger passengerFromTrip(long tripId) throws SQLException {
    	
    	Connection connection = null;
        PreparedStatement passengerFromTripStmt = null;

        try {

        	connection = DatasourceProvider.getConnection();
        	passengerFromTripStmt = connection.prepareStatement("SELECT PassengerID,RegDate FROM passenger WHERE passengerid=(SELECT passengerID FROM trip WHERE tripid=?)");
        	passengerFromTripStmt.setLong(1, tripId);
        	ResultSet resultSet = passengerFromTripStmt.executeQuery();

        	Passenger passenger = null;

        	if(resultSet.next()) {

        		passenger = new Passenger();
        		passenger.setPassengerId(resultSet.getLong("PassengerID"));
        		passenger.setRegDate(resultSet.getTimestamp("RegDate"));
        	}

        	resultSet.close();
        	return passenger;

        } finally {
                if(passengerFromTripStmt!=null) passengerFromTripStmt.close();
                if(connection!=null) connection.close();
        }

    }
    
    public static LinkedHashMap<Long,Timestamp> getCallTypePickFromTripAssign(long callTypeId,long tripId) throws SQLException {
    	
    	Connection connection = null;
        PreparedStatement callTypePickFromTripAssignStmt = null;

        try {

        	connection = DatasourceProvider.getConnection();

        	//TODO
        	/**
        	 * The calltype will be done were the pickup time is valid 
        	 * and the drivers has not cancelled
        	 */
        	callTypePickFromTripAssignStmt = connection.prepareStatement("SELECT driverid,"
        			+ "COALESCE("
        			+ "(SELECT HailTime FROM trip "
        			+ "WHERE trip.driverid=trip_assign.driverid "
        			+ "AND HailTime IS NOT NULL "
        			+ "AND HailTime!='' AND CallTypeID=? "
        			+ "AND Status='com' "
        			+ "ORDER BY HailTime DESC LIMIT 1), "
        			+ "(SELECT OriginalJoinDate FROM "
        			+ "driver WHERE driver.driverid=trip_assign.driverid "
        			+ ")) as HailTime "
        			+ "FROM trip_assign WHERE tripid=? AND driverid IS NOT NULL ORDER BY HailTime ASC");

        	callTypePickFromTripAssignStmt.setLong(1, callTypeId);
        	callTypePickFromTripAssignStmt.setLong(2, tripId);
        	ResultSet resultSet = callTypePickFromTripAssignStmt.executeQuery();

        	LinkedHashMap<Long, Timestamp> callTypePicks = new LinkedHashMap<>();

        	while(resultSet.next()) {
        		callTypePicks.put(resultSet.getLong("driverid"),resultSet.getTimestamp("HailTime"));
        	}

        	resultSet.close();
        	return callTypePicks;

        } finally {
                if(callTypePickFromTripAssignStmt!=null) callTypePickFromTripAssignStmt.close();
                if(connection!=null) connection.close();
        }

    }
    
    public static LinkedHashMap<Long, Timestamp> getPassengerPickFromTripAssign(long passengerId,long tripId) throws SQLException {
 	   
        Connection connection = null;
        PreparedStatement passengerPickFromTripAssignStmt = null;

        try {

                connection = DatasourceProvider.getConnection();
                passengerPickFromTripAssignStmt = connection.prepareStatement("SELECT driverid, "
                                + "(SELECT HailTime FROM trip "
                                + "WHERE trip.driverid=trip_assign.driverid "
                                + "AND HailTime IS NOT NULL AND Passengerid=? "
                                + "AND Status='com' "
                                + "ORDER BY HailTime DESC LIMIT 1) as HailTime FROM "
                                + "trip_assign WHERE tripid=? "
                                + "AND trip_assign.driverid IS NOT NULL ORDER BY HailTime ASC");
                passengerPickFromTripAssignStmt.setLong(1, passengerId);
                passengerPickFromTripAssignStmt.setLong(2, tripId);
                ResultSet resultSet = passengerPickFromTripAssignStmt.executeQuery();
                
                LinkedHashMap<Long, Timestamp> passengerPicks = new LinkedHashMap<>();
                while(resultSet.next()) {

                    passengerPicks.put(resultSet.getLong("driverid"),resultSet.getTimestamp("HailTime"));
            }

            resultSet.close();
            return passengerPicks;

        } finally {
        	if(passengerPickFromTripAssignStmt!=null) passengerPickFromTripAssignStmt.close();
            if(connection!=null) connection.close();
        }
    }
    
    public static LinkedHashMap<Long, Timestamp> getZoneJoinTimeFromTripAssign(Long zoneId,long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement zoneJoinTimeFromTripAssignStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		zoneJoinTimeFromTripAssignStmt = connection.prepareStatement("SELECT driverid,COALESCE("
                                + "(SELECT driver.zoneJoinTime FROM driver WHERE driver.driverid=trip_assign.driverid "
                                + "AND driver.zoneId=? AND driver.zoneJoinTime IS NOT NULL),"
                                + "now()) as zoneJoinTime FROM trip_assign WHERE tripid=? "
                                + "AND trip_assign.driverid IS NOT NULL ORDER BY zoneJoinTime ASC");
    		
    		
    		zoneJoinTimeFromTripAssignStmt.setString(1, zoneId!=null?zoneId.toString():null);
    		zoneJoinTimeFromTripAssignStmt.setLong(2, tripId);
    		
    		ResultSet resultSet = zoneJoinTimeFromTripAssignStmt.executeQuery();
            LinkedHashMap<Long, Timestamp> zoneJoinTimes = new LinkedHashMap<>();
            
            while(resultSet.next()) {
                zoneJoinTimes.put(resultSet.getLong("driverid"),resultSet.getTimestamp("zoneJoinTime"));

                LOGGER.info("The driverid is "+resultSet.getLong("driverid")+" The zone join time is "+resultSet.getTimestamp("zoneJoinTime"));
            }
    		
            resultSet.close();
            return zoneJoinTimes;
            
    	} finally {
    		if(zoneJoinTimeFromTripAssignStmt!=null) zoneJoinTimeFromTripAssignStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static CallType getTripCallType(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getTripCallTypeStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getTripCallTypeStmt = connection.prepareStatement("SELECT calltype.CalltypeID,calltype.TaxiCompanyID,calltype.Name,calltype.Distance,"
    				+ "calltype.LastCall,calltype.PassengerFrequency,calltype.ZoneOrder,calltype.CandidateNumber,calltype.HailRange,"
    				+ "calltype.ClearBiddingTime,calltype.RequestWaitTime,calltype.TimetoPickUp,calltype.AlternateCallTypeId,"
    				+ "calltype.ForceAssignment "
    				+ "FROM trip,calltype WHERE trip.CallTypeID=calltype.CallTypeID AND trip.tripid=?");
    		getTripCallTypeStmt.setLong(1, tripId);
    		
    		ResultSet resultSet = getTripCallTypeStmt.executeQuery();
    		CallType callType = null;
    		
    		if(resultSet.next()) {
    			callType = new CallType();
    			callType.setCallTypeId(resultSet.getLong("CalltypeID"));
                callType.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
                callType.setName(resultSet.getString("Name"));
                callType.setForceAssignment(resultSet.getBoolean("ForceAssignment"));
                callType.setDistance(resultSet.getDouble("Distance"));
                callType.setLastCall(resultSet.getDouble("LastCall"));
                callType.setPassengerFrequency(resultSet.getDouble("PassengerFrequency"));
                callType.setCandidateNumber(resultSet.getInt("CandidateNumber"));
                callType.setHailRange(resultSet.getDouble("HailRange"));
                callType.setClearBiddingTime(resultSet.getInt("ClearBiddingTime"));
                callType.setRequestWaitTime(resultSet.getInt("RequestWaitTime"));
                callType.setTimeToPickUp(resultSet.getInt("TimetoPickUp"));
                callType.setAlternateCallTypeId(resultSet.getLong("AlternateCallTypeId"));
                callType.setZoneOrder(resultSet.getDouble("ZoneOrder"));
    		}
    		
    		resultSet.close();
    		return callType;
    		
    	} finally {
    		if(getTripCallTypeStmt!=null) getTripCallTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int setTripCallType(long tripId,long callTypeId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setTripCallTypeStmt = null;
    	
    	try {
    	
    		connection = DatasourceProvider.getConnection();
    		setTripCallTypeStmt = connection.prepareStatement("UPDATE trip SET CallTypeId=? WHERE tripid=?");
    		setTripCallTypeStmt.setLong(1, callTypeId);
    		setTripCallTypeStmt.setLong(2, tripId);
    		int result = setTripCallTypeStmt.executeUpdate();
    		return result;
    		
    	} finally {
    		if(setTripCallTypeStmt!=null) setTripCallTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int setAppointmentCallType(long appointmentId,long callTypeId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setAppointmentCallTypeStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		setAppointmentCallTypeStmt = connection.prepareStatement("UPDATE appointments SET CallTypeId=? WHERE appointmentid=?");
    		setAppointmentCallTypeStmt.setLong(1, callTypeId);
    		setAppointmentCallTypeStmt.setLong(2, appointmentId);
    		int result = setAppointmentCallTypeStmt.executeUpdate();
    		return result;
    	
    	} finally {
    		if(setAppointmentCallTypeStmt!=null) setAppointmentCallTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static TripData getTripData(long tripId) throws SQLException {

    	Connection connection = null;
        PreparedStatement getTripDataStmt = null;

        try {

                LOGGER.info("Getting the trip data for "+tripId);

                connection = DatasourceProvider.getConnection();
                getTripDataStmt = connection.prepareStatement("SELECT  "
                                + "t.poked,t.hailtime, t.pickuplat, "
                                + "t.pickuplong, t.contactphone ,"
                                + "t.pickupaddress, t.specialneeds, "
                                + "t.destination, t.deviceid, "
                                + "t.driverid, t.status, "
                                + "t.taxicompanyid, t.fare, "
                                + "t.paymenttype, t.bookagent, "
                                + "p.gender, p.name, p.mobile, "
                                + "p.passengerid, p.extra, "
                                + "p.origincountry, p.softwareversion, "
                                + "p.locale, p.discountrides, tc.shortname as companyname "
                                + ", tc.merchantaccount ,tc.brandname "
                                + "FROM trip t LEFT OUTER JOIN taxicompany tc ON t.taxicompanyid = tc.taxicompanyid AND "
                                + "tc.taxicompanyid != 1, passenger p WHERE t.passengerid = p.passengerid AND t.tripid = ?");
                getTripDataStmt.setLong(1, tripId);
                ResultSet resultSet = getTripDataStmt.executeQuery();

                TripData tripData = null;

                if(resultSet.next()) {

                    tripData = new TripData();

                    tripData.setHailTime(resultSet.getTimestamp("hailtime").getTime());
                    tripData.setLat(resultSet.getDouble("pickuplat"));
                    tripData.setLng(resultSet.getDouble("pickuplong"));
                    tripData.setGaddress(resultSet.getString("pickupaddress"));
                    tripData.setSpecial(resultSet.getString("specialneeds"));
                    tripData.setStatus(resultSet.getString("status"));
                    tripData.setDestination(resultSet.getString("destination"));
                    tripData.setGender(resultSet.getString("gender"));
                    tripData.setPassengerName(resultSet.getString("name"));
                    tripData.setMobile(resultSet.getString("mobile"));
                    tripData.setPassengerId(resultSet.getLong("passengerid"));
                    /* TODO AFTER THE CAMPAIGN ENDS, MODIFY THIS CODE TO ALWAYS SET isDiscounted TO FALSE */
                    tripData.setIsDiscounted(resultSet.getLong("discountrides") > 0);
                    tripData.setLocale(resultSet.getString("locale"));
                    tripData.setExtra(resultSet.getString("extra"));
                    tripData.setVersion(resultSet.getString("softwareversion"));
                    tripData.setTaxicompanyId(resultSet.getLong("taxicompanyid"));
                    tripData.setCompanyName(resultSet.getString("companyname"));
                    tripData.setFare(resultSet.getDouble("fare"));
                    tripData.setPaymentType(resultSet.getString("paymenttype"));
                    tripData.setBookagent(resultSet.getString("bookagent"));
                    tripData.setPassengerCountry(resultSet.getString("origincountry"));
                    tripData.setContactPhone(resultSet.getString("contactPhone"));
                }

                resultSet.close();
                return tripData;

        } finally {
                if(getTripDataStmt!=null) getTripDataStmt.close();
                if(connection!=null) connection.close();
        }
    }
    
    public static int setTripDriverType(long tripId,long driverTypeId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setTripDriverTypeStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		setTripDriverTypeStmt = connection.prepareStatement("UPDATE trip SET trip.DriverTypeId=? WHERE tripid=?");
    		setTripDriverTypeStmt.setLong(1, driverTypeId);
    		setTripDriverTypeStmt.setLong(2, tripId);
    		
    		return setTripDriverTypeStmt.executeUpdate();
    		
    	} finally {
    		if(setTripDriverTypeStmt!=null) setTripDriverTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int setAppointmentDriverType(long appointmentId,long driverTypeId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setAppointmentDriverTypeStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		setAppointmentDriverTypeStmt = connection.prepareStatement("UPDATE appointments SET appointments.DriverTypeId=? WHERE AppointmentID=?");
    		setAppointmentDriverTypeStmt.setLong(1, driverTypeId);
    		setAppointmentDriverTypeStmt.setLong(2, appointmentId);
    		
    		return setAppointmentDriverTypeStmt.executeUpdate();
    		
    	} finally {
    		if(setAppointmentDriverTypeStmt!=null) setAppointmentDriverTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    private static void setCandidateDrivers(Long tripId,CallType callType) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCurrentTripStmt = null;
    	PreparedStatement getCandidateDriversStmt = null;
    	PreparedStatement setCandidateDriversStmt = null;
    	
    	try {
 
    		connection = DatasourceProvider.getConnection();
    		connection.setAutoCommit(false);
    		
    		getCurrentTripStmt = connection.prepareStatement("SELECT HailTime,PickUpLat,PickUpLong FROM trip WHERE tripid=?");
    		getCurrentTripStmt.setLong(1, tripId);
    		
    		ResultSet tripResultSet = getCurrentTripStmt.executeQuery();
    		
    		Timestamp hailTime = null;
    		Double pickUpLat = null;
    		Double pickUpLong = null;
    		
    		if(tripResultSet.next()) {
    			hailTime = tripResultSet.getTimestamp("HailTime");
    			pickUpLat = tripResultSet.getDouble("PickUpLat");
    			pickUpLong = tripResultSet.getDouble("PickUpLat");
    		}
    		
    		getCurrentTripStmt.close();
    		
    		getCandidateDriversStmt = connection.prepareStatement("SELECT deviceid,driverid FROM ("
    				+ "SELECT DISTINCT(d.deviceid), "
    				+ "distance_between(d.currentlat, d.currentlong, t.pickuplat, t.pickuplong) as distance, "
    				+ "dc.taxicompanyid,dr.driverid,d.currentlat,d.currentlong  FROM device d, driver dr, drivercompanies dc, trip t, passenger p "
    				+ "WHERE t.tripid=? AND p.passengerid = t.passengerid AND dc.driverid = dr.driverid AND (case when (t.DriverTypeId  IS NULL ) then TRUE else (dr.TypeId = t.DriverTypeId) end)"
    				+ "AND dc.taxicompanyid = t.taxicompanyid AND dr.deviceid = d.deviceid "
    				+ "AND d.status = 'ava' AND dr.active = 'true' AND dr.status = 'act' "
    				+ "AND dr.OnRide = 'false' "
    				+ "AND (case when(t.driverid IS NULL) then TRUE else (t.driverid=dr.driverid) end) "
    				+ "AND (case when ( "
        			+ " (SELECT calltype.ForceAssignment FROM calltype WHERE calltype.calltypeid=t.calltypeid ) IS FALSE ) "
        			+ "then TRUE else "
        			+ " (case when(t.zoneId IS NOT NULL) then (dr.zoneid IS NOT NULL AND dr.zoneJoinTime IS NOT NULL ) else (dr.zoneid IS NULL AND dr.zoneJoinTime IS NULL ) end)"
        			+ " end) "
    				+ "AND (dr.driverid NOT IN (SELECT bla.DriverID FROM blacklisted bla WHERE bla.PassengerID = t.passengerid ))) a, taxicompany tc "
    				+ "WHERE a.taxicompanyid = tc.taxicompanyid AND distance <= tc.hailrange ORDER BY distance ASC LIMIT ?");
    		getCandidateDriversStmt.setLong(1, tripId);
    		getCandidateDriversStmt.setInt(2, callType.getCandidateNumber());
    		
    		//(dr.zoneid IS NULL AND dr.zoneJoinTime IS NULL )
    		
    		ResultSet resultSet = getCandidateDriversStmt.executeQuery();
    		
    		//String deviceIds = "";
    		
    		while(resultSet.next()) {
    			//deviceIds += ","+resultSet.getLong("deviceid");
    		
    			setCandidateDriversStmt = connection.prepareStatement(""
        				+ "INSERT INTO trip_assign ("
        				+ "tripid, driverid,deviceid, responsetime, lat, lng, distance"
        				+ ") "
        				+ "(SELECT trip.tripid,?, device.deviceid, "
        				+ "time_to_sec(subtime(time(trip.hailtime), time(current_timestamp()))), "
        				+ "device.currentlat, device.currentlong, "
        				+ "distance_between ("
        				+ "trip.pickuplat, "
        				+ "trip.pickuplong, "
        				+ "currentlat, currentlong) "
        				+ "FROM device,trip WHERE device.deviceid=? AND trip.tripid=?) "
        				+ "ON DUPLICATE KEY UPDATE driverid = null");
        		setCandidateDriversStmt.setLong(1, resultSet.getLong("driverid"));
        		setCandidateDriversStmt.setLong(2, resultSet.getLong("deviceid"));
        		setCandidateDriversStmt.setLong(3, tripId);
        		setCandidateDriversStmt.executeUpdate();
        		setCandidateDriversStmt.close();
    		}
    	
    		resultSet.close();
    		
    		//deviceIds = deviceIds.replaceFirst(",", "");
    		
    		
    		connection.commit();
    		
    	} catch(SQLException e) {
    		LOGGER.error("Got and exception ",e);
    		if(connection!=null) {
    			try {
    				LOGGER.error("Rolling back transaction");
    				connection.rollback();
    			} catch(SQLException ex) {
    				LOGGER.error("Prolem rolling back transaction",e);
    			}
    		} 
    	} finally {
    		if(getCurrentTripStmt!=null) getCurrentTripStmt.close();
    		if(getCandidateDriversStmt!=null) getCandidateDriversStmt.close();
    		//if(setCandidateDriversStmt!=null) setCandidateDriversStmt.close();
    		if(connection!=null) {
    			connection.setAutoCommit(true);
    			connection.close();
    		}
    	}
    	
    }
    
    public static Long getDriverZoneId(Long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriverZoneIdStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
            getDriverZoneIdStmt = connection.prepareStatement("SELECT zoneid FROM driver "
                    + "WHERE driverid=? AND zonejointime IS NOT NULL");
            getDriverZoneIdStmt.setLong(1, driverId);
            
            ResultSet resultSet = getDriverZoneIdStmt.executeQuery();

                Long zoneId = null;

                if(resultSet.next()) {

                        zoneId = resultSet.getLong("zoneid");
                }

                resultSet.close();
                return zoneId;

        } finally {
                if(getDriverZoneIdStmt!=null) getDriverZoneIdStmt.close();
                if(connection!=null) connection.close();
        }

    }
 
    /**
     * Gives the taxicompanies default call type if not exist
     * @param appointmentId
     * @return
     * @throws SQLException
     */
    public static Long getAppointCallType(long appointmentId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getAppointCallTypeStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getAppointCallTypeStmt = connection.prepareStatement("SELECT COALESCE(calltypeid,"
    				+ "(SELECT taxicompany.DefaultCalltypeID FROM taxicompany "
    				+ "WHERE taxicompany.taxicompanyid=appointments.taxicompanyid)"
    				+ ") as calltypeid FROM appointments WHERE appointmentid=?");
    		getAppointCallTypeStmt.setLong(1, appointmentId);
    		
    		ResultSet resultSet = getAppointCallTypeStmt.executeQuery();
    		
    		Long appointCallType = null;
    		
    		if(resultSet.next()) {
    			appointCallType = resultSet.getLong("calltypeid");
    		}
    		
    		resultSet.close();
    		
    		return appointCallType;
    		
    	} finally {
    		if(getAppointCallTypeStmt!=null) getAppointCallTypeStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    /**
     * Setting the time to pick up
     * @param tripId
     * @param timeToPickUp
     * @throws SQLException
     */
    public static int setTimeToPickUp(Long tripId,Integer timeToPickUp) throws SQLException {

        Connection connection = null;
        PreparedStatement setTimeToPickUpStmt = null;

        try {
                connection = DatasourceProvider.getConnection();
                setTimeToPickUpStmt = connection.prepareStatement("UPDATE trip SET TimeToPickup=? WHERE tripID=?");
                setTimeToPickUpStmt.setInt(1, timeToPickUp);
                setTimeToPickUpStmt.setLong(2, tripId);
                return setTimeToPickUpStmt.executeUpdate();

        } finally {
                if(setTimeToPickUpStmt!=null) setTimeToPickUpStmt.close();
                if(connection!=null) connection.close();
        }

    }
    
    public static List<Long> getDriversOnRide() throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriversWithStatusStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriversWithStatusStmt = connection.prepareStatement("SELECT DriverId FROM driver WHERE OnRide='true'");
    		ResultSet resultSet = getDriversWithStatusStmt.executeQuery();
    		
    		List<Long> driverIds = new ArrayList<>();
    		while(resultSet.next()) {
    			Long driverId = resultSet.getLong("DriverId");
    			driverIds.add(driverId);
    		}
    		
    		resultSet.close();
    		return driverIds;
    		
    	} finally {
    		if(getDriversWithStatusStmt!=null) getDriversWithStatusStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    
    public static List<Long> getDriversWithStatus(Driver.Status status) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriversWithStatusStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriversWithStatusStmt = connection.prepareStatement("SELECT DriverId FROM driver WHERE Status=?");
    		getDriversWithStatusStmt.setString(1, status.toString());
    		ResultSet resultSet = getDriversWithStatusStmt.executeQuery();
    		
    		List<Long> driverIds = new ArrayList<>();
    		while(resultSet.next()) {
    			Long driverId = resultSet.getLong("DriverId");
    			driverIds.add(driverId);
    		}
    		
    		resultSet.close();
    		return driverIds;
    		
    	} finally {
    		if(getDriversWithStatusStmt!=null) getDriversWithStatusStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Integer getTripPickedRideDelay(long tripId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getTripPickedRideDelayStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		getTripPickedRideDelayStmt = connection.prepareStatement("SELECT calltype.PickedRideDelay "
    				+ "FROM trip,calltype "
    				+ "WHERE trip.tripId=? "
    				+ "AND calltype.calltypeid=trip.calltypeid");
    		getTripPickedRideDelayStmt.setLong(1, tripId);
    		ResultSet resultSet = getTripPickedRideDelayStmt.executeQuery();
    		Integer pickedRideDelay = 0;
    		
    		if(resultSet.next()) {
    			pickedRideDelay = resultSet.getInt("PickedRideDelay");
    		}
    		
    		resultSet.close();
    		return pickedRideDelay;
    		
    	} finally {
    		if(getTripPickedRideDelayStmt!=null) getTripPickedRideDelayStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    
    public static OnRideStatusInfo getOnRideStatusInfo(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getOnRideStatusInfoStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		
    		getOnRideStatusInfoStmt = connection.prepareStatement("SELECT "
    				+ "trip.PickupTime,calltype.PickedRideDelay FROM trip,calltype "
    				+ "WHERE DriverID=? "
    				+ "AND calltype.calltypeid=trip.calltypeid "
    				+ "ORDER BY tripid DESC LIMIT 1");
    		getOnRideStatusInfoStmt.setLong(1, driverId);
    		
    		ResultSet resultSet = getOnRideStatusInfoStmt.executeQuery();
    		
    		OnRideStatusInfo onRideStatusInfo = null;
    		
    		if(resultSet.next()) {
    		
    			onRideStatusInfo = new OnRideStatusInfo();
        		onRideStatusInfo.setPickedRideDelay(resultSet.getInt("PickedRideDelay"));
    			onRideStatusInfo.setPickUpTime(resultSet.getTimestamp("PickupTime"));
    		}
    		
    		resultSet.close();
    		
    		return onRideStatusInfo;
    		
    	} finally {
    		if(getOnRideStatusInfoStmt!=null) getOnRideStatusInfoStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Timestamp getDriversLastTripPickupTime(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getDriversLastTripStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getDriversLastTripStmt = connection.prepareStatement("SELECT "
    				+ "PickupTime FROM trip "
    				+ "WHERE DriverID=? ORDER BY tripid DESC LIMIT 1");
    		getDriversLastTripStmt.setLong(1, driverId);
    		
    		ResultSet resultSet = getDriversLastTripStmt.executeQuery();
    		
    		Timestamp pickupTime = null;
    		
    		while(resultSet.next()) {
    		
    			pickupTime = resultSet.getTimestamp("PickupTime");
    		}
    		
    		resultSet.close();
    		
    		return pickupTime;
    		
    	} finally {
    		if(getDriversLastTripStmt!=null) getDriversLastTripStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static List<Long> getPassengerDriversLastDays(long passengerId,int days) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getPassengerDriversLastDaysStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getPassengerDriversLastDaysStmt = connection.prepareStatement("SELECT DISTINCT(driverid),HailTime FROM trip "
    				+ "WHERE passengerid=? "
    				+ "AND HailTime>DATE_sub(now(),INTERVAL ? DAY) "
    				+ "AND Status='com' "
    				+ "ORDER BY tripid DESC");
    		getPassengerDriversLastDaysStmt.setLong(1, passengerId);
    		getPassengerDriversLastDaysStmt.setInt(2, days);
    		
    		ResultSet resultSet = getPassengerDriversLastDaysStmt.executeQuery();
    		
    		List<Long> driverIds = new ArrayList<>();
    		
    		while (resultSet.next()) {
    			Long driverId = resultSet.getLong("driverid");
    			Date date = resultSet.getDate("HailTime");
    			driverIds.add(driverId);
			
    			LOGGER.info("The driver id "+driverId+" "+date.toString());
    		}
    		
    		resultSet.close();
    		return driverIds;
    		
    	} finally {
    		if(getPassengerDriversLastDaysStmt!=null) getPassengerDriversLastDaysStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int setDriverStatus(long driverId,Driver.Status status) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement setDriverStatusStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		setDriverStatusStmt = connection.prepareStatement("UPDATE driver SET Status=? WHERE DriverId=?");
    		setDriverStatusStmt.setString(1, status.toString());
    		setDriverStatusStmt.setLong(2, driverId);
    		int updateCode = setDriverStatusStmt.executeUpdate();
    		return updateCode;
    		
    	} finally {
    		if(setDriverStatusStmt!=null) setDriverStatusStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static int setDriverStatusOnRide(long driverId) throws SQLException {
    
    	Connection connection = null;
    	PreparedStatement setDriverStatusStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		setDriverStatusStmt = connection.prepareStatement("UPDATE driver SET OnRide='true' WHERE DriverId=?");
    		setDriverStatusStmt.setLong(1, driverId);
    		int updateCode = setDriverStatusStmt.executeUpdate();
    		return updateCode;
    		
    	} finally {
    		if(setDriverStatusStmt!=null) setDriverStatusStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Trip getTrip(long tripId) throws SQLException {

        Connection connection = null;
        PreparedStatement getTripStmt = null;

        try {

        	connection = DatasourceProvider.getConnection();
        	getTripStmt = connection.prepareStatement("SELECT tripid,HailTime,PassengerId,driverid,"
        			+ "PickUpAddress,AssignTime,PickupTime,ArrivalTime,ZoneId,CalltypeID,Destination,"
        			+ "TaxiCompanyID,CancelTime,CreditCost,TimeToPickup FROM trip WHERE tripid=?");
        	
        	getTripStmt.setLong(1, tripId);
        	ResultSet resultSet = getTripStmt.executeQuery();

        	Trip trip = null;

        	if(resultSet.next()) {
        		trip = new Trip();
        		trip.setTripId(resultSet.getLong("tripid"));
        		trip.setArrivalTime(resultSet.getTimestamp("ArrivalTime"));
        		trip.setHailTime(resultSet.getTimestamp("HailTime"));
        		trip.setAssignTime(resultSet.getTimestamp("AssignTime"));
        		trip.setCallTypeId(resultSet.getLong("PassengerId"));
        		trip.setDriverId(resultSet.getLong("driverid"));
        		trip.setZoneId(resultSet.getLong("ZoneId"));
        		trip.setCallTypeId(resultSet.getLong("CalltypeID"));
        		trip.setPickUpAddress(resultSet.getString("PickUpAddress"));
        		trip.setDestination(resultSet.getString("Destination"));
        		trip.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
        		trip.setCancelTime(resultSet.getTimestamp("CancelTime"));
        		trip.setPickUpTime(resultSet.getTimestamp("PickupTime"));
        		trip.setCreditCost(resultSet.getBigDecimal("CreditCost"));
        		trip.setTimeToPickUp(resultSet.getInt("TimeToPickup"));
        	}
        
        	resultSet.close();
        	return trip;
       
        } finally {
        	if(getTripStmt!=null) getTripStmt.close();
        	if(connection!=null) connection.close();
        }
    }
    
    public static int unsetDriverStatusOnRide(long driverId) throws SQLException {
        
    	Connection connection = null;
    	PreparedStatement setDriverStatusStmt = null;
    	
    	try {	
    		connection = DatasourceProvider.getConnection();
    		setDriverStatusStmt = connection.prepareStatement("UPDATE driver SET OnRide='false' WHERE DriverId=?");
    		setDriverStatusStmt.setLong(1, driverId);
    		int updateCode = setDriverStatusStmt.executeUpdate();
    		return updateCode;
    	} finally {
    		if(setDriverStatusStmt!=null) setDriverStatusStmt.close();
    		if(connection!=null) connection.close();
    	}
    }
    
    public static int getTodaysDriversExpirations(long callTypeId,long driverId) throws SQLException {
		 
    	Connection connection = null;
    	PreparedStatement getTodaysDriversExpirationsStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getTodaysDriversExpirationsStmt = connection.prepareStatement("SELECT COUNT(*) as expirations FROM trip "
    				+ "WHERE calltypeid=? AND driverid=? AND (Status='exp' OR Status='rem' )"
    				+ "and date(HailTime)=date(now());");
    		getTodaysDriversExpirationsStmt.setLong(1, callTypeId);
    		getTodaysDriversExpirationsStmt.setLong(2, driverId);
    		
    		ResultSet resultSet = getTodaysDriversExpirationsStmt.executeQuery();
    		
    		int expirations = 0;
    		
    		if(resultSet.next()) {
    			expirations = resultSet.getInt("expirations");
    		}
    		
    		resultSet.close();
    		
    		return expirations;
    		
    	} finally {
    		if(getTodaysDriversExpirationsStmt!=null) getTodaysDriversExpirationsStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
public static Penalty getCallTypeExpirationPenalty(long callTypeId) throws SQLException {
	    
    	Connection connection = null;
    	PreparedStatement getCallTypeExpirationPenaltyStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getCallTypeExpirationPenaltyStmt = connection.prepareStatement("SELECT penalty.PenaltyID,penalty.TaxiCompanyID,"
    				+ "penalty.Name,penalty.Minutes,penalty.PerMinutes,penalty.OffSet FROM calltypepenalties,penalty "
    				+ "WHERE calltypepenalties.PenaltyID=penalty.PenaltyID AND calltypepenalties.event='expire'"
    				+ "AND calltypepenalties.CallTypeID=? LIMIT 1;");
    		getCallTypeExpirationPenaltyStmt.setLong(1, callTypeId);
    		
    		ResultSet resultSet = getCallTypeExpirationPenaltyStmt.executeQuery();
    		
    		Penalty penalty = null;
	
    		if(resultSet.next()) {
    			
    			penalty = new Penalty();
    			penalty.setPenaltyId(resultSet.getLong("PenaltyID"));
    			penalty.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
    			penalty.setName(resultSet.getString("Name"));
    			penalty.setMinutes(resultSet.getInt("Minutes"));
    			penalty.setPerMinutes(resultSet.getInt("PerMinutes"));
    			penalty.setOffSet(resultSet.getInt("OffSet"));
    		}
    		
    		resultSet.close();
    		
    		return penalty;
    		
    	} finally {
    		if(getCallTypeExpirationPenaltyStmt!=null) getCallTypeExpirationPenaltyStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }

	public static void setDriverPenalty(long driverId,Long penaltyId,Long tripId,Long userId,Timestamp startTime,Timestamp endTime,String comment) throws SQLException {
	   
		Connection connection = null;
		PreparedStatement setDriverPenaltyStmt = null;
		PreparedStatement blockDriverStmt = null;
	
		try {
		
			connection = DatasourceProvider.getConnection();
			connection.setAutoCommit(false);
			setDriverPenaltyStmt = connection.prepareStatement("INSERT INTO driverpenalty (DriverID,PenaltyID,TripID,UserID,Status,StartTime,EndTime,Comment)"
					+ " VALUES (?,?,?,?,?,?,?,?)");
			setDriverPenaltyStmt.setLong(1, driverId);
			setDriverPenaltyStmt.setString(2, penaltyId!=null?penaltyId.toString():null);
			setDriverPenaltyStmt.setString(3, tripId!=null?tripId.toString():null);
			setDriverPenaltyStmt.setString(4, userId!=null?userId.toString():null);
			setDriverPenaltyStmt.setString(5, "active");
			setDriverPenaltyStmt.setTimestamp(6, startTime);
			setDriverPenaltyStmt.setTimestamp(7, endTime);
			setDriverPenaltyStmt.setString(8, comment);
			setDriverPenaltyStmt.executeUpdate();
		
			blockDriverStmt = connection.prepareStatement("UPDATE driver SET Status='blo' WHERE DriverId=?");
			blockDriverStmt.setLong(1, driverId);
			blockDriverStmt.executeUpdate();
		
			connection.commit();
		
		} catch(SQLException e) {
			LOGGER.error("Got and exception ",e);
			if(connection!=null) {
				try {
					LOGGER.error("Rolling back transaction");
					connection.rollback();
				} catch(SQLException ex) {
					LOGGER.error("Prolem rolling back transaction",e);
				}
			}
		
		} finally {
			if(setDriverPenaltyStmt!=null) setDriverPenaltyStmt.close();
			if(blockDriverStmt!=null) blockDriverStmt.close();
			if(connection!=null) {
				connection.setAutoCommit(true);
				connection.close();
			}
		}
	}
    
	public static List<Long> getDriversInPenalties() throws SQLException {
		
    	Connection connection = null;
    	PreparedStatement getDriversInPenaltiesStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		getDriversInPenaltiesStmt = connection.prepareStatement("SELECT DISTINCT DriverId FROM driverpenalty "
    				+ "WHERE EndTime<now() AND Status='active'");
    		
    		
    		List<Long> driverIds = new ArrayList<Long>();
    		
    		ResultSet resultSet = getDriversInPenaltiesStmt.executeQuery();
    		
    		while (resultSet.next()) {
			
    			Long driverId = resultSet.getLong("DriverId");
    			driverIds.add(driverId);
			}
    		
    		resultSet.close();
    		return driverIds;
    		
    	} finally {
    		if(getDriversInPenaltiesStmt!=null) getDriversInPenaltiesStmt.close();
    		if(connection!=null) connection.close();
    	}
    		
	}

	public static void fixDriverPenalty(Long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement validActivePenalties = null;
    	PreparedStatement passedActivePenalties = null;
    	PreparedStatement setPenaltiesInactive = null;
    	PreparedStatement setDriverStatusStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		connection.setAutoCommit(false);
    		
    		validActivePenalties = connection.prepareStatement("SELECT COUNT(*) as activePenalties FROM driverpenalty "
    				+ "WHERE EndTime>now() AND Status='active' AND driverid=?");
    		validActivePenalties.setLong(1, driverId);
    		
    		ResultSet activeSet = validActivePenalties.executeQuery();
    		
    		int activePenalties = 0;
    		
    		if(activeSet.next()) {
    			activePenalties = activeSet.getInt("activePenalties");
    		}
    		activeSet.close();
    		
    		passedActivePenalties = connection.prepareStatement("SELECT COUNT(*) as passedPenalties FROM driverpenalty "
    				+ "WHERE EndTime<now() AND Status='active' AND driverid=?");
    		passedActivePenalties.setLong(1, driverId);
    		
    		ResultSet passedSet = passedActivePenalties.executeQuery();
    		
    		int passedPenalties = 0;
    		
    		if(passedSet.next()) {
    			passedPenalties = passedSet.getInt("passedPenalties");
    		}
    		passedSet.close();
    		
    		if(passedPenalties>0) {
    			
    			setPenaltiesInactive = connection.prepareStatement("UPDATE driverpenalty "
    					+ "SET Status='inactive' WHERE EndTime<now() AND driverid=?");
    			setPenaltiesInactive.setLong(1, driverId);
    			setPenaltiesInactive.executeUpdate();
    			
    			if(activePenalties==0) {
    				setDriverStatusStmt = connection.prepareStatement("UPDATE driver SET Status='act' WHERE driverid=?");
    				setDriverStatusStmt.setLong(1, driverId);
    				setDriverStatusStmt.executeUpdate();
    			}
    			
    		}
    		
    		connection.commit();
    		
    	} catch(SQLException e) {
    		if(connection!=null) {
    			LOGGER.info("Rolling back connection ",e);
    			connection.rollback();
    		}	
    	} finally {
    		if(validActivePenalties!=null) validActivePenalties.close();
    		if(passedActivePenalties!=null) passedActivePenalties.close();
    		if(setPenaltiesInactive!=null) setPenaltiesInactive.close();
    		if(setDriverStatusStmt!=null) setDriverStatusStmt.close();
    		if(connection!=null) {
    			connection.setAutoCommit(true);
    			connection.close();
    		}
    	}
    	
    }	

	
    /**
     * 
     * @param callTypeId
     * @param offset
     * @return
     * @throws SQLException
     */
    public static Penalty getCallTypeCancelPenalty(long callTypeId,Integer offset) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCallTypeCancelPenaltyStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getCallTypeCancelPenaltyStmt = connection.prepareStatement("SELECT penalty.PenaltyID,penalty.TaxiCompanyID,"
    				+ "penalty.Name,penalty.Minutes,penalty.PerMinutes,penalty.OffSet FROM calltypepenalties,penalty "
    				+ "WHERE calltypepenalties.PenaltyID=penalty.PenaltyID AND calltypepenalties.event='cancel'"
    				+ "AND calltypepenalties.CallTypeID=? AND penalty.OffSet<=? ORDER BY penalty.OffSet DESC LIMIT 1");
    		getCallTypeCancelPenaltyStmt.setLong(1, callTypeId);
    		getCallTypeCancelPenaltyStmt.setInt(2, offset);
    		
    		ResultSet resultSet = getCallTypeCancelPenaltyStmt.executeQuery();
    		
    		Penalty penalty = null;
    		
    		if(resultSet.next()) {
    		
    			penalty = new Penalty();
    			penalty.setPenaltyId(resultSet.getLong("PenaltyID"));
    			penalty.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
    			penalty.setName(resultSet.getString("Name"));
    			penalty.setMinutes(resultSet.getInt("Minutes"));
    			penalty.setPerMinutes(resultSet.getInt("PerMinutes"));
    			penalty.setOffSet(resultSet.getInt("OffSet"));
    		
    		}
    		
    		resultSet.close();
    		
    		return penalty;
    		
    	} finally {
    		if(getCallTypeCancelPenaltyStmt!=null) getCallTypeCancelPenaltyStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    /**
     * Penalties that have to to with delayes
     * @param callTypeId
     * @return
     * @throws SQLException
     */
    public static Penalty getCallTypeDelayPenalty(long callTypeId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement getCallTypeCompletionPenaltyStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		getCallTypeCompletionPenaltyStmt = connection.prepareStatement("SELECT penalty.PenaltyID,penalty.TaxiCompanyID,"
    				+ "penalty.Name,penalty.Minutes,penalty.PerMinutes,penalty.OffSet FROM calltypepenalties,penalty "
    				+ "WHERE calltypepenalties.PenaltyID=penalty.PenaltyID AND calltypepenalties.event='delay'"
    				+ "AND calltypepenalties.CallTypeID=?");
    		getCallTypeCompletionPenaltyStmt.setLong(1, callTypeId);
    		
    		ResultSet resultSet = getCallTypeCompletionPenaltyStmt.executeQuery();
    		
    		Penalty penalty = null;
    		
    		if(resultSet.next()) {
    			
    			penalty = new Penalty();
    			penalty.setPenaltyId(resultSet.getLong("PenaltyID"));
    			penalty.setTaxiCompanyId(resultSet.getLong("TaxiCompanyID"));
    			penalty.setName(resultSet.getString("Name"));
    			penalty.setMinutes(resultSet.getInt("Minutes"));
    			penalty.setPerMinutes(resultSet.getInt("PerMinutes"));
    			penalty.setOffSet(resultSet.getInt("OffSet"));
    		}
    		
    		resultSet.close();
    		
    		return penalty;
    		
    	} finally {
    		if(getCallTypeCompletionPenaltyStmt!=null) getCallTypeCompletionPenaltyStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static boolean isDriverActive(long driverId) throws SQLException {
    	
    	Connection connection = null;
    	PreparedStatement isDriverActiveStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		isDriverActiveStmt = connection.prepareStatement("SELECT driverid FROM driver WHERE driverid=? AND Status='act'");
    		isDriverActiveStmt.setLong(1, driverId);
    		
    		ResultSet resultSet = isDriverActiveStmt.executeQuery();
    		
    		boolean active = false;
    		
    		if(resultSet.next()) {
    			active = true;
    		}
  
    		return active;  		
    	} finally {
    		if(isDriverActiveStmt!=null) isDriverActiveStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    }
    
    public static Long deviceOnTripDriverId(long deviceId) throws SQLException {
    	Connection connection = null;
    	PreparedStatement deviceOnTripDriverIdStmt = null;
    	
    	try {
    		connection = DatasourceProvider.getConnection();
    		deviceOnTripDriverIdStmt = connection.prepareStatement("SELECT driverid FROM trip "
    				+ "WHERE deviceid=? AND driverid is NOT NULL " 
    				+ "AND (Status='req' OR Status='act' OR Status='wai') "
    				+ "ORDER BY tripid LIMIT 1");
    		
    		deviceOnTripDriverIdStmt.setLong(1, deviceId);
    		ResultSet resultSet = deviceOnTripDriverIdStmt.executeQuery();
    		
    		Long driverId = null;
    		while(resultSet.next()) {
    			driverId = resultSet.getLong("driverid");
    			if(resultSet.wasNull()) driverId = null;
    		}
    		
    		return driverId;
    	} finally {
    		if(deviceOnTripDriverIdStmt!=null)deviceOnTripDriverIdStmt.close();
    		if(connection!=null) connection.close();
    	}
    }
    
    /**
     * Insert to trip batching 
     * @param deviceDistances
     * @param tripId
     * @param hailTime
     * @param lat
     * @param lng
     * @throws SQLException
     */
    public static void insertTripDispatchForTrip(Map<Long, BigDecimal> deviceDistances,Long tripId,Timestamp hailTime,Double lat,Double lng) throws SQLException {
    	
    	Connection connection = null;
    	Statement batchStmt = null;
    	
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		batchStmt = connection.createStatement();
    		
    		Set<Long> deviceIds = deviceDistances.keySet();
    		for(Long deviceId:deviceIds) {
    			
    			String sqlStatement = "INSERT INTO trip_assign (tripid, deviceid, responsetime, lat, lng, distance) "
    			 		+ "(SELECT "+tripId+","+deviceId+", "
		 				+ "time_to_sec(subtime(time('"+hailTime+"'),time(current_timestamp()))), "
		 						+ "device.currentlat, device.currentlong, "+deviceDistances.get(deviceId).toString()
		 						+" FROM device WHERE deviceid="+deviceId+" )";
    			
    			LOGGER.info("Batch statement is "+sqlStatement);
    			batchStmt.addBatch(sqlStatement);
    		}
    		
    		batchStmt.executeBatch();
    		
    	} finally {
    		if(batchStmt!=null) batchStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    	
    }
    
    public static List<Zone> fetchAllZones() throws SQLException {
		
		List<Zone> zones = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement fetchAllZonesStmt = null;
		
		try {
			
			connection = DatasourceProvider.getConnection();
			fetchAllZonesStmt = connection.prepareStatement("SELECT zoneid,taxicompanyid,"
					+ "zonename,ZoneLat,ZoneLong,ZoneRange,OpensAt,ClosesAt,MergesWithZoneId,"
					+ "Active,StrictBorders,KickDrivers,ZoneAdvantage,ZonePoints "
					+ "FROM zones ORDER BY zoneid DESC");
			
			ResultSet resultSet = fetchAllZonesStmt.executeQuery();
			
			while(resultSet.next()) {
				 Zone zone = new Zone();
                 zone.setZoneId(resultSet.getLong("zoneid"));
                 zone.setTaxiCompanyId(resultSet.getLong("taxicompanyid"));
                 zone.setZoneName(resultSet.getString("zonename"));
                 zone.setZoneLat(resultSet.getDouble("ZoneLat"));
                 zone.setZoneLong(resultSet.getDouble("ZoneLong"));
                 zone.setZoneRange(resultSet.getDouble("ZoneRange"));
                 zone.setOpensAt(resultSet.getTime("OpensAt"));
                 zone.setClosesAt(resultSet.getTime("ClosesAt"));
                 zone.setMergesWithZoneId(resultSet.getLong("MergesWithZoneId"));
                 zone.setActive(resultSet.getBoolean("Active"));
                 zone.setStrictBorders(resultSet.getBoolean("StrictBorders"));
                 zone.setKickDrivers(resultSet.getBoolean("KickDrivers"));
                 zone.setZoneAdvantage(resultSet.getBoolean("ZoneAdvantage"));
                 zone.setZonePoints(resultSet.getString("ZonePoints"));
                 zones.add(zone);

                 LOGGER.debug("The zone "+zone.getZoneId()+" "+zone.getZoneName());
			}
			
			return zones;
		
		} finally {
			if(fetchAllZonesStmt!=null) fetchAllZonesStmt.close();
			if(connection!=null) connection.close();
		}
		
	}
    
    public static Map<Long,Driver> fetchDriverForCache() throws SQLException {
    	
    	Map<Long,Driver> drivers = new HashMap<Long,Driver>();
    	
    	Connection connection = null;
    	PreparedStatement fetchDriverForCacheStmt = null;
 
    	try {
    		
    		connection = DatasourceProvider.getConnection();
    		fetchDriverForCacheStmt = connection.prepareStatement("SELECT driver.DriverID,"
    				+ "device.DeviceID,device.IMEI,device.SoftwareVersion "
    				+ "FROM driver,device WHERE driver.Status='act' "
    				+ "AND driver.active = 'true' "
    				+ "AND driver.DeviceID=device.DeviceID");
    		
    		ResultSet resultSet = fetchDriverForCacheStmt.executeQuery();
    		
    		while(resultSet.next()) {
    			Driver driver = new Driver();
    			driver.setDriverId(resultSet.getLong("DriverID"));
    			Device device = new Device();
    			device.setDeviceId(resultSet.getLong("DeviceID"));
    			device.setImei(resultSet.getString("IMEI"));
    			device.setSoftwareVersion(resultSet.getString("SoftwareVersion"));
    			driver.setDevice(device);
    			drivers.put(driver.getDriverId(), driver);
    		}
    		
    		
    		resultSet.close();
    		return drivers;
    		
    	} finally {
    		if(fetchDriverForCacheStmt!=null) fetchDriverForCacheStmt.close();
    		if(connection!=null) connection.close();
    	}
    	
    	
    }
    
}

