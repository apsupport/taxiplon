package aploon.datahelper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.jolbox.bonecp.BoneCPDataSource;

/*
 * Datasource provider with bonecp configuration
 * and providing back a datasource
 */
public class DatasourceProvider {

	private static BoneCPDataSource dataSource;
	
	private static final Logger LOGGER = Logger.getLogger(DatasourceProvider.class);
	
	private DatasourceProvider() {};
	
	static {

		LOGGER.info("Initializing the datasource");
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			
			Properties properties = new Properties();
			properties.load(new FileInputStream("DB.properties"));
			
			BoneCPDataSource boneCPDataSource = new BoneCPDataSource();
			boneCPDataSource.setJdbcUrl(properties.getProperty("jdbcConnection"));
			boneCPDataSource.setUsername(properties.getProperty("user"));
			boneCPDataSource.setPassword(properties.getProperty("password"));
			
			Integer minConnections = null;
			
			if(properties.getProperty("minconnections")!=null) {
				String minConnectionsStr = properties.getProperty("minconnections");
				minConnections = Integer.parseInt(minConnectionsStr);
			} else {
				minConnections = 1;
			}
			
			boneCPDataSource.setMinConnectionsPerPartition(minConnections);
			
			Integer maxConnections = null;
			
			if(properties.getProperty("maxconnections")!=null) {
				String maxConnectionsStr = properties.getProperty("maxconnections");
				maxConnections = Integer.parseInt(maxConnectionsStr);
			} else {
				maxConnections = 1;
			}
			
			LOGGER.info("The Connections are "+maxConnections+" "+minConnections);
			
			boneCPDataSource.setMaxConnectionsPerPartition(maxConnections);
			
			boneCPDataSource.setPartitionCount(1);
			
			dataSource = boneCPDataSource;
			
		} catch (ClassNotFoundException | FileNotFoundException e) {
			e.printStackTrace();
			LOGGER.error("Mysql class not loaded properties not found",e);
			throw new RuntimeException("Fatal Error",e);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("Problem with the properties file",e);
			throw new RuntimeException("Fatal Error",e);
		}
		
	}
	
	public static DataSource getDataSource() {
		
		return dataSource;
	}
	
	public static Connection getConnection() throws SQLException {
	
		LOGGER.info("Connection from  datasource "+dataSource.hashCode());
		
		return dataSource.getConnection();
	}

	public static void closeDatasource() {
	
		dataSource.close();
	}
	
}
