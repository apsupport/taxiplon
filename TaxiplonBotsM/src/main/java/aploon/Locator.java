package aploon;

/**
 * Locator in java
 */

import aploon.model.Coordinates;

import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;

import com.sun.org.apache.xpath.internal.XPathAPI;

import com.taxiplon.Util;

import java.io.StringReader;

import java.math.BigDecimal;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.NodeIterator;

import org.xml.sax.InputSource;
/**
 * @deprecated use TerraLocator instead
 */
@Deprecated
public class Locator {

	// url comes from http://code.google.com/apis/maps/documentation/geocoding/index.html#GeocodingRequests
	public static final String GOOGLE_MAP_WEB_SERVICE_URL =
			"http://maps.google.com/maps/api/geocode/xml?" + // either of xml or json
			"&language=%s"; // free style address (spaces must be replaced by pluses '+')
	public static final String INTERNATIONAL_LANG_CODE = "en";
	private String SERVICE_CHARSET = "UTF-8";
	private String primaryLanguage;
	private Pattern wordFollowedByDotPattern;
	private Pattern numberMinusNumberPattern;
	private Pattern streetCornerPattern;
	private XPath xpath;
	private Util util;

	public Locator(String lang) {
		super();
		wordFollowedByDotPattern = Pattern.compile(".*\\. ?(.+)");
		numberMinusNumberPattern = Pattern.compile("(.*[0-9]+)-[0-9]+(.*)");
		streetCornerPattern =
					  Pattern.compile("(.*\\d+ *&|&.*\\d+)"); //this will have an exception in the matching that the split symbol will also create a group
		primaryLanguage = lang;
		XPathFactory factory = XPathFactory.newInstance();
	    xpath = factory.newXPath();
	    util = new Util();
	}
	
	static final SpatialReference SPATIAL_REFERENCE_TAXIPLON = SpatialReference.create(3857);
	static final SpatialReference SPATIAL_REFERENCE_GPS = SpatialReference.create(4326);
	static final SpatialReference SPATIAL_REFERENCE_2100 = SpatialReference.create(2100);
	
	public static Point projectPointFrom2100ToGPS(float lng, float lat) {
	    return (Point) GeometryEngine.project(new Point(lng, lat), SPATIAL_REFERENCE_2100, SPATIAL_REFERENCE_GPS);
	}

	public String getAddressByCoordinates(Coordinates coord, boolean alsoFetchInternationalAddress) throws Exception {
		String address = "";
		try {
			address = fetchAddressByCoordinatesInLanguage(coord.getLat(), coord.getLng(), primaryLanguage);
		    coord.setNormalizedAddress(address);
		    //this will get the address in english (needed for TTS on Android)
		    if(alsoFetchInternationalAddress) {
		        String internationalAddress = fetchAddressByCoordinatesInLanguage(coord.getLat(), coord.getLng(), INTERNATIONAL_LANG_CODE);
				coord.setInternationalAddress(internationalAddress);
		    }
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to match requested address due to exception: " + e.getMessage());
		}
		return address;
	}
	
	public String fetchAddressByCoordinatesInLanguage(Double lat, Double lng, String lang) throws Exception {
		String outcome = null;
		String url = String.format(GOOGLE_MAP_WEB_SERVICE_URL, lang) + "&sensor=false" + "&latlng=" + lat + "," + lng;
	    String content = util.getContent(url, null, SERVICE_CHARSET, false);
	    DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    // Parse the XML file and build the Document object in RAM
	    Document doc = docBuilder.parse(new InputSource(new StringReader(content)));
	    doc.getDocumentElement().normalize();
	    String status = XPathAPI.eval(doc.getDocumentElement(), "/GeocodeResponse/status").toString();
	    if (status.equals("OK")) {
	        //see a sample XML returned by Google at http://maps.googleapis.com/maps/api/geocode/xml?latlng=40.714224,-73.961452&sensor=true
	        NodeIterator results = XPathAPI.selectNodeIterator(doc, "/GeocodeResponse/result");
	        Node item;
	        while ((item = results.nextNode()) != null && outcome == null) {
	            String type = XPathAPI.eval(item, "type").toString();
	            HashMap<String, String> parts = new HashMap<String, String>();
	            //the coolest result returned has type 'street_address', looks like '279-281 Bedford Ave, Brooklyn, NY 11211, USA'
	            if (type.equals("street_address")) {
	                NodeIterator addressComponents = XPathAPI.selectNodeIterator(item, "address_component");
	                Node component;
	                while ((component = addressComponents.nextNode()) != null) {
	                    /* Google returns more than one type in address_component's for
	                     * all levels except for route and street_number, but it seems that the first one
	                     * is always what we are looking for, whilst the other one is 'political' whatever that means
	                     */
	                    String componentType = XPathAPI.eval(component, "type").toString();
	                    String longName = XPathAPI.eval(component, "long_name").toString();
	                    parts.put(componentType, longName);
	                }
	                String part1 = parts.get("route");
	                String part2 = parts.get("street_number");
	                String part3 = parts.get("sublocality");
	                String part4 = parts.get("locality");
	                String part5 = parts.get("administrative_area_level_3");
	                //String part6 = parts.get("administrative_area_level_2");
	                //String part7 = parts.get("administrative_area_level_1");
					outcome = part1 + (part2 != null ? " " + part2 : "") + (part3 != null ? ", " + part3 : "") +
	                                           (part4 != null ? " | " + part4 : "") + //the symbol | tells the parser that the following part should be on a new line
	                                           (part5 != null ? ", " + part5 : "");
	                    //(part5 != null? ", " + part6 : "") +
	                    //(part6 != null? ", " + part7 : "")
	            }
	        }
	        //if the location couldn't been determined very precisely
	        if (outcome == null) {
	            Node formattedAddressNode =
	                               (Node)xpath.evaluate("//formatted_address", new InputSource(new StringReader(content)),
	                                                    XPathConstants.NODE);
	            outcome = formattedAddressNode.getTextContent();
	        }
	        
	    }
	    else {
	        throw new RuntimeException("Google said " + status);
	    }
		return outcome;
	}

	public Coordinates getCoordinatesByAddress(String address, String regionCode) throws RuntimeException {
		address = normalizeAddress(address).replaceAll(" ", "+");
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		try {
			String url = String.format(GOOGLE_MAP_WEB_SERVICE_URL, primaryLanguage) + "&sensor=false" + "&region=" + regionCode + "&address=" + 
										java.net.URLEncoder.encode(address, "UTF-8");
			String content = util.getContent(url, null, SERVICE_CHARSET, false);
			//      InputSource inputXml = new InputSource(new StringReader(content));
			String status = xpath.evaluate("/GeocodeResponse/status", new InputSource(new StringReader(content)));
			if (status.equals("OK")) {
				// Because the evaluator may return multiple entries, we specify that the expression
				// return a NODESET and place the result in a NodeList.
				Node latNode =
(Node)xpath.evaluate("//geometry/location/lat", new InputSource(new StringReader(content)), XPathConstants.NODE);
				Node lngNode =
(Node)xpath.evaluate("//geometry/location/lng", new InputSource(new StringReader(content)), XPathConstants.NODE);
				Node normalizedAddressNode =
								(Node)xpath.evaluate("//formatted_address", new InputSource(new StringReader(content)),
													 XPathConstants.NODE);
				Coordinates coord = null;
				try {
					Double lat = null, lng = null;
					String latStr = latNode.getTextContent();
					//          System.out.println("Latitude: " + latStr);
					lat = Double.parseDouble(latStr);
					String lngStr = lngNode.getTextContent();
					//          System.out.println("Longtitude: " + lngStr);
					lng = Double.parseDouble(lngStr);
					String normalizedAddress = normalizedAddressNode.getTextContent();
					//          System.out.println("Normalized address: " + normalizedAddress);
					if (lat != null && lng != null) {
						coord = new Coordinates(lat, lng, normalizedAddress);
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				return coord;
			}
			else {
				throw new RuntimeException("Google said " + status);
			}
		}
		catch (Exception e) {
			//      e.printStackTrace();
			throw new RuntimeException("Failed to match requested address due to exception: " + e.getMessage());
		}
	}

	/**
	 * As Google Maps API requires the address dest look up dest be in specific form,
	 * this method normalizes misformed addresses, that will clearly return no results.
	 * @param addr
	 * @return Same address stripped of misformed parts that prevent Google Maps API src locating it.
	 */
	public String normalizeAddress(String addr) {
		addr = addr.toUpperCase();
		// escape commas
		addr = addr.replaceAll(",", "");
		// Eliminate corners (e.g. yakinthou & kapodistriou 54 or ). The number can follow the first or the second street name.
		addr = addr.replaceAll("\u039A\u0391\u0399", "&");
		Matcher m = streetCornerPattern.matcher(addr);
		if (m.find()) {
			addr = m.group(1);
			addr = addr.replace('&', ' ');
		}
		// special case is saint (e.g. ag. georgiou should not be treated as georgiou)
		addr = addr.replace("\u0391\u0393\\.?", "\u0391\u0393\u0399\u039F\u03A5");
		// Strip off the parts followed by dots (e.g. el.venizelou, v.sofias)
		m = wordFollowedByDotPattern.matcher(addr);
		if (m.find()) {
			addr = m.group(1);
		}
		// Keep just one number of the building (e.g. kifisias 221-223)
		m = numberMinusNumberPattern.matcher(addr);
		if (m.find()) {
			addr = m.group(1) + m.group(2);
		}
		// special treatment for 'kon/nou'
		addr =
	 addr.replace("\u039A\u03A9\u039D/\u039D\u039F\u03A5", "\u039A\u03A9\u039D\u03A3\u03A4\u0391\u039D\u03A4\u0399\u039D\u039F\u03A5");
		// special treatment for 'kon/pol' (matches both kon/polis and kon/poleos)
		addr =
	 addr.replace("\u039A\u03A9\u039D/\u03A0\u039F\u039B", "\u039A\u03A9\u039D\u03A3\u03A4\u0391\u039D\u03A4\u0399\u039D\u039F\u03A5\u03A0\u039F\u039B");
		// special treatment for 'thes/niki'
		addr =
	 addr.replace("\u0398\u0395\u03A3/\u039D\u0399\u039A\u0397", "\u0398\u0395\u03A3\u03A3\u0391\u039B\u039F\u039D\u0399\u039A\u0397");
		// remove useless spaces
		addr = addr.trim();
		return addr + "+Greece";
	}

	public static void main(String[] args) {
		Locator l = new Locator("el");
		try {
			Coordinates coord = new Coordinates(new BigDecimal("38.0336412"), new BigDecimal("23.8340532"));
			String addressFound =
						 l.getAddressByCoordinates(coord, true);
			System.out.println("Address found: " + coord.getNormalizedAddress() + ", international version: " + coord.getInternationalAddress());
		}
		catch (Exception e) {
			System.err.println("Failed to find address by given coordinates due to exception: " + e.getMessage());
		}
		args = new String[] {
					//        "\u039B. \u039A\u0397\u03A6\u0399\u03A3\u0399\u0391\u03A3 221-223, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
					//"\u03A5\u0391\u039A\u0399\u039D\u0398\u039F\u03A5 & \u039A\u0391\u03A0\u039F\u0394\u0399\u03A3\u03A4\u03A1\u0399\u039F\u03A5 54, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
					//"\u039B\u0395\u03A9\u03A6. \u039A\u03A5\u03A0\u03A1\u039F\u03A5 39 & \u039C\u03A0\u0399\u0396\u0391\u039D\u0399\u039F\u03A5 1 \u0391\u03A1\u0393\u03A5\u03A1\u039F\u03A5\u03A0\u039F\u039B\u0397",
					//"\u03A7\u03B5\u03B9\u03BC\u03AC\u03C1\u03B1\u03C2 61 \u03BA\u03B1\u03B9 \u039A\u03B1\u03BB\u03BB\u03B9\u03C0\u03CC\u03BB\u03B5\u03C9\u03C2",
					//"\u03A0\u03A1\u03A9\u03A4\u039F\u03A0\u0391\u03A0\u0391\u0394\u0391\u039A\u0397 1 \u0395\u039B.\u0392\u0395\u039D\u0399\u0396\u0395\u039B\u039F\u03A5&\u03A3\u03A5\u03A1\u039F\u03A5, \u0393\u0391\u039B\u0391\u03A4\u03A3\u0399",
					//        "\u0392.\u03A3\u039F\u03A6\u0399\u0391\u03A3 13, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
					//        "\u039B\u0395\u03A9\u03A6. \u039A\u0397\u03A6\u0399\u03A3\u0399\u0391\u03A3 170 - \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
					//        "\u039B.\u039A\u0397\u03A6\u0399\u03A3\u0399\u0391\u03A3 205, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
					//        "\u039B. \u039A\u03A5\u039C\u0397\u03A3 110, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399",
					//        "\u0391\u0393.\u039A\u03A9\u039D/\u039D\u039F\u03A5 24, \u039C\u0391\u03A1\u039F\u03A5\u03A3\u0399"
					} ;
		for (String s : args) {
			//l.getCoordinatesByAddress(s);
		}
	}

}
