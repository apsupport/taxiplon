package gr.datamation.dataloader.entities;

import java.util.Calendar;

public class TrainInfo {
	
	private Calendar arrivalDateTime;
	private String 	 trainCode;
	private String   from;
	
	
	
	public Calendar getArrivalDateTime() {
		return arrivalDateTime;
	}
	public void setArrivalDateTime(Calendar arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}
	public String getTrainCode() {
		return trainCode;
	}
	public void setTrainCode(String trainCode) {
		this.trainCode = trainCode;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	
	
	

}
