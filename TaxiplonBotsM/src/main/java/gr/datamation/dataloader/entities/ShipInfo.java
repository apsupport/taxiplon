package gr.datamation.dataloader.entities;

import java.util.Date;

public class ShipInfo {
	
	private String 	port;
	private String 	vesselName;
	private Date	programmedArrival;
	private Date	estimatedArrival;
	private Date    arrival;	
	private Date 	departure;
	
	public String getPort() {
		return port;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	
	public String getVesselName() {
		return vesselName;
	}
	
	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}
	
	public Date getProgrammedArrival() {
		return programmedArrival;
	}
	
	public void setProgrammedArrival(Date programmedArrival) {
		this.programmedArrival = programmedArrival;
	}
	
	public Date getEstimatedArrival() {
		return estimatedArrival;
	}
	
	public void setEstimatedArrival(Date estimatedArrival) {
		this.estimatedArrival = estimatedArrival;
	}
	
	public Date getArrival() {
		return arrival;
	}
	
	public void setArrival(Date arrival) {
		this.arrival = arrival;
	}

	public Date getDeparture() {
		return departure;
	}

	public void setDeparture(Date departure) {
		this.departure = departure;
	}

}
