package gr.datamation.dataloader.entities;

import java.util.Calendar;

public class OpenCalendarInfo {
	
	private String 		nameOfEvent;
	private String 		placeOfEvent;
	private String 		description;
	private String 		addressOfEvent;
	private String 		eventType;
	private Calendar	eventStartDateTime;
	private Calendar    eventEndDateTime;
	private String      eventDateTimeString;
	
	
	public String getNameOfEvent() {
		return nameOfEvent;
	}
	public void setNameOfEvent(String nameOfEvent) {
		this.nameOfEvent = nameOfEvent;
	}
	public String getPlaceOfEvent() {
		return placeOfEvent;
	}
	public void setPlaceOfEvent(String placeOfEvent) {
		this.placeOfEvent = placeOfEvent;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddressOfEvent() {
		return addressOfEvent;
	}
	public void setAddressOfEvent(String addressOfEvent) {
		this.addressOfEvent = addressOfEvent;
	}
	public Calendar getEventStartDateTime() {
		return eventStartDateTime;
	}
	public void setEventStartDateTime(Calendar eventStartDateTime) {
		this.eventStartDateTime = eventStartDateTime;
	}
	public Calendar getEventEndDateTime() {
		return eventEndDateTime;
	}
	public void setEventEndDateTime(Calendar eventEndDateTime) {
		this.eventEndDateTime = eventEndDateTime;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventDateTimeString() {
		return eventDateTimeString;
	}
	public void setEventDateTimeString(String eventDateTimeString) {
		this.eventDateTimeString = eventDateTimeString;
	}
	
}
