package gr.datamation.dataloader.entities;

import java.text.SimpleDateFormat;

import java.util.Date;

public class FlightInfo {

	private String 	fromTo;
	private String 	company;
	private String 	flightNo;
	private String 	via;
	private String 	gate;
	private Date	expectedAD;
	private Date expectedTime;
	private Date programmedTime;
	private Date	programmedAD;
	private String	notes;
	
	//example of time returned is 2011-06-01T09:47:00, they call it XML DateTime
	final static SimpleDateFormat xmlDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	final static SimpleDateFormat twentyFourHour = new SimpleDateFormat("HH:mm");
	
	public static Date parseTime(String time) {
		try {
			return xmlDateTime.parse(time);
		}
		catch(Exception e) {
			try {
			    return twentyFourHour.parse(time);
			}
			catch(Exception ex) {
			    return null;
			}
		}
	}

	public void setProgrammedTime(String programmedTime) {
		this.programmedTime = parseTime(programmedTime);
	}

	public String getFormattedProgrammedTime() {
		return twentyFourHour.format(programmedTime);
	}

	public void setExpectedTime(String expectedTime) {
		this.expectedTime = parseTime(expectedTime);
	}

	public String getFormattedExpectedTime() {
		return twentyFourHour.format(expectedTime);
	}
	
	public String getFromTo() {
		return fromTo;
	}
	public void setFromTo(String fromTo) {
		this.fromTo = fromTo;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		//this will make sure that the flight code is identical with the one returned by flightstats.com (e.g. OA 017 from aia.gr == OA 17 from flightstats.com)
		try {
			String[] parts = flightNo.split(" ", 2);
			flightNo = parts[0] + " " + Integer.parseInt(parts[1]);
		}
		catch(Exception e) {
			System.err.println("Failed to split the flight no and parse the numeric part due to exception: " + e.getMessage());
		}
		this.flightNo = flightNo;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public Date getExpectedAD() {
		return expectedAD;
	}
	public void setExpectedAD(Date expectedAD) {
		this.expectedAD = expectedAD;
	}
	public Date getProgrammedAD() {
		return programmedAD;
	}
	public void setProgrammedAD(Date programmedAD) {
		this.programmedAD = programmedAD;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void setProgrammedTime(Date programmedTime) {
		this.programmedTime = programmedTime;
	}

	public Date getProgrammedTime() {
		return programmedTime;
	}

	public void setExpectedTime(Date expectedTime) {
		this.expectedTime = expectedTime;
	}

	public Date getExpectedTime() {
		return expectedTime;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getGate() {
		return gate;
	}
}
