package gr.datamation.dataloader.parser;

import java.util.List;

import gr.datamation.dataloader.entities.FlightInfo;
import gr.datamation.dataloader.util.Utils;


public class AIAParser {
	
	private String	mainSite = "http://www.aia.gr/currentflights.asp?";
	private int 	languageId;
	private int 	currentPage;
	private int 	limitResults;
	private boolean arrivals;
	private int 	orderBy;
	private boolean orderAsc;
	private String 	airport;
	private String 	company;
	private int 	range;
	
	private Utils util;
		
	public AIAParser(boolean arrivals) {
		util = new Utils();
		//default GET parameters
		mainSite = "http://www.aia.gr/currentflights.asp?langid=1&currentPage=1&Arrivals=&Departures=" + (arrivals? "" : "Departures") + "&Airport=&Company=&range=3&orderby=0&orderdirection=asc&limit=100";
	}

	public AIAParser(int languageId, int currentPage, int limitResults,
			boolean arrivals, int orderBy, boolean orderAsc, String airport,
			String company, int range) {
		super();
		util = new Utils();
		this.languageId = languageId;
		this.currentPage = currentPage;
		this.limitResults = limitResults;
		this.arrivals = arrivals;
		this.orderBy = orderBy;
		this.orderAsc = orderAsc;
		this.airport = airport;
		this.company = company;
		this.range = range;
		
		mainSite = "http://www.aia.gr/currentflights.asp?";
		mainSite += languageId == 0 ? "langid=1" : "langid=" + languageId;
		mainSite += currentPage == 1 ? "&currentPage=1" : "&currentPage=" + currentPage;
		mainSite += limitResults == 0 ? "&limit=100" : "&limit=" + limitResults;
		mainSite += arrivals ? "&Arrivals=Arrivals" : "&Departures=Departures";
		mainSite += orderBy == 0 ? "&orderby=0" : "&orderby=" + orderBy;
		mainSite += orderAsc ? "&orderdirection=asc" : "&orderdirection=desc";
		mainSite += "&Airport=" + airport;
		mainSite += "&Company=" + company;
		mainSite += "&range=" + range;
	}

	public int getLanguageId() {
		return languageId;
	}

	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getLimitResults() {
		return limitResults;
	}

	public void setLimitResults(int limitResults) {
		this.limitResults = limitResults;
	}

	public boolean isArrivals() {
		return arrivals;
	}

	public void setArrivals(boolean arrivals) {
		this.arrivals = arrivals;
	}

	public int getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}

	public boolean isOrderAsc() {
		return orderAsc;
	}

	public void setOrderAsc(boolean orderAsc) {
		this.orderAsc = orderAsc;
	}

	public String getAirport() {
		return airport;
	}

	public void setAirport(String airport) {
		this.airport = airport;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}
	
	public List<FlightInfo> parsePage() throws Exception {
		try {
			return parse(mainSite);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		
	}
	
	/**
	 * This method just extracts the links and the "body" text from the page.
	 * @param pageToParse
	 * @throws Exception
	 */
	public List<FlightInfo> parse(String pageToParse) throws Exception {
		String pageContent = "";
		pageContent = util.getContent(pageToParse, null);
		try {
			AIAHTMLParser pp = new AIAHTMLParser();
			return pp.parseContent(pageContent);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	
	

}
