package gr.datamation.dataloader.parser;

import gr.datamation.dataloader.entities.FlightInfo;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class AIAHTMLParser extends HTMLEditorKit.ParserCallback {

	String payload;
	boolean containerStarted;
	int tagCounter;
	int startingPos;
	int startingPosTD;
	int endingPos;
	//7 columns
	int tdCounter = 0;
	
	private List<FlightInfo> flightInfoList = new ArrayList<FlightInfo>();
	private FlightInfo currentFlightInfo;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");

	/**
	 * Parses the page to find just one block per page and returns that block back to the caller.
	 * @param htmlDocument
	 */
	public List<FlightInfo> parseContent(String htmlDocument) throws Exception {
		tagCounter = 0;
		containerStarted = false;
		HTMLEditorKit.Parser delegator = new ParserDelegator();
		delegator.parse(new StringReader(htmlDocument), this, true);
		return flightInfoList;
	}

	public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		if (t.toString().equalsIgnoreCase("table")) {
			Object name = a.getAttribute(HTML.Attribute.CLASS);
			if (name != null && name.toString().equalsIgnoreCase("flights")) {
				containerStarted = true;
				startingPos = pos;
			}
			if (containerStarted) {
				tagCounter++;
			}
		}
		else if (t.toString().equalsIgnoreCase("td") && containerStarted) {
			startingPosTD = pos;
			if (tdCounter == 0)
				currentFlightInfo = new FlightInfo();
			tdCounter++;
		}
	}

	public void handleEndTag(HTML.Tag t, int pos) {
		if (t.toString().equalsIgnoreCase("table") && containerStarted) {
			if (--tagCounter == 0) {
				containerStarted = false;
				endingPos = pos;
			}
		}
		else if (t.toString().equalsIgnoreCase("td") && containerStarted) {
			startingPosTD = -1;
		}
	}

	public void handleText(char[] data, int pos) {
		if (containerStarted && startingPosTD > 0) {
			payload = new String(data);
			payload = payload.replaceAll("\\u00a0", "");
			payload = payload.trim();
			switch (tdCounter) {
				case 1: currentFlightInfo.setFromTo(payload); break;
				case 2: currentFlightInfo.setCompany(payload); break;
				case 3: currentFlightInfo.setFlightNo(payload); break;
				case 4: currentFlightInfo.setVia(payload); break;
				case 5: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentFlightInfo.setExpectedAD(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;
				case 6: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentFlightInfo.setProgrammedAD(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;
				case 7: currentFlightInfo.setNotes(payload); break;
			}
			if (tdCounter == 7) {
				tdCounter = 0;
				flightInfoList.add(currentFlightInfo);
			}
		}
	}

}
