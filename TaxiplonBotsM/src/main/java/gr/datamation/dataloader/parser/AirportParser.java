package gr.datamation.dataloader.parser;

import gr.datamation.dataloader.entities.FlightInfo;

import java.text.SimpleDateFormat;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import java.util.GregorianCalendar;

import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class AirportParser extends DefaultHandler {
	
	public static final String ROOT_ELEMENT = "Flight";
	
	ArrayList<FlightInfo> flights;
	
	public AirportParser() {
		super();
		this.flights = new ArrayList<FlightInfo>();
	}
	
	FlightInfo currentFlight = null;
	
	/**
	 * Predefined sorting of the airport schedule on scheduled time in ascending order
	 * @note not really useful
	 */
	public void sort() {
		//sort by scheduled arrival time
		Object[] flightArray = flights.toArray();
		Arrays.sort(flightArray, new Comparator<Object>() {
			@Override
			public int compare(Object left, Object right) {
				return ((FlightInfo) left).getProgrammedTime().compareTo(((FlightInfo) right).getProgrammedTime());
			}
		});
		flights.clear();
		for(Object o : flightArray) {
			flights.add((FlightInfo) o);
		}
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		if(qName.equals(ROOT_ELEMENT)) {
		    currentFlight = new FlightInfo();
			String fromTo = atts.getValue("OriginCity");
			if(fromTo == null) {
				fromTo = atts.getValue("DestinationCity");
			}
			currentFlight.setFromTo(fromTo);
		    currentFlight.setCompany(atts.getValue("AirlineName"));
		    currentFlight.setFlightNo(atts.getValue("Flight"));
			currentFlight.setGate(atts.getValue("Gate"));
		    currentFlight.setVia(null);
		    currentFlight.setExpectedTime(atts.getValue("CurrentTime"));
		    currentFlight.setProgrammedTime(atts.getValue("ScheduledTime"));
		    currentFlight.setNotes(atts.getValue("StatusCode"));
			flights.add(currentFlight);
		}
	}

	public void setFlights(ArrayList<FlightInfo> flights) {
		this.flights = flights;
	}

	public ArrayList<FlightInfo> getFlights() {
		return flights;
	}
}
