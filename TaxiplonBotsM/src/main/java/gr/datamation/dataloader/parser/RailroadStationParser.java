package gr.datamation.dataloader.parser;

import gr.datamation.dataloader.entities.TrainInfo;

import java.util.List;

public interface RailroadStationParser {
	
	public List<TrainInfo> parsePage() throws Exception;
	
	public String getName();
	
}
