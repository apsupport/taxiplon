package gr.datamation.dataloader.parser;


import gr.datamation.dataloader.entities.TrainInfo;
import gr.datamation.dataloader.util.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class OSEParser implements RailroadStationParser {
	
	private static String stationName = "Σταθμός Λαρίσσης";
	
	private String mainSite =
		   "http://tickets.trainose.gr/dromologia/ajax.php?c=dromologia&op=vres_dierxomena&sta8mos=%CE%91%CE%98%CE%97%CE%9D";
	private Utils util;
	private HashMap<String, String> stations = new HashMap<String, String>();

	public OSEParser() {
		util = new Utils();
		stations.put("ΧΑΛΚ", "Χαλκίδα");
		stations.put("ΛΑΡΙ", "Λάρισα");
		stations.put("ΠΑΤΡ", "Πάτρα");
		stations.put("ΘΕΣΣ", "Θεσσαλονίκη");
		stations.put("ΚΑΛΑ", "Καλαμπάκα");
		stations.put("ΛΕΙΑ", "Λειανοκλάδι");
		stations.put("ΟΙΝΟ", "Οινόη");
	}

	public OSEParser(Utils util) {
		super();
		util = new Utils();
		this.util = util;
	}

	public List<TrainInfo> parsePage() throws Exception {
		try {
			String url = mainSite;
			String pageContent = util.getContent(url, "UTF-8");
			return parseProfilesJson(pageContent);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getName() {
		return stationName;
	}

	public List<TrainInfo> parseProfilesJson(String jsonStringDate) {
		List<TrainInfo> returnList = new ArrayList<TrainInfo>();
		try {
			//System.out.println(jsonStringDate);
			JSONObject myjson = new JSONObject(jsonStringDate);

			JSONArray jsonArray = myjson.getJSONArray("data");

			int size = jsonArray.length();
			ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();
			for(int i = 0; i < size; i++) {
				JSONObject currentJSONObject = jsonArray.getJSONObject(i);
				arrays.add(currentJSONObject);
				String destination = currentJSONObject.getString("pros");
				if("ΑΘΗΝ".equals(destination)) {
					TrainInfo info = new TrainInfo();
					info.setFrom(stations.get((String)currentJSONObject.get("apo")));
					info.setTrainCode((String)currentJSONObject.get("treno"));
					SimpleDateFormat sdf = new SimpleDateFormat("HH.mm");
					try {
						Calendar cal = Calendar.getInstance();
						String hour = (String)currentJSONObject.get("wra");
						//HANDLE CORRECTLY THE TIME BECAUSE .07 CANNOT BE PARSED AS 00.07 FOR EXAMPLE
						if(hour.startsWith("."))
							hour = "0" + hour;
						else if(hour.indexOf(".") < 0)
							hour = hour + ".00";
						if(hour.substring(hour.indexOf(".") + 1).length() == 1)
							hour = hour + "0";
						Date d = sdf.parse(hour);
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(d);
						cal.set(Calendar.HOUR_OF_DAY, cal2.get(Calendar.HOUR_OF_DAY));
						cal.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE));
						cal.set(Calendar.SECOND, 0);
						info.setArrivalDateTime(cal);
					}
					catch(ParseException e) {
						e.printStackTrace();
					}
					returnList.add(info);
				}
			}

			//Finally
			//JSONObject[] jsons = new JSONObject[arrays.size()];
			//arrays.toArray(jsons);

		}
		catch(JSONException e) {
			e.printStackTrace();
		}
		return returnList;
	}

}
