package gr.datamation.dataloader.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.management.loading.PrivateClassLoader;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import gr.datamation.dataloader.entities.ShipInfo;
import gr.datamation.dataloader.util.Utils;


public class MarineTrafficParser {


	//Main page
	//http://www.marinetraffic.com/ais/gr/datasheet.aspx?PORT_NAME=PIRAEUS&SHIPNAME=&menuid=&datasource=V_ETA&app=&mode=&B1=%CE%91%CE%BD%CE%B1%CE%B6%CE%AE%CF%84%CE%B7%CF%83%CE%B7
	//PORT_NAME
	//?PORT_NAME=PIRAEUS&
	//SHIPNAME
	//SHIPNAME=&
	//datasource
	//datasource=V_ETA&
	//app
	//app=&
	//mode
	//mode=&
	//B1
	//B1=%CE%91%CE%BD%CE%B1%CE%B6%CE%AE%CF%84%CE%B7%CF%83%CE%B7

	private String mainURL = "";
	private String failoverURL = "";
	private String portName;
	private String shipName;
	private String dataSource;
	private String app;
	private String mode;
	private String B1;

	private Utils util;

	private static final String arrivalTemplateUrl = "http://www.marinetraffic.com/en/ais/index/port_moves/port:%s/move_type:0/per_page:30";
	private static final String departureTemplateUrl = "http://www.marinetraffic.com/en/ais/index/port_moves/port:%s/move_type:1/per_page:30";
	private static final String estimatedArrivalTemplateUrl = "http://www.marinetraffic.com/en/ais/index/eta/port:%s/per_page:30";
	
	private static final Logger LOGGER = Logger.getLogger(MarineTrafficParser.class);
	
	private List<ShipInfo> arrivals = new ArrayList<>();
	private List<ShipInfo> departures = new ArrayList<>();
	private List<ShipInfo> estimatedArrivals = new ArrayList<>();
	
	public MarineTrafficParser(String portName) {
		
		util = new Utils();
		
		LOGGER.info("The portname is "+portName);
	
		this.portName = portName;
		parseEstimatedArrival();
		//parseArrivals();
		//parseDepartures();
		
		//String arrivalUrl = String.format(arrivalTemplateUrl, portName);
		//String departureUrl = String.format(departureTemplateUrl, portName);
		
		/**
		//default GET parameters
		mainURL = "http://www.marinetraffic.com/ais/gr/datasheet.aspx?PORT_NAME=" + portName + "&SHIPNAME=&menuid=&datasource=V_ETA&app=&mode=&B1=Search";
		//8/7/2011 there was a problem 500 error for the greek version, hence added a failover scenario to fetch data from the english version
		failoverURL = "http://www.marinetraffic.com/ais/datasheet.aspx?PORT_NAME=" + portName + "&SHIPNAME=&menuid=&datasource=V_ETA&app=&mode=&B1=Search";
		*/
	}
	
	private void parseEstimatedArrival() {
		
		String estimatedArrivalUrl = String.format(estimatedArrivalTemplateUrl, portName);
		try {
			String html = requestWithAlteredAgent(estimatedArrivalUrl);
			
			Document document = Jsoup.parse(html);
			Element portInfoElement = document.select("div.filters_results_table").first();
			Elements elements =  portInfoElement.getAllElements();
			
			for(Element element:elements) {
				if(element.tag().toString().equals("tr")&&element.select("span").first()!=null) {
					if(element.select("td[align$=center]").first()==null) {
						//LOGGER.info("Get it "+element.text());
						
						Elements tdElements = element.select("td");
						
						LOGGER.info("The elemenents "+tdElements.size());
						
						if(tdElements.size()==6) {
							
							String portStr = tdElements.get(0).text();
							String vesselsStr = tdElements.get(1).text();
							String scheduledArrivalStr = tdElements.get(2).text();
							String estimatedArrivalStr = tdElements.get(3).text();
							String arrivalStr = tdElements.get(4).text();
							
							//LOGGER.info("To sum up "+portStr+" "+vesselsStr+" "+
							//scheduledArrivalStr+" "+estimatedArrivalStr+" "+arrivalStr);
							
							DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
							
								
							ShipInfo shipInfo = new ShipInfo();
								
							try {
								Date scheduledArrival = dateFormat.parse(scheduledArrivalStr);
								shipInfo.setProgrammedArrival(scheduledArrival);
							} catch(ParseException e) {
								LOGGER.debug("Αrrival date for port "+portStr +" not found");
							}
							try {
								Date estimatedArrival = dateFormat.parse(estimatedArrivalStr);
								shipInfo.setEstimatedArrival(estimatedArrival);
							} catch(ParseException e) {
								LOGGER.debug("Estimated arrival date for port "+portStr+" not found");
							}
							
							try {
								Date arrival = dateFormat.parse(arrivalStr);
								shipInfo.setArrival(arrival);
							} catch(ParseException e) {
								LOGGER.debug("Arrival date for port "+portStr+" not found");
							}
								
							shipInfo.setPort(portStr);
							shipInfo.setVesselName(vesselsStr);
							estimatedArrivals.add(shipInfo);
			
						}
					}
				}
			}
			
		} catch(IOException e) {
			LOGGER.error("IO  exception on ports",e);	
		} 
		
	}
	
	private void parseDepartures() {
		
		String departureUrl = String.format(departureTemplateUrl, portName);
		
		try {
			String html = requestWithAlteredAgent(departureUrl);
			//LOGGER.info("The html is "+html);
			
			Document document = Jsoup.parse(html);
			Element portInfoElement = document.select("div.filters_results_table").first();
			Elements elements =  portInfoElement.getAllElements();
			
			for(Element element:elements) {
				if(element.tag().toString().equals("tr")&&element.select("span").first()!=null) {
					if(element.select("td[align$=center]").first()==null) {
						Elements tdElements = element.select("td");
						if(tdElements.size()==4) {
							String dateStr = tdElements.get(0).text();
							String detStr = tdElements.get(1).text();
							String portStr = tdElements.get(2).text();
							String vesselStr = tdElements.get(3).text();
							LOGGER.info("To sum up "+dateStr+" "+detStr+" "+portStr+" "+vesselStr);
							
							DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
							try {
								Date arrival = dateFormat.parse(dateStr);
								ShipInfo shipInfo = new ShipInfo();
								shipInfo.setDeparture(arrival);
								shipInfo.setPort(portStr);
								shipInfo.setVesselName(vesselStr);
								departures.add(shipInfo);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
						}	
					}
				}
			}
			
			
		} catch (IOException e) {
			LOGGER.error("The io exception is",e);
		}
		
	}
	
	private void parseArrivals() {
	
		String arrivalUrl = String.format(arrivalTemplateUrl, portName);
		
		try {
			String html = requestWithAlteredAgent(arrivalUrl);
			
			//LOGGER.info("The html is "+html);
			
			Document document = Jsoup.parse(html);
			Element portInfoElement = document.select("div.filters_results_table").first();
			Elements elements =  portInfoElement.getAllElements();
			
			for(Element element:elements) {
				if(element.tag().toString().equals("tr")&&element.select("span").first()!=null) {
					if(element.select("td[align$=center]").first()==null) {
						Elements tdElements = element.select("td");
						if(tdElements.size()==4) {
							String dateStr = tdElements.get(0).text();
							String detStr = tdElements.get(1).text();
							String portStr = tdElements.get(2).text();
							String vesselStr = tdElements.get(3).text();
							LOGGER.info("To sum up "+dateStr+" "+detStr+" "+portStr+" "+vesselStr);
							ShipInfo shipInfo = new ShipInfo();
							DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
							try {
								Date arrival = dateFormat.parse(dateStr);
								shipInfo.setArrival(arrival);
								shipInfo.setPort(portStr);
								shipInfo.setVesselName(vesselStr);
								arrivals.add(shipInfo);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}	
					}
				}
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	public List<ShipInfo> getEstimatedArrivals() {
		return estimatedArrivals;
	}

	public List<ShipInfo> getArrivals() {
	
		return arrivals;
	}
	
	public List<ShipInfo> getDepartures() {
		
		return departures;
	}
	
	/**
	 *
	 * @param portName
	 * @param shipName
	 * @param dataSource
	 * @param app
	 * @param mode
	 * @param b1
	 * @param util
	 * @deprecated
	 */
	@Deprecated
	public MarineTrafficParser(String portName, String shipName, String dataSource, String app,
							   String mode, String b1, Utils util) {
		super();
		util = new Utils();
		this.portName = portName;
		this.shipName = shipName;
		this.dataSource = dataSource;
		this.app = app;
		this.mode = mode;
		B1 = b1;
		this.util = util;

		mainURL = "http://www.marinetraffic.com/ais/gr/datasheet.aspx?";
		mainURL += portName.equals("") ? "PORT_NAME=PIRAEUS" : "PORT_NAME=" + portName;
		mainURL += shipName.equals("") ? "&SHIPNAME=" : "&SHIPNAME=" + shipName;
		mainURL += dataSource.equals("") ? "&dataSource=V_ETA" : "&datasource=" + dataSource;
		mainURL += mode.equals("") ? "&mode=" : "&mode=" + mode;
		mainURL += b1.equals("") ? "&B1=%CE%91%CE%BD%CE%B1%CE%B6%CE%AE%CF%84%CE%B7%CF%83%CE%B7" : "&B1=" + b1;
	}

	public List<ShipInfo> parsePage() throws Exception {
		try {
			return parse(mainURL);
		}
		catch(Exception e) {
			try {
				return parse(failoverURL);
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * This method just extracts the links and the "body" text from the page.
	 * @param pageToParse
	 * @throws Exception
	 */
	public List<ShipInfo> parse(String pageToParse) throws Exception {
		String pageContent = "";
		pageContent = util.getContent(pageToParse, null);
		try {
			MarineTrafficHTMLParser pp = new MarineTrafficHTMLParser();
			return pp.parseContent(pageContent);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String requestWithAlteredAgent(String urlString) throws IOException {
		
		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();
		conn.setRequestProperty("User-Agent",
		        "Mozilla/5.0 (Windows NT 5.1; rv:19.0) Gecko/20100101 Firefox/19.0");
		conn.connect();
		
		BufferedReader serverResponse = new BufferedReader(
				new InputStreamReader(conn.getInputStream())
				);
		
		String theLine = null;
		StringBuilder stringBuilder = new StringBuilder();
		
		while ( (theLine=serverResponse.readLine())!=null ) {
			stringBuilder.append(theLine);
		}
		
		serverResponse.close();
		
		return stringBuilder.toString();
	}
	
}
