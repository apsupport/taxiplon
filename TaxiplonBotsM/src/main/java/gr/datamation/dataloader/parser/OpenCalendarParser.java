package gr.datamation.dataloader.parser;

import java.util.ArrayList;
import java.util.List;

import gr.datamation.dataloader.entities.OpenCalendarInfo;
import gr.datamation.dataloader.util.Utils;


public class OpenCalendarParser {
		
		private String	mainSite = "http://www.opencalendar.gr/portal/resource/event/location/attika/period/today/pageNumber/";
		private Utils util;
		
		
		public OpenCalendarParser() {
			util = new Utils();
		}

		public OpenCalendarParser(Utils util) {
			super();
			util = new Utils();
			this.util = util;
		}

		public List<OpenCalendarInfo> parsePage() throws Exception {
			try {
				List<OpenCalendarInfo> openCalendarEvents = new ArrayList<OpenCalendarInfo>();
				for (int i = 1;i <=6; i++) {
					String url = mainSite + i;
					openCalendarEvents.addAll(parse(url));
				}
				return openCalendarEvents;
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				return null;
		}
		
		/**
		 * This method just extracts the links and the "body" text from the page.
		 * @param pageToParse
		 * @throws Exception
		 */
		public List<OpenCalendarInfo> parse(String pageToParse) throws Exception {
			String pageContent = "";
			pageContent = util.getContent(pageToParse, "UTF-8");
			try {
				OpenCalendarHTMLParser pp = new OpenCalendarHTMLParser();
				return pp.parseContent(pageContent);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}	

}
