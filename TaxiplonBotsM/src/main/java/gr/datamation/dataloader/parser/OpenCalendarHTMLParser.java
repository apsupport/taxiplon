package gr.datamation.dataloader.parser;

import gr.datamation.dataloader.entities.OpenCalendarInfo;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class OpenCalendarHTMLParser extends HTMLEditorKit.ParserCallback {

	String payload;
	boolean containerStarted;
	int tagCounter;
	int startingPos;
	int startingPosFont;
	int endingPos;
	//7 columns
	int tdCounter = 0;
	boolean paragraphStarted;
	boolean smallStarted;
	boolean addToListTagStarted;
	
	private List<OpenCalendarInfo> openCalendarInfoList = new ArrayList<OpenCalendarInfo>();
	private OpenCalendarInfo currentOpenCalendarInfo;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * Parses the page to find just one block per page and returns that block back to the caller.
	 * @param htmlDocument
	 */
	public List<OpenCalendarInfo> parseContent(String htmlDocument) throws Exception {
		tagCounter = 0;
		containerStarted = false;
		HTMLEditorKit.Parser delegator = new ParserDelegator();
		delegator.parse(new StringReader(htmlDocument), this, true);
	    //sort(openCalendarInfoList);
		return openCalendarInfoList;
	}
	
	/**
	 * Sorts the list by scheduled arrival time.
	 * @param boats
	 */
/*	public void sort(List<OpenCalendarInfo> boats) {
	  Object[] boatsArray = boats.toArray();
	  Arrays.sort(boatsArray, new Comparator<Object>() {
		  @Override
		  public int compare(Object left, Object right) {
			  return ((OpenCalendarInfo) left).getProgrammedArrival().compareTo(((OpenCalendarInfo) right).getProgrammedArrival());
		  }
	  });
	  boats.clear();
	  for(Object o : boatsArray) {
		  boats.add((OpenCalendarInfo) o);
	  }
  }*/

	public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		if (t.toString().equalsIgnoreCase("table")) {
			Object name = a.getAttribute(HTML.Attribute.CLASS);
/*			if (name != null && name.toString().equalsIgnoreCase("flights")) {
				containerStarted = true;
				startingPos = pos;
			}*/
			if (name != null && name.toString().equalsIgnoreCase("contentObjectList")) {
				containerStarted = true;
				tagCounter++;
			}
/*			if (containerStarted) {
				tagCounter++;
			}*/
		}
		else if (t.toString().equalsIgnoreCase("td") && containerStarted) {
			if (tdCounter == 0) {
				currentOpenCalendarInfo = new OpenCalendarInfo();
				currentOpenCalendarInfo.setEventType("");
			}
			tdCounter++;
		}
		else if (t.toString().equalsIgnoreCase("p") && containerStarted) {
			paragraphStarted = true;
		}
		else if (t.toString().equalsIgnoreCase("small") && containerStarted) {
			smallStarted = true;
		}
		else if (t.toString().equalsIgnoreCase("span") && containerStarted) {
			addToListTagStarted = true;
		}
		
	}

	public void handleEndTag(HTML.Tag t, int pos) {
		if (t.toString().equalsIgnoreCase("table") && containerStarted) {
			if (--tagCounter == 0) {
				containerStarted = false;
				endingPos = pos;
			}
		}
		else if (t.toString().equalsIgnoreCase("p") && containerStarted) {
			paragraphStarted = false;
		}
		else if (t.toString().equalsIgnoreCase("small") && containerStarted) {
			smallStarted = false;
		}
		else if (t.toString().equalsIgnoreCase("span") && containerStarted) {
			addToListTagStarted = false;
		}
	}

	public void handleText(char[] data, int pos) {
		if (containerStarted) {
			payload = new String(data);
			payload = payload.replaceAll("\\u00a0", "");
			payload = payload.trim();
			switch (tdCounter) {
				case 2: 
					if (!payload.trim().equals("")) {
						if (paragraphStarted)
							currentOpenCalendarInfo.setDescription(payload);
						else if (smallStarted) {
							if ("".equals(currentOpenCalendarInfo.getEventType()))
								currentOpenCalendarInfo.setEventType(payload);
							else
								currentOpenCalendarInfo.setEventType(currentOpenCalendarInfo.getEventType() + ", " + payload);
						}
						else if (!addToListTagStarted) {
							if (payload.indexOf("'") >= 0) {
								try {
									currentOpenCalendarInfo.setNameOfEvent(payload.substring(1, payload.lastIndexOf("'")));
								}
								catch (StringIndexOutOfBoundsException e) {
									//Forgotten second quote in title. In this case, set the name of the event as payload itself
									currentOpenCalendarInfo.setNameOfEvent(payload);
								}
							}
							else
								currentOpenCalendarInfo.setNameOfEvent(payload);
						}
					}
					break;
				case 3:
					String[] splitAddress = payload.split(",");
					if (splitAddress.length > 1) {
						currentOpenCalendarInfo.setPlaceOfEvent(splitAddress[0]);
						if (splitAddress[1].indexOf("*") > 0)
							currentOpenCalendarInfo.setAddressOfEvent(splitAddress[1].substring(0, splitAddress[1].indexOf("*") -1));
						else
							currentOpenCalendarInfo.setAddressOfEvent(splitAddress[1]);
					}
					else {
						currentOpenCalendarInfo.setPlaceOfEvent(payload);
						currentOpenCalendarInfo.setAddressOfEvent(payload); 						
					}
					break;
				case 4:
					System.out.println("------  " + new String(data));
					if (payload.indexOf(":") < 0) {
						System.out.println("==============> Excluding: " + currentOpenCalendarInfo.getNameOfEvent());
						currentOpenCalendarInfo = null;
					}
					else {
						if (payload.indexOf(":") == payload.lastIndexOf(":")) {
							Calendar c = Calendar.getInstance();
							String[] dateData = payload.split(",");
							try {
								String[] timeData = dateData[1].split(":");
								c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeData[0].replace(" ", "")));
								c.set(Calendar.MINUTE, Integer.parseInt(timeData[1].replace(" ", "")));
							    c.set(Calendar.SECOND, 0);
							    currentOpenCalendarInfo.setEventStartDateTime(c);
							}
							catch (Exception e) {
								currentOpenCalendarInfo = null;
							}
						}
						else {
							currentOpenCalendarInfo.setEventDateTimeString(payload);
						}
					}

					break;
/*				case 2: currentOpenCalendarInfo.setVesselName(payload); break;
				case 3: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentOpenCalendarInfo.setProgrammedArrival(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;
				case 4: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentOpenCalendarInfo.setEstimatedArrival(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;
				case 5: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentOpenCalendarInfo.setArrival(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;*/
			}
			if (tdCounter == 4) {
				tdCounter = 0;
				if (currentOpenCalendarInfo != null)
					openCalendarInfoList.add(currentOpenCalendarInfo);
			}
		}
	}

}

