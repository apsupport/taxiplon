package gr.datamation.dataloader.parser;

import gr.datamation.dataloader.entities.ShipInfo;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class MarineTrafficHTMLParser extends HTMLEditorKit.ParserCallback {

	String payload;
	boolean containerStarted;
	int tagCounter;
	int startingPos;
	int startingPosFont;
	int endingPos;
	//7 columns
	int tdCounter = 0;
	
	private List<ShipInfo> shipInfoList = new ArrayList<ShipInfo>();
	private ShipInfo currentShipInfo;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * Parses the page to find just one block per page and returns that block back to the caller.
	 * @param htmlDocument
	 */
	public List<ShipInfo> parseContent(String htmlDocument) throws Exception {
		tagCounter = 0;
		containerStarted = false;
		HTMLEditorKit.Parser delegator = new ParserDelegator();
		delegator.parse(new StringReader(htmlDocument), this, true);
	    sort(shipInfoList);
		return shipInfoList;
	}
	
	/**
	 * Sorts the list by scheduled arrival time.
	 * @param boats
	 */
	public void sort(List<ShipInfo> boats) {
	  Object[] boatsArray = boats.toArray();
	  Arrays.sort(boatsArray, new Comparator<Object>() {
		  @Override
		  public int compare(Object left, Object right) {
			  return ((ShipInfo) left).getProgrammedArrival().compareTo(((ShipInfo) right).getProgrammedArrival());
		  }
	  });
	  boats.clear();
	  for(Object o : boatsArray) {
		  boats.add((ShipInfo) o);
	  }
  }

	public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		if (t.toString().equalsIgnoreCase("table")) {
/*			Object name = a.getAttribute(HTML.Attribute.CLASS);
			if (name != null && name.toString().equalsIgnoreCase("flights")) {
				containerStarted = true;
				startingPos = pos;
			}*/
			containerStarted = true;
			tagCounter++;
/*			if (containerStarted) {
				tagCounter++;
			}*/
		}
		else if (t.toString().equalsIgnoreCase("font") && containerStarted) {
			startingPosFont = pos;
			if (tdCounter == 0)
				currentShipInfo = new ShipInfo();
			tdCounter++;
		}
	}

	public void handleEndTag(HTML.Tag t, int pos) {
		if (t.toString().equalsIgnoreCase("table") && containerStarted) {
			if (--tagCounter == 0) {
				containerStarted = false;
				endingPos = pos;
			}
		}
		else if (t.toString().equalsIgnoreCase("td") && containerStarted) {
			startingPosFont = -1;
		}
	}

	public void handleText(char[] data, int pos) {
		if (containerStarted && startingPosFont > 0) {
			payload = new String(data);
			payload = payload.replaceAll("\\u00a0", "");
			payload = payload.trim();
			switch (tdCounter) {
				case 1: currentShipInfo.setPort(payload); break;
				case 2: currentShipInfo.setVesselName(payload); break;
				case 3: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentShipInfo.setProgrammedArrival(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;
				case 4: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentShipInfo.setEstimatedArrival(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;
				case 5: try {
					if (payload.replaceAll(" ", "").length() > 0)
						currentShipInfo.setArrival(sdf.parse(payload));
				} catch (ParseException e) {
					e.printStackTrace();
				} break;
			}
			if (tdCounter == 6) {
				tdCounter = 0;
				shipInfoList.add(currentShipInfo);
			}
		}
	}

}

