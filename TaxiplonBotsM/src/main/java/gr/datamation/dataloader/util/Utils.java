package gr.datamation.dataloader.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	private String sessionCharset;
	int requestCounter;
	
	public Utils() {
	    sessionCharset = null;
	}

	public String getContent(String serviceUrl, String serviceCharset) throws Exception {
		return getContent(serviceUrl, serviceCharset, true);
	}

	/**
	 * Fetch the content of service at the given URL with provided parameters.
	 * Also transcode the input into the Encoding found on the page.
	 * @param serviceUrl
	 * @param serviceCharset
	 * @return
	 * @throws Exception
	 */
	public String getContent(String serviceUrl, String serviceCharset, boolean allowSessionCharsetChange) throws Exception {
	    HttpURLConnection serviceConnection = (HttpURLConnection) new URL(serviceUrl).openConnection();
		serviceConnection.addRequestProperty("User-Agent", "blah"); //google denies access to some pages when User-Agent is Java
	    //read returned output
		BufferedReader in =
				  (serviceCharset != null) ? new BufferedReader(new InputStreamReader(serviceConnection.getInputStream(),
																					  serviceCharset)) :
				  new BufferedReader(new InputStreamReader(serviceConnection.getInputStream()));
	    requestCounter++;
		String inputLine;
		String returnedContent = "";
		//<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		Pattern charsetPattern = Pattern.compile("<meta.*content=\"text/html;.*charset=([A-Za-z0-9\\-]*)\"");
		while ((inputLine = in.readLine()) != null) {
			if (serviceCharset == null) {
				//if charset is not predefined, then find out and format all contents using this charset
				Matcher m = charsetPattern.matcher(inputLine);
				if (m.find()) {
				    serviceCharset = m.group(1);
					if(allowSessionCharsetChange) {
						sessionCharset = serviceCharset.toUpperCase();
					}
				}

			}
			returnedContent += sessionCharset == null? inputLine : new String(inputLine.getBytes(), sessionCharset);
		}
		in.close();
		return returnedContent;
	}
	
}
