package gr.datamation.dataloader;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import gr.datamation.dataloader.entities.FlightInfo;
import gr.datamation.dataloader.entities.OpenCalendarInfo;
import gr.datamation.dataloader.entities.ShipInfo;
import gr.datamation.dataloader.entities.TrainInfo;
import gr.datamation.dataloader.parser.AIAParser;
import gr.datamation.dataloader.parser.MarineTrafficParser;
import gr.datamation.dataloader.parser.OSEParser;
import gr.datamation.dataloader.parser.OpenCalendarParser;

public class MainProgram {

	
	public static void main(String args[]) {
//		try {
//			AIAParser parser = new AIAParser(1, 1, 100, false, 1, true, "", "", 2);
//			List<FlightInfo> list = parser.parsePage();
//			Iterator<FlightInfo> it = list.iterator();
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
//			while (it.hasNext()) {
//				FlightInfo fi = it.next();
//				System.out.println(	padEmptySpace(fi.getFromTo(), 21) + "  " + padEmptySpace(fi.getCompany(), 22) + "  " + padEmptySpace(fi.getFlightNo(), 10) + 
//									"  " + padEmptySpace(fi.getVia(), 5) + "  " + padEmptySpace(fi.getExpectedAD() == null ? "" : sdf.format(fi.getExpectedAD()), 15) +
//									"     " + padEmptySpace(fi.getProgrammedAD() == null ? "" : sdf.format(fi.getProgrammedAD()), 15) + 
//									"     " + padEmptySpace(fi.getNotes(), 10));
//			}
//		}
//		catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("\n\n\n");
//		
//		
//		try {
//			MarineTrafficParser parser2 = new MarineTrafficParser("PIRAEUS");
//			List<ShipInfo> list = parser2.parsePage();
//			Iterator<ShipInfo> it = list.iterator();
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//			while (it.hasNext()) {
//				ShipInfo si = it.next();
//				System.out.println(	padEmptySpace(si.getPort(), 10) + "  " + padEmptySpace(si.getVesselName(), 23) + "  " +
//									padEmptySpace(si.getProgrammedArrival() == null ? "" : sdf.format(si.getProgrammedArrival()), 15) + "     " +
//									padEmptySpace(si.getEstimatedArrival() == null ? "" : sdf.format(si.getEstimatedArrival()), 15) + "     " + 
//									padEmptySpace(si.getArrival() == null ? "" : sdf.format(si.getArrival()), 15) + "     ");
//			}
//		}
//		catch(Exception e) {
//			e.printStackTrace();
//		}		
/*		try {
			OpenCalendarParser parser = new OpenCalendarParser();
			List<OpenCalendarInfo> list = parser.parsePage();
//			for(OpenCalendarInfo oci : list) {
//				System.out.println(oci);
//			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}*/
		

		
		try {
		OSEParser parser = new OSEParser();
		List<TrainInfo> list = parser.parsePage();
		Iterator<TrainInfo> it = list.iterator();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		while (it.hasNext()) {
			TrainInfo ti = it.next();
			System.out.println(	padEmptySpace(ti.getFrom(), 12) + "  " + padEmptySpace(ti.getTrainCode(), 8) + "  " +
								padEmptySpace(sdf.format(ti.getArrivalDateTime().getTime()), 15));
		}
	}
	catch(Exception e) {
		e.printStackTrace();
	}
	}
	
	private static String padEmptySpace(String str, int len){
		if (str == null){
			String s = "";
			for(int i=0;i<len;i++)
				s=s.concat(" ");
			return s;
		}
		else {
			int strLen = str.length();
			for (int i=0;i<len-strLen;i++)
				str = str.concat(" "); 
			return str;	
		}
	}
}
