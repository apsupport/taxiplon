package gr.datamation.athcrawl;

import java.text.SimpleDateFormat;

import java.util.Iterator;
import java.util.List;

import gr.datamation.athcrawl.entities.AthinoramaInfo;
import gr.datamation.athcrawl.parser.AthinoramaParser;

import java.util.Locale;

public class MainProgram {


	public static void main(String[] args) {
		try {
			AthinoramaParser parser = new AthinoramaParser();
			List<AthinoramaInfo> list = parser.parsePage();
			Iterator<AthinoramaInfo> it = list.iterator();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
			while(it.hasNext()) {
				AthinoramaInfo fi = it.next();
				System.out.println(padEmptySpace(fi.getNameOfEvent(), 55) + "  " +
								   padEmptySpace(fi.getPlaceOfEvent(), 40) + "   " +
								   padEmptySpace(fi.getAddressOfEvent(), 40) + "  " +
								   padEmptySpace(fi.getEventStartDateTime() != null ?
												 sdf.format(fi.getEventStartDateTime().getTime()) : "", 15) + "  " +
								   padEmptySpace(fi.getDescription(), 100) + "  ");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		System.out.println("\n\n\n");

	}

	private static String padEmptySpace(String str, int len) {
		if(str == null) {
			String s = "";
			for(int i = 0; i < len; i++)
				s = s.concat(" ");
			return s;
		}
		else {
			int strLen = str.length();
			for(int i = 0; i < len - strLen; i++)
				str = str.concat(" ");
			return str;
		}
	}
}
