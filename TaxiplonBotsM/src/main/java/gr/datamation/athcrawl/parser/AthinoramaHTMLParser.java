package gr.datamation.athcrawl.parser;

import gr.datamation.athcrawl.entities.AthinoramaInfo;

import java.io.StringReader;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import java.util.regex.Matcher;

import java.util.regex.Pattern;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class AthinoramaHTMLParser extends HTMLEditorKit.ParserCallback {

	String payload;
	boolean containerStarted;
	int startingPos;
	int tagFlag = 0;
	int dataCounter = 0;
	boolean extraData = false;
	boolean timeData = false;
	int counter = 0;

	private AthinoramaInfo eventInfo = null;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
	private SimpleDateFormat sdf2 = new SimpleDateFormat("hh.mm a");
	private SimpleDateFormat sdf3 = new SimpleDateFormat("hh a");

	/**
	 * Parses the page to find just one block per page and returns that block back to the caller.
	 * @param htmlDocument
	 */
	public AthinoramaInfo parseContent(String htmlDocument) throws Exception {
		containerStarted = false;
		HTMLEditorKit.Parser delegator = new ParserDelegator();
		delegator.parse(new StringReader(htmlDocument), this, true);
		return eventInfo;
	}

	public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		//System.out.println(" -----START TAG-------> " + t.toString());
		if(t.toString().equalsIgnoreCase("a")) {
			Object name = a.getAttribute(HTML.Attribute.CLASS);
			if(name != null && name.toString().equalsIgnoreCase("Mobile_text_red")) {
				containerStarted = true;
				startingPos = pos;
				//NEW OBJECT IS CREATED BECAUSE NEW EVENT IS FOUND
				eventInfo = new AthinoramaInfo();
			}
			else if(name != null && name.toString().equalsIgnoreCase("Mobile_text_red_small")) {
				containerStarted = true;
				startingPos = pos;
				tagFlag = 1;
			}
		}
		else if(t.toString().equalsIgnoreCase("p")) {
			Object name = a.getAttribute(HTML.Attribute.CLASS);
			if(name != null && name.toString().equalsIgnoreCase("box_info")) {
				containerStarted = true;
				startingPos = pos;
				tagFlag = 2;
			}
		}
		else if(t.toString().equalsIgnoreCase("strong")) {
			timeData = true;
		}
	}

	public void handleEndTag(HTML.Tag t, int pos) {
		if(t.toString().equalsIgnoreCase("a") && containerStarted) {
			containerStarted = false;
			extraData = true;
			tagFlag = 3;
		}
	}

	public void handleText(char[] data, int pos) {
		if(counter == 2) {
			Calendar c = Calendar.getInstance();
			String[] dateSplit = new String(data).split(":");
//			Calendar c2 = Calendar.getInstance();
		    dateSplit[1] = dateSplit[1].trim();//replace("�.�.", "��").replace("�.�.", "��").
		    Matcher m = Pattern.compile("([0-9]*?\\.?[0-9]*?) (.*)").matcher(dateSplit[1]);
			if(m.find()) {
				String time[] = m.group(1).split("\\.");
				String pmmarker = m.group(2);
				c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]) + (pmmarker.startsWith("μ")? 12 : 0));
				c.set(Calendar.MINUTE, time.length > 1? Integer.parseInt(time[1]) : 0);
			    c.set(Calendar.SECOND, 0);
			}
//		    try {
//				c2.setTime(sdf2.parse(dateSplit[1]));
//			}//			catch(ParseException e) {
//				try {
//					c2.setTime(sdf3.parse(dateSplit[1]));
//				}
//				catch(ParseException e1) {
//					e1.printStackTrace();
//				}
//			}
			//System.out.println(" ------------> " + new String(data) + "  " + sdf2.format(c.getTime()));
			eventInfo.setEventStartDateTime(c);
			timeData = false;
			counter = 0;
		}
		if(timeData) {
			counter++;
			timeData = !timeData;
		}

		if((containerStarted && startingPos > 0) || extraData) {
			payload = new String(data);
			payload = payload.replaceAll("\\u00a0", "");
			payload = payload.trim();
			switch(tagFlag) {
				case 0:
					eventInfo.setNameOfEvent(payload);
					break;
				case 1:
					eventInfo.setPlaceOfEvent(payload);
					break;
				case 2:
					eventInfo.setDescription(payload);
					break;
				case 3:
					eventInfo.setAddressOfEvent(payload);
					extraData = false;
					break;
			}
			if(tagFlag == 1) {
				tagFlag = 0;
			}
		}
	}

}
