package gr.datamation.athcrawl.parser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gr.datamation.athcrawl.entities.AthinoramaInfo;
import gr.datamation.dataloader.util.Utils;


public class AthinoramaParser {

	private String mainSite = "http://www.athinorama.gr/mobile/music/data/days/day.aspx?id=7";
	private String baseAdr = "http://www.athinorama.gr/mobile/music/data/days/";
	private Utils util;


	public AthinoramaParser() {
		util = new Utils();
		//default GET parameters
		int id = 1;
		Calendar c = Calendar.getInstance();
		if(c.get(Calendar.DAY_OF_WEEK) == 5)
			id = 1;
		switch(c.get(Calendar.DAY_OF_WEEK)) {
			case 1:
				id = 4;
				break;
			case 2:
				id = 5;
				break;
			case 3:
				id = 6;
				break;
			case 4:
				id = 7;
				break;
			case 5:
				id = 1;
				break;
			case 6:
				id = 2;
				break;
			case 7:
				id = 3;
				break;
		}
		mainSite = "http://www.athinorama.gr/mobile/music/data/days/day.aspx?id=" + id;
	}

	public List<AthinoramaInfo> parsePage() throws Exception {
		try {
			return parse(mainSite);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * This method just extracts the links and the "body" text from the page.
	 * @param pageToParse
	 * @throws Exception
	 */
	public List<AthinoramaInfo> parse(String pageToParse) throws Exception {

		List<AthinoramaInfo> eventsList = new ArrayList<AthinoramaInfo>();
		String pageContent = "";
		pageContent = util.getContent(pageToParse, "UTF-8");
		Matcher m1 = Pattern.compile("<[aA].*?href=\"(.*?)\".*?>(.*?)<").matcher(pageContent);
		//System.out.println("Links on " + pageToParse + ":");

		while(m1.find()) {
			String link = m1.group(1);
			String title = m1.group(2);
			//System.out.println(link + " --- " + title);
			//filter out nameless links, links reloading the same page, mailto links, pdf's, javascripts
			if(link.contains("event.aspx")) {
				//normalize the link
				if(!link.startsWith("http://")) {
					link = baseAdr + link;
				}
				AthinoramaHTMLParser pp = new AthinoramaHTMLParser();
				eventsList.add(pp.parseContent(util.getContent(link, "UTF-8")));
			}
		}
		return eventsList;
	}

}
