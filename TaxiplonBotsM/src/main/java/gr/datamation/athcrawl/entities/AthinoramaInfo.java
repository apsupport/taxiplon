package gr.datamation.athcrawl.entities;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class AthinoramaInfo {

	private String 		nameOfEvent;
	private String 		placeOfEvent;
	private String 		description;
	private String 		addressOfEvent;
	private Calendar	eventStartDateTime;
	private Calendar    eventEndDateTime;
	
	
	public String getNameOfEvent() {
		return nameOfEvent;
	}
	public void setNameOfEvent(String nameOfEvent) {
		this.nameOfEvent = nameOfEvent;
	}
	public String getPlaceOfEvent() {
		return placeOfEvent;
	}
	public void setPlaceOfEvent(String nameOfPlace) {
		this.placeOfEvent = nameOfPlace;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddressOfEvent() {
		return addressOfEvent;
	}
	public void setAddressOfEvent(String addressOfEvent) {
		this.addressOfEvent = addressOfEvent;
	}
	public Calendar getEventStartDateTime() {
		return eventStartDateTime;
	}
	public void setEventStartDateTime(Calendar eventDateTime) {
		this.eventStartDateTime = eventDateTime;
	}

	public void setEventEndDateTime(Calendar eventEndDateTime) {
		this.eventEndDateTime = eventEndDateTime;
	}

	public Calendar getEventEndDateTime() {
		return eventEndDateTime;
	}
}
